
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing;

//~--- non-JDK imports --------------------------------------------------------

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author fnell
 */
public class TestMarshallTest
  {
    static TestMarshall instance;

    public TestMarshallTest()
      {
        instance = new TestMarshall();
      }

    @Test public void testRunKeywordDrivenTests()
      {
        System.out.println("runKeywordDrivenTests");
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
