
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Reporting;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.junit.Test;

/**
 *
 * @author fnell
 */
public class TestReportEmailerTest
  {
    static TestReportEmailerUtility generateReport;
    ApplicationConfig config;
    List<TestResult> testResults;

    public TestReportEmailerTest()
      {
      }

    @Test public void parameterToStringMethodTest()
      {
        config = new ApplicationConfig();
        testResults = new ArrayList<TestResult>();

        Duration testDuration = new Duration(new DateTime(), new DateTime().plusSeconds(20));

//        TestEntity test1 = new TestEntity("GSQ1", "Google Search Query");
//
//        TestEntity test2 = new TestEntity("GSR1", "Google Search Results");
//
//        TestEntity test3 = new TestEntity("TT21", "Test Test Test");
//
//        TestEntity test4 = new TestEntity("DTD1", "Demo Test Data");

//        TestResult result1 = new TestResult(test1, false, "Failed to locate search button",
//                                 testDuration.getStandardSeconds());
//
//        testResults.add(result1);
//
//        TestResult result2 = new TestResult(test2, true, "Test Passed", testDuration.getStandardSeconds() + 12);
//
//        testResults.add(result2);
//
//        TestResult result3 = new TestResult(test2, false, "Failed to locate center of the universe",
//                                 testDuration.getStandardSeconds());
//
//        testResults.add(result3);
//
//        TestResult result4 = new TestResult(test3, true, "Test Passed", testDuration.getStandardSeconds() + 66);
//
//        testResults.add(result4);

        generateReport = new TestReportEmailerUtility(testResults);

        generateReport.inputFilePath = ApplicationConfig.InputFileName();

        generateReport.SendResultsEmail();

      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
