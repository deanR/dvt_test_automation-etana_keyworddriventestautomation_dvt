
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import org.junit.Test;


/**
 *
 * @author FerdinandN
 */
public class EtanaRISplitIncorrectOnClaimRecoveriesTestpack
  {
    static TestMarshall instance;

    public EtanaRISplitIncorrectOnClaimRecoveriesTestpack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaRISplitIncorrectOnClaimRecoveriesTestpack.xlsx");
      }

    @Test public void RunEtanaCreateNewPolicyTestpack()
      {
        System.out.println("Running Etana RI Split On Incorrect Recoveries Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }

//~ Formatted by Jindent --- http://www.jindent.com
