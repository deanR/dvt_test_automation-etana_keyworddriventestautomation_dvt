
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

import org.junit.Test;

/**
 *
 * @author ferdinandN
 */
public class EtanaSASRIAScenario4TestPack
  {
    static TestMarshall instance;

    public EtanaSASRIAScenario4TestPack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario4TestPack.xlsx");

      }

    @Test public void RunEtanaSASRIAScenario4TestPack()
      {
        System.out.println("Running Etana SASRIA Scenario 4 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
