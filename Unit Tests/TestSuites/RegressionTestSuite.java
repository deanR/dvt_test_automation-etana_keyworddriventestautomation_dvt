
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

import org.junit.Test;

/**
 *
 * @author ferdinandN
 */
public class RegressionTestSuite
  {
    static TestMarshall instance;

    @Test public void RunCreateNewCorporateClientTestpack()
      {
        instance = new TestMarshall("TestPacks/EtanaCreateNewCorporateClientTestpack.xlsx");
        System.out.println("Running Etana Monster Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();

      }

    @Test public void RunCreateNewPersonalClientTestpack()
      {
        instance = new TestMarshall("TestPacks/EtanaCreateNewPersonalClientTestpack.xlsx");
        System.out.println("Running Etana Monster Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();

      }

    @Test public void RunSASRIAScenario1Testpack()
      {
        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario1TestPack.xlsx");
        System.out.println("Running Etana SASRIA Scenario 1 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();

      }

    @Test public void RunSASRIAScenario2Testpack()
      {
        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario2TestPack.xlsx");
        System.out.println("Running Etana SASRIA Scenario 2 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();

      }

    @Test public void RunSASRIAScenario3Testpack()
      {
        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario3TestPack.xlsx");
        System.out.println("Running Etana SASRIA Scenario 3 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();

      }

    @Test public void RunSASRIAScenario4Testpack()
      {
        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario4TestPack.xlsx");
        System.out.println("Running Etana SASRIA Scenario 4 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();

      }

    @Test public void RunSalvageAndThirdPartyRecoveryTestpack()
      {
        instance = new TestMarshall("TestPacks/EtanaSalvageAndThirdPartyRecoveryTestPack.xlsx");
        System.out.println("Running Etana Salvage And Third Party Recovery Test Pack on "
                           + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();

      }

    @Test public void RunCreateNewEPLMonthlyPolicyTestpack()
      {
        instance = new TestMarshall("TestPacks/EtanaCreateNewEPLMonthlyPolicyTestPack.xlsx");
        System.out.println("Running Etana Create New EPL Monthly Policy Test Pack on "
                           + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();

      }

    @Test public void RunBankingDetailsForCorporateClientTestpack()
      {
        instance = new TestMarshall("TestPacks/EtanaBankingDetailsCorporateTestPack.xlsx");
        System.out.println("Running Etana Banking Details (Add Edit Delete) for Corporate Client Test Pack on "
                           + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();

      }

    @Test public void RunBankingDetailsForPersonalClientTestpack()
      {
        instance = new TestMarshall("TestPacks/EtanaBankingDetailsPersonalTestPack.xlsx");
        System.out.println("Running Etana Banking Details (Add Edit Delete) for Personal Client Test Pack on "
                           + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();

      }

    @Test public void RunEBPMonthlyPolicyTestpack()
      {
        instance = new TestMarshall("TestPacks/CreateEBPMonthlyPolicyTestpack.xlsx");
        System.out.println("Running Etana EBP Monthly Policy Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();

      }

    @Test public void RunEHATPolicyTestpack()
      {
        instance = new TestMarshall("TestPacks/CreateNewEHATPolicy.xlsx");
        System.out.println("Running Etana EHAT Policy Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();

      }

    @Test public void RunEHATAnnaulPolicyTestpack()
      {
        instance = new TestMarshall("TestPacks/EtanaHospitalityAndTourismAnnualPolicy.xlsx");
        System.out.println("Running Etana EHAT Annual Policy Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();

      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
