
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;


/**
 *
 * @author ferdinandN
 */
public class EtanaAddEditDeleteAddressDetailsCorporateTestPack
  {
   
    static TestMarshall instance;
    

    @Test public void RunEtanaCreatePersonalClientTestPack()
      {
        instance = new TestMarshall("TestPacks/EtanaCorporateClientAddressDetails.xlsx");
        System.out.println("Running Etana Create Corporate Client Address Details Test Pack");
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
