/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TestSuites;

import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import org.junit.Test;

/**
 *
 * @author SNcantswa
 */
public class EtanaLightVehicleTestPack
{
 static TestMarshall instance;

    public EtanaLightVehicleTestPack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EkunduVehicleOlderThan20Years.xlsx");
      }

    @Test public void RunEtanaEBPMotorChangesTestpack()
      {
        System.out.println("Running Etana EBP Light Vehicle s Test Pack on"
                           + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
}
