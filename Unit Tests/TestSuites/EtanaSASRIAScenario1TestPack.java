
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

import org.junit.Test;

/**
 *
 * @author deanR
 */
public class EtanaSASRIAScenario1TestPack
  {
    static TestMarshall instance;

    public EtanaSASRIAScenario1TestPack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario1TestPack.xlsx");

      }

    @Test public void RunEtanaSASRIAScenario1TestPack()
      {
        System.out.println("Running Etana SASRIA Scenario1 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
