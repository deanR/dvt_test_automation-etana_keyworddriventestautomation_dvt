
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Reporting.ReportGeneratorTest;

import KeywordDrivenTestFramework.Testing.TestMarshallTest;

import KeywordDrivenTestFramework.Utilities.ExcelReaderUtilityTest;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Ferdi
 */
@RunWith(Suite.class) @Suite.SuiteClasses({ ReportGeneratorTest.class, ExcelReaderUtilityTest.class,
        TestMarshallTest.class })
public class FullTest
  {
    @BeforeClass public static void setUpClass() throws Exception
      {
      }

    @AfterClass public static void tearDownClass() throws Exception
      {
      }

    @Before public void setUp() throws Exception
      {
      }

    @After public void tearDown() throws Exception
      {
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
