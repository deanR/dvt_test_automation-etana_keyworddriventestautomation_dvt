
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import org.junit.Test;

/**
 *
 * @author FerdinandN
 */
public class EtanaCreateNewEDPLPolicyForPersonalClientTestpack {

    static TestMarshall instance;

    public EtanaCreateNewEDPLPolicyForPersonalClientTestpack() {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaCreateNewEDPLPolicyForPersonalClientTestpack2.xlsx");
    }

    @Test
    public void RunEtanaCreateNewPolicyForNewClientTestpack() {
        System.out.println("Running Etana Create New Policy for New Client Test Pack on "
                + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
