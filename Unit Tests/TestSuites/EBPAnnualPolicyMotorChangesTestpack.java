/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TestSuites;

import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import org.junit.Test;

/**
 *
 * @author ferdinandN
 */
public class EBPAnnualPolicyMotorChangesTestpack 
{
    static TestMarshall instance;

    public EBPAnnualPolicyMotorChangesTestpack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaEBPAnnualMotorChanges.xlsx");
      }

    @Test public void RunEtanaEBPMotorChangesTestpack()
      {
        System.out.println("Running Etana EBP Motor changes Test Pack on "
                           + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
}
