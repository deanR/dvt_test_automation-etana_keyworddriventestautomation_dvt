
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

import org.junit.Test;

/**
 *
 * @author deanR
 */
public class EtanaSalvageAndThirdPartyRecoveryTestPack
  {
    static TestMarshall instance;

    public EtanaSalvageAndThirdPartyRecoveryTestPack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaSalvageAndThirdPartyRecoveryTestPack.xlsx");

      }

    @Test public void RunEtanaSalvageRecoveryTestPack()
      {
        System.out.println("Running Salvage and third party Recovery Test Pack on "
                           + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
