/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import static TestSuites.Full_Test.instance;
import org.junit.Test;

/**
 *
 * @author ferdinandN
 */
public class EtanaMonsterTestPack {

    @Test
    public void RunEtanaMonsterTestPack() {
        instance = new TestMarshall("TestPacks/EtanaMonsterTestPack.xlsx");
        System.out.println("Running Etana Monster Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
    }
}
