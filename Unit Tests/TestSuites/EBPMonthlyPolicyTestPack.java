
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

import org.junit.Test;

/**
 *
 * @author ferdinandN
 */
public class EBPMonthlyPolicyTestPack
  {
    static TestMarshall instance;

    public EBPMonthlyPolicyTestPack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/CreateEBPMonthlyPolicyTestpack.xlsx");
      }

    @Test public void RunEtanaCreateNewEBPMonthlyPolicyTestpack()
      {
        System.out.println("Running Etana Create New EBP Monthly Policy Test Pack on "
                           + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
