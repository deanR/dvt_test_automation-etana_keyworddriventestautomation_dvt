
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

import org.junit.Test;

import static TestSuites.EtanaCreateNewEBPPolicyForCorporateClientTestpack.instance;

/**
 *
 * @author FerdinandN
 */
public class EtanaNewEPLPolicyTestPack
  {
    static TestMarshall instance;

    public EtanaNewEPLPolicyTestPack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaNewEPLPolicyTestPack.xlsx");
      }

    @Test public void RunEtanaNewEPLPolicyTestPack()
      {
        System.out.println("Running Etana New EPL Policy Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
