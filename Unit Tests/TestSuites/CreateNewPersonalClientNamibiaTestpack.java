/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TestSuites;

import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;

/**
 *
 * @author ferdinandN
 */
public class CreateNewPersonalClientNamibiaTestpack
{
    static TestMarshall instance;

    public CreateNewPersonalClientNamibiaTestpack()
      {
        instance = new TestMarshall("TestPacks/EtanaCreateNewPersonalClientNamibiaTestpack.xlsx");
      }

    @Test public void RunEtanaCreateCorporateClientTestPack()
      {
        System.out.println("Running Etana Create Personal Client Test Pack");
        instance.runKeywordDrivenTests();
      }
}
