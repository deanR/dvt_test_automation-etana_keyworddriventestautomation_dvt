
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

import org.junit.Test;

import static TestSuites.EtanaCreateNewPolicyTestpack.instance;

/**
 *
 * @author FerdinandN
 */
public class EtanaEBPFinalisePolicyAndOpenClaimTestPack
  {
    static TestMarshall instance;

    public EtanaEBPFinalisePolicyAndOpenClaimTestPack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaFinalisePolicyAndOpenClaimTestPack.xlsx");
      }

    @Test public void RunEtanaEBPFinalisePolicyAndOpenClaimTestPack()
      {
        System.out.println("Running Etana EBP Finalise Policy and Open Claim Test Pack on "
                           + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
