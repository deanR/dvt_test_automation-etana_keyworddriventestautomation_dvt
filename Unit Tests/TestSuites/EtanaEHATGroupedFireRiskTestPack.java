
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import org.junit.Test;

/**
 *
 * @author deanR
 */
public class EtanaEHATGroupedFireRiskTestPack
  {
    static TestMarshall instance;

    public EtanaEHATGroupedFireRiskTestPack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/CreateNewEHATPolicy.xlsx");

      }

    @Test public void RunEtanaHospitalityAndTourismAnnualPolicyTestPack()
      {
        System.out.println("Running Etana Hospitality and Tourism Annual Policy Test Pack on "
                           + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
