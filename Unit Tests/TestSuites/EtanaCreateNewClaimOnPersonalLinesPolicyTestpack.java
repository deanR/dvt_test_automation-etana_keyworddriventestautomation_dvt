
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

import org.junit.Test;

import static TestSuites.EtanaCreateNewPolicyForNewClientTestpack.instance;

/**
 *
 * @author FerdinandN
 */
public class EtanaCreateNewClaimOnPersonalLinesPolicyTestpack
  {
    static TestMarshall instance;

    public EtanaCreateNewClaimOnPersonalLinesPolicyTestpack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaCreateNewClaimOnPersonalLinesPolicyTestpack.xlsx");
      }

    @Test public void RunEtanaCreateNewClaimOnPersonalLinesPolicyTestpack()
      {
        System.out.println("Running Etana Open New Claim on Personal Lines Policy Test Pack on "
                           + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
