
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

import org.junit.Test;

import static TestSuites.EtanaSASRIAScenario4TestPack.instance;

/**
 *
 * @author deanR
 */
public class EtanaSASRIAFullTest
  {
    @Test public void RunEtanaSASRIAScenario1TestPack()
      {
        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario1TestPack.xlsx");
        System.out.println("Running Etana SASRIA Scenario1 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }

    @Test public void RunEtanaSASRIAScenario2TestPack()
      {
        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario2TestPack.xlsx");
        System.out.println("Running Etana SASRIA Scenario 2 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }

    @Test public void RunEtanaSASRIAScenario3TestPack()
      {
        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario3TestPack.xlsx");
        System.out.println("Running Etana SASRIA Scenario 3 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }

    @Test public void RunEtanaSASRIAScenario4TestPack()
      {
        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario4TestPack.xlsx");
        System.out.println("Running Etana SASRIA Scenario 4 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
