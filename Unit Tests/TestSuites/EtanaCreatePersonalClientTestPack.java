
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;

import org.junit.Test;

import static TestSuites.EtanaBusinessPolicyTests.instance;

/**
 *
 * @author FerdinandN
 */
public class EtanaCreatePersonalClientTestPack
  {
    static TestMarshall instance;

    public EtanaCreatePersonalClientTestPack()
      {
        instance = new TestMarshall("TestPacks/EtanaCreateNewPersonalClientTestpack.xlsx");
      }

    @Test public void RunEtanaCreatePersonalClientTestPack()
      {
        System.out.println("Running Etana Create Personal Client Test Pack");
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
