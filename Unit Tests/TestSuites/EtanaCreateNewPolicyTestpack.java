
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

import org.junit.Test;

import static TestSuites.EtanaBusinessPolicyTests.instance;

/**
 *
 * @author FerdinandN
 */
public class EtanaCreateNewPolicyTestpack
  {
    static TestMarshall instance;

    public EtanaCreateNewPolicyTestpack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaCreateNewPolicyTestpack.xlsx");
      }

    @Test public void RunEtanaCreateNewPolicyTestpack()
      {
        System.out.println("Running Etana Create New Policy Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
