
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

import org.junit.Test;

import static TestSuites.EtanaPayCorporateClaim1TestPack.instance;

/**
 *
 * @author FerdinandN
 */
public class EtanaPayCorporateClaim2TestPack
  {
    static TestMarshall instance;

    public EtanaPayCorporateClaim2TestPack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaPayCorporateClaim2TestPack.xlsx");

      }

    @Test public void RunEtanaPayClaim2TestPack()
      {
        System.out.println("Running Etana Pay Claim 2 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
