/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TestSuites;

import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import org.junit.Test;

/**
 * 
 * @author deanR
 */
public class EtanaOpenClaimOnPublicLiabilityRiskTestpack 
{
    static TestMarshall instance;

    public EtanaOpenClaimOnPublicLiabilityRiskTestpack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaOpenClaimOnPublicLiabilityRiskTestpack.xlsx");
      }
    @Test public void RunEtanaMidTermAdjustment3TestPack()
      {
        System.out.println("Running Etana Open Claim on public liability risk Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }
