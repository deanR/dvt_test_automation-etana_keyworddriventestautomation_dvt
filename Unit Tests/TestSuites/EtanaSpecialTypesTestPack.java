/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TestSuites;

import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import org.junit.Test;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class EtanaSpecialTypesTestPack {
  
  
  static TestMarshall instance;

    public EtanaSpecialTypesTestPack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaSpecialTypesTestPack.xlsx");

      }

    @Test public void RunEtanaSpecialTypesTestPack()
      {
        System.out.println("Running Etana Special Types Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }

}
