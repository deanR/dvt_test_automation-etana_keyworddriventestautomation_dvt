
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

import org.junit.Test;

/**
 *
 * @author deanR
 */
public class EtanaAddEditDeleteBankingDetailsCorporateTestPack
  {
    static TestMarshall instance;

    public EtanaAddEditDeleteBankingDetailsCorporateTestPack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaBankingDetailsCorporateTestPack.xlsx");

      }

    @Test public void RunEtanaAddEditDeleteBankingDetailsCorporateTestPack()
      {
        System.out.println("Running Banking Details (Add Edit Delete) for Corporate Client Test Pack on "
                           + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
