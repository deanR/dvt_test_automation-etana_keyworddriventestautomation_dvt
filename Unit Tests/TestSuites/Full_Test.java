
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import org.junit.Test;


/**
 *
 * @author ferdinandN
 */
public class Full_Test
  {

     static TestMarshall instance;
     
    @Test public void RunEtanaCreateCorporateClientTestPack()
      {
        instance = new TestMarshall("TestPacks/EtanaCreateNewCorporateClientTestpack.xlsx");
        System.out.println("Running Etana Create Corporate Client Test Pack");
        instance.runKeywordDrivenTests();
      }

    @Test public void RunEtanaCreatePersonalClientTestPack()
      {
        instance = new TestMarshall("TestPacks/EtanaCreateNewPersonalClientTestpack.xlsx");
        System.out.println("Running Etana Create Personal Client Test Pack");
        instance.runKeywordDrivenTests();
      }

    @Test public void RunEtanaMonsterTestPack()
      {
        instance = new TestMarshall("TestPacks/EtanaMonsterTestPack.xlsx");
        System.out.println("Running Etana Monster Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
    
    @Test public void RunEtanaSASRIAScenario1TestPack()
      {
        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario1TestPack.xlsx");
        System.out.println("Running Etana SASRIA Scenario1 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }

  
    @Test
    public void RunEtanaSASRIAScenario2TestPack()
    {
        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario2TestPack.xlsx");
        System.out.println("Running Etana SASRIA Scenario 2 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
    }

    @Test
    public void RunEtanaSASRIAScenario3TestPack()
    {
        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario3TestPack.xlsx");
        System.out.println("Running Etana SASRIA Scenario 3 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
    } 

    @Test
    public void RunEtanaSASRIAScenario4TestPack()
    {
        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario4TestPack.xlsx");
        System.out.println("Running Etana SASRIA Scenario 4 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
    }



    @Test
    public void RunEtanaCreateNewEBPAndEPLPolicyTestpack()
    {
        instance = new TestMarshall("TestPacks/EtanaCreateNewEBPPolicyForCorporateClientTestpack.xlsx");
        System.out.println("Running Etana Create New Policy Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();

        instance = new TestMarshall("TestPacks/EtanaCreateNewEDPLPolicyForPersonalClientTestpack.xlsx");
        System.out.println("Running Etana Create New Policy for New Client Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
    }

    @Test
    public void RunEtanaCreateNewEDPLPolicyForNewClientTestpack()
    {
        instance = new TestMarshall("TestPacks/EtanaCreateNewEDPLPolicyForPersonalClientTestpack.xlsx");
        System.out.println("Running Etana Create New Policy for New Client Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();    
    }

  }


//~ Formatted by Jindent --- http://www.jindent.com
