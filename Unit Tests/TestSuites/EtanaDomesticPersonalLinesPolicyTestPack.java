
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;

import org.junit.Test;

import static TestSuites.EtanaBusinessPolicyTests.instance;

/**
 *
 * @author FerdinandN
 */
public class EtanaDomesticPersonalLinesPolicyTestPack
  {
    public EtanaDomesticPersonalLinesPolicyTestPack()
      {
        instance = new TestMarshall("TestPacks/EtanaDomesticPersonalLinesPolicyTestPack.xlsx");
      }

    @Test public void RunEtanaDomesticPolicyTests()
      {
        System.out.println("Running Etana Domestic Personal Lines Policy Test Pack");
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
