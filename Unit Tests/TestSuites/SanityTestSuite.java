
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import org.junit.Test;


/**
 *
 * @author ferdinandN
 */
public class SanityTestSuite
  {
    static TestMarshall instance;

    @Test public void RunMonsterTestPack()
      {
        instance = new TestMarshall("TestPacks/EtanaMonsterTestPack.xlsx");
        System.out.println("Running Etana Monster Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();

      }

    @Test public void RunEBPTestPack()
      {
        instance = new TestMarshall("TestPacks/EtanaCreateNewEBPPolicyForCorporateClientTestpack.xlsx");
        System.out.println("Running Etana Create New EBP Policy Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();

      }

    @Test public void RunEDPLTestPack()
      {
        instance = new TestMarshall("TestPacks/EtanaCreateNewEDPLPolicyForPersonalClientTestpack.xlsx");
        System.out.println("Running Etana Create New EDPL Policy Test Pack on" + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
