/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TestSuites;

import static KeywordDrivenTestFramework.Core.BaseClass.CurrentEnvironment;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.Enums.TestEnvironments;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import org.junit.Test;

/**
 *
 * @author ferdinandN
 */
public class SADCNamPersonalLinesAnnualPolicy
{
    static TestMarshall instance;

    public SADCNamPersonalLinesAnnualPolicy()
      {
         ApplicationConfig.EkunduPageUrl =  TestEnvironments.NamQA.EkunduUrl;
         instance.CurrentEnvironment = Enums.TestEnvironments.NamQA;
        instance = new TestMarshall("TestPacks/EtanaSADCPersonalLinesPolicy.xlsx");
      }

    @Test 
    public void RunEtanaCreateCorporateClientTestPack()
      {
        System.out.println("Running Etana SADC Personal Lines Annual Policy Test Pack");
        instance.runKeywordDrivenTests();
      }
}
