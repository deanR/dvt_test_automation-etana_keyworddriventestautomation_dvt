
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;
import org.junit.Test;


/**
 *
 * @author FerdinandN
 */
public class EtanaCreateCorporateClientTestPack
  {
    static TestMarshall instance;

    public EtanaCreateCorporateClientTestPack()
      {
        instance = new TestMarshall("TestPacks/EtanaCreateNewCorporateClientTestpack.xlsx");
      }

    @Test public void RunEtanaCreateCorporateClientTestPack()
      {
        System.out.println("Running Etana Create Corporate Client Test Pack");
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
