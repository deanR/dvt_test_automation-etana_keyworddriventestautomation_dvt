
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

import org.junit.Test;

/**
 *
 * @author deanR
 */
public class EtanaAddEditDeleteBankingDetailsPersonalTestPack
  {
    static TestMarshall instance;

    public EtanaAddEditDeleteBankingDetailsPersonalTestPack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaBankingDetailsPersonalTestPack.xlsx");

      }

    @Test public void RunEtanaAddEditDeleteBankingDetailsTestPack()
      {
        System.out.println("Running Banking Details (Add Edit Delete) for Personal Client Test Pack on "
                           + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
