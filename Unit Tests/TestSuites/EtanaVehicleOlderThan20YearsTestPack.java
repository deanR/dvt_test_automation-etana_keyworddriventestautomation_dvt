/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TestSuites;

import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import org.junit.Test;

/**
 *
 * @author SNcantswa
 */
public class EtanaVehicleOlderThan20YearsTestPack 
{
    static TestMarshall instance;

    public EtanaVehicleOlderThan20YearsTestPack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EkunduVehicleOlderThan20Years.xlsx");
      }

    @Test public void RunEtanaEBPMotorChangesTestpack()
      {
        System.out.println("Running Etana EBP Vehicle Older Than 20 Years Test Pack on "
                           + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
}
