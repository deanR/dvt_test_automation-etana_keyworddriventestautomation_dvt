/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TestSuites;

import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import org.junit.Test;

/**
 *
 * @author ferdinandN
 */
public class EPLMotorChangesTestpack 
{
    static TestMarshall instance;

    public EPLMotorChangesTestpack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaEPLDomesticAnnualMotorChanges.xlsx");
      }

    @Test public void RunEtanaEPLMotorChangesTestpack()
      {
        System.out.println("Running Etana EPL Motor changes Test Pack on "
                           + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
}
