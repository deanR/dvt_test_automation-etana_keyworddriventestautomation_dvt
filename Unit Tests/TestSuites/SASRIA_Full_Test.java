/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TestSuites;

import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import static TestSuites.EtanaSASRIAScenario3TestPack.instance;
import org.junit.Test;


public class SASRIA_Full_Test 
{
    @Test public void RunEtanaSASRIAScenario1TestPack()
      {
        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario1TestPack.xlsx");
        System.out.println("Running Etana SASRIA Scenario1 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
    
    @Test public void RunEtanaSASRIAScenario2TestPack()
      {
          
        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario2TestPack.xlsx");  
        System.out.println("Running Etana SASRIA Scenario 2 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
    
    @Test public void RunEtanaSASRIAScenario3TestPack()
      {
        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario3TestPack.xlsx");  
        System.out.println("Running Etana SASRIA Scenario 3 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }

    
    @Test public void RunEtanaSASRIAScenario4TestPack()
      {
          
        instance = new TestMarshall("TestPacks/EtanaSASRIAScenario4TestPack.xlsx");  
        System.out.println("Running Etana SASRIA Scenario 4 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
}
