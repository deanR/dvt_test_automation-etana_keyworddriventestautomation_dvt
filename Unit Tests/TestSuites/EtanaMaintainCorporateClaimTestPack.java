
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import org.junit.Test;

/**
 *
 * @author FerdinandN
 */
public class EtanaMaintainCorporateClaimTestPack {

    static TestMarshall instance;

    public EtanaMaintainCorporateClaimTestPack() {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaMaintainCorporateClaimTestPack.xlsx");
    }

    @Test
    public void RunEtanaMaintainClaimTestPack() {
        System.out.println("Running Etana Maintain Claim Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
