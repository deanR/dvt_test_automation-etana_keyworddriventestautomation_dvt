
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

import org.junit.Test;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class EtanaAddEditDeleteAddressDetailsPersonalTestPack
  {
    static TestMarshall instance;

    public EtanaAddEditDeleteAddressDetailsPersonalTestPack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaPersonalClientAddress.xlsx");

      }

    @Test public void RunEtanaAddEditDeleteBankingDetailsCorporateTestPack()
      {
        System.out.println("Running Personal Client Address (Add Edit Delete) for Personal Client Test Pack on "
                           + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
