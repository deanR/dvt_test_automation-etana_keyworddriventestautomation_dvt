
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import org.junit.Test;

/**
 *
 * @author FerdinandN
 */
public class EtanaPayCorporateClaim1TestPack {

    static TestMarshall instance;

    public EtanaPayCorporateClaim1TestPack() {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaPayCorporateClaim1TestPack.xlsx");

    }

    @Test
    public void RunEtanaPayClaim1TestPack() {
        System.out.println("Running Etana Pay Claim 1 Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
