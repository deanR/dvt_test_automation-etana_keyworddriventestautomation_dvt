
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import org.junit.Test;


/**
 *
 * @author FerdinandN
 */
public class EtanaCreateNewClaimOnEBPTestpack
  {
    static TestMarshall instance;

    public EtanaCreateNewClaimOnEBPTestpack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaCreateNewClaimOnEBPTestpack.xlsx");
      }

    @Test public void RunEtanaCreateNewClaimTestpack()
      {
        System.out.println("Running Etana Open New Claim Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
