
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package TestSuites;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Testing.TestMarshall;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

import org.junit.Test;

/**
 *
 * @author Ferdinand
 */
public class EtanaBusinessPolicyTests
  {
    static TestMarshall instance;

    public EtanaBusinessPolicyTests()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance =
            new TestMarshall("C:/Automation_DVT/etana_keyworddriventestautomation_dvt/TestPacks/EtanaBusinessPolicyPack.xlsx");

        instance = new TestMarshall("TestPacks/EtanaBusinessPolicyPack.xlsx");

      }

    @Test public void RunEtanaBusinessPolicyTests()
      {
        System.out.println("Running Etana Business Policy Test Pack on " + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
