/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TestSuites;

import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import org.junit.Test;

/**
 * 
 * @author deanR
 */
public class EtanaEBPMotorChangesMonthlyPolicyTestPack 
{
    static TestMarshall instance;

    public EtanaEBPMotorChangesMonthlyPolicyTestPack()
      {
        ApplicationConfig appConfig = new ApplicationConfig();

        instance = new TestMarshall("TestPacks/EtanaEBPMotorChangesMonthlyPolicyTestPack.xlsx");
      }

    @Test public void RunEtanaEPLMotorChangesTestpack()
      {
        System.out.println("Running Etana EPL Motor changes monthly Test Pack on "
                           + ApplicationConfig.EkunduPageUrl());
        instance.runKeywordDrivenTests();
      }

}
