
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Entities;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author fnell
 */
public class TestEntity
  {
    // Properties -
    // TestMethod
    // TestParameters

    // Methods -
    // constructors
    // addParameter
    // -----------------------------------------------------------------------
    // Serves as a container for each test found in the spreadsheet/test input
    // document.
    public String TestCaseId;
    public String TestMethod;
    public String TestDescription;
    public Map<String,String> TestParameters;
	
	
    public TestEntity()
    {

    }

    public TestEntity(String testCaseId, String testMethod, String testDescription)
    {
            this.TestCaseId = testCaseId;
            this.TestMethod = testMethod;
            this.TestDescription = testDescription;


    }

    public void addParameter(String parameterName, String parameterValue)
      {
        if (TestParameters == null)
          {
            this.TestParameters = new HashMap<String, String>();
          }

        this.TestParameters.put(parameterName, parameterValue);
      }

    public String getData(String parameterName)
      {
        String returnedValue = this.TestParameters.get(parameterName);

        if (parameterName == null)
          {
            parameterName = "";

            System.err.println("[Error] Parameter ' " + parameterName + " ' not found");
          }

        return returnedValue;
      }

    public void updateParameter(String parameterName, String newParameterValue)
      {
        this.TestParameters.put(parameterName, newParameterValue);
      }

    public void addParameterAsFirst(String parameterName, String parameterValue)
      {
        Map<String, String> tempParamList = new HashMap<String, String>();

        tempParamList.put(parameterName, parameterValue);

        tempParamList.putAll(this.TestParameters);

        this.TestParameters = tempParamList;
      }

    @SuppressWarnings("rawtypes")
    public String testParametersToString()
      {
        if ((TestParameters != null) && (TestParameters.size() > 0))
          {
            String parameters = "";
            Iterator<Entry<String, String>> paramIterator = TestParameters.entrySet().iterator();

            while (paramIterator.hasNext())
              {
                Map.Entry pairs = (Map.Entry) paramIterator.next();

                parameters = parameters + pairs.getKey() + "|" + pairs.getValue() + ",";
                paramIterator.remove();
              }

            return parameters;
          }
        else
          {
            return "No test parameters";
          }
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
