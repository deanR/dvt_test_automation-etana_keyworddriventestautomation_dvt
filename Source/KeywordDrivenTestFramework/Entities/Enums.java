package KeywordDrivenTestFramework.Entities;

/**
 * Created with IntelliJ IDEA.
 * User: fnell
 * Date: 2013/04/26
 * Time: 3:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class Enums
  {
    public enum BrowserType { IE, FireFox, Chrome, Safari }
    
    public enum ResultStatus{ PASS, FAIL, WARNING, UNCERTAIN}
    
    
    public enum TestEnvironments
    {
        QA ("http://etaqawb04/ekundu"),
        Support("http://etasupwb08/ekundu"),
        Deploy(" http://etadplwb01/ekundu"),
        UAT ("http://etauatwb02/ekundu"), 
        Combined ("http://ETATSTWB06/eKundu"), 
        NamQA ("http://hsss4iqawb01/Centauri/(S(4dscm155kyx4wmm2wyuscd45))/secure/workmanager.aspx"),
        Finance("http://etafinwb01/ekundu");
    
        public final String EkunduUrl;
        
         TestEnvironments(String ekunduUrl)
        {
            this.EkunduUrl = ekunduUrl;
        }
         
}
                
    
    
    
  }


//~ Formatted by Jindent --- http://www.jindent.com
