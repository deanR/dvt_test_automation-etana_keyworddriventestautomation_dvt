/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.browserType;
import static KeywordDrivenTestFramework.Core.BaseClass.reportGenerator;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Reporting.ReportGenerator;
import KeywordDrivenTestFramework.Reporting.ResolveEnvironmentName;
import KeywordDrivenTestFramework.Reporting.TestReportEmailerUtility;
import KeywordDrivenTestFramework.Testing.TestClasses.BuildingsandSectionDetailsRisk;
import KeywordDrivenTestFramework.Testing.TestClasses.EBPEditElectronicEquipment;
import KeywordDrivenTestFramework.Testing.TestClasses.EBPEditGroupedFireRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EHATGrouprFireRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EKunduCreateCorporateClientClaimPublicLiabilityTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EKunduCreateCorporateClientClaimRISplitTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EKunduCreateCorporateClientClaimTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EKunduCreateCorporateClientNewTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EKunduCreateCorporateClientTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EKunduEBPMTAMotorChangesEditRollsRoyceTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EKunduEHATCreateCorporateClientNewTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EKunduEHATCreateCorporateClientTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EKunduMTAMotorChangesCopyPoloVivoTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EKunduMTAMotorChangesDeletePoloVivoTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EKunduMTAMotorChangesRollsRoyceDecreaseSumInsuredTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EKunduMTAMotorChangesTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduAdrressDetailsAddEditDeletePersonalTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduAuthoriseClaim1Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduBankDetailsAddEditDeleteCorporateTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduBankDetailsAddEditDeletePersonalTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduBroadFormLiabilityRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduBuildingsAndContentSectionDetailsSASRIATest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduBuildingsAndContentSectionDetailsTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduBuildingsAndContentsSASRIATest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduBuildingsAndContentsSASRIATest1;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduBusinessAllRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduCombinedLiabilityRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduContingentLiabilityRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduCorporateClientAddressDetailsTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduCreateCorporateClientForBankDetailsTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduCreatePersonalClientClaimTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduCreatePersonalClientForBankingDetailsTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduCreatePersonalClientNamibiaTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduCreatePersonalClientTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduCrisis24RiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduDeleteGoodsinTransitRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduDirectorsAndOfficesRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduDomesticAndPropertyLiabilityRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduDomesticAndPropertyRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPAddMotorPrivateLDVRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPAddMotorRiskMotorCycleTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPAddMotorRiskOlderThan20Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPAnnualMotorChangesTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPBusinessAllRiskMonthlyTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPBusinessAllRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPElectronicEquipement2RiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPGoodsInTransit;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPGroupedFire3Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPGroupedFireTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPMTAAddMotorRiskClassicImportsTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPMoneyRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPMonthlyMTAEditBusinessAllRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPMonthlyMTCTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPMonthlyMTRTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPMonthlyOOSMTATest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPMonthlyRenewalSelection3Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPMonthlyRenewalSelectiontTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPMonthlyRenewalSelectiontTest2;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPMotorChangesMonthlyPolicyRenewalDeleteAddTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPMotorChangesMonthlyPolicyRenewalTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPMotorSpecifiedCaravanTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPMotorSpecifiedIVECOTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPMotorSpecifiedSpecialTypeTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEBPRenewalSelectionViaFindClientTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEHATBusinessAllRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEHATElectronicEquipmentTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEPLDomesticAndPropertyTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEPLMTAAddDomesticAndPropertyTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEPLMTAAddMotorRiskAstonMartinTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEPLMTAAddMotorRiskClassicImportsTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEPLMTAAddMotorRiskMotorCycleTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEPLMTAAddMotorRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEPLMTAEditDomesticandPropertyRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEPLMTCTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEPLMTRTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEPLMotorRiskStep7Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEPLOOSTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEPLPersonalAccidentTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEPLRenewalSelectionTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEPLRenewalSelection_6Months_February_Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEPLRenewalSelection_6Months_Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEPLWaterAndPleasureCraftRisk;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEditBusinessAllRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduElectronicEquipmentRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEmployersLiabilityRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduEnrouteRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduExtendedPersonalLiabilityRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduFarePayingPassengerLiabilityTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduFidelityRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduFinaliseCorporateClientPolicyTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduFinalisePersonalClientPolicy;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduFinalisePolicyTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduGlassRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduGoodsInTransitRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduGroupedFireRiskTypeTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduInSyncMTATest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduInitiateEBPMotorChangesPolicyTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduInitiatePersonalClientPolicyTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduInitiatePolicyTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduLightVehicleTestClass;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMTA3GroupedFireTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMTAAddDometicAndPropertyTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMTADeleteDomesticAndProperty4_3Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMTADeleteGoodsInTransitTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMTADeleteMotorRisk3Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMTADomesticAndPropertyScenario4_7Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMTAExtendExpiryDateTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMTAOOSTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMTAStep13Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMTAWaterAndPleasureCraftSCenario2_1Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMTAWaterAndPleasureCraftSCenario4_2_1Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMTAWaterAndPleasureCraftScenario4_1_3Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMachineryBreakDownRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMaintainClaimSalvageRecoveryTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMaintainCorporateClientClaim1Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMaintainCorporateClientClaim2Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMaintainCorporateClientClaimRISplitTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMidTermAdjustment2Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMidTermAdjustment3Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMidTermAdjustmentTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMidtermAdjustment4Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMoneyRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMotoSectionDetailsTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMotorFleetRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMotorRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMotorSpecifiedRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduMotorTradersInternalRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduNavigateToStartPageTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduOOSMTAScenario3;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduOOSMTRTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduOutOfSyncMTAApplyChangesOnOOSVersion;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduPayClaim1Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduPayClaim2Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduPayClaimRISplitTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduPersonalAccidentRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduPersonalClientEnrouteRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduPersonalClientPersonalAccidentRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduPersonalClientRenewalSelectionScenario2Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduPersonalClientRenewalSelectionScenario3Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduPersonalClientRenewalSelectionTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduPublicLiabilityRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduRISplitPublicLiabilityRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduRecommendClaimTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduReinsateEBPPolicyTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduRenewalSelectionTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduSADCMotorSpecifiedRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduSASRIAOOSMTCTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduSASRIARenewaSelectionScenrio4_4Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduSASRIARenewaSelectionViaFindClientScenrio4_4Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduSASRIARenewalSelection4_8Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduSASRIARenewalViaFindClient4_8Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduSASRIAScenario1NewEPLPolicyTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduSASRIAScenario1_2MTATest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduSASRIAScenario2_2Test;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduSalvageRecoveryAllPartiesTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduSalvageRecoveryRISplitTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduSalvageRecoveryTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduSecondUserLoginTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduStatedBenefitsRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduSyncCancellationTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduTheftRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduThirdPartyRecovery1;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduThirdPartyRecoveryAllParties_Old;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduThirdUserLoginTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduVehicleOlderThan20Years;
import KeywordDrivenTestFramework.Testing.TestClasses.EkunduWaterandPleasureCraftRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EtanaHeavyVehicleTest;
import KeywordDrivenTestFramework.Testing.TestClasses.EtanaSpecialTypesTest;
import KeywordDrivenTestFramework.Testing.TestClasses.MotorTradersExternalRiskTest;
import KeywordDrivenTestFramework.Testing.TestClasses.SASRIASCenario4MTAMotor2Test;
import KeywordDrivenTestFramework.Testing.TestClasses.SASRIASCenario4MTAMotorTest;
import KeywordDrivenTestFramework.Testing.TestClasses.SASRIASCenario4_5MTAMotor3Test;
import KeywordDrivenTestFramework.Testing.TestClasses.SASRIASCenario4_7MTAMotor3Test;
import KeywordDrivenTestFramework.Testing.TestClasses.SASRIAScenario4Test;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import KeywordDrivenTestFramework.Utilities.ExcelReaderUtility;
import KeywordDrivenTestFramework.Utilities.SeleniumDriverUtility;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author fnell
 */
public class TestMarshall extends BaseClass {

    // Handles calling test methods based on test parameters , instantiates Selenium Driver object   
    ExcelReaderUtility excelInputReader;
    TestReportEmailerUtility reportEmailer;
    PrintStream errorOutputStream;
    PrintStream infoOutputStream;
    //ResolveEnvironmentName enviro = new ResolveEnvironmentName();

    public TestMarshall() {
        inputFilePath = ApplicationConfig.InputFileName();
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
        browserType = ApplicationConfig.SelectedBrowser();
        //selectedTestEnvironment = enviroEnvironmentName;
        reportGenerator = new ReportGenerator(inputFilePath, ApplicationConfig.ReportFileDirectory());
        SeleniumDriverInstance = new SeleniumDriverUtility(browserType);

    }

    public TestMarshall(String inputFilePathOverride) {
        inputFilePath = inputFilePathOverride;
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
        browserType = ApplicationConfig.SelectedBrowser();
        reportGenerator = new ReportGenerator(inputFilePath, ApplicationConfig.ReportFileDirectory());
        SeleniumDriverInstance = new SeleniumDriverUtility(browserType);

    }

    public void runKeywordDrivenTests() {
        testDataList = loadTestData(inputFilePath);

        resolveScenarioName();

        if (CurrentEnvironment == null) {
            CurrentEnvironment = new ResolveEnvironmentName().EnvironmentName;
        }

        this.generateReportDirectory();
        this.redirectOutputStreams();

        if (testDataList.size() < 1) {
            System.err.println("Test data object is empty - spreadsheet not found or is empty");
        } else {

            // Each case represents a test keyword found in the excel spreadsheet
            for (TestEntity testData : testDataList) {
                testCaseId = testData.TestCaseId;
                // Make sure browser is not null - could have thrown an exception and terminated
                CheckBrowserExists();
                // Skip test methods and test case id's starting with ';'
                if (!testData.TestMethod.startsWith(";") && !testData.TestCaseId.startsWith(";")) {
                    System.out.println("Executing test - " + testData.TestMethod);
                    switch (testData.TestMethod) {
                        // A login test starts with a fresh Driver instance
                        case "Navigate to Ekundu Home Page": {
                            ensureNewBrowserInstance();
                            EkunduNavigateToStartPageTest navigateToEkunduStartPage = new EkunduNavigateToStartPageTest(testData);
                            reportGenerator.addResult(navigateToEkunduStartPage.executeTest());
                            break;
                        }
                        case "Create Corporate Client": {
                            EKunduCreateCorporateClientTest createCorpClient = new EKunduCreateCorporateClientTest(testData);
                            reportGenerator.addResult(createCorpClient.executeTest());

                            break;
                        }

                        case "Create Corporate Client New": {
                            EKunduCreateCorporateClientNewTest createCorpClient = new EKunduCreateCorporateClientNewTest(testData);
                            reportGenerator.addResult(createCorpClient.executeTest());

                            break;
                        }

                        case "EHAT - Create Corporate Client": {
                            EKunduEHATCreateCorporateClientTest createCorpClient = new EKunduEHATCreateCorporateClientTest(testData);
                            reportGenerator.addResult(createCorpClient.executeTest());

                            break;
                        }

                        case "EHAT Create Corporate Client New": {
                            EKunduEHATCreateCorporateClientNewTest EHATCCC = new EKunduEHATCreateCorporateClientNewTest(testData);
                            reportGenerator.addResult(EHATCCC.executeTest());

                            break;
                        }

                        case "Create Corporate Claim": {
                            //System.out.println("Executing Create Claim test");
                            EKunduCreateCorporateClientClaimTest createNewClaim = new EKunduCreateCorporateClientClaimTest(testData);
                            reportGenerator.addResult(createNewClaim.executeTest());
                            break;
                        }
                        case "Create Corporate Claim RI Split": {
                            //System.out.println("Executing Create Claim test");
                            EKunduCreateCorporateClientClaimRISplitTest createNewClaim = new EKunduCreateCorporateClientClaimRISplitTest(testData);
                            reportGenerator.addResult(createNewClaim.executeTest());
                            break;
                        }
                        case "EBP - Pay Claim RI Split": {
                            //System.out.println("Executing Create Claim test");
                            EkunduPayClaimRISplitTest createNewClaim = new EkunduPayClaimRISplitTest(testData);
                            reportGenerator.addResult(createNewClaim.executeTest());
                            break;
                        }
                        case "Create Personal Client": {
                            EkunduCreatePersonalClientTest CreatePersonalClientTest = new EkunduCreatePersonalClientTest(testData);
                            reportGenerator.addResult(CreatePersonalClientTest.executeTest());
                            break;
                        }

                        case "Create Personal Client(Nam)": {

                            EkunduCreatePersonalClientNamibiaTest CreatePersonalClientTest = new EkunduCreatePersonalClientNamibiaTest(testData);
                            reportGenerator.addResult(CreatePersonalClientTest.executeTest());
                            break;
                        }
                        case "Initiate Personal Client Policy": {
                            EkunduInitiatePersonalClientPolicyTest CreateNewPolicyForNewClientTest = new EkunduInitiatePersonalClientPolicyTest(testData);
                            reportGenerator.addResult(CreateNewPolicyForNewClientTest.executeTest());
                            break;
                        }

                        case "Create Personal Claim": {
                            EkunduCreatePersonalClientClaimTest CreatePersonalClientClaimTest = new EkunduCreatePersonalClientClaimTest(testData);
                            reportGenerator.addResult(CreatePersonalClientClaimTest.executeTest());
                            break;
                        }

                        case "EBP - Initiate Policy": {
                            EkunduInitiatePolicyTest InitiatePolicy = new EkunduInitiatePolicyTest(testData);
                            reportGenerator.addResult(InitiatePolicy.executeTest());
                            break;

                        }

                        case "EBP - Grouped Fire Risk": {
                            EkunduGroupedFireRiskTypeTest GroupedFire = new EkunduGroupedFireRiskTypeTest(testData);
                            reportGenerator.addResult(GroupedFire.executeTest());
                            break;
                        }

                        case "EBP - Finalise Policy": {
                            EkunduFinalisePolicyTest FinalisePolicy = new EkunduFinalisePolicyTest(testData);
                            reportGenerator.addResult(FinalisePolicy.executeTest());
                            break;
                        }

                        case "EBP - Motor Specified Risk": {
                            EkunduMotorSpecifiedRiskTest MotorSpecified = new EkunduMotorSpecifiedRiskTest(testData);
                            reportGenerator.addResult(MotorSpecified.executeTest());
                            break;
                        }

                        case "EBP - Glass Risk": {
                            EkunduGlassRiskTest GlassRisk = new EkunduGlassRiskTest(testData);
                            reportGenerator.addResult(GlassRisk.executeTest());
                            break;
                        }

                        case "EBP - Business All Risk": {
                            EkunduBusinessAllRiskTest BusinessAllRiskRisk = new EkunduBusinessAllRiskTest(testData);
                            reportGenerator.addResult(BusinessAllRiskRisk.executeTest());
                            break;
                        }

                        case "EBP - Goods In Transit Risk": {
                            EkunduGoodsInTransitRiskTest GoodsInTransit = new EkunduGoodsInTransitRiskTest(testData);
                            reportGenerator.addResult(GoodsInTransit.executeTest());
                            break;
                        }

                        case "EBP - Electronic Equipment Risk": {
                            EkunduElectronicEquipmentRiskTest ElectronicEquipment = new EkunduElectronicEquipmentRiskTest(testData);
                            reportGenerator.addResult(ElectronicEquipment.executeTest());
                            break;
                        }

                        case "EBP - Employers Liability Risk": {
                            EkunduEmployersLiabilityRiskTest EmployersLiability = new EkunduEmployersLiabilityRiskTest(testData);
                            reportGenerator.addResult(EmployersLiability.executeTest());
                            break;
                        }

                        case "EBP - Public Liability Risk": {
                            EkunduPublicLiabilityRiskTest PublicLiability = new EkunduPublicLiabilityRiskTest(testData);
                            reportGenerator.addResult(PublicLiability.executeTest());
                            break;
                        }
                        case "EBP - RI Split Public Liability Risk": {
                            EkunduRISplitPublicLiabilityRiskTest PublicLiabilityRISplit = new EkunduRISplitPublicLiabilityRiskTest(testData);
                            reportGenerator.addResult(PublicLiabilityRISplit.executeTest());
                            break;
                        }

                        case "EBP - Motor Fleet Risk": {
                            EkunduMotorFleetRiskTest MotorFleet = new EkunduMotorFleetRiskTest(testData);
                            reportGenerator.addResult(MotorFleet.executeTest());
                            break;
                        }

                        case "EBP - Theft Risk": {
                            EkunduTheftRiskTest Theft = new EkunduTheftRiskTest(testData);
                            reportGenerator.addResult(Theft.executeTest());
                            break;
                        }

                        case "EBP - Fidelity Risk": {
                            EkunduFidelityRiskTest Fidelity = new EkunduFidelityRiskTest(testData);
                            reportGenerator.addResult(Fidelity.executeTest());
                            break;
                        }

                        case "EBP - Stated Benefits Risk": {
                            EkunduStatedBenefitsRiskTest StatedBenefits = new EkunduStatedBenefitsRiskTest(testData);
                            reportGenerator.addResult(StatedBenefits.executeTest());
                            break;
                        }

                        case "EBP - Personal Accident Risk": {
                            EkunduPersonalAccidentRiskTest PersonalAccident = new EkunduPersonalAccidentRiskTest(testData);
                            reportGenerator.addResult(PersonalAccident.executeTest());
                            break;
                        }

                        case "EBP - Money Risk": {
                            EkunduMoneyRiskTest Money = new EkunduMoneyRiskTest(testData);
                            reportGenerator.addResult(Money.executeTest());
                            break;
                        }

                        case "EBP - Combined Liability Risk": {
                            EkunduCombinedLiabilityRiskTest CombinedLiability = new EkunduCombinedLiabilityRiskTest(testData);
                            reportGenerator.addResult(CombinedLiability.executeTest());
                            break;
                        }

                        case "EBP - Enroute Risk": {
                            EkunduEnrouteRiskTest Enroute = new EkunduEnrouteRiskTest(testData);
                            reportGenerator.addResult(Enroute.executeTest());
                            break;
                        }

                        case "EBP - Contingent Liability Risk": {
                            EkunduContingentLiabilityRiskTest ContingentLiability = new EkunduContingentLiabilityRiskTest(testData);
                            reportGenerator.addResult(ContingentLiability.executeTest());
                            break;
                        }

                        case "EDPL - Personal Client - Personal  Accident Risk": {
                            EkunduPersonalClientPersonalAccidentRiskTest PersonalAccident = new EkunduPersonalClientPersonalAccidentRiskTest(testData);
                            reportGenerator.addResult(PersonalAccident.executeTest());
                            break;
                        }

                        case "EDPL - Domestic and Property Risk": {
                            EkunduDomesticAndPropertyRiskTest DomesticAndProperty = new EkunduDomesticAndPropertyRiskTest(testData);
                            reportGenerator.addResult(DomesticAndProperty.executeTest());
                            break;
                        }

                        case "EDPL - Building and Contents Risk": {
                            EkunduDomesticAndPropertyRiskTest DomesticAndProperty = new EkunduDomesticAndPropertyRiskTest(testData);
                            reportGenerator.addResult(DomesticAndProperty.executeTest());
                            break;
                        }

                        case "EDPL - Personal Client - Enroute Risk": {
                            EkunduPersonalClientEnrouteRiskTest Enroute = new EkunduPersonalClientEnrouteRiskTest(testData);
                            reportGenerator.addResult(Enroute.executeTest());
                            break;
                        }

                        case "EDPL - Water and Pleasure Craft Risk": {
                            EkunduWaterandPleasureCraftRiskTest WaterAndPleasureCraft = new EkunduWaterandPleasureCraftRiskTest(testData);
                            reportGenerator.addResult(WaterAndPleasureCraft.executeTest());
                            break;
                        }

                        case "EDPL - Motor Risk": {
                            EkunduMotorRiskTest Motor = new EkunduMotorRiskTest(testData);
                            reportGenerator.addResult(Motor.executeTest());
                            break;
                        }
                        case "Finalise Personal Client Policy": {
                            EkunduFinalisePersonalClientPolicy finalise = new EkunduFinalisePersonalClientPolicy(testData);
                            reportGenerator.addResult(finalise.executeTest());
                            break;
                        }

                        case "EBP - Maintain Corporate Client Claim 1": {
                            EkunduMaintainCorporateClientClaim1Test maintain = new EkunduMaintainCorporateClientClaim1Test(testData);
                            reportGenerator.addResult(maintain.executeTest());
                            break;
                        }
                        case "EBP - Maintain Corporate Client Claim  RI Split": {
                            EkunduMaintainCorporateClientClaimRISplitTest maintain = new EkunduMaintainCorporateClientClaimRISplitTest(testData);
                            reportGenerator.addResult(maintain.executeTest());
                            break;
                        }

                        case "EBP - Maintain Corporate Client Claim 2": {
                            EkunduMaintainCorporateClientClaim2Test maintain2 = new EkunduMaintainCorporateClientClaim2Test(testData);
                            reportGenerator.addResult(maintain2.executeTest());
                            break;
                        }

                        case "EBP - Pay Claim 1": {
                            EkunduPayClaim1Test payClaim = new EkunduPayClaim1Test(testData);
                            reportGenerator.addResult(payClaim.executeTest());
                            break;
                        }

                        case "Second User Login": {
                            EkunduSecondUserLoginTest login2 = new EkunduSecondUserLoginTest(testData);
                            reportGenerator.addResult(login2.executeTest());
                            break;
                        }

                        case "Third User Login": {
                            EkunduThirdUserLoginTest login3 = new EkunduThirdUserLoginTest(testData);
                            reportGenerator.addResult(login3.executeTest());
                            break;
                        }

                        case "EBP - Pay Claim 2": {
                            EkunduPayClaim2Test payClaim2 = new EkunduPayClaim2Test(testData);
                            reportGenerator.addResult(payClaim2.executeTest());
                            break;
                        }

                        case "Recommend Claim 1": {
                            EkunduRecommendClaimTest recommendClaim1 = new EkunduRecommendClaimTest(testData);
                            reportGenerator.addResult(recommendClaim1.executeTest());
                            break;
                        }

                        case "Authorise Claim 1": {
                            EkunduAuthoriseClaim1Test authoriseClaim1 = new EkunduAuthoriseClaim1Test(testData);
                            reportGenerator.addResult(authoriseClaim1.executeTest());
                            break;
                        }

                        case "EBP - Grouped Fire Risk 2": {
                            EkunduEBPGroupedFireTest groupedFire = new EkunduEBPGroupedFireTest(testData);
                            reportGenerator.addResult(groupedFire.executeTest());
                            break;
                        }

                        case "EBP - Money Risk 2": {
                            EkunduEBPMoneyRiskTest Money = new EkunduEBPMoneyRiskTest(testData);
                            reportGenerator.addResult(Money.executeTest());
                            break;
                        }

                        case "EBP - Goods In Transit Risk 2": {
                            EkunduEBPGoodsInTransit goodsInTransit = new EkunduEBPGoodsInTransit(testData);
                            reportGenerator.addResult(goodsInTransit.executeTest());
                            break;
                        }

                        case "EBP - Mid Term Adjustment": {
                            EkunduMidTermAdjustmentTest MTA = new EkunduMidTermAdjustmentTest(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;
                        }

                        case "EBP - Business All Risk 2": {
                            EkunduEBPBusinessAllRiskTest businessAllRisk = new EkunduEBPBusinessAllRiskTest(testData);
                            reportGenerator.addResult(businessAllRisk.executeTest());
                            break;
                        }

                        case "EBP - Grouped Fire Risk 3": {
                            EkunduEBPGroupedFire3Test groupedFire = new EkunduEBPGroupedFire3Test(testData);
                            reportGenerator.addResult(groupedFire.executeTest());
                            break;
                        }

                        case "EBP - Electronic Equipment Risk 2": {
                            EkunduEBPElectronicEquipement2RiskTest electronic = new EkunduEBPElectronicEquipement2RiskTest(testData);
                            reportGenerator.addResult(electronic.executeTest());
                            break;
                        }

                        case "EBP - Mid Term Adjustment 2": {
                            EkunduMidTermAdjustment2Test MTA2 = new EkunduMidTermAdjustment2Test(testData);
                            reportGenerator.addResult(MTA2.executeTest());
                            break;
                        }

                        case "EBP - Edit Business All Risk": {
                            EkunduEditBusinessAllRiskTest editBAR = new EkunduEditBusinessAllRiskTest(testData);
                            reportGenerator.addResult(editBAR.executeTest());
                            break;
                        }

                        case "EBP - Edit Grouped Fire Risk": {
                            EBPEditGroupedFireRiskTest editGroupedFire = new EBPEditGroupedFireRiskTest(testData);
                            reportGenerator.addResult(editGroupedFire.executeTest());
                            break;
                        }

                        case "EBP - Edit Electronic Equipment Risk 2": {
                            EBPEditElectronicEquipment editEE = new EBPEditElectronicEquipment(testData);
                            reportGenerator.addResult(editEE.executeTest());
                            break;
                        }

                        case "EBP - Mid Term Adjustment 3": {
                            EkunduMTA3GroupedFireTest MTA3 = new EkunduMTA3GroupedFireTest(testData);
                            reportGenerator.addResult(MTA3.executeTest());
                            break;
                        }

                        case "EBP - Grouped Fire Risk - MTA": {
                            EkunduMidTermAdjustment3Test GFMTA = new EkunduMidTermAdjustment3Test(testData);
                            reportGenerator.addResult(GFMTA.executeTest());
                            break;
                        }

                        case "EBP - Finalise Client Policy": {
                            EkunduFinaliseCorporateClientPolicyTest finalise = new EkunduFinaliseCorporateClientPolicyTest(testData);
                            reportGenerator.addResult(finalise.executeTest());
                            break;
                        }

                        case "EBP - Mid Term Adjustment 4": {
                            EkunduMidtermAdjustment4Test MTA4 = new EkunduMidtermAdjustment4Test(testData);
                            reportGenerator.addResult(MTA4.executeTest());
                            break;
                        }

                        case "EBP - MTA Sync In Cancellation": {
                            EkunduSyncCancellationTest sync = new EkunduSyncCancellationTest(testData);
                            reportGenerator.addResult(sync.executeTest());
                            break;
                        }

                        case "EBP - Reinstate Policy": {
                            EkunduReinsateEBPPolicyTest reinstate = new EkunduReinsateEBPPolicyTest(testData);
                            reportGenerator.addResult(reinstate.executeTest());
                            break;
                        }

                        case "EBP - MTA Extend Expiry Date": {
                            EkunduMTAExtendExpiryDateTest extendDate = new EkunduMTAExtendExpiryDateTest(testData);
                            reportGenerator.addResult(extendDate.executeTest());
                            break;
                        }

                        case "EPL - Domestic and Property Risk": {
                            EkunduSASRIAScenario1NewEPLPolicyTest EPLDomestic = new EkunduSASRIAScenario1NewEPLPolicyTest(testData);
                            reportGenerator.addResult(EPLDomestic.executeTest());
                            break;
                        }

                        case "EEPL - Domestic and Property Risk": {
                            EkunduSASRIAScenario1NewEPLPolicyTest EEPLDomestic = new EkunduSASRIAScenario1NewEPLPolicyTest(testData);
                            reportGenerator.addResult(EEPLDomestic.executeTest());
                            break;
                        }

                        case "EPL - SASRIA Scenario1_2 MTA": {
                            EkunduSASRIAScenario1_2MTATest EPLMTA = new EkunduSASRIAScenario1_2MTATest(testData);
                            reportGenerator.addResult(EPLMTA.executeTest());
                            break;
                        }

                        case "EBP - MTA Out of Sync OOS": {
                            EkunduMTAOOSTest MTAOOS = new EkunduMTAOOSTest(testData);
                            reportGenerator.addResult(MTAOOS.executeTest());
                            break;
                        }

                        case "EBP - MTA Out of Sync Changes on Version": {
                            EkunduOutOfSyncMTAApplyChangesOnOOSVersion MTAOOSChanges = new EkunduOutOfSyncMTAApplyChangesOnOOSVersion(testData);
                            reportGenerator.addResult(MTAOOSChanges.executeTest());
                            break;
                        }

                        case "EBP - Renewal Selection Via Find Client": {
                            EkunduEBPRenewalSelectionViaFindClientTest renewal = new EkunduEBPRenewalSelectionViaFindClientTest(testData);
                            reportGenerator.addResult(renewal.executeTest());
                            break;
                        }

                        case "EBP - In Sync MTA": {
                            EkunduInSyncMTATest InSync = new EkunduInSyncMTATest(testData);
                            reportGenerator.addResult(InSync.executeTest());
                            break;
                        }

                        case "EBP - MTA Step 14": {
                            EkunduDeleteGoodsinTransitRiskTest deleteGIT = new EkunduDeleteGoodsinTransitRiskTest(testData);
                            reportGenerator.addResult(deleteGIT.executeTest());
                            break;
                        }

                        case "EBP - Directors and Offices": {
                            EkunduDirectorsAndOfficesRiskTest EBPDO = new EkunduDirectorsAndOfficesRiskTest(testData);
                            reportGenerator.addResult(EBPDO.executeTest());
                            break;
                        }

                        case "EBP - Motor Traders External Risk": {
                            MotorTradersExternalRiskTest motorexternal = new MotorTradersExternalRiskTest(testData);
                            reportGenerator.addResult(motorexternal.executeTest());
                            break;
                        }

                        case "EBP - Motor Traders Internal Risk": {
                            EkunduMotorTradersInternalRiskTest motorInternal = new EkunduMotorTradersInternalRiskTest(testData);
                            reportGenerator.addResult(motorInternal.executeTest());
                            break;
                        }

                        case "EEPL - MTA Water and Pleasure Craft Risk": {
                            EkunduSASRIAScenario1_2MTATest MTAPleasurecraft = new EkunduSASRIAScenario1_2MTATest(testData);
                            reportGenerator.addResult(MTAPleasurecraft.executeTest());
                            break;
                        }

                        case "EEPL - MTA Edit Water and Pleasure Craft Risk": {
                            EkunduMTAWaterAndPleasureCraftSCenario2_1Test MTAEditPleasurecraft = new EkunduMTAWaterAndPleasureCraftSCenario2_1Test(testData);
                            reportGenerator.addResult(MTAEditPleasurecraft.executeTest());
                            break;
                        }

                        case "EEPL - Renewal Selection": {
                            EkunduPersonalClientRenewalSelectionTest renewal = new EkunduPersonalClientRenewalSelectionTest(testData);
                            reportGenerator.addResult(renewal.executeTest());
                            break;
                        }

                        case "EEPL -MTA OOS Delete Water And Pleasure Craft": {
                            EkunduEPLOOSTest EPLOOS = new EkunduEPLOOSTest(testData);
                            reportGenerator.addResult(EPLOOS.executeTest());
                            break;
                        }

                        case "EEPL -MTA Domestic and Property": {
                            EkunduSASRIAScenario2_2Test EPLEditDAPRisk = new EkunduSASRIAScenario2_2Test(testData);
                            reportGenerator.addResult(EPLEditDAPRisk.executeTest());
                            break;
                        }

                        case "EEPL -MTA Buildings and Contents": {
                            EkunduSASRIAScenario2_2Test EPLEditDAPRisk = new EkunduSASRIAScenario2_2Test(testData);
                            reportGenerator.addResult(EPLEditDAPRisk.executeTest());
                            break;
                        }

                        case "EEPL - Renewal Selection Scenario 2": {
                            EkunduPersonalClientRenewalSelectionScenario2Test renewalselection2 = new EkunduPersonalClientRenewalSelectionScenario2Test(testData);
                            reportGenerator.addResult(renewalselection2.executeTest());
                            break;
                        }

                        case "EEPL - Renewal Selection Scenario 3": {
                            EkunduPersonalClientRenewalSelectionScenario3Test renewalselection3 = new EkunduPersonalClientRenewalSelectionScenario3Test(testData);
                            reportGenerator.addResult(renewalselection3.executeTest());
                            break;
                        }

                        case "EEPL -MTA Domestic and Property 3": {
                            EkunduOOSMTAScenario3 OOSMTA3 = new EkunduOOSMTAScenario3(testData);
                            reportGenerator.addResult(OOSMTA3.executeTest());
                            break;
                        }

                        case "EEPL - Domestic and Property Scenario 4.1.1": {
                            SASRIAScenario4Test DAPScenario4 = new SASRIAScenario4Test(testData);
                            reportGenerator.addResult(DAPScenario4.executeTest());
                            break;
                        }

                        case "EEPL - SASRIA Motor Risk 4.1.2": {
                            SASRIASCenario4MTAMotorTest MotorScenario4 = new SASRIASCenario4MTAMotorTest(testData);
                            reportGenerator.addResult(MotorScenario4.executeTest());
                            break;
                        }

                        case "EEPL - SASRIA Scenario 4.1.3 Water And Pleasure Craft": {
                            EkunduMTAWaterAndPleasureCraftScenario4_1_3Test WAP = new EkunduMTAWaterAndPleasureCraftScenario4_1_3Test(testData);
                            reportGenerator.addResult(WAP.executeTest());
                            break;
                        }

                        case "EEPL - MTA Edit Water and Pleasure Craft Risk 4.2.1": {
                            EkunduMTAWaterAndPleasureCraftSCenario4_2_1Test MTAPleasurecraft = new EkunduMTAWaterAndPleasureCraftSCenario4_2_1Test(testData);
                            reportGenerator.addResult(MTAPleasurecraft.executeTest());
                            break;
                        }

                        case "EPL - MTA Domestic and Property 4.2.2": {
                            EkunduMTAAddDometicAndPropertyTest EPL_MTA_Domestic = new EkunduMTAAddDometicAndPropertyTest(testData);
                            reportGenerator.addResult(EPL_MTA_Domestic.executeTest());
                            break;
                        }

                        case "EEPL - MTA Motor risk 4.2.3": {
                            SASRIASCenario4MTAMotor2Test EPL_MTA_Motor2 = new SASRIASCenario4MTAMotor2Test(testData);
                            reportGenerator.addResult(EPL_MTA_Motor2.executeTest());
                            break;
                        }

                        case "EPL - MTA Delete Domestic and Property Risk 4.3": {
                            EkunduMTADeleteDomesticAndProperty4_3Test Del_Domestic_Prop = new EkunduMTADeleteDomesticAndProperty4_3Test(testData);
                            reportGenerator.addResult(Del_Domestic_Prop.executeTest());
                            break;
                        }

                        case "EEPL - Renewal Selection 4.4": {
                            EkunduSASRIARenewaSelectionScenrio4_4Test renewal4_4 = new EkunduSASRIARenewaSelectionScenrio4_4Test(testData);
                            reportGenerator.addResult(renewal4_4.executeTest());
                            break;
                        }

                        case "EEPL - Renewal Selection Via Find Client 4.4": {
                            EkunduSASRIARenewaSelectionViaFindClientScenrio4_4Test renewal4_4 = new EkunduSASRIARenewaSelectionViaFindClientScenrio4_4Test(testData);
                            reportGenerator.addResult(renewal4_4.executeTest());
                            break;
                        }

                        case "EEPL - MTA Motor Risk 4.5": {
                            SASRIASCenario4_5MTAMotor3Test EPL_MTA_Motor3 = new SASRIASCenario4_5MTAMotor3Test(testData);
                            reportGenerator.addResult(EPL_MTA_Motor3.executeTest());
                            break;
                        }

                        case "EEPL -MTA Delete Motor Risk3 4.6": {
                            EkunduMTADeleteMotorRisk3Test DeleteMotor3 = new EkunduMTADeleteMotorRisk3Test(testData);
                            reportGenerator.addResult(DeleteMotor3.executeTest());
                            break;
                        }

                        case "EEPL - MTA Domestic and Property Scenario 4.7.1": {
                            EkunduMTADomesticAndPropertyScenario4_7Test DAPScenario4 = new EkunduMTADomesticAndPropertyScenario4_7Test(testData);
                            reportGenerator.addResult(DAPScenario4.executeTest());
                            break;
                        }

                        case "EEPL - Domestic and Property Scenario 4": {
                            SASRIAScenario4Test DAPScenario4 = new SASRIAScenario4Test(testData);
                            reportGenerator.addResult(DAPScenario4.executeTest());
                            break;
                        }

                        case "EEPL - MTA Motor Risk": {
                            SASRIASCenario4MTAMotorTest MotorScenario4 = new SASRIASCenario4MTAMotorTest(testData);
                            reportGenerator.addResult(MotorScenario4.executeTest());
                            break;
                        }

                        case "EBP - MTA Edit Grouped Fire Risk Step 13": {
                            EkunduMTAStep13Test step13 = new EkunduMTAStep13Test(testData);
                            reportGenerator.addResult(step13.executeTest());
                            break;
                        }

                        case "EBP - MTA Delete Goods In Transit Step 14": {
                            EkunduMTADeleteGoodsInTransitTest step14 = new EkunduMTADeleteGoodsInTransitTest(testData);
                            reportGenerator.addResult(step14.executeTest());
                            break;
                        }

                        case "EEPL - Motor Risk 4.7.2": {
                            SASRIASCenario4_7MTAMotor3Test step14 = new SASRIASCenario4_7MTAMotor3Test(testData);
                            reportGenerator.addResult(step14.executeTest());
                            break;
                        }

                        case "EEPL - Renewal Selection 4.8": {
                            EkunduSASRIARenewalSelection4_8Test step14 = new EkunduSASRIARenewalSelection4_8Test(testData);
                            reportGenerator.addResult(step14.executeTest());
                            break;
                        }

                        case "EEPL - Renewal Selection Via Client Details 4.8": {
                            EkunduSASRIARenewalViaFindClient4_8Test step14 = new EkunduSASRIARenewalViaFindClient4_8Test(testData);
                            reportGenerator.addResult(step14.executeTest());
                            break;
                        }

                        case "EEPL - Out of Sync MTC 4.9": {
                            EkunduSASRIAOOSMTCTest step4_9 = new EkunduSASRIAOOSMTCTest(testData);
                            reportGenerator.addResult(step4_9.executeTest());
                            break;
                        }

                        case "EEPL - Out of Sync MTR 4.10": {
                            EkunduOOSMTRTest step4_10 = new EkunduOOSMTRTest(testData);
                            reportGenerator.addResult(step4_10.executeTest());
                            break;
                        }

                        case "EBP - Salvage Recovery": {
                            EkunduSalvageRecoveryTest salvage = new EkunduSalvageRecoveryTest(testData);
                            reportGenerator.addResult(salvage.executeTest());
                            break;
                        }
                        case "EBP - Salvage Recovery RI Split": {
                            EkunduSalvageRecoveryRISplitTest salvage = new EkunduSalvageRecoveryRISplitTest(testData);
                            reportGenerator.addResult(salvage.executeTest());
                            break;
                        }

                        case "EBP - Salvage Recovery All Parties": {
                            EkunduSalvageRecoveryAllPartiesTest salvageAll = new EkunduSalvageRecoveryAllPartiesTest(testData);
                            reportGenerator.addResult(salvageAll.executeTest());
                            break;
                        }

                        case "EBP - Third Party Recovery": {
                            EkunduThirdPartyRecovery1 TPR = new EkunduThirdPartyRecovery1(testData);
                            reportGenerator.addResult(TPR.executeTest());
                            break;
                        }

                        case "EBP - Third Party Recovery All Parties": {
                            EkunduThirdPartyRecoveryAllParties_Old TPRAP = new EkunduThirdPartyRecoveryAllParties_Old(testData);
                            reportGenerator.addResult(TPRAP.executeTest());
                            break;
                        }

                        case "Maintain Claim Salvage Recovery": {
                            EkunduMaintainClaimSalvageRecoveryTest Maintainsalvage = new EkunduMaintainClaimSalvageRecoveryTest(testData);
                            reportGenerator.addResult(Maintainsalvage.executeTest());
                            break;
                        }

                        case "EPL - Domestic And Property Risk Step 3": {
                            EkunduEPLDomesticAndPropertyTest DMP = new EkunduEPLDomesticAndPropertyTest(testData);
                            reportGenerator.addResult(DMP.executeTest());
                            break;
                        }

                        case "EPL - Personal  Accident Risk Step 4": {
                            EkunduEPLPersonalAccidentTest PA = new EkunduEPLPersonalAccidentTest(testData);
                            reportGenerator.addResult(PA.executeTest());
                            break;
                        }

                        case "EPL - Enroute Risk Step 5": {
                            EkunduPersonalClientEnrouteRiskTest Enroute = new EkunduPersonalClientEnrouteRiskTest(testData);
                            reportGenerator.addResult(Enroute.executeTest());
                            break;
                        }

                        case "EDPL - Water and Pleasure Craft Risk Step 6": {
                            EkunduEPLWaterAndPleasureCraftRisk WAP = new EkunduEPLWaterAndPleasureCraftRisk(testData);
                            reportGenerator.addResult(WAP.executeTest());
                            break;
                        }

                        case "EPL - Motor Risk Step 7": {
                            EkunduEPLMotorRiskStep7Test Motor = new EkunduEPLMotorRiskStep7Test(testData);
                            reportGenerator.addResult(Motor.executeTest());
                            break;
                        }

                        case "EPL - MTA Add Domestic and Property Risk Step 1": {
                            EkunduEPLMTAAddDomesticAndPropertyTest DAP = new EkunduEPLMTAAddDomesticAndPropertyTest(testData);
                            reportGenerator.addResult(DAP.executeTest());
                            break;
                        }

                        case "EPL - MTA Edit Domestic and Property Risk Step 2": {
                            EkunduEPLMTAEditDomesticandPropertyRiskTest DAP = new EkunduEPLMTAEditDomesticandPropertyRiskTest(testData);
                            reportGenerator.addResult(DAP.executeTest());
                            break;
                        }

                        case "EPL - Renewal Selection": {
                            EkunduEPLRenewalSelectionTest EPLRS = new EkunduEPLRenewalSelectionTest(testData);
                            reportGenerator.addResult(EPLRS.executeTest());
                            break;
                        }

                        case "EPL - MTA Add Motor Risk": {
                            EkunduEPLMTAAddMotorRiskTest MTAMotor = new EkunduEPLMTAAddMotorRiskTest(testData);
                            reportGenerator.addResult(MTAMotor.executeTest());
                            break;
                        }

                        case "EPL - Renewal Selection 6 Months (September)": {
                            EkunduEPLRenewalSelection_6Months_Test EPLRS = new EkunduEPLRenewalSelection_6Months_Test(testData);
                            reportGenerator.addResult(EPLRS.executeTest());
                            break;
                        }

                        case "EPL - Renewal Selection 6 Months (October)": {
                            EkunduEPLRenewalSelection_6Months_Test EPLRS = new EkunduEPLRenewalSelection_6Months_Test(testData);
                            reportGenerator.addResult(EPLRS.executeTest());
                            break;
                        }

                        case "EPL - Renewal Selection 6 Months (November)": {
                            EkunduEPLRenewalSelection_6Months_Test EPLRS = new EkunduEPLRenewalSelection_6Months_Test(testData);
                            reportGenerator.addResult(EPLRS.executeTest());
                            break;
                        }

                        case "EPL - Renewal Selection 6 Months (December)": {
                            EkunduEPLRenewalSelection_6Months_Test EPLRS = new EkunduEPLRenewalSelection_6Months_Test(testData);
                            reportGenerator.addResult(EPLRS.executeTest());
                            break;
                        }

                        case "EPL - Renewal Selection 6 Months (January)": {
                            EkunduEPLRenewalSelection_6Months_Test EPLRS = new EkunduEPLRenewalSelection_6Months_Test(testData);
                            reportGenerator.addResult(EPLRS.executeTest());
                            break;
                        }

                        case "EPL - Renewal Selection 6 Months (February)": {
                            EkunduEPLRenewalSelection_6Months_February_Test EPLRS = new EkunduEPLRenewalSelection_6Months_February_Test(testData);
                            reportGenerator.addResult(EPLRS.executeTest());
                            break;
                        }

                        case "EPL - MTC": {
                            EkunduEPLMTCTest EPLMTC = new EkunduEPLMTCTest(testData);
                            reportGenerator.addResult(EPLMTC.executeTest());
                            break;
                        }

                        case "EPL - MTR": {
                            EkunduEPLMTRTest EPLMTR = new EkunduEPLMTRTest(testData);
                            reportGenerator.addResult(EPLMTR.executeTest());
                            break;
                        }

                        case "EBP - Renewal Selection": {
                            EkunduRenewalSelectionTest renewal = new EkunduRenewalSelectionTest(testData);
                            reportGenerator.addResult(renewal.executeTest());
                            break;
                        }

                        case "EHAT - Grouped Fire Risk": {
                            EHATGrouprFireRiskTest EHATGroupedFire = new EHATGrouprFireRiskTest(testData);
                            reportGenerator.addResult(EHATGroupedFire.executeTest());
                            break;
                        }

                        case "Create Personal Client Bank Details": {
                            EkunduCreatePersonalClientForBankingDetailsTest CPBD = new EkunduCreatePersonalClientForBankingDetailsTest(testData);
                            reportGenerator.addResult(CPBD.executeTest());
                            break;
                        }

                        case "Bank Details Add Edit Delete": {

                            EkunduBankDetailsAddEditDeletePersonalTest BD = new EkunduBankDetailsAddEditDeletePersonalTest(testData);
                            reportGenerator.addResult(BD.executeTest());
                            break;
                        }

                        case "Create Corporate Client Bank Details": {
                            EkunduCreateCorporateClientForBankDetailsTest CCBD = new EkunduCreateCorporateClientForBankDetailsTest(testData);
                            reportGenerator.addResult(CCBD.executeTest());
                            break;
                        }

                        case "Bank Details Add Edit Delete Corp": {

                            EkunduBankDetailsAddEditDeleteCorporateTest BDC = new EkunduBankDetailsAddEditDeleteCorporateTest(testData);
                            reportGenerator.addResult(BDC.executeTest());
                            break;
                        }

                        case "Broad Form Liability Risk": {

                            EkunduBroadFormLiabilityRiskTest BFL = new EkunduBroadFormLiabilityRiskTest(testData);
                            reportGenerator.addResult(BFL.executeTest());
                            break;
                        }

                        case "EHAT Business All Risk": {

                            EkunduEHATBusinessAllRiskTest BAR = new EkunduEHATBusinessAllRiskTest(testData);
                            reportGenerator.addResult(BAR.executeTest());
                            break;
                        }

                        case "EHAT Crisis 24 Risk": {

                            EkunduCrisis24RiskTest C24 = new EkunduCrisis24RiskTest(testData);
                            reportGenerator.addResult(C24.executeTest());
                            break;
                        }

                        case "EHAT Domestic and Property Liability Risk": {

                            EkunduDomesticAndPropertyLiabilityRiskTest dap = new EkunduDomesticAndPropertyLiabilityRiskTest(testData);
                            reportGenerator.addResult(dap.executeTest());
                            break;
                        }

                        case "EHAT Electronic Equipment Risk": {

                            EkunduEHATElectronicEquipmentTest EE = new EkunduEHATElectronicEquipmentTest(testData);
                            reportGenerator.addResult(EE.executeTest());
                            break;
                        }

                        case "EBP - Business All Risk Monthly": {

                            EkunduEBPBusinessAllRiskMonthlyTest BAR = new EkunduEBPBusinessAllRiskMonthlyTest(testData);
                            reportGenerator.addResult(BAR.executeTest());
                            break;
                        }

                        case "EBP - MTA Edit Business All Risk": {

                            EkunduEBPMonthlyMTAEditBusinessAllRiskTest BAR = new EkunduEBPMonthlyMTAEditBusinessAllRiskTest(testData);
                            reportGenerator.addResult(BAR.executeTest());
                            break;
                        }

                        case "EBP Monthly - Renewal Selection": {

                            EkunduEBPMonthlyRenewalSelectiontTest BAR = new EkunduEBPMonthlyRenewalSelectiontTest(testData);
                            reportGenerator.addResult(BAR.executeTest());
                            break;
                        }

                        case "Fare Paying Passenger Liability Risk": {

                            EkunduFarePayingPassengerLiabilityTest FPPL = new EkunduFarePayingPassengerLiabilityTest(testData);
                            reportGenerator.addResult(FPPL.executeTest());
                            break;
                        }

                        case "EBP Monthly - Renewal Selection 2": {

                            EkunduEBPMonthlyRenewalSelectiontTest2 RS = new EkunduEBPMonthlyRenewalSelectiontTest2(testData);
                            reportGenerator.addResult(RS.executeTest());
                            break;
                        }

                        case "EBP Monthly - MTC": {

                            EkunduEBPMonthlyMTCTest MonthlyMTC = new EkunduEBPMonthlyMTCTest(testData);
                            reportGenerator.addResult(MonthlyMTC.executeTest());
                            break;
                        }

                        case "EBP Monthly - MTR": {

                            EkunduEBPMonthlyMTRTest MonthlyMTR = new EkunduEBPMonthlyMTRTest(testData);
                            reportGenerator.addResult(MonthlyMTR.executeTest());
                            break;
                        }

                        case "EBP Monthly - Renewal Selection 3": {

                            EkunduEBPMonthlyRenewalSelection3Test RS = new EkunduEBPMonthlyRenewalSelection3Test(testData);
                            reportGenerator.addResult(RS.executeTest());
                            break;
                        }

                        case "EBP Monthly - OOS MTA": {

                            EkunduEBPMonthlyOOSMTATest OOSMTA = new EkunduEBPMonthlyOOSMTATest(testData);
                            reportGenerator.addResult(OOSMTA.executeTest());
                            break;
                        }

                        case "Delete Edit Personal Client Address": {

                            EkunduAdrressDetailsAddEditDeletePersonalTest address = new EkunduAdrressDetailsAddEditDeletePersonalTest(testData);
                            reportGenerator.addResult(address.executeTest());
                            break;
                        }

                        case "Add - Edit - Delete Home Address": {

                            EkunduCorporateClientAddressDetailsTest address = new EkunduCorporateClientAddressDetailsTest(testData);
                            reportGenerator.addResult(address.executeTest());
                            break;
                        }

                        case "PL - Pleasure Craft Risk": {

                            EkunduWaterandPleasureCraftRiskTest PleasureCraft = new EkunduWaterandPleasureCraftRiskTest(testData);
                            reportGenerator.addResult(PleasureCraft.executeTest());
                            break;
                        }

                        case "PL - Domestic and Property Liability Risk": {

                            EkunduDomesticAndPropertyRiskTest DAPL = new EkunduDomesticAndPropertyRiskTest(testData);
                            reportGenerator.addResult(DAPL.executeTest());
                            break;
                        }

                        case "PL - Personal  Accident Risk": {

                            EkunduPersonalClientPersonalAccidentRiskTest PA = new EkunduPersonalClientPersonalAccidentRiskTest(testData);
                            reportGenerator.addResult(PA.executeTest());
                            break;
                        }

                        case "PL - Extended Personal Liability Risk": {

                            EkunduExtendedPersonalLiabilityRiskTest EPL = new EkunduExtendedPersonalLiabilityRiskTest(testData);
                            reportGenerator.addResult(EPL.executeTest());
                            break;
                        }

                        case "PL - Motor Specified Risk": {

                            EkunduSADCMotorSpecifiedRiskTest MSR = new EkunduSADCMotorSpecifiedRiskTest(testData);
                            reportGenerator.addResult(MSR.executeTest());
                            break;
                        }

                        case "EPL - Motor Section Details Risk": {

                            EkunduMotoSectionDetailsTest MotorSection = new EkunduMotoSectionDetailsTest(testData);
                            reportGenerator.addResult(MotorSection.executeTest());
                            break;
                        }

                        case "EPL - Motor (Polo Vivo)": {

                            EkunduEPLMTAAddMotorRiskTest MotorVivo = new EkunduEPLMTAAddMotorRiskTest(testData);
                            reportGenerator.addResult(MotorVivo.executeTest());
                            break;
                        }

                        case "EPL - Motor - Caravan": {

                            EkunduEPLMTAAddMotorRiskTest MotorCaravan = new EkunduEPLMTAAddMotorRiskTest(testData);
                            reportGenerator.addResult(MotorCaravan.executeTest());
                            break;
                        }

                        case "EPL - Motor - Trailer": {

                            EkunduEPLMTAAddMotorRiskTest MotorTrailer = new EkunduEPLMTAAddMotorRiskTest(testData);
                            reportGenerator.addResult(MotorTrailer.executeTest());
                            break;
                        }

                        case "EPL - Motor - Vehicles Older Than 20 Years": {

                            EkunduEPLMTAAddMotorRiskTest MotorTrailer = new EkunduEPLMTAAddMotorRiskTest(testData);
                            reportGenerator.addResult(MotorTrailer.executeTest());
                            break;
                        }

                        case "EBP - Motor - Vehicles Older Than 20 Years": {

                            EkunduEBPAddMotorRiskOlderThan20Test old = new EkunduEBPAddMotorRiskOlderThan20Test(testData);
                            reportGenerator.addResult(old.executeTest());
                            break;
                        }

                        case "EPL - Motor -Private LDV": {

                            EkunduEPLMTAAddMotorRiskTest MotorTrailer = new EkunduEPLMTAAddMotorRiskTest(testData);
                            reportGenerator.addResult(MotorTrailer.executeTest());
                            break;
                        }

                        case "EBP - Motor -Private LDV": {

                            EkunduEBPAddMotorPrivateLDVRiskTest MotorTrailer = new EkunduEBPAddMotorPrivateLDVRiskTest(testData);
                            reportGenerator.addResult(MotorTrailer.executeTest());
                            break;
                        }

                        case "EPL - Motor (Classic / Imports)": {

                            EkunduEPLMTAAddMotorRiskClassicImportsTest MotorClassic = new EkunduEPLMTAAddMotorRiskClassicImportsTest(testData);
                            reportGenerator.addResult(MotorClassic.executeTest());
                            break;
                        }

                        case "EBP - Motor (Classic / Imports)": {

                            EkunduEBPMTAAddMotorRiskClassicImportsTest MotorClassic = new EkunduEBPMTAAddMotorRiskClassicImportsTest(testData);
                            reportGenerator.addResult(MotorClassic.executeTest());
                            break;
                        }

                        case "EPL - Motor (Motorcycle)": {

                            EkunduEPLMTAAddMotorRiskMotorCycleTest MotorCycle = new EkunduEPLMTAAddMotorRiskMotorCycleTest(testData);
                            reportGenerator.addResult(MotorCycle.executeTest());
                            break;

                        }

                        case "EBP - Motor (Motorcycle)": {

                            EkunduEBPAddMotorRiskMotorCycleTest MotorCycle = new EkunduEBPAddMotorRiskMotorCycleTest(testData);
                            reportGenerator.addResult(MotorCycle.executeTest());
                            break;

                        }

                        case "EPL - Private Motor (Aston Martin)": {

                            EkunduEPLMTAAddMotorRiskAstonMartinTest Aston = new EkunduEPLMTAAddMotorRiskAstonMartinTest(testData);
                            reportGenerator.addResult(Aston.executeTest());
                            break;

                        }

                        case "EBP - Private Motor (Aston Martin)": {

                            EkunduEPLMTAAddMotorRiskAstonMartinTest Aston = new EkunduEPLMTAAddMotorRiskAstonMartinTest(testData);
                            reportGenerator.addResult(Aston.executeTest());
                            break;

                        }

                        case "EBP - Motor Specified Risk (IVECO)": {

                            EkunduEBPMotorSpecifiedIVECOTest IVECO = new EkunduEBPMotorSpecifiedIVECOTest(testData);
                            reportGenerator.addResult(IVECO.executeTest());
                            break;

                        }

                        case "EBP - Motor Specified Risk (Special Type)": {

                            EkunduEBPMotorSpecifiedSpecialTypeTest ST = new EkunduEBPMotorSpecifiedSpecialTypeTest(testData);
                            reportGenerator.addResult(ST.executeTest());
                            break;

                        }

                        case "EPL - MTA - Edit Rolls Royce": {

                            EKunduMTAMotorChangesTest MTA = new EKunduMTAMotorChangesTest(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;
                        }

                        case "EBP - MTA - Edit Rolls Royce": {

                            EKunduEBPMTAMotorChangesEditRollsRoyceTest MTA = new EKunduEBPMTAMotorChangesEditRollsRoyceTest(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;
                        }

                        case "EBP - Motor Specified Risk (Caravan)": {

                            EkunduEBPMotorSpecifiedCaravanTest CRV = new EkunduEBPMotorSpecifiedCaravanTest(testData);
                            reportGenerator.addResult(CRV.executeTest());
                            break;

                        }

                        case "EPL - MTA - Edit Rolls Royce (Decrease Sum Insured)": {

                            EKunduMTAMotorChangesTest MTA = new EKunduMTAMotorChangesTest(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;

                        }

                        case "EBP - MTA - Edit Rolls Royce (Decrease Sum Insured)": {

                            EKunduMTAMotorChangesRollsRoyceDecreaseSumInsuredTest MTA = new EKunduMTAMotorChangesRollsRoyceDecreaseSumInsuredTest(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;

                        }

                        case "EPL - MTA - Copy Motor Risk (Polo Vivo)": {

                            EKunduMTAMotorChangesTest MTA = new EKunduMTAMotorChangesTest(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;

                        }

                        case "EBP - MTA - Copy Motor Risk (Polo Vivo)": {

                            EKunduMTAMotorChangesCopyPoloVivoTest MTA = new EKunduMTAMotorChangesCopyPoloVivoTest(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;

                        }

                        case "EPL - MTA - Delete Motor Risk (Polo Vivo)": {

                            EKunduMTAMotorChangesTest MTA = new EKunduMTAMotorChangesTest(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;

                        }

                        case "EBP - MTA - Delete Motor Risk (Polo Vivo)": {

                            EKunduMTAMotorChangesDeletePoloVivoTest MTA = new EKunduMTAMotorChangesDeletePoloVivoTest(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;

                        }

                        case "EBP Motor Changes Monthly - Policy Renewal": {

                            EkunduEBPMotorChangesMonthlyPolicyRenewalTest MCMRS = new EkunduEBPMotorChangesMonthlyPolicyRenewalTest(testData);
                            reportGenerator.addResult(MCMRS.executeTest());
                            break;

                        }

                        case "EBP Motor Changes Monthly - Policy Renewal (Add - Delete)": {

                            EkunduEBPMotorChangesMonthlyPolicyRenewalDeleteAddTest MCMRS = new EkunduEBPMotorChangesMonthlyPolicyRenewalDeleteAddTest(testData);
                            reportGenerator.addResult(MCMRS.executeTest());
                            break;

                        }

                        case "EDPL - Buildings and Contents Section Details Risk": {

                            BuildingsandSectionDetailsRisk MCMRS = new BuildingsandSectionDetailsRisk(testData);
                            reportGenerator.addResult(MCMRS.executeTest());
                            break;

                        }

                        case "Create Corporate Claim Public liability": {
                            EKunduCreateCorporateClientClaimPublicLiabilityTest createNewClaim = new EKunduCreateCorporateClientClaimPublicLiabilityTest(testData);
                            reportGenerator.addResult(createNewClaim.executeTest());
                            break;
                        }

                        case "EDPL - Building and Contents Section Details": {
                            EkunduBuildingsAndContentSectionDetailsTest BCSD = new EkunduBuildingsAndContentSectionDetailsTest(testData);
                            reportGenerator.addResult(BCSD.executeTest());
                            break;
                        }

                        case "EDPL - Building and Contents Section Details SASRIA": {
                            EkunduBuildingsAndContentSectionDetailsSASRIATest BCSD = new EkunduBuildingsAndContentSectionDetailsSASRIATest(testData);
                            reportGenerator.addResult(BCSD.executeTest());
                            break;
                        }
                        
                        case "EDPL - Building and Contents Risk SASRIA 1": {
                            EkunduBuildingsAndContentsSASRIATest1 BC = new EkunduBuildingsAndContentsSASRIATest1(testData);
                            reportGenerator.addResult(BC.executeTest());
                            break;
                        }

                        case "EDPL - Building and Contents Risk SASRIA 2": {
                            EkunduBuildingsAndContentsSASRIATest BC = new EkunduBuildingsAndContentsSASRIATest(testData);
                            reportGenerator.addResult(BC.executeTest());
                            break;
                        }

                        case "EBP - Initiate Policy (Motor Changes)": {

                            EkunduInitiateEBPMotorChangesPolicyTest MTA = new EkunduInitiateEBPMotorChangesPolicyTest(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;

                        }

                        case "EBP - Motor Specified (Private Motor)": {

                            EkunduEBPAnnualMotorChangesTest MTA = new EkunduEBPAnnualMotorChangesTest(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;

                        }
                        case "EBP - Motor Section Details Risk": {

                            EkunduEBPAnnualMotorChangesTest MTA = new EkunduEBPAnnualMotorChangesTest(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;

                        }

                        case "EBP - Motor - Caravan": {

                            EkunduEBPAnnualMotorChangesTest MTA = new EkunduEBPAnnualMotorChangesTest(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;

                        }

                        case "EBP - Motor - Trailer": {

                            EkunduEBPAnnualMotorChangesTest MTA = new EkunduEBPAnnualMotorChangesTest(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;

                        }

                        case "EBP - Machinery Breakdown / Consequential Loss": {

                            EkunduMachineryBreakDownRiskTest MBD = new EkunduMachineryBreakDownRiskTest(testData);
                            reportGenerator.addResult(MBD.executeTest());
                            break;

                        }

                        case "EBP-Special Types": {

                            EtanaSpecialTypesTest MTA = new EtanaSpecialTypesTest(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;

                        }
                        case "EBP-Heavy Vehicle": {

                            EtanaHeavyVehicleTest MTA = new EtanaHeavyVehicleTest(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;

                        }
                        case "EBP-Vehicle Older Than 20 Years": {

                            EkunduVehicleOlderThan20Years MTA = new EkunduVehicleOlderThan20Years(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;

                        }
                        case "EBP-Light Vehicle": {
                            EkunduLightVehicleTestClass MTA = new EkunduLightVehicleTestClass(testData);
                            reportGenerator.addResult(MTA.executeTest());
                            break;
                        }
                    }

                    System.out.println("Continuing to next test method");
                }
            }
        }
        SeleniumDriverInstance.shutDown();
        reportGenerator.generateTestReport();
        reportEmailer = new TestReportEmailerUtility(reportGenerator.testResults);
        reportEmailer.SendResultsEmail();

        this.flushOutputStreams();
    }

    private List<TestEntity> loadTestData(String inputFilePath) {
        return excelInputReader.getTestDataFromExcelFile(inputFilePath);
    }

    public static void CheckBrowserExists() {
        if (SeleniumDriverInstance == null) {
            SeleniumDriverInstance = new SeleniumDriverUtility(browserType);
            SeleniumDriverInstance.startDriver();
        }
    }

    public static void ensureNewBrowserInstance() {
        if (SeleniumDriverInstance.isDriverRunning()) {
            SeleniumDriverInstance.shutDown();
        }
        SeleniumDriverInstance.startDriver();
    }

    public String generateDateTimeString() {
        Date dateNow = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss");

        return dateFormat.format(dateNow).toString();
    }

    public void generateReportDirectory() {
        this.reportDirectory = ApplicationConfig.ReportFileDirectory() + "\\" + scenarioName + "_" + this.generateDateTimeString();
    }

    public void redirectOutputStreams() {
        try {
            File reportDirectory = new File(this.reportDirectory);
            File errorFile = new File(this.reportDirectory + "\\" + "ErrorTestLog.txt");
            File infoFile = new File(this.reportDirectory + "\\" + "InfoTestLog.txt");

            reportDirectory.mkdirs();

            errorOutputStream = new PrintStream(errorFile);
            infoOutputStream = new PrintStream(infoFile);

            System.setOut(infoOutputStream);
            System.setErr(errorOutputStream);
        } catch (FileNotFoundException ex) {
            System.err.println("[Error] could not create log files - " + ex.getMessage());
        }
    }

    public void flushOutputStreams() {

        errorOutputStream.flush();
        infoOutputStream.flush();

        errorOutputStream.close();
        infoOutputStream.close();

    }
}
