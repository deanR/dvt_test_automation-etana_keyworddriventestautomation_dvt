
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import org.openqa.selenium.JavascriptExecutor;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author ferdinandN
 */
public class EkunduEBPMonthlyMTRTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduEBPMonthlyMTRTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToFindClientPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find client page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the find client page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findClient())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to  find client", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!reinstatePolicy())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to reinstate policy", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to  reinstate policy- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!changeBusinessSource())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to change business source", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to change business source- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!QuoteBusinessAllRisk())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to quote Business All Risk", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to quote Business All Risk- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Policy Mid-term Reinstatement completed successfully",
                              this.getTotalExecutionTime());

      }

    private boolean navigateToFindClientPage()
      {
        if(!SeleniumDriverInstance.navigateToFindClientPage(EkunduViewClientDetailsPage.ClientHoverTabId(),"Find Client"))
        {
            return false;
        }

        return true;
      }

    private boolean findClient()
      {
        String clientCode = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                                "Retrieved Client Code");

        if (clientCode.equals("parameter not found"))
          {
            clientCode = testData.TestParameters.get("Client Code");
          }
        else
          {
            testData.updateParameter("Client Code", clientCode);
          }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
          {
            return false;
          }

        return true;
      }

    private boolean reinstatePolicy()
      {
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ViewAllPoliciesCheckboxId(),
                true))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);

           String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                                  "Retrieved Policy Number");

        if (policyNumber.equals("parameter not found"))
          {
            policyNumber = testData.TestParameters.get("Policy Number");
          }
        else
          {
            testData.updateParameter("Policy Number", policyNumber);
          }
         
         if(!SeleniumDriverInstance.changePolicy(policyNumber, EkunduViewClientDetailsPage.ReinstatePolicyLinkText()))
        {
            return false;
        }

        SeleniumDriverInstance.acceptAlertDialog();

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean changeBusinessSource()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PolicyHeaderTabId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.BusinessSourceDropDownListId(),
                this.getData("Business Source")))
          {
            return false;
          }

        JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;

        js.executeScript("__doPostBack('ctl00$cntMainBody$POLICYHEADER__BUSINESSTYPE','onchange')");

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AgentCodeButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.FindAgentDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.AgentCodeTextBoxId(),
                testData.TestParameters.get("Agent Code")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchAgentsButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);

//      if(!SeleniumDriverInstance.doubleClickElementbyLinkText("select"))
//      {
//          return false;
//      }

        SeleniumDriverInstance.clickElementById("ctl01_cntMainBody_grdvSearchResults_ctl02_lnkbtnSelect");

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Policy details updated successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        return true;
      }

     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
   
    private boolean QuoteBusinessAllRisk()
      {
       
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.BusinessAllRiskMonthlyActionDropdownListXpath(),
                "Edit"))
          {
            return false;
          }
          

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Business All Risk quoted successfully", false);

        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
