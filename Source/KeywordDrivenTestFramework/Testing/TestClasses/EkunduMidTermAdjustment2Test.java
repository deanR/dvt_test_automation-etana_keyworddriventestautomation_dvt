/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.CurrentEnvironment;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduElectronicEquipmentRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author FerdinandN
 */
public class EkunduMidTermAdjustment2Test extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public EkunduMidTermAdjustment2Test(TestEntity testData) {
        this.testData = testData;

    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToFindClientPage()) {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find client page", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to navigate to the find client page - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!findClient()) {
            SeleniumDriverInstance.takeScreenShot("Failed to find client", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!verifyClientDetailsPageHasLoaded()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the client details page has loaded", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the client details page has loaded- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterMidTermAdjustmentData()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Mid Term Adjustment data", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Mid Term Adjustment data- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!editBusinessRiskItemDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to edit Business All Risk", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to edit Business All Risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!addBusinessRiskItemDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to add Business All Risk item ", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to add Business All Risk item- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!editGroupedFireRisk()) {
            SeleniumDriverInstance.takeScreenShot("Failed to edit Grouped fire risk", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to edit Grouped fire risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!editFACPropPlacement()) {
            SeleniumDriverInstance.takeScreenShot("Failed to edit FAC Prop Placement data", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to edit FAC Prop Placement data- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!editEletctronicEquipmentRisk()) {
            SeleniumDriverInstance.takeScreenShot("Failed to edit Electronic Equipment risk", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to edit Electronic Equipment risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Mid Term Adjustment completed successfully", this.getTotalExecutionTime());
    }

    private boolean navigateToFindClientPage() {
        if (!SeleniumDriverInstance.navigateToFindClientPage(EkunduViewClientDetailsPage.ClientHoverTabId(), "Find Client")) {
            return false;
        }
        return true;
    }

    private boolean findClient() {
        String clientCode = SeleniumDriverInstance.retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"), "Retrieved Client Code");

        if (clientCode.equals("parameter not found")) {
            clientCode = testData.TestParameters.get("Client Code");
        } else {
            testData.updateParameter("Client Code", clientCode);
        }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId())) {
            return false;
        }

        return true;
    }

    private boolean verifyClientDetailsPageHasLoaded() {
        if (!SeleniumDriverInstance.validateElementTextValueById(EkunduViewClientDetailsPage.ViewClientDetailsLabelId(), "View Client Details")) {
            return false;
        }
        SeleniumDriverInstance.takeScreenShot("Client Details page successfully loaded", false);
        return true;
    }

    private boolean enterMidTermAdjustmentData() {
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText())) {
            return false;
        }

        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                "Retrieved Policy Number");

        if (policyNumber.equals("parameter not found")) {
            policyNumber = testData.TestParameters.get("Policy Number");
        } else {
            testData.updateParameter("Policy Number", policyNumber);
        }

//        if (!SeleniumDriverInstance.changePolicy(policyNumber, "Change")) {
//            return false;
//        }
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ChangePolicyLinkText())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.clickElementbyLinkText("Change")) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MTATypeofChangeDropDownListId(), this.getData("Type of Change"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MIAEffectiveDateTextBoxId(), this.getData("Effective Date"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId())) {
            return false;
        }

        return true;

    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean editBusinessRiskItemDetails() {

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.BusinessAllRiskActionDropdownListXpath(), "Edit")) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        //SeleniumDriverInstance.Driver.navigate().refresh();
//       
//        if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduBusinessAllRiskPage.RiskItemsDeleteItemLinkText()))
//        {
//            return false;
//        }
//        
//        
//        
//        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Risk Item Edited successfully", false);
        return true;
    }

    private boolean addBusinessRiskItemDetails() {
        SeleniumDriverInstance.pause(2000);
        //SeleniumDriverInstance.Driver.navigate().refresh();

        //this.clickBARAddRiskItemButton();
        if (!SeleniumDriverInstance.clickElementbyName(EkunduBusinessAllRiskPage.BusinessAllRiskItemAddItemButtonName())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduBusinessAllRiskPage.BusinessAllRiskItemCategoryDropdownListId(), this.getData("BAR - Item Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemSumInsuredTextboxId(), this.getData("BAR - Item Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemDescriptionTextboxId(), this.getData("BAR - Item Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemRateTextboxId(), this.getData("BAR - Item Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumTextboxId(), this.getData("BAR - Risk Item - Flat Premium"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Business Risk Item details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;

    }

    private boolean clickBARAddRiskItemButton() {

        WebElement cell = null;
        if (CurrentEnvironment == Enums.TestEnvironments.QA) {
            //div[@id = "ctl00_cntMainBody_BAR__ITEM"]/table/tfoot/tr/td[9]/input[@value = "Add"]
            cell = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_BAR__ITEM\"]/table/tfoot/tr/td[10]/input[@value = \"Add\"]"));

        }

        if (cell == null) {
            cell = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_BAR__ITEM\"]/table/tfoot/tr/td[9]/input[@value = \"Add\"]"));
        }
        {

        }
        if (cell != null) {
            cell.click();
            System.out.println("[TestInfo] Add Risk Item Button Clicked");
            return true;
        } else {
            System.err.println("[Error] Failed to click add risk item button");
            return false;
        }
    }

    private boolean editGroupedFireRisk() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.GroupedFireRiskActionDropdownListXpath(), "Edit")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireOverviewFlatPremiumCheckBoxId(), true)) {
           return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireRiskDataBuildingsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.FireRiskDataBuildingsRiskSumInsuredTextBoxId(), this.getData("GF - Fire - Risk Data - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.FireRiskDataBuildingsRiskPremiumTextBoxId(), this.getData("GF - Fire - Risk Data - Premium"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Fire risk entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementbyLinkText("Reinsurances");

        return true;
    }

    private boolean editFACPropPlacement() {
        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.MunichReinsurancePercTextBoxId(), this.getData("GF - Reinsurance Details - FAC Placement - Munich Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain")) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("FAC Prop Placement edited successfully", false);

        SeleniumDriverInstance.scrollDownByOnePage("ctl00_cntMainBody_ReInsurance2007Cntrl_lblOrgReinsurance");

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementbyLinkText("Reinsurances");
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(5000);
        return true;
    }

    private boolean editEletctronicEquipmentRisk() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.ElectronicEquipmentRiskActionDropdownListXpath(), "Edit")) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        // SeleniumDriverInstance.Driver.navigate().refresh();
        //this.clickAddEERiskItemButton();
        if (!SeleniumDriverInstance.clickElementbyName(EkunduElectronicEquipmentRiskPage.AddEERiskItemButtonName())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduElectronicEquipmentRiskPage.ItemDetailsCategoryDropdownListId(), this.getData("EE - Item Details - Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsDescriptionTextboxId(), this.getData("EE - Item Details - Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSumInsuredTextboxId(), this.getData("EE - Item Details - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduElectronicEquipmentRiskPage.ItemDetailsFlatPremiumCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsFlatPremiumTextboxId(), this.getData("EE - Item Details - Flat Premium"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Item Details Entered Sucessfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementbyLinkText("Reinsurances");
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(5000);
        return true;
    }

    private boolean clickAddEERiskItemButton() {
        WebElement cell = null;

        try {
            if (CurrentEnvironment == Enums.TestEnvironments.QA) {
                //*[@id="ctl00_cntMainBody_EE__ITEM"]/table/tfoot/tr/td[9]/input
                cell = SeleniumDriverInstance.Driver.findElement(By.xpath("//*[@id=\"ctl00_cntMainBody_EE__ITEM\"]/table/tfoot/tr/td[9]/input"));
            }

//            if (cell == null) {
//                cell = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_BAR__ITEM\"]/table/tfoot/tr/td[9]/input[@value = \"Add\"]"));
//            }
//            
            if (cell != null) {
                cell.click();
                System.out.println("[TestInfo] Add Risk Item Button Clicked");
                return true;
            } else {
                System.err.println("[Error] Failed to click add risk item button");
                return false;
            }

        } catch (Exception e) {
            System.err.println("[Error] Failed to click add risk item button" + e.getMessage());
            return false;
        }

    }

}
