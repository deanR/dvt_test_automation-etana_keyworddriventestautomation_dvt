/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.CreateNewClaimPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduGoodsInTransitRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author ferdinandN
 */
public class EkunduEBPRenewalSelectionViaFindClientTest extends BaseClass
{
    TestEntity testData;
    TestResult testResult;
    JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver; 
    
    public EkunduEBPRenewalSelectionViaFindClientTest(TestEntity testData)
    {
        this.testData = testData;

    }
    
    public TestResult executeTest()
    {  
         SeleniumDriverInstance.DriverExceptionDetail = "";
         this.setStartTime();

          if(!navigateToFindClientPage())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to navigate to the find client page", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to navigate to the find client page- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
          
//          if(!verifyFindClientPageHasLoaded())
//        {
//            SeleniumDriverInstance.takeScreenShot( "Failed to load the find client page", true);
//            return new TestResult(testData,false, "Failed toload the find client page- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//        }
          
          if(!findPolicy())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to  find client", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
          
            if(!startRenewalSelection())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to  start renewal selection", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to start renewal selection- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
                
            if(!quoteEBPRoadsideAssist())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to quote roadside assist", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to quote roadside assist- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
            
            if(!applyMTAChangesOnEBP_GroupedFire())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to  apply MTA changes on grouped fire", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to apply MTA changes on grouped fire- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
            
            if(!quotePolicyRisks())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to  apply MTA changes on grouped fire", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to apply MTA changes on grouped fire- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
            
            if(!editGoodsinTransitRisk())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to edit Goods in Transit risk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to edit Goods in Transit risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
            
            if(!deleteBusinessAllRisk())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to delete Business All Risk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to delete Business All Risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
 
            if(!makePolicyLive())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to make policy live", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to  make policy live- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
            
            return new TestResult(testData,Enums.ResultStatus.PASS, "Policy Renewal Selection completed succesfully", this.getTotalExecutionTime());
       
        
    }
    
    private boolean deleteBusinessAllRisk()
    {
        System.out.println("Attempting to delete Business All Risk");
        
            if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.BusinessAllRiskActionDropdownListXpath(), "Delete"))
            {
                return false;
            }
            
            SeleniumDriverInstance.pause(3000);
        
        
        SeleniumDriverInstance.acceptAlertDialog();
        
        SeleniumDriverInstance.pause(5000);
        SeleniumDriverInstance.takeScreenShot("Business All Risk deleted", false);
        
        System.out.println("Business All Risk deleted");
        
        return true;
    }
    
    private boolean quoteEBPRoadsideAssist()
   {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(), "Edit"))
        {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
           
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        
        SeleniumDriverInstance.clickElementbyLinkText("Reinsurances");
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId()))
        {
            return false;
        }
        
            return true; 
    }
    
    private boolean navigateToFindClientPage()
    {       
         if(!SeleniumDriverInstance.navigateToFindClientPage(EKunduHomePage.ClientHoverTabId(),"Find Client"))
        {
            return false;
        }
       
        
         SeleniumDriverInstance.pause(5000);
            return true;       
    }
      
    private boolean verifyFindClientPageHasLoaded()
    {   
        if(!SeleniumDriverInstance.validateElementTextValueById(EKunduFindClientPage.PageHeaderSpanId(),"Search Criteria"))
        {
            return false;
        }
        else
            SeleniumDriverInstance.takeScreenShot( "Find Client page was successfully loaded", false);
            return true;
    }
    
    private boolean findPolicy()
    { 
        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),"Retrieved Policy Number");
      
      if(policyNumber.equals("parameter not found"))
      {
          policyNumber = testData.TestParameters.get("PolicyRef");
      }
      else
      {
          testData.updateParameter("PolicyRef", policyNumber);
      }
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PolicyNumberTextBoxId(),   policyNumber))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Client Found", false);
        
        if(!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean startRenewalSelection()
    {
        if(!SeleniumDriverInstance.clickElementbyLinkText("Policies"))
        {
            return false;
        }
         String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                                  "Retrieved Policy Number");

        if (policyNumber.equals("parameter not found"))
          {
            policyNumber = testData.TestParameters.get("Policy Number");
          }
        else
          {
            testData.updateParameter("Policy Number", policyNumber);
          }
         
         if(!SeleniumDriverInstance.changePolicy(policyNumber, "Details"))
        {
            return false;
        }
          
          return true;
    }
    
    private boolean applyMTAChangesOnEBP_GroupedFire() 
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.GroupedFireRiskActionDropdownListXpath(), "Edit"))
        {
            return false;
        }
        
        
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) 
        {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) 
        {
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) 
        {
            return false;
        }


        SeleniumDriverInstance.takeScreenShot("Grouped Fire risk edited successfully", false);
        
                   
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
                        
        this.enterFACPropPlacementData();
    
        SeleniumDriverInstance.takeScreenShot("MTA Changes On EBP Grouped fire Applied successfully", false);

        
        return true;

    }
 
    private boolean enterFACPropPlacementData()
    {
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), this.getData("ReinsurerCode1")))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
           {
               return false;
           }
           
           SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
           if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
            {
                return false;
            }
           
           SeleniumDriverInstance.pause(2000);

            if(!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
            {
                return false;
            }
            
            SeleniumDriverInstance.pause(2000);
     
           
           if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId(), this.getData("EVERESTREThis%")))
           {
               return false;
           }
           
            if(!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
           {
               return false;
           }   
            
            SeleniumDriverInstance.pause(5000);
            
            if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
        {
            return false;
        }
           
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), this.getData("ReinsurerCode2")))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
           {
               return false;
           }
           
           SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
           
           if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
        {
            return false;
        }
    
           
           if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.HannoverreReinsurancePercTextBoxId(), this.getData("HANNOVEREThis%")))
           {
               return false;
           }
           
                if(!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
           {
               return false;
           }   
                
                SeleniumDriverInstance.pause(5000);
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), this.getData("ReinsurerCode3")))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectMunichReinsurerLinkId()))
           {
               return false;
           }
           
           SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
           
           if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
        {
            return false;
        }
           
           SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
        {
            return false;
        }
           
  
           if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.MunichReinsurancePercTextBoxId(), this.getData("MUNICHREAFRICAThis%")))
           {
               return false;
           }
           
                if(!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
           {
               return false;
           }    
                
                SeleniumDriverInstance.pause(5000);
                
                 if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), this.getData("ReinsurerCode4")))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectCaisseReinsurerLinkId()))
           {
               return false;
           }
           
           SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
           
           if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
        {
            return false;
        }
           
  
           if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.CaisseReinsurancePercTextBoxId(), this.getData("CAISSEThis%")))
           {
               return false;
           }
           
                if(!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
           {
               return false;
           }
                
                SeleniumDriverInstance.pause(2000);
           
                SeleniumDriverInstance.takeScreenShot("FAC Proc Placement data entered successfully", false);
                
                SeleniumDriverInstance.pause(2000);
                
        if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
        {
            return false;
        }
           
           SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
         
           return true;
       }
       
    private boolean quotePolicyRisks()
    {  
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.ElectronicEquipmentRiskActionDropdownListXpath(), "Edit"))
        {
            return false;
        }
        
         
          if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
          
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        
        
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId());
        
        
        
        SeleniumDriverInstance.takeScreenShot("Electronic Equipment Risk Quoted successfully", false);
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.MoneyRiskActionDropdownListXpath(), "Edit"))
        {
            return false;
        }
        
   
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
       
  
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(), "Edit"))
        {
            return false;
        }
        
   
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
       
  
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId()))
        {
            return false;
        }
       
          SeleniumDriverInstance.takeScreenShot("Policy Risks quoted successfully", false);
          
          return true;
   
    }
    
    private boolean makePolicyLive()
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(), "Edit"))
        {
            return false;
        }
        
   
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
        {
            return false;
        }
       
  
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId()))
        {
            return false;
        }
        
       SeleniumDriverInstance.scrollElementIntoViewById(EkunduViewClientDetailsPage.RenewalInvitePrintButtonId());
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RenewalInvitePrintButtonId()))
           {
               return false;
           }
       
       SeleniumDriverInstance.pause(3000);
       
       
       SeleniumDriverInstance.fullScrolltoBottomOfPage();
      
       
       SeleniumDriverInstance.scrollElementIntoViewById(EkunduViewClientDetailsPage.MakeLiveButtonId());
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.MakeLiveButtonId()))
           {
               return false;
           }
       
       SeleniumDriverInstance.takeScreenShot("Policy made live", false);
       
       return true;
   }
  
    public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
    
    private boolean editGoodsinTransitRisk()
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.GoodsinTransitActionDropdownListXpath(), "Edit"))
        {
            return false;
        }
        
        
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.scrollElementIntoViewById(EkunduViewClientDetailsPage.FinishButtonId());
        
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        
        this.enterFACXOLPlacementData();
        
        return true;
        
    }
    
    private boolean enterFACXOLPlacementData()
    {
       SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
       
        if(!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.AddFACXOLButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), this.getData("Reinsurer Code")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
        {
            return false;
        }
        
       // SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLLowerLimitId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLLowerLimitId(), this.getData("FAP LOX Lower Limit")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        
        //SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId(), this.getData("FAP LOX Upper Limit")))
        {
            return false;
        }
        
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        //SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId());
        
        SeleniumDriverInstance.pause(10000);
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId(), this.getData("FAP LOX Participation Percentage")))
        {
            return false;
        }
        
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
       // SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId(), this.getData("FAP LOX Premium")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
         
         SeleniumDriverInstance.pause(10000);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.FACXOLOkButtonId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        
        if(!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.AddFACXOLButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), this.getData("Reinsurer Code 2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLLowerLimitId(), this.getData("FAP LOX Lower Limit 2")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId(), this.getData("FAP LOX Upper Limit 2")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
         
         SeleniumDriverInstance.pause(5000);
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId(), this.getData("FAP LOX Participation Percentage 2")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId(), this.getData("FAP LOX Premium 2")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLCommisionTextboxId(), this.getData("FAP LOX Commision")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
         SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId());
      
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), this.getData("Reinsurer Code 3")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
        {
            return false;
        }
 
        SeleniumDriverInstance.pause(10000);
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLCaisseParticipationPercTextboxId(), this.getData("FAP LOX Participation Percentage 2")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLCiassePremiumTextboxId(), this.getData("FAP LOX Premium 2")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLCaisseCommisionTextboxId(), this.getData("FAP LOX Commision")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
         
         SeleniumDriverInstance.pause(10000);
         
        
        if(!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.FACXOLOkButtonId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.pause(5000);
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId()))
        {
            return false;
        }
        
        return  true;
    }
}
