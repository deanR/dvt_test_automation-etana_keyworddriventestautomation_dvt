
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author FerdinandN
 */
public class EkunduEditBusinessAllRiskTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduEditBusinessAllRiskTest(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!deleteBusinessRiskItemDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to delete Business Risk Item", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to delete Business Risk Item- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!addBusinessRiskItemDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to add Business Risk Item", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to add Business Risk Item- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS,
                              "Business All Risk edited successfully - "
                              + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());

      }

     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean deleteBusinessRiskItemDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EditBusinessAllRiskLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduBusinessAllRiskPage.RiskItemsDeleteItemLinkText()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Risk Item deleted successfully", false);

        return true;
      }

    private boolean addBusinessRiskItemDetails()
      {
        this.clickAddRiskItemButton();

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduBusinessAllRiskPage.BusinessAllRiskItemCategoryDropdownListId(),
                this.getData("Business All Risk Item Category")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemSumInsuredTextboxId(),
                this.getData("Business All Risk Item Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemDescriptionTextboxId(),
                this.getData("Business All Risk Item Description")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemRateTextboxId(),
                this.getData("Business All Risk Item Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumTextboxId(),
                this.getData("Risk Item -  Flat Premium")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Business Risk Item details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;

      }

    private boolean clickAddRiskItemButton()
      {
        WebElement cell =
            SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_BAR__ITEM\"]/table/tfoot/tr/td[7]/input[@value = \"Add\"]"));

        if (cell != null)
          {
            cell.click();
            System.out.println("[TestInfo] Add Risk Item Button Clicked");

            return true;
          }
        else
          {
            System.err.println("[Error] Failed to click add risk item button");

            return false;
          }

      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
