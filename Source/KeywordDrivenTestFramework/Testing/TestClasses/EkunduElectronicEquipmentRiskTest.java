/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduElectronicEquipmentRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduGoodsInTransitRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author FerdinandN
 */
public class EkunduElectronicEquipmentRiskTest extends BaseClass {

    TestEntity testData;
    TestResult testResultt;

    public EkunduElectronicEquipmentRiskTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk button is present", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the add risk button is present - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterRiskDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter risk details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterFirstAmountPayable()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter First Amount Payable", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter First Amount Payable - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterExtensions()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Extensions", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Extensions - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!addFirstRiskItem()) {
            SeleniumDriverInstance.takeScreenShot("Failed to add risk item number 1", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to add risk item number 1 - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!addSecondItem()) {
            SeleniumDriverInstance.takeScreenShot("Failed to add risk item number 2", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to add risk item number 2 - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!addThirdItem()) {
            SeleniumDriverInstance.takeScreenShot("Failed to add risk item number 3", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to add risk item number 3 - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!addForthItem()) {
            SeleniumDriverInstance.takeScreenShot("Failed to add risk item number 4", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to add risk item number 4 - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!addFifthItem()) {
            SeleniumDriverInstance.takeScreenShot("Failed to add risk item number 5", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to add risk item number 5 - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!addSixthItem()) {
            SeleniumDriverInstance.takeScreenShot("Failed to add risk item number 6", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to add risk item number 6 - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!addSeventhItem()) {
            SeleniumDriverInstance.takeScreenShot("Failed to add risk item number 7", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to add risk item number 7 - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!addEighthItem()) {
            SeleniumDriverInstance.takeScreenShot("Failed to add risk item number 8", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to add risk item number 8 - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!SpecifyAlarmWarranty()) {
            SeleniumDriverInstance.takeScreenShot("Failed to specify Alarm Warranty", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to specify Alarm Warranty - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterEndorsements()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter endorsements", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter endorsements - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterNotes()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk notes", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter risk notes - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Electronic Equipment Risk details entered successfully" + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
    }

    private boolean verifyAddRiskButtonisPresent() {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectElectronicEquipmentRiskType();
            return true;
        } else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            selectElectronicEquipmentRiskType();
            return true;
        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");
            return false;
        }

    }

    private boolean selectElectronicEquipmentRiskType() {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.pause(10000);
        SeleniumDriverInstance.takeScreenShot("Electronic Equipment Risk selected sucessfully", false);
        return true;

    }

    private boolean enterRiskDetails() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduElectronicEquipmentRiskPage.PrimaryIndustryDropDownListId(), this.getData("Details Primary Industry"))) {
            return false;
        }

        SeleniumDriverInstance.WaitUntilDropDownListPopulatedById(EkunduElectronicEquipmentRiskPage.SecondaryIndustryDropDownListId());

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduElectronicEquipmentRiskPage.SecondaryIndustryDropDownListId(), this.getData("Details Secondary Industry"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduElectronicEquipmentRiskPage.TertiaryIndustryDropDownListId(), this.getData("Details Tertiary Industry"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(), this.getData("Details Industry"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.IndustrySearchResultsSelectedRowId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Risk details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }
        return true;

    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean enterFirstAmountPayable() {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduElectronicEquipmentRiskPage.RiskItemFAPPerSectionCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemFAPPerSectionMinimumPercTextboxId(), this.getData("FAP Per Section Min Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemFAPPerSectionMinimumAmountTextboxId(), this.getData("FAP Per Section Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemFAPPerSectionMaximumAmountTextboxId(), this.getData("FAP Per Section Max Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduElectronicEquipmentRiskPage.RiskItemFAPLightningCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemFAPLightningMinimumPercTextboxId(), this.getData("FAP Lightning/Surge Min Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemFAPLightningMinimumAmountTextboxId(), this.getData("FAP Lightning/Surge Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemFAPLightningMaximumAmountTextboxId(), this.getData("FAP Lightning/Surge Max Amount"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("First Amount Payable details entered successfully", false);
        return true;

    }

    private boolean enterExtensions() {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduElectronicEquipmentRiskPage.ExtensionsAdditionalClaimsPreparationCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsAdditionalClaimsPreparationLimitofIndemnityTextboxId(), this.getData("Extensions Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsAdditionalClaimsPreparationRateTextboxId(), this.getData("Extensions Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduElectronicEquipmentRiskPage.ExtensionsIncompatibilityCoverCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsIncompatibilityCoverLimitofIndemnityTextboxId(), this.getData("Extensions Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsIncompatibilityCoverRateTextboxId(), this.getData("Extensions Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsIncompatibilityCoverFAPPercTextboxId(), this.getData("Extensions FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsIncompatibilityCoverFAPAmountTextboxId(), this.getData("Extensions FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduElectronicEquipmentRiskPage.ExtensionsIncreaseCostCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsIncreaseCostLimitofIndemnityTextboxId(), this.getData("Extensions Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsIncreaseCostRateTextboxId(), this.getData("Extensions Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsIncreaseCostFAPPercTextboxId(), this.getData("Extensions FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsIncreaseCostFAPAmountTextboxId(), this.getData("Extensions FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduElectronicEquipmentRiskPage.ExtensionsIncreaseCostTimeFAPDropdownListId(), this.getData("Extensions Time FAP"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduElectronicEquipmentRiskPage.ExtensionsLossofRevenueCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsLossofRevenueLimitofIndemnityTextboxId(), this.getData("Extensions Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsLossofRevenueRateTextboxId(), this.getData("Extensions Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsLossofRevenueFAPPercTextboxId(), this.getData("Extensions FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsLossofRevenueFAPAmountTextboxId(), this.getData("Extensions FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduElectronicEquipmentRiskPage.ExtensionsReinstatementofDataCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsReinstatementofDataLimitofIndemnityTextboxId(), this.getData("Extensions Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsReinstatementofDataRateTextboxId(), this.getData("Extensions Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsReinstatementofDataFAPPercTextboxId(), this.getData("Extensions FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsReinstatementofDataFAPAmountTextboxId(), this.getData("Extensions FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduElectronicEquipmentRiskPage.ExtensionsTelkomAccessLinesCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsTelkomAccessLinesLimitofIndeminityTextboxId(), this.getData("Extensions Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsTelkomAccessLinesRateTextboxId(), this.getData("Extensions Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsTelkomAccessLinesFAPPercTextboxId(), this.getData("Extensions FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsTelkomAccessLinesFAPAmountTextboxId(), this.getData("Extensions FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduElectronicEquipmentRiskPage.ExtensionsTransitCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsTransitLimitofIndeminityTextboxId(), this.getData("Extensions Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsTransitRateTextboxId(), this.getData("Extensions Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsTransitFAPPercTextboxId(), this.getData("Extensions FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ExtensionsTransitFAPAmountTextboxId(), this.getData("Extensions FAP Amount"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Extensions entered successfully", false);
        return true;

    }

    private boolean addFirstRiskItem() {
        //clickAddRiskItemButton();

        if (!SeleniumDriverInstance.clickElementbyName("ctl00$cntMainBody$EE__ITEM$ctl02$ctl00")) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduElectronicEquipmentRiskPage.ItemDetailsCategoryDropdownListId(), this.getData("Item Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsDescriptionTextboxId(), this.getData("Item 1 Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSerialNumberTextboxId(), this.getData("Item 1 Serial Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsNumberofItemsTextboxId(), this.getData("Item 1 Number of Items"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSumInsuredTextboxId(), this.getData("Item Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsRateTextboxId(), this.getData("Item Agreed Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ItemDetailsRateButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemCommentsTextboxId(), this.getData("Item 1 Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemNotesTextboxId(), this.getData("Item 1 Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Item 1 entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }
        return true;
    }

    private boolean addSecondItem() {
       //clickAddRiskItemButton();

        if (!SeleniumDriverInstance.clickElementbyName("ctl00$cntMainBody$EE__ITEM$ctl03$ctl00")) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduElectronicEquipmentRiskPage.ItemDetailsCategoryDropdownListId(), this.getData("Item Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsDescriptionTextboxId(), this.getData("Item 2 Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSerialNumberTextboxId(), this.getData("Item 2 Serial Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsNumberofItemsTextboxId(), this.getData("Item 2 Number of Items"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSumInsuredTextboxId(), this.getData("Item Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsRateTextboxId(), this.getData("Item Agreed Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ItemDetailsRateButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemCommentsTextboxId(), this.getData("Item 2 Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemNotesTextboxId(), this.getData("Item 2 Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Item 2 entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }
        return true;
    }

    private boolean addThirdItem() {
       //clickAddRiskItemButton();

        if (!SeleniumDriverInstance.clickElementbyName("ctl00$cntMainBody$EE__ITEM$ctl04$ctl00")) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduElectronicEquipmentRiskPage.ItemDetailsCategoryDropdownListId(), this.getData("Item Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsDescriptionTextboxId(), this.getData("Item 3 Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSerialNumberTextboxId(), this.getData("Item 3 Serial Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsNumberofItemsTextboxId(), this.getData("Item 3 Number of Items"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSumInsuredTextboxId(), this.getData("Item Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsRateTextboxId(), this.getData("Item Agreed Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ItemDetailsRateButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemCommentsTextboxId(), this.getData("Item 3 Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemNotesTextboxId(), this.getData("Item 3 Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Item 3 entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }
        return true;

    }

    private boolean addForthItem() {
        //clickAddRiskItemButton();
        if (!SeleniumDriverInstance.clickElementbyName("ctl00$cntMainBody$EE__ITEM$ctl05$ctl00")) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduElectronicEquipmentRiskPage.ItemDetailsCategoryDropdownListId(), this.getData("Item Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsDescriptionTextboxId(), this.getData("Item 4 Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSerialNumberTextboxId(), this.getData("Item 4 Serial Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSumInsuredTextboxId(), this.getData("Item Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsRateTextboxId(), this.getData("Item Agreed Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ItemDetailsRateButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemCommentsTextboxId(), this.getData("Item 4 Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemNotesTextboxId(), this.getData("Item 4 Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Item 4 entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean addFifthItem() {
        //clickAddRiskItemButton();
        if (!SeleniumDriverInstance.clickElementbyName("ctl00$cntMainBody$EE__ITEM$ctl06$ctl00")) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduElectronicEquipmentRiskPage.ItemDetailsCategoryDropdownListId(), this.getData("Item Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsDescriptionTextboxId(), this.getData("Item 5 Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSerialNumberTextboxId(), this.getData("Item 5 Serial Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSumInsuredTextboxId(), this.getData("Item Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsRateTextboxId(), this.getData("Item Agreed Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ItemDetailsRateButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemCommentsTextboxId(), this.getData("Item 5 Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemNotesTextboxId(), this.getData("Item 5 Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Item 5 entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean addSixthItem() {
        //clickAddRiskItemButton();
        if (!SeleniumDriverInstance.clickElementbyName("ctl00$cntMainBody$EE__ITEM$ctl07$ctl00")) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduElectronicEquipmentRiskPage.ItemDetailsCategoryDropdownListId(), this.getData("Item Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsDescriptionTextboxId(), this.getData("Item 6 Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSerialNumberTextboxId(), this.getData("Item 6 Serial Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSumInsuredTextboxId(), this.getData("Item Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsRateTextboxId(), this.getData("Item Agreed Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ItemDetailsRateButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemCommentsTextboxId(), this.getData("Item 6 Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemNotesTextboxId(), this.getData("Item 6 Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Item 6 entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean addSeventhItem() {
        //clickAddRiskItemButton();
        if (!SeleniumDriverInstance.clickElementbyName("ctl00$cntMainBody$EE__ITEM$ctl08$ctl00")) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduElectronicEquipmentRiskPage.ItemDetailsCategoryDropdownListId(), this.getData("Item Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsDescriptionTextboxId(), this.getData("Item 7 Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSerialNumberTextboxId(), this.getData("Item 7 Serial Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSumInsuredTextboxId(), this.getData("Item Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsRateTextboxId(), this.getData("Item Agreed Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ItemDetailsRateButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemCommentsTextboxId(), this.getData("Item 7 Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemNotesTextboxId(), this.getData("Item 7 Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Item 7 entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean addEighthItem() {
        //clickAddRiskItemButton();
        if (!SeleniumDriverInstance.clickElementbyName("ctl00$cntMainBody$EE__ITEM$ctl09$ctl00")) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduElectronicEquipmentRiskPage.ItemDetailsCategoryDropdownListId(), this.getData("Item Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsDescriptionTextboxId(), this.getData("Item 8 Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSerialNumberTextboxId(), this.getData("Item 8 Serial Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSumInsuredTextboxId(), this.getData("Item Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsRateTextboxId(), this.getData("Item Agreed Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ItemDetailsRateButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemCommentsTextboxId(), this.getData("Item 8 Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemNotesTextboxId(), this.getData("Item 8 Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Item 8 entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean SpecifyAlarmWarranty() {
        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduElectronicEquipmentRiskPage.RiskAlarmWarrantyDropdownListId(), this.getData("Item Alarm Warranty"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Alarm Warranty entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterEndorsements() {
        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.EndorsementsSelectButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.EndorsementsAddAllButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.EndorsementsApplyButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterNotes() {
        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemCommentsTextboxId(), this.getData("Electronic Equipment Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.RiskItemNotesTextboxId(), this.getData("Electronic Equipment Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.ElectronicEquipmentNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Documents");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementById("btnGeneratePdf");


       SeleniumDriverInstance.clickElementById("popup_ok");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Risks");

        return true;

    }

    private boolean clickAddRiskItemButton() {
        WebElement cell = SeleniumDriverInstance.Driver.findElement(By.name("ctl00$cntMainBody$EE__ITEM$ctl04$ctl00"));
        //WebElement cell = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_EE__ITEM\"]/table/tfoot/tr/td[9]/input[@value = \"Add\"]"));
        if (cell != null) //div[@id = \"ctl00_cntMainBody_EE__ITEM\"]/table/tfoot/tr/td[6]/input
        {
            cell.click();
            System.out.println("[TestInfo] Add Risk Item Button Clicked");
            return true;
        } else {
            System.err.println("[Error] Failed to click add risk item button");
            return false;
        }

    }
}
