/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

/**
 *
 * @author deanR
 */
public class EkunduMTAOOSTest extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public EkunduMTAOOSTest(TestEntity testData) {
        this.testData = testData;

    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToFindClientPage()) {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find client page", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to navigate to the find client page - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!findClient()) {
            SeleniumDriverInstance.takeScreenShot("Failed to find client", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!verifyClientDetailsPageHasLoaded()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the client details page has loaded", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the client details page has loaded- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterMidTermAdjustmentData()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Mid Term Adjustment data", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Mid Term Adjustment data- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!editBusinessAllRisk()) {
            SeleniumDriverInstance.takeScreenShot("Failed to edit Business All Risk", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to edit Business All Risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!editGroupedFireRisk()) {
            SeleniumDriverInstance.takeScreenShot("Failed to edit Grouped fire risk", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to edit Grouped fire risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

//         if(!deleteGoodsinTransitRisk())
//        {
//            SeleniumDriverInstance.takeScreenShot( "Failed to delete Goods in Transit risk", true);
//            return new TestResult(testData,false, "Failed to delete Goods in Transit risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//        }
//         
//         if(!deleteBusinessAllRisk())
//        {
//            SeleniumDriverInstance.takeScreenShot( "Failed to delete Business All Risk", true);
//            return new TestResult(testData,false, "Failed to delete Business All Risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//        }
//         
//         if(!editMoneyRisk())
//        {
//            SeleniumDriverInstance.takeScreenShot( "Failed to edit Money risk", true);
//            return new TestResult(testData,false, "Failed to edit Money risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//        }
        if (!performRoadSideAssistAndMakePolicyLive()) {
            SeleniumDriverInstance.takeScreenShot("Failed to perform roadside assist and make policy live", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to perform roadside assist and make policy live- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        SeleniumDriverInstance.takeScreenShot("Mid Term Adjustment OOS completed successfully", false);
        return new TestResult(testData, Enums.ResultStatus.PASS, "Mid Term Adjustment OOS completed successfully", this.getTotalExecutionTime());

    }

    private boolean navigateToFindClientPage() {
        if (!SeleniumDriverInstance.navigateToFindClientPage(EkunduViewClientDetailsPage.ClientHoverTabId(), "Find Client")) {
            return false;
        }
        return true;
    }

    private boolean findClient() {
        String clientCode = SeleniumDriverInstance.retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"), "Retrieved Client Code");

        if (clientCode.equals("parameter not found")) {
            clientCode = testData.TestParameters.get("Client Code");
        } else {
            testData.updateParameter("Client Code", clientCode);
        }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId())) {
            return false;
        }

        return true;
    }

    private boolean verifyClientDetailsPageHasLoaded() {
        if (!SeleniumDriverInstance.validateElementTextValueById(EkunduViewClientDetailsPage.ViewClientDetailsLabelId(), "View Client Details")) {
            return false;
        }
        SeleniumDriverInstance.takeScreenShot("Client Details page successfully loaded", false);
        return true;
    }

    private boolean enterMidTermAdjustmentData() {
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText())) {
            return false;
        }

        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                "Retrieved Policy Number");

        if (policyNumber.equals("parameter not found")) {
            policyNumber = testData.TestParameters.get("Policy Number");
        } else {
            testData.updateParameter("Policy Number", policyNumber);
        }

        if (!SeleniumDriverInstance.changePolicy(policyNumber, "Change")
                || !SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ChangePolicyLinkText())) {
            return false;

        }

        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MTATypeofChangeDropDownListId(), this.getData("Type of Change"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MIAEffectiveDateTextBoxId(), this.getData("Effective Date"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId())) {
            return false;
        }

        SeleniumDriverInstance.acceptAlertDialog();
        SeleniumDriverInstance.acceptAlertDialog();
        SeleniumDriverInstance.acceptAlertDialog();

        return true;

    }

    private boolean editGroupedFireRisk() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.GroupedFireRiskActionDropdownListXpath(), "Edit")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireOverviewFlatPremiumCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireRiskDataBuildingsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.FireRiskDataBuildingsRiskSumInsuredTextBoxId(), this.getData("GF - Fire - Risk Data - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.FireRiskDataBuildingsRiskPremiumTextBoxId(), this.getData("GF - Fire - Risk Data - Premium"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Fire risk entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

//           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
//           {
//               return false;
//           }
//           
//           if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), this.getData("Reinsurance Details - FAC Placement Reinsurer Code 1")))
//           {
//               return false;
//           }
//           
//           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
//           {
//               return false;
//           }
//           
//           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
//           {
//               return false;
//           }
//           
//            SeleniumDriverInstance.pause(5000); 
//           
//           if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.CaisseReinsurancePercTextBoxId(), this.getData("Reinsurance Details - FAC Placement - Caisse Percentage")))
//           {
//               return false;
//           }
//           
//            if(!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
//           {
//               return false;
//           }    
//
//            SeleniumDriverInstance.pause(5000);
//            
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId())) {
            return false;
        }

        return true;
    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean deleteGoodsinTransitRisk() {
        JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;

        js.executeScript("javascript:__doPostBack('ctl00$cntMainBody$MultiRisk1$grdvRisk','Page$2')");

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.DeleteGoodsInTransitLinkId())) {
            return false;
        }

        SeleniumDriverInstance.acceptAlertDialog();

        SeleniumDriverInstance.takeScreenShot("Risk deleted", false);

        return true;
    }

    private boolean deleteBusinessAllRisk() {
        JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;

        js.executeScript("javascript:__doPostBack('ctl00$cntMainBody$MultiRisk1$grdvRisk','Page$2')");

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.DeleteBusinessAllRiskLinkId())) {
            return false;
        }

        SeleniumDriverInstance.acceptAlertDialog();

        SeleniumDriverInstance.takeScreenShot("Risk deleted", false);

        return true;
    }

    private boolean editMoneyRisk() {
        if (SeleniumDriverInstance.checkRiskActionElement()) {
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.editMoneyRiskLinkId())) {
                return false;
            }
        } else {
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.MoneyRiskActionDropdownListXpath(), "Edit")) {
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MoneyCoverMajorLimitSumInsuredTextBoxId(), this.getData("Cover - Major Limit - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MoneyCoverMajorLimitPremiumTextBoxId(), this.getData("Cover - Major Limit - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.takeScreenShot("Money risk edited successfully", false);
        return true;

    }

    private boolean performRoadSideAssistAndMakePolicyLive() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(), "Edit")) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Road Side Assist quoted successfully", false);

        //SeleniumDriverInstance.scrollDownByOnePage("ctl00_cntMainBody_SummaryCoverCntrl_ctl00_AgentCommissionCntrl_lblAgentDisplay");
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.MakeLiveButtonId())) {
            return false;
        }

        return true;
    }

    private boolean editBusinessAllRisk() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.BusinessAllRiskActionDropdownListXpath(), "Edit")) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        //SeleniumDriverInstance.Driver.navigate().refresh();
//        if (!SeleniumDriverInstance.clickElementbyXpath(EkunduBusinessAllRiskPage.AddRiskItemButtonXpath())) {
//            return false;
//        }
        if (!SeleniumDriverInstance.clickElementbyName(EkunduBusinessAllRiskPage.BusinessAllRiskItemAddItemButtonName2())) {
            return false;
        }

//        if (!SeleniumDriverInstance.clickElementbyName(EkunduBusinessAllRiskPage.BusinessAllRiskIAddButtonName())) {
//            return false;
//        }else if(!SeleniumDriverInstance.clickElementbyName(EkunduBusinessAllRiskPage.BusinessAllRiskItemAddItemButtonName())) {
//            return false;
//        }
        //this.clickBARAddRiskItemButton(); 
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduBusinessAllRiskPage.BusinessAllRiskItemCategoryDropdownListId(), this.getData("Business All Risk Item Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemSumInsuredTextboxId(), this.getData("Business All Risk Item Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemDescriptionTextboxId(), this.getData("Business All Risk Item Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemRateTextboxId(), this.getData("Business All Risk Item Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumTextboxId(), this.getData("Risk Item - Flat Premium"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Business Risk Item details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.takeScreenShot("Business All Risk edited successfully", false);
        return true;

    }

    private boolean clickBARAddRiskItemButton() {
        WebElement cell = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_BAR__ITEM\"]/table/tfoot/tr/td[7]/input[@value = \"Add\"]"));
        if (cell != null) {
            cell.click();
            System.out.println("[TestInfo] Add Risk Item Button Clicked");
            return true;
        } else {
            System.err.println("[Error] Failed to click add risk item button");
            return false;
        }
    }

}
