
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SeleniumDriverUtility;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;
import java.io.FileInputStream;

/**
 *
 * @author vdhayal
 */
public class SugarCRMCreateContactTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;
    String Region, FirstNamePrefix, FirstName, LastName, JobTitle, AccountName, VerifyTestDataResult;

    public SugarCRMCreateContactTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        if (!VerifyTestDataResult.equals(""))
          {
            return new TestResult(testData, Enums.ResultStatus.FAIL, VerifyTestDataResult, this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Account creation completed successfully", this.getTotalExecutionTime());
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
