
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import static KeywordDrivenTestFramework.Entities.Enums.TestEnvironments.NamQA;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

/**
 *
 * @author FerdinandN
 */
public class EkunduInitiatePersonalClientPolicyTest extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public EkunduInitiatePersonalClientPolicyTest(TestEntity testData) {
        this.testData = testData;

    }

    public TestResult executeTest() {

        this.setStartTime();

        if (!navigateToFindClientPage()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to navigate to the find client page"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to navigate to the find client page- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!findClient()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to find client"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to find client - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!beginNewQuoteCreation()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to start new quote"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to start new quote - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterSummaryDetails()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter summary details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter summary details - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

//      if(!finalisePolicyCreating())
//      {
//          SeleniumDriverInstance.takeScreenShot(("Failed to create Policy for personal client"), true);
//          return new TestResult(testData, false, "Failed to create Policy for personal client- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//      }
        SeleniumDriverInstance.takeScreenShot(("Policy Client Policy initiation completed successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Personal Client Policy initiation completed successfully",
                this.getTotalExecutionTime());
    }

    private boolean navigateToFindClientPage() {
//        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),
//                EKunduHomePage.FindClientTabXPath()))
//          {
//          }

        if (!SeleniumDriverInstance.navigateTo(EKunduHomePage.ClientHoverTabId(), "Find Client")) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Navigation to the Find Client page was successful", false);

        return true;
    }

    private boolean verifyFindClientPageHasLoaded() {
        if (!SeleniumDriverInstance.validateElementTextValueById(EKunduFindClientPage.PageHeaderSpanId(),
                "Search Criteria")) {
            return false;
        } else {
            SeleniumDriverInstance.takeScreenShot("Find Client page was successfully loaded", false);
        }

        return true;
    }

    private boolean findClient() {
        String clientCode = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                "Retrieved Client Code");

        if (clientCode.equals("parameter not found")) {
            clientCode = testData.TestParameters.get("Client Code");
        } else {
            testData.updateParameter("Client Code", clientCode);
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ClientCodeTextBoxId(),
                clientCode)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SearchButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.SelectClientLinkId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Client found", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SelectClientLinkId())) {
            return false;
        }

        return true;
    }

    private boolean beginNewQuoteCreation() {
//        if (CurrentEnvironment == UAT || CurrentEnvironment == QA) {
//
//            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SelectProductDropdownListId(),
//                    testData.TestParameters.get("Personal Client Product 2"))) {
//                return false;
//            }
//        } else {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.SelectProductDropdownListId(),
                testData.TestParameters.get("Personal Client Product"))) {
            return false;
        }
//        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NewQuoteButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Begin new Quote ", false);

        return true;
    }

    private boolean enterSummaryDetails() {
        if (CurrentEnvironment == NamQA) {
            SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_POLICYHEADER__ANALYSISCODE", "Newspaper Advertising");
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AgentCodeButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.FindAgentDialogFrameId())) {
            return false;
        }

        // change test logic to use agent code to search and click the appropriate entry from the table
        // Enter the required agent code first, then click search
        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.AgentCodeTextBoxId(),
                testData.TestParameters.get("Agent Code"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchAgentsButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementById("ctl01_cntMainBody_grdvSearchResults_ctl02_lnkbtnSelect");

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        // add id to page objects + add entry to input file for agent code.
        String agentCodeTextbox = SeleniumDriverInstance.retrieveTextById("ctl00_cntMainBody_POLICYHEADER__AGENTCODE");
        int pollCount = 0;

        while ((agentCodeTextbox == "") && (pollCount < ApplicationConfig.WaitTimeout())) {
            SeleniumDriverInstance.pause(1000);
            agentCodeTextbox = SeleniumDriverInstance.retrieveTextById("ctl00_cntMainBody_POLICYHEADER__AGENTCODE");
            pollCount++;
        }

        System.out.println("[INFO] Agent code entered as - " + agentCodeTextbox);

//      if(!agentCodeTextbox.equalsIgnoreCase("ABSA01B"))
//      {
//          System.err.println("[ERROR] Agent code was not sucessfully entered");
//          return false;
//      }
        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.CoverStartDateTextBoxId());

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.CoverStartDateTextBoxId(),
                testData.TestParameters.get("Personal Client Cover Start Date"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.PaymentTermDropDownListId(),
                testData.TestParameters.get("Payment Term"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.CollectionFrequencyDropDownListId(),
                testData.TestParameters.get("Personal Client Collection Frequency"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Summary details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
