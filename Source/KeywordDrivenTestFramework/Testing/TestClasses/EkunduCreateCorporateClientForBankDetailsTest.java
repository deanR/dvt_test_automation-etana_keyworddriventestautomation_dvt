/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;

/**
 *
 * @author deanR
 */
public class EkunduCreateCorporateClientForBankDetailsTest extends BaseClass {

    TestEntity testData;
    TestResult testResult;
    String VerifyTestDataResult, Branch, CompanyName, CompanyReg, NumberOfEmployees, MainContact;
    String PrimaryIndustry, SecondaryIndustry, TertiaryIndustry, BusinessDescription;
    String CorrospondenceAddressSuburbSearch, BusinessAddressSuburbSearch, CorrospondenceAddressLine1, BusinessAddressLine1;

    public EkunduCreateCorporateClientForBankDetailsTest(TestEntity testData) {
        this.testData = testData;

    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        verifyTestData();
        if (!VerifyTestDataResult.equals("")) {
            return new TestResult(testData, Enums.ResultStatus.FAIL, VerifyTestDataResult, this.getTotalExecutionTime());
        }

        this.setStartTime();

        if (!verifyHomePageHasLoaded()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that Ekundu home page has loaded", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that Ekundu home page has loaded - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!navigateToCorporateClientCreationPage()) {
            SeleniumDriverInstance.takeScreenShot("Failed to change regional branch", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to change regional branch - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!verifyCorporateClientCreationPageHasLoaded()) {
            SeleniumDriverInstance.takeScreenShot("Corporate client creation page failed to load", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Corporate client creation page failed to load - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if (!enterCompanyDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter company details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter company details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if (!enterIndustryInformation()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter industry informations", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter industry information - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if (!navigateToAddressDetailsTab()) {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to address details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to navigate to address details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if (!enterCorrespondentAddress()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter correspondent address", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter correspondent address - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        SeleniumDriverInstance.pause(4000);
        
        if (!enterBusinessAddress()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter business address", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter business address - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if (!enterBusinessDescription()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter business desccription", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter business desccription - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!submitCorporateClientDetaills()) {
            SeleniumDriverInstance.takeScreenShot("Failed to submit corporate client details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to submit corporate client details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if (!verifyCorporateClientCreationSubmitWasSuccessful()) {
            SeleniumDriverInstance.takeScreenShot("Corporate client submit failed", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Corporate client submit failed - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        SeleniumDriverInstance.takeScreenShot("Corporate Client creation completed successfully", false);
        return new TestResult(testData, Enums.ResultStatus.PASS, "Corporate Client creation completed successfully", this.getTotalExecutionTime());
    }

    private void verifyTestData() {
        VerifyTestDataResult = "";

        if (!testData.TestParameters.containsKey("Branch")) {
            Branch = "Branch not specified";
        } else {
            Branch = testData.TestParameters.get("Branch");
        }

        if (!testData.TestParameters.containsKey("Company Name")) {
            VerifyTestDataResult = "Company Name not specified";
        } else {
            CompanyName = testData.TestParameters.get("Company Name");
        }

        if (!testData.TestParameters.containsKey("Company Reg")) {
            VerifyTestDataResult = "Company Reg  not specified";
        } else {
            CompanyReg = testData.TestParameters.get("Company Reg");
        }

        if (!testData.TestParameters.containsKey("Number Of Employees")) {
            VerifyTestDataResult = "Number Of Employees not specified";
        } else {
            NumberOfEmployees = testData.TestParameters.get("Number Of Employees");
        }

        if (!testData.TestParameters.containsKey("Main Contact")) {
            VerifyTestDataResult = "Main Contact not specified";
        } else {
            MainContact = testData.TestParameters.get("Main Contact");
        }

        if (!testData.TestParameters.containsKey("Primary Industry")) {
            VerifyTestDataResult = "Primary Industry not specified";
        } else {
            PrimaryIndustry = testData.TestParameters.get("Primary Industry");
        }

        if (!testData.TestParameters.containsKey("Secondary Industry")) {
            SecondaryIndustry = "Secondary Industry not specified";
        } else {
            SecondaryIndustry = testData.TestParameters.get("Secondary Industry");
        }

        if (!testData.TestParameters.containsKey("Tertiary Industry")) {
            VerifyTestDataResult = "Tertiary Industry not specified";
        } else {
            TertiaryIndustry = testData.TestParameters.get("Tertiary Industry");
        }

        if (!testData.TestParameters.containsKey("Business Description")) {
            VerifyTestDataResult = "Business Description not specified";
        } else {
            BusinessDescription = testData.TestParameters.get("Business Description");
        }

        if (!testData.TestParameters.containsKey("Corrospondence Address Suburb")) {
            VerifyTestDataResult = "Corrospondence Address Suburb not specified";
        } else {
            CorrospondenceAddressSuburbSearch = testData.TestParameters.get("Corrospondence Address Suburb");
        }

        if (!testData.TestParameters.containsKey("Corrospondence Address Line1")) {
            VerifyTestDataResult = "Corrospondence Address Line1 not specified";
        } else {
            CorrospondenceAddressLine1 = testData.TestParameters.get("Corrospondence Address Suburb");
        }

        if (!testData.TestParameters.containsKey("Business Address Line1")) {
            VerifyTestDataResult = "Business Address Line1 not specified";
        } else {
            BusinessAddressLine1 = testData.TestParameters.get("Business Address Line1");
        }

        if (!testData.TestParameters.containsKey("Business Address Suburb")) {
            VerifyTestDataResult = "Business Address Suburb not specified";
        } else {
            BusinessAddressSuburbSearch = testData.TestParameters.get("Business Address Suburb");
        }

        if (!testData.TestParameters.containsKey("File Code")) {
            VerifyTestDataResult = "File Code not specified";
        } else {
            testData.TestParameters.get("File Code");
        }

    }

    private boolean verifyHomePageHasLoaded() {
        try {
            if (!SeleniumDriverInstance.waitForElementById(EKunduHomePage.HomeDivId())) {
                return false;
            } else {
                SeleniumDriverInstance.takeScreenShot("Home page successfully loaded", false);
            }
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    private boolean verifyCorporateClientCreationPageHasLoaded() {
        try {
            if (!SeleniumDriverInstance.waitForElementById(EKunduCreateCorporateClientPage.AddCorporateClientSpanId())) {
                return false;
            } else {
                SeleniumDriverInstance.takeScreenShot("Corporate client creation page successfully loaded", false);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean verifyCorporateClientCreationSubmitWasSuccessful() {
        try {
//            if (!SeleniumDriverInstance.waitForElementById(EKunduCreateCorporateClientPage.ViewCorporateClientDetailsSpanId())) {
//                return false;
//            }

            testData.addParameter("Retrieved Client Code", SeleniumDriverInstance.retrieveTextById(EKunduCreateCorporateClientPage.ClientIdSpanId()));
            SeleniumDriverInstance.takeScreenShot("Corporate client submittion was successful", false);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean navigateToCorporateClientCreationPage() {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(), EKunduHomePage.NewCorporateClientTabXPath())) {
            return false;
        }
        SeleniumDriverInstance.takeScreenShot("Navigation to the Create Personal client page was successful", false);
        return true;
    }

    private boolean enterCompanyDetails() {
        if (!SeleniumDriverInstance.enterTextById(EKunduCreateCorporateClientPage.CompanyNameTextBoxId(), this.CompanyName + " " + SeleniumDriverInstance.generateDateTimeString())) {
            return false;
        }
        if (!SeleniumDriverInstance.enterTextById(EKunduCreateCorporateClientPage.CompanyRegTextBoxId(), this.CompanyReg)) {
            return false;
        }
        if (!SeleniumDriverInstance.enterTextById(EKunduCreateCorporateClientPage.MainContactTextBoxId(), this.MainContact)) {
            return false;
        }
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EKunduCreateCorporateClientPage.NumberOfEmployeesDropDownId(), this.NumberOfEmployees)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EKunduCreateCorporateClientPage.fileCodeTextBoxId(), testData.TestParameters.get("File Code"))) {
            return false;
        }
        return true;
    }

    private boolean enterIndustryInformation() {

        SeleniumDriverInstance.WaitUntilDropDownListPopulatedById(EKunduCreateCorporateClientPage.PrimaryIndustryDropDownId());
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EKunduCreateCorporateClientPage.PrimaryIndustryDropDownId(), this.PrimaryIndustry)) {
            return false;
        }

        SeleniumDriverInstance.WaitUntilDropDownListPopulatedById(EKunduCreateCorporateClientPage.SecondaryIndustryDropDownId());
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EKunduCreateCorporateClientPage.SecondaryIndustryDropDownId(), this.SecondaryIndustry)) {
            return false;
        }

        SeleniumDriverInstance.WaitUntilDropDownListPopulatedById(EKunduCreateCorporateClientPage.TertiaryIndustryDropDownId());
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EKunduCreateCorporateClientPage.TertiaryIndustryDropDownId(), this.TertiaryIndustry)) {
            return false;
        }
        return true;
    }

    private boolean navigateToAddressDetailsTab() {
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EKunduCreateCorporateClientPage.AddressesTabPartialLinkText())) {
            return false;
        }
        return true;
    }

    private boolean enterCorrespondentAddress() {
        SeleniumDriverInstance.takeScreenShot("Navigation to the address details tab was successful", false);
        SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.AddAdressLinkId());

        SeleniumDriverInstance.switchToFrameById(EKunduCreateCorporateClientPage.AddressFrameId());

        //SeleniumDriverInstance.switchToDefaultContent();
        if (!enterAddressDetails(this.CorrospondenceAddressLine1)) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);
//        SeleniumDriverInstance.switchToDefaultContent();
//
//        SeleniumDriverInstance.clickElementbyXpath("/html/body/div[2]/div[1]/button/span[1]");
        return true;
    }

    private boolean enterBusinessAddress() {
        //SeleniumDriverInstance.switchToFrameById(EKunduCreateCorporateClientPage.AddressFrameId());
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EKunduCreateCorporateClientPage.AddresssTypeDropDownId(), "Business Address")) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        if (!enterAddressDetails(this.BusinessAddressLine1)) {
            return false;
        }

        SeleniumDriverInstance.switchToDefaultContent();

        SeleniumDriverInstance.clickElementbyXpath(EKunduCreateCorporateClientPage.CloseAddressDialogButtonXPath2());
        return true;
    }

    private boolean submitCorporateClientDetaills() {
       //SeleniumDriverInstance.scrollDownByOnePage("ctl00_cntMainBody_BankDetail_grdBankDetails_ctl02_btnBankDelete");
        //SeleniumDriverInstance.scrollToElement(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId());
        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.waitForElementById(EKunduCreateCorporateClientPage.EditCorporateClientCreationButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Corporate client details successfully submitted", false);
        return true;
    }

    private boolean enterBusinessDescription() {
        /*
         if(!SeleniumDriverInstance.clickElementbyPartialLinkText(EKunduCreateCorporateClientPage.AdditionalRegulatoryInformationTabPartialLinkText()))
         {
         return false;
         }
        
         if(!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.EtanaBusinessPolicyCheckBoxId()))
         {
         return false;
         }
         */
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EKunduCreateCorporateClientPage.BusinessDescriptionTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EKunduCreateCorporateClientPage.BusinessDescriptionTextBoxId(), this.BusinessDescription)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EKunduCreateCorporateClientPage.AdditionalRegulatoryInformationTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EKunduCreateCorporateClientPage.EtanaBusinessPolicyCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyLinkText(EKunduCreateCorporateClientPage.EBPTabLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EKunduCreateCorporateClientPage.EtanaMotorFleetCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyLinkText(EKunduCreateCorporateClientPage.EBPAggregatesTabLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EKunduCreateCorporateClientPage.MotorFleetTypeofPremiumDropDownListId(), testData.TestParameters.get("Motor Fleet Type of Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EKunduCreateCorporateClientPage.MotorFleetTotalFleetPremiumTextBoxId(), testData.TestParameters.get("Total Fleet Premium"))) {
            return false;
        }

        return true;
    }

    private boolean enterAddressDetails(String addressLine1) {
//
//       
//        SeleniumDriverInstance.switchToFrameById(EKunduCreateCorporateClientPage.AddressFrameId());

        if (!SeleniumDriverInstance.enterTextById(EKunduCreateCorporateClientPage.AddressLine1TextBoxId(), addressLine1)) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.ArrowdownToElementById(EKunduCreateCorporateClientPage.AddressLine1TextBoxId())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.AddressAddButtonId())) {
            return false;
        }
        return true;
    }

}
