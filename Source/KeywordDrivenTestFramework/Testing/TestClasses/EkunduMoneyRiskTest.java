
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduElectronicEquipmentRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMoneyRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduMoneyRiskTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduMoneyRiskTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyRisksDialogisPresent())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the Risk Dialog is present - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterIndustryDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Industry Details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Industry Details - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterRiskItemDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Risk Item Details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Risk Item Details - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterExtensions())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Extensions", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Extensions - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterEndorsements())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Endorsements", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Endorsements - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterNotes())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Notes", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Notes - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot("Money Risk details entered successfully", false);

        return new TestResult(testData, Enums.ResultStatus.PASS,
                              "Money Risk details entered successfully" + SeleniumDriverInstance.DriverExceptionDetail,
                              this.getTotalExecutionTime());

      }

     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
  
    private boolean verifyRisksDialogisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectMoneyRiskType();

            return true;

          }
        else if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectMoneyRiskType();

            return true;
          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

      }

    private boolean selectMoneyRiskType()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(10000);
        SeleniumDriverInstance.takeScreenShot("Money Risk type selected successfully", false);

        return true;
      }

    private boolean enterIndustryDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(),
                this.getData("Industry")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.IndustrySearchResultsSelectedRowId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Industry details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduMoneyRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterRiskItemDetails()
      {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMoneyRiskPage.RiskItemAlarmWarrantyDropdownListId(),
                this.getData("Risk Item Alarm Warranty")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMoneyRiskPage.RiskItemTypeofSafeDropdownListId(),
                this.getData("Risk Item Type of Safe")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.RiskItemCoverMajorLimitSumInsuredTextboxId(),
                this.getData("Cover - Major Limit - Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMoneyRiskPage.RiskItemMinorLimitsInsuredPremisesCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.RiskItemMinorLimitsInsuredPremisesNumberofPeopleTextboxId(),
                this.getData("Minor Limits - Insured Premises - Number of People")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.RiskItemMinorLimitsInsuredPremisesLimitofIndemnityTextboxId(),
                this.getData("Minor Limits - Insured Premises - Limit of Indemnity")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.RiskItemMinorLimitsInsuredPremisesRateTextboxId(),
                this.getData("Minor Limits - Insured Premises - Agreed Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMoneyRiskPage.RiskRateButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk Item details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduMoneyRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterExtensions()
      {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMoneyRiskPage.ExtensionsAdditionalClaimsCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.ExtensionsAdditionalClaimsLimitofIndemnityTextboxId(),
                this.getData("Extensions - Limit of Indemnity")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.ExtensionsAdditionalClaimsRateTextboxId(),
                this.getData("Extensions - Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMoneyRiskPage.ExtensionsClothingCheckboxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.ExtensionsClothingRateTextboxId(),
                this.getData("Extensions - Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.ExtensionsClothingPremiumTextboxId(),
                this.getData("Extensions - Premium")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.ExtensionsClothingFAPPercentageTextboxId(),
                this.getData("Extensions - FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.ExtensionsClothingFAPAmountTextboxId(),
                this.getData("Extensions - FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMoneyRiskPage.ExtensionsDeathCheckboxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.ExtensionsDeathLImitofIndemnityboxId(),
                this.getData("Extensions - Limit of Indemnity")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMoneyRiskPage.ExtensionsLockandKeysCheckboxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.ExtensionsLockandKeysRateTextboxId(),
                this.getData("Extensions - Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.ExtensionsLockandKeysPremiumTextboxId(),
                this.getData("Extensions - Premium")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.ExtensionsLockandKeysFAPPercentageTextboxId(),
                this.getData("Extensions - FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.ExtensionsLockandKeysFAPAmountTextboxId(),
                this.getData("Extensions - FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMoneyRiskPage.ExtensionsMedicalExpensesCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.ExtensionsMedicalExpensesLImitofIndemnityTextboxId(),
                this.getData("Extensions - Limit of Indemnity")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.ExtensionsPersonalAccidentNumberofEmployeesboxId(),
                this.getData("Exensions - Number of Employees")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMoneyRiskPage.ExtensionsRiotandStrikeCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMoneyRiskPage.ExtensionsTotalTemporalDisabilityCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.ExtensionsTotalTemporalDisabilityLImitofIndemnityTextboxId(),
                this.getData("Exensions - Limit of Indemnity")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.ExtensionsTotalTemporalDisabilityNumberofWeeksDropdownListId(),
                this.getData("Extensions - Number of Weeks")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Extensions entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduMoneyRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterEndorsements()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduMoneyRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMoneyRiskPage.EndorsementsSelectButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMoneyRiskPage.EndorsementsAddAllButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMoneyRiskPage.EndorsementsApplyButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduMoneyRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterNotes()
      {
        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskCommentsTextboxId(),
                this.getData("Risk Notes")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskPolicyNotesTextboxId(),
                this.getData("Risk Notes")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Comments entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(1000);
        
        SeleniumDriverInstance.clickElementbyLinkText("Documents");
        
        SeleniumDriverInstance.pause(1000);
        
         SeleniumDriverInstance.clickElementById("btnGeneratePdf");
         
         SeleniumDriverInstance.pause(1000);
//         
         SeleniumDriverInstance.clickElementById("popup_ok");
//         
//         
//         SeleniumDriverInstance.pause(3000);
//         
         SeleniumDriverInstance.clickElementbyLinkText("Risks");
        
        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
