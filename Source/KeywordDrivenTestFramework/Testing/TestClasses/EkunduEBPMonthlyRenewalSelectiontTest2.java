
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;

/**
 *
 * @author ferdinandN
 */
public class EkunduEBPMonthlyRenewalSelectiontTest2 extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduEBPMonthlyRenewalSelectiontTest2(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToPolicyRenewalPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the Renewal Selection page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the Renewal Selection page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findPolicyAndCopyToRenewalCycle())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to copy policy to renewal cycle", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to copy policy to renewal cycle- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!navigateToClientDetailsPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to find policy", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find policy- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterPolicyRenewalData())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter policy renewal data", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter policy renewal data- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!makePolicyLive())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to finalise policy", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to finalise policy- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS,
                              "Policy renewal for EBP monthly completed successfully "
                              + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());

      }

    private boolean navigateToPolicyRenewalPage()
      {
        if(!SeleniumDriverInstance.navigateToFindClientPage(EKunduHomePage.PolicyHoverTabId(),"Renewal Selection"))
        {
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementById(EKunduHomePage.RenewalSelectionDivId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Navigation to the Renewal Selection page was successful", false);

        return true;
      }

     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
    
    private boolean findPolicyAndCopyToRenewalCycle()
      {
        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                                  "Retrieved Policy Number");

        if (policyNumber.equals("parameter not found"))
          {
            policyNumber = testData.TestParameters.get("Policy Number");
          }
        else
          {
            testData.updateParameter("Policy Number", policyNumber);
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PolicyReferenceTextBoxId(), policyNumber))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FindNowButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.RenewalSelectionSelectPolicyLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Policy Found", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RenewalSelectionSelectPolicyLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(10000);

        return true;

      }

    private boolean navigateToClientDetailsPage()
      {
        if(!SeleniumDriverInstance.navigateToFindClientPage(EkunduViewClientDetailsPage.ClientHoverTabId(),"Find Client"))
        {
            return false;
        }

        SeleniumDriverInstance.pause(3000);
        this.findClient();

        return true;
      }
    
    private boolean findClient()
      {
        String clientCode =
            SeleniumDriverInstance.retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                "Retrieved Client Code");

        if (clientCode.equals("parameter not found"))
          {
            clientCode = testData.TestParameters.get("Client Code");
          }
        else
          {
            testData.updateParameter("Client Code", clientCode);
          }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterPolicyRenewalData()
      {
        if (!SeleniumDriverInstance.clickElementbyLinkText("Policies"))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementbyLinkText("Details"))
          {
            return false;
          }

        return true;
      }

    private boolean enterPolicyHeaderData()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PolicyHeaderTabId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.BusinessSourceDropDownListId(),
                this.getData("Business Source")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.PaymentTermDropDownListId(),
                this.getData("Payment Term")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Policy details updated successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean QuoteBusinessAllRisk()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.MTAEditBusinessAllRiskLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;

      }

    private boolean makePolicyLive()
      {
        SeleniumDriverInstance.pause(3000);
       

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.MakeLiveButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.acceptAlertDialog();

        SeleniumDriverInstance.pause(10000);

        return true;

      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
