/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author ferdinandN
 */
public class EkunduExtendedPersonalLiabilityRiskTest extends BaseClass 
{
    TestEntity testData;
    TestResult testResult;

    public EkunduExtendedPersonalLiabilityRiskTest(TestEntity testData)
    {
        this.testData = testData;

    }
    
    public TestResult executeTest()
    {
        this.setStartTime();

        if (!verifyRisksDialogisPresent())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the riss dialog is present"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the riss dialog is present- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }
        
        if (!enterEndorsements())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk  endorsements"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk endorsements- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterNotes())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk notes"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk notes- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }
        
        SeleniumDriverInstance.takeScreenShot(("Extended Personal Liability risk added successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Extended Personal Liability risk added successfully",
                              this.getTotalExecutionTime());
        
      }
    
    private boolean verifyRisksDialogisPresent()
    {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            selectExtendedPersonalLiabilityRiskType();

            return true;

          }
        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectExtendedPersonalLiabilityRiskType();

            return true;
          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

      }

    private boolean selectExtendedPersonalLiabilityRiskType()
    {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(testData.TestParameters.get("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Extended Personal Liability Risk type selected successfully", false);

        return true;
      }
              
    private boolean enterEndorsements()
    {
          if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementNextButtonId()))
          {
            return false;
          }
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementsSelectButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduHomePage.EndorsementsAddAllButtonID()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EKunduHomePage.EndorsementsApplyButtonID()))
          {
            return false;
          }
        
        SeleniumDriverInstance.pause(3000);
        
        //this.editEndorsementDocument();

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementNextButtonId()))
          {
            return false;
          }

        return true;
      }
    
    private boolean editEndorsementDocument()
    {
        if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EditPersonalLiabilityEndorsementsLinkId()))
         {
             return false;
         }
        
        SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId());
        
        SeleniumDriverInstance.pause(3000);
        
        SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.DocumentEditorFrameId());
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EndorsementsBodyId(),
                ("Extended Personal Liability Risk")))
          {
            return false;
          }
        
        SeleniumDriverInstance.switchToDefaultContent();
        
        SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId());
     
         SeleniumDriverInstance.pause(10000);
      
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementsSaveButtonId()))
         {
             return false;
         }
        
        SeleniumDriverInstance.switchToDefaultContent();
        
        return true;
        
    }
        
    private boolean enterNotes()
    {

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.PersonalAccidentNotesTextBoxId(),
                testData.TestParameters.get("Comments")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalAccidentNextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;
      }
}
