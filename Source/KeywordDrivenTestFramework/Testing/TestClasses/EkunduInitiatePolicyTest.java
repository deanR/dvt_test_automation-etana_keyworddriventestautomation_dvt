
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

/**
 *
 * @author FerdinandN
 */
public class EkunduInitiatePolicyTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduInitiatePolicyTest(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToFindClientPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find client page", true);

            return new TestResult(testData,  Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the find client page - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!verifyFindClientPageHasLoaded())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Find Client page has loaded", true);

            return new TestResult(testData,  Enums.ResultStatus.FAIL,
                                  "Failed to verify that the Find Client page has loaded - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findClient())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to find client", true);

            return new TestResult(testData,  Enums.ResultStatus.FAIL,
                                  "Failed to find client - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!verifyClientDetailsPageHasLoaded())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to detect client details page", true);

            return new TestResult(testData,  Enums.ResultStatus.FAIL,
                                  "Failed to detect client details page - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!beginNewQuoteCreation())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to create new quote", true);

            return new TestResult(testData,  Enums.ResultStatus.FAIL,
                                  "Failed to create new quote - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterSummaryDetails())
          {
            return new TestResult(testData,  Enums.ResultStatus.FAIL,
                                  "Failed to enter summary details - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        return new TestResult(testData,  Enums.ResultStatus.PASS, "Client policy successfully Initiated", this.getTotalExecutionTime());
      }

  

    private boolean navigateToFindClientPage()
      {
          
        if(!SeleniumDriverInstance.navigateToFindClientPage(EkunduViewClientDetailsPage.ClientHoverTabId(),"Find Client"))
        {
            return false;
        }
        


        return true;
      }

    private boolean verifyFindClientPageHasLoaded()
      {
        SeleniumDriverInstance.takeScreenShot("Navigation to the Find Client page was successful", false);

        if (!SeleniumDriverInstance.validateElementTextValueById(EKunduFindClientPage.PageHeaderSpanId(),
                "Search Criteria"))
          {
            return false;
          }
        else
          {
            SeleniumDriverInstance.takeScreenShot("Find Client page was successfully loaded", false);
          }

        return true;
      }

    private boolean findClient()
      {
        String clientCode =
            SeleniumDriverInstance.retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                "Retrieved Client Code");

        if (clientCode.equals("parameter not found"))
          {
            clientCode = testData.TestParameters.get("Client Code");
          }
        else
          {
            testData.updateParameter("Client Code", clientCode);
          }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
          {
            return false;
          }

        return true;
      }

    private boolean verifyClientDetailsPageHasLoaded()
      {
        if (!SeleniumDriverInstance.validateElementTextValueById(EkunduViewClientDetailsPage.ViewClientDetailsLabelId(),
                "View Client Details"))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Details page successfully loaded", false);

        return true;
      }
    
     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean beginNewQuoteCreation()
      {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.SelectProductDropDownListId(),
                getData("Product")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NewQuoteButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(1000);
        SeleniumDriverInstance.takeScreenShot("Begin new ", false);

        return true;
      }

    private boolean enterSummaryDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AgentCodeButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.FindAgentDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.AgentCodeTextBoxId(),
                testData.TestParameters.get("Agent Code")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchAgentsButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);

//      if(!SeleniumDriverInstance.doubleClickElementbyLinkText("select"))
//      {
//          return false;
//      }

        SeleniumDriverInstance.clickElementById("ctl01_cntMainBody_grdvSearchResults_ctl02_lnkbtnSelect");

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        // add id to page objects + add entry to input file for agent code.

        String agentCodeTextbox = SeleniumDriverInstance.retrieveTextById("ctl00_cntMainBody_POLICYHEADER__AGENTCODE");
        int pollCount = 0;

        while ((agentCodeTextbox == "") && (pollCount < ApplicationConfig.WaitTimeout()))
          {
            SeleniumDriverInstance.pause(1000);
            agentCodeTextbox = SeleniumDriverInstance.retrieveTextById("ctl00_cntMainBody_POLICYHEADER__AGENTCODE");
            pollCount++;
          }

        System.out.println("[INFO] Agent code entered as - " + agentCodeTextbox);

        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.CoverStartDateTextBoxId());

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.CoverStartDateTextBoxId(),
                this.getData("Cover Start Date")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.PaymentTermDropDownListId(),
                this.getData("Payment Term")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.CollectionFrequencyDropDownListId(),
                this.getData("Collection Frequency")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.CoverStartDateTextBoxId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Summary details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
