/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduEHATBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduElectronicEquipmentRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author ferdinandN
 */
public class EkunduEBPBusinessAllRiskMonthlyTest extends BaseClass {

    TestEntity testData;
    TestResult testResultt;

    public EkunduEBPBusinessAllRiskMonthlyTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk button is present", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the add risk button is present - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterRiskDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter risk details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterFirstRiskItemDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter 1st risk item details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter 1st risk item details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterSecondRiskItemDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter 2nd risk item details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter 2nd risk item details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterThirdRiskItemDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter 3rd risk item details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter 3rd risk item details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterFourthRiskItemDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter 4th risk item details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter 4th risk item details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterFifthRiskItemDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter 5th risk item details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter 5th risk item details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterExtensions()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk extensions", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter risk extensions  - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterEndorsements()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk endorsements", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter risk endorsements - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterNotes()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk notes", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter risk notes - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        SeleniumDriverInstance.takeScreenShot("Business All Risk details entered successfully", false);
        return new TestResult(testData, Enums.ResultStatus.PASS, "Business All Risk details entered successfully", this.getTotalExecutionTime());

    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean verifyAddRiskButtonisPresent() {
        if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            selectBusinessAllRisk();
            return true;
        } else if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectBusinessAllRisk();
            return true;
        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");
            return false;
        }

    }

    private boolean selectBusinessAllRisk() {

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.pause(10000);

        SeleniumDriverInstance.takeScreenShot("Business All Risk selected sucessfully", false);
        return true;
    }

    private boolean enterRiskDetails() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduBusinessAllRiskPage.RiskItemsAlarmWarrantyDropdownListId(), this.getData("Alarm Warranty"))) {
            return false;
        }

        return true;

    }

    private boolean enterFirstRiskItemDetails() {
        SeleniumDriverInstance.pause(1000);
//       SeleniumDriverInstance.Driver.navigate().refresh();
//        SeleniumDriverInstance.pause(3000);
//        
//        if(!SeleniumDriverInstance.clickElementbyXpath(EkunduBusinessAllRiskPage.AddRiskItemButtonXpath()))
//        {
//            return false;
//        }

        if (!SeleniumDriverInstance.clickElementbyName(EkunduBusinessAllRiskPage.RiskItemsAddRiskItemsButtonName())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        //this.clickAddRiskItemButton();

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduBusinessAllRiskPage.BusinessAllRiskItemCategoryDropdownListId(), this.getData("Item 1 Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemSumInsuredTextboxId(), this.getData("Item 1 Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemDescriptionTextboxId(), this.getData("Item 1 Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.EBPItemDetailSerialNumberTextboxId(), this.getData("Item 1 Serial Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemRateTextboxId(), this.getData("Item 1 Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.EBPItemDetailMaxRITextboxId(), this.getData("Item 1 Max RI Exposure"))) {
            return false;
        }

        //extensions
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsReplacementCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsIncreasedCostCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsFAPCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemExtensionsFAPMinPercTextboxId(), this.getData("Business All Risk Item 1 - FAP - Minimum Perc"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemExtensionsFAPMinAmountTextboxId(), this.getData("Business All Risk Item 1 - FAP - Minimum Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemFAPMaxAmountTextboxId(), this.getData("Business All Risk Item 1 - FAP - Maximum Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumTextboxId(), this.getData("Item 1 Flat Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskRateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(3000);
//        SeleniumDriverInstance.Driver.navigate().refresh();
//        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementbyName(EkunduEHATBusinessAllRiskPage.EBPInterestedPartiesAddButtonName())) {
            return false;
        }

        //this.clickAddRiskItemButton(); 
        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.InterestedPartiesNameTextboxId(), this.getData("Interested Party Name 1"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemNotesTextboxId(), this.getData("Business All Risk Item Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemPolicyNotesTextboxId(), this.getData("Business All Risk Item Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Business All Risk Item number 1 added successfully", false);

        return true;

    }

    private boolean enterSecondRiskItemDetails() {
        SeleniumDriverInstance.pause(1000);
//        SeleniumDriverInstance.Driver.navigate().refresh();
//        SeleniumDriverInstance.pause(3000);

//        if (!SeleniumDriverInstance.clickElementbyXpath(EkunduBusinessAllRiskPage.AddRiskItemButtonXpath())) {
//            return false;
//        }
        //this.clickAddRiskItemButton();
        if (!SeleniumDriverInstance.clickElementbyName(EkunduBusinessAllRiskPage.BusinessAllRiskItemAddItemButtonName())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        //SeleniumDriverInstance.clickElementbyName("ctl00$cntMainBody$BAR__ITEM$ctl03$ctl00");
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduBusinessAllRiskPage.BusinessAllRiskItemCategoryDropdownListId(), this.getData("Item 2 Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemSumInsuredTextboxId(), this.getData("Item 2 Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemDescriptionTextboxId(), this.getData("Item 2 Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.EBPItemDetailSerialNumberTextboxId(), this.getData("Item 2 Serial Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemRateTextboxId(), this.getData("Item 2 Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.EBPItemDetailMaxRITextboxId(), this.getData("Item 2 Max RI Exposure"))) {
            return false;
        }

        //extensions
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsReplacementCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsIncreasedCostCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsFAPCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemExtensionsFAPMinPercTextboxId(), this.getData("Business All Risk Item 2 - FAP - Minimum Perc"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemExtensionsFAPMinAmountTextboxId(), this.getData("Business All Risk Item 2 - FAP - Minimum Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemFAPMaxAmountTextboxId(), this.getData("Business All Risk Item 2 - FAP - Maximum Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumTextboxId(), this.getData("Item 2 Flat Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskRateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(3000);
//        SeleniumDriverInstance.Driver.navigate().refresh();
//        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementbyName(EkunduEHATBusinessAllRiskPage.EBPInterestedPartiesAddButtonName())) {
            return false;
        }

        //this.clickAddRiskItemButton(); 
        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.InterestedPartiesNameTextboxId(), this.getData("Interested Party Name 2"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemNotesTextboxId(), this.getData("Business All Risk Item Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemPolicyNotesTextboxId(), this.getData("Business All Risk Item Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Business All Risk Item number 2 added successfully", false);

        return true;

    }

    private boolean enterThirdRiskItemDetails() {
        SeleniumDriverInstance.pause(1000);
//       SeleniumDriverInstance.Driver.navigate().refresh();
//        SeleniumDriverInstance.pause(3000);

//        if (!SeleniumDriverInstance.clickElementbyXpath(EkunduBusinessAllRiskPage.AddRiskItemButtonXpath())) {
//            return false;
//        }
        if (!SeleniumDriverInstance.clickElementbyName(EkunduBusinessAllRiskPage.BusinessAllRiskItemAddItemButtonName2())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduBusinessAllRiskPage.BusinessAllRiskItemCategoryDropdownListId(), this.getData("Item 3 Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemSumInsuredTextboxId(), this.getData("Item 3 Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemDescriptionTextboxId(), this.getData("Item 3 Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.EBPItemDetailSerialNumberTextboxId(), this.getData("Item 3 Serial Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemRateTextboxId(), this.getData("Item 3 Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.EBPItemDetailMaxRITextboxId(), this.getData("Item 3 Max RI Exposure"))) {
            return false;
        }

        //extensions
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsReplacementCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsIncreasedCostCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsFAPCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemExtensionsFAPMinPercTextboxId(), this.getData("Business All Risk Item 3 - FAP - Minimum Perc"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemExtensionsFAPMinAmountTextboxId(), this.getData("Business All Risk Item 3 - FAP - Minimum Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemFAPMaxAmountTextboxId(), this.getData("Business All Risk Item 3 - FAP - Maximum Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumTextboxId(), this.getData("Item 3 Flat Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskRateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(3000);
//        SeleniumDriverInstance.Driver.navigate().refresh();
//        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementbyName(EkunduEHATBusinessAllRiskPage.EBPInterestedPartiesAddButtonName())) {
            return false;
        }

        //this.clickAddRiskItemButton(); 
        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.InterestedPartiesNameTextboxId(), this.getData("Interested Party Name 3"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemNotesTextboxId(), this.getData("Business All Risk Item Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemPolicyNotesTextboxId(), this.getData("Business All Risk Item Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Business All Risk Item number 3 added successfully", false);

        return true;

    }

    private boolean enterFourthRiskItemDetails() {
        SeleniumDriverInstance.pause(1000);
//       SeleniumDriverInstance.Driver.navigate().refresh();
//        SeleniumDriverInstance.pause(3000);

//        if (!SeleniumDriverInstance.clickElementbyXpath(EkunduBusinessAllRiskPage.AddRiskItemButtonXpath())) {
//            return false;
//        }
        if (!SeleniumDriverInstance.clickElementbyName(EkunduBusinessAllRiskPage.BusinessAllRiskItemAddItemButtonName4())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduBusinessAllRiskPage.BusinessAllRiskItemCategoryDropdownListId(), this.getData("Item 4 Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemSumInsuredTextboxId(), this.getData("Item 4 Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemDescriptionTextboxId(), this.getData("Item 4 Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.EBPItemDetailSerialNumberTextboxId(), this.getData("Item 4 Serial Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemRateTextboxId(), this.getData("Item 4 Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.EBPItemDetailMaxRITextboxId(), this.getData("Item 4 Max RI Exposure"))) {
            return false;
        }

        //extensions
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsReplacementCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsIncreasedCostCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsFAPCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemExtensionsFAPMinPercTextboxId(), this.getData("Business All Risk Item 4 - FAP - Minimum Perc"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemExtensionsFAPMinAmountTextboxId(), this.getData("Business All Risk Item 4 - FAP - Minimum Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemFAPMaxAmountTextboxId(), this.getData("Business All Risk Item 4 - FAP - Maximum Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumTextboxId(), this.getData("Item 4 Flat Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskRateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(3000);
//        SeleniumDriverInstance.Driver.navigate().refresh();
//        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementbyName(EkunduEHATBusinessAllRiskPage.EBPInterestedPartiesAddButtonName())) {
            return false;
        }

        //this.clickAddRiskItemButton(); 
        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.InterestedPartiesNameTextboxId(), this.getData("Interested Party Name 4"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemNotesTextboxId(), this.getData("Business All Risk Item Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemPolicyNotesTextboxId(), this.getData("Business All Risk Item Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Business All Risk Item number 4 added successfully", false);

        return true;

    }

    private boolean enterFifthRiskItemDetails() {
        SeleniumDriverInstance.pause(1000);
//        SeleniumDriverInstance.Driver.navigate().refresh();
//        SeleniumDriverInstance.pause(3000);

//        if (!SeleniumDriverInstance.clickElementbyXpath(EkunduBusinessAllRiskPage.AddRiskItemButtonXpath())) {
//            return false;
//        }
        if (!SeleniumDriverInstance.clickElementbyName(EkunduBusinessAllRiskPage.BusinessAllRiskItemAddItemButtonName5())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduBusinessAllRiskPage.BusinessAllRiskItemCategoryDropdownListId(), this.getData("Item 5 Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemSumInsuredTextboxId(), this.getData("Item 5 Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemDescriptionTextboxId(), this.getData("Item 5 Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.EBPItemDetailSerialNumberTextboxId(), this.getData("Item 5 Serial Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemRateTextboxId(), this.getData("Item 5 Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.EBPItemDetailMaxRITextboxId(), this.getData("Item 5 Max RI Exposure"))) {
            return false;
        }

        //extensions
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsReplacementCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsIncreasedCostCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsFAPCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemExtensionsFAPMinPercTextboxId(), this.getData("Business All Risk Item 5 - FAP - Minimum Perc"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemExtensionsFAPMinAmountTextboxId(), this.getData("Business All Risk Item 5 - FAP - Minimum Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemFAPMaxAmountTextboxId(), this.getData("Business All Risk Item 5 - FAP - Maximum Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumTextboxId(), this.getData("Item 5 Flat Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskRateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(3000);
//        SeleniumDriverInstance.Driver.navigate().refresh();
//        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementbyName(EkunduEHATBusinessAllRiskPage.EBPInterestedPartiesAddButtonName())) {
            return false;
        }

        //this.clickAddRiskItemButton(); 
        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.InterestedPartiesNameTextboxId(), this.getData("Interested Party Name 5"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemNotesTextboxId(), this.getData("Business All Risk Item Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemPolicyNotesTextboxId(), this.getData("Business All Risk Item Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Business All Risk Item number 5 added successfully", false);

        return true;

    }

    private boolean enterExtensions() {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.RiskItemsExtensionsAdditionalClaimsCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.RiskItemsExtensionsRiotandStrikeCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.RiskItemsExtensionsAdditionalClaimsLimitofIndemnityTextboxId(), this.getData("Extensions Limit Of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.RiskItemsExtensionsAdditionalClaimsRateTextboxId(), this.getData("Extensions Rate"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Extensions entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduEHATBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduEHATBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterEndorsements() {
        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EndorsementsSelectButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EndorsementsAddAllButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EndorsementsApplyButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        return true;

    }

    private boolean enterNotes() {
        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskCommentsTextboxId(), this.getData("Business All Risk Policy Comments"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskPolicyNotesTextboxId(), this.getData("Business All Risk Policy Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Comments entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        return true;
    }

    private boolean clickAddRiskItemButton() {
        WebElement cell = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_BAR__ITEM\"]/table/tfoot/tr/td[7]/input[@value = \"Add\"]"));
        if (cell != null) {
            cell.click();
            System.out.println("[TestInfo] Add Risk Item Button Clicked");
            return true;
        } else {
            System.err.println("[Error] Failed to click add risk item button");
            return false;
        }

    }

}
