
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduStatedBenefitsRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduStatedBenefitsRiskTest extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public EkunduStatedBenefitsRiskTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the add risk button is present ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify that the add risk button is present - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterInsuredPersons()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter insured persons", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter insured persons - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterExtensions()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter extensions", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter extensions - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterNotes()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter notes", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter notes - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        SeleniumDriverInstance.takeScreenShot("Stated Benefits risk entered successfully", false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Stated Benefits risk entered successfully",
                this.getTotalExecutionTime());

    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean verifyAddRiskButtonisPresent() {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectStatedBenefitsRisk();

            return true;
        } else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            selectStatedBenefitsRisk();
        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err
                    .println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");

            return false;
        }

        return true;
    }

    private boolean selectStatedBenefitsRisk() {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.pause(10000);
        SeleniumDriverInstance.takeScreenShot("Stated Benefits risk selected sucessfully", false);

        return true;
    }

    private boolean enterInsuredPersons() {
        if (!SeleniumDriverInstance.clickElementById(EkunduStatedBenefitsRiskPage.RiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.RiskItemsReinsuranceLimitofLiabilityTextboxId(),
                this.getData("Stated Benefits - Reinsurance Limit of Liability"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(
                EkunduStatedBenefitsRiskPage.RiskItemsInsuredPersonsAddButtonXpath())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.EmployeeDetailsTotalAnnualEarningsTextboxId(),
                this.getData("Employee Details - Total Annual Earnings"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduStatedBenefitsRiskPage.EmployeeDetailsOccupationDropdownListId(),
                this.getData("Employee Details - Occupation"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.EmployeeDetailsOccupationDescriptionTextboxId(),
                this.getData("Employee Details - Occupation Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduStatedBenefitsRiskPage.EmployeeDetailsNameTextboxId(),
                this.getData("Employee Details - Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduStatedBenefitsRiskPage.EmployeeDetailsNumberofYearsTextboxId(),
                this.getData("Employee Details - Number of Years"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.EmployeeDetailsCoverDeathLimitofIndemnityTextboxId(),
                this.getData("Employee Details - Cover - Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.EmployeeDetailsCoverDeathAgreedRateTextboxId(),
                this.getData("Employee Details - Cover - Agreed Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduStatedBenefitsRiskPage.EmployeeDetailsCoverPermanentDisabilityCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.EmployeeDetailsCoverPermanentDisabilityAgreedRateTextboxId(),
                this.getData("Employee Details - Cover - Agreed Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduStatedBenefitsRiskPage.EmployeeDetailsCoverTTDCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.EmployeeDetailsCoverTTDLimitofIndemnityTextboxId(),
                this.getData("Employee Details - Cover - Limit of Indemnity"))) {
            return false;
        }

        SeleniumDriverInstance
                .WaitUntilDropDownListPopulatedById(EkunduStatedBenefitsRiskPage
                        .EmployeeDetailsCoverTTDNumberofWeeksDropdownListId());

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingName(
                EkunduStatedBenefitsRiskPage.EmployeeDetailsCoverTTDNumberofWeeksDropdownListId(), "52 weeks")) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.EmployeeDetailsCoverTTDAgreedRateTextboxId(),
                this.getData("Employee Details - Cover - Agreed Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduStatedBenefitsRiskPage.EmployeeDetailsCoverMedicalExpensesCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduStatedBenefitsRiskPage.EmployeeDetailsCoverMedicalExpensesLimitofIndemnityDropdownListId(),
                this.getData("Employee Details - Cover - Medical Expenses Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.EmployeeDetailsCoverMedicalExpensesAgreedRateTextboxId(),
                this.getData("Employee Details - Cover - Agreed Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.EmployeeDetailsCoverMedicalExpensesFAPAmountTextboxId(),
                this.getData("Employee Details - Cover - Medical Expenses FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduStatedBenefitsRiskPage.RiskRateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Ensured Persons entered sucessfully", true);

        if (!SeleniumDriverInstance.clickElementById(EkunduStatedBenefitsRiskPage.RiskNextButtonId())) {
            return false;
        }

        return true;

    }

    private boolean enterExtensions() {
        if (!SeleniumDriverInstance.clickElementById(EkunduStatedBenefitsRiskPage.RiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduStatedBenefitsRiskPage.ExtensionsBodyTransportationCheckboxId(), true)) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.ExtensionsBodyTransportationRateTextboxId(),
                this.getData("Extensions Rate"))) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.ExtensionsBodyTransportationPremiumTextboxId(),
                this.getData("Extensions Premium"))) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.ExtensionsBodyTransportationFAPPercentageTextboxId(),
                this.getData("Extensions - FAP Percentage"))) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.ExtensionsBodyTransportationFAPAmountTextboxId(),
                this.getData("Extensions - FAP Amount"))) {
            return true;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduStatedBenefitsRiskPage.ExtensionsDisappearanceCheckboxId(), true)) {
            return true;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduStatedBenefitsRiskPage.ExtensionsFuneralCostsCheckboxId(), true)) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduStatedBenefitsRiskPage.ExtensionsFuneralCostsRateTextboxId(),
                this.getData("Extensions Rate"))) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.ExtensionsFuneralCostsPremiumTextboxId(),
                this.getData("Extensions Premium"))) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.ExtensionsFuneralCostsFAPPercentageTextboxId(),
                this.getData("Extensions - FAP Percentage"))) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.ExtensionsFuneralCostsFAPAmountTextboxId(),
                this.getData("Extensions - FAP Amount"))) {
            return true;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduStatedBenefitsRiskPage.ExtensionsRepatriationCostsCheckboxId(), true)) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.ExtensionsRepatriationCostsRateTextboxId(),
                this.getData("Extensions Rate"))) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.ExtensionsRepatriationCostsPremiumTextboxId(),
                this.getData("Extensions Premium"))) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.ExtensionsRepatriationCostsFAPPercentageTextboxId(),
                this.getData("Extensions - FAP Percentage"))) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.ExtensionsRepatriationCostsFAPAmountTextboxId(),
                this.getData("Extensions - FAP Amount"))) {
            return true;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduStatedBenefitsRiskPage.ExtensionsSeriousIllnessCheckboxId(), true)) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduStatedBenefitsRiskPage.ExtensionsSeriousIllnessRateTextboxId(),
                this.getData("Extensions Rate"))) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.ExtensionsSeriousIllnessPremiumTextboxId(),
                this.getData("Extensions Premium"))) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.ExtensionsSeriousIllnessFAPPercentageTextboxId(),
                this.getData("Extensions - FAP Percentage"))) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.ExtensionsSeriousIllnessFAPAmountTextboxId(),
                this.getData("Extensions - FAP Amount"))) {
            return true;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduStatedBenefitsRiskPage.ExtensionsSicknessCheckboxId(),
                true)) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.ExtensionsSicknessLimitofIndemnityTextboxId(),
                this.getData("Extensions - Sickness Limit of Indemnity"))) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduStatedBenefitsRiskPage.ExtensionsSicknessRateTextboxId(),
                this.getData("Extensions Rate"))) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduStatedBenefitsRiskPage.ExtensionsSicknessFAPPercentageTextboxId(),
                this.getData("Extensions - FAP Percentage"))) {
            return true;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduStatedBenefitsRiskPage.ExtensionsSicknessFAPAmountTextboxId(),
                this.getData("Extensions - FAP Amount"))) {
            return true;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduStatedBenefitsRiskPage.ExtensionsTerrorismCheckboxId(),
                true)) {
            return true;
        }

        SeleniumDriverInstance.takeScreenShot("Extensions entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduStatedBenefitsRiskPage.RiskNextButtonId())) {
            return true;
        }

        return true;

    }

    private boolean enterNotes() {
        if (!SeleniumDriverInstance.clickElementById(EkunduStatedBenefitsRiskPage.RiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduStatedBenefitsRiskPage.RiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduStatedBenefitsRiskPage.RiskCommentsTextboxId(),
                this.getData("Risk Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduStatedBenefitsRiskPage.RiskAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduStatedBenefitsRiskPage.RiskPolicyNotesTextboxId(),
                this.getData("Risk Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduStatedBenefitsRiskPage.RiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance
                .clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Documents");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementById("btnGeneratePdf");

//         SeleniumDriverInstance.pause(3000);
//         
        SeleniumDriverInstance.clickElementById("popup_ok");
//         

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Risks");

        return true;

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
