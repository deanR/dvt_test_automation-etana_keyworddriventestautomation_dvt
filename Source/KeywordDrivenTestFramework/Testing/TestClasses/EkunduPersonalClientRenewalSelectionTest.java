
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author deanR
 */
public class EkunduPersonalClientRenewalSelectionTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduPersonalClientRenewalSelectionTest(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToPolicyRenewalPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the Renewal Selection page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the Renewal Selection page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findPolicy())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to find policy", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find policy- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!navigateToRenewalManagerPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the Renewal Manager page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the Renewal Manager page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterRenewalManagerData())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Renewal Manager data", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Renewal Manager data- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Renewal selection completed succesfully", this.getTotalExecutionTime());
      }

    private boolean navigateToPolicyRenewalPage()
      {
          if(!SeleniumDriverInstance.navigateTo(EKunduHomePage.PolicyHoverTabId(),"Renewal Selection"))
        {
            return false;
        }
          
//        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.PolicyHoverTabId(),
//                EKunduHomePage.RenewalSelectionTabXPath()))
//          {
//            return false;
//          }

        if (!SeleniumDriverInstance.waitForElementById(EKunduHomePage.RenewalSelectionDivId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Navigation to the Renewal Selection page was successful", false);

        return true;
      }

    private boolean findPolicy()
      {
        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase1"),
                                  "Policy Reference Number");

        if (policyNumber.equals("parameter not found"))
          {
            policyNumber = testData.TestParameters.get("PolicyRef");
            System.err.println("Failed to retrieve Policy Number");
          }
        else
          {
            testData.addParameter("Policy Reference Number", policyNumber);
            testData.updateParameter("PolicyRef", policyNumber);
            System.out.println("Policy Number retrieved - " + policyNumber);
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PolicyReferenceTextBoxId(),
                testData.TestParameters.get("PolicyRef")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FindNowButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.RenewalSelectionSelectPolicyLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Policy Found", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RenewalSelectionSelectPolicyLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(20000);

        return true;
      }

    private boolean navigateToRenewalManagerPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.PolicyHoverTabId(),
                EKunduHomePage.RenewalManagerTabXPath()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Navigation to Renewal Manager page was  successful", false);

        return true;
      }

    private boolean enterRenewalManagerData()
      {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.RenewalManagerRenewalStatusDropDownListId(),
                this.getData("Renewal Status")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.RenewalManagerProductTypeDropDownListId(),
                this.getData("Renewal Product Type")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.RenewalManagerBranchDropDownListId(),
                this.getData("Renewal Branch Code")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RenewalManagerSearchButtonId()))
          {
            return false;
          }

//      if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RenewalManagerCheckPolicyCheckBoxId(), true))
//      {
//          return false;
//      }

        if (!SeleniumDriverInstance.checkRenewalManagerPolicy(this.retrievePolicyNumber()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RenewalManagerStatusButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.RenewalStatusFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.RenewalStatusDropDownListId(),
                this.getData("Renewal Status")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RenewalManagerUpdateStatusButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.acceptAlertDialog();

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Renewal manager data entered successfully", false);

        return true;
      }
    
     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private String retrievePolicyNumber()
      {
        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase1"),
                                  "Policy Reference Number");

        if (policyNumber.equals("parameter not found"))
          {
            policyNumber = testData.TestParameters.get("PolicyRef");
            System.err.println("Failed to retrieve Policy Number");
          }
        else
          {
            testData.addParameter("Policy Reference Number", policyNumber);
            testData.updateParameter("PolicyRef", policyNumber);
            System.out.println("Policy Number retrieved - " + policyNumber);
          }

        return policyNumber;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
