
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduFidelityRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMoneyRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author FerdinandN
 */
public class EkunduEBPMoneyRiskTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduEBPMoneyRiskTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyRisksDialogisPresent())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the Risk Dialog is present - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterIndustryDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Industry Details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Industry Details - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterRiskItemDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Risk Item Details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Risk Item Details - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot("Money Risk details entered successfully", false);

        return new TestResult(testData, Enums.ResultStatus.PASS,
                              "Money Risk details entered successfully" + SeleniumDriverInstance.DriverExceptionDetail,
                              this.getTotalExecutionTime());

      }

    public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean verifyRisksDialogisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());

            return selectMoneyRiskType();
          }

        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            System.out.println("Add risk button clicked.");

            return selectMoneyRiskType();
          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

      }

    private boolean selectMoneyRiskType()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(10000);
        SeleniumDriverInstance.takeScreenShot("Money Risk type selected successfully", false);

        return true;
      }

    private boolean enterIndustryDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(),
                this.getData("Industry")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.IndustrySearchRowId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Industry details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduMoneyRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterRiskItemDetails()
      {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMoneyRiskPage.RiskItemFlatPremiumCheckboxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMoneyRiskPage.RiskItemAlarmWarrantyDropdownListId(),
                this.getData("Risk Item Alarm Warranty")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.RiskItemCoverMajorLimitSumInsuredTextboxId(),
                this.getData("Cover - Major Limit - Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMoneyRiskPage.RiskItemCoverMajorLimitPremiumTextboxId(),
                this.getData("Cover - Major Limit - Premium")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblCommOpName"))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblResidenceName"))
          {
            return false;
          }
        

        if (!SeleniumDriverInstance.clickElementById(EkunduMoneyRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk Item detais entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduMoneyRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMoneyRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMoneyRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMoneyRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId()))
          {
            return false;
          }

        return true;

      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
