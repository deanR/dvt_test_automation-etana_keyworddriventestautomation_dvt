
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreatePersonalClientPage;

/**
 *
 * @author DeanR
 */
public class EkunduAdrressDetailsAddEditDeletePersonalTest extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public EkunduAdrressDetailsAddEditDeletePersonalTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        this.setStartTime();

        if (!navigateToFindClientPage()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to navigate to the find client page"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to navigate to the find client page- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!verifyFindClientPageHasLoaded()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the find client page has loaded"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify that the find client page has loaded - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!findClient()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to find client"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to find client - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterSiteAddressDetails()) {
            SeleniumDriverInstance.takeScreenShot(("Could not enter site address suburb "), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Could not enter site address suburb  - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!EditAddress()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to Edit Address details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to Edit Address details  - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!DeleteAddress()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to Delete Address details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to Delete Address details  - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!VerifyAddressDelete()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify address deletion"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify address deletion  - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        SeleniumDriverInstance.takeScreenShot(("Adding, Editing, and Deleting of Addresses completed successfully"),
                false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Adding, Editing, and Deleting of Addresses completed successfully",
                this.getTotalExecutionTime());
    }

    private boolean navigateToFindClientPage() {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),
                EKunduHomePage.FindClientTabXPath())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Navigation to the Find Client page was successful", false);

        return true;
    }

    private boolean verifyFindClientPageHasLoaded() {
        if (!SeleniumDriverInstance.validateElementTextValueById(EKunduFindClientPage.PageHeaderSpanId(),
                "Search Criteria")) {
            return false;
        } else {
            SeleniumDriverInstance.takeScreenShot("Find Client page was successfully loaded", false);
        }

        return true;
    }

    private boolean findClient() {
        String clientCode = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                "Retrieved Client Code");

        if (clientCode.equals("parameter not found")) {
            clientCode = testData.TestParameters.get("Client Code");
        } else {
            testData.updateParameter("Client Code", clientCode);
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ClientCodeTextBoxId(),
                clientCode)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SearchButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.SelectClientLinkId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Client found", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SelectClientLinkId())) {
            return false;
        }

        return true;
    }

    private boolean enterSiteAddressDetails() {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.editClientButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduCreatePersonalClientPage.AddressTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreatePersonalClientPage.AddressAddLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameById(EkunduCreatePersonalClientPage.AddressFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreatePersonalClientPage.addressTypeDropDownListId(),
                testData.TestParameters.get("Address Type - site"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.addressLine1TextBoxId(),
                testData.TestParameters.get("Home Address Line 1"))) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.ArrowdownToElementById(EkunduCreatePersonalClientPage.addressLine1TextBoxId())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.AddressAddButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EkunduCreatePersonalClientPage.CloseAddressDialog2ButtonXPath())) {
            return false;
        }

        return true;
    }

    private boolean EditAddress() // <editor-fold defaultstate="collapsed" desc="enterBankDetails">
    {
//        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.editClientButtonId())) {
//            return false;
//        }
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduCreatePersonalClientPage.AddressTabPartialLinkText())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Navigationto the Address tab was successful", false);

        if (!SeleniumDriverInstance.clientAddressActionLinks("Site Address", "Edit")) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameById("modalDialog")) {
            return false;
        }

        SeleniumDriverInstance.clearTextById(EkunduCreatePersonalClientPage.addressLine1TextBoxId());

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduCreatePersonalClientPage.addressLine1TextBoxId(),
                testData.TestParameters.get("edit Line 1"))) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.ArrowdownToElementById(EkunduCreatePersonalClientPage.addressLine1TextBoxId())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.UpdateAddressButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(EkunduCreatePersonalClientPage.CloseAddressDialog2ButtonXPath())) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        return true;
    }

    // </editor-fold>
    private boolean DeleteAddress() {
//        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.editClientButtonId())) {
//            return false;
//        }
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduCreatePersonalClientPage.AddressTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clientAddressActionLinks("Site Address", "Delete")) {
            return false;
        }
//
//        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.DeleteAddressLinkId())) {
//            return false;
//        }

//        SeleniumDriverInstance.acceptAlertDialog();
//        updateClientDetails();
        SeleniumDriverInstance.pause(1000);
        SeleniumDriverInstance.takeScreenShot("Address Deleted successfully", false);

        return true;
    }

    // </editor-fold>
    private boolean VerifyAddressDelete() {
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduCreatePersonalClientPage.AddressTabPartialLinkText())) {
            return false;
        }
        
         if(SeleniumDriverInstance.clientAddressActionLinks("Site Address", "Delete")) {
            return false;
        }
//
//
//        if (!SeleniumDriverInstance.waitForElementById("ctl00_cntMainBody_Addresses_drgAddresses_ctl04_hypAddressEdit")) {
//            SeleniumDriverInstance.takeScreenShot("Address deleted successfully", false);
//        }

        return true;
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
