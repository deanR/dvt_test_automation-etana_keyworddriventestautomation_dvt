
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.CreateNewClaimPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMaintainCorporateClientClaim1Page;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduRecommendClaimTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduRecommendClaimTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!navigateToFindClaimPage())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to navigate to the find claim page"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the find claim page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findClaim())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to find claim"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find claim- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!RecommendClaim())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to recommend claim payment"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to recommend claim payment- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Corporate client claim recommended successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Corporate client claim recommended successfully",
                              this.getTotalExecutionTime());

      }

    private boolean navigateToFindClaimPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EkunduMaintainCorporateClientClaim1Page.ClaimHoverTabId(),
                EkunduMaintainCorporateClientClaim1Page.AuthoriseClaimLinkXpath()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.waitForElementById(EkunduMaintainCorporateClientClaim1Page.ClaimReferenceTextBoxId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Navigation to the Find Claim page was successful", false);

        return true;
      }

    private boolean findClaim()
      {
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.OkButtonId()))
          {
            return false;
          }

        String claimref = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                              "Retrieved Corporate Claim Number");

        if (claimref.equals("parameter not found"))
          {
            claimref = testData.TestParameters.get("Claim Reference Number");
          }
        else
          {
            testData.updateParameter("Claim Reference Number", claimref);
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.ClaimReferenceTextBoxId(),
                claimref))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimFindNowButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

//        if (!SeleniumDriverInstance.waitForElementById(EkunduMaintainCorporateClientClaim1Page.RecommendClaimLinkId()))
//          {
//            return false;
//          }

        SeleniumDriverInstance.takeScreenShot("Claim found", false);

        return true;

      }

    private boolean RecommendClaim()
      {
        if (!SeleniumDriverInstance.clickElementbyLinkText("Recommend"))
          {
            return false;
          }

//      if(SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.RecommendClaimValidationSummaryDivId()))
//      {
//          SeleniumDriverInstance.takeScreenShot("The user is not within the recommend/authorise limits for this payment type", true);
//          System.err.println("The user is not within the recommend/authorise limits for this payment type");
//          return false;
//      }

        SeleniumDriverInstance.scrollElementIntoViewById(CreateNewClaimPage.RiskTypeDropdownListId());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimFinishButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.RecommendClaimButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Claim recommended successfully", false);

        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
