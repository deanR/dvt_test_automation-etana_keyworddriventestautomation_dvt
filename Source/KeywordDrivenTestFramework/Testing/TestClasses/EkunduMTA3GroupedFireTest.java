
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduMTA3GroupedFireTest extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public EkunduMTA3GroupedFireTest(TestEntity testData) {
        this.testData = testData;

    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToFindClientPage()) {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find client page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to navigate to the find client page - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!findClient()) {
            SeleniumDriverInstance.takeScreenShot("Failed to find client", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!verifyClientDetailsPageHasLoaded()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the client details page has loaded", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify that the client details page has loaded- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterMidTermAdjustmentData()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Mid Term Adjustment data", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Mid Term Adjustment data- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Mid Term Adjustment completed successfully",
                this.getTotalExecutionTime());
    }

    private boolean navigateToFindClientPage() {
        if (!SeleniumDriverInstance.navigateToFindClientPage(EkunduViewClientDetailsPage.ClientHoverTabId(), "Find Client")) {
            return false;
        }

        return true;
    }

    private boolean findClient() {
        String clientCode
                = SeleniumDriverInstance.retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                        "Retrieved Client Code");

        if (clientCode.equals("parameter not found")) {
            clientCode = testData.TestParameters.get("Client Code");
        } else {
            testData.updateParameter("Client Code", clientCode);
        }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId())) {
            return false;
        }

        return true;
    }

    private boolean verifyClientDetailsPageHasLoaded() {
        if (!SeleniumDriverInstance.validateElementTextValueById(EkunduViewClientDetailsPage.ViewClientDetailsLabelId(),
                "View Client Details")) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Client Details page successfully loaded", false);

        return true;
    }

    private boolean enterMidTermAdjustmentData() {
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText())) {
            return false;
        }

        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                "Retrieved Policy Number");

        if (policyNumber.equals("parameter not found")) {
            policyNumber = testData.TestParameters.get("Policy Number");
        } else {
            testData.updateParameter("Policy Number", policyNumber);
        }

        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ChangePolicyLinkText())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

//         if(!SeleniumDriverInstance.changePolicy(policyNumber, "Change"))
//        {
//            return false;
//        }
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MTATypeofChangeDropDownListId(),
                this.getData("Type of Change"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MIAEffectiveDateTextBoxId(),
                this.getData("Effective Date"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId())) {
            return false;
        }

        return true;

    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
