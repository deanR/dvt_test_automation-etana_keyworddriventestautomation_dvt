/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorFleetRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author ferdinandN
 */
public class EkunduEBPAnnualMotorChangesTest extends BaseClass
{
    
    TestEntity testData;
    TestResult testResultt;

    public EkunduEBPAnnualMotorChangesTest(TestEntity testData)
      {
        this.testData = testData;
      }
    
    public TestResult executeTest()
      {
        this.setStartTime();
        
        if(testData.TestMethod.toUpperCase().trim().contains("EBP - Motor Section Details Risk".toUpperCase().trim()))
        {
       
            if (!verifyRisksDialogisPresent())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to select Motor Section Details Risk"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to select Motor Section Details Risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
        
            if (!enterMotorSectionDetailsRiskData())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to add Motor Section Details Risk"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to add Motor Section Details Risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            
               return new TestResult(testData, Enums.ResultStatus.PASS, "Motor Section Details Risk  added successfully", this.getTotalExecutionTime()); 
               
      }
        
        else if(testData.TestMethod.toUpperCase().trim().contains("EBP - Motor - Caravan".toUpperCase().trim()))
        {
       
            if (!verifyRisksDialogisPresent())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to select Motor Section Details Risk"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to select Motor Section Details Risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
        
            if (!addMotorCaravan())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to add Motor Section Details Risk"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to add Motor Section Details Risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            
               return new TestResult(testData, Enums.ResultStatus.PASS, "Motor Section Details Risk  added successfully", this.getTotalExecutionTime()); 
               
      }
        
        else if(testData.TestMethod.toUpperCase().trim().contains("EBP - Motor - Trailer".toUpperCase().trim()))
        {
       
            if (!verifyRisksDialogisPresent())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to select Motor Section Details Risk"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to select Motor Section Details Risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
        
            if (!addMotorTrailer())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to add Motor Section Details Risk"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to add Motor Section Details Risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            
               return new TestResult(testData, Enums.ResultStatus.PASS, "Motor Section Details Risk  added successfully", this.getTotalExecutionTime()); 
               
      }
           
 
            
             if (!verifyRisksDialogisPresent())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to select Motor Section Details Risk"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to select Motor Section Details Risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            
            
            if (!enterPrivateMotorRiskData())
              {
                SeleniumDriverInstance.takeScreenShot(("Failed to add Private Motor"), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to add Private Motor- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
              }
            
            return new TestResult(testData, Enums.ResultStatus.PASS, "Motor Risk  added successfully", this.getTotalExecutionTime());
            
        
      }
    
    private boolean verifyRisksDialogisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            selectMotorRiskType();

            return true;
          }

        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectMotorRiskType();

            return true;

          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

      }
    
     private boolean selectMotorRiskType()
      {
        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(testData.TestParameters.get("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Motor risk type selected successfully", false);

        return true;

      }
    
    private boolean enterMotorSectionDetailsRiskData()
    {
         if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.NumberofVehiclesTextBoxId(), testData.TestParameters.get("Number of Vehicles")))
          {
            return false;
          }
                  
        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.NumberofClaimsIncurredTextBoxId(), testData.TestParameters.get("Number of Claims Incurred in Last 12 Months")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }
        
        SeleniumDriverInstance.pause(5000);
        
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
          {
            return false;
          }
          
          
          if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId()))
          {
           return false;
          } 
          
          SeleniumDriverInstance.takeScreenShot("Motor Section Details Risk Added Successfully", false);
         
          return true;
      
    }
    
    private boolean enterPrivateMotorRiskData()
    {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
           return false;
          }
        
        System.out.println("Perfoming vehicle selection");
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SearchVehicleModelIconId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);
        
        SeleniumDriverInstance.WaitUntilDropDownListPopulatedById(EkunduCreateNewPolicyForNewClientPage.MakeDropdownListId());
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MakeDropdownListId(),
                testData.TestParameters.get("Vehicles Make")))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleYearDropdownListId(),
                testData.TestParameters.get("Vehicles Year")))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleModelDropdownListId(),
                testData.TestParameters.get("Vehicles Model")))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.VehicleModelSelectedRowId()))
          {
            return false;
          }
        
        SeleniumDriverInstance.pause(3000);
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorClassofUseDropdownListId(),
                testData.TestParameters.get("Vehicle Class of Use")))
          {
            return false;
          }
        
        System.out.println("Vehicle Type Tab - Options Section");

        SeleniumDriverInstance.pause(2000);
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.VehicleTypeOptionsEndorsementsCheckboxId(), true))
       {
           return false;
       }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesCheckBoxId(), true))
       {
           return false;
       }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriversCheckBoxId(), true))
       {
           return false;
       }
        
     System.out.println("Vehicle Type Tab - Claims History and Main Driver");
               
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleOvernightParkingDropdownListId(),
                testData.TestParameters.get("Vehicle Overnight Parking")))
        {
          return false;
        }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegularDriverNameTextBoxId(),
                testData.TestParameters.get("Regular Driver Name")))
        {
          return false;
        }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MainDriverDateLicenseIssuedTextboxId(),
                testData.TestParameters.get("Main Driver - Date License Issued")))
        {
          return false;
        }
        
        SeleniumDriverInstance.takeScreenShot("Vehicle Type details entered successfully", true);
        
        System.out.println("Navigating to the Motor Details Tab...");
        
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
           return false;
          }
        
        System.out.println("Entering Vehicle Cover Details...");
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AreaCodeTextboxId(),
                testData.TestParameters.get("Vehicle Cover Details - Area Code")))
            {
              return false;
            }
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorTrackingDeviceDropdownListId(),
            testData.TestParameters.get("Tracking Device")))
        {
          return false;
        }
        
        System.out.println("Entering Vehicle Registration Details...");
        
         if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EngineNumberTextboxId(),
                testData.TestParameters.get("Engine Number")))
        {
          return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ChassisNumberTextboxId(),
            testData.TestParameters.get("Chassis Number")))
        {
          return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationNumberTextboxId(),
            generateDateTimeString()))
        {
          return false;
        }

        try
        {
             JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver  ;
             js.executeScript("VerifyEPLRegistrationNumber();");
        }
        catch(Exception e)
        {
            System.err.println("Failed to verify Registration number");
        }

        SeleniumDriverInstance.pause(3000);



        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleRegOwnerTextBoxId(),
            testData.TestParameters.get("Registered Owner")))
        {
          return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationDateTextboxId(),
            testData.TestParameters.get("Registration Date")))
        {
          return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NATISCodeDropdownListId(),
            testData.TestParameters.get("NATIS Code")))
        {
          return false;
        }
        
        
        System.out.println("Motor Cover");
        
          if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleCoverTypeDropdownListId(),
                testData.TestParameters.get("Motor Cover Type")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BasisofSettlementDropdownListId(),
                testData.TestParameters.get("Basis of Settlement")))
          {
            return false;
          }
      
        
        System.out.println("Motor Accessories");
            
            if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddAccessoriesButtonName()))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessoryDescriptionTextBoxId(),
            testData.TestParameters.get("Accessory Description")))
            {
              return false;
            }
       
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessorySumInsuredTextBoxId(),
            testData.TestParameters.get("Accessory Sum Insured")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
            {
              return false;
            }
        
            System.out.println("Motor Extensions");
            
            if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsEBPMotorChangesCarHireCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.CarHireLimitofIndemnityDropdownListId(),
                testData.TestParameters.get("Motor Extensions - Car Hire - Limit of Indemnity")))
            {
              return false;
            }

            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.CarHireConditionsDropdownListId(),
                testData.TestParameters.get("Motor Extensions - Car Hire Conditions")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
            
            SeleniumDriverInstance.pause(3000);
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsContingentLiabilityCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsContingentLiabilityLimitofIndemnityTextboxId(),
                testData.TestParameters.get("Motor Extensions - Contingent Liability - Limit of Indemnity")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsContingentLiabilityRateTextboxId(),
                testData.TestParameters.get("Motor Extensions - Contingent Liability - Rate")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
            
            SeleniumDriverInstance.pause(3000);
            
            if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsCreditShortallCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsCreditShortfallRateTextboxId(),
                testData.TestParameters.get("Motor Extensions - Credit Shortfall - Rate")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
            
            SeleniumDriverInstance.pause(3000);
            
            if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsExcessWaiverCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsExcessWaiverPremiumTextboxId(),
                testData.TestParameters.get("Motor Extensions - Excess Waiver - Premium")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
            
            SeleniumDriverInstance.pause(3000);
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsFireAndexplosionRateTextboxId(),
                testData.TestParameters.get("Motor Extensions - Fire & Explosion - Rate")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsFireAndexplosionPremiumTextboxId(),
                testData.TestParameters.get("Motor Extensions - Fire & Explosion - Premium")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
            
            SeleniumDriverInstance.pause(3000);
            
            if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsLossofKeysCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsLossofKeysLimitofIndemnityTextboxId(),
                testData.TestParameters.get("Motor Extensions - Loss of Keys - Limit of Indemnity")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsLossofKeysPremiumTextboxId(),
                testData.TestParameters.get("Motor Extensions - Loss of Keys - Premium")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
            
            SeleniumDriverInstance.pause(3000);
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsParkingFacilitiesCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsParkingFacilitiesLimitofIndemnityTextboxId(),
                testData.TestParameters.get("Motor Extensions - Parking Facilities - Limit of Indemnity")))
            {
              return false;
            
            }

            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsRiotandStrikeCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsRoadsideAssistCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsTheftofCarRadioCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsTheftofCarRadioRateTextboxId(),
                testData.TestParameters.get("Motor Extensions - Theft of Car Radio - Rate")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsThirdPartyLiabilityRateTextboxId(),
                testData.TestParameters.get("Motor Extensions - Third Party Liability - Rate")))
            {
              return false;
            }
            
             if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsThirdPartyLiabilityPremiumTextboxId(),
                testData.TestParameters.get("Motor Extensions - Third Party Liability - Premium")))
            {
              return false;
            }
             
             if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsWreckageRemovalCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsWreckageRemovalLimitofIndemnityTextboxId(),
                testData.TestParameters.get("Motor Extensions - Wreckage Removal - Limit of Indemnity")))
            {
              return false;
            
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsWreckageRemovalRateTextboxId(),
                testData.TestParameters.get("Motor Extensions - Wreckage Removal - Rate")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
            
            SeleniumDriverInstance.pause(3000);
            
            System.out.println("Voluntary Excess");
           
             
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryExcessPercentageTextBoxId(),testData.TestParameters.get("Voluntary Excess Percentage")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryMinimumAmountTextBoxId(),testData.TestParameters.get("Voluntary Minimum Amount")))
            {
              return false;
            }
            
            
             System.out.println("Additional Compulsory Excess");
            
             if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryExcessPercentageTextBoxId(),testData.TestParameters.get("Compulsory Excess Percentage")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryMinimumAmountTextBoxId(),testData.TestParameters.get("Compulsory Minimum Amount")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
            
            SeleniumDriverInstance.pause(3000);
            
            SeleniumDriverInstance.takeScreenShot("Motor Details entered successfully", false);
            

            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
            System.out.println("Navigating to the First Amount Payable tab...");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPOwnDamageMaxPercentageTextBoxId(),
                testData.TestParameters.get("FAP - Own Damage -  Max Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPOwnDamageMaxAmountTextBoxId(),
                testData.TestParameters.get("FAP - Own Damage -  Max Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPThirdPartyMinimumPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Third Party - Minimum Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPThirdPartyMaximumPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Third Party - Max Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPThirdPardyMinimumAmountTextBoxId(),
                testData.TestParameters.get("FAP - Third Party -  Minimum Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPThirdPardyMaxAmountTextBoxId(),
                testData.TestParameters.get("FAP - Third Party - Max Amount")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPFireAndExplosionMinimumPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Fire and Explosion - Minimum Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPFireAndExplosionMaxPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Fire and Explosion - Max Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPFireAndExplosionMinimumAmountMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Fire and Explosion -  Minimum Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPFireAndExplosionMaxAmountMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Fire and Explosion - Max Amount")))
          {
            return false;
          }

        
//
//        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPTheftHijackMinimumPercentageTextboxId(),
//               testData.TestParameters.get("FAP Minimum Percentage")))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPTheftHijackMinimumAmountTextboxId(),
//               testData.TestParameters.get("FAP Minimum Amount")))
//          {
//            return false;
//          }
        
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblTheftHijack"))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPHijackMinimumPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Theft Hijack - Minimum Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPHijackMaximumPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Theft Hijack - Max Percentage")))
          {
            return false;
          }
        
         if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPHijackMinimumAmountTextboxId(),
                testData.TestParameters.get("FAP - Theft Hijack -  Minimum Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPHijackMaxAmountTextBoxId(),
                testData.TestParameters.get("FAP - Theft Hijack - Max Amount")))
          {
            return false;
          }
        
        SeleniumDriverInstance.takeScreenShot("First Amount Payable details entered successfully", false);
        
         if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
            System.out.println("Navigating to the Endorsements tab...");
            
            if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsSelectButtonId()))
            {
                return false;
            }

            if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsAddAllButtonId()))
            {
                return false;
            }

            if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsApplyButtonId()))
            {
                return false;
            }
            
            SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
        
        

        

        System.out.println("Navigating to the Specified Drivers tab...");
        
        
            if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddSpecifiedDriverButtonName()))
            {
              return false;
            }    
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverNameTextBoxId(),
            testData.TestParameters.get("Regular Driver Name")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.EPLSpecifiedDriverIDTypeDropdownListId(),  testData.TestParameters.get("Main Driver - ID Type")))
            {
               return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverIDNumberTextBoxId(),
            testData.TestParameters.get("Main Driver - ID Number")))
            {
              return false;
            }
            
            SeleniumDriverInstance.takeScreenShot("Specified Drivers details entered successfully", false);
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
            System.out.println("Navigating to the Interested  tab...");
            
            if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.MSAddInterestedPartiesButtonName()))
            {
              return false;
            }

            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MSTypeofAgreementTypeDropdownListId(),
                    testData.TestParameters.get("Type of Agreement")))
            {
              return false;
            }

            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.InterestedPartyInstitutionNameDropdownListId(),
                testData.TestParameters.get("Institution Name")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.InterestedPartyDescriptionTextBoxId(),testData.TestParameters.get("IP - Description")))
            {
              return false;
            }

        
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
             {
               return false;
             }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
             {
               return false;
             }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
             {
               return false;
             }
            
            System.out.println("Navigating to the notes tab");
            
             if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MotorCommentsTextBoxId(),
                testData.TestParameters.get("Motor Notes")))
            {
              return false;
            }

            SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);

            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
              {
                return false;
              }
            
            if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
            {
                return false;
            }

            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_ReInsurance2007Cntrl_ddlReinsurance",
                    "MS Liability (Section B)"))
              {
                return false;
              }
            
            SeleniumDriverInstance.pause(3000);

            if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
            {
                return false;
            }
            
            if(!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
            {
                return false;
            }

            if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId()))
            {
                return false;
            }
            
            SeleniumDriverInstance.takeScreenShot("Private Motor (Audi) added successfullyfalse", false);
                    
            
        return true;
            
            

    }
    
    private boolean addMotorCaravan()
      {
         if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SearchVehicleModelIconId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);
        
        
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehiclesSourceDropdownListId(),
                testData.TestParameters.get("Vehicles Source")))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);

        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.VehicleModelSelectedRowId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);
       
            System.out.println("Options...");
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.EndorsementsCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesCheckBoxId(), true))
            {
              return false;
            }
            
           
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NoClaimRebateDropdownListId(),
                testData.TestParameters.get("No Claim Rebate")))
            {
              return false;
            }
            
            
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
            {
              return false;
            }
            
           
            System.out.println("Registration Details...");
            
          
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationNumberTextboxId(),
                generateDateTimeString()))
            {
              return false;
            }
            
            try
            {
                 JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver  ;
                 js.executeScript("VerifyEPLRegistrationNumber();");
            }
            catch(Exception e)
            {
                System.err.println("Failed to verify Registration number");
            }
          
            SeleniumDriverInstance.pause(3000);
            
           
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleRegOwnerTextBoxId(),
                testData.TestParameters.get("Registered Owner")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationDateTextboxId(),
                testData.TestParameters.get("Registration Date")))
            {
              return false;
            }
            
            System.out.println("Motor Cover");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleSumInsuredTextBoxId(),
                testData.TestParameters.get("Vehicle Sum Insured")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.CoverTypeDropdownListId(),
                testData.TestParameters.get("Motor Cover Type")))
            {
              return false;
            }
            
            
            
            System.out.println("Motor Accessories");
            
            if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddAccessoriesButtonName()))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessoryDescriptionTextBoxId(),
            testData.TestParameters.get("Accessory Description")))
            {
              return false;
            }
       
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessorySumInsuredTextBoxId(),
            testData.TestParameters.get("Accessory Sum Insured")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
            {
              return false;
            }
       
        System.out.println("Motor Extensions");
           
             if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsCreditShortFallCheckBoxId(), true))
            {
              return false;
            }
             
             if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CreditShortfallRateTextBoxId(),testData.TestParameters.get("Extensions - Credit Shortfall - Rate")))
            {
              return false;
            }
             
             if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
             
             if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsExcessWaiverCheckBoxId(), true))
            {
              return false;
            }
             
             if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsExcessWaiverPremiumTextboxId(),testData.TestParameters.get("Extensions - Excess Waiver - Premium")))
            {
              return false;
            }
             
             if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
                    
             if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsFireAndexplosionRateTextboxId(),testData.TestParameters.get("Extensions - Fire and Explosion - Rate")))
            {
              return false;
            }
              
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsFireAndexplosionPremiumTextboxId(),testData.TestParameters.get("Extensions - Fire and Explosion - Premium")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
             
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ThirdPartyLiabilityRateMotorChangesTextBoxId(),testData.TestParameters.get("Extensions - Third Party Liability - Rate")))
            {
              return false;
            }
              
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ThirdPartyLiabilityPremiumMotorChangesTextBoxId(),testData.TestParameters.get("Extensions - Third Party Liability - Premium")))
            {
              return false;
            }
            
           
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsWreckageRemovalCheckBoxId(), true))
            {
              return false;
            }
             
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsWreckageRemovalLimitofIndemnityTextboxId(),testData.TestParameters.get("Extensions - Wreckage Removal - Limit of Indemnity")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsWreckageRemovalRateTextboxId(),testData.TestParameters.get("Extensions - Wreckage Removal - Rate")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
            
            System.out.println("Voluntary Excess");
           
             
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryExcessPercentageTextBoxId(),testData.TestParameters.get("Voluntary Excess Percentage")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryMinimumAmountTextBoxId(),testData.TestParameters.get("Voluntary Minimum Amount")))
            {
              return false;
            }
            
            
             System.out.println("Additional Compulsory Excess");
            
             if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryExcessPercentageTextBoxId(),testData.TestParameters.get("Compulsory Excess Percentage")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryMinimumAmountTextBoxId(),testData.TestParameters.get("Compulsory Minimum Amount")))
            {
              return false;
            }
            
            SeleniumDriverInstance.takeScreenShot("Motor Details entered successfully", false);
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
             System.out.println("Navigating to the First Amount Payable tab...");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPOwnDamageMaxPercentageTextBoxId(),
                testData.TestParameters.get("FAP - Own Damage -  Max Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPOwnDamageMaxAmountTextBoxId(),
                testData.TestParameters.get("FAP - Own Damage -  Max Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPThirdPartyMinimumPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Third Party - Minimum Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPThirdPartyMaximumPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Third Party - Max Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPThirdPardyMinimumAmountTextBoxId(),
                testData.TestParameters.get("FAP - Third Party -  Minimum Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPThirdPardyMaxAmountTextBoxId(),
                testData.TestParameters.get("FAP - Third Party - Max Amount")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPFireAndExplosionMinimumPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Fire and Explosion - Minimum Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPFireAndExplosionMaxPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Fire and Explosion - Max Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPFireAndExplosionMinimumAmountMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Fire and Explosion -  Minimum Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPFireAndExplosionMaxAmountMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Fire and Explosion - Max Amount")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
            System.out.println("Endorsements...");
            
            enterEndorsements();
            

            
            System.out.println("Interested Parties...");
            
            this.enterInterestedParties();
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
            System.out.println("Risk Notes...");
            
             if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedNotesTextBoxId(), testData.TestParameters.get("Motor Specified Comments")))
            {
                return false;
            }

           if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedAddNotesCheckboxId(), true))
            {
                return false;
            }

           if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedPolicyNotesTextBoxId(), testData.TestParameters.get("Motor Specified Notes")))
            {
                return false;
            }
            
            SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);
        
        
            if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
                return false;
            }
            
            SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_ReInsurance2007Cntrl_ddlReinsurance",
                "MS Own Damage (Section A)"))
            {
              return false;
            }
            
        SeleniumDriverInstance.pause(3000);
            
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        SeleniumDriverInstance.clickElementbyLinkText("Reinsurances");

        
        SeleniumDriverInstance.pause(3000);

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        
        return true;
        
  }

 private boolean enterEndorsements()
    {
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsSelectButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsAddAllButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsApplyButtonId()))
         {
             return false;
         }
         
         
         SeleniumDriverInstance.takeScreenShot("Endorsement details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        
            return true;
       
     }
 
 private boolean enterInterestedParties()
      {
          //SeleniumDriverInstance.Driver.navigate().refresh();
          SeleniumDriverInstance.pause(3000);
          
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.MSAddInterestedPartiesButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MSTypeofAgreementTypeDropdownListId(),
                testData.TestParameters.get("Type of Agreement")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.InterestedPartyInstitutionNameDropdownListId(),
                testData.TestParameters.get("Institution Name")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }

//        if (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedNotesTextBoxId(),
//                testData.TestParameters.get("Interested Party Comments")))
//          {
//            return false;
//          }

        SeleniumDriverInstance.takeScreenShot("Interested parties details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
        return true;
        
      }
 
 private boolean addMotorTrailer()
      {
         if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SearchVehicleModelIconId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);
        
        
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehiclesSourceDropdownListId(),
                testData.TestParameters.get("Vehicles Source")))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);

        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.TrailorVehicleSelectedRowId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);
       
            System.out.println("Options...");
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.EndorsementsCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesCheckBoxId(), true))
            {
              return false;
            }
            
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NoClaimRebateDropdownListId(),
                testData.TestParameters.get("No Claim Rebate")))
            {
              return false;
            }
            
           SeleniumDriverInstance.takeScreenShot("Vehicle Type details entered successfully", false);
            
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
            {
              return false;
            }
            
            System.out.println("Registration Details...");
           
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationNumberTextboxId(),
                generateDateTimeString()))
            {
              return false;
            }
            
            try
            {
                 JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver  ;
                 js.executeScript("VerifyEPLRegistrationNumber();");
            }
            catch(Exception e)
            {
                System.err.println("Failed to verify Registration number");
            }
          
            SeleniumDriverInstance.pause(3000);
            
           
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleRegOwnerTextBoxId(),
                testData.TestParameters.get("Registered Owner")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationDateTextboxId(),
                testData.TestParameters.get("Registration Date")))
            {
              return false;
            }
            
            System.out.println("Motor Cover");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleSumInsuredTextBoxId(),
                testData.TestParameters.get("Vehicle Sum Insured")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.CoverTypeDropdownListId(),
                testData.TestParameters.get("Motor Cover Type")))
            {
              return false;
            }
            
            
            
            System.out.println("Motor Accessories");
            
            if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddAccessoriesButtonName()))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessoryDescriptionTextBoxId(),
            testData.TestParameters.get("Accessory Description")))
            {
              return false;
            }
       
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessorySumInsuredTextBoxId(),
            testData.TestParameters.get("Accessory Sum Insured")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
            {
              return false;
            }
       
        System.out.println("Motor Extensions");
           
             if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsCreditShortFallCheckBoxId(), true))
            {
              return false;
            }
             
             if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CreditShortfallRateTextBoxId(),testData.TestParameters.get("Extensions - Credit Shortfall - Rate")))
            {
              return false;
            }
             
             if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
             
             if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsExcessWaiverCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsExcessWaiverPremiumTextboxId(),
                testData.TestParameters.get("Extensions - Excess Waiver - Premium")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
            
            SeleniumDriverInstance.pause(3000);
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsFireAndexplosionRateTextboxId(),
                testData.TestParameters.get("Motor Extensions - Fire & Explosion - Rate")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsFireAndexplosionPremiumTextboxId(),
                testData.TestParameters.get("Motor Extensions - Fire & Explosion - Premium")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
            
            SeleniumDriverInstance.pause(3000);
            
            if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsParkingFacilitiesCheckBoxId(), true))
            {
              return false;
            }
                    
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsParkingFacilitiesLimitofIndemnityTextboxId(),
                testData.TestParameters.get("Motor Extensions - Parking Facilities - Limit of Indemnity")))
            {
              return false;
            }
            
            if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsRiotandStrikeCheckBoxId(), true))
            {
              return false;
            }
            
             
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ThirdPartyLiabilityRateMotorChangesTextBoxId(),testData.TestParameters.get("Extensions - Third Party Liability - Rate")))
            {
              return false;
            }
              
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ThirdPartyLiabilityPremiumMotorChangesTextBoxId(),testData.TestParameters.get("Extensions - Third Party Liability - Premium")))
            {
              return false;
            }
            
           
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
                    
            if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsWreckageRemovalCheckBoxId(), true))
            {
              return false;
            }
                    
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsWreckageRemovalLimitofIndemnityTextboxId(),testData.TestParameters.get("Extensions - Third Party Liability - Premium")))
            {
              return false;
            }
            
            System.out.println("Voluntary Excess");
           
             
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryExcessPercentageTextBoxId(),testData.TestParameters.get("Voluntary Excess Percentage")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryMinimumAmountTextBoxId(),testData.TestParameters.get("Voluntary Minimum Amount")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryExcessDiscountTextBoxId(),testData.TestParameters.get("Voluntary Excess Discount")))
            {
              return false;
            }
            
             System.out.println("Additional Compulsory Excess");
            
             if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryExcessPercentageTextBoxId(),testData.TestParameters.get("Compulsory Excess Percentage")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryMinimumAmountTextBoxId(),testData.TestParameters.get("Compulsory Minimum Amount")))
            {
              return false;
            }
            
            SeleniumDriverInstance.takeScreenShot("Motor Details entered successfully", false);
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
             System.out.println("Navigating to the First Amount Payable tab...");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPOwnDamageMaxPercentageTextBoxId(),
                testData.TestParameters.get("FAP - Own Damage -  Max Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPOwnDamageMaxAmountTextBoxId(),
                testData.TestParameters.get("FAP - Own Damage -  Max Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPThirdPartyMinimumPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Third Party - Minimum Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPThirdPartyMaximumPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Third Party - Max Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPThirdPardyMinimumAmountTextBoxId(),
                testData.TestParameters.get("FAP - Third Party -  Minimum Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPThirdPardyMaxAmountTextBoxId(),
                testData.TestParameters.get("FAP - Third Party - Max Amount")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPFireAndExplosionMinimumPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Fire and Explosion - Minimum Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPFireAndExplosionMaxPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Fire and Explosion - Max Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPFireAndExplosionMinimumAmountMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Fire and Explosion -  Minimum Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPFireAndExplosionMaxAmountMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Fire and Explosion - Max Amount")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
           
            System.out.println("Interested Parties...");
            
//            this.enterInterestedParties();
            
            SeleniumDriverInstance.pause(3000);
          
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.MSAddInterestedPartiesButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MSTypeofAgreementTypeDropdownListId(),
                testData.TestParameters.get("Type of Agreement")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.InterestedPartyInstitutionNameDropdownListId(),
                testData.TestParameters.get("Institution Name")))
          {
            return false;
          }
        
        if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MSInterestedPartiesDescriptionTextAreaId(), 
             testData.TestParameters.get("IP - Description")))
        {
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }

//        if (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedNotesTextBoxId(),
//                testData.TestParameters.get("Interested Party Comments")))
//          {
//            return false;
//          }
//
//        SeleniumDriverInstance.takeScreenShot("Interested parties details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
        
            if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
                return false;
            }
            
            SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_ReInsurance2007Cntrl_ddlReinsurance",
                "MS Own Damage (Section A)"))
          {
            return false;
          }
            
            SeleniumDriverInstance.pause(2000);
            
            SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

            SeleniumDriverInstance.pause(2000);
            
            SeleniumDriverInstance.clickElementbyLinkText("Reinsurances");
            
            
            if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
          {
               return false;
           }
           
           SeleniumDriverInstance.pause(5000);
           
           if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(),testData.TestParameters.get("Reinsurance Details - FAC Placement Reinsurer Code 1")))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
           {
               return false;
           }
           
           SeleniumDriverInstance.pause(3000);
           
               // SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId());
           
           if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId(), testData.TestParameters.get("Reinsurance Details - FAC Placement - ACE Percentage")))
           {
               return false;
           }
           
            if(!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
           {
               return false;
           }
            
            SeleniumDriverInstance.pause(5000);
            
            System.out.println("Company 1 added successfully");
            
            if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId()))
            {
                return false;
            }
        
        return true;
        
  }
 
 private boolean enterMotorRiskTypeDetailsClassic_Imports()
         {
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
              {
                return false;
              }

            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SearchVehicleModelIconId()))
              {
                return false;
              }

            SeleniumDriverInstance.pause(3000);
            
          
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleSourceDropdownListId(),
                    testData.TestParameters.get("Source")))
                {
                  return false;
                }
                
                SeleniumDriverInstance.pause(3000);
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleMakeOlderVehiclesDropdownListId(),
                    testData.TestParameters.get("Vehicles Make")))
              {
                return false;
              }

            SeleniumDriverInstance.pause(3000);

            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleYearTextBoxId(),
                    testData.TestParameters.get("Vehicles Year")))
              {
                return false;
              }

            SeleniumDriverInstance.pause(3000);

            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleModel2TextBoxId(),
                    testData.TestParameters.get("Vehicles Model")))
              {
                return false;
              }
            
            SeleniumDriverInstance.clearTextById(EkunduCreateNewPolicyForNewClientPage.CCTextBoxId());
            
            if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduCreateNewPolicyForNewClientPage.CCTextBoxId(),
                    testData.TestParameters.get("CC")))
              {
                return false;
              }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.GVMTextBoxId(),
                    testData.TestParameters.get("GVM")))
              {
                return false;
              }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BodyTypeDropdownListId(),
                    testData.TestParameters.get("Body Type")))
              {
                return false;
              }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleTypeDropdownListId(),
                    testData.TestParameters.get("Vehicle Type")))
              {
                return false;
              }

            SeleniumDriverInstance.pause(3000);
            
             if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.VehicleNextButtonId()))
            {
              return false;
            }
             
             System.out.println("Options...");
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.EndorsementsCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriversCheckBoxId(), true))
            {
              return false;
            }
            
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleOvernightParkingDropdownListId(),
                testData.TestParameters.get("Vehicle parking overnight")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
            {
              return false;
            }
            
            System.out.println("Vehicle Cover Details...");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AreaCodeTextboxId(),
                testData.TestParameters.get("Area Code")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorTrackingDeviceDropdownListId(),
                testData.TestParameters.get("Tracking Device")))
            {
              return false;
            }
            
            System.out.println("Registration Details...");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EngineNumberTextboxId(),
                testData.TestParameters.get("Engine Number")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ChassisNumberTextboxId(),
                testData.TestParameters.get("Chassis Number")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationNumberTextboxId(),
                generateDateTimeString()))
            {
              return false;
            }
            
            try
            {
                 JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver  ;
                 js.executeScript("VerifyEPLRegistrationNumber();");
            }
            catch(Exception e)
            {
                System.err.println("Failed to verify Registration number");
            }
          
            SeleniumDriverInstance.pause(3000);
            
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegOwnerTextBoxId(),
                testData.TestParameters.get("Registered Owner")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationDateTextboxId(),
                testData.TestParameters.get("Registration Date")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NATISCodeDropdownListId(),
                testData.TestParameters.get("NATIS Code")))
            {
              return false;
            }
            
            System.out.println("Motor Cover");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleSumInsuredTextBoxId(),
            testData.TestParameters.get("Motor Cover Sum Insured")))
            {
              return false;
            }
            
             System.out.println("Motor Accessories");
            
            if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddAccessoriesButtonName()))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessoryDescriptionTextBoxId(),
            testData.TestParameters.get("Accessory Description")))
            {
              return false;
            }
       
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessorySumInsuredTextBoxId(),
            testData.TestParameters.get("Accessory Sum Insured")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
            {
              return false;
            }
            
             System.out.println("Motor Extensions");
            
            if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsCarHireCheckBoxId(), true))
            {
              return false;
            }
                                                
//             if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CarHirePremiumTextBoxId(),testData.TestParameters.get("Extensions - Car Hire - Premium")))
//            {
//              return false;
//            }
//            
             if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsCreditShortFallCheckBoxId(), true))
            {
              return false;
            }
             
             if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CreditShortfallRateTextBoxId(),testData.TestParameters.get("Extensions - Credit Shortfall - Rate")))
            {
              return false;
            }
            
              if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ThirdPartyLiabilityRateTextBoxId(),testData.TestParameters.get("Extensions - Third Party Liability - Rate")))
            {
              return false;
            }
              
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ThirdPartyLiabilityPremiumTextBoxId(),testData.TestParameters.get("Extensions - Third Party Liability - Premium")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsWindscreenCheckBoxId(), true))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.WindscreenFAPPercentageTextBoxId(),testData.TestParameters.get("Extensions - Windscreen - FAP Percentage")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.WindscreenFAPAmountTextBoxId(),testData.TestParameters.get("Extensions - Windscreen - FAP Amount")))
            {
              return false;
            }
            
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
            
            System.out.println("Voluntary Excess");
           
             
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryExcessPercentageTextBoxId(),testData.TestParameters.get("Voluntary Excess Percentage")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryMinimumAmountTextBoxId(),testData.TestParameters.get("Voluntary Minimum Amount")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryExcessDiscountTextBoxId(),testData.TestParameters.get("Voluntary Excess Discount")))
            {
              return false;
            }
            
             System.out.println("Additional Compulsory Excess");
            
             if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryExcessPercentageTextBoxId(),testData.TestParameters.get("Compulsory Excess Percentage")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryMinimumAmountTextBoxId(),testData.TestParameters.get("Compulsory Minimum Amount")))
            {
              return false;
            }
            
            SeleniumDriverInstance.takeScreenShot("Motor Details entered successfully", false);
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
            System.out.println("Endorsements");
            
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsSelectButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsAddAllButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsApplyButtonId()))
         {
             return false;
         }
         
         
         SeleniumDriverInstance.takeScreenShot("Endorsement details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        
        
        System.out.println("Interested parties");
        
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.MSAddInterestedPartiesButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MSTypeofAgreementTypeDropdownListId(),
                testData.TestParameters.get("Type of Agreement")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.InterestedPartyInstitutionNameDropdownListId(),
                testData.TestParameters.get("Institution Name")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartyAddNotesLabelId()))
          {
            return false;
          }
                  
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.InterestedPartyNotesTextBoxId(),
                testData.TestParameters.get("Interested Party Comments")))
          {
            return false;
          }

            SeleniumDriverInstance.takeScreenShot("Interested parties details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
        
        System.out.println("Financial Overview");
        
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.PremAdjBasisDropdownListId(),
                testData.TestParameters.get("Premium Adjustment Basis")))
          {
            return false;
          }
        
        SeleniumDriverInstance.clearTextById(EkunduCreateNewPolicyForNewClientPage.FlatPremiumAmountTextBoxId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduCreateNewPolicyForNewClientPage.MotorSpecifiedAdjustmentPercentageTextBoxId(),testData.TestParameters.get("Financial Overview - Adjustment Percentage")))
            {
              return false;
            }
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.PremAdjReasonDropdownListId(),
                testData.TestParameters.get("Premium Adjustment Reason")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
        
        
        System.out.println("Notes");
        
//        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedNotesTextBoxId(), testData.TestParameters.get("Motor Specified Comments")))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedAddNotesCheckboxId(), true))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedPolicyNotesTextBoxId(), testData.TestParameters.get("Motor Specified Notes")))
//         {
//             return false;
//         }
            
            SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);
        
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
         {
             return false;
         }
        
        
        System.out.println("FAC");
       
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BandDropdownListId(),
                testData.TestParameters.get("Reinsurance Band")))
          {
            return false;
          }
        
        SeleniumDriverInstance.pause(5000);
        
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddPropFACButtonId()))
          {
            return false;
          }
            SeleniumDriverInstance.pause(3000);
            
            if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(),
                testData.TestParameters.get("FAC Prop - Addison")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);

//        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId());
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceSumInsuredTextBoxId(),
                testData.TestParameters.get("Unallocated Amount")))
          {
            return false;
          }
        
         SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);

        System.out.println("Company 1 added successfully");
        
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
          {
            return false;
          }

//        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceOkButtonId()))
          {
            return false;
          }

//        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceOkButtonId());

        SeleniumDriverInstance.takeScreenShot("Prop FAC details entered successfully", false);
            
           return true;

       }
     
}
