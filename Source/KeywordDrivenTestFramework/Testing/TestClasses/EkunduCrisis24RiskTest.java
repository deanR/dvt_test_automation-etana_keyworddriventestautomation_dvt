
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCrisis24Page;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author deanR
 */
public class EkunduCrisis24RiskTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduCrisis24RiskTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk button is present", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the add risk button is present - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterCoverDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Cover details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Cover details - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterEndorsements())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Endorsements", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Endorsements - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!Notes())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Notes", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Notes - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS, "EHAT Crisis 24 Risk details entered successfully",
                              this.getTotalExecutionTime());
      }

    public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean verifyAddRiskButtonisPresent()
      {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectCrisis24Risk();

            return true;
          }
        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectCrisis24Risk();

            return true;
          }
        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");

            return false;
          }
      }

    private boolean selectCrisis24Risk()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(10000);
        SeleniumDriverInstance.takeScreenShot("Business All Risk selected sucessfully", false);

        return true;
      }

    private boolean enterCoverDetails()
      {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCrisis24Page.AssistancecheckboxId(), true))
          {
            //return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCrisis24Page.AssistancecLimitOfIndemnityTextboxId(),
                this.getData("Assistance - Limit Of Indemnity")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCrisis24Page.AssistancecRateTextboxId(),
                this.getData("Assistance - Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCrisis24Page.InterventioncheckboxId(), true))
          {
            //return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCrisis24Page.InterventionLimitOfIndemnityTextboxId(),
                this.getData("Intervention - Limit Of Indemnity")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCrisis24Page.InterventionRateTextboxId(),
                this.getData("Intervention - Rate")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Cover Details entered successfully", false);

        return true;
      }

    private boolean enterEndorsements()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCrisis24Page.Crisis24RiskNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCrisis24Page.EndorsementsSelectButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCrisis24Page.EndorsementsAddAllButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCrisis24Page.EndorsementsApplyButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCrisis24Page.Crisis24RiskNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean Notes()
      {
        if (!SeleniumDriverInstance.enterTextById(EkunduCrisis24Page.Crisis24RiskItemCommentsTextboxId(),
                this.getData("Comments")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCrisis24Page.Crisis24RiskItemPolicyNotesCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCrisis24Page.Crisis24RiskItemNotesTextboxId(),
                this.getData("Notes")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCrisis24Page.Crisis24RiskNextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Notes entered successfully", false);

        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId()))
          {
            return false;
          }

        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
