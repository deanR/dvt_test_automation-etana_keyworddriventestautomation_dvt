/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduGroupedFireRiskTypeTest extends BaseClass
{
    TestEntity testData;
    TestResult testResult;
    
    public EkunduGroupedFireRiskTypeTest(TestEntity testData)
    {
        this.testData = testData;

    }
    
    public TestResult executeTest()
    {  
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if(!verifyAddRiskButtonisPresent())
        {
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to verify that the Add Risk button is present - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
       
        if(!enterIndustryDetails())
        {
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter industry details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if(!enterRiskDetails())
        {
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter risk details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if(!enterInsuredPerilsRatesDetails())
        {
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter insured perils rates details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if(!enterFireRiskData())
        {
             return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter fire risk data - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if(!enterFireMiscellaneousDataAndFirstAmountPayableDetails())
        {
             SeleniumDriverInstance.takeScreenShot( "Failed to enter miscellaneous and first amount payable fire data", true);
             return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter miscellaneous and first amount payable fire data - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if(!enterFireExtensionsDetails())
        {
             return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter fire extensions details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if(!enterBusinessInterruptionCoverDetails())
        {
             return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter business interruption cover details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if(!enterBusinessInterruptionFirstAmountPayableDetails())
        {
             return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter business interruption first amount payable data - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if(!enterBusinessInterruptionExtensionsDetails())
        {
             return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter business interruption extension data - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if(!addBusinessInterruptionSpecifiedCustomerAndSupplier())
        {
             return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter business interruption specified customer and supplier data - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if(!enterBusinessCombinedDetails())
        {
             return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter business combined data - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if(!enterARDetails())
        {
             return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter AR details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if(!enterOCDetails())
        {
             return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter OC details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if(!enterADDetails())
        {
             return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter AD details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if(!enterComment())
        {
             return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter policy comment - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
     

        return new TestResult(testData, Enums.ResultStatus.PASS, "Grouped Fire risk details successfully entered ", this.getTotalExecutionTime());
    } 
    
     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
   
    private boolean verifyAddRiskButtonisPresent()         
    {
            SeleniumDriverInstance.pause(2000);

        if(SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
        {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectGroupedFireRiskType();
            
            return true;
        }
        
        else if(SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            selectGroupedFireRiskType();
            return true;
        }
        
        else
        { 
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");
            return false;
        }
    }
 
    
   
    
    private boolean selectGroupedFireRiskType() 
    {        
        if(!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
        {
            return false;
        }

        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return false;
        }
            SeleniumDriverInstance.pause(10000);
            SeleniumDriverInstance.takeScreenShot( "Grouped Fire Risk type selected successfully", false);
            return true;
    }
    
    private boolean enterIndustryDetails() 
    {
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId()))
        {
            return false;
        }
    
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(), this.getData("Industry")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId()))
        {
            return false;
        }
        if(!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.IndustrySearchResultsSelectedRowId()))
        {
            return false;
        }
            SeleniumDriverInstance.pause(2000);

            SeleniumDriverInstance.takeScreenShot("Industry details entered successfully", false);
            
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
            
            return true;
    }
    
    private boolean enterRiskDetails() 
    {
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RSFireSectionsFireCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RSFireSectionsBusinessInterruptionCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RSFireSectionsBuildingsCombinedCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RSFireSectionsOfficeContentsCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RSFireSectionsAccountsRecievableCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RSFireSectionsAccidentalDamageCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.ManualReinsuranceLimitDropDownListId(), this.getData("Manual Insurance Limit")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.TypeOfConstructionDropDownListId(), this.getData("Type Of Construction")))
        {
            return false;
        }        
            SeleniumDriverInstance.takeScreenShot("Risk details entered successfully", false);
    
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
        return true;
        
    }
 
    private boolean enterInsuredPerilsRatesDetails() 
    {
        
        if(!SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.InsuredPerilsDataTableTitleLabelId()))
        {
            SeleniumDriverInstance.takeScreenShot("Insured perils page did not load correctly", true);
            return false;
        }
        
            SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.RatesFinalAgreedRateTextBoxId());
         
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.RatesFinalAgreedRateTextBoxId(), this.getData("Rates - Final Agreed Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RatesFinalAgreedRateTextBoxId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
        {
            return false;
        }
            SeleniumDriverInstance.takeScreenShot("Insured Perils Rates details entered successfully", false);
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
      
        return true;
    }
    
    private boolean enterFireRiskData() 
    {
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireRiskDataBuildingsCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireRiskDataBuildingsRiskSumInsuredTextBoxId(), this.getData("Fire - Risk Data  - Buildings Risk - Sum Insured")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireRiskDataRentCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireRiskDataRentRiskSumInsuredTextBoxId(), this.getData("Fire - Risk Data  - Rent Risk - Sum Insured")))
        {
            return false;
        }
//        
//        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireRiskDataPlantandMachCheckBoxId(), true))
//        {
//            return false;
//        }
//        
//        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireRiskDataPlantRiskSumInsuredTextBoxId(), this.getData("Fire - Risk Data  - Plant and Machinery - Sum Insured")))
//        {
//            return false;
//        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireRiskDataStockCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireRiskDataStockRiskSumInsuredTextBoxId(), this.getData("Fire - Risk Data  - Stock Risk - Sum Insured")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireRiskDataMiscCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireRiskNumberOfMonthsTextBoxId(), this.getData("Fire - Risk Data  - Number of Months")))
        {
            return false;
        }
       
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireBuildingsExcalationCheckBoxId(), true))
        {
            return false;
        }

        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireBuildingsExcalationPercentTextBoxId(), this.getData("Fire - Escalation - Buildings - Percentage")))
        {
            return false;
        }
        
//        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FirePlantMachineryExcalationCheckBoxId(), true))
//        {
//            return false;
//        }
//        
//        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FirePlantMachineryExcalationPercentTextBoxId(), this.getData("Fire - Escalation - Plant Machinery - Percentage")))
//        {
//            return false;
//        }
//        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireStockDeclarationCheckBoxId(), true))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Fire risk data entered successfully", false);
            return true;
    }
    
    private boolean enterFireMiscellaneousDataAndFirstAmountPayableDetails() 
    {
        if(!SeleniumDriverInstance.clickElementbyName(EkunduViewClientDetailsPage.AddMiscItemButtonName()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MiscItemDescriptionTextBoxId(), this.getData("Fire - Miscellaneous - Miscellaneous Item Description")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MiscItemSumInsuredTextBoxId(), this.getData("Fire - Miscellaneous -Miscellaneous Item Sum Insured")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }   

            SeleniumDriverInstance.scrollDownByOnePage(EkunduViewClientDetailsPage.FireFirstAmountPayableCheckBoxId());

        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireFirstAmountPayableCheckBoxId(), true))
        {
            return false;
        }

        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireMinimumPercentageFirstAmountPayableTextBoxId(), this.getData("Fire - First Amount Payable - Min Percentage")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireMinimumAmountFirstAmountPayableTextBoxId(), this.getData("Fire - First Amount Payable - Min Amount")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireMaximumAmountFirstAmountPayableTextBoxId(), this.getData("Fire - First Amount Payable - Max Amount")))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Fire Miscellaneous Data and FirstAmount Payable details entered successfully", false);
            return true;
    }
    
    private boolean enterFireExtensionsDetails() 
    {
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireClaimsPreparationCheckBoxId(), true))
        {
            return false;
        }
        
            SeleniumDriverInstance.pause(2000);
                
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireClaimsPrepLimitOfIndemnityTextBoxId(), this.getData("Fire - Claims Preparation - Limit of Indemnity")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireClaimsPrepRateTextBoxId(), this.getData("Fire - Claims Preparation - Rate")))
        {
            
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireDisposalOfSalvageCheckBoxId(), true))
        {
            return false;
        }
        
            SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireLeakageCheckBoxId(), true))
        {
            return false;
        }
        
            SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireLeakgeLimitOfIndemnityTextBoxId(), this.getData("Fire - Leakage - Limit of Indemnity")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireLeakgeRateTextBoxId(), this.getData("Fire - Leakage - Rate")))
        {
            
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireRiotAndStrikeCheckBoxId(), true))
        {
            return false;
        }
        
            SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireRiotPremiumTextBoxId(), this.getData("Fire - Riot - Premium")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireSubsidenceAndLandslipCheckBoxId(), true))
        {
            return false;
        }
        
            SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireSubsidenceLimitOfIndemnityTextBoxId(), this.getData("Fire - Subsidence - Limit of Indemnity")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireSubsidenceRateTextBoxId(), this.getData("Fire - Subsidence - Rate")))
        {
            
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.FireSubsidenceCoverTypeDropDownListId(), this.getData("Fire - Subsidence - Cover Type")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.FireSubsidenceGeoTechDropDownListId(), this.getData("Fire - Subsidence - GeoTech")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FireSubsidenceRateTextBoxId()))
        {
            
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
            SeleniumDriverInstance.takeScreenShot("Fire extension details entered successfully", false);
           // SeleniumDriverInstance.pressKeyOnElementById(EkunduViewClientDetailsPage.NextButtonId(), Keys.ENTER);
         
            return true;
    }
    
    private boolean enterBusinessInterruptionCoverDetails() 
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.BusinessInterruptionOverviewTypeOfCoverDropDownListId(), this.getData("Business Interruption - Overview - Type of Cover")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.BIOverviewIndemnityPeriodDropDownListId(), this.getData("Business Interruption - Overview - Indemnity Period")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIOverviewConversionFactorTextBoxId(), this.getData("Business Interruption - Overview - Conversion Factor")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BICoverGrossProfitBasisCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BICoverGrossProfitBasisSumInsuredTextBoxId(), this.getData("Business Interruption - Cover - Gross Profit Basis - Sum Insured")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BICoverGrossRentalsCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BICoverGrossRentalsMonthsTextBoxId(), this.getData("Business Interruption - Cover - Gross Rentals - Months")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BICoverGrossRentalsSumInsuredTextBoxId(), this.getData("Business Interruption - Cover - Gross Rentals - Sum Insured")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BICoverRevenueCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BICoverRevenueSumInsuredTextBoxId(), this.getData("Business Interruption - Cover - Revenue - Sum Insured")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BICoverAdditionalIncreaseCostOfWorkingCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.BICoverAdditionalIncreaseCostOfWorkingTypeDropDownListId(), this.getData("Business Interruption - Cover - Additional Increased Cost of Working - Type")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BICoverAdditionalIncreaseCostOfWorkingSumInsuredTextBoxId(), this.getData("Business Interruption - Cover - Additional Increased Cost of Working - Sum Insured")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BICoverWagesCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.BICoverWagesMonthsDropDownListId(), this.getData("Business Interruption - Cover - Wages - Months")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BICoverWagesSumInsuredTextBoxId(), this.getData("Business Interruption - Cover - Wages - Sum Insured")))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Business interruption cover details entered successfully", false);
            return true;
    }
    
    private boolean enterBusinessInterruptionFirstAmountPayableDetails() 
    {
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BIFirstAmountPayableCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIFirstAmountPayableMinPercentageTextBoxId(), 
                this.getData("Business Interruption - First Amount Payable - Min Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIFirstAmountPayableMinAmountTextBoxId(), 
                this.getData("Business Interruption - First Amount Payable - Min Amount")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIFirstAmountPayableMaxAmountTextBoxId(),
                this.getData("Business Interruption - First Amount Payable - Max Amount")))
        {
            return false;
        }
 
            return true;
    }
  
    private boolean enterBusinessInterruptionExtensionsDetails() 
    {
       
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BIExtensionsAccidentalDamageCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsAccidentalDamageLimitTextBoxId(), this.getData("Business Interruption - Extensions - Accidental Damage - Limit")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsAccidentalDamageRateTextBoxId(), this.getData("Business Interruption - Extensions - Accidental Damage - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsAccidentalDamageFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Accidental Damage - FAP Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsAccidentalDamageFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Accidental Damage - FAP Amount")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BIExtensionsClaimsPreparationCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsClaimsLimitTextBoxId(), this.getData("Business Interruption - Extensions - Claims - Limit")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsClaimsRateTextBoxId(), this.getData("Business Interruption - Extensions - Claims - Rate")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BIExtensionsDisposalOfSalvageCheckBoxId(), true))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BIExtensionsFinesAndPenaltiesCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsFinesLimitTextBoxId(), this.getData("Business Interruption - Extensions - Fines - Limit")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsFinesRateTextBoxId(), this.getData("Business Interruption - Extensions - Fines - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsFinesFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Fines - FAP Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsFinesFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Fines - FAP Amount")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BIExtensionsPreventionOfAccessCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsPreventionOfAccesRateTextBoxId(), this.getData("Business Interruption - Extensions - Prevention of Access - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsPreventionOfAccesFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Prevention of Access - FAP Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsPreventionOfAccesFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Prevention of Access - FAP Amount")))
        {
            return false;
        }
        
            SeleniumDriverInstance.scrollDownByOnePage(EkunduViewClientDetailsPage.BIExtensionsPublicTelecommunicationsExtendedCheckBoxId());
         
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BIExtensionsPublicTelecommunicationsExtendedCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsPublicTelecommunicationsExtendedRateTextBoxId(), this.getData("Business Interruption - Extensions - Public Telecom Extended - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsPublicTelecommunicationsExtendedFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Public Telecom Extended - FAP Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsPublicTelecommunicationsExtendedFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Public Telecom Extended - FAP Amount")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BIExtensionsPublicUtilitiesExtendedCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsPublicUtilitiesExtendedRateTextBoxId(), this.getData("Business Interruption - Extensions - Public Util Extended - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsPublicUtilitiesExtendedFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Public Util Extended - FAP Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsPublicUtilitiesExtendedFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Public Util Extended - FAP Amount")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BIExtensionsSpecifiedCustomersCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsSpecifiedCustomersRateTextBoxId(), this.getData("Business Interruption - Extensions - Specific Customers - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsSpecifiedCustomersFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Specific Customers - FAP Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsSpecifiedCustomersFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Specific Customers - FAP Amount")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BIExtensionsSpecifiedSuppliersCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsSpecifiedSuppliersRateTextBoxId(), this.getData("Business Interruption - Extensions - Specific Suppliers - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsSpecifiedSuppliersFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Specific Suppliers - FAP Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsSpecifiedSuppliersFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Specific Suppliers - FAP Amount")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BIExtensionsUnSpecifiedCustomersCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsUnSpecifiedCustomersRateTextBoxId(), this.getData("Business Interruption - Extensions - Un Specified Customers - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsUnSpecifiedCustomersFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Un Specified Customers - FAP Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsUnSpecifiedCustomersFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Un Specified Customers - FAP Amount")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BIExtensionsUnSpecifiedSuppliersCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsUnSpecifiedSuppliersLimitOfIndemnityTextBoxId(), this.getData("Business Interruption - Extensions - Un Specified Suppliers - Limit")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsUnSpecifiedSuppliersRateTextBoxId(), this.getData("Business Interruption - Extensions - Un Specified Suppliers - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsUnSpecifiedSuppliersFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Un Specified Suppliers - FAP Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BIExtensionsUnSpecifiedSuppliersFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Un Specified Suppliers - FAP Amount")))
        {
            return false;
        }
  
            return true;
    }
    
    private boolean addBusinessInterruptionSpecifiedCustomerAndSupplier() 
    {
            SeleniumDriverInstance.scrollDownByOnePage(EkunduViewClientDetailsPage.BIExtensionsUnSpecifiedSuppliersFAPAmountTextBoxId());
        
        if(!SeleniumDriverInstance.clickElementbyName(EkunduViewClientDetailsPage.BIAddSpecifiedSupplierButtonName()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BISpecifiedSuppliersSupplierNameTextBoxId(), this.getData("Business Interruption - Add Specified Supplier - Name")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BISpecifiedSuppliersSupplierPercentageTextBoxId(), this.getData("Business Interruption - Add Specified Supplier - Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
        
            SeleniumDriverInstance.scrollDownByOnePage(EkunduViewClientDetailsPage.BIExtensionsUnSpecifiedSuppliersFAPAmountTextBoxId());

            SeleniumDriverInstance.scrollDownByOnePage(EkunduViewClientDetailsPage.BIExtensionsUnSpecifiedSuppliersFAPAmountTextBoxId());

        if(!SeleniumDriverInstance.clickElementbyName(EkunduViewClientDetailsPage.BIAddSpecifiedCustomerButtonName()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BISpecifiedCustomersCustomerNameTextBoxId(), this.getData("Business Interruption - Add Specified Customer - Name")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BISpecifiedCustomersCustomerPercentageTextBoxId(), this.getData("Business Interruption - Add Specified Customer - Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
        
            SeleniumDriverInstance.scrollDownByOnePage(EkunduViewClientDetailsPage.BIExtensionsUnSpecifiedSuppliersFAPAmountTextBoxId());
        
            SeleniumDriverInstance.scrollDownByOnePage(EkunduViewClientDetailsPage.BIExtensionsUnSpecifiedSuppliersFAPAmountTextBoxId());
        
            SeleniumDriverInstance.takeScreenShot("Business interruption specified customer and supplier details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }

        return true;
    }
    
    private boolean enterBusinessCombinedDetails() 
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.BCBuildingsCombinedOccupationDropDownListId(), 
                this.getData("Business Combined - Buildings Combined - Occupation")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCBuildingsCombinedSumInsuredTextBoxId(), 
                this.getData("Business Combined - Buildings Combined - Sum Insured")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCBuildingsCombinedAgreedRateTextBoxId(), 
                this.getData("Business Combined - Buildings Combined - Agreed Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BCBuildingsCombinedEscalationInflationCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCBuildingsCombinedEscalationPercentageTextBoxId(), 
                this.getData("Business Combined - Buildings Combined -  Escalation - Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCBuildingsCombinedInflation1stYearPercentageTextBoxId(),
                this.getData("Business Combined - Buildings Combined -  Infation - 1st Year Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCBuildingsCombinedInflation2ndYearPercentageTextBoxId(), 
                this.getData("Business Combined - Buildings Combined -  Infation - 2nd Year Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BCMiscellaneousItemsOverviewCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BCAdditionalRentCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCAdditionalRentSumInsuredTextBoxId(), 
                this.getData("Business Combined - Additional Rent - Sum Insured")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCAdditionalRentRateTextBoxId(), this.getData("Business Combined - Additional Rent - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyName(EkunduViewClientDetailsPage.BCAddMiscellaneousItemButtonName()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCMiscellaneousItemDescriptionTextBoxId(), this.getData("Business Combined - Add Miscellaneous Item - Description")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCMiscellaneousItemSumInsuredTextBoxId(), this.getData("Business Combined - Add Miscellaneous Item - Sum Insured")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCMiscellaneousItemRateTextBoxId(), this.getData("Business Combined - Add Miscellaneous Item - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        } 
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BCFirstAmountPayableCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCFirstAmountPayablePercentageTextBoxId(), this.getData("Business Combined - First Amount Payable - Min Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCFirstAmountPayableAmountTextBoxId(), this.getData("Business Combined - First Amount Payable -Min Amount")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCFirstAmountPayableMaximumTextBoxId(), this.getData("Business Combined - First Amount Payable - Max Amount")))
        {
            return false;
        }
          
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BCExtentionsClaimsPreparationCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCExtentionsClaimsPreparationLimitTextBoxId(), this.getData("Business Combined - Extensions - Claims Preparation - Limit")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BCExtentionsPreventionOfAccessCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BCExtentionsRiotCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCExtentionsRiotPremiumTextBoxId(), this.getData("Business Combined - Extensions - Riot - Premium")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCExtentionsRiotFAPPercentageTextBoxId(), this.getData("Business Combined - Extensions - Riot - FAP Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCExtentionsRiotFAPAmountTextBoxId(), this.getData("Business Combined - Extensions - Riot - FAP Amount")))
        {
            return false;
        }

        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BCExtentionsSubsidenceCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCExtentionsSubsidenceLimitTextBoxId(), this.getData("Business Combined - Extensions - Subsidence - Limit")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCExtentionsSubsidenceRateTextBoxId(), this.getData("Business Combined - Extensions - Subsidence - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCExtentionsSubsidenceFAPPercentageTextBoxId(), this.getData("Business Combined - Extensions - Subsidence - FAP Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCExtentionsSubsidenceFAPAmountTextBoxId(), this.getData("Business Combined - Extensions - Subsidence - FAP Amount")))
        {
            return false;
        }
        
            SeleniumDriverInstance.scrollDownByOnePage(EkunduViewClientDetailsPage.BCExtentionsSubsidenceFAPAmountTextBoxId());
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.BCExtentionsSubsidenceCoverTypeDropDownListId(), this.getData("Business Combined - Extensions - Subsidence - Cover Type")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.BCExtentionsSubsidenceGeoTechDropDownListId(), this.getData("Business Combined - Extensions - Subsidence - GeoTech")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.BCExtentionsSubsidenceFAPAmountTextBoxId()))
        {
            return false;
        }

        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Business combined details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        } 
 
            return true;   
    }
    
    private boolean enterARDetails() 
    {
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.AROutstandingDebitBalancesCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.AROutstandingDebitBalancesSumInsuredTextBoxId(), this.getData("AR - Outstanding Debit Balances - Sum Insured")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ARFirstAmountPayableCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ARFirstAmountPayableMinPercentageTextBoxId(), this.getData("AR - First Amount Payable - Min Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ARFirstAmountPayableMinAmountTextBoxId(), this.getData("AR - First Amount Payable - Min Amount")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ARFirstAmountPayableMaxAmountTextBoxId(), this.getData("AR - First Amount Payable - Max Amount")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ARExtensionsClaimsCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ARExtensionsClaimsLimitTextBoxId(), this.getData("AR - Extensions  - Claims - Limit")))
        {
            return false;
        }

        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ARExtensionsDuplicateRecordsCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ARExtensionsProtectionsCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ARExtensionsRiotCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ARExtensionsRiotPremiumTextBoxId(), this.getData("AR - Extensions - Riot - Premium")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ARExtensionsFAPPercentageTextBoxId(), this.getData("AR - Extensions - Riot - FAP Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ARExtensionsFAPAmountTextBoxId(), this.getData("AR - Extensions - Riot - FAP Amount")))
        {
            return false;
        }
      
        SeleniumDriverInstance.scrollElementIntoViewById(EkunduViewClientDetailsPage.RateButtonId());
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
        {
            return false;
        } 
        
        SeleniumDriverInstance.scrollElementIntoViewById(EkunduViewClientDetailsPage.NextButtonId());
        
        
            SeleniumDriverInstance.takeScreenShot("AR details entered successfully", false);
            SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId());


            return true;   
    }
    
    private boolean enterOCDetails() 
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.OCAlarmWarrantyDropDownListId(), this.getData("OC - Alarm Warranty")))
        {
            return false;
        }
        
        // Cover
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.OCCoverOfficeContentsCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCCoverOfficeContentsSumInsuredTextBoxId(), this.getData("OC - Cover - Office Contents - Sum Insured")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.OCCoverLossOfDocumentsCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCCoverLossOfDocumentsSumInsuredTextBoxId(), this.getData("OC - Cover - Loss of Documentation - Sum Insured")))
        {
            return false;
        }

        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.OCCoverLiabilityForDocumentsCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCCoverLiabilityForDocumentsSumInsuredTextBoxId(), this.getData("OC - Cover - Liability for Documents - Sum Insured")))
        {
            return false;
        }

        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.OCCoverTheftNonForcibleCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCCoverTheftNonForcibleSumInsuredTextBoxId(), this.getData("OC - Cover - Theft Non Forcible - Sum Insured")))
        {
            return false;
        }

        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.OCCoverTheftByForcibleEntryCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCCoverTheftForcibleSumInsuredTextBoxId(), this.getData("OC - Cover - Theft by Forcible Entry - Sum Insured")))
        {
            return false;
        }

        // First Amount Payable
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.OCFirstAmountPayableOfficeContentsCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCFirstAmountPayableOfficeContentsMinPercentageTextBoxId(), this.getData("OC - First Amount Payable - Office Contents - Min Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCFirstAmountPayableOfficeContentsMinAmountTextBoxId(), this.getData("OC - First Amount Payable - Office Contents - Min Amount")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCFirstAmountPayableOfficeContentsMaxAmountTextBoxId(), this.getData("OC - First Amount Payable - Office Contents - Max Amount")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.OCFirstAmountPayableLossofDocumentsCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCFirstAmountPayableLossOfDocumentsMinPercentageTextBoxId(), this.getData("OC - First Amount Payable - Loss of Documents - Min Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCFirstAmountPayableLossOfDocumentsMinAmountTextBoxId(), this.getData("OC - First Amount Payable - Loss of Documents - Min Amount")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCFirstAmountPayableLossOfDocumentsMaxAmountTextBoxId(), this.getData("OC - First Amount Payable - Loss of Documents - Max Amount")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.OCFirstAmountPayableLiabilityForDocumentsCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCFirstAmountPayableLiabilityForDocumentsMinPercentageTextBoxId(), this.getData("OC - First Amount Payable - Liability for Documents - Min Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCFirstAmountPayableLiabilityForDocumentsMinAmountTextBoxId(), this.getData("OC - First Amount Payable - Liability for Documents - Min Amount")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCFirstAmountPayableLiabilityForDocumentsMaxAmountTextBoxId(), this.getData("OC - First Amount Payable - Liability for Documents - Max Amount")))
        {
            return false;
        }

        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.OCFirstAmountPayableTheftNonForcibleCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCFirstAmountPayableTheftNonForcibleMinPercentageTextBoxId(), this.getData("OC - First Amount Payable - Theft non Forcible - Min Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCFirstAmountPayableTheftNonForcibleMinAmountTextBoxId(), this.getData("OC - First Amount Payable - Theft non Forcible - Min Amount")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCFirstAmountPayableTheftNonForcibleMaxAmountTextBoxId(), this.getData("OC - First Amount Payable - Theft non Forcible - Max Amount")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.OCFirstAmountPayableTheftByForcibleEntryCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCFirstAmountPayableTheftForcibleMinPercentageTextBoxId(), this.getData("OC - First Amount Payable - Theft Forcible - Min Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCFirstAmountPayableTheftForcibleMinAmountTextBoxId(), this.getData("OC - First Amount Payable - Theft Forcible - Min Amount")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCFirstAmountPayableTheftForcibleMaxAmountTextBoxId(), this.getData("OC - First Amount Payable - Theft Forcible - Max Amount")))
        {
            return false;
        }

        //Extensions
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.OCExtentionsAdditionalIncresedCostOfWorkingCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCExtensionsAdditionalIncreasedCostLimitTextBoxId(), this.getData("OC - Extensions - Additional Increased Cost - Limit")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCExtensionsAdditionalIncreasedCostRateTextBoxId(), this.getData("OC - Extensions - Additional Increased Cost - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCExtensionsAdditionalIncreasedCostFAPPercentageTextBoxId(), this.getData("OC - Extensions - Additional Increased Cost - FAP Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCExtensionsAdditionalIncreasedCostFAPAmountTextBoxId(), this.getData("OC - Extensions - Additional Increased Cost - FAP Amount")))
        {
            return false;
        }

        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.OCExtentionsClaimsPreparationCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCExtensionsClaimsLimitTextBoxId(), this.getData("OC - Extensions - Claims - Limit")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCExtensionsClaimsRateTextBoxId(), this.getData("OC - Extensions - Claims - Rate")))
        {
            return false;
        }
 
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.OCExtentionsFirstLossAverageCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.OCExtentionsMaliciousDamageCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCExtensionsMaliciousDamageLimitTextBoxId(), this.getData("OC - Extensions - Malicious Damage - Limit")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCExtensionsMaliciousDamageRateTextBoxId(), this.getData("OC - Extensions - Malicious Damage - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCExtensionsMaliciousDamageFAPPercentageTextBoxId(), this.getData("OC - Extensions - Malicious Damage - FAP Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCExtensionsMaliciousDamageFAPAmountTextBoxId(), this.getData("OC - Extensions - Malicious Damage - FAP Amount")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.OCExtentionsRiotCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCExtensionsRiotPremiumTextBoxId(), this.getData("OC - Extensions - Riot - Premium")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCExtensionsRiotFAPPercentageTextBoxId(), this.getData("OC - Extensions - Riot - FAP Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCExtensionsRiotFAPAmountTextBoxId(), this.getData("OC - Extensions - Riot - FAP Amount")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OCExtensionsRiotFAPPercentageTextBoxId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("OC details entered successfully", false);
            
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        } 
        
     return true;   
    }
    
    private boolean enterADDetails() 
    {
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADCoverAccidentalDamageCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.ADAccidentalDamageBasisOfCoverDropDownListId(), this.getData("AD - Accidental Damage Basis of Cover")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADCoverAccidentalDamageFullValueTextBoxId(), this.getData("AD -  Cover - Accidental Damage - Full value")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADCoverAccidentalDamageFirstLossTextBoxId(), this.getData("AD -  Cover - Accidental Damage - First loss")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADCoverLeakageCheckBoxId(), true))
        {
            return false;
        }

        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADCoverLeakageFirstLossTextBoxId(), this.getData("AD -  Cover - Leakage - First loss")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADFirstAmountPayableAccidentalDamageCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADFirstAmountPayableAccidentalDamageMinPercentageTextBoxId(), this.getData("AD -  First Amount Payable - Accidental Damage - Min Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADFirstAmountPayableAccidentalDamageMinAmountTextBoxId(), this.getData("AD -  First Amount Payable - Accidental Damage - Min Amount")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADFirstAmountPayableAccidentalDamageMaxAmountTextBoxId(), this.getData("AD -  First Amount Payable - Accidental Damage - Max Amount")))
        {
            return false;
        }

        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADFirstAmountPayableLeakageCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADFirstAmountPayableLeakageMinPercentageTextBoxId(), this.getData("AD -  First Amount Payable - Leakage - Min Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADFirstAmountPayableLeakageMinAmountTextBoxId(), this.getData("AD -  First Amount Payable - Leakage - Min Amount")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADFirstAmountPayableLeakageMaxAmountTextBoxId(), this.getData("AD -  First Amount Payable - Leakage - Max Amount")))
        {
            return false;
        }
 
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADExtensionsAdditionalIncreasedCostOfWorkingCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADExtensionsAdditionalIncreasedCostLimitTextBoxId(), this.getData("AD - Extensions - Additional Increased Cost - Limit")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADExtensionsAdditionalIncreasedCostRateTextBoxId(), this.getData("AD - Extensions - Additional Increased Cost - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADExtensionsAdditionalIncreasedCostFAPPercentageTextBoxId(), this.getData("AD - Extensions - Additional Increased Cost - FAP Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADExtensionsAdditionalIncreasedCostFAPAmountTextBoxId(), this.getData("AD - Extensions - Additional Increased Cost - Fap Amount")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADExtensionsClaimsPreparationCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADExtensionsClaimsPreparationLimitTextBoxId(), this.getData("AD - Extensions - Claims Preparation - Limit")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADExtensionsClaimsPreparationRateTextBoxId(), this.getData("AD - Extensions - Claims Preparation - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADMemorandaReinstatementCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADMemorandaFirstLossAverageCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ADExtensionsClaimsPreparationRateTextBoxId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("AD details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }

            return true;
    }
    
    private boolean enterComment() 
    {       
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.NotesCommentsTextBoxId(), this.getData("Policy Comment")))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Policy comments entered successfully", false);
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        
        SeleniumDriverInstance.pause(1000);
        
        SeleniumDriverInstance.clickElementbyLinkText("Documents");
        
        SeleniumDriverInstance.pause(1000);
        
         SeleniumDriverInstance.clickElementById("btnGeneratePdf");
         
         SeleniumDriverInstance.pause(1000);
         
         SeleniumDriverInstance.clickElementById("popup_ok");
         
         
         SeleniumDriverInstance.pause(1000);
         
         SeleniumDriverInstance.clickElementbyLinkText("Risks");
          
            return true;   
    }
     
    private boolean verifyRatingDetailsPageHasLoaded() 
    {      
        if(!SeleniumDriverInstance.validateElementTextValueById(EkunduViewClientDetailsPage.RatingDetailsLabelId(),"Rating Details"))
        {
            return false;
        }
            SeleniumDriverInstance.takeScreenShot("Rating Details page loaded successfully", false);
            return true;   
    }
    
}
