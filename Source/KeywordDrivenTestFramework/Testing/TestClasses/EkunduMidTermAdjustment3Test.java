
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduMidTermAdjustment3Test extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduMidTermAdjustment3Test(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        //SeleniumDriverInstance.pause(10000);
        if (!verifyAddRiskButtonisPresent())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk button is present", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the Add Risk button is present - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterIndustryDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter industry details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter industry details - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterRiskSelectionDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk selection details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk selection details - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterOCDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter office contents details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter office contents details - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!copyGroupedFireRisk())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to copy grouped fire risk", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to copy grouped fire risk - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Mid Term Adjustment completed successfully",
                              this.getTotalExecutionTime());

      }
 
    public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
 

    private boolean enterIndustryDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(),
                this.getData("Industry")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.IndustrySearchRowId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Industry details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterRiskSelectionDetails()
      {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RSFireSectionsOfficeContentsCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.TypeOfConstructionDropDownListId(),
                this.getData("Type Of Construction")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk selection details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterOCDetails()
      {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.OCAlarmWarrantyDropDownListId(),
                this.getData("OC - Alarm Warranty")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OfficeContentsFlatPremiumCheckBoxId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OCCoverOfficeContentsCheckBoxId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCCoverOfficeContentsSumInsuredTextBoxId(),
                this.getData("OC - Cover - Office Contents - Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.OCCoverOfficeContentsPremiumTextBoxId(),
                this.getData("OC - Cover - Office Contents - Premium")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Office Contents data entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;
      }

    private boolean copyGroupedFireRisk()
      {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.GroupedFireRiskActionDropdownListXpath(),
                "Copy"))
          {
            return false;
          }
        

        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.CopyRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.CopyRiskDropDownListId(),
                this.getData("Copy Risk - Risk Type")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.CopyRiskSubmitButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }
        
        SeleniumDriverInstance.Driver.navigate().refresh();

        return true;
      }

    private boolean verifyAddRiskButtonisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
        System.out.println("Add risk button clicked.");

        return selectGroupedFireRiskType();

      }

    private boolean selectGroupedFireRiskType()
      {
        if (!SeleniumDriverInstance.switchToLastDuplicateFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);
        SeleniumDriverInstance.takeScreenShot("Grouped Fire Risk type selected successfully", false);

        return true;

      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
