/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduElectronicEquipmentRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author ferdinandN
 */
public class MotorTradersExternalRiskTest extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public MotorTradersExternalRiskTest(TestEntity testData) {
        this.testData = testData;

    }

    public TestResult executeTest() {
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the add risk button is present"), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the add risk button is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterSubSectionAData()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter Sub Section A data"), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Sub Section A data- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterSubSectionBData()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter Sub Section B data"), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Sub Section B data- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterExtensions()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk extensions"), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter risk extensions- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterNotes()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk notes"), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter risk notes- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        SeleniumDriverInstance.takeScreenShot(("Motor Traders External risk added successfully"), false);
        return new TestResult(testData, Enums.ResultStatus.PASS, "Motor Traders External risk added successfully", this.getTotalExecutionTime());

    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean verifyAddRiskButtonisPresent() {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectMotorTradersExternalRiskType();
            return true;
        } else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            selectMotorTradersExternalRiskType();
            return true;
        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");
            return false;
        }
    }

    private boolean selectMotorTradersExternalRiskType() {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.takeScreenShot("Motor Traders Internal Risk type selected successfully", false);

        return true;
    }

    private boolean enterSubSectionAData() {

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.CoverVoluntarySubsectionBCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.CoverVoluntarySubsectionCCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.CoverVoluntaryFAPPercentageTextBoxId(), this.getData("Cover - Voluntary FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.CoverVoluntaryFAPAmountTextBoxId(), this.getData("Cover - Voluntary FAP Amount"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Cover detaills entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SubSectionASumInsuredTextBoxId(), this.getData("SubSectionA - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SubSectionARateTextBoxId(), this.getData("SubSectionA - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SubSectionAFAPPercTextBoxId(), this.getData("SubSectionA - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SubSectionAFAPAmountTextBoxId(), this.getData("SubSectionA - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SubSectionAAnnualWageAmountTextBoxId(), this.getData("SubSectionA - Annual Wages Amount"))) {
            return false;
        }

//        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.SubSectionATypeOfWages(), this.getData("SubSectionA - Type Of Wages")))
//        {
//            return false;
//        }
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.SubSectionAThirdPartyOnlyCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.SubSectionAFireAndTheftCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.SubSectionADeletionOfDemonstrationCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.SubSectionAExclusionOfOwnVehiclesCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Section A data entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;

    }

    private boolean enterSubSectionBData() {
        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SubSectionBLimitofIndemnityTextBoxId(), this.getData("SubSection B - Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SubSectionBRateTextBoxId(), this.getData("SubSection B - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SubSectionBFAPAmountTextBoxId(), this.getData("SubSection B - FAP Amount"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("SubSection B data entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;

    }

    private boolean enterExtensions() {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsContingentLiabilityCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsContingentLiabilityLOITextBoxId(), this.getData("Extensions - Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsContingentLiabilityRateTextBoxId(), this.getData("Extensions - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsContingentLiabilityFAPPercTextBoxId(), this.getData("Extensions - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsContingentLiabilityFAPAmountTextBoxId(), this.getData("Extensions - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsLossofKeysCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsLossofKeysFAPPercTextBoxId(), this.getData("Extensions - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsLossofKeysFAPAmountTextBoxId(), this.getData("Extensions - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsLossofUseCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsLossofUsePremiumTextBoxId(), this.getData("Extensions - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsLossofUseFAPPercTextBoxId(), this.getData("Extensions - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsLossofUseFAPAmountTextBoxId(), this.getData("Extensions - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsMotorCycleCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsMotorCycleFAPPercTextBoxId(), this.getData("Extensions - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsMotorCycleFAPAmountTextBoxId(), this.getData("Extensions - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsPassangerLiabilityCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsPassangerLiabilityLOITextBoxId(), this.getData("Extensions - Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsPassangerLiabilityRateTextBoxId(), this.getData("Extensions - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsPassangerLiabilityFAPPercTextBoxId(), this.getData("Extensions - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsPassangerLiabilityFAPAmountTextBoxId(), this.getData("Extensions - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionSocialDomesticCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsSocialDomesticPremiumTextBoxId(), this.getData("Extensions - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsSocialDomesticFAPPercTextBoxId(), this.getData("Extensions - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsSocialDomesticFAPAmountTextBoxId(), this.getData("Extensions - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionSpecialTypeVehiclesCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsSpecialTypeVehiclesFAPPercTextBoxId(), this.getData("Extensions - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsSpecialTypeVehiclesFAPAmountTextBoxId(), this.getData("Extensions - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionUnauthorisedUseCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsUnauthorisedUsePremiumTextBoxId(), this.getData("Extensions - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsUnauthorisedUseFAPPercTextBoxId(), this.getData("Extensions - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsUnauthorisedUseFAPAmountTextBoxId(), this.getData("Extensions - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionVehicleLentCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsVehicleLentPremiumTextBoxId(), this.getData("Extensions - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsVehicleLentFAPPercTextBoxId(), this.getData("Extensions - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsVehicleLentFAPAmountTextBoxId(), this.getData("Extensions - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionVehicleLentCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsVehicleLentPremiumTextBoxId(), this.getData("Extensions - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsVehicleLentFAPPercTextBoxId(), this.getData("Extensions - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsVehicleLentFAPAmountTextBoxId(), this.getData("Extensions - FAP Amount"))) {
            return false;
        }

//      if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionWindscreenCoverCheckBoxId(), true))
//        {
//            return false;
//        }
//         
//             
//     if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsWindscreenCoverPremiumTextBoxId(), this.getData("Extensions - Premium"))) 
//        {
//            return false;
//        }
//     
//     if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsWindscreenCoverFAPPercTextBoxId(), this.getData("Extensions - FAP Percentage"))) 
//        {
//            return false;
//        }
//     
//      
//     if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsWindscreenCoverFAPAmountTextBoxId(), this.getData("Extensions - FAP Amount"))) 
//        {
//            return false;
//        }
//     
//        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionWreckageRemovalCheckBoxId(), true))
//        {
//            return false;
//        }
//         
//             
//     if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsWreckageRemovalPremiumTextBoxId(), this.getData("Extensions - Premium"))) 
//        {
//            return false;
//        }
//     
//     if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsWreckageRemovalFAPPercTextBoxId(), this.getData("Extensions - FAP Percentage"))) 
//        {
//            return false;
//        }
//     
//      
//     if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExternalExtensionsWreckageRemovalFAPAmountTextBoxId(), this.getData("Extensions - FAP Amount"))) 
//        {
//            return false;
//        }
        SeleniumDriverInstance.takeScreenShot("Risk extensions entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterNotes() {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskCommentsTextboxId(), this.getData("Risk Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskPolicyNotesTextboxId(), this.getData("Risk Schedule Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Comments entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Documents");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementById("btnGeneratePdf");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementById("popup_ok");
         
         
         SeleniumDriverInstance.pause(1000);
         
        SeleniumDriverInstance.clickElementbyLinkText("Risks");

        return true;
    }
}
