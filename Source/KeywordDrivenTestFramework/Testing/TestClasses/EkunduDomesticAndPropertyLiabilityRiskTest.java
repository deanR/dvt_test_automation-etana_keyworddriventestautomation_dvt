
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduDomesticAndPropertyLiabilityRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author deanR
 */
public class EkunduDomesticAndPropertyLiabilityRiskTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduDomesticAndPropertyLiabilityRiskTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!verifyRisksDialogisPresent())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the risks dialod is presenet"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to select risk type - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!addSurveyType())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter survey type"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter survey type - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterRiskCoverage())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk coverage"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk coverage - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterBuildingsDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter building details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter building details- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterInterestedParties())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter interested parties"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter interested parties- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterContents())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter contents"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter contents- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterEndorsements())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter endorsements"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter endorsements- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Domestic and Property Liability risk added successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Domestic and Property Liability risk added successfully",
                              this.getTotalExecutionTime());

      }

    public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean verifyRisksDialogisPresent()
      {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectDomesticandPropertyLiabilityRiskType();

            return true;
          }
        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectDomesticandPropertyLiabilityRiskType();

            return true;
          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err
                .println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

      }

    private boolean selectDomesticandPropertyLiabilityRiskType()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(10000);

        SeleniumDriverInstance.takeScreenShot("Domestic and Property Liability Risk type selected successfully", false);

        return true;
      }

    private boolean addSurveyType()
      {
        if (!SeleniumDriverInstance.clickElementbyName(
                EkunduDomesticAndPropertyLiabilityRiskPage.AddSurveyButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduDomesticAndPropertyLiabilityRiskPage.SelectSurveyTypeDropdownListId(),
                testData.TestParameters.get("Survey Type")))
          {
            return false;
          }

        SeleniumDriverInstance.clearTextById(EkunduDomesticAndPropertyLiabilityRiskPage.SurveyDateTextBoxId());

        if (!SeleniumDriverInstance.enterTextById(EkunduDomesticAndPropertyLiabilityRiskPage.SurveyDateTextBoxId(),
                testData.TestParameters.get("Date")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduDomesticAndPropertyLiabilityRiskPage.SurveyReferenceTextBoxId(),
                testData.TestParameters.get("Reference")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduDomesticAndPropertyLiabilityRiskPage.SurveyNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduDomesticAndPropertyLiabilityRiskPage.SurveyReportCommentsTextAreaId(),
                testData.TestParameters.get("Comments")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Survey type entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduDomesticAndPropertyLiabilityRiskPage.SurveyNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduDomesticAndPropertyLiabilityRiskPage.SurveyNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterRiskCoverage()
      {
        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduDomesticAndPropertyLiabilityRiskPage.CheckBuildingCheckBoxId(), true))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduDomesticAndPropertyLiabilityRiskPage.CheckContentsCheckBoxId(), true))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduDomesticAndPropertyLiabilityRiskPage.TypeofResidenceDropdownListId(),
                testData.TestParameters.get("Type of Residence")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduDomesticAndPropertyLiabilityRiskPage.TypeofConstructionDropdownListId(),
                testData.TestParameters.get("Type of Construction")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduDomesticAndPropertyLiabilityRiskPage.OccupancyDropdownListId(),
                testData.TestParameters.get("Occupancy")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduDomesticAndPropertyLiabilityRiskPage.LocalityDropdownListId(),
                testData.TestParameters.get("Locality")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk coverage entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduDomesticAndPropertyLiabilityRiskPage.SurveyNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterBuildingsDetails()
      {
        if (!SeleniumDriverInstance.enterTextById(
                EkunduDomesticAndPropertyLiabilityRiskPage.buildingSumInsuredTextBoxId(),
                testData.TestParameters.get("Building Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduDomesticAndPropertyLiabilityRiskPage.SurveyRateButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Building details entered successfully", false);

        return true;
      }

    private boolean enterInterestedParties()
      {
        if (!SeleniumDriverInstance.clickElementbyName(
                EkunduDomesticAndPropertyLiabilityRiskPage.AddInterestedPartiesButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduDomesticAndPropertyLiabilityRiskPage.TypeofAgreementTypeDropdownListId(),
                testData.TestParameters.get("Type of Agreement")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduDomesticAndPropertyLiabilityRiskPage.InstitutionNameDropdownListId(),
                testData.TestParameters.get("Institution Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduDomesticAndPropertyLiabilityRiskPage.SurveyNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduDomesticAndPropertyLiabilityRiskPage.InterestedPartiesCommentsTextAreaId(),
                testData.TestParameters.get("Comments")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Interested parties details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduDomesticAndPropertyLiabilityRiskPage.SurveyNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduDomesticAndPropertyLiabilityRiskPage.SurveyNextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterContents()
      {
        if (!SeleniumDriverInstance.enterTextById(
                EkunduDomesticAndPropertyLiabilityRiskPage.ContentsSumInsuredTextBoxId(),
                testData.TestParameters.get("Contents Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduDomesticAndPropertyLiabilityRiskPage.NoClaimDiscountDropdownListId(),
                testData.TestParameters.get("Claim Discount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduDomesticAndPropertyLiabilityRiskPage.SecurityDiscountsLevel5AlarmCheckBoxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduDomesticAndPropertyLiabilityRiskPage.SecurityDiscountsSecurityDoorsCheckBoxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduDomesticAndPropertyLiabilityRiskPage.SecurityDiscountsGuardDogsCheckBoxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduDomesticAndPropertyLiabilityRiskPage.SecurityDiscountsburglabarsCheckBoxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduDomesticAndPropertyLiabilityRiskPage.SecurityDiscountsElectricFencingCheckBoxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduDomesticAndPropertyLiabilityRiskPage.SecurityDiscountsGuardedAccessCheckBoxId(), true))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Contents entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduDomesticAndPropertyLiabilityRiskPage.SurveyRateButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementbyName(
                EkunduDomesticAndPropertyLiabilityRiskPage.AddInterestedParties2ButtonID()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduDomesticAndPropertyLiabilityRiskPage.TypeofAgreement2TypeDropdownListId(),
                testData.TestParameters.get("Type of Agreement 2")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduDomesticAndPropertyLiabilityRiskPage.InstitutionName2DropdownListId(),
                testData.TestParameters.get("Institution Name 2")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduDomesticAndPropertyLiabilityRiskPage.SurveyNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduDomesticAndPropertyLiabilityRiskPage.InterestedParties2CommentsTextAreaId(),
                testData.TestParameters.get("Comments")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Interested parties details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduDomesticAndPropertyLiabilityRiskPage.SurveyNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduDomesticAndPropertyLiabilityRiskPage.SurveyNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduDomesticAndPropertyLiabilityRiskPage.SurveyNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterEndorsements()
      {
        if (!SeleniumDriverInstance.clickElementbyName(
                EkunduDomesticAndPropertyLiabilityRiskPage.AddEndorsementButtonID()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EndorsementsSelectButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.DAPLEndorsementsAddAllButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EHATDAPEndorsementsApplyButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduDomesticAndPropertyLiabilityRiskPage.SectionsDropdownListId(),
                testData.TestParameters.get("Endorsement Sections")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduDomesticAndPropertyLiabilityRiskPage.EndorsementsCommentsTextAreaId(),
                testData.TestParameters.get("Comments")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        SeleniumDriverInstance
            .clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;

      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
