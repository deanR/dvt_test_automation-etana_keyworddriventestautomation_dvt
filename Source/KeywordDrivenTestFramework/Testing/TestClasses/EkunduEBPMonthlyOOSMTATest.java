/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author ferdinandN
 */
public class EkunduEBPMonthlyOOSMTATest extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public EkunduEBPMonthlyOOSMTATest(TestEntity testData) {
        this.testData = testData;

    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToFindClientPage()) {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find client page", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to navigate to the find client page- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!findClient()) {
            SeleniumDriverInstance.takeScreenShot("Failed to  find client", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterMidTermAdjustmentData()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter mid-term adjustment data", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter mid-term adjustment data- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!editBusinessAllRisk()) {
            SeleniumDriverInstance.takeScreenShot("Failed to edit Business All Risk", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to edit Business All Risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!deleteRiskItems()) {
            SeleniumDriverInstance.takeScreenShot("Failed to delete risk items", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to delete risk items- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!makePolicyLive()) {
            SeleniumDriverInstance.takeScreenShot("Failed to make policy live", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to make policy live- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!quoteBackdatedPolicies()) {
            SeleniumDriverInstance.takeScreenShot("Failed to quote backdated MTA policies", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to quote backdated MTA policies- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!makeOOSLive()) {
            SeleniumDriverInstance.takeScreenShot("Failed to make OOS live", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to make OOS live- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Out of Synch MTA completed successfully- ", this.getTotalExecutionTime());
    }

    private boolean navigateToFindClientPage() {
        if (!SeleniumDriverInstance.navigateToFindClientPage(EkunduViewClientDetailsPage.ClientHoverTabId(), "Find Client")) {
            return false;
        }
        return true;
    }

    private boolean findClient() {
        String clientCode = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"), "Retrieved Client Code");

        if (clientCode.equals("parameter not found")) {
            clientCode = testData.TestParameters.get("Client Code");
        } else {
            testData.updateParameter("Client Code", clientCode);
        }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId())) {
            return false;
        }

        return true;
    }

    private boolean enterMidTermAdjustmentData() {
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText())) {
            return false;
        }

        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                "Retrieved Policy Number");

        if (policyNumber.equals("parameter not found")) {
            policyNumber = testData.TestParameters.get("Policy Number");
        } else {
            testData.updateParameter("Policy Number", policyNumber);
        }

//         if(!SeleniumDriverInstance.changePolicy(policyNumber, "Change"))
//        {
//            return false;
//        }
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ChangePolicyLinkText())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MTATypeofChangeDropDownListId(), testData.TestParameters.get("Type of Change"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MIAEffectiveDateTextBoxId(), testData.TestParameters.get("Effective Date"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId())) {
            return false;
        }

        SeleniumDriverInstance.acceptAlertDialog();

        return true;

    }

    private boolean editBusinessAllRisk() {
        SeleniumDriverInstance.pause(2000);
        System.out.println("Editing Business All risk");

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.BusinessAllRiskMonthlyActionDropdownListXpath(), "Edit")) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
//        SeleniumDriverInstance.Driver.navigate().refresh();
//        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementbyXpath(EkunduBusinessAllRiskPage.editOfficeMachinesLinkXpath())) {
            return false;
        }

        System.out.println("Editing Office Machines and equipment");

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduBusinessAllRiskPage.BusinessAllRiskItemSumInsuredTextboxId(), this.getData("Office and Machines New Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskRateButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.uncheckCheckBoxSelectionById(EkunduBusinessAllRiskPage.BusinessAllRiskPolicyNotesCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Cellphone risk item edited successfully", false);

        return true;

    }

    private boolean deleteRiskItems() {
        SeleniumDriverInstance.pause(3000);
//       SeleniumDriverInstance.Driver.navigate().refresh();
//        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementbyXpath(EkunduBusinessAllRiskPage.deleteRadioEquipmentLinkXpath())) {
            return false;
        }

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementbyXpath(EkunduBusinessAllRiskPage.deleteFirstCellularLinkXpath())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("risk items deleted successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        return true;

    }

    private boolean makePolicyLive() {
        SeleniumDriverInstance.pause(2000);
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(), "Edit")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.MakeLiveButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(3000);

        SeleniumDriverInstance.acceptAlertDialog();

        SeleniumDriverInstance.pause(5000);
        return true;
    }

    private boolean quoteBackdatedPolicies() {
        System.out.println("Quoting backdated MTA policies");
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.BackdatedEditPolicy1LinkId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.BusinessAllRiskMonthlyActionDropdownListXpath(), "Edit")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        System.out.println("Quoting Roadside Assist");

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(), "Edit")) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(3000);

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

      //SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Road Side Assist quoted successfully", false);

      //SeleniumDriverInstance.scrollElementIntoViewById(EkunduViewClientDetailsPage.SaveQuoteButtonId());
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SaveQuoteButtonId())) {
            return false;
        }

        System.out.println("Quoting Second backdated policy");

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.BackdatedEditPolicy2LinkId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.BusinessAllRiskMonthlyActionDropdownListXpath(), "Edit")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        System.out.println("Quoting Roadside Assist");

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(), "Edit")) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Road Side Assist quoted successfully", false);

        SeleniumDriverInstance.scrollElementIntoViewById(EkunduViewClientDetailsPage.SaveQuoteButtonId());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SaveQuoteButtonId())) {
            return false;
        }

        System.out.println("Quoting Third backdated policy");

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.BackdatedEditPolicy3LinkId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.BusinessAllRiskMonthlyActionDropdownListXpath(), "Edit")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(5000);

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        System.out.println("Quoting Roadside Assist");

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(), "Edit")) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Road Side Assist quoted successfully", false);

        SeleniumDriverInstance.scrollElementIntoViewById(EkunduViewClientDetailsPage.SaveQuoteButtonId());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SaveQuoteButtonId())) {
            return false;
        }

//        System.out.println("Quoting Fourth backdated policy");
//
//        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.BackdatedEditPolicy4LinkId())) {
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.BusinessAllRiskMonthlyActionDropdownListXpath(), "Edit")) {
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
//            return false;
//        }
//
//        SeleniumDriverInstance.pause(5000);
//
//        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
//        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
//
//        System.out.println("Quoting Roadside Assist");
//        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(), "Edit")) {
//            return false;
//        }
//
//        SeleniumDriverInstance.pause(2000);
//
//        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
//            return false;
//        }
//
//        SeleniumDriverInstance.pause(2000);
//
//        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
//
//        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId())) {
//            return false;
//        }
//
//        SeleniumDriverInstance.takeScreenShot("Road Side Assist quoted successfully", false);
//
//        SeleniumDriverInstance.scrollElementIntoViewById(EkunduViewClientDetailsPage.SaveQuoteButtonId());
//
//        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SaveQuoteButtonId())) {
//            return false;
//        }

        return true;
    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean makeOOSLive() {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OOSOKButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(3000);

        SeleniumDriverInstance.acceptAlertDialog();

        SeleniumDriverInstance.takeScreenShot("Backdate policies quoted and made live sucessfully", false);
        return true;
    }

}
