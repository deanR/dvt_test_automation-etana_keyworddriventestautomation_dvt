/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.JavascriptExecutor;

/**
 * 
 * @author deanR
 */
public class EkunduEBPMotorChangesMonthlyPolicyRenewalDeleteAddTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduEBPMotorChangesMonthlyPolicyRenewalDeleteAddTest(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToPolicyRenewalPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the Renewal Selection page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the Renewal Selection page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findPolicyAndCopyToRenewalCycle())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to copy policy to renewal cycle", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to copy policy to renewal cycle- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }
        
        if (!navigateToFindClientPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find client page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the find client page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findClient())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to find client", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterPolicyRenewalData())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter policy renewal data", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter policy renewal data- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

       if (!verifyRisksDialogisPresent())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to verify that the risk dialog is present"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the risk dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            
            
       if (!enterMotorRiskTypeDetailsBentley())
            {
               SeleniumDriverInstance.takeScreenShot(("Failed to enter risk type details"), true);
               return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter risk type details- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
       
       if (!makePolicyLive())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to make policy live", true);

            return new TestResult(testData,  Enums.ResultStatus.FAIL,
                                  "Failed to  make policy live- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS,
                              "Policy renewal for EBP monthly completed successfully "
                              + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());

      }

    private boolean navigateToPolicyRenewalPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.PolicyHoverTabId(),
                EKunduHomePage.RenewalSelectionTabXPath()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.waitForElementById(EKunduHomePage.RenewalSelectionDivId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Navigation to the Renewal Selection page was successful", false);

        return true;
      }

    private String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean findPolicyAndCopyToRenewalCycle()
      {
        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                                  "Retrieved Policy Number");

        if (policyNumber.equals("parameter not found"))
          {
            policyNumber = testData.TestParameters.get("Policy Number");
          }
        else
          {
            testData.updateParameter("Policy Number", policyNumber);
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PolicyReferenceTextBoxId(), policyNumber))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FindNowButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.RenewalSelectionSelectPolicyLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Policy Found", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RenewalSelectionSelectPolicyLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);

        return true;

      }
    
    private boolean navigateToFindClientPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),
                EKunduHomePage.FindClientTabXPath()))
          {
            return false;
          }

        return true;
      }

    private boolean findClient()
      {
        String clientCode =
            SeleniumDriverInstance.retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase1"),
                "Retrieved Client Code");

        if (clientCode.equals("parameter not found"))
          {
            clientCode = testData.TestParameters.get("Client Code");
          }
        else
          {
            testData.updateParameter("Client Code", clientCode);
          }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
          {
            return false;
          }

        return true;
      }
  
    private boolean enterPolicyRenewalData()
      {
        if (!SeleniumDriverInstance.clickElementbyLinkText("Policies"))
          {
            return false;
          }

         String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                                  "Retrieved Policy Number");

        if (policyNumber.equals("parameter not found"))
          {
            policyNumber = testData.TestParameters.get("Policy Number");
          }
        else
          {
            testData.updateParameter("Policy Number", policyNumber);
          }
         
         if(!SeleniumDriverInstance.changePolicy(policyNumber, "Details"))
        {
            return false;
        }

        return true;
      }
    
    private boolean verifyRisksDialogisPresent()
         {
            SeleniumDriverInstance.pause(2000);

            if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
              {
                SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
                selectMotorRiskType();

                return true;
              }

            else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
              {
                selectMotorRiskType();

                return true;

              }

            else
              {
                SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
                System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

                return false;

              }

          }

    private boolean selectMotorRiskType()
     {
        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Motor risk type selected successfully", false);

        return true;

      }

    private boolean enterMotorRiskTypeDetailsBentley()
     {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SearchVehicleModelIconId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);


        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleSourceDropdownListId(),
                testData.TestParameters.get("Source")))
            {
              return false;
            }

            SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MakeDropdownListId(),
                testData.TestParameters.get("Vehicles Make")))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleYearDropdownListId(),
                testData.TestParameters.get("Vehicles Year")))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.ModelDropdownListId(),
                testData.TestParameters.get("Vehicles Model")))
          {
            return false;
          }

//            SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.VehicleModelSelectedRowId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.GVMTextboxId(),testData.TestParameters.get("GVM")))
            {
              return false;
            }

         
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
        {
          return false;
        }

        System.out.println("Registration Details...");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EngineNumberTextboxId(),
                testData.TestParameters.get("Engine Number")))
            {
              return false;
            }
                        
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationNumberTextboxId(),
                generateDateTimeString()))
            {
              return false;
            }
            
            try
            {
                 JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver  ;
                 js.executeScript("VerifyEPLRegistrationNumber();");
            }
            catch(Exception e)
            {
                System.err.println("Failed to verify Registration number");
            }
          
            SeleniumDriverInstance.pause(3000);
            
            //SeleniumDriverInstance.checkPresenceofElementById("popup_ok", "popup_ok");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleRegOwnerTextBoxId(),
                testData.TestParameters.get("Registered Owner")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationDateTextboxId(),
                testData.TestParameters.get("Registration Date")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NATISCodeDropdownListId(),
                testData.TestParameters.get("NATIS Code")))
            {
              return false;
            }
            
            System.out.println("Motor Cover...");
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.DetailsMotorCoverSumInsuredTextBoxId(), this.getData("Motor Cover Sum Insured")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.DetailsMotorCoverTypeDropdownListId(), this.getData("Motor Cover Type")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.DetailsMotorCoverBasisofSettlementDropdownListId(), this.getData("Motor Basis of Settlement")))
        {
           return false;
        }


        System.out.println("Extensions...");
            
            if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsFireandExplosionRateTextboxId(), this.getData("Extensions - Fire and Explosion - Rate")))
            {
            return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsFireandExplosionPremiumTextboxId(), this.getData("Extensions - Fire and Explosion - Premium")))
            {
            return false;
            }
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.RadioTheftCheckBoxId(), true))
            {
              return false;
            }

            if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsRadioTheftTextboxId(), this.getData("Extensions - Radio theft - Rate")))
            {
            return false;
            }
           
            if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsThirdPartLiabilityRateTextBoxId(),  this.getData("Extensions - Third Party Liability Rate")))
            {
            return false;
            }
        
         if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsThirdPartLiabilityPremiumTextBoxId(),  this.getData("Extensions - Third Party Liability Premium")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
        {
           return false;
        }
         
         SeleniumDriverInstance.pause(3000);
         
         if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
           return false;
        }
         
         System.out.println("FAP......");
         
         if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.FAPBasic3rdpartyMinPercTextBoxId(), this.getData("FAP - Basic 3rd Party - Min %")))
         {
             return true;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.FAPBasicFireandExplosionMinPercTextBoxId(), this.getData("FAP - Fire and Explosion Min %")))
         {
             return true;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.FAPBasic3rdpartyMinAmountTextBoxId(), this.getData("FAP - Basic 3rd Party - Min Amount")))
         {
             return true;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.FAPBasicFireandExplosionMinAmountTextBoxId(), this.getData("FAP - Fire and Explosion")))
         {
             return true;
         }
         
            SeleniumDriverInstance.takeScreenShot("First Amount Payable details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }

        
    System.out.println("Reinsurance Details");

    if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))


    if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BandDropdownListId(),
            testData.TestParameters.get("Reinsurance Band")))
      {
        return false;
      }

    SeleniumDriverInstance.pause(2000);
    
    if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))

    if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceOkButtonId()))
      {
        return false;
      }

//        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceOkButtonId());

    SeleniumDriverInstance.takeScreenShot("Reinsurance Details entered successfully", false);
    
    System.out.println("Delete 1st Motor specified");
    
    
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.MotorSpecifiedRisk1DeleteActionDropdownListXpath(),"Delete"))
              {
                return false;
              }
          

        SeleniumDriverInstance.acceptAlertDialog();

        SeleniumDriverInstance.pause(5000);     

        SeleniumDriverInstance.takeScreenShot("Motor specified risk 1 deleted succesfully", false);

       return true;

     }
    
    private boolean makePolicyLive()
      {
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(),"Edit"))
              {
                return false;
              }
          

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());


        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.MakeLiveButtonId()))
          {
            return false;
          }
        
        SeleniumDriverInstance.acceptAlertDialog();

        SeleniumDriverInstance.takeScreenShot("Policy made live", false);

        SeleniumDriverInstance.pause(3000);

        return true;
      }

  
  }
