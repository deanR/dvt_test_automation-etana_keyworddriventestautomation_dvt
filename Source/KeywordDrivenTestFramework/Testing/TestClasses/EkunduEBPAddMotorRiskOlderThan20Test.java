
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorFleetRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.JavascriptExecutor;


/**
 *
 * @author ferdinandN
 */
public class EkunduEBPAddMotorRiskOlderThan20Test extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduEBPAddMotorRiskOlderThan20Test(TestEntity testData)
      {
        this.testData = testData;
      }
    
    

    public TestResult executeTest()
      {
        this.setStartTime();
        
        
           if (!verifyRisksDialogisPresent())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to verify that the riss dialog is present"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            
            
            if (!addMotorVehiclesOlderThan20Years())
              {
                SeleniumDriverInstance.takeScreenShot(("Failed to enter risk type details"), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter risk type details- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
              }
            
            return new TestResult(testData, Enums.ResultStatus.PASS, "Motor Risk (Vehicles Older Than 20 Years) added successfully", this.getTotalExecutionTime());
       
      }

    private boolean verifyRisksDialogisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            selectMotorRiskType();

            return true;
          }

        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectMotorRiskType();

            return true;

          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

      }

    private boolean selectMotorRiskType()
      {
        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Motor risk type selected successfully", false);

        return true;

      }
    
     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

     private boolean enterEndorsements()
    {
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsSelectButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsAddAllButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsApplyButtonId()))
         {
             return false;
         }
         
         
         SeleniumDriverInstance.takeScreenShot("Endorsement details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        
            return true;
         
         
     }
     
      private boolean enterInterestedParties()
      {
          //SeleniumDriverInstance.Driver.navigate().refresh();
          SeleniumDriverInstance.pause(3000);
          
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.MSAddInterestedPartiesButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MSTypeofAgreementTypeDropdownListId(),
                testData.TestParameters.get("Type of Agreement")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.InterestedPartyInstitutionNameDropdownListId(),
                testData.TestParameters.get("Institution Name")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }

//        if (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedNotesTextBoxId(),
//                testData.TestParameters.get("Interested Party Comments")))
//          {
//            return false;
//          }

        SeleniumDriverInstance.takeScreenShot("Interested parties details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
        return true;
        
      }
      
      public String generateDateTimeString()
    {
      Date dateNow = new Date( );
      SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd_hh-mm-ss");
      
      return dateFormat.format(dateNow).toString();
    }
       
       private boolean addMotorVehiclesOlderThan20Years()
      {
         if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SearchVehicleModelIconId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);
        
        
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehiclesSourceDropdownListId(),
                testData.TestParameters.get("Vehicles Source")))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleMakeOlderVehiclesDropdownListId(),
                testData.TestParameters.get("Vehicles Make")))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleYearOlderVehiclesTextbox(),
                testData.TestParameters.get("Vehicles Year")))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleModelOlderVehiclesTextbox(),
                testData.TestParameters.get("Vehicles Model")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleModelOlderVehiclesCCTextbox(),
                testData.TestParameters.get("Vehicles CC")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleModelOlderVehiclesGVMTextbox(),
                testData.TestParameters.get("Vehicles GVM")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleBodyTypeOlderVehiclesDropdownListId(),
                testData.TestParameters.get("Vehicles Body Type")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.VehiclesCaptureCriteriaButtonId()))
            {
              return false;
            }
        
        

        SeleniumDriverInstance.pause(2000);
       
            System.out.println("Options...");
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.EndorsementsCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriversCheckBoxId(), true))
            {
              return false;
            }
            
//            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MainDriverDropdownListId(),
//                testData.TestParameters.get("Main Driver")))
//            {
//                
//              return false;
//              
//            }
//            
//            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleOvernightParkingDropdownListId(),
//                testData.TestParameters.get("Vehicle parking overnight")))
//            {
//              return false;
//            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
            {
              return false;
            }
            
            System.out.println("Vehicle Cover Details...");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleCoverDetailsAreaCodeTextboxId(),
                testData.TestParameters.get("Vehicle Cover Details - Area Code")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorTrackingDeviceDropdownListId(),
                testData.TestParameters.get("Tracking Device")))
            {
              return false;
            }
           
            System.out.println("Registration Details...");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EngineNumberTextboxId(),
                testData.TestParameters.get("Engine Number")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ChassisNumberTextboxId(),
                testData.TestParameters.get("Chassis Number")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationNumberTextboxId(),
                generateDateTimeString()))
            {
              return false;
            }
            
            try
            {
                 JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver  ;
                 js.executeScript("VerifyEPLRegistrationNumber();");
            }
            catch(Exception e)
            {
                System.err.println("Failed to verify Registration number");
            }
          
            SeleniumDriverInstance.pause(3000);
            
           
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleRegOwnerTextBoxId(),
                testData.TestParameters.get("Registered Owner")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationDateTextboxId(),
                testData.TestParameters.get("Registration Date")))
            {
              return false;
            }
                    
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NATISCodeDropdownListId(),
                testData.TestParameters.get("NATIS Code")))
            {
              return false;
            }

            
            System.out.println("Motor Cover");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleSumInsuredTextBoxId(),
                testData.TestParameters.get("Vehicle Sum Insured")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BasisofSettlementDropdownListId(),
                testData.TestParameters.get("Basis of Settlement")))
            {
              return false;
            }
            
            
            
            System.out.println("Motor Accessories");
            
            if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddAccessoriesButtonName()))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessoryDescriptionTextBoxId(),
            testData.TestParameters.get("Accessory Description")))
            {
              return false;
            }
       
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessorySumInsuredTextBoxId(),
            testData.TestParameters.get("Accessory Sum Insured")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
            {
              return false;
            }
       
           
            
            System.out.println("Voluntary Excess");
           
             
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryExcessPercentageTextBoxId(),testData.TestParameters.get("Voluntary Excess Percentage")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryMinimumAmountTextBoxId(),testData.TestParameters.get("Voluntary Minimum Amount")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryExcessDiscountTextBoxId(),testData.TestParameters.get("Voluntary Excess Discount")))
            {
              return false;
            }
            
             System.out.println("Additional Compulsory Excess");
            
             if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryExcessPercentageTextBoxId(),testData.TestParameters.get("Compulsory Excess Percentage")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryMinimumAmountTextBoxId(),testData.TestParameters.get("Compulsory Minimum Amount")))
            {
              return false;
            }
            
            SeleniumDriverInstance.takeScreenShot("Motor Details entered successfully", false);
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
            System.out.println("FAP...");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPThirdPardyMinimumAmountTextBoxId(),
                testData.TestParameters.get("FAP - Third Party -  Minimum Amount")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPFireAndExplosionMinimumAmountMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Fire and Explosion -  Minimum Amount")))
          {
            return false;
          }
            
            System.out.println("Endorsements...");
            
            enterEndorsements();
            
            System.out.println("Driver1...");
            //<editor-fold defaultstate="collapsed" desc="driver">
             //click add driver button
            if  (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddSpecifiedDriverButtonName()))
              {
                  return false;
              }
            //enter driver name
            if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverNameTextBoxId(),
                    testData.TestParameters.get("Driver")))
              {
                  return false;
              }
            //select id type
            if  (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverIDTypeDropdownId(),
                    testData.TestParameters.get("ID Type")))
              {
                  return false;
              }
            //enter id number
            if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverIDNumberTextBoxId(), 
                    testData.TestParameters.get("ID Number")))
              {
                  return false;
              }
            //click next driver button
            if  (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BasicDetailsNextButtonName()))
              {
                  return false;
              }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }

            
            System.out.println("Interested Parties...");
            
            this.enterInterestedParties();
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
             System.out.println("Financial Overview tab...");
             
             if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.AdjustmentBasisDropdownListId(), testData.TestParameters.get("Adjustment Basis")))
            {
                return false;
            }
                    
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AdjustmentPercentageTextboxId(), testData.TestParameters.get("Adjustment Percentage")))
            {
                return false;
            }
            
             if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.AdjustmentReasonDropdownListId(), testData.TestParameters.get("Adjustment Reason")))
            {
                return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
                return false;
            }
            
            SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_ReInsurance2007Cntrl_ddlReinsurance",
                "MS Own Damage (Section A)"))
          {
            return false;
          }
            
            SeleniumDriverInstance.pause(2000);
            
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText("Reinsurances"))
          {
            return false;
          }
        
        
            
            this.enterFACPropPlacementData();
            
           SeleniumDriverInstance.pause(3000);
            
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText("Reinsurances"))
          {
            return false;
          }
        
        SeleniumDriverInstance.pause(3000);

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        
        return true;
        
  }
       
       private boolean enterFACPropPlacementData()
      {
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(),
                this.getData("FAC Prop - Hannoverre")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);

//        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId());
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
          {
            return false;
          }
        
        
        SeleniumDriverInstance.pause(3000);
        
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText("Reinsurances"))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceSumInsuredTextBoxId(),
                this.getData("Reinsurance Amount")))
          {
            return false;
          }
        
         SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);

        System.out.println("Company 1 added successfully");
        return true;
      }
           
}


//~ Formatted by Jindent --- http://www.jindent.com
