
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduDomesticAndPropertyRiskTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduDomesticAndPropertyRiskTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!verifyRisksDialogisPresent())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the risks dialod is presenet"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to select risk type - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!addSurveyType())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter survey type"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter survey type - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterRiskCoverage())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk coverage"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk coverage - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterBuildingsDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter building details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter building details- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

//        if (!enterInterestedParties())
//          {
//            SeleniumDriverInstance.takeScreenShot(("Failed to enter interested parties"), true);
//
//            return new TestResult(testData, Enums.ResultStatus.FAIL,
//                                  "Failed to enter interested parties- "
//                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//          }

        if (!enterContents())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter contents"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter contents- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }
        
        if (!allRisk())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter all risk item details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter all risk item details- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterEndorsements())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter endorsements"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter endorsements- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Buildings and Contents risk added successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Buildings and Contents risk added successfully",
                              this.getTotalExecutionTime());

      }

    private boolean verifyRisksDialogisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());

            selectDomesticandPropertyRiskType();

            return true;
          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

        
      }
    
    

    private boolean selectDomesticandPropertyRiskType()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(testData.TestParameters.get("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Domestic and Property Risk type selected successfully", false);
        
        return true;
      }

    private boolean addSurveyType()
      {
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddSurveyButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.SelectSurveyTypeDropdownListId(),
                testData.TestParameters.get("Survey Type")))
          {
            return false;
          }

        SeleniumDriverInstance.clearTextById(EkunduCreateNewPolicyForNewClientPage.SurveyDateTextBoxId());

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SurveyDateTextBoxId(),
                testData.TestParameters.get("Survey Date")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SurveyReferenceTextBoxId(),
                testData.TestParameters.get("Survey Reference")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SurveyReportCommentsTextAreaId(),
                testData.TestParameters.get("Comments")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Survey type entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterRiskCoverage()
      {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.CheckBuildingCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.CheckContentsCheckBoxId(),
                true))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.CheckAllRiskCheckBoxId(),
                true))
          {
            return false;
          }
        
//        if (!SeleniumDriverInstance.uncheckCheckBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.BuildingsandContentsLegalCheckBoxId(),
//                true))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.uncheckCheckBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.BuildingsandContentsliabilityCheckBoxId(),
//                true))
//          {
//            return false;
//          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TypeofResidenceDropdownListId(),
                testData.TestParameters.get("Type of Residence")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TypeofConstructionDropdownListId(),
                testData.TestParameters.get("Type of Construction")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.OccupancyDropdownListId(),
                testData.TestParameters.get("Occupancy")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.LocalityDropdownListId(),
                testData.TestParameters.get("Locality")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk coverage entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.RiskCoverageNextButtonName()))
          {
            return false;
          }

        return true;
      }

    private boolean enterBuildingsDetails()
      {
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BuildingSumInsuredTextBoxId(),
                testData.TestParameters.get("Building Sum Insured")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsRateButtonId()))
          {
            return false;
          }

//        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AdjustmentPercentageTextBoxId(),
//                testData.TestParameters.get("Adjustment Percentage")))
//          {
//            return false;
//          }
//        
//        SeleniumDriverInstance.pause(2000);
//        
//        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReasonDropdownListId()))
//          {
//            return false;
//          }
//        
//        SeleniumDriverInstance.pause(2000);
//        
//        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.ReasonDropdownListId(),
//                testData.TestParameters.get("Reason")))
//          {
//            return false;
//          }
//        
//
//        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SubsidenceSumInsuredTextBoxId(),
//                testData.TestParameters.get("SubsidenceSumInsured")))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SubsidenceandLandslipPremiumTextBoxId(),
//                testData.TestParameters.get("Subsidence and Landslip Premium")))
//          {
//            return false;
//          }

        SeleniumDriverInstance.takeScreenShot("Building details entered successfully", false);
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterInterestedParties()
      {
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddInterestedPartiesButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TypeofAgreementTypeDropdownListId(),
                testData.TestParameters.get("Type of Agreement")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.InstitutionNameDropdownListId(),
                testData.TestParameters.get("Institution Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.InterestedParties2CommentsTextAreaId(),
                testData.TestParameters.get("Interested Party Comments")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Interested parties details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterContents()
      {
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ContentsSumInsuredTextBoxId(),
                testData.TestParameters.get("Contents Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NoClaimDiscountDropdownListId(),
                testData.TestParameters.get("Claim Discount")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.SecurityDiscountsLevel5AlarmCheckBoxId(),
                true))
          {
            return false;
          }
               
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsRateButtonId()))
          {
            return false;
          }

//        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.SecurityDiscountsSecurityDoorsCheckBoxId(),
//                true))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ContentsAdjustmentPercentageTextBoxId(),
//                testData.TestParameters.get("Adjustment Percentage")))
//          {
//            return false;
//          }
//        
//        SeleniumDriverInstance.pause(2000);
//        
//        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ContentsAdjustmentReasonDropdownListId()))
//          {
//            return false;
//          }
//        
//        SeleniumDriverInstance.pause(2000);
//
//        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.ContentsAdjustmentReasonDropdownListId(),
//                testData.TestParameters.get("Reason")))
//          {
//            return false;
//          }

        SeleniumDriverInstance.takeScreenShot("Contents entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        return true;

      }
    
    private boolean allRisk()
    { 
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddAllRiskItemsButtonName()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.AllRiskCategoryDropdownListId(),
                testData.TestParameters.get("Category")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.allRiskDescriptionTextBoxId(),
                testData.TestParameters.get("Description")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.allRiskSerialTextBoxId(),
                testData.TestParameters.get("Serial/IMEI")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.allRiskSumInsuredTextBoxId(),
                testData.TestParameters.get("All Risk - Sum Insured")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsRateButtonId()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }
        
        return true;
    }

    private boolean enterEndorsements()
      {
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddEndorsementButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.EndorsementSectionsDropdownListId(),
                testData.TestParameters.get("Endorsement Sections")))
          {
            return false;
          }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EndorsementsSelectButtonId()))
        {
            return false;
        }
         
         if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EndorsementsBuildingsAddAllButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BuildingsAndContentsEndorsementsApplyButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementNextButtonId()))
          {
            return false;
          }


        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementNextButtonId());
         

        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementNextButtonId());

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;

      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
