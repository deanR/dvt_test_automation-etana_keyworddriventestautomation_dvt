
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EkunduEhatElectronicEquipmentPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class EkunduFarePayingPassengerLiabilityTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduFarePayingPassengerLiabilityTest(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent())
          {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the Add Risk button is present - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterRiskDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk details - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterEndorsements())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter endorsements", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter endorsements - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterNotes())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk notes", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk notes - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Fare paying passenger liability Added successfully ",
                              this.getTotalExecutionTime());
      }

    // Custom method to shorten data retrieval
     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
   
    private boolean verifyAddRiskButtonisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectFarePayingRiskType();

            return true;
          }

        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectFarePayingRiskType();

            return true;
          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");

            return false;
          }
      }

    private boolean selectFarePayingRiskType()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(10000);

        SeleniumDriverInstance.takeScreenShot("EHAT - Fare Paying Passenger Liability Risk type selected successfully",
                false);

        return true;
      }

    private boolean enterRiskDetails()
      {
        if (!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.LimitOfIndemnityTextBoxID(),
                this.getData("Limit of Indemnity")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.NoOfSeatsTextBoxID(),
                this.getData("No Of Seats")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.PremiumPerSeatTextBoxID(),
                this.getData("Premium per Seat")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.VehicleDescriptionTextBoxID(),
                this.getData("Vehicle Description")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.RegNoTextBoxID(),
                this.getData("Reg No")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.FarePayingMinPercTextBoxID(),
                this.getData("Min Perc")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.FarePayingMaxAMTextBoxID(),
                this.getData("Max Amt")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.NextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterEndorsements()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.EndorsementsSelectButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.FarePayingEndorsementsAddAllButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.FarePayingEndorsementsApplyButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.NextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterNotes()
      {
        if (!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.FarePayingRiskItemCommentsTextboxId(),
                this.getData("Fare Paying Notes")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduEhatElectronicEquipmentPage.FarePayingNotesCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.FarePayingRiskItemNotesTextboxId(),
                this.getData("Fare Paying Notes")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.NextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;

      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
