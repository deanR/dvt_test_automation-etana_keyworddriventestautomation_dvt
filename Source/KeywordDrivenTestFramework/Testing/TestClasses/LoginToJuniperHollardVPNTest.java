
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.*;

import KeywordDrivenTestFramework.Testing.PageObjects.JuniperHollardVPNLoginPage;
import KeywordDrivenTestFramework.Testing.PageObjects.JuniperMainPage;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;

/**
 *
 * @author fnell
 */
public class LoginToJuniperHollardVPNTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;
    String VerifyTestDataResult, UserName, Password;

    public LoginToJuniperHollardVPNTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        verifyTestData();

        if (!VerifyTestDataResult.equals(""))
          {
            return new TestResult(testData, Enums.ResultStatus.FAIL, VerifyTestDataResult, this.getTotalExecutionTime());
          }

        this.setStartTime();

        if (!navigateToLoginPage())
          {
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to navigate to login page", this.getTotalExecutionTime());
          }

        /*
         * if(!verifyPageHasLoaded())
         * return new TestResult(testData,false, "Failed to verify that login page has loaded", this.getTotalExecutionTime());
         * if(!enterUsername())
         * return new TestResult(testData,false, "Failed to enter user name", this.getTotalExecutionTime());
         * if(!enterPassword())
         * return new TestResult(testData,false, "Failed to enter password", this.getTotalExecutionTime());
         * if(!selectRealm())
         * return new TestResult(testData,false, "Failed to select the Active Directory Realm", this.getTotalExecutionTime());
         * if(!clickSignInButton())
         * return new TestResult(testData,false, "Failed to click sign in button", this.getTotalExecutionTime());
         */
        if (!verifyLoginSuccessfull())
          {
            return new TestResult(testData, Enums.ResultStatus.PASS, "Login was not successful", this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Login completed successfully", this.getTotalExecutionTime());
      }

    private void verifyTestData()
      {
        VerifyTestDataResult = "";

        if (!testData.TestParameters.containsKey("User Name"))
          {
            VerifyTestDataResult = "User Name not specified";
          }
        else
          {
            UserName = testData.TestParameters.get("User Name");
          }

        if (!testData.TestParameters.containsKey("Password"))
          {
            VerifyTestDataResult = "Password not specified";
          }
        else
          {
            Password = testData.TestParameters.get("Password");
          }
      }

    private boolean navigateToLoginPage()
      {
        if (!SeleniumDriverInstance.navigateTo(JuniperHollardVPNLoginPage.PageUrl()))
          {
            return false;
          }
        else
          {
            return true;
          }
      }

    private boolean verifyPageHasLoaded()
      {
        try
          {
            if (!SeleniumDriverInstance.waitForElementByName(JuniperHollardVPNLoginPage.LoginFormName()))
              {
                return false;
              }
            else
              {
                return true;
              }
          }
        catch (Exception e)
          {
            return false;
          }
      }

    private boolean verifyLoginSuccessfull()
      {
        try
          {
            if (SeleniumDriverInstance.waitForElementByName("btnContinue"))
              {
                SeleniumDriverInstance.clickElementbyName("btnContinue");
              }

            if (!SeleniumDriverInstance.waitForElementById(JuniperMainPage.userMenuDashboardTab()))
              {
                return false;
              }
            else
              {
                return true;
              }
          }
        catch (Exception e)
          {
            return false;
          }
      }

    private boolean enterUsername()
      {
        if (!SeleniumDriverInstance.enterTextByName(JuniperHollardVPNLoginPage.UserNameTextBoxName(), UserName))
          {
            return false;
          }
        else
          {
            return true;
          }
      }

    private boolean enterPassword()
      {
        if (!SeleniumDriverInstance.enterTextByName(JuniperHollardVPNLoginPage.PasswordTextBoxName(), Password))
          {
            return false;
          }
        else
          {
            return true;
          }
      }

    private boolean selectRealm()
      {
        if (!SeleniumDriverInstance.selectByValueFromDropDownListUsingName(JuniperHollardVPNLoginPage.RealmSelectionDropDownListName(),
                "Active Directory Realm"))
          {
            return false;
          }
        else
          {
            return true;
          }
      }

    private boolean clickSignInButton()
      {
        if (!SeleniumDriverInstance.clickElementbyName(JuniperHollardVPNLoginPage.SignInSubmitButtonName()))
          {
            return false;
          }
        else
          {
            return true;
          }
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
