
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.CreateNewClaimPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMaintainCorporateClientClaim1Page;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author deanR
 */
public class EkunduMaintainClaimSalvageRecoveryTest_Old extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduMaintainClaimSalvageRecoveryTest_Old(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!navigateToFindClaimPage())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to navigate to the find claim page"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the find claim page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findClaim())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to find claim"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find claim- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!startClaimMaintenance())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to initiate claim maintenance"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to initiate claim maintenance- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterIncreasedBuildingsReservesAmounts())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter Increased Reserve Amounts"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Increased Reserve Amounts- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Maitain Claim for Salvage Recovery completed successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Maitain Claim for Salvage Recovery completed successfully",
                              this.getTotalExecutionTime());

      }

     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean navigateToFindClaimPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EkunduMaintainCorporateClientClaim1Page.ClaimHoverTabId(),
                EkunduMaintainCorporateClientClaim1Page.ClaimSearchLinkXpath()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.waitForElementById(EkunduMaintainCorporateClientClaim1Page.ClaimReferenceTextBoxId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Navigation to the Find Claim page was successful", false);

        return true;
      }

    private boolean findClaim()
      {
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.OkButtonId()))
          {
            return false;
          }

        String claimref = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                              "Retrieved Corporate Claim Number");

        if (claimref.equals("parameter not found"))
          {
            claimref = testData.TestParameters.get("Claim Reference Number");
          }
        else
          {
            testData.updateParameter("Claim Reference Number", claimref);
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.ClaimReferenceTextBoxId(),
                claimref))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimFindNowButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.waitForElementById(EkunduMaintainCorporateClientClaim1Page.EditClaimLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Claim found", false);

        return true;

      }

    private boolean startClaimMaintenance()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.EditClaimLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMaintainCorporateClientClaim1Page.ClaimReasonforChangeDropdowlistId(),
                this.getData("Reason for Change")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimOkButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.scrollDownByOnePage("Div6");

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }
        
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId());

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId());

        return true;
      }

    private boolean enterIncreasedBuildingsReservesAmounts()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.PerilsBuildingsRecoveriesLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnAdd"))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl01_cntMainBody_ddlRecoveryType",
                this.getData("Recovery Type")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById("ctl01_cntMainBody_txtInitialReserve",
                this.getData("Initial Reserve")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById("ctl01_cntMainBody_btnOk"))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Increased reserves amounts entered successfully", false);

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ThirdPartyTabPartialLinkText());

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnAddForTP"))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToLastDuplicateFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl01_cntMainBody_ddlRecoveryType",
                this.getData("Recovery Type TP")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById("ctl01_cntMainBody_txtInitialReserve",
                this.getData("Initial Reserve")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById("ctl01_cntMainBody_btnOk"))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Third Party reserves amounts entered successfully", false);

        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ReservesSubmitButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.CopyRiskSubmitButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ReinsuranceOkButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyXpath(CreateNewClaimPage.ClaimConfirmationNoButtonXpath());

        SeleniumDriverInstance.clickElementbyXpath(CreateNewClaimPage.ClaimConfirmationNoButtonXpath());

        SeleniumDriverInstance.takeScreenShot("Claim perils entered successfully", false);

        return true;

      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
