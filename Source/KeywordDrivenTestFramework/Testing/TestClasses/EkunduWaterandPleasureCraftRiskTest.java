
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import static KeywordDrivenTestFramework.Entities.Enums.TestEnvironments.NamQA;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduDomesticAndPropertyLiabilityRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author FerdinandN
 */
public class EkunduWaterandPleasureCraftRiskTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduWaterandPleasureCraftRiskTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!verifyRisksDialogisPresent())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the risks dialog is present"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the risks dialog is present- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterVesselandHullDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter Vessel and Hull details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Vessel and Hull details- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterNotes())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk notes"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk notes- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Water and Pleasure Craft risk added successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Pleasure Craft risk added successfully",
                              this.getTotalExecutionTime());

      }

    private boolean verifyRisksDialogisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            selectWaterandPleasureCraftRiskType();

            return true;
          }

        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectWaterandPleasureCraftRiskType();

            return true;

          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

      }

    private boolean selectWaterandPleasureCraftRiskType()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(testData.TestParameters.get("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Water and Pleasure craft risk type selected successfully", false);

        return true;

      }

    private boolean enterVesselandHullDetails()
      {
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.NameofVesselTextBoxId(),
                testData.TestParameters.get("Name of Vessel")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MakeandModelTextBoxId(),
                testData.TestParameters.get("Make and Model")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TypeofVesselDropdownListId(),
                testData.TestParameters.get("Type of Vessel")))
          {
            return false;
          }
        SeleniumDriverInstance.pause(3000);
        
        if(CurrentEnvironment == NamQA)
        {
            try 
            {
                JavascriptExecutor js = (JavascriptExecutor)SeleniumDriverInstance.Driver;
                js.executeScript("pc.toggleOnVesselType(); pc.clearAllControls('NAM');");
            }
            catch(Exception e)
            {
                System.err.println("Failed to execute javascript");
            }
            
        }
        
        

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorBoatSpeedDropdownListId(),
                testData.TestParameters.get("Motor Boat Max Speed")))
          {
            return false;
          }
        
        if(CurrentEnvironment == NamQA)
        {
           if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VesselHorsePowerTextboxId(),
                testData.TestParameters.get("Horse Power")))
            {
              return false;
            } 
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NumberofEnginesDropdownListId(),
                testData.TestParameters.get("No of Engines")))
          {
            return false;
          }
        
        

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.EngineTypeDropdownListId(),
                testData.TestParameters.get("Engine Type")))
          {
            return false;
          }
        
        SeleniumDriverInstance.pause(3000);
        
        if(CurrentEnvironment == NamQA)
        {
           try 
           {
                JavascriptExecutor js = (JavascriptExecutor)SeleniumDriverInstance.Driver;
                js.executeScript("pc.toggleEngine()");
           }
           catch(Exception e)
           {
               System.err.println("Failed to execute javascript");
           }
           
                if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine1YearTextBoxId(),
                    testData.TestParameters.get("Engine 1 Year")))
              {
                return false;
              }

            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine1MakeTextBoxId(),
                    testData.TestParameters.get("Engine 1 Make")))
              {
                return false;
              }

            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine1SerialNumberTextBoxId(),
                    testData.TestParameters.get("Engine 1 Serial Number")))
              {
                return false;
              }

            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine1SumInsuredSADCTextBoxId(),
                    testData.TestParameters.get("Engine 1 Sum Insured")))
              {
                return false;
              }

             if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine2YearTextBoxId(),
                    testData.TestParameters.get("Engine 2 Year")))
              {
                return false;
              }

            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine2MakeTextBoxId(),
                    testData.TestParameters.get("Engine 2 Make")))
              {
                return false;
              }

            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine2SerialNumberTextBoxId(),
                    testData.TestParameters.get("Engine 2 Serial Number")))
              {
                return false;
              }

            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine2SumInsuredSADCTextBoxId(),
                    testData.TestParameters.get("Engine 2 Sum Insured")))
              {
                return false;
              }
        }

        
        else
        {

            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine1SumInsuredTextBoxId(),
                    testData.TestParameters.get("Engine 1 Sum Insured")))
              {
                return false;
              }

            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine1RateTextBoxId(),
                    testData.TestParameters.get("Engine 1 Rate")))
              {
                return false;
              }

            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.HullRateRequiredTextBoxId(),
                    testData.TestParameters.get("Hull Rate")))
              {
                return false;
              }
        }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MaterialofHullTextBoxId(),
                testData.TestParameters.get("Material of Hull")))
          {
            return false;
          }
        
        if(CurrentEnvironment == NamQA)
        {
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.LengthofHullSADCTextBoxId(), testData.TestParameters.get("Length of Hull")))
              {
                return false;
              }

            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.HullYearofManufactureSADCTextBoxId(), testData.TestParameters.get("Hull Year of Manufacture")))
              {
                return false;
              }

            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SerialNumberTextBoxId(), testData.TestParameters.get("Serial Number")))
              {
                return false;
              }

            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.HullSumInsuredSADCTextBoxId(), testData.TestParameters.get("Hull Sum Insured")))
              {
                return false;
              }
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.HullSumInsuredTextBoxId(),
                testData.TestParameters.get("Hull Sum Insured")))
          {
            return false;
          }
        
        if(CurrentEnvironment != NamQA)
        {
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.HullUseDropdownListId(),
                    testData.TestParameters.get("Hull Use")))
              {
                return false;
              }
        }

        
        if(CurrentEnvironment == NamQA)
        {
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
              {
                return false;
              }

            this.enterInterestedPartyDetails();
            this.enterEndorsements();
        
        }
        

        SeleniumDriverInstance.takeScreenShot("Vessel/Hull details entered successfully", false);

        if(CurrentEnvironment != NamQA)
        {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureNextButtonId()))
          {
            return false;
          }
        
        }

        return true;

      }
    
    private boolean enterEndorsements()
    {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureNextButtonId()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EndorsementsSelectButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EndorsementsAddAllButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EndorsementsApplyButtonId()))
          {
            return false;
          }
        
//        this.editEndorsementDocument();
        
        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);
        
        return true;
       
    }
    
    private boolean editEndorsementDocument()
    {
        if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EditEndorsementsLinkId()))
         {
             return false;
         }
        
        SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId());
        
        SeleniumDriverInstance.pause(3000);
        
        SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.DocumentEditorFrameId());
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EndorsementsBodyId(),
                ("Personal Lines Pleasure Craft\n\n")))
          {
            return false;
          }
        
       
        if (!SeleniumDriverInstance.enterTextByXpath(EkunduCreateNewPolicyForNewClientPage.EndorsementsLine2ParagraphXpath(),
                ("Test 1\n")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextByXpath(EkunduCreateNewPolicyForNewClientPage.EndorsementsLine3ParagraphXpath(),
                ("Test 2")))
          {
            return false;
          }
        
       
        
       //*[@id="tinymce"]/div/p[3]
       
        
        SeleniumDriverInstance.switchToDefaultContent();
        
        SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId());
        
//         if(!SeleniumDriverInstance.clickElementbyXpath(EkunduCreateNewPolicyForNewClientPage.BulletListLinkXpath()))
//         {
//             return false;
//         }
         
         SeleniumDriverInstance.pause(10000);
      
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementsSaveButtonId()))
         {
             return false;
         }
        
        SeleniumDriverInstance.switchToDefaultContent();
        
        return true;
        
        
    }
  

    private boolean enterNotes()
      {
        SeleniumDriverInstance.pause(2000);
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureNextButtonId()))
        {
            return true;
        }

        SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureCraftCommentsTextAreaId(),
                testData.TestParameters.get("Comments"));

        SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);

        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureNextButtonId()))
        {
            return true;
        }
        
        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureNextButtonId());
        
        //SeleniumDriverInstance.checkPresenceofElementById("ctl00_cntMainBody_RatingDetails1_lblRatingDetails", EkunduCreateNewPolicyForNewClientPage.WaterandPleasureNextButtonId());


        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        
//        SeleniumDriverInstance.pause(3000);
//        
//        SeleniumDriverInstance.clickElementbyLinkText("Documents");
//        
//         SeleniumDriverInstance.clickElementById("btnGeneratePdf");
//         
//         SeleniumDriverInstance.clickElementById("popup_ok");
//         
//         SeleniumDriverInstance.pause(3000);
//         
//         SeleniumDriverInstance.clickElementbyLinkText("Risks");

        return true;
      }
    
    private boolean enterInterestedPartyDetails()
    {
        if (!SeleniumDriverInstance.clickElementbyName(
                EkunduDomesticAndPropertyLiabilityRiskPage.AddInterestedPartiesButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduDomesticAndPropertyLiabilityRiskPage.TypeofAgreementTypeDropdownListId(),
                testData.TestParameters.get("Type of Agreement")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduDomesticAndPropertyLiabilityRiskPage.InstitutionNameDropdownListId(),
                testData.TestParameters.get("Institution Name")))
          {
            return false;
          }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.InterestedPartyDescriptionSADCTextboxId(),
                testData.TestParameters.get("Interested Party Description")))
        {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduDomesticAndPropertyLiabilityRiskPage.SurveyNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduDomesticAndPropertyLiabilityRiskPage.InterestedPartiesCommentsTextAreaId(),
                testData.TestParameters.get("Comments")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduDomesticAndPropertyLiabilityRiskPage.SurveyNextButtonId()))
          {
            return false;
          }
        
        SeleniumDriverInstance.takeScreenShot("Interested party details entered successfully", false);
        
        return true;
    
    }
  }

    


//~ Formatted by Jindent --- http://www.jindent.com
