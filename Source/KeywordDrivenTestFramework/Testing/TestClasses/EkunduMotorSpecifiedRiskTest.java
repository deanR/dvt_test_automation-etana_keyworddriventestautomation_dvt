/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author FerdinandN
 */
public class EkunduMotorSpecifiedRiskTest extends BaseClass
{
    TestEntity testData;
    TestResult testResultt;
   
    
    public EkunduMotorSpecifiedRiskTest(TestEntity testData)
    {
        this.testData = testData;
    }    
    
    public TestResult executeTest()
    {  
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();
        if(!verifyRisksDialogisPresent())
        {
           SeleniumDriverInstance.takeScreenShot("Failed to verify that the risks dialog is present", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to verify that the risks dialog is present - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

    
        if(!enterMotorDetails())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Motor details", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Motor details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterFAPDetails())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter First Amount Payable details", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter First Amount Payable details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterEndorsements())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Endorsements", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Endorsements - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterNotes())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Notes", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Notes - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        return new TestResult(testData, Enums.ResultStatus.PASS, "Motor Specified risk details entered successfully", this.getTotalExecutionTime());
    }
    
     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
     
     private boolean verifyRisksDialogisPresent()
    {  
            SeleniumDriverInstance.pause(2000);
       
            if(SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
            {
                    selectMotorSpecifiedRiskType();
                    return true;

            }
            else if(SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
                {
                    SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
                    selectMotorSpecifiedRiskType();
                    return true;
                }
            
            else
                {
                    SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
                    System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");
                    return false;

                }

    }
     
    private boolean selectMotorSpecifiedRiskType() 
    {
        if(!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            return false;
        }
        
        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return false;
        }
            SeleniumDriverInstance.pause(2000);
            SeleniumDriverInstance.takeScreenShot( "Motor Specified Risk type selected successfully", false);
            return true;
    }

    private boolean enterMotorDetails()
    {
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.DetailsVehicleSearchIconId()))
        {
           return false;
        }
            SeleniumDriverInstance.WaitUntilDropDownListPopulatedById(EkunduMotorSpecifiedRiskPage.DetailsVehicleMakeDropdownListId());
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.DetailsVehicleMakeDropdownListId(), this.getData("Vehicle Make")))
        {
           return false;
        }
            SeleniumDriverInstance.pause(2000);
        
        
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.DetailsVehicleYearDropdownListId(), this.getData("Vehicle Year")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.DetailsVehicleModelDropdownListId(), this.getData("Vehicle Model")))
        {
           return false;
        }
        
            SeleniumDriverInstance.pause(3000);
        
            SelectVehicle();
            
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.VehicleTypeOptionsEndorsementsCheckboxId(), true))
       {
           return false;
       }
            
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.VehicleTypeClaimsAccidentDropdownListId(), this.getData("Accidents")))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.VehicleTypeClaimsOtherDropdownListId(), this.getData("Other Claims")))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
           return false;
        }
            
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.DetailsRegistrationDetailsRegNumberTextBoxId(), generateDateTimeString()))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.DetailsRegistrationDetailsRegOwnerTextBoxId(), this.getData("Registered Owner")))
        {
           return false;
        }
           
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.DetailsRegistrationDetailsNATISCodeDropdownListId(), this.getData("NATIS Code")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.DetailsMotorCoverSumInsuredTextBoxId(), this.getData("Motor Cover Sum Insured")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.DetailsMotorCoverTypeDropdownListId(), this.getData("Motor Cover Type")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.DetailsMotorCoverBasisofSettlementDropdownListId(), this.getData("Motor Basis of Settlement")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.ExtensionsCarHireCheckboxId(), true))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.ExtensionsCarHireLimitofIndemnityDropdownListId(), this.getData("Extensions - Car Hire - Limit of Indemnity")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsCarHirePostingPremiumTextBoxId(), this.getData("Extensions - Car Hire - Posting Premium")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.ExtensionsCarHireConditionsDropdownListId(), this.getData("Extensions - Car Hire - Conditions")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.ExtensionsContingentLiabilityCheckboxId(), true))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsContingentLiabilityLimitofIndemnityTextBoxId(), this.getData("Extensions - Contingent Liability - Limit of Indemnity")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsContingentLiabilityRateTextBoxId(), this.getData("Extensions - Contingent Liability - Rate")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsContingentLiabilityPostingPremiumTextBoxId(), this.getData("Extensions - Contingent Liability - Posting Premium")))
        {
           return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.ExtensionsCreditShortfallCheckboxId(), true))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsCreditShortfallRateTextboxId(), this.getData("Extensions - Credit Shortfall - Rate")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsCreditShortfallPostingPremiumTextboxId(), this.getData("Extensions - Credit Shortfall - Posting Premium")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.ExtensionsExcessWaiverCheckboxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsExcessWaiverPremiumTextboxId(), this.getData("Extensions - Excess Waiver - Premium")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsExcessWaiverPostingPremiumTextboxId(), this.getData("Extensions - Excess Waiver - Posting Premium")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsFireandExplosionRateTextboxId(), this.getData("Extensions - Fire and Explosion - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.ExtensionsLossofKeysCheckboxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsLossofKeysLimitofIndemnityTextBoxId(), this.getData("Extensions -Loss of Keys - Limit of Indemnity")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsLossofKeysPremiumTextBoxId(), this.getData("Extensions -Loss of Keys - Premium")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsLossofKeysPostingPremiumTextBoxId(), this.getData("Extensions -Loss of Keys - Posting Premium")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.ExtensionsParkingFacilitiesCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsParkingFacilitiesLimitofIndemnityTextBoxId(), this.getData("Extensions -Parking Facilities - Limit of Indemnity")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.ExtensionsRiotandStrikeCheckBoxId(),  true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.ExtensionsTheftofCarRadioCheckBoxId(),  true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsTheftofCarRadioRateTextBoxId(),  this.getData("Extensions - Theft of Car Radio  - Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsThirdPartLiabilityRateTextBoxId(),  this.getData("Extensions - Third Party Liability Rate")))
        {
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsThirdPartLiabilityPremiumTextBoxId(),  this.getData("Extensions - Third Party Liability Premium")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.ExtensionsWreckageRemovalCheckBoxId(), true))
         {
             return false;
         } 
         
         if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsWreckageRemovalLimitofIndemnityTextBoxId(), this.getData("Extensions - Wreckage Removal - Limit of Indemnity")))
         {
             return false;
         } 
        
         if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsWreckageRemovalRateTextBoxId(), this.getData("Extensions - Wreckage Removal - Rate")))
         {
             return false;
         } 
        
         if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsWreckageRemovalPostingPremiumTextBoxId(), this.getData("Extensions - Wreckage Removal - Posting Premium")))
         {
             return false;
         } 
         
            SeleniumDriverInstance.scrollDownByOnePage(EkunduMotorSpecifiedRiskPage.ExtensionsWreckageRemovalPostingPremiumTextBoxId());
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.DetailsRateButtonId()))
         {
             return false;
         }
        
            SeleniumDriverInstance.pause((2000));
            SeleniumDriverInstance.takeScreenShot("Vehicle details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
  
           return true;  
    }
    
    private boolean enterFAPDetails()
    {
         if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.FAPBasic3rdpartyMinAmountTextBoxId(), this.getData("FAP - Basic 3rd Party - Min Amount")))
         {
             return true;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.FAPBasicFireandExplosionMinAmountTextBoxId(), this.getData("FAP - Fire and Explosion")))
         {
             return true;
         }
         
            SeleniumDriverInstance.takeScreenShot("First Amount Payable details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
         return true;
     }
     
    private boolean enterEndorsements()
    {
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsSelectButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsAddAllButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsApplyButtonId()))
         {
             return false;
         }
         
         SeleniumDriverInstance.takeScreenShot("Endorsement details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        
            return true;
         
         
     }
    
//    private boolean SelectVehicle()
//    {
//        try
//        {       
//            WebElement VehiclesTable = SeleniumDriverInstance.Driver.findElement(By.id("popupGrid"));
//            List<WebElement> rows = VehiclesTable.findElements(By.tagName("tr"));
//            for(WebElement row: rows)
//            {
//                if(row.findElements(By.tagName("td")).get(2).getText().equals("2011"))
//                {         
//                    row.findElements(By.tagName("td")).get(0).click();
//                    System.out.println("[Info] Vehicle selected, - Year - 2011");
//                    return true;
//                }
//            }
//            
//            System.err.println("[Error] Failed to select vehicle"); 
//            return false;
//            
//        }
//        catch(Exception e)
//        {
//            SeleniumDriverInstance.DriverExceptionDetail = e.getMessage();
//            System.err.println("[Error] Failed to select vehicle, exception detected - Fault - " + e.getMessage());
//            return false; 
//        }
//    }
    
     private boolean SelectVehicle()
    { 
            WebElement VehiclesTable = SeleniumDriverInstance.Driver.findElement(By.id("popupGrid"));
            VehiclesTable.click();
            System.out.println("[Info] Vehicle selected, - Year - 2011");
            return true;
    
    }
     
     private boolean enterNotes()
    { 
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedNotesTextBoxId(), this.getData("Motor Specified Notes")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedAddNotesCheckboxId(), true))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedPolicyNotesTextBoxId(), this.getData("Motor Specified Notes")))
         {
             return false;
         }
            
            SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);
        
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
       
          
            SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
            SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
            
        return true;
              
    }
     
}
