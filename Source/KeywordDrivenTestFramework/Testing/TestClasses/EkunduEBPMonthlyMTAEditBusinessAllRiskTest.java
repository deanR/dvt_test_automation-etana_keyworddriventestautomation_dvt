/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduEHATBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduElectronicEquipmentRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author ferdinandN
 */
public class EkunduEBPMonthlyMTAEditBusinessAllRiskTest extends BaseClass {

    TestEntity testData;
    TestResult testResultt;

    public EkunduEBPMonthlyMTAEditBusinessAllRiskTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToFindClientPage()) {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find client page", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to navigate to the find client page - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!findClient()) {
            SeleniumDriverInstance.takeScreenShot("Failed to find client", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!verifyClientDetailsPageHasLoaded()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the client details page has loaded", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the client details page has loaded- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterMidTermAdjustmentData()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Mid Term Adjustment data", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Mid Term Adjustment data- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!editBusinessAllRisk()) {
            SeleniumDriverInstance.takeScreenShot("Failed to edit Business All Risk", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to edit Business All Risk" + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!editSamsungCellphone()) {
            SeleniumDriverInstance.takeScreenShot("Failed to edit Samsung risk item", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to edit Samsung risk item" + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!AddSixthRiskItemDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to add 6th risk item", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to add 6th risk item" + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!AddSeventhRiskItemDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to add 7th risk item", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to add 7th risk item" + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        return new TestResult(testData, Enums.ResultStatus.PASS, "MTA edit Business All Risk completed successfully", this.getTotalExecutionTime());
    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean navigateToFindClientPage() {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(), EKunduHomePage.FindClientTabXPath())) {
            return false;
        }
        return true;
    }

    private boolean findClient() {
        String clientCode = SeleniumDriverInstance.retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"), "Retrieved Client Code");

        if (clientCode.equals("parameter not found")) {
            clientCode = testData.TestParameters.get("Client Code");
        } else {
            testData.updateParameter("Client Code", clientCode);
        }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId())) {
            return false;
        }

        return true;
    }

    private boolean verifyClientDetailsPageHasLoaded() {
        if (!SeleniumDriverInstance.validateElementTextValueById(EkunduViewClientDetailsPage.ViewClientDetailsLabelId(), "View Client Details")) {
            return false;
        }
        SeleniumDriverInstance.takeScreenShot("Client Details page successfully loaded", false);
        return true;
    }

    private boolean enterMidTermAdjustmentData() {
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText())) {
            return false;
        }

        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                "Retrieved Policy Number");

        if (policyNumber.equals("parameter not found")) {
            policyNumber = testData.TestParameters.get("Policy Number");
        } else {
            testData.updateParameter("Policy Number", policyNumber);
        }

//         if(!SeleniumDriverInstance.changePolicy(policyNumber, "Change"))
//        {
//            return false;
//        }
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ChangePolicyLinkText())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MTATypeofChangeDropDownListId(), this.getData("Type of Change"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MIAEffectiveDateTextBoxId(), this.getData("Effective Date"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId())) {
            return false;
        }

        return true;

    }

    private boolean editBusinessAllRisk() {
        SeleniumDriverInstance.pause(2000);
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.BusinessAllRiskMonthlyActionDropdownListXpath(), "Edit")) {
            return false;
        }

        return true;
    }

    private boolean editSamsungCellphone() {
        SeleniumDriverInstance.pause(1000);
//        SeleniumDriverInstance.Driver.navigate().refresh();
//        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementbyXpath(EkunduBusinessAllRiskPage.EditSamsungRiskItemLinkXpath())) {
            return false;
        }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduBusinessAllRiskPage.BusinessAllRiskItemSumInsuredTextboxId(), this.getData("Samsung New Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskRateButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.uncheckCheckBoxSelectionById(EkunduBusinessAllRiskPage.BusinessAllRiskPolicyNotesCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Cellphone risk item edited successfully", false);

        return true;

    }

    private boolean AddSixthRiskItemDetails() {
        SeleniumDriverInstance.pause(1000);
//        SeleniumDriverInstance.Driver.navigate().refresh();
//        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementbyName(EkunduBusinessAllRiskPage.AddRiskItem6ButtonName())) {
            return false;
        }
        //this.clickAddRiskItemButton();

        SeleniumDriverInstance.pause(1000);
        //SeleniumDriverInstance.clickElementbyName("ctl00$cntMainBody$BAR__ITEM$ctl03$ctl00");
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduBusinessAllRiskPage.BusinessAllRiskItemCategoryDropdownListId(), this.getData("Item 6 Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemSumInsuredTextboxId(), this.getData("Item 6 Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemDescriptionTextboxId(), this.getData("Item 6 Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.EBPItemDetailSerialNumberTextboxId(), this.getData("Item 6 Serial Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemRateTextboxId(), this.getData("Item 6 Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.EBPItemDetailMaxRITextboxId(), this.getData("Item 6 Max RI Exposure"))) {
            return false;
        }

        //extensions
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsReplacementCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsIncreasedCostCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsFAPCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemExtensionsFAPMinPercTextboxId(), this.getData("Business All Risk Item 6 - FAP - Minimum Perc"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemExtensionsFAPMinAmountTextboxId(), this.getData("Business All Risk Item 6 - FAP - Minimum Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemFAPMaxAmountTextboxId(), this.getData("Business All Risk Item 6 - FAP - Maximum Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumTextboxId(), this.getData("Item 6 Flat Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskRateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);
//        SeleniumDriverInstance.Driver.navigate().refresh();
//        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementbyName(EkunduEHATBusinessAllRiskPage.EBPInterestedPartiesAddButtonName())) {
            return false;
        }

        //this.clickAddRiskItemButton(); 
        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.InterestedPartiesNameTextboxId(), this.getData("Interested Party Name 6"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemNotesTextboxId(), this.getData("Business All Risk Item Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemPolicyNotesTextboxId(), this.getData("Business All Risk Item Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Business All Risk Item number 6 added successfully", false);

        return true;

    }

    private boolean AddSeventhRiskItemDetails() {
        SeleniumDriverInstance.pause(1000);
//        SeleniumDriverInstance.Driver.navigate().refresh();
//        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementbyName(EkunduBusinessAllRiskPage.AddRiskItem7ButtonName())) {

            return false;
        }
        //this.clickAddRiskItemButton();
        SeleniumDriverInstance.pause(1000);
        //SeleniumDriverInstance.clickElementbyName("ctl00$cntMainBody$BAR__ITEM$ctl03$ctl00");
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduBusinessAllRiskPage.BusinessAllRiskItemCategoryDropdownListId(), this.getData("Item 7 Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemSumInsuredTextboxId(), this.getData("Item 7 Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemDescriptionTextboxId(), this.getData("Item 7 Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.EBPItemDetailSerialNumberTextboxId(), this.getData("Item 7 Serial Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemRateTextboxId(), this.getData("Item 7 Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.EBPItemDetailMaxRITextboxId(), this.getData("Item 7 Max RI Exposure"))) {
            return false;
        }

        //extensions
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsReplacementCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsIncreasedCostCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.BARItemExtensionsFAPCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemExtensionsFAPMinPercTextboxId(), this.getData("Business All Risk Item 7 - FAP - Minimum Perc"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemExtensionsFAPMinAmountTextboxId(), this.getData("Business All Risk Item 7 - FAP - Minimum Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemFAPMaxAmountTextboxId(), this.getData("Business All Risk Item 7 - FAP - Maximum Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumTextboxId(), this.getData("Item 7 Flat Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskRateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);
        // SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementbyName(EkunduEHATBusinessAllRiskPage.EBPInterestedPartiesAddButtonName())) {
            return false;
        }

        //this.clickAddRiskItemButton(); 
        if (!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.InterestedPartiesNameTextboxId(), this.getData("Interested Party Name 7"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemNotesTextboxId(), this.getData("Business All Risk Item Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemPolicyNotesTextboxId(), this.getData("Business All Risk Item Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Business All Risk Item number 7 added successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;

    }

}
