
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduElectronicEquipmentRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduEnrouteRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduEnrouteRiskTest extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public EkunduEnrouteRiskTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk button is present", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify that the add risk button is present - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterRiskDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter risk details - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterNotes()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk notes", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter risk notes - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        SeleniumDriverInstance.takeScreenShot("Enroute risk entered successfully", false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Enroute risk entered successfully", this.getTotalExecutionTime());

    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean verifyAddRiskButtonisPresent() {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectEnrouteRiskType();

            return true;
        } else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            selectEnrouteRiskType();
        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");

            return false;
        }

        return true;
    }

    private boolean selectEnrouteRiskType() {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.pause(10000);

        SeleniumDriverInstance.takeScreenShot("Enroute risk selected sucessfully", false);

        return true;
    }

    private boolean enterRiskDetails() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduEnrouteRiskPage.RiskPlanTypeDropdownListId(),
                this.getData("Risk - Plan Type"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEnrouteRiskPage.RiskNumberofPersonsTextboxId(),
                this.getData("Risk - No of Persons"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduEnrouteRiskPage.RiskDisabilityWeeksDropdownListId(),
                this.getData("Risk - Diabilibity Weeks"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEnrouteRiskPage.RiskVINNumberTextboxId(),
                this.getData("VIN/Chassis Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduEnrouteRiskPage.RiskRateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Risk details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduEnrouteRiskPage.RiskNextButtonId())) {
            return false;
        }

        return true;

    }

    private boolean enterNotes() {
        if (!SeleniumDriverInstance.clickElementById(EkunduEnrouteRiskPage.RiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskCommentsTextboxId(),
                this.getData("Risk Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskPolicyNotesTextboxId(),
                this.getData("Risk Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Notes entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Documents");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementById("btnGeneratePdf");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementById("popup_ok");
//         
//         
//         SeleniumDriverInstance.pause(3000);
//         
        SeleniumDriverInstance.clickElementbyLinkText("Risks");

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
