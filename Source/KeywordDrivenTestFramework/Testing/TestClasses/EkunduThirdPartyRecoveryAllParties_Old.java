/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.CreateNewClaimPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMaintainCorporateClientClaim1Page;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

/**
 *
 * @author deanR
 */
public class EkunduThirdPartyRecoveryAllParties_Old extends BaseClass
{
    TestEntity testData;
    TestResult testResultt;
    
     public EkunduThirdPartyRecoveryAllParties_Old(TestEntity testData)
    {
        this.testData = testData;
    }
    
     public TestResult executeTest()
    {  
         SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();
        
        if(!navigateToFindClaimPage())
          {
          SeleniumDriverInstance.takeScreenShot(("Failed to navigate to the find claim page"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to navigate to the find claim page- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }
      
         if(!findClaim())
           {
          SeleniumDriverInstance.takeScreenShot(("Failed to find claim"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to find claim- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
           }
         
         if(!ThirdPartyRecovery())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to initiate Third Party Recovery"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to initiate Third Party Recovery- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
         
         if(!navigateToCaseManagementPage())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to navigate to Case management page"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to navigate to Case management page- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
         
         if(!Findcase())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to Find Case"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Find Case- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
         
         if(!OpenClaim())
          {
            SeleniumDriverInstance.takeScreenShot("Claim could not be found", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Claim could not be found-" + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime()) ;
          }
         
            SeleniumDriverInstance.clearTextById(CreateNewClaimPage.lossDateTextBoxId());
            
         if(!findPolicy())
          {
            SeleniumDriverInstance.takeScreenShot("Policy could not be found", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Policy could not be found-" + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }
         
         if(!enterClaimOverviewDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Claim overview details could not be entered", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Claim overview details could not be entered-" + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }
         
         if(!enterPremiumDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Premium details could not be entered", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Premium details could not be entered" + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }
         
         if(!editReserves())
          {
            SeleniumDriverInstance.takeScreenShot("Reserves could not be entered", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Reserves could not be entered" + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }
         
         if(!payClient())
          {
            SeleniumDriverInstance.takeScreenShot("Reserves could not be entered", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Reserves could not be entered" + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }
         
        return new TestResult(testData, Enums.ResultStatus.PASS, "Third party recovery against all parties completed successfully", this.getTotalExecutionTime());
     }
    
     private boolean navigateToFindClaimPage()
    {
       if(!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EkunduMaintainCorporateClientClaim1Page.ClaimHoverTabId(),EkunduMaintainCorporateClientClaim1Page.ClaimSearchLinkXpath()))
        {
            return false;
        }
       
       SeleniumDriverInstance.pause(2000);
       
       if(!SeleniumDriverInstance.waitForElementById(EkunduMaintainCorporateClientClaim1Page.ClaimReferenceTextBoxId()))
       {
           return false;
       }
        SeleniumDriverInstance.takeScreenShot( "Navigation to the Find Claim page was successful", false);
        return true;   
    }
    
    private boolean findClaim()
    {
        if(!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.OkButtonId()))
        {
            return false;
        } 
        
        String claimref = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase1"),"Retrieved Corporate Claim Number");
      
        if(claimref.equals("parameter not found"))
        {
            claimref = testData.TestParameters.get("Claim Reference Number");
        }
        else
        {
            testData.updateParameter("Claim Reference Number", claimref);
        }
    
        if(!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.ClaimReferenceTextBoxId(), claimref))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimFindNowButtonId()))
        {
            return false;
        }
        
       this.RetrieveCaseNumber();
        
        SeleniumDriverInstance.takeScreenShot("Claim found", false);
        
        return true;
        
    }
    
    private boolean ThirdPartyRecovery()
    {
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.TPRecoveryLinkId()))
       {
           return false;
       } 
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilsTPRecoveryLinkId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
       
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AgentRadioButtonId()))
       {
           return false;
       }
       
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
        
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.EditPartyLinkId()))
       {
           return false;
       }
       
       //**********************************************************************************************************
       if(!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            return false;
        }
       
       if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.PaymentDetailsRecievedAmountTextboxId(), testData.TestParameters.get("Recieved Amount")))
         {
             return false;
         }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PaymentDetailsOkButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return false;
        }
       
       //***********************************************************************************************************
       SeleniumDriverInstance.pause(2000);
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ThisReceiptTabLinkText());
       
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MediaRefDropDownListId(), testData.TestParameters.get("Media Ref")))
         {
             return false;
         }
       
       SeleniumDriverInstance.pause(2000);
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilDetailsNextButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilDetailsNextButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMaintainCorporateClientClaim1Page.ClaimReasonforChangeDropdowlistId(), this.getData("Reason")))
       {
           return false;
       } 
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimOkButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimReinsuranceOkButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ReturnToCaseButtonId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.pause(2000);
       
       SeleniumDriverInstance.takeScreenShot( "Third Party Recovery completed successfully", false);
       
       return true;
    }
    
    private boolean RetrieveCaseNumber()
    {
        try
        {
        WebElement cell = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_updClaimSearch\"]/div/div/table/tbody/tr/td[1]"));
        String casenumber = cell.getText();
        System.out.println("Case number retrieved - " +casenumber );
        testData.addParameter("Case Number", casenumber);
        }
        catch (Exception e)
        {
            System.err.println("Failed to retrieve Case number -" + e.getMessage() );
        }
        
        
        return true;
    }
    
    private boolean navigateToCaseManagementPage()
    {
       if(!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EkunduMaintainCorporateClientClaim1Page.ClaimHoverTabId(),EkunduMaintainCorporateClientClaim1Page.CaseManagementLinkXPath()))
        {
            return false;
        }
       
       SeleniumDriverInstance.pause(2000);
       
       if(!SeleniumDriverInstance.waitForElementById(EkunduMaintainCorporateClientClaim1Page.CaseNumberTextBoxId()))
       {
           return false;
       }
        SeleniumDriverInstance.takeScreenShot( "Navigation to the Case management page was successful", false);
        return true;   
    }
    
    private boolean Findcase()
    {
        
        if(!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.OkButtonId()))
        {
            return false;
        } 
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.CaseNumberTextBoxId(), testData.TestParameters.get("Case Number")))
         {
             return false;
         }
        
        SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchCaseButtonId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.takeScreenShot( "case found successfully", false);
       
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.EditCaseLinkId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.pause(3000);
       
       
       return true;
    }
    
    private boolean OpenClaim()
    {
        if(!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.openClaimButtonId()))
        {
            return false;
        }
            SeleniumDriverInstance.takeScreenShot("Claim opened successfully", false);
        return true;
        
    }
    
    private boolean findPolicy()
    {
        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),"Retrieved Policy Number");
      
      if(policyNumber.equals("parameter not found"))
      {
          policyNumber = testData.TestParameters.get("Policy Number");
      }
      else
      {
          testData.updateParameter("Policy Number", policyNumber);
      }
        if(!SeleniumDriverInstance.enterTextById(CreateNewClaimPage.policynumberTextBoxId(), policyNumber))
        {
            return false;
        }
       
        if(!SeleniumDriverInstance.enterTextById(CreateNewClaimPage.lossDateTextBoxId(), testData.TestParameters.get("Loss Date")))
        {
            return false;
        }
        
        clickFindNowButtonId();
        
        selectPolicyId();
           
        return true;
        
        
    }
    
    private boolean clickFindNowButtonId()
    {
        if(!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.findnowButtonId()))
        {
        return false;
        }
        
            SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementById(CreateNewClaimPage.SelectPolicyLinkId()))
        {
            SeleniumDriverInstance.takeScreenShot("Policy doesn't exist", true);
            return false;
        }
            SeleniumDriverInstance.takeScreenShot("Policy found", false);
        return true;
    }
    
    private boolean selectPolicyId()
    {
        if(!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.SelectPolicyLinkId()))
        {
        return false;
        }
            SeleniumDriverInstance.takeScreenShot("Policy selected", false);
        return true;
    }
    
    private boolean enterClaimOverviewDetails()
    {
            SeleniumDriverInstance.pause(3000);
            SeleniumDriverInstance.WaitUntilDropDownListPopulatedById(CreateNewClaimPage.RiskTypeDropdownListId());

        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.RiskTypeDropdownListId(), testData.TestParameters.get("Risk Type")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.ProgressStatusDropdownListId(), testData.TestParameters.get("Progress Status")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.PrimaryCauseDropdownListId(), testData.TestParameters.get("Primary Cause")))
        {
            return false;
        }

            JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;

            js.executeScript("__doPostBack('UpdateSecondaryCause', '');setTimeout('__doPostBack(\\'ctl00$cntMainBody$CONTROL__PRIMARY_CAUSE\\',\\'\\')', 0)");

            SeleniumDriverInstance.WaitUntilDropDownListPopulatedById(CreateNewClaimPage.SecondaryCauseDropdownListId());
             
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.SecondaryCauseDropdownListId(), testData.TestParameters.get("Secondary Cause")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.ClaimsHandlerDropdownListId(), testData.TestParameters.get("Claims Handler")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(CreateNewClaimPage.ClaimDescriptionTextBoxId(), testData.TestParameters.get("Claim Description")))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Claim overview details entered successfully", false);
         
        if(!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId()))
        {
            return false;
        }
        
            SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());
            
            SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.checkBoxSelectionById("ctl00_cntMainBody_EBP_CLM_MOTOR__COND_CONF",true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById("ctl00_cntMainBody_EBP_CLM_MOTOR__FAST_TRACK_APPL",true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.PremiumConfirmerDropdownListId(), testData.TestParameters.get("Premium Confirmer")))
        {
            return false;
        }
        
        return true;     
    }
    
    private boolean enterPremiumDetails()
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.PremiumConfirmerDropdownListId(), testData.TestParameters.get("Premium Confirmer")))
        {
            return false;
        }
        
            JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;
        
            js.executeScript("document.getElementById('"+ CreateNewClaimPage.ClaimNextButtonId() + "').click();");

            SeleniumDriverInstance.takeScreenShot("Premium details entered successfully", false);

            if(!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId()))
       {
           return false;
       }
       
            SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

            SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

            SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

            SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

            SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

            SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

            SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());
       
            SeleniumDriverInstance.pause(1000);
            
            return true;
    }
    
    private boolean editReserves()
    {
        SeleniumDriverInstance.scrollElementIntoViewById(CreateNewClaimPage.LoadReserveEditLinkId());
        
        if(!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.LoadReserveEditLinkId()))
        {
        return false;
        } 
        
        if(!SeleniumDriverInstance.enterTextById(CreateNewClaimPage.ClaimReserverOwnDamageNewValueTextBoxId(), testData.TestParameters.get("Reserve New Value 1")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(CreateNewClaimPage.ClaimReserverThirdPartyNewValueTextBoxId(), testData.TestParameters.get("Reserve New Value 2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(CreateNewClaimPage.ClaimReserverSalvageNewValueTextBoxId(), testData.TestParameters.get("Reserve New Value 3")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ReserverFinishButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.PerilsSubmitButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimReinsuranceOkButtonId()))
       {
           return false;
       }
        
        ClaimSelect();
        
            return true;
    }
    
    private boolean ClaimSelect()
    {
        String retrievedClaimRef = SeleniumDriverInstance.retrieveTextById(EkunduViewClientDetailsPage.ClaimReferenceNumberLabelId());

        this.testData.addParameter("Retrieved Claim Number", retrievedClaimRef);

        System.out.println("[Info] Claim number retrieved  - Ref. - " + retrievedClaimRef);

        SeleniumDriverInstance.takeScreenShot("Claim reference number retrieved successfully - " + retrievedClaimRef,false);
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ReturnToCaseButtonId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.pause(2000);
        
        if (!SeleniumDriverInstance.Payclaim(retrievedClaimRef, "Pay"))
          {
            return false;
          }
        
        return true;
    }
    
    private boolean payClient()
    {
       
       if(!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMaintainCorporateClientClaim1Page.ClaimReasonforChangeDropdowlistId(), this.getData("Pay Client Reason")))
       {
           return false;
       } 
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimOkButtonId()))
       {
           return false;
       }
       
       
            SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

            SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

            SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

            SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

            SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

            SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

            SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());
       
      // this.clicknextButton();
       
       SeleniumDriverInstance.scrollElementIntoViewById(CreateNewClaimPage.PerilsBuildingsDetailsLinkId());
       
       if(!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.PerilsBuildingsDetailsLinkId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.PaymentDetailsTabPartialLinkText());
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.PaymentDetailsTabPartialLinkText());
       
       SeleniumDriverInstance.pause(2000);
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.PaymentDetailsInsuredRadioButtonId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.pause(2000);
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.PaymentDetailsTabPartialLinkText());
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.PaymentDetailsTabPartialLinkText());
       
//       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
//       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.PaymentDetailsTabPartialLinkText());
       
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.EditParty1LinkId()))
       {
           return false;
       }
        
        //**********************************************************************************************************
       if(!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            return false;
        }
       
       if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.PaymentDetailsRecievedAmountTextboxId(), testData.TestParameters.get("Insured Payment Amount")))
         {
             return false;
         }
       
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.PaymentDetailsTaxGroupDropDownListId(), testData.TestParameters.get("Tax Group")))
         {
             return false;
         }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PaymentDetailsOkButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return false;
        }
       
       //***********************************************************************************************************
       SeleniumDriverInstance.pause(2000);
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ReserverFinishButtonId()))
        {
            return false;
        }
       
       if(!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.PerilsSubmitButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimReinsuranceOkButtonId()))
       {
           return false;
       }
        
        return true;
    }
    
    private void clicknextButton()
    {
        do
        {
            
       if(!SeleniumDriverInstance.waitForElementById(CreateNewClaimPage.PerilsBuildingsDetailsLinkId()))
       {
            SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());
       }
        }
        
         while(!SeleniumDriverInstance.waitForElementById(CreateNewClaimPage.PerilsBuildingsDetailsLinkId()));
      
    }
    
    public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
   
}
