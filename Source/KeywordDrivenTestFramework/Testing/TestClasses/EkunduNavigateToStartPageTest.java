
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import static KeywordDrivenTestFramework.Entities.Enums.TestEnvironments.*;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author fnell
 */
public class EkunduNavigateToStartPageTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;
    String VerifyTestDataResult, Branch;

    public EkunduNavigateToStartPageTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        verifyTestData();

        if (!VerifyTestDataResult.equals(""))
          {
            return new TestResult(testData, Enums.ResultStatus.FAIL, VerifyTestDataResult, this.getTotalExecutionTime());
          }

        this.setStartTime();

        if (!navigateToEkunduApplication())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to Ekundu application", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to Ekundu application - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!verifyHomePageHasLoaded())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that Ekundu home page has loaded", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that Ekundu home page has loaded - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!loginToApplication())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to login to Ekundu application", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to login to Ekundu application  - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

//      if(!verifyHomePageHasLoaded())
//      {
//        SeleniumDriverInstance.takeScreenShot( "Failed to verify that Ekundu home page has loaded after login", true);
//        return new TestResult(testData,false, "Failed to verify that Ekundu home page has loaded after login- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//      }
//        if (!changeBranch())
//          {
//            SeleniumDriverInstance.takeScreenShot("Failed to change regional branch", true);
//
//            return new TestResult(testData, false,
//                                  "Failed to change regional branch - " + SeleniumDriverInstance.DriverExceptionDetail,
//                                  this.getTotalExecutionTime());
//          }

        if (!retrieveAppVersion())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to retrieve Application version", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to retrieve Application version - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!addPageUrlParameter())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to add page URL parameter", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to add page URL parameter - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Successfully navigated to the start page", this.getTotalExecutionTime());

      }

    private void verifyTestData()
      {
        VerifyTestDataResult = "";

        if ((testData.TestParameters == null) ||!testData.TestParameters.containsKey("Branch"))
          {
            VerifyTestDataResult = "Branch not specified";
          }
        else
          {
            Branch = testData.TestParameters.get("Branch");
          }
      }

    private boolean navigateToEkunduApplication()
      {
        if (!SeleniumDriverInstance.navigateTo(EKunduHomePage.PageUrl()))
          {
            return false;
          }
        else
          {
            SeleniumDriverInstance.takeScreenShot("Navigation to the home page was successful", false);

            return true;
          }
      }

    private boolean verifyHomePageHasLoaded()
      {
        try
          {
            if (!SeleniumDriverInstance.waitForElementById(EKunduHomePage.HomeDivId()))
              {
                return false;
              }
            else
              {
                SeleniumDriverInstance.takeScreenShot("The Ekundu home page was loaded successfully", false);
              }

            return true;
          }
        catch (Exception e)
          {
            return false;
          }
      }

    private boolean loginToApplication()
      {

        if(CurrentEnvironment == Finance)
        {
            if (!SeleniumDriverInstance.enterTextById(EKunduHomePage.UsernameTextBoxId(),
                testData.TestParameters.get("User Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EKunduHomePage.PasswordTextBoxId(),
                testData.TestParameters.get("Password")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EKunduHomePage.LoginButtonId()))
          {
            return false;
          } 
              
          else
          {

            if (!SeleniumDriverInstance.moveToAndClickElementByLinkText("Logout"))
              {
                return false;
              }

            if (!SeleniumDriverInstance.enterTextById(EKunduHomePage.UsernameTextBoxId(),
                    testData.TestParameters.get("User Name")))
              {
                return false;
              }

            if (!SeleniumDriverInstance.enterTextById(EKunduHomePage.PasswordTextBoxId(),
                    testData.TestParameters.get("Password")))
              {
                return false;
              }

            if (!SeleniumDriverInstance.clickElementById(EKunduHomePage.LoginButtonId()))
              {
                return false;
              }

          }
        
        }

        else
        {
            if (!SeleniumDriverInstance.moveToAndClickElementByLinkText("Logout"))
              {
                return false;
              }

            if (!SeleniumDriverInstance.enterTextById(EKunduHomePage.UsernameTextBoxId(),
                    testData.TestParameters.get("User Name")))
              {
                return false;
              }

            if (!SeleniumDriverInstance.enterTextById(EKunduHomePage.PasswordTextBoxId(),
                    testData.TestParameters.get("Password")))
              {
                return false;
              }

            if (!SeleniumDriverInstance.clickElementById(EKunduHomePage.LoginButtonId()))
              {
                return false;
              }
            
        }

        SeleniumDriverInstance.takeScreenShot("Login was successful", false);

        return true;
      }
      
    private boolean changeBranch()
      {
        try
          {
            Thread.sleep(4000);
            //SeleniumDriverInstance.Driver.navigate().refresh();
          }
        catch (Exception e)
          {
          }

        if (!SeleniumDriverInstance.moveToAndClickElementById(EKunduHomePage.ChangeBranchLinkId()))
          {
          }

        if (!SeleniumDriverInstance.switchToFrameById(EKunduHomePage.ChangeBranchFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EKunduHomePage.SelectBranchDropDownListId(),
                this.Branch))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduHomePage.SelectBranchOkButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContentWhenElementNoLongerVisible(EKunduHomePage.ChangeBranchFrameId()))
          {
            return false;
          }

        return true;
      }

    private boolean retrieveAppVersion()
      {
        String app_version =
            SeleniumDriverInstance.retrieveTextById(EkunduViewClientDetailsPage.EkunduAppVersionLabelId());

        this.testData.addParameter("Application Version", app_version);

        System.out.println("[Info] Application Version number retrieved successfully - " + app_version);

        return true;

      }

    private boolean addPageUrlParameter()
      {
        String pageurl = EKunduHomePage.PageUrl();

        this.testData.addParameter("Page URL/Environment", pageurl);

        System.out.println("[Info] Page URL added successfully - " + pageurl);

        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
