/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPageNew;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/*
 *
 * @author sncantswa
 */

public class EtanaVehicleOver20Years  extends BaseClass
{
    TestEntity testData;
    TestResult testResultt;
    
     public EtanaVehicleOver20Years(TestEntity testData)
    {
        this.testData = testData;
    }
    
     public TestResult executeTest()
    {
        this.setStartTime();
        
        if(testData.TestMethod.toUpperCase().contains("EBP-Vehicle Older Than 20 Years".toUpperCase()))
         {
             //<editor-fold defaultstate="collapsed" desc="Vehicle Older Than 20 Years">
        
            if (!verifyRisksDialogisPresent())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to verify Risks Dialogis Present"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            if (!RiskAddressInfor())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to verify Risk Address Infor"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify Risk Address Infor " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            if (!addVehicleoverthan20())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to add Vehicle over than 20 years"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to add Vehicle over than 20 years- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
//            if (!FirstAmountPayablepageSpecialType())
//            {
//              SeleniumDriverInstance.takeScreenShot(("Failed to add First Amount Payable page Special Type"), true);
//              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//            }
//            if (!enterEndorsements())
//            {
//              SeleniumDriverInstance.takeScreenShot(("Failed to enter Endorsements"), true);
//              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//            }
//            if (!specifiedDriverPage())
//            {
//              SeleniumDriverInstance.takeScreenShot(("Failed to enter specified Driver Page"), true);
//              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//            }
//            if (!enterInterestedParties())
//            {
//              SeleniumDriverInstance.takeScreenShot(("Failed to enter Interested Parties"), true);
//              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//            }
//            if (!enterInterestedPartyNotes())
//            {
//              SeleniumDriverInstance.takeScreenShot(("Failed to enter Interested Party Notes"), true);
//              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//            }
//            if (!clickFinancialOverViewNextButton())
//            {
//              SeleniumDriverInstance.takeScreenShot(("Failed to click Financial Over View Next Button"), true);
//              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//            }
//            if (!enterMotorSpecifiedNotes())
//            {
//              SeleniumDriverInstance.takeScreenShot(("Failed to enter Motor Specified Notes"), true);
//              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//            }
//            if (!clickReinsuranceDetails())
//            {
//              SeleniumDriverInstance.takeScreenShot(("Failed to click Reinsurance Details"), true);
//              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//            }
//            if (!findReInsure())
//            {
//              SeleniumDriverInstance.takeScreenShot(("Failed to click find ReInsure"), true);
//              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//            }
//             if (!placementDetails())
//            {
//              SeleniumDriverInstance.takeScreenShot(("Failed to click placement Details"), true);
//              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//            }
           //</editor-fold >
         }
         SeleniumDriverInstance.takeScreenShot(("Captured EBP Annual Policy Test successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Vehicle Older Than 20 Years added successfully", this.getTotalExecutionTime());
    }
    
   // Step 1.2.2.6 Vehicles older than 20 Years
    
    private String getData(String parameterName)
    {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu verify Risks Dialogis Present Methods">
    private boolean verifyRisksDialogisPresent()
    {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            selectMotorRiskType();

            return true;
          }

        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectMotorRiskType();

            return true;

          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu select Motor Risk Type Methods">
    private boolean selectMotorRiskType()
    {
        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }
        

        SeleniumDriverInstance.takeScreenShot("Motor risk type selected successfully", false);

        return true;

      }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu Risk Adress page Methods">
     private boolean RiskAddressInfor()
     {
         //click the next button to continue
        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPageNew.BusinessAllRiskNextButtonId()))
        {
            return false;
        }
         return true;
     }
    //</editor-fold>
     
    //<editor-fold defaultstate="collapsed" desc="Ekundu add Vehicle over than 20">
    private boolean addVehicleoverthan20()
    {
        System.out.println("Vehicle Type...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu vehicle type Methods">
//        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
//          {
//            return false;
//          }
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SearchVehicleModelIconId()))
          {
            return false;
          }
        SeleniumDriverInstance.pause(3000);
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehiclesSourceDropdownListId(),
                testData.TestParameters.get("Source")))
          {
            return false;
          }
        SeleniumDriverInstance.pause(3000);
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleMakeOlderVehiclesDropdownListId(),
                testData.TestParameters.get("Make")))
          {
            return false;
          }
        SeleniumDriverInstance.pause(3000);
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleYearTextBoxId(),
                testData.TestParameters.get("Year")))
          {
            return false;
          }
        SeleniumDriverInstance.pause(3000);
         if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleModelOlderVehiclesDropdownListId(),
                testData.TestParameters.get("Model")))
          {
            return false;
          }
        SeleniumDriverInstance.pause(3000);
         if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CCTextBoxId(),
                 testData.TestParameters.get("CC")))
          {
            return false;
          }
        SeleniumDriverInstance.pause(3000);
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.GVMTextBoxId(),
                testData.TestParameters.get("GVM")))
          {
            return false;
          }
        SeleniumDriverInstance.pause(3000);
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleBodyTypeOlderVehiclesDropdownListId(),
                testData.TestParameters.get("Body Type")))
          {
            return false;
          }
         if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleTypeDropdownListId(),
                testData.TestParameters.get("Vehicle Type")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.VehiclesCaptureCriteriaButtonId()))
          {
            return false;
          }
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorClassofUseDropdownListId(),
                testData.TestParameters.get("Class of use")))
          {
            return false;
          }
        
        
        SeleniumDriverInstance.pause(2000);
        //</editor-fold>
        
        System.out.println("Options...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu options Methods">
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.EndorsementsCheckBoxId(), true))
          {
            return false;
          }
            
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesCheckBoxId(), true))
         {
            return false;
         }
            
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriversCheckBoxId(), true))
         {
            return false;
         } 
         //</editor-fold>
        
        System.out.println("Claim history and  underwritten criteria...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu Claim history and  underwritten criteria Methods">
         if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleOvernightParkingDropdownListId(),
                testData.TestParameters.get("Overnight Packing")))
         {
            return false;
         }
        //</editor-fold>
         
        System.out.println("main Driver...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu main Driver">
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MainDriverRegularDriverTextboxId(),
                testData.TestParameters.get("Driver Name")))
          {
            return false;
          }
         if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MainDriverDateLicenseIssuedTextboxId(),
                 testData.TestParameters.get("License Issued")))
          {
            return false;
          }
        SeleniumDriverInstance.pause(3000);
        //click next
         if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BasicDetailsNextButtonName()))
          {
            return false;
          }
        //</editor-fold>
        
        System.out.println("Vehicle Cover Details...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu options Methods">
         //enter area code
         if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AreaCodeTextboxId(),
                  testData.TestParameters.get("Area Code")))
          {
            return false;
          }
        //select tracking device
         if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorTrackingDeviceDropdownListId() ,
                 testData.TestParameters.get("Tracking Device")))
          {
            return false;
          }
         
        //</editor-fold>
         
        System.out.println("Registration...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu vehicle type Methods">
         if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EngineNumberTextboxId(),
                testData.TestParameters.get("Engine Number")))
         {
            return false;
         }
          if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ChassisNumberTextboxId(),
                testData.TestParameters.get("Chassis Number")))
         {
            return false;
         }
         if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationNumberTextboxId(),
                testData.TestParameters.get("Registration Number")))
         {
            return false;
         }
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleRegOwnerTextBoxId(),
                testData.TestParameters.get("Registered Owner")))
         {
            return false;
         }
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationDateTextboxId(),
                testData.TestParameters.get("Registration Date")))
         {
              return false;
         }
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NATISCodeDropdownListId(),
                testData.TestParameters.get("NATIS Code")))
         {
              return false;
         }
        //</editor-fold>
        
        System.out.println("Motor Cover...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu Motor Cover Methods">
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleSumInsuredTextBoxId(),
                testData.TestParameters.get("Sum Insured")))
         {
            return false;
         }
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleCoverTypeDropdownListId(),
                testData.TestParameters.get("Cover Type")))
         {
            return false;
         }
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BasisofSettlementDropdownListId(),
                testData.TestParameters.get("Basis of Settlement")))
         {
              return false;
         }
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddAccessoriesButtonName()))
         {
            return false;
         }
        //</editor-fold >
        
         System.out.println("Accessories and equipment...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu Accessories and equipment Methods">
        
//        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessoryDescriptionTextBoxId(),
//            testData.TestParameters.get("Accessory Description")))
//         {
//            return false;
//         }
//       
//        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessorySumInsuredTextBoxId(),
//            testData.TestParameters.get("Accessory Sum Insured")))
//         {
//            return false;
//         }
//            
//        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
//        {
//            return false;
//        }
        //</editor-fold>
        
        System.out.println("Extensions...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu Extensions Methods">
//        System.out.println("Contigent Liability...");
//        //<editor-fold defaultstate="collapsed" desc="Contigent Liability">
//        //check Contigent Liability
//        if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.ExtensionsContingentLiabilityCheckboxId()
//                , true))
//         {
//            return false;
//         }
//        //enter limit of indemnity
//        if  (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsContingentLiabilityLimitofIndemnityTextBoxId(),
//                "Contingent Liability Limit of Indemnity"))
//         {
//            return false;
//         }
//        //enter limit of indemnity rate
//        if  (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsContingentLiabilityRateTextBoxId(),
//                "Contingent Liability Rate"))
//         {
//            return false;
//         }
//        //click buttn rate
//        if  (!SeleniumDriverInstance.clickElementById(""))
//         {
//            return false;
//         }
//      //</editor-fold >
//        System.out.println("Credit Shortfall...");
//        //<editor-fold defaultstate="collapsed" desc="Credit Shortfall">
//        //check Credit Shortfall
//        if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.ExtensionsCreditShortfallCheckboxId(),
//                true))
//         {
//            return false;
//         }
//        //enter Credit Shortfall rate
//        if  (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsCreditShortfallRateTextboxId(), 
//                "Credit Shortfall Rate"))
//         {
//            return false;
//         }
//      //</editor-fold>
//        System.out.println("Excess Waiver...");
//        //<editor-fold defaultstate="collapsed" desc="Excess Waiver">
//        //enter Excess Waiver premium
//        if  (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsExcessWaiverPremiumTextboxId()
//                , "Excess Waiver Premium"))
//         {
//            return false;
//         }
//        //click buttn rate
//        if  (!SeleniumDriverInstance.clickElementById(""))
//         {
//            return false;
//         }
//      //</editor-fold>
//        System.out.println("Fire and explosion...");
//        //<editor-fold defaultstate="collapsed" desc="Fire and Explosion">
//        //enter fire and explosion Rate
//        if  (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsFireandExplosionRateTextboxId(),
//                "Fire and Explosion Rate"))
//        {
//            return false;
//        }
//        //enter fire and explosion Premium
//        if  (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsFireandExplosionPremiumTextboxId(),
//                "Fire and Explosion Premium"))
//        {
//            return false;
//        }
//        //click buttn rate
//        if  (!SeleniumDriverInstance.clickElementById(""))
//         {
//            return false;
//         }
//        //</editor-fold>
//        System.out.println("Loss of keys...");
//        //<editor-fold defaultstate="collapsed" desc="Loss of keys">
//        //check Loss of keys
//        if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.ExtensionsLossofKeysCheckboxId(), true))
//         {
//            return false;
//         }
//        //enter Loss of keys limit of indemnity
//        if  (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsLossofKeysLimitofIndemnityTextBoxId(), 
//                "Loss of Keys Limit of Indemnity"))
//        {
//            return false;
//        }
//        //enter Loss of keys Premium
//        if  (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsLossofKeysPremiumTextBoxId(), 
//                "Loss of Keys Premium"))
//        {
//            return false;
//        }
//        //click buttn rate
//        if  (!SeleniumDriverInstance.clickElementById(""))
//         {
//            return false;
//         }
//        //</editor-fold>
//        System.out.println("Third party liability...");
//        //<editor-fold defaultstate="collapsed" desc="Third party liability">
//        //enter Third party Rate
//        if  (!SeleniumDriverInstance.enterTextById("", ""))
//        {
//            return false;
//        }
//        //enter Third party Premium
//        if  (!SeleniumDriverInstance.enterTextById("", ""))
//        {
//            return false;
//        }
//        //click buttn rate
//        if  (!SeleniumDriverInstance.clickElementById(""))
//         {
//            return false;
//         }
//        //</editor-fold>
//        System.out.println("Wreckage Removal...");
//        //<editor-fold defaultstate="collapsed" desc="Wreckage Removal">
//        //check Wreckage Removal
//        if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.ExtensionsWreckageRemovalCheckBoxId(), true))
//        {
//            return false;
//        }
//        //enter Wreckage Removal limited liability
//        if  (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsWreckageRemovalLimitofIndemnityTextBoxId(), 
//                "Wreckage Removal Limit of Indemnity"))
//        {
//            return false;
//        }
//        //enter Wreckage Removal rate
//        if  (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsWreckageRemovalRateTextBoxId(), 
//                "Wreckage Removal Rate"))
//        {
//            return false;
//        }
//        //click buttn rate
//        if  (!SeleniumDriverInstance.clickElementById(""))
//         {
//            return false;
//         }
//        //</editor-fold>
        //</editor-fold>
        
        System.out.println("Voluntary Excess...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu Voluntary Excess Methods">
//     
//        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryExcessDiscountTextBoxId(),
//                testData.TestParameters.get("Excess Discount")))
//         {
//            return false;
//         } 
        //</editor-fold>
        
        System.out.println("Additional Compulsory Excess...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu Additional Compulsory Excess Methods">

//        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryExcessPercentageTextBoxId(),
//                testData.TestParameters.get("Compulsory Excess Percentage")))
//            {
//              return false;
//            }
//        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryMinimumAmountTextBoxId(),
//                testData.TestParameters.get("Compulsory Minimum Amount")))
//            {
//              return false;
//            }
//        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//            {
//              return false;
//            }
        //</editor-fold>
        return true;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu First Amount Payable Special Type car page Methods">
    private boolean FirstAmountPayablepageSpecialType()
    {
      System.out.println("Own Damage...");
      //<editor-fold defaultstate="collapsed" desc="Own Damage">
      //enter Own Damage maximum%
      if  (!SeleniumDriverInstance.enterTextById("", ""))
        {
            return false;
        }
      //enter Own Damage maximum amount
      if  (!SeleniumDriverInstance.enterTextById("", ""))
        {
            return false;
        }
      //</editor-fold >
      System.out.println("third party...");
      //<editor-fold defaultstate="collapsed" desc="third party">
      //enter third party minimum%
      if  (!SeleniumDriverInstance.enterTextById("", ""))
        {
            return false;
        }
      //enter third party maximum%
      if  (!SeleniumDriverInstance.enterTextById("", ""))
        {
            return false;
        }
      //enter third party minimum amount
      if  (!SeleniumDriverInstance.enterTextById("", ""))
        {
            return false;
        }
      //enter third party maximum amount
      if  (!SeleniumDriverInstance.enterTextById("", ""))
        {
            return false;
        }
      //</editor-fold>
      System.out.println("Fire and Explosion...");
      //<editor-fold defaultstate="collapsed" desc="Fire and Explosion">
      //enter fire and explosion minimum%
      if  (!SeleniumDriverInstance.enterTextById("", ""))
        {
            return false;
        }
      //enter fire and explosion maximum%
      if  (!SeleniumDriverInstance.enterTextById("", ""))
        {
            return false;
        }
      //enter fire and explosion minimum amount
      if  (!SeleniumDriverInstance.enterTextById("", ""))
        {
            return false;
        }
      //enter fire and explosion maximum amount
      if  (!SeleniumDriverInstance.enterTextById("", ""))
        {
            return false;
        }
       //</editor-fold>
      System.out.println("Theft Hijack...");
      //<editor-fold defaultstate="collapsed" desc="Theft Hijack">
       //check Theft Hijack minimum%
      if  (!SeleniumDriverInstance.checkBoxSelectionById("", true))
        {
            return false;
        }
      //enter Theft Hijack minimum%
      if  (!SeleniumDriverInstance.enterTextById("", ""))
        {
            return false;
        }
      //enter Theft Hijack maximum%
      if  (!SeleniumDriverInstance.enterTextById("", ""))
        {
            return false;
        }
      //enter Theft Hijack minimum amount
      if  (!SeleniumDriverInstance.enterTextById("", ""))
        {
            return false;
        }
      //enter Theft Hijack maximum amount
      if  (!SeleniumDriverInstance.enterTextById("", ""))
        {
            return false;
        }
       //</editor-fold>
      //click next amount payable
      if  (!SeleniumDriverInstance.clickElementById(""))
        {
            return false;
        }
       return true;
     }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu endossments page methods">
      private boolean enterEndorsements()
    {
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsSelectButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsAddAllButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsApplyButtonId()))
         {
             return false;
         }
         
         
         SeleniumDriverInstance.takeScreenShot("Endorsement details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        
            return true;
         
         
     }
    //</editor-fold>
      
    //<editor-fold defaultstate="collapsed" desc="Ekundu specified driver Methods">
    private boolean specifiedDriverPage()
    {
      System.out.println("Driver1...");
      //<editor-fold defaultstate="collapsed" desc="driver">
       //click add driver button
      if  (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddSpecifiedDriverButtonName()))
        {
            return false;
        }
      //enter driver name
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverNameTextBoxId(),
              "Driver"))
        {
            return false;
        }
      //select id type
      if  (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverIDTypeDropdownListId()",
              "ID Type"))
        {
            return false;
        }
      //enter id number
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverIDNumberTextBoxId(), 
              "ID Number"))
        {
            return false;
        }
       //</editor-fold>
      System.out.println("Driver2...");
      //<editor-fold defaultstate="collapsed" desc="driver">
       //click add driver button
      if  (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddSpecifiedDriverButtonName()))
        {
            return false;
        }
      //enter driver name
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverNameTextBoxId(),
              "Driver2"))
        {
            return false;
        }
      //select id type
      if  (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverIDTypeDropdownListId()",
              "ID Type2"))
        {
            return false;
        }
      //enter id number
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverIDNumberTextBoxId(), 
              "ID Number2"))
        {
            return false;
        }
       //</editor-fold>
      //click next driver button
      if  (!SeleniumDriverInstance.clickElementById("EkunduCreateNewPolicyForNewClientPage.NextButtonId()"))
        {
            return false;
        }
      
      //click next driver button
      if  (!SeleniumDriverInstance.clickElementById("EkunduCreateNewPolicyForNewClientPage.NextButtonId()"))
        {
            return false;
        }
      return true;
    }
    //</editor-fold> 
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu enter Interested Parties Methods">
    private boolean enterInterestedParties()
    {
          //SeleniumDriverInstance.Driver.navigate().refresh();
          SeleniumDriverInstance.pause(3000);
          
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.MSAddInterestedPartiesButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MSTypeofAgreementTypeDropdownListId(),
                testData.TestParameters.get("Type of Agreement")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.InterestedPartyInstitutionNameDropdownListId(),
                testData.TestParameters.get("Institution Name")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Interested parties details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
        return true;
        
      }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu enter Interested Party Notes Methods">
    private boolean enterInterestedPartyNotes()
    { 
        //enter Interested Party comment
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.InterestedPartyNotesTextBoxId(),
                "Interested Party Notes"))
         {
             return false;
         }
        //check add notes to policy
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesCheckBoxId(), true))
         {
             return false;
         }
        //enter Interested Party printed and schedule
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesCommentsTextAreaId(),
                "Interested Parties Comments"))
         {
             return false;
         }
         //click the finnish button  
         SeleniumDriverInstance.takeScreenShot("interested parties notes entered successfully", false);
        
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesCommentsNextButtonName()))
         {
             return false;
         }
         SeleniumDriverInstance.clickElementById("");   
        return true;      
    }
     //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu click Financial Over View Next Methods">
     private boolean clickFinancialOverViewNextButton()
     {
        System.out.println("Adjustment...");
         //select premium adjusment basis
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.PremAdjBasisDropdownListId(),
                 "Adjustment Basis"))
         {
            return false;
         }
         //enter flat premium amount
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MotorSpecifiedAdjustmentPercentageTextBoxId()
                 ,"Adjustment Percentage"))
         {
            return false;
         }
         //select premium adjusment reason
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.PremAdjReasonDropdownListId(),
                 "Adjustment Reason"))
         {
            return false;
         }
        if(!SeleniumDriverInstance.clickElementById(""))
         {
            return false;
         }
         return true;
     }
     //</editor-fold>
     
    //<editor-fold defaultstate="collapsed" desc="Ekundu enter MotorS pecified Notes Methods">
    private boolean enterMotorSpecifiedNotes()
    { 
        //enter Motor Specified comment
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedNotesTextBoxId()
                , "Specified Notes Comment"))
         {
             return false;
         }
        //check add notes to policy
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedAddNotesCheckboxId(), true))
         {
             return false;
         }
        //enter Motor Specified printed and schedule
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedPolicyNotesTextBoxId(),
                "Party Notes"))
         {
             return false;
         }
         //click the next button  
         SeleniumDriverInstance.takeScreenShot("Motor Specified notes entered successfully", false);
        
        
        if(!SeleniumDriverInstance.clickElementById(""))
         {
             return false;
         } 
        return true;      
    }
     //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu click Reinsurance Details Methods">
     private boolean clickReinsuranceDetails()
     {
         //click Reinsurance details tab
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedReinsuranceDetailstabLinkText()))
         {
             return false;
         } 
          //click Reinsurance details tab
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BandDropdownListId(),
                 "MS Liability (Section B)"))
         {
             return false;
         }
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BandDropdownListId(),
                 "MS Own Damage (Section A)"))
         {
             return false;
         }
         //click Reinsurance Add FAC XOL 
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddPropFACButtonId()))
         {
             return false;
         } 
         return true;
     }
    //</editor-fold>
     
    //<editor-fold defaultstate="collapsed" desc="Ekundu Reinsure Methods">
     private boolean findReInsure()
     {
        //enter Reinsurer code
         if(!SeleniumDriverInstance.enterTextById("EkunduMotorSpecifiedRiskPage",""))
         {
             return false;
         } 
        //click the search button
        if(!SeleniumDriverInstance.clickElementById("EkunduMotorSpecifiedRiskPage"))
         {
             return false;
         } 
         //click select from a list
        if(!SeleniumDriverInstance.clickElementById("EkunduMotorSpecifiedRiskPage"))
         {
             return false;
         } 
         return true;
     }
     //</editor-fold>
     
    //<editor-fold defaultstate="collapsed" desc="Ekundu placement Details Methods">
     private boolean placementDetails()
     {
        //enter FAC Lower limit
         if(!SeleniumDriverInstance.enterTextById("EkunduMotorSpecifiedRiskPage","Lower Limit"))
         {
             return false;
         } 
         //enter FAC Upper limit
         if(!SeleniumDriverInstance.enterTextById("EkunduMotorSpecifiedRiskPage","Upper Limit"))
         {
             return false;
         } 
         //enter caisse participation %
         if(!SeleniumDriverInstance.enterTextById("EkunduMotorSpecifiedRiskPage","CAISSE-Participation"))
         {
             return false;
         } 
         //enter caisse premium
         if(!SeleniumDriverInstance.enterTextById("EkunduMotorSpecifiedRiskPage","CAISSE-Premium"))
         {
             return false;
         } 
         //click button ok
         if(!SeleniumDriverInstance.enterTextById("EkunduMotorSpecifiedRiskPage",""))
         {
             return false;
         } 
         //click button ok
         if(!SeleniumDriverInstance.enterTextById("EkunduMotorSpecifiedRiskPage",""))
         {
             return false;
         } 
       return true;
     }
     //</editor-fold>
}
