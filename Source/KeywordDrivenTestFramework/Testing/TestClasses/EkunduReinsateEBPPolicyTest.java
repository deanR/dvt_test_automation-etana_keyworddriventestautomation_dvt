/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author FerdinandN
 */
public class EkunduReinsateEBPPolicyTest extends BaseClass
{
    TestEntity testData;
    TestResult testResult;
    
    public EkunduReinsateEBPPolicyTest(TestEntity testData)
    {
        this.testData = testData;

    }
    
     public TestResult executeTest()
    {  
         SeleniumDriverInstance.DriverExceptionDetail = "";
         this.setStartTime();
         
         if(!navigateToFindClientPage())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to navigate to the find client page", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to navigate to the find client page - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
    
        if(!findClient())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to find client", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!verifyClientDetailsPageHasLoaded())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to verify that the client details page has loaded", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to verify that the client details page has loaded- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!reinstatePolicy())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to reinstate policy", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to  reinstate policy- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
       if(!quoteBusinessAllRisk())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to quote Business all risk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to quote Business all risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!quoteGroupedfireRisk())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to quote grouped fire risk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to quote grouped fire risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!quoteGoodsInTransitRisk())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to quote goods in transit risk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to quote goods in transit risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!quoteMoneyRisk())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to quote the money risk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to quote the money risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
       
        if(!quoteElectronicEquipmentRisk())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to quote electronic equipment risk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to quote electronic equipment risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
         if(!quoteGroupedfireRisk2())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to quote grouped fire risk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to quote grouped fire risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
          if(!quoteGroupedfireRisk3())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to quote grouped fire risk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to quote grouped fire risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
         
         SeleniumDriverInstance.takeScreenShot( "Policy Reinstatement completed successfully", false);
         return new TestResult(testData, Enums.ResultStatus.PASS, "Policy Reinstatement completed successfully", this.getTotalExecutionTime());
     
    }
    
     private boolean navigateToFindClientPage()
    {       
        if(!SeleniumDriverInstance.navigateToFindClientPage(EkunduViewClientDetailsPage.ClientHoverTabId(),"Find Client"))
        {
            return false;
        }
            return true;       
    }
    
    private boolean findClient()
    {       
          String clientCode = SeleniumDriverInstance.retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"), "Retrieved Client Code");
        
        if(clientCode.equals("parameter not found"))
        {
            clientCode = testData.TestParameters.get("Client Code");
        }
        else
            testData.updateParameter("Client Code", clientCode);
            
        if(!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Client Found", false);
        
        if(!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean verifyClientDetailsPageHasLoaded() 
    {
        if(!SeleniumDriverInstance.validateElementTextValueById(EkunduViewClientDetailsPage.ViewClientDetailsLabelId(), "View Client Details"))
        {
            return false;
        }
            SeleniumDriverInstance.takeScreenShot( "Client Details page successfully loaded", false);
        return true;
    }
 
    private boolean reinstatePolicy()
    {
        if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ViewAllPoliciesCheckboxId(), true))
        {
            return false;
        }
        
        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                                  "Retrieved Policy Number");

        if (policyNumber.equals("parameter not found"))
          {
            policyNumber = testData.TestParameters.get("Policy Number");
          }
        else
          {
            testData.updateParameter("Policy Number", policyNumber);
          }
         
         if(!SeleniumDriverInstance.changePolicy(policyNumber, EkunduViewClientDetailsPage.ReinstatePolicyLinkText()))
        {
            return false;
        }
        
        SeleniumDriverInstance.pause(3000);
         
//         if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinstatePolicyLinkText()))
//        {
//            return false;
//        }
         
         SeleniumDriverInstance.acceptAlertDialog();
        
         if(!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId()))
        {
            return false;
        }
         return true;
    }
    
    private boolean quotePolicyRisks()
    {
         if(!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EditBusinessAllRiskLinkId()))
        {
            return false;
        }
         
          if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
          
           SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
  
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.takeScreenShot("Business All Risk Quoted successfully", false);
        
          if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.editGroupedFireRiskLinkId()))
        {
            return false;
        }
         
          if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
          
          
  
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.takeScreenShot("Grouped Fire Risk Quoted successfully", false);
          
          if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.editElectronicEquipmentLinkId()))
        {
            return false;
        }
         
          if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.takeScreenShot("Electronic Equipment Risk Quoted successfully", false);
          
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SelectRoadSideAssistQuoteEditLinkId()))
        {
            return false;
        }
   
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
        
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
  
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId()))
        {
            return false;
        }
          
          SeleniumDriverInstance.takeScreenShot("Policy Risks quoted successfully", false);
          
          return true;
   
    }
    
   private boolean quoteGroupedfireRisk()
    {
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.GroupedFireRiskActionDropdownListXpath(), "Edit"))
        {
            return false;
        }
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
   
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
         {
             return false;
         }

//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Grouped fire risk quoted successfully", false);
        
        return true;
    }
    
    private boolean quoteGroupedfireRisk2()
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.GroupedFireRisk5DeleteActionDropdownListXpath(), "Edit"))
        {
            return false;
        }
        
          
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
//   
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
//
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Grouped fire risk quoted successfully", false);
        
        return true;
    }
    
     private boolean quoteGroupedfireRisk3()
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.GroupedFireRiskEditActionDropdownListXpath(), "Edit"))
        {
        return false;
        }
        
           
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
   
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
         {
             return false;
         }
//
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Grouped fire risk quoted successfully", false);
        
        return true;
    }
    private boolean quoteGoodsInTransitRisk()
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.GoodsinTransitActionDropdownListXpath(), "Edit"))
        {
            return false;
        }
        
   
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
         {
             return false;
         }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Goods in transit risk quoted successfully", false);
        
        return true;
    }
    
    private boolean quoteMoneyRisk()
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.MoneyRiskActionDropdownListXpath(), "Edit"))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
   
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }

        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Money risk quoted successfully", false);
        
        return true;
    }
    
    private boolean quoteBusinessAllRisk()
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.BusinessAllRiskActionDropdownListXpath(), "Edit"))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
   
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
         {
             return false;
         }

//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Business all risk quoted successfully", false);
        
        return true;
    }
    
    private boolean quoteElectronicEquipmentRisk()
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.ElectronicEquipmentRiskActionDropdownListXpath(), "Edit"))
        {
            return false;
        }
       
       
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
   
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
         {
             return false;
         }
//
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//         {
//             return false;
//         }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Electronic equipment risk quoted successfully", false);
        
        return true;
    }
    
    
   
    
}
