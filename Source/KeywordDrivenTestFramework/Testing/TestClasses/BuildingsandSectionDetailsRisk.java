/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class BuildingsandSectionDetailsRisk extends BaseClass 
{

    TestEntity testData;
    TestResult testResultt;

    public BuildingsandSectionDetailsRisk(TestEntity testData)
      {
        this.testData = testData;
      }
    
    public TestResult executeTest()
      {
        this.setStartTime();

        if (!verifyRisksDialogisPresent())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the risks dialod is presenet"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to select risk type - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }
        
        if (!enterRiskDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "enter risk details - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }
        
        return new TestResult(testData, Enums.ResultStatus.PASS, "Building and Contents Section details risk added successfully",
                              this.getTotalExecutionTime());
        
}
    
     private boolean verifyRisksDialogisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            selectBuildingverifyRisksDialogisPresentsandContentSectionDetailsRiskType();

            return true;

          }
        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectBuildingverifyRisksDialogisPresentsandContentSectionDetailsRiskType();

            return true;
          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

      }
    
    
     private boolean selectBuildingverifyRisksDialogisPresentsandContentSectionDetailsRiskType()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(testData.TestParameters.get("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Buildings and Content Section Details Risk type selected successfully", false);
        
        return true;
      }
     
     private boolean enterRiskDetails()
     {
         
         if (!SeleniumDriverInstance.checkBoxSelectionById("ctl00_cntMainBody_SD__HOME_ASSISTANCE_APPL",
                true))
          {
            return false;
          }
         
         if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }
         
         if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_SD__HOME_ASSIST_ADJ_BASIS",
                "Flat Premium"))
          {
            return false;
          }
         
         if (!SeleniumDriverInstance.clearTextAndEnterValueById("ctl00_cntMainBody_SD__HOME_ASSIST_FLAT_PREMIUM_AMOUNT",
                "250"))
          {
            return false;
          }
         
         if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_SD__HOME_ASSIST_ADJ_REASON",
                "Match existing rate"))
          {
            return false;
          }
         
         if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }
         
         if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }
         
         if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_SD__PL_SUM_INSURED",
                "R20,000,000.00"))
          {
            return false;
          }
         
         SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId());
         
         SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId());
         
         SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
         
         
         if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_ReInsurance2007Cntrl_ddlReinsurance",
                "Home Assist"))
          {
            return false;
          }
         
         SeleniumDriverInstance.pause(3000);
         
         
         SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
         
         SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
         
         SeleniumDriverInstance.pause(3000);
         
        SeleniumDriverInstance.clickElementbyLinkText("Documents");
        
         SeleniumDriverInstance.clickElementById("btnGeneratePdf");
         
         SeleniumDriverInstance.clickElementById("popup_ok");
         
         SeleniumDriverInstance.pause(3000);
         
         SeleniumDriverInstance.clickElementbyLinkText("Risks");
         
         return true;
         
         
     }
     
     
    
}
