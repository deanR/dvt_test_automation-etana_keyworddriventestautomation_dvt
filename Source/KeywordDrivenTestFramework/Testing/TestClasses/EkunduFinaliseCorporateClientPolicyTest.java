
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduFinaliseCorporateClientPolicyTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduFinaliseCorporateClientPolicyTest(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToFindClientPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find client page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the find client page  - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());

          }

        if (!findClientAndEditQuote())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to find client and edit quote", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find client and edit quote  - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());

          }

        if (!performRoadSideAssisQuotation())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to perform Road Side Assistance quotation", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to perform Road Side Assistance quotation - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());

          }

        if (!makePolicyLiveandRetrievePolicyReference())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to make the policy live", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to make the policy live- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());

          }

        SeleniumDriverInstance.takeScreenShot("Client policy finalised successfully", true);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Client policy finalised successfully", this.getTotalExecutionTime());

      }

    public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean navigateToFindClientPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),
                EKunduHomePage.FindClientTabXPath()))
          {
            return false;
          }

        return true;
      }

    private boolean findClientAndEditQuote()
      {
        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(),
                this.getData("Client Code")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.waitForElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsQuotesTabPartialLinkText()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ViewClientDetailsEditQuoteLinkId()))
          {
            return false;
          }

        return true;
      }

    private boolean performRoadSideAssisQuotation()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SelectRoadSideAssistQuoteEditLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Road Side Assist quoted successfully", false);

        return true;
      }

    private boolean makePolicyLiveandRetrievePolicyReference()
      {
        SeleniumDriverInstance.scrollDownByOnePage("ctl00_cntMainBody_SummaryCoverCntrl_ctl00_AgentCommissionCntrl_lblAgentDisplay");

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.MakeLiveButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client policy finalised", false);

        SeleniumDriverInstance.pause(10000);

        String retrievedPRef =
            SeleniumDriverInstance.retrieveTextById(EkunduViewClientDetailsPage.PolicyReferenceNumberLabelId());

        this.testData.addParameter("Retrieved Policy Number", retrievedPRef);

        System.out.println("[Info] policy number retrieved  - Ref. - " + retrievedPRef);

        SeleniumDriverInstance.takeScreenShot("Policy reference number retrieved successfully - " + retrievedPRef,
                false);

        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
