
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author deanR
 */
public class EkunduSASRIAPolicyRenewalViaFindClientScenrio4_4Test extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduSASRIAPolicyRenewalViaFindClientScenrio4_4Test(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToFindClientPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find client page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the find client page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findClient())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to  find client", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!startRenewalSelection())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to  start renewal selection", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to start renewal selection- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!makePolicyLive())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to make policy live", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to  make policy live- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Policy Renewal Selection completed succesfully- ",
                              this.getTotalExecutionTime());
      }

    private boolean navigateToFindClientPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),
                EKunduHomePage.FindClientTabXPath()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(20000);

        return true;
      }

    private boolean findClient()
      {
        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                                  "Policy Reference Number");

        if (policyNumber.equals("parameter not found"))
          {
            policyNumber = testData.TestParameters.get("PolicyRef");
          }
        else
          {
            testData.updateParameter("Policy Reference Number", policyNumber);
          }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.PolicyNumberTextBoxId(), policyNumber))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
          {
            return false;
          }

        return true;
      }

    private boolean startRenewalSelection()

      {
        if (!SeleniumDriverInstance.clickElementbyLinkText("Policies"))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementbyLinkText("Details"))
          {
            return false;
          }

        return true;
      }

    private boolean makePolicyLive()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalLinesEditLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.ReinsuranceBandDropDownListId(),
                this.getData("Reinsurance Band")))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.scrollDownByOnePage("ctl00_cntMainBody_SummaryCoverCntrl_ctl00_AgentCommissionCntrl_lblAgentDisplay");

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MakePolicyLiveButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Policy made live", false);

        return true;
      }
    
     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

  }


//~ Formatted by Jindent --- http://www.jindent.com
