
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduGoodsInTransitRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorFleetRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;


import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author FerdinandN
 */
public class EkunduEBPGoodsInTransit extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduEBPGoodsInTransit(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk button is present", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the add risk button is present - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterIndustryDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Industry details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Industry details - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterRiskItemDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Risk Item details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Risk Item details - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterFACXOLPlacementData())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter FAC XOL Placement Data", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter FAC XOL Placement Data - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot("Goods In Transit risk added successfully", false);

        return new TestResult(testData, Enums.ResultStatus.PASS,
                              "Goods In Transit risk added successfully"
                              + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());

      }

    private boolean verifyAddRiskButtonisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return selectGoodsInTransitRisk();
          }
        else if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            System.out.println("Add risk button clicked.");

            return selectGoodsInTransitRisk();
          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

      }

    private boolean selectGoodsInTransitRisk()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(10000);
        SeleniumDriverInstance.takeScreenShot("Goods In Transit risk selected sucessfully", false);

        return true;
      }

 public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
 
    private boolean enterIndustryDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.MotorFleetNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(),
                this.getData("Risk Item Industry")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.IndustrySearchRowId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Industry details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.MotorFleetNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterRiskItemDetails()
      {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RiskItemFlatPremiumCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduGoodsInTransitRiskPage.RiskItemRoadCheckboxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.RiskItemSumInsuredTextboxId(),
                this.getData("Risk Item - Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.RiskItemLoadLimitTextboxId(),
                this.getData("Risk Item - Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.RiskItemFinalPremiumTextboxId(),
                this.getData("Risk Item - Final Premium")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk Item details entered successfully", false);
        SeleniumDriverInstance.scrollDownByOnePage(EkunduGoodsInTransitRiskPage.RiskItemRoadCheckboxId());

        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.GoodsInTransitNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterFACXOLPlacementData()
      {
        SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.AddFACXOLButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(),
                this.getData("Reinsurer Code")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
          {
            return false;
          }

        // SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLLowerLimitId());

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLLowerLimitId(),
                this.getData("FAP LOX Lower Limit")))
          {
            return false;
          }
        
        

        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");

        // SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId());

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId(),
                this.getData("FAP LOX Upper Limit")))
          {
            return false;
          }
        
        System.out.println("Company 1 Upper limit entered successfully");

        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");

        // SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId());

        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId(),
                this.getData("FAP LOX Participation Percentage")))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");

        // SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId());

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId(),
                this.getData("FAP LOX Premium")))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        SeleniumDriverInstance.pause(5000);

        String participationPerc =
            SeleniumDriverInstance.retrieveTextById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId());

        System.out.println("Participation percentage retrieved " + participationPerc);

        do
          {
            if (participationPerc.equals("0.0000"))
              {
                SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId(),
                        this.getData("FAP LOX Participation Percentage"));
                SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
                SeleniumDriverInstance.pause(5000);

              }
          }

        while (participationPerc.equals("0.0000"));

        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.FACXOLOkButtonId()))
          {
            return false;
          }
        
        SeleniumDriverInstance.pause(5000);
        
        SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        
        SeleniumDriverInstance.pause(2000);
        
        SeleniumDriverInstance.clickElementbyPartialLinkText("Reinsurances");


        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.AddFACXOLButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(),
                this.getData("Reinsurer Code 2")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLLowerLimitId(),
                this.getData("FAP LOX Lower Limit 2")))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId(),
                this.getData("FAP LOX Upper Limit 2")))
          {
            return false;
          }
        
        System.out.println("Company 2 Upper limit entered successfully");
        
        SeleniumDriverInstance.pause(3000);

        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId(),
                this.getData("FAP LOX Participation Percentage 2")))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId(),
                this.getData("FAP LOX Premium 2")))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLCommisionTextboxId(),
                this.getData("FAP LOX Commision")))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");

        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId());

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(),
                this.getData("Reinsurer Code 3")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLCaisseParticipationPercTextboxId(),
                this.getData("FAP LOX Participation Percentage 2")))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLCiassePremiumTextboxId(),
                this.getData("FAP LOX Premium 2")))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLCaisseCommisionTextboxId(),
                this.getData("FAP LOX Commision")))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");

        String participationPerc2 =
            SeleniumDriverInstance.retrieveTextById(EkunduGoodsInTransitRiskPage.FACXOLCaisseParticipationPercTextboxId());

        System.out.println("Participation percentage retrieved " + participationPerc2);

        do
          {
            if (participationPerc.equals("0.0000"))
              {
                SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId(),
                        this.getData("FAP LOX Participation Percentage"));
                SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
                SeleniumDriverInstance.pause(5000);

              }
            else if (participationPerc2.equals("0.0000"))
              {
                SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLCaisseParticipationPercTextboxId(),
                        this.getData("FAP LOX Participation Percentage 2"));
                SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
                SeleniumDriverInstance.pause(5000);

              }
          }

        while (participationPerc.equals("0.0000") || participationPerc2.equals("0.0000"));

        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.FACXOLOkButtonId()))
          {
            return false;
          }
        
        SeleniumDriverInstance.pause(5000);
        
        SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        
        SeleniumDriverInstance.clickElementbyPartialLinkText("Reinsurances");

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId()))
          {
            return false;
          }

        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
