
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduFidelityRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduFidelityRiskTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduFidelityRiskTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the add risk button is present ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the add risk button is present - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterRiskItems())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk items", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk items - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterExtensions())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Extensions", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Extensions - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterNotes())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Notes", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Notes - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot("Fidelity risk entered successfully", false);

        return new TestResult(testData, Enums.ResultStatus.PASS,
                              "Fidelity risk entered successfully" + SeleniumDriverInstance.DriverExceptionDetail,
                              this.getTotalExecutionTime());

      }

     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean verifyAddRiskButtonisPresent()
      {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectFidelityRisk();

            return true;
          }
        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectFidelityRisk();
          }
        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");

            return false;
          }

        return true;
      }

    private boolean selectFidelityRisk()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);
        SeleniumDriverInstance.takeScreenShot("Fidelity risk selected sucessfully", false);

        return true;
      }

    private boolean enterRiskItems()
      {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduFidelityRiskPage.RiskItemBasisofCoverDropdownListId(),
                this.getData("Risk Items Basis of Cover")))
          {
            return false;
          }

        // JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;

        // js.executeScript("onchange=\"ToggleBasis(Blanket Basis);");

        if (!SeleniumDriverInstance.clickElementById(EkunduFidelityRiskPage.FidelityRiskRateButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.BlanketBasisNumberofEmployeesTextboxId(),
                this.getData("Blanket Basis Number of Employees")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.BlanketBasisSumInsuredTextboxId(),
                this.getData("Blanket Basis Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduFidelityRiskPage.RiskItemsProposalObtainedCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduFidelityRiskPage.RiskItemsFAPCheckboxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.RiskItemsFAPMinimumPercentageTextboxId(),
                this.getData("FAP Min Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.RiskItemsFAPMinimumAmountTextboxId(),
                this.getData("FAP Min Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.RiskItemsFAPMaximumAmountTextboxId(),
                this.getData("FAP Max Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduFidelityRiskPage.RiskItemsAdditionalFAPCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.RiskItemsAdditionalFAPMinimumPercentageTextboxId(),
                this.getData("FAP Min Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.RiskItemsAdditionalFAPMinimumAmountTextboxId(),
                this.getData("FAP Min Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.RiskItemsAdditionalFAPMaximumAmountTextboxId(),
                this.getData("FAP Max Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduFidelityRiskPage.FidelityRiskRateButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk Item details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduFidelityRiskPage.FidelityRiskNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterExtensions()
      {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduFidelityRiskPage.ExtensionsAdditionalClaimsPrepCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsAdditionalClaimsPrepLimitofIndemnityTextboxId(),
                this.getData("Extensions Limit of Indemnity")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsAdditionalClaimsPrepRateTextboxId(),
                this.getData("Extensions Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduFidelityRiskPage.ExtensionsComputerExtensionLoadCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsComputerExtensionLoadRateTextboxId(),
                this.getData("Extensions Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsComputerExtensionLoadFAPPercentageTextboxId(),
                this.getData("Extensions FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsComputerExtensionLoadFAPAmountTextboxId(),
                this.getData("Extensions FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduFidelityRiskPage.ExtensionsLossDiscoverGreaterThan24MonthsCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsLossDiscoverGreaterThan24MonthsRateTextboxId(),
                this.getData("Extensions Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsLossDiscoverGreaterThan24MonthsFAPPercentageTextboxId(),
                this.getData("Extensions FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsLossDiscoverGreaterThan24MonthsFAPAmountTextboxId(),
                this.getData("Extensions FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduFidelityRiskPage.ExtensionsLossDiscover24To36MonthsCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsLossDiscover24To36MonthsRateTextboxId(),
                this.getData("Loss Discover 24-36 Months Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsLossDiscover24To36MonthsFAPPercentageTextboxId(),
                this.getData("Extensions FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsLossDiscover24To36MonthsFAPAmountTextboxId(),
                this.getData("Extensions FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduFidelityRiskPage.ExtensionsRecoveryCheckboxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsRecoveryRateTextboxId(),
                this.getData("Extensions Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsRecoveryFAPPercentageTextxtboxId(),
                this.getData("Extensions FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsRecoveryFAPAmountTextboxId(),
                this.getData("Extensions FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduFidelityRiskPage.ExtensionsReductionorReinstatementCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsReductionorReinstatementRateTextboxId(),
                this.getData("Extensions Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsReductionorReinstatementFAPPercentageTextxtboxId(),
                this.getData("Extensions FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsReductionorReinstatementCheckboxId(),
                this.getData("Extensions FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduFidelityRiskPage.ExtensionsRetroactiveCoverCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsRetroactiveCoverRateTextboxId(),
                this.getData("Extensions Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsRetroactiveCoverFAPPercentageTextxtboxId(),
                this.getData("Extensions FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.ExtensionsRetroactiveCoverFAPAmountTextboxId(),
                this.getData("Extensions FAP Amount")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Extensions entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduFidelityRiskPage.FidelityRiskNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterNotes()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduFidelityRiskPage.FidelityRiskNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduFidelityRiskPage.FidelityRiskNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.RiskCommentsTextboxId(),
                this.getData("Risk Notes")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduFidelityRiskPage.AddNotesLabelId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduFidelityRiskPage.RiskNotesTextboxId(),
                this.getData("Risk Notes")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduFidelityRiskPage.FidelityRiskNextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(1000);
        
        SeleniumDriverInstance.clickElementbyLinkText("Documents");
        
        SeleniumDriverInstance.pause(1000);
        
         SeleniumDriverInstance.clickElementById("btnGeneratePdf");
         
//         SeleniumDriverInstance.pause(3000);
//         
         SeleniumDriverInstance.clickElementById("popup_ok");
//         
         
         SeleniumDriverInstance.pause(1000);
         
         SeleniumDriverInstance.clickElementbyLinkText("Risks");
        
        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
