
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduElectronicEquipmentRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author ferdinandN
 */
public class EkunduDirectorsAndOfficesRiskTest extends BaseClass {

    TestEntity testData;
    TestResult testResultt;

    public EkunduDirectorsAndOfficesRiskTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyRisksDialogisPresent()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify that the Risk Dialog is present - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterRiskUnderwritingCriteria()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Risk Underwriting Criteria", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Risk Underwriting Criteria - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterNotes()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Notes", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Notes - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }
        
        if (!verifyPremium()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify premium", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify premium  - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        SeleniumDriverInstance.takeScreenShot("Directors And Offices Risk details entered successfully", false);

        return new TestResult(testData, Enums.ResultStatus.PASS,
                "Directors And Offices Risk details entered successfully- "
                + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());

    }

    private boolean verifyRisksDialogisPresent() {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectDirectorsandOfficersLiabilityRiskType();

            return true;
        } else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            selectDirectorsandOfficersLiabilityRiskType();

            return true;
        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

        }

    }

    private boolean selectDirectorsandOfficersLiabilityRiskType() {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.takeScreenShot("Directors and offices Risk type selected successfully", false);

        return true;
    }

    private boolean enterRiskUnderwritingCriteria() {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.DORiskNoChangesCheckBoxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.DORiskIsSolventCheckBoxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.DORiskOpInSaCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.DORiskNotListedCheckBoxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.DORiskCompanyTypeCheckBoxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.DORiskClaimsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.DORiskDirectorCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.uncheckCheckBoxSelectionById(EkunduViewClientDetailsPage.RetroActiveDateCheckBoxId(), true)) {
            return false;
        }
        
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RetroActiveDateCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.DORatingCriteriaGrossAssetsDropDownListId(),
                this.getData("Gross Assets"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.DORatingCriteriaLimitofIndemnityDropDownListId(),
                this.getData("Limit of indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.DirectorsandOfficesRiskRateButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Risk underwriting criteria details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;

    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean enterNotes() {
        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskCommentsTextboxId(),
                this.getData("Directors and Offices Risk Item Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskPolicyNotesTextboxId(),
                this.getData("Directors and Offices Risk Policy Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Comments entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        return true;
    }
    
    private boolean verifyPremium() {
        
        if (!SeleniumDriverInstance.verifyPremiumDetails()) {
            return false;
        }
        
        

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Documents");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementById("btnGeneratePdf");
//
//        SeleniumDriverInstance.pause(3000);
//
        SeleniumDriverInstance.clickElementById("popup_ok");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Risks");

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
