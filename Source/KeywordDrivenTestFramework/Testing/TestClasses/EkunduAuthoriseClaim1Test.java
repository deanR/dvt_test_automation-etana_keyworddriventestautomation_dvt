
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.CreateNewClaimPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMaintainCorporateClientClaim1Page;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduAuthoriseClaim1Test extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduAuthoriseClaim1Test(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!navigateToAuthoriseClaimPage())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to navigate to the find claim page"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the find claim page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findClaim())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to find claim"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find claim- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!AuthoriseClaim())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to authorise claim payment"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to authorise claim payment- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Corporate client claim authorised successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Corporate client claim authorised successfully",
                              this.getTotalExecutionTime());

      }

    private boolean navigateToAuthoriseClaimPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EkunduMaintainCorporateClientClaim1Page.ClaimHoverTabId(),
                EkunduMaintainCorporateClientClaim1Page.AuthoriseClaimLinkXpath()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.waitForElementById(EkunduMaintainCorporateClientClaim1Page.ClaimReferenceTextBoxId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Navigation to the Find Claim page was successful", false);

        return true;
      }

    private boolean findClaim()
      {
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.OkButtonId()))
          {
            return false;
          }

        String claimref = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                              "Retrieved Corporate Claim Number");

        if (claimref.equals("parameter not found"))
          {
            claimref = testData.TestParameters.get("Claim Reference Number");
          }
        else
          {
            testData.updateParameter("Claim Reference Number", claimref);
          }

        SeleniumDriverInstance.clearTextById(EkunduMaintainCorporateClientClaim1Page.ClaimReferenceTextBoxId());

        if (!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.ClaimReferenceTextBoxId(),
                claimref))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimFindNowButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

//        if (!SeleniumDriverInstance.waitForElementById(EkunduMaintainCorporateClientClaim1Page.AuthoriseClaimLinkId()))
//          {
//            return false;
//          }

        SeleniumDriverInstance.takeScreenShot("Claim found", false);

        return true;

      }

    private boolean AuthoriseClaim()
      {
//        if (!SeleniumDriverInstance.clickElementbyLinkText("Authorise"))   
//          {
//            return false;
//          }
          
          if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_grdvAuthoriseclaimpayments_ctl02_lnkAuthorise"))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

         if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimFinishButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.AuthoriseClaimButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.AuthoriseClaimOkButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Claim authorised successfully", false);

        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
