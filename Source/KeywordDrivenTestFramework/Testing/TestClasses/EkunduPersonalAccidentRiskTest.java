
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduPersonalAccidentRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduPersonalAccidentRiskTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduPersonalAccidentRiskTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the add risk button is present ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the add risk button is present - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterIndustryDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter industry details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter industry details - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterCoverDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter cover details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter cover details - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterExtensions())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter extensions", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter extensions - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterNotes())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter notes", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter notes - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot("Personal Accident risk entered successfully", false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Personal Accident risk entered successfully",
                              this.getTotalExecutionTime());

      }

     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean verifyAddRiskButtonisPresent()
      {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectPersonalAccidentRisk();

            return true;
          }
        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectPersonalAccidentRisk();
          }
        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err
                .println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");

            return false;
          }

        return true;
      }

    private boolean selectPersonalAccidentRisk()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(10000);
        SeleniumDriverInstance.takeScreenShot("Personal Accident risk selected sucessfully", false);

        return true;
      }

    private boolean enterIndustryDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduPersonalAccidentRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.RiskItemsReinsuranceLimitofLiabilityTextboxId(),
                this.getData("Risk Items - Reinsurance Limit of Liability")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementbyXpath(EkunduPersonalAccidentRiskPage.RiskItemsAddButtonXpath()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(),
                this.getData("Employee Details - Industry")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(
                EKunduCreateCorporateClientPage.IndustrySearchResultsSelectedRowId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Industry details entered successfully", false);

        return true;
      }

    private boolean enterCoverDetails()
      {
        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.EmployeeDetailsNumberofEmployeesTextboxId(),
                this.getData("Employee Details - Number of Employees")))
          {
            return false;
          }

//        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
//                EkunduPersonalAccidentRiskPage.EmployeeDetailsCoverPeriodDropdownListId(),
//                this.getData("Employee Details - Cover Period")))
//          {
//            return false;
//          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduPersonalAccidentRiskPage.EmployeeDetailsCoverPeriodDropdownListId(),
                this.getData("Employee Details - Cover Period")))
          {
            return false;
          }


        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.CoverDetailsDeathCompensationTextboxId(),
                this.getData("Employee Details - Cover Details - Compensation")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.CoverDetailsDeathAgreedRateTextboxId(),
                this.getData("Employee Details - Cover Details - Agreed Rate")))
          {
            return false;
          }

//        if (!SeleniumDriverInstance.checkBoxSelectionById(
//                EkunduPersonalAccidentRiskPage.CoverDetailsPermanentDisabilityCheckboxId(), true))
//          {
//            return false;
//          }
        
         if (!SeleniumDriverInstance.clickElementById(EkunduPersonalAccidentRiskPage.CoverDetailsPermanentDisabilityCheckboxId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.CoverDetailsPermanentDisabilitRateTextboxId(),
                this.getData("Employee Details - Cover Details - Agreed Rate")))
          {
            return false;
          }

//        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduPersonalAccidentRiskPage.CoverDetailsTDDCheckboxId(),
//                true))
//          {
//            return false;
//          }
        
         if (!SeleniumDriverInstance.clickElementById(EkunduPersonalAccidentRiskPage.CoverDetailsTDDCheckboxId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.CoverDetailsTDDCompensationTextboxId(),
                this.getData("Employee Details - Cover Details - Compensation")))
          {
            return false;
          }

//        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
//                EkunduPersonalAccidentRiskPage.CoverDetailsTDDNumberofWeeksDropdownListId(),
//                this.getData("Cover Details - TDD Number of Weeks")))
//          {
//            return false;
//          }
        if (!SeleniumDriverInstance.enterTextById(EkunduPersonalAccidentRiskPage.CoverDetailsTDDNumberofWeeksDropdownListId(),
                this.getData("Cover Details - TDD Number of Weeks")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduPersonalAccidentRiskPage.CoverDetailsTDDAgreedRateTextboxId(),
                this.getData("Employee Details - Cover Details - Agreed Rate")))
          {
            return false;
          }

//        if (!SeleniumDriverInstance.checkBoxSelectionById(
//                EkunduPersonalAccidentRiskPage.CoverDetailsMedicalExpensesCheckboxId(), true))
//          {
//            return false;
//          }

         if (!SeleniumDriverInstance.clickElementById(EkunduPersonalAccidentRiskPage.CoverDetailsMedicalExpensesCheckboxId()))
          {
            return false;
          }
//        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
//                EkunduPersonalAccidentRiskPage.CoverDetailsMedicalExpensesSumInsuredDropdownListsId(),
//                this.getData("Employee Details - Cover Details - Medical Expenses Sum Insured")))
//          {
//            return false;
//          }
         
         if (!SeleniumDriverInstance.enterTextById(EkunduPersonalAccidentRiskPage.CoverDetailsMedicalExpensesSumInsuredDropdownListsId(),
                this.getData("Employee Details - Cover Details - Medical Expenses Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.CoverDetailsMedicalExpensesRateTextboxId(),
                this.getData("Employee Details - Cover Details - Agreed Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduPersonalAccidentRiskPage.RiskRateButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Cover details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduPersonalAccidentRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterExtensions()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduPersonalAccidentRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduPersonalAccidentRiskPage.ExtensionsBodyTransportationCheckboxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsBodyTransportationRateTextboxId(),
                this.getData("Extensions Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsBodyTransportationPremiumTextboxId(),
                this.getData("Extensions Premium")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsBodyTransportationFAPPercentageTextboxId(),
                this.getData("Extensions - FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsBodyTransportationFAPAmountTextboxId(),
                this.getData("Extensions - FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduPersonalAccidentRiskPage.ExtensionsFuneralCostsCheckboxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduPersonalAccidentRiskPage.ExtensionsFuneralCostsRateTextboxId(),
                this.getData("Extensions Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsFuneralCostsPremiumTextboxId(),
                this.getData("Extensions Premium")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsFuneralCostsFAPPercentageTextboxId(),
                this.getData("Extensions - FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsFuneralCostsFAPAmountTextboxId(),
                this.getData("Extensions - FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduPersonalAccidentRiskPage.ExtensionsRepatriationCostsCheckboxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsRepatriationCostsRateTextboxId(),
                this.getData("Extensions Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsRepatriationCostsPremiumTextboxId(),
                this.getData("Extensions Premium")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsRepatriationCostsFAPPercentageTextboxId(),
                this.getData("Extensions - FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsRepatriationCostsFAPAmountTextboxId(),
                this.getData("Extensions - FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduPersonalAccidentRiskPage.ExtensionsSeriousIllnessCheckboxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsSeriousIllnessRateTextboxId(),
                this.getData("Extensions Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsSeriousIllnessPremiumTextboxId(),
                this.getData("Extensions Premium")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsSeriousIllnessFAPPercentageTextboxId(),
                this.getData("Extensions - FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsSeriousIllnessFAPAmountTextboxId(),
                this.getData("Extensions - FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduPersonalAccidentRiskPage.ExtensionsSicknessCheckboxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsSicknessLimitofIndemnityTextboxId(),
                this.getData("Extensions - Sickness Limit of Indemnity")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduPersonalAccidentRiskPage.ExtensionsSicknessRateTextboxId(),
                this.getData("Extensions Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsSicknessFAPPercentageTextboxId(),
                this.getData("Extensions - FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduPersonalAccidentRiskPage.ExtensionsSicknessFAPAmountTextboxId(),
                this.getData("Extensions - FAP Amount")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Extensions entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduPersonalAccidentRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduPersonalAccidentRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterNotes()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduPersonalAccidentRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduPersonalAccidentRiskPage.RiskCommentsTextboxId(),
                this.getData("Risk Notes")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduPersonalAccidentRiskPage.RiskAddNotesLabelId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduPersonalAccidentRiskPage.RiskPolicyNotesTextboxId(),
                this.getData("Risk Notes")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduPersonalAccidentRiskPage.RiskNextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance
            .clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(1000);
        
        SeleniumDriverInstance.clickElementbyLinkText("Documents");
        
        SeleniumDriverInstance.pause(1000);
        
         SeleniumDriverInstance.clickElementById("btnGeneratePdf");
         
//         SeleniumDriverInstance.pause(3000);
//         
         SeleniumDriverInstance.clickElementById("popup_ok");
//         
         
         SeleniumDriverInstance.pause(1000);
         
         SeleniumDriverInstance.clickElementbyLinkText("Risks");
        
        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
