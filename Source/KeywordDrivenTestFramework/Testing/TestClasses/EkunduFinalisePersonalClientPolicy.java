
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduFinalisePersonalClientPolicy extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public EkunduFinalisePersonalClientPolicy(TestEntity testData) {
        this.testData = testData;

    }

    public TestResult executeTest() {
        this.setStartTime();

        if (!finalisePolicyCreating()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to finalise Personal Client Policy"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to finalise Personal Client Policy- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        SeleniumDriverInstance.takeScreenShot(("Personal client policy finalised successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Personal client policy finalised successfully",
                this.getTotalExecutionTime());
    }

    private boolean finalisePolicyCreating() {
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(),
                "Edit")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalLinesNextButtonId())) {
            return false;
        }

//        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalLinesNextButtonId());
//
//        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalLinesNextButtonId());
//          
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalLinesNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId())) {
            return false;
        }
        
        if (!SeleniumDriverInstance.scrollToElement(EkunduCreateNewPolicyForNewClientPage.MakePolicyLiveButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MakePolicyLiveButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.PolicyCreationConfirmationSpanId())) {
            return false;
        }

        String PolicyRef
                = SeleniumDriverInstance.retrieveTextById(EkunduCreateNewPolicyForNewClientPage.PolicyRefLabelId());

        testData.addParameter("Policy Reference Number", PolicyRef);

        if (PolicyRef.isEmpty()) {
            System.err.println("Failed to retrieve policy number");
        } else {
            System.out.println("Policy number retrieved - " + PolicyRef);
        }

        SeleniumDriverInstance.takeScreenShot("Policy created successfully", false);

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
