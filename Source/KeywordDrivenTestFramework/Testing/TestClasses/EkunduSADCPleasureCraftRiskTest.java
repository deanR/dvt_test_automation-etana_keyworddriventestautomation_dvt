
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import org.openqa.selenium.Keys;


/**
 *
 * @author FerdinandN
 */
public class EkunduSADCPleasureCraftRiskTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduSADCPleasureCraftRiskTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!verifyRisksDialogisPresent())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the risks dialog is present"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the risks dialog is present- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterVesselandHullDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter Vessel and Hull details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Vessel and Hull details- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterNotes())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk notes"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk notes- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Water and Pleasure Craft risk added successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Water and Pleasure Craft risk added successfully",
                              this.getTotalExecutionTime());

      }

    private boolean verifyRisksDialogisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            selectPleasureCraftRiskType();

            return true;
          }

        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectPleasureCraftRiskType();

            return true;

          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

      }

    private boolean selectPleasureCraftRiskType()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(testData.TestParameters.get("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Water and Pleasure craft risk type selected successfully", false);

        return true;

      }

    private boolean enterVesselandHullDetails()
      {
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MaterialofHullSADCTextBoxId(),
                testData.TestParameters.get("Material of Hull")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.LengthofHullSADCTextBoxId(),
                testData.TestParameters.get("Length of Hull")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.HullYearofManufactureSADCTextBoxId(),
                testData.TestParameters.get("Hull Year of Manufacture")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SerialNumberTextBoxId(),
                testData.TestParameters.get("Serial Number")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.HullSumInsuredSADCTextBoxId(),
                testData.TestParameters.get("Hull Sum Insured")))
          {
            return false;
          }


        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.HullNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.NameofVesselSADCTextBoxId(),
                testData.TestParameters.get("Name of Vessel")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MakeandModelSADCTextBoxId(),
                testData.TestParameters.get("Make and Model")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TypeofVesselSADCDropdownListId(),
                testData.TestParameters.get("Type of Vessel")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorBoatSpeedSADCDropdownListId(),
                testData.TestParameters.get("Motor Boat Max Speed")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VesselHorsePowerTestboxId(),
                testData.TestParameters.get("Horse Power")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NumberofEnginesSADCDropdownListId(),
                testData.TestParameters.get("No of Engines")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.EngineTypeSADCDropdownListId(),
                testData.TestParameters.get("Engine Type")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine1YearTextBoxId(),
                testData.TestParameters.get("Engine 1 Year")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine1MakeTextBoxId(),
                testData.TestParameters.get("Engine 1 Make")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine1SerialNumberTextBoxId(),
                testData.TestParameters.get("Engine 1 Serial Number")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine1SumInsuredSADCTextBoxId(),
                testData.TestParameters.get("Engine 1 Sum Insured")))
          {
            return false;
          }
        
         if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine2YearTextBoxId(),
                testData.TestParameters.get("Engine 2 Year")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine2MakeTextBoxId(),
                testData.TestParameters.get("Engine 2 Make")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine2SerialNumberTextBoxId(),
                testData.TestParameters.get("Engine 2 Serial Number")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine2SumInsuredSADCTextBoxId(),
                testData.TestParameters.get("Engine 2 Sum Insured")))
          {
            return false;
          }



        SeleniumDriverInstance.takeScreenShot("Vessel/Hull details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureNextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterNotes()
      {
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureNextButtonId());

        SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureCraftCommentsTextAreaId(),
                testData.TestParameters.get("Comments"));

        SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);

        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EnrouteNextButtonId());

        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EnrouteNextButtonId());

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;
      }
    
    private boolean enterEndorsements()
    {
        if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsSelectButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsAddAllButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsApplyButtonId()))
         {
             return false;
         }
         
         
         SeleniumDriverInstance.takeScreenShot("Endorsement details entered successfully", false);
         
         SeleniumDriverInstance.pause(5000);
         
         this.editEndorsementDocument();
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        
        
        return true;
    }
    
    private boolean editEndorsementDocument()
    {
        if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EditEndorsementsLinkId()))
         {
             return false;
         }
        
        SeleniumDriverInstance.switchToFrameById("ctl00_cntMainBody_txtDocumentEditor_ifr");
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EndorsementsBodyId(),
                ("Peronal Lines Pleasure Craft")))
          {
            return false;
          }
        
        SeleniumDriverInstance.switchToDefaultContent();
        
        SeleniumDriverInstance.switchToFrameById("modalDialog");
        
        if(!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnSave"))
         {
             return false;
         }
        
        SeleniumDriverInstance.switchToDefaultContent();
        
        return true;
        
        
    }
  }


//~ Formatted by Jindent --- http://www.jindent.com
