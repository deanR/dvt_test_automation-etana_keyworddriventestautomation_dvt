
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EkunduElectronicEquipmentRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author FerdinandN
 */
public class EBPEditElectronicEquipment extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EBPEditElectronicEquipment(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!enterRiskItemDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk item details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk item details - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Electronic Equipment Risk edited successfully - ",
                              this.getTotalExecutionTime());
      }

//    private String getData(String parameterName)
//      {
//        if (testData.TestParameters.containsKey(parameterName))
//          {
//            return testData.TestParameters.get(parameterName);
//          }
//        else
//          {
//            System.err.println("Parameter - " + parameterName + " was not defined");
//
//            return "";
//          }
//      }
    
    public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean enterRiskItemDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.editElectronicEquipmentLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        this.clickAddRiskItemButton();

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduElectronicEquipmentRiskPage.ItemDetailsCategoryDropdownListId(),
                this.getData("Item Details - Category")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsDescriptionTextboxId(),
                this.getData("Item Details - Description")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSumInsuredTextboxId(),
                this.getData("Item Details - Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduElectronicEquipmentRiskPage.ItemDetailsFlatPremiumCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsFlatPremiumTextboxId(),
                this.getData("Item Details - Flat Premium")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Item Details Entered Sucessfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;
      }

    private boolean clickAddRiskItemButton()
      {
        WebElement cell =
            SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_EE__ITEM\"]/table/tfoot/tr/td[6]/input[@value = \"Add\"]"));

        if (cell != null)
          {
            cell.click();
            System.out.println("[TestInfo] Add Risk Item Button Clicked");

            return true;
          }
        else
          {
            System.err.println("[Error] Failed to click add risk item button");

            return false;
          }
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
