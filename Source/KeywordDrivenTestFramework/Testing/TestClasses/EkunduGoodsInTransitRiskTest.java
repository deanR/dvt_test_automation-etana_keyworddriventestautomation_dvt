
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduGoodsInTransitRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduGoodsInTransitRiskTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduGoodsInTransitRiskTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the add risk button is present ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the add risk button is present - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterRiskItemDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk item details ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk item details - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterExtensions())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter extensions ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter extensions - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterEndorsements())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Endorsements ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Endorsements - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterComments())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Comments ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Comments - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS,
                              "Goods In Transit risk entered successfully"
                              + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());

      }

    private boolean verifyAddRiskButtonisPresent()
      {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectGoodsInTransitRisk();

            return true;
          }
        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectGoodsInTransitRisk();
          }
        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");

            return false;
          }

        return true;
      }

    private boolean selectGoodsInTransitRisk()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(10000);
        SeleniumDriverInstance.takeScreenShot("Goods In Transit risk selected sucessfully", false);

        return true;
      }

     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean enterRiskItemDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.GoodsInTransitNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.RiskItemSearchIndustryButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.RiskItemSearchIndustryTextboxId(),
                this.getData("Risk Item Industry")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.RiskItemSearchIndustryIconId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.IndustrySearchResultsSelectedRowId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduGoodsInTransitRiskPage.RiskItemRoadCheckboxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduGoodsInTransitRiskPage.RiskItemAirCheckboxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduGoodsInTransitRiskPage.RiskItemRailCheckboxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.RiskItemSumInsuredTextboxId(),
                this.getData("Risk Item Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.RiskItemLoadLimitTextboxId(),
                this.getData("Risk Item Load Limit")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.RiskItemAgreedRateTextboxId(),
                this.getData("Risk Item Agreed Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduGoodsInTransitRiskPage.RiskItemFAPCheckboxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.RiskItemFAPMaxAmountTextboxId(),
                this.getData("FAP Maximum Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduGoodsInTransitRiskPage.RiskItemAdditionalFAPCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.RiskItemAdditionalFAPMaxAmountTextboxId(),
                this.getData("Additional FAP Maximum Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduGoodsInTransitRiskPage.RiskItemTheftorHijackingCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.RiskItemTheftorHijackingMaxAmountTextboxId(),
                this.getData("FAP Theft/Hijacking Maximum Amount")))
          {
            return false;
          }

        SeleniumDriverInstance.scrollDownByOnePage(EkunduGoodsInTransitRiskPage.RiskItemSearchIndustryButtonId());

        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.RiskItemRateButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk Item details entered successfully", false);
        SeleniumDriverInstance.scrollDownByOnePage(EkunduGoodsInTransitRiskPage.RiskItemSearchIndustryButtonId());

        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.GoodsInTransitNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterExtensions()
      {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduGoodsInTransitRiskPage.ExtensionsAdditionalClaimCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.ExtensionsAdditionalClaimsLimitofIndemnityTextboxId(),
                this.getData("Extensions Additional Claims Limit of Indemnity")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.ExtensionsAdditionalClaimsRateTextboxId(),
                this.getData("Extensions Additional Claims Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduGoodsInTransitRiskPage.ExtensionsDebrisRemovalCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.ExtensionsDebrisRemovalLimitofIndemnityTextboxId(),
                this.getData("Extensions Debris Removal Limit of Indemnity")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.ExtensionsDebrisRemovalRateTextboxId(),
                this.getData("Extensions Debris Removal Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.ExtensionsDebrisRemovalFAPPercentageTextboxId(),
                this.getData("Extensions Debris Removal FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.ExtensionsDebrisRemovalFAPAmountTextboxId(),
                this.getData("Extensions Debris Removal FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduGoodsInTransitRiskPage.ExtensionsFireExtinguishingChargesCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.ExtensionsFireExtinguishingChargesLimitofIndemnityTextboxId(),
                this.getData("Extensions Fire Extinguishing Charges Limit of Indemnity")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.ExtensionsFireExtinguishingChargesRateTextboxId(),
                this.getData("Extensions Fire Extinguishing Charges Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.ExtensionsFireExtinguishingChargesFAPPercentageTextboxId(),
                this.getData("Extensions Fire Extinguishing Charges FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.ExtensionsFireExtinguishingChargesFAPAmountTextboxId(),
                this.getData("Extensions Fire Extinguishing Charges FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduGoodsInTransitRiskPage.ExtensionsRiotandStrikeCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.ExtensionsRiotandStrikePremiumTextboxId(),
                this.getData("Extensions Riot & Strike Premium")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.ExtensionsRiotandStrikeFAPPercentageTextboxId(),
                this.getData("Extensions Riot & Strike FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.ExtensionsRiotandStrikeFAPAmountTextboxId(),
                this.getData("Extensions Riot & Strike FAP Amount")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Extensions entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.GoodsInTransitNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterEndorsements()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.GoodsInTransitNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.EndorsementsSelectButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.EndorsementsAddAllButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.EndorsementsApplyButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.GoodsInTransitNextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterComments()
      {
        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.NotesTextboxId(),
                this.getData("Goods in Transit Notes")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.AddPolicyNotesLabelId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGoodsInTransitRiskPage.PolicyNotesTextboxId(),
                this.getData("Goods in Transit Notes")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Comments entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.GoodsInTransitNextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(1000);
        
        SeleniumDriverInstance.clickElementbyLinkText("Documents");
        
        SeleniumDriverInstance.pause(1000);
        
         SeleniumDriverInstance.clickElementById("btnGeneratePdf");
        
         
         SeleniumDriverInstance.clickElementById("popup_ok");
         
         
         SeleniumDriverInstance.pause(1000);
         
         SeleniumDriverInstance.clickElementbyLinkText("Risks");
        
        return true;

      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
