
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.CreateNewClaimPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMaintainCorporateClientClaim1Page;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author deanR
 */
public class EkunduThirdPartyRecoveryClientTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduThirdPartyRecoveryClientTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToFindClaimPage())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to navigate to the find claim page"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the find claim page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findClaim())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to find claim"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find claim- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!ClientTPRecovery())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to initiate Client TP Recovery"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to initiate Client TP Recovery- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!InsurerTPRecovery())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to initiate Insurer TP Recovery"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to initiate Insurer TP Recovery- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!ClaimReceivableTPRecovery())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to initiate Claim Receivable TP Recovery"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to initiate Claim Receivable TP Recovery- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Third party recovery against all parties completed successfully",
                              this.getTotalExecutionTime());
      }

    private boolean navigateToFindClaimPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(
                EkunduMaintainCorporateClientClaim1Page.ClaimHoverTabId(),
                EkunduMaintainCorporateClientClaim1Page.ClaimSearchLinkXpath()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.waitForElementById(
                EkunduMaintainCorporateClientClaim1Page.ClaimReferenceTextBoxId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Navigation to the Find Claim page was successful", false);

        return true;
      }

    private boolean findClaim()
      {
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.OkButtonId()))
          {
            return false;
          }

        String claimref = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                              "Retrieved Corporate Claim Number");

        if (claimref.equals("parameter not found"))
          {
            claimref = testData.TestParameters.get("Claim Reference Number");
          }
        else
          {
            testData.updateParameter("Claim Reference Number", claimref);
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.ClaimReferenceTextBoxId(),
                claimref))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimFindNowButtonId()))
          {
            return false;
          }

        this.RetrieveCaseNumber();

        SeleniumDriverInstance.takeScreenShot("Claim found", false);

        return true;

      }

    private boolean ClientTPRecovery()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.TPRecoveryLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilsTPRecoveryLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ClientRadioButtonId()))
          {
            return false;
          }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.EditPartyLinkId()))
          {
            return false;
          }

        // **********************************************************************************************************
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(
                EkunduViewClientDetailsPage.PaymentDetailsRecievedAmountTextboxId(),
                testData.TestParameters.get("Recieved Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PaymentDetailsOkButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        // ***********************************************************************************************************
        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ThisReceiptTabLinkText());

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduViewClientDetailsPage.MediaRefDropDownListId(), testData.TestParameters.get("Media Ref")))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilDetailsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilDetailsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduMaintainCorporateClientClaim1Page.ClaimReasonforChangeDropdowlistId(), this.getData("Reason")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimOkButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(
                EkunduMaintainCorporateClientClaim1Page.ClaimReinsuranceOkButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ReturnToCaseButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Third Party Recovery completed successfully", false);

        return true;
      }

    private boolean InsurerTPRecovery()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RecoveryLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.PerilsTPRecoveryLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.InsurerRadioButtonId()))
          {
            return false;
          }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PartyButtonId()))
          {
            return false;
          }

        // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ReinsurerSearchReinsurerCodeTextBoxId(),
                this.getData("Reinsurer Code")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchPartyCodeButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.SelectPartyLinkText());

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.EditPartyLinkId()))
          {
            return false;
          }

        // **********************************************************************************************************
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(
                EkunduViewClientDetailsPage.PaymentDetailsRecievedAmountTextboxId(),
                testData.TestParameters.get("Insurer Recieved Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PaymentDetailsOkButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        // ***********************************************************************************************************
        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ThisReceiptTabLinkText());

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduViewClientDetailsPage.MediaRefDropDownListId(), testData.TestParameters.get("Media Ref")))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilDetailsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilDetailsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduMaintainCorporateClientClaim1Page.ClaimReasonforChangeDropdowlistId(), this.getData("Reason")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimOkButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(
                EkunduMaintainCorporateClientClaim1Page.ClaimReinsuranceOkButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ReturnToCaseButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Insurer TP Recovery completed successfully", false);

        return true;
      }

    private boolean ClaimReceivableTPRecovery()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RecoveryLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.PerilsTPRecoveryLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ClaimRecievableRadioButtonId()))
          {
            return false;
          }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.EditPartyLinkId()))
          {
            return false;
          }

        // **********************************************************************************************************
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(
                EkunduViewClientDetailsPage.PaymentDetailsRecievedAmountTextboxId(),
                testData.TestParameters.get("Claim Recieved Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PaymentDetailsOkButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        // ***********************************************************************************************************
        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ThisReceiptTabLinkText());

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduViewClientDetailsPage.MediaRefDropDownListId(), testData.TestParameters.get("Media Ref")))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilDetailsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilDetailsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduMaintainCorporateClientClaim1Page.ClaimReasonforChangeDropdowlistId(), this.getData("Reason")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimOkButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(
                EkunduMaintainCorporateClientClaim1Page.ClaimReinsuranceOkButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ReturnToCaseButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Claim Recievable TP Recovery completed successfully", false);

        return true;
      }

    private boolean RetrieveCaseNumber()
      {
        WebElement cell = SeleniumDriverInstance.Driver.findElement(By.xpath(
                              "//div[@id = \"ctl00_cntMainBody_updClaimSearch\"]/div/div/table/tbody/tr/td[1]"));

        String casenumber = cell.getText();

        System.out.println("Case number retrieved - " + casenumber);

        testData.addParameter("Case Number", casenumber);

        return true;
      }
    
    public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

  }


//~ Formatted by Jindent --- http://www.jindent.com
