
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.CreateNewClaimPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author FerdinandN
 */
public class EkunduThirdUserLoginTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduThirdUserLoginTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!loginToApplication())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to log in", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to log in - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!changeBranch())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to change branch", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to change branch- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Second user login completed successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Second user login completed successfully", this.getTotalExecutionTime());

      }

    private boolean loginToApplication()
      {
      if(!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.OkButtonId()))
       {
           return false;
       }

        if (!SeleniumDriverInstance.moveToAndClickElementByLinkText("Logout"))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EKunduHomePage.UsernameTextBoxId(),
                testData.TestParameters.get("User Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EKunduHomePage.PasswordTextBoxId(),
                testData.TestParameters.get("Password")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduHomePage.LoginButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Login was successful", false);

        return true;
      }

    private boolean changeBranch()
      {
        if (!SeleniumDriverInstance.moveToAndClickElementById(EKunduHomePage.ChangeBranchLinkId()))
          {
          }

        if (!SeleniumDriverInstance.switchToFrameById(EKunduHomePage.ChangeBranchFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EKunduHomePage.SelectBranchDropDownListId(),
                testData.TestParameters.get("Branch")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduHomePage.SelectBranchOkButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContentWhenElementNoLongerVisible(EKunduHomePage.ChangeBranchFrameId()))
          {
            return false;
          }

        return true;

      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
