
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.CreateNewClaimPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMaintainCorporateClientClaim1Page;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduMaintainCorporateClientClaim1Test extends BaseClass {

    TestEntity testData;
    TestResult testResult;
    
    public EkunduMaintainCorporateClientClaim1Test(TestEntity testData) {
        this.testData = testData;
    }
    
    public TestResult executeTest() {
        this.setStartTime();
        
        if (!navigateToFindClaimPage()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to navigate to the find claim page"), true);
            
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to navigate to the find claim page- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if (!findClaim()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to find claim"), true);
            
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to find claim- " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }
        
        if (!startClaimMaintenance()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to initiate claim maintenance"), true);
            
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to initiate claim maintenance- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if (!enterIncreasedBuildingsReservesAmounts()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter Increased Buildings Reserves Amounts"), true);
            
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Increased Buildings Reserves Amounts- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if (!enterIncreasedPlantandMachineryReservesAmounts()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter Increased Plant and Machinery Reserves Amounts"),
                    true);
            
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Increased Plant and Machinery Reserves Amounts- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if (!enterIncreasedStockReservesAmounts()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter Increased Stock Reserves Amounts"), true);
            
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Increased Stock Reserves Amounts- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        SeleniumDriverInstance.takeScreenShot(("Corporate client claim mantenance completed successfully"), false);
        
        return new TestResult(testData, Enums.ResultStatus.PASS, "Corporate client claim mantenance completed successfully",
                this.getTotalExecutionTime());
        
    }
    
    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");
            
            return "";
        }
    }
    
    private boolean navigateToFindClaimPage() {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EkunduMaintainCorporateClientClaim1Page.ClaimHoverTabId(),
                EkunduMaintainCorporateClientClaim1Page.ClaimSearchLinkXpath())) {
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        if (!SeleniumDriverInstance.waitForElementById(EkunduMaintainCorporateClientClaim1Page.ClaimReferenceTextBoxId())) {
            return false;
        }
        
        SeleniumDriverInstance.takeScreenShot("Navigation to the Find Claim page was successful", false);
        
        return true;
    }
    
    private boolean findClaim() {
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.OkButtonId())) {
            return false;
        }
        
        String claimref = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                "Retrieved Corporate Claim Number");
        
        if (claimref.equals("parameter not found")) {
            claimref = testData.TestParameters.get("Claim Reference Number");
        } else {
            testData.updateParameter("Claim Reference Number", claimref);
        }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.ClaimReferenceTextBoxId(),
                claimref)) {
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimFindNowButtonId())) {
            return false;
        }
        
        SeleniumDriverInstance.pause(20000);
        
        if (!SeleniumDriverInstance.waitForElementById(EkunduMaintainCorporateClientClaim1Page.EditClaimLinkId())) {
            return false;
        }
        
        SeleniumDriverInstance.takeScreenShot("Claim found", false);
        
        return true;
        
    }
    
    private boolean startClaimMaintenance() {
        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.EditClaimLinkId())) {
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMaintainCorporateClientClaim1Page.ClaimReasonforChangeDropdowlistId(),
                this.getData("Reason for Change"))) {
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimOkButtonId())) {
            return false;
        }
        
        SeleniumDriverInstance.scrollDownByOnePage("Div6");
        
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

//       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//      {
//          return false;
//      }
//
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
//      {
//          return false;
//      }
        return true;
    }
    
    private boolean enterIncreasedBuildingsReservesAmounts() {
        SeleniumDriverInstance.scrollElementIntoViewById(EkunduMaintainCorporateClientClaim1Page.PerilsBuildingsEditLinkId());
        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.PerilsBuildingsEditLinkId())) {
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.ReservesBuildingsOwnDamageNewValueTextBoxId(),
                this.getData("Buildings - Reserves - Own Damage -  New Value (Bigger)"))) {
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.ReservesBuildingsThirdPartyRecoveryNewValueTextBoxId(),
                this.getData("Buildings - Reserves - Third Party Recovery -  New Value (Bigger)"))) {
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.ReservesBuildingsSalvageRecoverNewValueTextBoxId(),
                this.getData("Buildings - Reserves - Salvage Recovery Non-Motor -  New Value (Bigger)"))) {
            return false;
        }
        
        SeleniumDriverInstance.takeScreenShot("Buildings increased reserves amounts entered successfully", false);
        
        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ReservesSubmitButtonId())) {
            return false;
        }
        
        return true;
        
    }
    
    private boolean enterIncreasedPlantandMachineryReservesAmounts() {
        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.PerilsPlantandMachineryEditLinkId())) {
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.ReservesPlantandMachineryOwnDamageNewValueTextBoxId(),
                this.getData("Plant & Machinery - Reserves - Own Damage -  New Value (Bigger)"))) {
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.ReservesPlantandMachineryThirdPartyRecoveryNewValueTextBoxId(),
                this.getData("Plant & Machinery - Reserves - Third Party Recovery -  New Value (Bigger)"))) {
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.ReservesPlantandMachinerySalvageRecoverNewValueTextBoxId(),
                this.getData("Plant & Machinery - Reserves - Salvage Recovery Non-Motor -  New Value (Bigger)"))) {
            return false;
        }
        
        SeleniumDriverInstance.takeScreenShot("Plant and Machinery increased reserves amounts entered successfully",
                false);
        
        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ReservesSubmitButtonId())) {
            return false;
        }
        
        return true;
        
    }
    
    private boolean enterIncreasedStockReservesAmounts() {
        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.PerilsStockEditLinkId())) {
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.ReservesStockOwnDamageNewValueTextBoxId(),
                this.getData("Stock - Reserves - Own Damage -  New Value (Bigger)"))) {
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.ReservesStockThirdPartyRecoveryNewValueTextBoxId(),
                this.getData("Stock - Reserves - Third Party Recovery -  New Value (Bigger)"))) {
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.ReservesStockSalvageRecoverNewValueTextBoxId(),
                this.getData("Stock - Reserves - Salvage Recovery Non-Motor -  New Value (Bigger)"))) {
            return false;
        }
        
        SeleniumDriverInstance.takeScreenShot("Stock increased reserves amounts entered successfully", false);
        
        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimFinishButtonId())) {
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        SeleniumDriverInstance.scrollDownByOnePage("ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl20_lnkReserves");
        
        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimFinishButtonId())) {
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimSubmitButtonId())) {
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ReinsuranceOkButtonId())) {
            return false;
        }
        
        SeleniumDriverInstance.takeScreenShot("Claim maintenance completed successfully", false);
        
        return true;
        
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
