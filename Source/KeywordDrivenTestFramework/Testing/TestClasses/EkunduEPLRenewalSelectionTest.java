
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author ferdinandN
 */
public class EkunduEPLRenewalSelectionTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduEPLRenewalSelectionTest(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToClientDetailsPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find client page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the find client page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findClientAndExtractPolicyNumber())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to find client", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!navigateToPolicyRenewalPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the Renewal Selection page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the Renewal Selection page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findPolicy())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to find policy", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find policy- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!navigateToClientDetailsPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find client page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the find client page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterPolicyRenewalData())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Mid Term Adjustment data", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Mid Term Adjustment data- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Policy Renewal Selection completed succesfully",
                              this.getTotalExecutionTime());

      }

    private boolean navigateToPolicyRenewalPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.PolicyHoverTabId(),
                EKunduHomePage.RenewalSelectionTabXPath()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.waitForElementById(EKunduHomePage.RenewalSelectionDivId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Navigation to the Renewal Selection page was successful", false);

        return true;
      }

    private boolean findPolicy()
      {
//    
//      String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase1"),"Retrieved Policy Number");
//       
//       if(policyNumber.equals("parameter not found"))
//       {
//           policyNumber = testData.TestParameters.get("Policy Number");
//       }
//       else
//       {
//           testData.updateParameter("Policy Number", policyNumber);
//       }
        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PolicyReferenceTextBoxId(),
                testData.TestParameters.get("Policy Number")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FindNowButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.RenewalSelectionSelectPolicyLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Policy Found", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RenewalSelectionSelectPolicyLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(10000);

        SeleniumDriverInstance.takeScreenShot("Policy successfully copied to the renewal cycle.", false);

        return true;

      }

    private boolean navigateToFindClientPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),
                EKunduHomePage.FindClientTabXPath()))
          {
            return false;
          }

        return true;
      }

    private boolean findClient()
      {
        String clientCode = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                                "Retrieved Client Code");

        if (clientCode.equals("parameter not found"))
          {
            clientCode = testData.TestParameters.get("Client Code");
          }
        else
          {
            testData.updateParameter("Client Code", clientCode);
          }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
          {
            return false;
          }

        return true;
      }

    private boolean findClientAndExtractPolicyNumber()
      {
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText()))
          {
            return false;
          }

        this.RetrievePolicyNumber();

        return true;
      }

    private boolean RetrievePolicyNumber()
      {
        WebElement cell =
            SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_ClientPolicies_updPanelClientQuotes\"]/div[2]/div/table/tbody/tr/td[1]"));

        String policynumber = cell.getText();

        System.out.println("Policy number retrieved - " + policynumber);

        testData.addParameter("Policy Number", policynumber);

        return true;
      }

    private boolean verifyClientDetailsPageHasLoaded()
      {
        if (!SeleniumDriverInstance.validateElementTextValueById(EkunduViewClientDetailsPage.ViewClientDetailsLabelId(),
                "View Client Details"))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Details page successfully loaded", false);

        return true;
      }

    private boolean enterPolicyRenewalData()
      {
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementbyLinkText("Details"))
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);

        SeleniumDriverInstance.scrollElementIntoViewById(EkunduViewClientDetailsPage.MakeLiveButtonId());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.MakeLiveButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);

        return true;

      }

    private boolean navigateToClientDetailsPage()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PersonalClientInfoLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(30000);

        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
