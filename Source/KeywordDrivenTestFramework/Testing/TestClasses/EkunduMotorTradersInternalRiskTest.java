
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduElectronicEquipmentRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author deanR
 */
public class EkunduMotorTradersInternalRiskTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduMotorTradersInternalRiskTest(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the add risk button is present"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the add risk button is present- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterCoverDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter cover details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter cover details- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterSubSectionAData())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter Subsection A data"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Subsection A data- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterSubSectionBData())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter Subsection B data"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Subsection B data- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterExtensions())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk extensions"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk extensions- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterNotes())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk notes"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk notes- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Motor Traders Internal risk added successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Motor Traders Internal risk added successfully",
                              this.getTotalExecutionTime());

      }

     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean verifyAddRiskButtonisPresent()
      {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectMotorTradersInternalRiskType();

            return true;
          }
        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectMotorTradersInternalRiskType();

            return true;
          }
        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");

            return false;
          }
      }

    private boolean selectMotorTradersInternalRiskType()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.takeScreenShot("Motor Traders Internal Risk type selected successfully", false);

        return true;
      }

    private boolean enterCoverDetails()
      {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.CoverVoluntarySubsectionBCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.CoverVoluntaryFAPPercentageTextBoxId(),
                this.getData("Cover - Voluntary FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.CoverVoluntaryFAPAmountTextBoxId(),
                this.getData("Cover - Voluntary FAP Amount")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Cover details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterSubSectionAData()
      {
        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SubSectionASumInsuredTextBoxId(),
                this.getData("SubSectionA - Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SubSectionARateTextBoxId(),
                this.getData("SubSectionA - Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SubSectionAFAPPercTextBoxId(),
                this.getData("SubSectionA - FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SubSectionAFAPAmountTextBoxId(),
                this.getData("SubSectionA - FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SubSectionAAnnualWageAmountTextBoxId(),
                this.getData("SubSectionA - Annual Wages Amount")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Section A data entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterSubSectionBData()
      {
        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SubSectionBLimitofIndemnityTextBoxId(),
                this.getData("SubSection B - Limit of Indemnity")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SubSectionBRateTextBoxId(),
                this.getData("SubSection B - Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SubSectionBFAPPercTextBoxId(),
                this.getData("SubSection B - FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SubSectionBFAPAmountTextBoxId(),
                this.getData("SubSection B - FAP Amount")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("SubSection B data entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterExtensions()
      {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorTradersExtensionsCarHoistsCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExtensionsCarHoistsPremiumTextBoxId(),
                this.getData("Extensions - Premium")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExtensionsCarHoistsFAPPercTextBoxId(),
                this.getData("Extensions - FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExtensionsCarHoistsFAPAmountTextBoxId(),
                this.getData("Extensions - FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorTradersExtensionsLossofKeysCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExtensionsLossofKeysPremiumTextBoxId(),
                this.getData("Extensions - Premium")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExtensionsLossofKeysFAPPercTextBoxId(),
                this.getData("Extensions - FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExtensionsLossofKeysFAPAmountTextBoxId(),
                this.getData("Extensions - FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorTradersExtensionsWindscreenCoverCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExtensionsWindscreenPremiumTextBoxId(),
                this.getData("Extensions - Premium")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExtensionsWindscreenFAPPercTextBoxId(),
                this.getData("Extensions - FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExtensionsWindscreenFAPAmountTextBoxId(),
                this.getData("Extensions - FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorTradersExtensionsWorkAwayCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExtensionsWorkAwayPremiumTextBoxId(),
                this.getData("Extensions - Premium")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExtensionsWorkAwayFAPPercTextBoxId(),
                this.getData("Extensions - FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExtensionsWorkAwayFAPAmountTextBoxId(),
                this.getData("Extensions - FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorTradersExtensionsWreckageRemovalCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExtensionsWreckagePremiumTextBoxId(),
                this.getData("Extensions - Premium")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExtensionsWreckageFAPPercTextBoxId(),
                this.getData("Extensions - FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MotorTradersExtensionsWreckageFAPAmountTextBoxId(),
                this.getData("Extensions - FAP Amount")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk extensions entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterNotes()
      {
        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskCommentsTextboxId(),
                this.getData("Risk Notes")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskPolicyNotesTextboxId(),
                this.getData("Risk Notes")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Comments entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(1000);
        
        SeleniumDriverInstance.clickElementbyLinkText("Documents");
        
        SeleniumDriverInstance.pause(1000);
        
         SeleniumDriverInstance.clickElementById("btnGeneratePdf");
         
         SeleniumDriverInstance.pause(1000);
         
         SeleniumDriverInstance.clickElementById("popup_ok");
////         
////         
////         SeleniumDriverInstance.pause(3000);
         
         SeleniumDriverInstance.clickElementbyLinkText("Risks");
        
        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
