
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreatePersonalClientPage;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author deanR
 */
public class EkunduBankDetailsAddEditDeleteCorporateTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduBankDetailsAddEditDeleteCorporateTest(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!navigateToFindClientPage())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to navigate to the find client page"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the find client page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!verifyFindClientPageHasLoaded())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the find client page has loaded"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the find client page has loaded - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findClient())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to find client"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find client - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!navigateToBankDetailsTab())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to navigate to the bank details tab"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the bank details tab  - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!AddBankDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to Add bank details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to Add bank details  - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!EditBankDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to Edit bank details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to Edit bank details  - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!DeleteBankDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to Delete bank details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to Delete bank details  - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!VerifyDeleteBankDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to vrify deletion banking details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to vrify deletion banking details  - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Adding, Editing, and Deleting of bank details completed successfully"),
                false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Adding, Editing, and Deleting of bank details completed successfully",
                              this.getTotalExecutionTime());

      }

    private boolean navigateToFindClientPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),
                EKunduHomePage.FindClientTabXPath()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Navigation to the Find Client page was successful", false);

        return true;
      }

    private boolean verifyFindClientPageHasLoaded()
      {
        if (!SeleniumDriverInstance.validateElementTextValueById(EKunduFindClientPage.PageHeaderSpanId(),
                "Search Criteria"))
          {
            return false;
          }
        else
          {
            SeleniumDriverInstance.takeScreenShot("Find Client page was successfully loaded", false);
          }

        return true;
      }

    private boolean findClient()
      {
        String clientCode = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                                "Retrieved Client Code");

        if (clientCode.equals("parameter not found"))
          {
            clientCode = testData.TestParameters.get("Client Code");
          }
        else
          {
            testData.updateParameter("Client Code", clientCode);
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ClientCodeTextBoxId(),
                clientCode))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SearchButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.SelectClientLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client found", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SelectClientLinkId()))
          {
            return false;
          }

        return true;
      }

    private boolean navigateToBankDetailsTab()

      // <editor-fold defaultstate="collapsed" desc="navigateToBankDetailsTab">
      {
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduCreatePersonalClientPage.bankDetailsTabPartialLinkText()))
          {
            return false;
          }

        return true;
      }

    // </editor-fold>

    private boolean AddBankDetails()

      // <editor-fold defaultstate="collapsed" desc="enterBankDetails">
      {
        SeleniumDriverInstance.takeScreenShot("Navigationto the bank details tab was successful", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.editClientButtonId()))
          {
            return false;
          }

        navigateToBankDetailsTab();

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.addBankLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToLastDuplicateFrameById(EkunduCreatePersonalClientPage.BankFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreatePersonalClientPage.bankPaymentTypeDropDownListId(),
                testData.TestParameters.get("Bank Payment Type")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.accountHolderNameTextBoxId(),
                testData.TestParameters.get("Account Holder Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.accountTypeTextBoxId(),
                testData.TestParameters.get("Account Type")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.accountNoTextBoxId(),
                testData.TestParameters.get("Account Number")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.branchCodeTextBoxId(),
                testData.TestParameters.get("Bank Branch Code")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreatePersonalClientPage.bankNameDropDownListId(),
                testData.TestParameters.get("Bank Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.addBankButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.acceptAlertDialog())
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        if (!SeleniumDriverInstance.waitForElementById(EkunduCreatePersonalClientPage.SelectBankDetailsLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Bank details entered successfully", false);

        SeleniumDriverInstance.takeScreenShot("Bank details entered successfully", false);

        updateClientDetails();

        SeleniumDriverInstance.pause(2000);

        return true;
      }

    // </editor-fold>

    private boolean EditBankDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.editClientButtonId()))
          {
            return false;
          }

        navigateToBankDetailsTab();

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.EditBankDetailsLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToFrameById(EkunduCreatePersonalClientPage.BankFrameId()))
          {
            return false;
          }

        SeleniumDriverInstance.clearTextById(EkunduCreatePersonalClientPage.accountTypeTextBoxId());

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.accountTypeTextBoxId(),
                testData.TestParameters.get("Account Type 2")))
          {
            return false;
          }

        SeleniumDriverInstance.clearTextById(EkunduCreatePersonalClientPage.accountNoTextBoxId());

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.accountNoTextBoxId(),
                testData.TestParameters.get("Account Number 2")))
          {
            return false;
          }

        SeleniumDriverInstance.clearTextById(EkunduCreatePersonalClientPage.branchCodeTextBoxId());

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.branchCodeTextBoxId(),
                testData.TestParameters.get("Bank Branch Code 2")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreatePersonalClientPage.bankNameDropDownListId(),
                testData.TestParameters.get("Bank Name 2")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.UpdateBankButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.acceptAlertDialog())
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        if (!SeleniumDriverInstance.waitForElementById(EkunduCreatePersonalClientPage.SelectBankDetailsLinkId()))
          {
            return false;
          }

        updateClientDetails();

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Edit Bank details completed successfully", false);

        return true;
      }

    private boolean DeleteBankDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.editClientButtonId()))
          {
            return false;
          }

        navigateToBankDetailsTab();

        if (!SeleniumDriverInstance.clickElementbyLinkText("Delete"))
          {
            return false;
          }

        if (!SeleniumDriverInstance.acceptAlertDialog())
          {
            return false;
          }

        updateClientDetails();

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Edit Bank details completed successfully", false);

        return true;
      }

    private boolean VerifyDeleteBankDetails()
      {
        navigateToBankDetailsTab();

        if (!SeleniumDriverInstance.waitForElementById(EkunduCreatePersonalClientPage.EditBankDetailsLinkId()))
          {
            SeleniumDriverInstance.takeScreenShot("Banking details deleted successfully", false);
          }

        return true;
      }

    private boolean updateClientDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.updateClientButtonId()))
          {
            return false;
          }

        // SeleniumDriverInstance.pause(20000);

        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
