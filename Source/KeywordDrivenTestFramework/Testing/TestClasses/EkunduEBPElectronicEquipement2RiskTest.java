
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduElectronicEquipmentRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author FerdinandN
 */
public class EkunduEBPElectronicEquipement2RiskTest extends BaseClass {

    TestEntity testData;
    TestResult testResultt;

    public EkunduEBPElectronicEquipement2RiskTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk button is present", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify that the add risk button is present - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterIndustryDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter industry details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter industry details - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterRiskItemDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk item details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter risk item details - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

//      if(!performRoadSideAssistQuotationAndMakePolicyLive())
//      {
//          SeleniumDriverInstance.takeScreenShot("Failed to perform Roadside assistance quotation", true);
//          return new TestResult(testData,false, "Failed to Roadside assistance quotation - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//      }
        return new TestResult(testData, Enums.ResultStatus.PASS, "Electronic Equipment Risk details entered successfully",
                this.getTotalExecutionTime());
    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean verifyAddRiskButtonisPresent() {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            System.out.println("Add risk button clicked.");

            return selectElectronicEquipmentRiskType();
        } else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return selectElectronicEquipmentRiskType();
        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

        }

    }

    private boolean selectElectronicEquipmentRiskType() {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.pause(10000);
        SeleniumDriverInstance.takeScreenShot("Electronic Equipment Risk selected sucessfully", false);

        return true;

    }

    private boolean enterIndustryDetails() {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(),
                this.getData("Industry"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.IndustrySearchRowId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Industry details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterRiskItemDetails() {
        //this.clickAddRiskItemButton();
        //SeleniumDriverInstance.Driver.navigate().refresh();

        SeleniumDriverInstance.pause(1000);
        //this.clickAddRiskItemButton();

        if (!SeleniumDriverInstance.clickElementbyName(EkunduElectronicEquipmentRiskPage.AddRiskItemButtonName())) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduElectronicEquipmentRiskPage.ItemDetailsCategoryDropdownListId(),
                this.getData("Item Details - Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsDescriptionTextboxId(),
                this.getData("Item Details - Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSumInsuredTextboxId(),
                this.getData("Item Details - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduElectronicEquipmentRiskPage.ItemDetailsFlatPremiumCheckboxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsFlatPremiumTextboxId(),
                this.getData("Item Details - Flat Premium"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Item Details Entered Sucessfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduElectronicEquipmentRiskPage.RiskAlarmWarrantyDropdownListId(),
                this.getData("Risk - Alarm Warranty"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementbyPartialLinkText("Reinsurances");
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;
    }

    private boolean performRoadSideAssistQuotationAndMakePolicyLive() {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SelectRoadSideAssistQuoteEditLinkId())) {
            return false;
        }

        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Road side assistance quote entered successfully", false);

        SeleniumDriverInstance.pause(5000);

        SeleniumDriverInstance.scrollDownByOnePage("ctl00_cntMainBody_SummaryCoverCntrl_ctl00_AgentCommissionCntrl_lblAgentDisplay");

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.MakeLiveButtonId())) {
            return false;
        }

        return true;
    }

    private boolean clickAddRiskItemButton() {
        WebElement cell = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_EE__ITEM\"]/table/tfoot/tr/td[9]/input[@value = \"Add\"]"));

        if (cell != null) {
            cell.click();
            System.out.println("[TestInfo] Add Risk Item Button Clicked");

            return true;
        } else {
            System.err.println("[Error] Failed to click add risk item button");

            return false;
        }

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
