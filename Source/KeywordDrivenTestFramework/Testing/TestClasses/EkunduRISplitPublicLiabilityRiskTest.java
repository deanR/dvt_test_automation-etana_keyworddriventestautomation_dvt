
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduPublicLiabilityRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduRISplitPublicLiabilityRiskTest extends BaseClass {

    TestEntity testData;
    TestResult testResultt;

    public EkunduRISplitPublicLiabilityRiskTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the add risk button is present ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify that the add risk button is present - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterIndustryDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter industry details ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter industry details - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterGeneralTenantsDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter General Tenants details ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter General Tenants details - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

//        if (!enterDefectiveWorkmanshipDetails()) {
//            SeleniumDriverInstance.takeScreenShot("Failed to enter Defective Workmanship details ", true);
//
//            return new TestResult(testData, Enums.ResultStatus.FAIL,
//                    "Failed to enter Defective Workmanship details - "
//                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//        }

        if (!verifyExclusiveSumInsured()) {
            SeleniumDriverInstance.takeScreenShot("Failed to Verify sum insured ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to Verify sum insured  - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        SeleniumDriverInstance.takeScreenShot("Public Liability risk entered successfully", false);

        return new TestResult(testData, Enums.ResultStatus.PASS,
                "Public Liability risk entered successfully"
                + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());

    }

    private boolean verifyAddRiskButtonisPresent() {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectPublicLiabilityRisk();

            return true;
        } else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            selectPublicLiabilityRisk();
        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");

            return false;
        }

        return true;
    }

    private boolean verifyExclusiveSumInsured() {

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduPublicLiabilityRiskPage.PublicLiabilityReinsuranceBandddlId(), "EBP - Public Liability")) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.validateElementTextValueById(EkunduPublicLiabilityRiskPage.SumInsuredTextBoxId(), "10,000,000.00")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementbyLinkText("Risks")) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        return true;
    }

    private boolean selectPublicLiabilityRisk() {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Public Liability risk selected sucessfully", false);

        return true;
    }

    private boolean enterIndustryDetails() {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(),
                this.getData("Risk Addresses Industry"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.IndustrySearchResultsSelectedRowId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Industry details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduPublicLiabilityRiskPage.RiskDefectiveWorkmanshipCheckboxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduPublicLiabilityRiskPage.RiskProductsCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityNextButtonId())) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        return true;

    }

    private boolean enterGeneralTenantsDetails() {
        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.GeneralTernantsLimitofIndemnityTextboxId(),
                this.getData("General Tenants Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.GeneralTernantsPremiumTextboxId(),
                this.getData("General Tenants Premium"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("General Tenants details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityNextButtonId())) {
            return false;
        }

        //Defective Workmanship limit of indeminity
        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.DefectiveWorkmanshipLimitofIndemnityTextboxId(),
                this.getData("Defective Workmanship Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.DefectiveWorkmanshipTurnoverTextboxId(),
                this.getData("Defective Workmanship Turnover"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.RiskDefectiveWorkmanshipRateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityNextButtonId())) {
            return false;
        }

        //public liability section Next
        if (!productsLiabilityDetails()) {
            return false;
        }

        //Extention section Next
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityFinishButtonId())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        return true;
    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean enterDefectiveWorkmanshipDetails() {
        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.DefectiveWorkmanshipLimitofIndemnityTextboxId(),
                this.getData("Defective Workmanship Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.DefectiveWorkmanshipTurnoverTextboxId(),
                this.getData("Defective Workmanship Turnover"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduPublicLiabilityRiskPage.DefectiveWorkmanshipQuestionnaireCheckboxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityRateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Defective Workmanship details entered successfully", true);

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityNextButtonId())) {
            return false;
        }

        return true;

    }

    private boolean productsLiabilityDetails() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduPublicLiabilityRiskPage.ProductsLiabilityBasisDropdownListId(),
                this.getData("Products Liability Basis"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.ProductsLiabilityLimitofIndemnityTextboxId(),
                this.getData("Products Liability Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.ProductsLiabilityTurnoverTextboxId(),
                this.getData("Products Liability Turnover"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduPublicLiabilityRiskPage.ProductsLiabilityFoodPoisoningOnlyCheckboxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduPublicLiabilityRiskPage.ProductsLiabilityQuestionnairesReceivedCheckboxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityRateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Products Liability details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityNextButtonId())) {
            return false;
        }

        return true;
    }

}
//by Jindent --- http://www.jindent.com
