
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduEmployersLiabilityRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduEmployersLiabilityRiskTest extends BaseClass {

    TestEntity testData;
    TestResult testResultt;

    public EkunduEmployersLiabilityRiskTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the add risk button is present ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify that the add risk button is present - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterRiskDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter risk details - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterNotes()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk notes", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter risk notes - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        SeleniumDriverInstance.takeScreenShot("Employers Liability risk entered successfully", false);

        return new TestResult(testData, Enums.ResultStatus.PASS,
                "Employers Liability risk entered successfully"
                + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());

    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean verifyAddRiskButtonisPresent() {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectEmployersLiabilityRisk();

            return true;
        } else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            selectEmployersLiabilityRisk();
        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");

            return false;
        }

        return true;
    }

    private boolean selectEmployersLiabilityRisk() {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.pause(10000);
        SeleniumDriverInstance.takeScreenShot("Employers Liability risk selected sucessfully", false);

        return true;
    }

    private boolean enterRiskDetails() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduEmployersLiabilityRiskPage.RiskDetailsClassDropdownListId(),
                this.getData("Risk Details Class"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEmployersLiabilityRiskPage.RiskDetailsLimitofIndemnityTextboxId(),
                this.getData("Risk Details Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEmployersLiabilityRiskPage.RiskDetailsAnnualEarningsTextboxId(),
                this.getData("Risk Details Annual Earnings"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEmployersLiabilityRiskPage.RiskDetailsAgreedRateTextboxId(),
                this.getData("Risk Details Agreed Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduEmployersLiabilityRiskPage.RiskDetailsRateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Risk details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduEmployersLiabilityRiskPage.RiskDetailsEmployersLiabilityNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterNotes() {
        SeleniumDriverInstance.clickElementById(EkunduEmployersLiabilityRiskPage.RiskDetailsEmployersLiabilityNextButtonId());
        SeleniumDriverInstance.clickElementById(EkunduEmployersLiabilityRiskPage.RiskDetailsEmployersLiabilityNextButtonId());

        if (!SeleniumDriverInstance.enterTextById(EkunduEmployersLiabilityRiskPage.RiskCommentsTextboxId(),
                this.getData("Risk Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduEmployersLiabilityRiskPage.RiskAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduEmployersLiabilityRiskPage.RiskNotesTextboxId(),
                this.getData("Risk Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Employers Liability notes entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduEmployersLiabilityRiskPage.RiskDetailsEmployersLiabilityNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Documents");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementById("btnGeneratePdf");

        SeleniumDriverInstance.clickElementById("popup_ok");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Risks");

        return true;

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
