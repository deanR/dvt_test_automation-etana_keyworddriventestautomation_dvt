/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduGoodsInTransitRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPageNew;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduGoodsInTransitRiskPageNew;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPageNew;

/**
 *
 * @author ferdinandN
 */
public class EkunduEPLWaterAndPleasureCraftRisk extends BaseClass
{
    TestEntity testData;
    TestResult testResultt;
    
    public EkunduEPLWaterAndPleasureCraftRisk(TestEntity testData)
    {
        this.testData = testData;
    } 
    
    public TestResult executeTest()
     {
      this.setStartTime();
      
      if(!verifyRisksDialogisPresent())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to verify that the risks dialog is present"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the risks dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
      
      if(!enterVesselandHullDetails())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to enter Vessel and Hull details"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Vessel and Hull details- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
           
       if(!enterAccessories())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to enter accessories"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter accessories- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
       
       if(!enterExtensions())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to enter extensions"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter extensions- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
              
        if(!enterInterestedParties())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to enter extensions"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter extensions- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
       
       if(!enterNotes())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to enter risk notes"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter risk notes- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
       
       if(!enterFACPropPlacementData())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to enter FAC Prop Placement data"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter FAC Prop Placement data- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
       
//       if(!enterFACXOLPlacementData())
//      {
//          SeleniumDriverInstance.takeScreenShot(("Failed to enter FAC XOL Placement data"), true);
//          return new TestResult(testData, false, "Failed to enter FAC XOL Placement data- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//      }
      
        SeleniumDriverInstance.takeScreenShot(("Water and Pleasure Craft risk added successfully"), false);
        return new TestResult(testData,Enums.ResultStatus.PASS, "Water and Pleasure Craft risk added successfully", this.getTotalExecutionTime());  
 
     }
    
    private boolean verifyRisksDialogisPresent()
    {
            SeleniumDriverInstance.pause(2000);
       
            if(SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
                {
                    SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
                    selectWaterandPleasureCraftRiskType();
                    return true;
                }
            
            else if(SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
            {
                    selectWaterandPleasureCraftRiskType();
                    return true;

            }
             
            
            else
                {
                    SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
                    System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");
                    return false;

                }

    }
  
    private boolean selectWaterandPleasureCraftRiskType()
    {
        if(!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            return false;
        }
          
        SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"));
        
 
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return false;
        }
      
        SeleniumDriverInstance.takeScreenShot("Water and Pleasure craft risk type selected successfully", false);
        return true;
     
    }
     
    private boolean enterVesselandHullDetails()
    {
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MaterialofHullTextBoxId(), testData.TestParameters.get("Material of Hull")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.HullSumInsuredTextBoxId(), testData.TestParameters.get("Hull Sum Insured")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.HullUseDropdownListId(), testData.TestParameters.get("Hull Use")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPageNew.VesselSelfBuiltCheckBoxId(), true))
         {
             return false;
         }
         
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPageNew.AdditionalRateTextBoxId(), "0.18"))
         {
             return false;
         }
         
        if(!SeleniumDriverInstance.checkBoxSelectionById("ctl00_cntMainBody_HULL__ACC_APPL", true))
         {
             return false;
         } 
        
        if(!SeleniumDriverInstance.checkBoxSelectionById("ctl00_cntMainBody_HULL__IP_APPL", true))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.NameofVesselTextBoxId(), testData.TestParameters.get("Name of Vessel")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MakeandModelTextBoxId(), testData.TestParameters.get("Make and Model")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TypeofVesselDropdownListId(), testData.TestParameters.get("Type of Vessel")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorBoatSpeedDropdownListId(), testData.TestParameters.get("Motor Boat Max Speed")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NumberofEnginesDropdownListId(), testData.TestParameters.get("No of Engines")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.EngineTypeDropdownListId(), testData.TestParameters.get("Engine Type")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine1SumInsuredTextBoxId(), testData.TestParameters.get("Engine 1 Sum Insured")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.Engine1RateTextBoxId(), testData.TestParameters.get("Engine 1 Rate")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.HullRateRequiredTextBoxId(), testData.TestParameters.get("Hull Rate")))
         {
             return false;
         }
         
            SeleniumDriverInstance.takeScreenShot("Vessel/Hull details entered successfully", false);
         
            SeleniumDriverInstance.scrollDownByOnePage(EkunduCreateNewPolicyForNewClientPage.HullUseDropdownListId());
         
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureNextButtonId()))
         {
             return false;
         }
        
             return true;
             
     } 
    
     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
    
    private boolean enterAccessories()
    {
        if(!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPageNew.AccessoriesAddButtonName()))
         {
             return false;
         }
        
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPageNew.AccessoriesDescriptionTextBoxId(), testData.TestParameters.get("Accessories - Description")))
         {
             return false;
         }
         
          if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPageNew.AccessoriesSumInsuredTextBoxId(), testData.TestParameters.get("Accessories - Sum Insured")))
         {
             return false;
         }
          
           if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPageNew.AccessoriesRateTextBoxId(), testData.TestParameters.get("Accessories - Rate")))
         {
             return false;
         }
           
           SeleniumDriverInstance.takeScreenShot("Accessories entered successfully", false);
           
           if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureNextButtonId()))
         {
             return false;
         }
        
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureNextButtonId()))
         {
             return false;
         }
        
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureNextButtonId()))
         {
             return false;
         }
        
   
        return true;
        
        
    }
    
     private boolean enterInterestedParties()
     {
        if(!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPageNew.AddInterestedParties2ButtonName()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPageNew.TypeofAgreementType2DropdownListId(), testData.TestParameters.get("Type of Agreement")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPageNew.InstitutionName3DropdownListId(), testData.TestParameters.get("Institution Name")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPageNew.InterestedPartiesComments3TextAreaId(), testData.TestParameters.get("Interested Party Comments")))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Interested parties details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
        {
            return false;
        }
     
            return true;
        
    }
    
    private boolean enterExtensions()
    {
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPageNew.WaterSkierLiabilityCheckBoxId(), true))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPageNew.DroppingOffOfMotorCheckBoxId(), true))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.DroppingOffMotorSumTextBoxId(), testData.TestParameters.get("Dropping off of motor - Sum insured")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.DroppingOffMotorRateTextBoxId(), testData.TestParameters.get("Dropping off of motor - Rate")))
         {
             return false;
         }
        
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPageNew.SubmergeObjectsExtCheckBoxId(), true))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SubmergerObjectsSumTextBoxId(), testData.TestParameters.get("Submerge objects ext - Sum insured")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPageNew.SubmergerObjectsRateTextBoxId(), testData.TestParameters.get("Submerge objects ext - Rate")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureNextButtonId()))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureNextButtonId()))
         {
             return false;
         }
        
        
        return true;
    }
    
    private boolean enterNotes()
    {
        SeleniumDriverInstance.pause(2000);
        
        SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.WaterandPleasureCraftCommentsTextAreaId(), testData.TestParameters.get("Comments"));


        SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);

        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EnrouteNextButtonId());


        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EnrouteNextButtonId());

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        return true;
     }
    
    private boolean enterFACPropPlacementData()
       {
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
          {
               return false;
           }
           
           SeleniumDriverInstance.pause(5000);
           
           if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(),testData.TestParameters.get("Reinsurance Details - FAC Placement Reinsurer Code 1")))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
           {
               return false;
           }
           
           SeleniumDriverInstance.pause(8000);
           
               // SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId());
           
           if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId(), testData.TestParameters.get("Reinsurance Details - FAC Placement - ACE Percentage")))
           {
               return false;
           }
           
            if(!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
           {
               return false;
           }
            
            SeleniumDriverInstance.pause(5000);
            
            System.out.println("Company 1 added successfully");
        
            return true;
       } 
    
    
    private boolean enterFACXOLPlacementData()
    {
        SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPageNew.ReinsuranceBandDropdownListId(), "EPL - PC Own Damage (Section A)");
        
        SeleniumDriverInstance.pause(3000);
            
        if(!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.AddFACXOLButtonId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.pause(3000);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPageNew.FACSelectFirstReinsurer3LinkId()))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), testData.TestParameters.get("Etana Reinsurer Code")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPageNew.FACSelectReinsurer3LinkId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), testData.TestParameters.get("Med Reinsurer Code")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
        {
            return false;
        }
         if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLLowerLimitId(),testData.TestParameters.get("FAP LOX Lower Limit")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        
        //SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId(), testData.TestParameters.get("FAP LOX Upper Limit")))
        {
            return false;
        }
        
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        //SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId());
        
        SeleniumDriverInstance.pause(5000);
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId(),testData.TestParameters.get("Etana SLFG Participation Percentage")))
        {
            return false;
        }
        
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
         
       // SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId(), testData.TestParameters.get("Etana SLFG Premium")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPageNew.FACXOLParticipationPerc2TextboxId(), testData.TestParameters.get("Etana MOTBD Participation Percentage")))
        {
            return false;
        }
       
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
       
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPageNew.FACXOLPremium2TextboxId(), testData.TestParameters.get("Etana MOTBD Premium")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
          if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPageNew.FACXOLParticipationPerc3TextboxId(), testData.TestParameters.get("Med Participation Percentage")))
        {
            return false;
        }
       
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
       
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPageNew.FACXOLPremium3TextboxId(), testData.TestParameters.get("Med Premium")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
      
        SeleniumDriverInstance.pause(5000);
        
        SeleniumDriverInstance.takeScreenShot("FAC XOL Placement 1 completed successfully", false);
        
        SeleniumDriverInstance.pause(5000);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.FACXOLOkButtonId()))
        {
            return false;
        }
        
//        SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.AddFACXOLButtonId());
//       
//        
//        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), testData.TestParameters.get("Reinsurer Code3")))
//        {
//            return false;
//        }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
//        {
//            return false;
//        }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
//        {
//            return false;
//        }
//        
//        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId());
//        
//        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), testData.TestParameters.get("Reinsurer Code4")))
//        {
//            return false;
//        }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
//        {
//            return false;
//        }
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
//        {
//            return false;
//        }
//         if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLLowerLimitId(),testData.TestParameters.get("FAP LOX Lower Limit2")))
//        {
//            return false;
//        }
//        
//         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
//        
//        
//        //SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId());
//        
//        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId(), testData.TestParameters.get("FAP LOX Upper Limit2")))
//        {
//            return false;
//        }
//        
//        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
//        
//        //SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId());
//        
//        SeleniumDriverInstance.pause(5000);
//        
//        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId(),testData.TestParameters.get("FAP LOX Participation Percentage1")))
//        {
//            return false;
//        }
//        
//        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
//         
//       // SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId());
//        
//        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId(), testData.TestParameters.get("FAP LOX Premium1")))
//        {
//            return false;
//        }
//        
//         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
//        
//        
//        
//        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPerc2TextboxId(), testData.TestParameters.get("FAP LOX Participation Percentage1")))
//        {
//            return false;
//        }
//       
//        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
//       
//        
//        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLPremium2TextboxId(), testData.TestParameters.get("FAP LOX Premium1")))
//        {
//            return false;
//        }
//        
//         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
//        
//      
//        SeleniumDriverInstance.pause(5000);
//        
//        SeleniumDriverInstance.takeScreenShot("FAC XOL Placement 1 completed successfully", false);
//        
//        if(!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.FACXOLOkButtonId()))
//        {
//            return false;
//        }
//        
        SeleniumDriverInstance.pause(5000);
        
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(10000);
        
        
        return true;
        
    }
     
       
}

