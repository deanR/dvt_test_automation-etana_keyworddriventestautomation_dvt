/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduEhatElectronicEquipmentPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class EkunduEHATElectronicEquipmentTest extends BaseClass
{
     TestEntity testData;
    TestResult testResultt;
    
    public EkunduEHATElectronicEquipmentTest(TestEntity testData)
    {
        this.testData = testData;
    }
    
    public TestResult executeTest()
    {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();
        
        if(!verifyAddRiskButtonisPresent())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk button is present", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to verify that the add risk button is present - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterIndustryDetails())
        {
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter industry details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterBusinessRiskItemDetailsLaptops())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Business Risk Item details for Laptops", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Business Risk Item details for Laptops - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterFirstAmountPayable())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter First Amount Payable", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter First Amount Payable - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterExtensions())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Extensions", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Extensions - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
         if(!enterRisk())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Risk Details", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter endorsements - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!enterEndorsements())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter endorsements", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter endorsements - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!enterNotes())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk notes", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter risk notes - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
            return new TestResult(testData,Enums.ResultStatus.PASS, "EHAT Electronic Equipment Risk added successfully" + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
    }
    
     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
    
    private boolean verifyAddRiskButtonisPresent()
    {
        if(SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
        {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectElectronicEquipmentRiskType();
            return true;
        }
        else if(SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            selectElectronicEquipmentRiskType(); 
            return true;
        }
        else
        {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");
            return false;
        }
 
    }
    
    private boolean selectElectronicEquipmentRiskType()
    {
        if(!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
        {
            return false;
        }

        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return false;
        }
            SeleniumDriverInstance.pause(10000);
            
            SeleniumDriverInstance.takeScreenShot("Electronic Equipment Risk selected sucessfully", false);
            
            return true;
        
    }
    
    private boolean enterIndustryDetails() 
    {
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId()))
        {
            return false;
        }
    
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(), this.getData("Details Industry")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId()))
        {
            return false;
        }
        if(!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.IndustrySearchResultsSelectedRowId()))
        {
            return false;
        }
            SeleniumDriverInstance.pause(2000);

            SeleniumDriverInstance.takeScreenShot("Industry details entered successfully", false);
            
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
            
            return true;
    }
    
     private boolean enterBusinessRiskItemDetailsLaptops()
    {
        if(!SeleniumDriverInstance.clickElementbyName(EkunduEhatElectronicEquipmentPage.BusinessAllRiskAddButtonName2()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduEhatElectronicEquipmentPage.ItemCategoryDropdownListId(), this.getData("Item 2 Category")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ItemDetailDescriptionTextboxId(), this.getData("Item 2 Description")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ItemDetailSerialNumberTextboxId(), this.getData("Item 2 Serial Number")))
        {
            return false;
        }
        
            SeleniumDriverInstance.clearTextById(EkunduEhatElectronicEquipmentPage.ItemDetailDateAddedTextboxId());
       
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduEhatElectronicEquipmentPage.ItemDetailDateAddedTextboxId(), this.getData("Item 2 Date Added")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ItemDetailSumInsuredTextboxId(), this.getData("Item Sum Insured 2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ItemDetailNoOfItemsTextboxId(), this.getData("number of items 2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ItemDetailRateTextboxId(), this.getData("Item Agreed Rate 2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEhatElectronicEquipmentPage.FAPcheckboxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.FAPMinPercentageTextboxId(), this.getData("FAP Min Percentage 2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.FAPMinAmountTextboxId(), this.getData("FAP Min Amount 2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.FAPMaxAmountTextboxId(), this.getData("FAP Max Amount 2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyName(EkunduEhatElectronicEquipmentPage.InterestedPartiesAddButtonName()))
        {
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.InterestedPartiesTextboxId(), this.getData("Interested Party Name 2")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.NextButtonId()))
        {
            return false;
        } 
         
         if(!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.NextButtonId()))
        {
            return false;
        } 
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ItemNotes1TextboxId(), this.getData("Notes")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEhatElectronicEquipmentPage.NotesCheckboxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ItemNotes2TextboxId(), this.getData("Notes")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.NextButtonId()))
        {
            return false;
        } 
        
        SeleniumDriverInstance.takeScreenShot("Laptop item Details entered successfully", false);
        
        return true;
    }
    
    private boolean enterFirstAmountPayable()
    {
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEhatElectronicEquipmentPage.RiskItemFAPPerSectionCheckboxId(), true))
        {
            return false;
        }
        
        SeleniumDriverInstance.pause(1500);
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.RiskItemFAPPerSectionMinimumPercTextboxId(), this.getData("FAP Per Section Min Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.RiskItemFAPPerSectionMinimumAmountTextboxId(), this.getData("FAP Per Section Min Amount 2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.RiskItemFAPPerSectionMaximumAmountTextboxId(), this.getData("FAP Per Section Max Amount 2")))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEhatElectronicEquipmentPage.RiskItemFAPLightningCheckboxId(), true))
        {
            return false;
        }
        
        SeleniumDriverInstance.pause(1500);
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.RiskItemFAPLightningMinimumPercTextboxId(), this.getData("FAP Lightning/Surge Min Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.RiskItemFAPLightningMinimumAmountTextboxId(), this.getData("FAP Per Section Min Amount")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.RiskItemFAPLightningMaximumAmountTextboxId(), this.getData("FAP Per Section Max Amount")))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("First Amount Payable details entered successfully", false);
            return true;
            
        
    }
    
    private boolean enterExtensions()
    {
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEhatElectronicEquipmentPage.ExtensionsAdditionalClaimsPreparationCheckboxId(), true))
        {
            return false;
        }
        
        SeleniumDriverInstance.pause(1500);
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsAdditionalClaimsPreparationLimitofIndemnityTextboxId(), this.getData("Extensions Limit of Indemnity")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsAdditionalClaimsPreparationRateTextboxId(), this.getData("Extensions Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEhatElectronicEquipmentPage.ExtensionsFinesAndPenaltiesCheckboxId(), true)) 
        {
            return false;
        }
        
        SeleniumDriverInstance.pause(1500);
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsFinesAndPenaltiesLimitofIndemnityTextboxId(), this.getData("Extensions Limit of Indemnity")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsFinesAndPenaltiesRateTextboxId(), this.getData("Extensions Rate")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsFinesAndPenaltiesFAPPercTextboxId(), this.getData("Extensions FAP Percentage")))
        {
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsFinesAndPenaltiesFAPAmountTextboxId(), this.getData("Extensions FAP Amount")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEhatElectronicEquipmentPage.ExtensionsIncreaseCostCheckboxId(), true))
        {
            return false;
        }
         
         SeleniumDriverInstance.pause(1500);
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsIncreaseCostLimitofIndemnityTextboxId(), this.getData("Extensions Limit of Indemnity")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsIncreaseCostRateTextboxId(), this.getData("Extensions Rate")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsIncreaseCostFAPPercTextboxId(), this.getData("Extensions FAP Percentage")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsIncreaseCostFAPAmountTextboxId(), this.getData("Extensions FAP Amount")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduEhatElectronicEquipmentPage.ExtensionsIncreaseCostIndemnityPeriodDropdownListId(), this.getData("Indemnity period")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduEhatElectronicEquipmentPage.ExtensionsIncreaseCostTimeFAPDropdownListId(), this.getData("Time FAP")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEhatElectronicEquipmentPage.ExtensionsLossofRevenueCheckboxId(), true))
        {
            return false;
        }
         
         SeleniumDriverInstance.pause(1500);
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsLossofRevenueLimitofIndemnityTextboxId(), this.getData("Extensions Limit of Indemnity")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsLossofRevenueRateTextboxId(), this.getData("Extensions Rate")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsLossofRevenueFAPPercTextboxId(), this.getData("Extensions FAP Percentage")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsLossofRevenueFAPAmountTextboxId(), this.getData("Extensions FAP Amount")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduEhatElectronicEquipmentPage.ExtensionsLossOfRevenueIndemnityPeriodDropdownListId(), this.getData("Indemnity period")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEhatElectronicEquipmentPage.ExtensionsReinstatementofDataCheckboxId(), true))
        {
            return false;
        }
         
         SeleniumDriverInstance.pause(1500);
         
             if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsReinstatementofDataLimitofIndemnityTextboxId(), this.getData("Extensions Limit of Indemnity")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsReinstatementofDataRateTextboxId(), this.getData("Extensions Rate")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsReinstatementofDataFAPPercTextboxId(), this.getData("Extensions FAP Percentage")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsReinstatementofDataFAPAmountTextboxId(), this.getData("Extensions FAP Amount")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEhatElectronicEquipmentPage.ExtensionsTelkomAccessLinesCheckboxId(), true))
        {
            return false;
        }
         
         SeleniumDriverInstance.pause(1500);
         
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsTelkomAccessLinesLimitofIndeminityTextboxId(), this.getData("Extensions Limit of Indemnity")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsTelkomAccessLinesRateTextboxId(), this.getData("Extensions Rate")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsTelkomAccessLinesFAPPercTextboxId(), this.getData("Extensions FAP Percentage")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsTelkomAccessLinesFAPAmountTextboxId(), this.getData("Extensions FAP Amount")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEhatElectronicEquipmentPage.ExtensionsTransitCheckboxId(), true))
        {
            return false;
        }
         
         SeleniumDriverInstance.pause(1500);
         
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsTransitLimitofIndeminityTextboxId(), this.getData("Extensions Limit of Indemnity")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsTransitRateTextboxId(), this.getData("Extensions Rate")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsTransitFAPPercTextboxId(), this.getData("Extensions FAP Percentage")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.ExtensionsTransitFAPAmountTextboxId(), this.getData("Extensions FAP Amount")))
        {
            return false;
        }
         
            SeleniumDriverInstance.takeScreenShot("Extensions entered successfully", false);
            
        if(!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.NextButtonId()))
        {
            return false;
        }
            
            return true;
    }
    
   private boolean enterRisk()
   {
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduEhatElectronicEquipmentPage.RiskAlarmWarrantyDropdownListId(), this.getData("Alarm Warranty")))
        {
            return false;
        }
       
       if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEhatElectronicEquipmentPage.TypeOfExposureCheckboxID(), true))
        {
            return false;
        }
         
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.OverrideMPLTextBoxID(), this.getData("Override MPL")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEhatElectronicEquipmentPage.AssetRegisterBaseCheckboxID(), true))
        {
            return false;
        }
        
            SeleniumDriverInstance.clearTextById(EkunduEhatElectronicEquipmentPage.AssetRegRecievedDateTextBoxID());
         
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduEhatElectronicEquipmentPage.AssetRegRecievedDateTextBoxID(), this.getData("Asset Date Recieved")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEhatElectronicEquipmentPage.TheftLimitCheckboxID(), true))
        {
            return false;
        }
         
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.TheftLimitAmountTextBoxID(), this.getData("Theft Limit Recieved")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyName(EkunduEhatElectronicEquipmentPage.MultiAddressesAddButtonName()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.ChangeButtonID()))
        {
            return false;
        } 
        
        if(!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.addressesSelectedRowId()))
        {
            return false;
        }
        
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.PremisesSumInsuredId(), this.getData("Premises Sum Insured")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.NextButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.NextButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.NextButtonId()))
        {
            return false;
        }
        
            return true;
   }
   
   private boolean enterEndorsements()
   {
       
       if(!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.EndorsementsSelectButtonId()))
        {
            return false;
        }
       
       
       if(!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.EndorsementsAddAllButtonId()))
        {
            return false;
        }
       
       
       if(!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.EndorsementsApplyButtonId()))
        {
            return false;
        }
       
            SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);
            
        if(!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.NextButtonId()))
        {
            return false;
        }
        
            return true;
   }
   
   private boolean enterNotes()
   {
//       if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.RiskItemCommentsTextboxId(), this.getData("Electronic Equipment Notes")))
//       {
//           return false;
//       }
//       
//       if(!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.RiskItemAddNotesLabelId()))
//        {
//            return false;
//        }
//       
//        if(!SeleniumDriverInstance.enterTextById(EkunduEhatElectronicEquipmentPage.RiskItemNotesTextboxId(), this.getData("Electronic Equipment Notes")))
//       {
//           return false;
//       }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduEhatElectronicEquipmentPage.NextButtonId()))
        {
            return false;
        }
        
            SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
            SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        
            return true;
       
   }
  
}