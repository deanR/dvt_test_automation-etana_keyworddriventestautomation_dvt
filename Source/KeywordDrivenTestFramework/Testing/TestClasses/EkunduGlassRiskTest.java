
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduGlassRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduGlassRiskTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduGlassRiskTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the add risk button is present ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the add risk button is present - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterRiskItemDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk item details ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk item details - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterExtensions())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter extensions ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter extensions - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterEndorsements())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter endorsements", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter endorsements - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterComments())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk comments", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk comments - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }
        
        if (!verifyPremium()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify premium", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify premium  - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Glass risk details entered successfully", this.getTotalExecutionTime());

      }

    private boolean verifyAddRiskButtonisPresent()
      {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectGlassRisk();

            return true;
          }
        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectGlassRisk();
          }
        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");

            return false;
          }

        return true;
      }

    private boolean selectGlassRisk()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(10000);
        SeleniumDriverInstance.takeScreenShot("Glass risk selected sucessfully", false);

        return true;
      }

    private boolean enterRiskItemDetails()
      {
//          SeleniumDriverInstance.clickElementById(EkunduGlassRiskPage.AddRiskItemButtonName());
//          
//          if (!SeleniumDriverInstance.clickElementById(EkunduGlassRiskPage.AddRiskItemButtonName()))
//          {
//            return false;
//          }

           
        if (!SeleniumDriverInstance.enterTextById(EkunduGlassRiskPage.RiskItemSumInsuredTextboxId(),
                this.getData("Risk Item Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduGlassRiskPage.RiskItemRateButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk item details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduGlassRiskPage.NextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterExtensions()
      {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduGlassRiskPage.ExtensionsAdditionalClaimsCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGlassRiskPage.ExtensionsAdditionalClaimsLimitofIndemnityTextboxId(),
                this.getData("Additional Claim Limit of Indemnity")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGlassRiskPage.ExtensionsAdditionalClaimsRateTextboxId(),
                this.getData("Additional Claim Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduGlassRiskPage.ExtensionsRiotandStrikesCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGlassRiskPage.ExtensionsRiotandStrikesPremiumTextboxId(),
                this.getData("Riot & Strike Premium")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGlassRiskPage.ExtensionsRiotandStrikesFAPPercentageTextboxId(),
                this.getData("Riot & Strike FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGlassRiskPage.ExtensionsRiotandStrikesFAPAmountTextboxId(),
                this.getData("Riot & Strike FAP Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduGlassRiskPage.ExtensionsSpecialReinstatementCheckboxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGlassRiskPage.ExtensionsSpecialReinstatementRateTextboxId(),
                this.getData("Special Reinstatement Rate")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGlassRiskPage.ExtensionsSpecialReinstatementFAPPercentageTextboxId(),
                this.getData("Special Reinstatement FAP Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGlassRiskPage.ExtensionsSpecialReinstatementFAPAmountTextboxId(),
                this.getData("Special Reinstatement FAP Amount")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Extensions entered successfuly", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduGlassRiskPage.NextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterEndorsements()
      {
        SeleniumDriverInstance.clickElementById(EkunduGlassRiskPage.NextButtonId());

        if (!SeleniumDriverInstance.clickElementById(EkunduGlassRiskPage.EndorsementsSelectButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduGlassRiskPage.EndorsementsAddAllButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduGlassRiskPage.EndorsementsApplyButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduGlassRiskPage.NextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterComments()
      {
        if (!SeleniumDriverInstance.enterTextById(EkunduGlassRiskPage.GlassNotesTextboxId(),
                this.getData("Glass Comments")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduGlassRiskPage.NotesAddNotesCheckboxId(), true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduGlassRiskPage.NotesPolicyNotesTextboxId(),
                this.getData("Glass Comments")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduGlassRiskPage.NextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        
        

        return true;
      }
    
    private boolean verifyPremium() {
        if (!SeleniumDriverInstance.verifyPremiumDetails()) {
            return false;
        }
        
        

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Documents");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementById("btnGeneratePdf");
//
//        SeleniumDriverInstance.pause(3000);
//
        SeleniumDriverInstance.clickElementById("popup_ok");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Risks");

        return true;
    }

     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
   
  }


//~ Formatted by Jindent --- http://www.jindent.com
