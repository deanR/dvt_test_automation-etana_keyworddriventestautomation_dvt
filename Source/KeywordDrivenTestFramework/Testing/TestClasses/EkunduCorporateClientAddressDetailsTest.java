
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreatePersonalClientPage;

/**
 *
 * @author ferdinandN
 */
public class EkunduCorporateClientAddressDetailsTest extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public EkunduCorporateClientAddressDetailsTest(TestEntity testData) {
        this.testData = testData;

    }

    public TestResult executeTest() {
        this.setStartTime();

        if (!navigateToFindClientPage()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to navigate to the find client page"), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to navigate to the find client page- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!findClient()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to find client"), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to find client" + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!addHomeAddress()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to add home address"), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to add home address" + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!editHomeAddress()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to edit home address"), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to edit home address" + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!deleteHomeAddress()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to delete home address"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to delete home address" + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Client address details successfully added, edited and deleted",
                this.getTotalExecutionTime());
    }

    private boolean navigateToFindClientPage() {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),
                EKunduHomePage.FindClientTabXPath())) {
            return false;
        }

        return true;
    }

    private boolean findClient() {
        String clientCode
                = SeleniumDriverInstance.retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                        "Retrieved Client Code");

        if (clientCode.equals("parameter not found")) {
            clientCode = testData.TestParameters.get("Client Code");
        } else {
            testData.updateParameter("Client Code", clientCode);
        }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId())) {
            return false;
        }

        return true;

    }

    private boolean addHomeAddress() {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.editClientButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduCreatePersonalClientPage.AddressTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.AddAdressLinkId())) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameById(EKunduCreateCorporateClientPage.AddressFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreatePersonalClientPage.addressTypeDropDownListId(),
                "Home Address")) {
            return false;
        }

//        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.FindSuburbButtonId()))
//          {
//            return false;
//          }
        //SeleniumDriverInstance.switchToDefaultContent();

//        if (!SeleniumDriverInstance.enterTextById(EKunduCreateCorporateClientPage.SuburbSearchTextBoxId(), "Westgate")) {
//            return false;
//        }

//         if (!SeleniumDriverInstance.enterTextById(EKunduCreateCorporateClientPage.AddressLine1TextBoxId(), "Westgate")) {
//            return false;
//        }
//        SeleniumDriverInstance.pause(1000);
//
//        if (!SeleniumDriverInstance.ArrowdownToElementById(EKunduCreateCorporateClientPage.AddressLine1TextBoxId())) {
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.AddressAddButtonId())) {
//            return false;
//        }
////        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SuburbSearchButtonId()))
//          {
//            return false;
//          }

//        if (!SeleniumDriverInstance.switchToDefaultContent()) {
//            return false;
//        }
////
//        SeleniumDriverInstance.pause(2000);
//
//        if (!SeleniumDriverInstance.clickNestedElementUsingIds(EKunduCreateCorporateClientPage.SuburbSearchResultsTableId(),
//                EKunduCreateCorporateClientPage.SuburbSearchResults2SelectedCellId()))
//          {
//            return false;
//          }

//        if (!SeleniumDriverInstance.switchToFrameById(EKunduCreateCorporateClientPage.AddressFrameId())) {
//            return false;
//        }

        if (!SeleniumDriverInstance.enterTextById(EKunduCreateCorporateClientPage.AddressLine1TextBoxId(),
                "12 Webster Street")) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.ArrowdownToElementById(EKunduCreateCorporateClientPage.AddressLine1TextBoxId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.AddressAddButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContentWhenElementNoLongerVisible(EKunduCreateCorporateClientPage.AddressFrameId())) {
            return false;
        }

        System.out.println("Attempting to close Address dialog.");
        SeleniumDriverInstance.pause(2000);

        // this.closeAddressDialog();
        if (!SeleniumDriverInstance.clickElementbyXpath(EkunduCreatePersonalClientPage.CloseAddressDialogButtonXPath2())) {
            //return false;
        }

        SeleniumDriverInstance.takeScreenShot("Home address successfully added", false);

        SeleniumDriverInstance.switchToDefaultContent();

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.updateClientButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        return true;

    }

    private boolean closeAddressDialog() {
        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EkunduCreatePersonalClientPage.CloseAddressDialogButtonXPath2())) {

            return false;

        }

        return true;
    }

    private boolean editHomeAddress() {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.editClientButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduCreatePersonalClientPage.AddressTabPartialLinkText())) {
            return false;
        }

//      if(!SeleniumDriverInstance.clickElementbyXpath("//*[@id=\"menu_2\"]/li[1]/a[2"))
//      {
//          return false;
//      }
        if (!SeleniumDriverInstance.clientAddressActionLinks("Home Address", "Edit")) {
            return false;
        }

        SeleniumDriverInstance.switchToFrameById((EKunduCreateCorporateClientPage.AddressFrameId()));

        SeleniumDriverInstance.clearTextById(EKunduCreateCorporateClientPage.AddressLine1TextBoxId());

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EKunduCreateCorporateClientPage.AddressLine1TextBoxId(),
                "12 Leerdam Street")) {
            return false;
        }

         SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.ArrowdownToElementById(EKunduCreateCorporateClientPage.AddressLine1TextBoxId())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.UpdateAddressButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath("/html/body/div[2]/div[1]/button/span[1]")) {
            //return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.updateClientButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Home address edited successfully", false);

        return true;

    }

    private boolean deleteHomeAddress() {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.editClientButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduCreatePersonalClientPage.AddressTabPartialLinkText())) {
            return false;
        }

        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.clientAddressActionLinks("Home Address", "Delete")) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Home address deleted successfully", true);

        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.updateClientButtonId())) {
            return false;
        }

        return true;

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
