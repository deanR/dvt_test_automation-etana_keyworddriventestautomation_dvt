/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduEHATBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author deanR
 */
public class EkunduEHATBusinessAllRiskTest extends BaseClass
{
   TestEntity testData;
    TestResult testResultt;
    
    public EkunduEHATBusinessAllRiskTest(TestEntity testData)
    {
        this.testData = testData;
    }  
    
    public TestResult executeTest()
    {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();
        
        if(!verifyAddRiskButtonisPresent())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk button is present", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to verify that the add risk button is present - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterBusinessRiskItemDetailsCellPhones())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Business Risk Item details for Cellular Phones", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Business Risk Item details for Cellular Phones - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterBusinessRiskItemDetailsLaptops())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Business Risk Item details for Laptops", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Business Risk Item details for Laptops - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!Extensions())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Extensions", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Extensions - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterEndorsements())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Endorsements", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Endorsements - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterFACPropPlacementData())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter FAC Prop Placement data", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter FAC Prop Placement data - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        return new TestResult(testData,Enums.ResultStatus.PASS, "EHAT Business All Risk details entered successfully", this.getTotalExecutionTime());
    }
    
    
     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
     
    private boolean verifyAddRiskButtonisPresent()
    {
        if(SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
        {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectBusinessAllRisk();
            return true;
        }
        else if(SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            selectBusinessAllRisk(); 
            return true;
        }
        else
        {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");
            return false;
        }
 
    }
    
    private boolean selectBusinessAllRisk()
    {
         
         if(!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
        {
            return false;
        }

        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return false;
        }
            SeleniumDriverInstance.pause(1000);
            
            SeleniumDriverInstance.takeScreenShot("Business All Risk selected sucessfully", false);
            
            return true;
    }
    
     private boolean enterBusinessRiskItemDetailsCellPhones()
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduEHATBusinessAllRiskPage.RiskItemsAlarmWarrantyDropdownListId(), this.getData("Alarm Warranty")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyName(EkunduEHATBusinessAllRiskPage.BusinessAllRiskAddButtonName()))
        {
            return false;
        }
        
        //clickAddRiskItemButton();
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduEHATBusinessAllRiskPage.ItemCategoryDropdownListId(), this.getData("Item 1 Category")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.ItemDetailDescriptionTextboxId(), this.getData("Item 1 Description")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.ItemDetailSerialNumberTextboxId(), this.getData("Item 1 Serial Number")))
        {
            return false;
        }
        
            SeleniumDriverInstance.clearTextById(EkunduEHATBusinessAllRiskPage.ItemDetailDateAddedTextboxId());
        
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduEHATBusinessAllRiskPage.ItemDetailDateAddedTextboxId(), this.getData("Item 1 Date Added")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.ItemDetailSumInsuredTextboxId(), this.getData("Item Sum Insured 1")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.ItemDetailMaxRITextboxId(), this.getData("Max RI Exposure 1")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.ItemDetailRateTextboxId(), this.getData("Item Agreed Rate 1")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEHATBusinessAllRiskPage.FAPcheckboxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.FAPMinPercentageTextboxId(), this.getData("FAP Min Percentage 1")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.FAPMinAmountTextboxId(), this.getData("FAP Min Amount 1")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.FAPMaxAmountTextboxId(), this.getData("FAP Max Amount 1")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyName(EkunduEHATBusinessAllRiskPage.InterestedPartiesAddButtonName()))
        {
            return false;
        }
        
        //this.clickAddRiskItemButton(); 
        
         if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.InterestedPartiesTextboxId(), this.getData("Interested Party Name 1")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
        {
            return false;
        } 
         
         if(!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
        {
            return false;
        } 
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.BusinessAllRiskItemNotes1TextboxId(), this.getData("Business All Risk Item Notes")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEHATBusinessAllRiskPage.BusinessAllRiskItemPolicyNotesCheckboxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.BusinessAllRiskItemNotes2TextboxId(), this.getData("Business All Risk Item Notes")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
        {
            return false;
        } 
        
        SeleniumDriverInstance.takeScreenShot("Cellphone item Details entered successfully", false);
        
        return true;
    }
     
     private boolean enterBusinessRiskItemDetailsLaptops()
    {
//        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduEHATBusinessAllRiskPage.RiskItemsAlarmWarrantyDropdownListId(), this.getData("Alarm Warranty")))
//        {
//            return false;
//        }
        
        if(!SeleniumDriverInstance.clickElementbyName(EkunduEHATBusinessAllRiskPage.BusinessAllRiskAddButtonName2()))
        {
            return false;
        }
        
        //clickAddRiskItemButton();
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduEHATBusinessAllRiskPage.ItemCategoryDropdownListId(), this.getData("Item 2 Category")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.ItemDetailDescriptionTextboxId(), this.getData("Item 2 Description")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.ItemDetailSerialNumberTextboxId(), this.getData("Item 2 Serial Number")))
        {
            return false;
        }
        
        SeleniumDriverInstance.clearTextById(EkunduEHATBusinessAllRiskPage.ItemDetailDateAddedTextboxId());
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.ItemDetailDateAddedTextboxId(), this.getData("Item 2 Date Added")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.ItemDetailSumInsuredTextboxId(), this.getData("Item Sum Insured 2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.ItemDetailMaxRITextboxId(), this.getData("Max RI Exposure 2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.ItemDetailRateTextboxId(), this.getData("Item Agreed Rate 2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEHATBusinessAllRiskPage.FAPcheckboxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.FAPMinPercentageTextboxId(), this.getData("FAP Min Percentage 2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.FAPMinAmountTextboxId(), this.getData("FAP Min Amount 2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.FAPMaxAmountTextboxId(), this.getData("FAP Max Amount 2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyName(EkunduEHATBusinessAllRiskPage.InterestedPartiesAddButtonName()))
        {
            return false;
        }
        
        //this.clickAddRiskItemButton(); 
        
         if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.InterestedPartiesTextboxId(), this.getData("Interested Party Name 2")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
        {
            return false;
        } 
         
         if(!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
        {
            return false;
        } 
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.BusinessAllRiskItemNotes1TextboxId(), this.getData("Business All Risk Item Notes")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEHATBusinessAllRiskPage.BusinessAllRiskItemPolicyNotesCheckboxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.BusinessAllRiskItemNotes2TextboxId(), this.getData("Business All Risk Item Notes")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
        {
            return false;
        } 
        
        SeleniumDriverInstance.takeScreenShot("Laptop item Details entered successfully", false);
        
        return true;
    }
     
     private boolean Extensions()
     {
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEHATBusinessAllRiskPage.ExtensionsClaimscheckboxId(), true))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEHATBusinessAllRiskPage.ExtensionsRiotscheckboxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.ExtensionsLimitOfIndemnityTextboxId(), this.getData("Extensions Limit Of Indemnity")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.ExtensionsRateTextboxId(), this.getData("Extensions Rate")))
        {
            return false;
        }
         
            SeleniumDriverInstance.takeScreenShot("Extensions entered successfully", false);
         
        if(!SeleniumDriverInstance.clickElementById(EkunduEHATBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
        {
            return false;
        }
         
         return true;
     }
     
      private boolean enterEndorsements()
     {
         if(!SeleniumDriverInstance.clickElementById(EkunduEHATBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduEHATBusinessAllRiskPage.EndorsementsSelectButtonId()))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduEHATBusinessAllRiskPage.EndorsementsAddAllButtonId()))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduEHATBusinessAllRiskPage.EndorsementsApplyButtonId()))
        {
            return false;
        }
         
            SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);
         
         if(!SeleniumDriverInstance.clickElementById(EkunduEHATBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.BusinessAllRiskItemNotes1TextboxId(), this.getData("Business All Risk Item Notes")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduEHATBusinessAllRiskPage.BusinessAllRiskItemPolicyNotesCheckboxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduEHATBusinessAllRiskPage.BusinessAllRiskItemNotes2TextboxId(), this.getData("Business All Risk Item Notes")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduEHATBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
        {
            return false;
        }
         
         return true;
         
         
     }
     
     private boolean enterFACPropPlacementData()
       {
           if(!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
           {
               return false;
           }
           
           //SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId());
           
           if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), this.getData("Reinsurance Details - FAC Placement Reinsurer Code 1")))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
           {
               return false;
           }
           
               SeleniumDriverInstance.pause(3000);
        
        if(!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
           {
               return false;
           }
        
         if(!SeleniumDriverInstance.clickElementbyPartialLinkText("Reinsurances"))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduEHATBusinessAllRiskPage.AddisonReinsurancePercTextBoxId(), this.getData("Reinsurance Details - FAC Sum Insured")))
           {
               return false;
           }
           
            if(!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
           {
               return false;
           }
            
            SeleniumDriverInstance.pause(4000);
            
            System.out.println("Company 1 added successfully");
           
                SeleniumDriverInstance.takeScreenShot("FAC Proc Placement data entered successfully", false);
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId()))
           {
               return false;
           }
           
           return true;
       }   
}
