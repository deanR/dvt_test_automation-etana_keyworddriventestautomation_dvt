
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author deanR
 */
public class EkunduSASRIARenewalViaFindClient4_8Test extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduSASRIARenewalViaFindClient4_8Test(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToFindClientPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find  client page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the find  client page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findClient())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to find client", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterPolicyRenewalData())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter mid term adjustment data", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter mid term adjustment data- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!makePolicyLive())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to finalise client policy", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to finalise client policy- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Renewal selection completed succesfully", this.getTotalExecutionTime());
      }

    private boolean navigateToFindClientPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),
                EKunduHomePage.FindClientTabXPath()))
          {
            return false;
          }

        return true;
      }

    private boolean findClient()
      {
        String clientCode = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                                "Retrieved Client Code");

        if (clientCode.equals("parameter not found"))
          {
            clientCode = testData.TestParameters.get("Client Code");
          }
        else
          {
            testData.updateParameter("Client Code", clientCode);
          }
        
        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterPolicyRenewalData()
      {
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementbyLinkText("Details"))
          {
            return false;
          }

        return true;
      }

    private boolean makePolicyLive()
      {
//      if(SeleniumDriverInstance.checkRiskActionElement())
//       {
//           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SelectRoadSideAssistQuoteEditLinkId()))
//           {
//               return false;
//           }
//           
//       }
//       else 
//       {
//           if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(), "Edit"))
//           {
//               return false;
//           }
//       }
//      
//       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
//       {
//           return false;
//       }
//       
//       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
//  
//       
//       SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.scrollElementIntoViewById(EkunduViewClientDetailsPage.MakeLiveButtonId());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.MakeLiveButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Policy made live", false);

        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
