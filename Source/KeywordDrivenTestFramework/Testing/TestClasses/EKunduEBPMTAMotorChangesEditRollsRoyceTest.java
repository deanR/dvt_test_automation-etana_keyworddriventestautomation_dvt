/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author ferdinandN
 */
public class EKunduEBPMTAMotorChangesEditRollsRoyceTest extends BaseClass
{
    TestEntity testData;
    TestResult testResult;
    
    public EKunduEBPMTAMotorChangesEditRollsRoyceTest(TestEntity testData)
    {
        this.testData = testData;

    }
    
    public TestResult executeTest()
    {  
         SeleniumDriverInstance.DriverExceptionDetail = "";
         this.setStartTime();
         
         if(!navigateToFindClientPage())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to navigate to the find client page", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to navigate to the find client page - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
    
        if(!findClient())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to find client", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to find client - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
//        if(!verifyClientDetailsPageHasLoaded())
//        {
//            SeleniumDriverInstance.takeScreenShot( "Failed to verify that the client details page has loaded", true);
//            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the client details page has loaded - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//        }
        
        if(!enterMidTermAdjustmentData())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to enter Mid Term Adjustment data", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Mid Term Adjustment data - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }  
        
        
                
        if(!editRollsRoyce())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to enter Mid Term Adjustment data", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Mid Term Adjustment data - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        
      
            return new TestResult(testData, Enums.ResultStatus.PASS, "Motor Risk (Rolls Royce) edited successfully", this.getTotalExecutionTime());

        
}
    
    private boolean navigateToFindClientPage()
    {       
        if(!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),EKunduHomePage.FindClientTabXPath()))
        {
            return false;
        }
            return true;       
    }
    
    private boolean findClient()
    { 
            String clientCode = SeleniumDriverInstance.retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"), "Retrieved Client Code");
        
        if(clientCode.equals("parameter not found"))
        {
            clientCode = testData.TestParameters.get("Client Code");
        }
        else
            testData.updateParameter("Client Code", clientCode);
            
        if(!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Client Found", false);
        
        if(!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean verifyClientDetailsPageHasLoaded() 
    {
        if(!SeleniumDriverInstance.validateElementTextValueById(EkunduViewClientDetailsPage.ViewClientDetailsLabelId(), "View Client Details"))
        {
            return false;
        }
            SeleniumDriverInstance.takeScreenShot( "Client Details page successfully loaded", false);
        return true;
    }
    
    private boolean enterMidTermAdjustmentData()
    {
         if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText()))
        {
            return false;
        } 
         
        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                                  "Policy Reference Number");

        if (policyNumber.equals("parameter not found"))
          {
            policyNumber = testData.TestParameters.get("Policy Number");
          }
        else
          {
            testData.updateParameter("Policy Number", policyNumber);
          }
         
//         if(!SeleniumDriverInstance.changePolicy(policyNumber, "Change"))
//        {
//            return false;
//        }
        SeleniumDriverInstance.pause(1000);
        if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ChangePolicyLinkText()))
        {
            return false;
        } 
         
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MTATypeofChangeDropDownListId(), testData.TestParameters.get("Type of Change")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MIAEffectiveDateTextBoxId(), testData.TestParameters.get("Effective Date")))
        {
            return false;
        }
         
          if(!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId()))
        {
            return false;
        }
       
          return true;
    }
    
    private boolean editRollsRoyce()
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.EditRollsRoyceDropdownListXpath(), "Edit"))
        {
            return false;
        }
        
         if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
         
         System.out.println("Increasing Motor Cover Sum Insured...");
         
         if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleSumInsuredTextBoxId(),
        testData.TestParameters.get("Motor Cover Sum Insured")))
        {
          return false;
        }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
         
         if(testData.TestMethod.toUpperCase().equalsIgnoreCase("EPL - MTA - Edit Rolls Royce".toUpperCase()))
         {
            this.editEndorsements();
            
            if(!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddSpecifiedDriverButtonName()))
            {
                return false;
            }
        
                  
            System.out.println("Specified Drivers...");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverNameTextBoxId(),
            testData.TestParameters.get("Regular Driver Name")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.EPLSpecifiedDriverIDTypeDropdownListId(),  testData.TestParameters.get("ID Type")))
            {
               return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverIDNumberTextBoxId(),
            testData.TestParameters.get("Main Driver - ID Number")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
                return false;
            }
            
            if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
            {
                return false;
            }
            
            SeleniumDriverInstance.pause(3000);
            
            if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
          {
            return false;
          }
            
            
            SeleniumDriverInstance.pause(3000);
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_ReInsurance2007Cntrl_ddlReinsurance",
                "MS Own Damage (Section A)"))
          {
            return false;
          }
            
             SeleniumDriverInstance.pause(5000);
             
             if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
          {
            return false;
          }
             
            this.enterFACPropPlacementData();
            
         }
         
         else 
         {
             
             if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
            {
                return false;
            }
         }
         
            SeleniumDriverInstance.pause(3000);
            
            SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
            
            
            SeleniumDriverInstance.pause(3000);
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_ReInsurance2007Cntrl_ddlReinsurance",
                "MS Own Damage (Section A)"))
            {
                return false;
            }
            
            SeleniumDriverInstance.pause(5000);
            
            if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))
            {
                return false;
            }
        
            if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
            {
                return false;
            }
            
             SeleniumDriverInstance.pause(3000);
             
            SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

            SeleniumDriverInstance.pause(3000);
        
        return true;
         
       
    }
    
    private boolean editEndorsements()
    {
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsSelectButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.EndorsementsClauseSelecionDropdownListXpath(), testData.TestParameters.get("Endorsements - Clause to Remove")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.EndorsementsRemoveClauseButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsApplyButtonId()))
         {
             return false;
         }
         
         
         SeleniumDriverInstance.takeScreenShot("Endorsement details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        
            return true;
         
         
     }
    
    private boolean enterFACPropPlacementData()
       {

            if(! SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
            {
                return false;
            }

            SeleniumDriverInstance.pause(5000);

              if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), testData.TestParameters.get("Reinsurance Details - FAC Placement Reinsurer Code 1")))
              {
                  return false;
              }

              if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
              {
                  return false;
              }

              if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
              {
                  return false;
              }

              SeleniumDriverInstance.pause(5000);


               if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))
                {
                  return false;
                }

              if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
                {
                  return false;
                }


              if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceSumInsuredCompany2TextBoxId(), testData.TestParameters.get("Reinsurance Details - FAC Placement - Unallocated Amount")))
              {
                  return false;
              }

                 SeleniumDriverInstance.pause(5000);

               if(!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
              {
                  return false;
              }

               SeleniumDriverInstance.pause(5000);

               System.out.println("Company 1 added successfully");
               

               if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))
                {
                  return false;
                }

              if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
                {
                  return false;
                }
                                
           return true;
       }
    
    private boolean copyMotorPoloVivo()
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.EditPoloDropdownListXpath(), "Copy"))
        {
            return false;
        }
        
         SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.CopyRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.CopyRiskDropDownListId(),
                testData.TestParameters.get("Copy Risk - Risk Type")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.CopyRiskSubmitButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }
        
//        System.out.println("Adding copied risk to policy");
//        
//        SeleniumDriverInstance.Driver.navigate().refresh();
//
//        try
//        {
//            WebElement checkbox = SeleniumDriverInstance.Driver.findElement(By.id("ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl12_chkRiskSelect"));
//            JavascriptExecutor js = (JavascriptExecutor)  SeleniumDriverInstance.Driver;
//            js.executeScript("arguments[0].click();", checkbox);
//        }
//        catch(Exception e)
//        {
//            System.err.println("Failed to add risk to policy - " + e.getMessage());
//        }
//        
//       //if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.SelectRiskCheckboxXpath(), true))
////          {
////            return false;
////          }
//        
        SeleniumDriverInstance.Driver.navigate().refresh();
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(),
                "Edit"))
          {
            return false;
          }
       
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalLinesNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))
            {
              return false;
            }

          if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
            {
              return false;
            }
          
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        
//        if (!SeleniumDriverInstance.checkBoxSelectionById("ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl12_chkRiskSelect", true))
//          {
//            return false;
//          }
        
        
       
        SeleniumDriverInstance.pause(2000);
        
        
        
       SeleniumDriverInstance.takeScreenShot("Motor (Polo Vivo) copied successfully", false);
       
       return true;

    }
    
    private boolean deletePoloVivo()
    {
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.DeleteCopyofPoloVivoDropdownListXpath(), "Delete"))
        {
            return false;
        } 
       
       SeleniumDriverInstance.acceptAlertDialog();
       
       SeleniumDriverInstance.takeScreenShot("Motor (Polo Vivo) deleted successfully", false);
       
        SeleniumDriverInstance.pause(5000);
        
        return true;

    }
    }
