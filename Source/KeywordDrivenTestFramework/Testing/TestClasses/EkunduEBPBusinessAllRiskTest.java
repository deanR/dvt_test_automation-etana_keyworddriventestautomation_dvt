
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author FerdinandN
 */
public class EkunduEBPBusinessAllRiskTest extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public EkunduEBPBusinessAllRiskTest(TestEntity testData) {
        this.testData = testData;

    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk button is present", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify that the add risk button is present - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterBusinessRiskItemDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Business Risk Item details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Business Risk Item details - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        return new TestResult(testData, Enums.ResultStatus.PASS,
                "Business All Risk added successfully" + SeleniumDriverInstance.DriverExceptionDetail,
                this.getTotalExecutionTime());

    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean verifyAddRiskButtonisPresent() {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            System.out.println("Add risk button clicked.");

            return selectBusinessAllRiskRiskType();
        } else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return selectBusinessAllRiskRiskType();
        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

        }
    }

    private boolean selectBusinessAllRiskRiskType() {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Business All Risk Risk type selected successfully", false);

        return true;
    }

    private boolean enterBusinessRiskItemDetails() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduBusinessAllRiskPage.RiskItemsAlarmWarrantyDropdownListId(),
                this.getData("Alarm Warranty"))) {
            return false;
        }

        //this.clickAddRiskItemButton();
        if (!SeleniumDriverInstance.clickElementbyName(EkunduBusinessAllRiskPage.BusinessAllRiskIAddButtonName())) {
            return false;
        }
        
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduBusinessAllRiskPage.BusinessAllRiskItemCategoryDropdownListId(),
                this.getData("Business All Risk Item Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemSumInsuredTextboxId(),
                this.getData("Business All Risk Item Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemDescriptionTextboxId(),
                this.getData("Business All Risk Item Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemRateTextboxId(),
                this.getData("Business All Risk Item Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumCheckboxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumTextboxId(),
                this.getData("Risk Item -  Flat Premium"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Business Risk Item details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;

    }

    private boolean clickAddRiskItemButton() {//*[@id="ctl00_cntMainBody_BAR__ITEM"]/table/tfoot/tr/td[7]/input
        WebElement cell
                = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_BAR__ITEM\"]/table/tfoot/tr/td[10]/input[@value = \"Add\"]"));

        if (cell != null) {
            cell.click();
            System.out.println("[TestInfo] Add Risk Item Button Clicked");

            return true;
        } else {
            System.err.println("[Error] Failed to click add risk item button");

            return false;
        }

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
