
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;

/**
 *
 * @author deanR
 */
public class EkunduSASRIAPolicyRenewalTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduSASRIAPolicyRenewalTest(TestEntity testData)
      {
        this.testData = testData;
      }

    private boolean navigateToRenewalSelectionPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.PolicyHoverTabId(),
                EKunduHomePage.RenewalSelectionTabXPath()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.PolicyRenewalDivId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Navigation to the policy renewal page was successful", false);

        return true;
      }

    private boolean searchForPolicy()
      {
        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PolicyRefTextBoxId(),
                this.getData("Policy Reference Number")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.RenewalSelectionSelectPolicyResultsId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Policy Found", false);

        return true;
      }

    private boolean putPolicyIntoRenewalCycle()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RenewalSelectionSelectPolicyResultsId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.RenewalSelectionMessageDivId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Policy is now awaiting update", false);

        return true;
      }

    private boolean navigateToRenewalManagerPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.PolicyHoverTabId(),
                EKunduHomePage.RenewalManagerTabXPath()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.PolicyRenewalDivId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Navigation to the policy renewal page was successful", false);

        return true;
      }

    private boolean updatePolicy()
      {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.RenewalManagerRenewalStatusDropDownListId(),
                this.getData("Renewal Status")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.RenewalManagerProductTypeDropDownListId(),
                this.getData("Product Type")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.RenewalManagerBranchDropDownListId(),
                this.getData("Branch")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RenewalManagerSearchButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RenewalManagerCheckPolicyCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RenewalManagerStatusButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.RenewalStatusFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.RenewalStatusDropDownListId(),
                this.getData("Renewal Status")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RenewalManagerUpdateStatusButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.acceptAlertDialog();

        SeleniumDriverInstance.switchToDefaultContent();

        return true;

      }
    
     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

  }


//~ Formatted by Jindent --- http://www.jindent.com
