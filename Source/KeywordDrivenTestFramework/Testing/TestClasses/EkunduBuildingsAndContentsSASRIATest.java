
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduBuildingsAndContentsSASRIATest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduBuildingsAndContentsSASRIATest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!verifyRisksDialogisPresent())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the risks dialod is presenet"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to select risk type - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!addSurveyType())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter survey type"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter survey type - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterRiskCoverage())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk coverage"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk coverage - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterBuildingsDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter building details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter building details- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }


        if (!enterEndorsements())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter endorsements"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter endorsements- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Buildings and Contents risk added successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Buildings and Contents risk added successfully",
                              this.getTotalExecutionTime());

      }

    private boolean verifyRisksDialogisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());

            selectDomesticandPropertyRiskType();

            return true;
          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

        
      }
    
    

    private boolean selectDomesticandPropertyRiskType()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(testData.TestParameters.get("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Domestic and Property Risk type selected successfully", false);
        
        return true;
      }

    private boolean addSurveyType()
      {

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterRiskCoverage()
      {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.CheckBuildingCheckBoxId(),
                true))
          {
            return false;
          }

        
        if (!SeleniumDriverInstance.uncheckCheckBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.BuildingsandContentsLegalCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.uncheckCheckBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.BuildingsandContentsliabilityCheckBoxId(),
                true))
          {
            return false;
          }


        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TypeofConstructionDropdownListId(),
                testData.TestParameters.get("Type of Construction")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk coverage entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterBuildingsDetails()
      {
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BuildingSumInsuredTextBoxId(),
                testData.TestParameters.get("Building Sum Insured")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsRateButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Building details entered successfully", false);
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterEndorsements()
      {
          
        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementNextButtonId());

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementNextButtonId());


        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;

      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
