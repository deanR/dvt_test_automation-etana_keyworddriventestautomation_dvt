
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduGlassRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author ferdinandN
 */
public class SASRIAScenario4Test extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public SASRIAScenario4Test(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!verifyRisksDialogisPresent())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the risks dialod is present"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to select risk type - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterDomesticAndPropertyRiskData())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the risks dialod is present"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to select risk type - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot("Domestic and Property risk added successfully - ", false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Domestic and Property risk added successfully",
                              this.getTotalExecutionTime());
      }

    private boolean verifyRisksDialogisPresent()
      {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectDomesticandPropertyRiskType();

            return true;
          }
        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectDomesticandPropertyRiskType();
          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

        return true;

      }

    private boolean selectDomesticandPropertyRiskType()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(10000);

        SeleniumDriverInstance.takeScreenShot("Domestic and Property Risk type selected successfully", false);

        return true;
      }

    private boolean enterDomesticAndPropertyRiskData()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.CheckBuildingCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.CheckContentsCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.CheckAllRiskItemsCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TypeofResidenceDropdownListId(),
                testData.TestParameters.get("Type of Residence")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TypeofConstructionDropdownListId(),
                testData.TestParameters.get("Type of Construction")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.OccupancyDropdownListId(),
                testData.TestParameters.get("Occupancy")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.LocalityDropdownListId(),
                testData.TestParameters.get("Locality")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BuildingSumInsuredTextBoxId(),
                this.getData("Buildings - Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduGlassRiskPage.RiskItemRateButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ContentsSumInsuredTextBoxId(),
                testData.TestParameters.get("Contents Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NoClaimDiscountDropdownListId(),
                testData.TestParameters.get("Claim Discount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.SecurityDiscountsLevel5AlarmCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.ContentsAdjustmentReasonDropdownListId(),
                testData.TestParameters.get("Contents Adjustment Reason")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduGlassRiskPage.RiskItemRateButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        this.clickAddRiskItemButton();

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.DAPRiskItemCategoryDropdownListId(),
                this.getData("Risk Item - Category")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.DAPRiskItemDescriptionTestboxId(),
                this.getData("Risk Item - Description")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.DAPRiskItemSumInsuredTestboxId(),
                this.getData("Risk Item - Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduGlassRiskPage.RiskItemRateButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Overview", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.takeScreenShot("Domestic and Property risk data entered successfully", false);

        return true;
      }

    private boolean clickAddRiskItemButton()
      {
        WebElement cell =
            SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_ALL_RISKS__ALL_RISK_ITEM\"]/table/tfoot/tr/td[7]/input[@value = \"Add\"]"));

        if (cell != null)
          {
            cell.click();
            System.out.println("[TestInfo] Add Risk Item Button Clicked");

            return true;
          }
        else
          {
            System.err.println("[Error] Failed to click add risk item button");

            return false;
          }

      }
    
    public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

  }


//~ Formatted by Jindent --- http://www.jindent.com
