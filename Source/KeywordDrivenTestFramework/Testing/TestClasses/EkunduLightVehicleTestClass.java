/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author sncantswa
 */
public class EkunduLightVehicleTestClass extends BaseClass 
{
    TestEntity testData;
    TestResult testResultt;
    
    public EkunduLightVehicleTestClass(TestEntity testData)
    {
        this.testData = testData;
    }
    
    public TestResult executeTest()
    {
        this.setStartTime();
        
        if(testData.TestMethod.toUpperCase().contains("EBP-Light Vehicle".toUpperCase()))
         {
            //<editor-fold defaultstate="collapsed" desc="Light Vehicle">
            if (!verifyRisksDialogisPresent())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to verify Risks Dialogis Present"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            if (!RiskAddressInfor())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to verify Risk Address Infor"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            if (!addlightVehicle())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to add light Vehicle"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            if (!FirstAmountPayablepageSpecialType())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to add First Amount Payable page Special Type"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            if (!enterEndorsements())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to enter Endorsements"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            if (!specifiedDriverPage())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to enter specified Driver Page"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            if (!enterInterestedParties())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to enter Interested Parties"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            if (!enterInterestedPartyNotes())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to enter Interested Party Notes"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            if (!clickFinancialOverViewNextButton())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to click Financial Over View Next Button"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            if (!enterMotorSpecifiedNotes())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to enter Motor Specified Notes"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            if (!clickRatingDetails())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to click Rating Details"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            if (!clickReinsuranceDetails())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to click Reinsurance Details"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
             if (!clickSummaryOfCoverTab())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to click Summary Of Cover Tab"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the riss dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
           //</editor-fold >
         }
       
        SeleniumDriverInstance.takeScreenShot(("Ran Light Vehicle successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Light Vehicle added successfully", this.getTotalExecutionTime());
    }
    
    private boolean findClient()
    {
        String clientCode = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                                "Retrieved Client Code");

        if (clientCode.equals("parameter not found"))
          {
            clientCode = testData.TestParameters.get("Client Code");
          }
        else
          {
            testData.updateParameter("Client Code", clientCode);
          }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
          {
            return false;
          }

        return true;
      }  
    private String getData(String parameterName)
    {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu verify Risks Dialogis Present Methods">
    private boolean verifyRisksDialogisPresent()
    {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            selectMotorRiskType();

            return true;
          }

        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectMotorRiskType();

            return true;

          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu select Motor Risk Type Methods">
    private boolean selectMotorRiskType()
    {
        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Motor risk type selected successfully", false);

        return true;

      }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu Risk Adress page Methods">
     private boolean RiskAddressInfor()
     {
         //click the next button to continue
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
         {
            return false;
         }
         return true;
     }
    //</editor-fold>
     
    //<editor-fold defaultstate="collapsed" desc="Ekundu add Light Vehicle">
    private boolean addlightVehicle()
    {
        System.out.println("Vehicle Type...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu vehicle type Methods">
//        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
//          {
//            return false;
//          }
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SearchVehicleModelIconId()))
          {
            return false;
          }
        SeleniumDriverInstance.pause(3000);
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehiclesSourceDropdownListId(),
                testData.TestParameters.get("Source")))
          {
            return false;
          }
        SeleniumDriverInstance.pause(3000);
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleMakeDropdownListId(),
                testData.TestParameters.get("Make")))
          {
            return false;
          }
        SeleniumDriverInstance.pause(3000);
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleYearDropdownListId(),
                testData.TestParameters.get("Year")))
          {
            return false;
          }
       SeleniumDriverInstance.pause(3000);
         if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleModelDropdownListId(),
                testData.TestParameters.get("Model")))
          {
            return false;
          }
        SeleniumDriverInstance.pause(3000);
        //select vehicle from the list
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.VehicleModelSelectedRowId()))
          {
            return false;
          }
        SeleniumDriverInstance.pause(2000);
        //</editor-fold>
        
        System.out.println("Specified Vehicle Type...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu Specified Vehicle Type Methods">
        //select class of use
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorClassofUseDropdownListId(),
                testData.TestParameters.get("Class Of Use")))
          {
            return false;
          }
        SeleniumDriverInstance.pause(3000);
        //</editor-fold>
        
        System.out.println("Options...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu options Methods">
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.EndorsementsCheckBoxId(), true))
          {
            return false;
          }
            
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesCheckBoxId(), true))
         {
            return false;
         }
            
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriversCheckBoxId(), true))
         {
            return false;
         }
        //click next button
         if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
          {
            return false;
          }
         //</editor-fold>
         
        System.out.println("Vehicle Cover Details...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu Vehicle Cover Details Methods">
         //enter area code
         if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AreaCodeTextboxId(),
                  testData.TestParameters.get("Area Code")))
          {
            return false;
          }
        //select tracking device
         if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorTrackingDeviceDropdownListId(),
                  testData.TestParameters.get("Tracking Device")))
          {
            return false;
          }
         
        //</editor-fold>
        
        System.out.println("Registration...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu vehicle type Methods">
         if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EngineNumberTextboxId(),
                testData.TestParameters.get("Engine Number")))
         {
            return false;
         }
          if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ChassisNumberTextboxId(),
                testData.TestParameters.get("Chassis Number")))
         {
            return false;
         }
         if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationNumberTextboxId(),
                testData.TestParameters.get("Registration Number")))
         {
            return false;
         }
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleRegOwnerTextBoxId(),
                testData.TestParameters.get("Registered Owner")))
         {
            return false;
         }
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationDateTextboxId(),
                testData.TestParameters.get("Registration Date")))
         {
              return false;
         }
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NATISCodeDropdownListId(),
                testData.TestParameters.get("NATIS Code")))
         {
              return false;
         }
        //</editor-fold>
         
        System.out.println("Motor Cover...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu Motor Cover Methods">
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleCoverTypeDropdownListId(),
                testData.TestParameters.get("Cover Type")))
         {
            return false;
         }
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BasisofSettlementDropdownListId(),
                testData.TestParameters.get("Basis of Settlement")))
         {
              return false;
         }
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddEquipmentButtonName()))
         {
            return false;
         }
        //</editor-fold >
        
        System.out.println("Accessories and equipment...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu Accessories and equipment Methods">
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessoryDescriptionTextBoxId(),
            testData.TestParameters.get("Accessory Description")))
         {
            return false;
         }
       
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessorySumInsuredTextBoxId(),
            testData.TestParameters.get("Accessory Sum Insured")))
         {
            return false;
         }
            
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AccessoryFinishButtonID()))
        {
            return false;
        }
        //</editor-fold>
        
        System.out.println("Extensions...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu Extensions Methods">
        System.out.println("Car Hire...");
        //<editor-fold defaultstate="collapsed" desc="Car Hire">
        //check Car Hire
        if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsEBPMotorChangesCarHireCheckBoxId(), true))
         {
            return false;
         }
        //enter Car Hire limit of indemnity
        if  (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.CarHireLimitofIndemnityDropdownListId(),
                testData.TestParameters.get("Car Hire Limit of indemnity")))
         {
            return false;
         }
        //select Car Hire condition
        if  (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.CarHireConditionsDropdownListId(),
                testData.TestParameters.get("Car Hire Limit Condition")))
         {
            return false;
         }
        //click buttn rate
        if  (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId()))
         {
            return false;
         }
      //</editor-fold >
        System.out.println("Contigent Liability...");
        //<editor-fold defaultstate="collapsed" desc="Contigent Liability">
        //check Contigent Liability
        if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsContingentLiabilityCheckBoxId(), true))
         {
            return false;
         }
        //enter limit of indemnity
        if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsContingentLiabilityLimitofIndemnityTextboxId(), 
                testData.TestParameters.get("Contingent liability Limit of Indemnity")))
         {
            return false;
         }
        //enter lContigent Liability rate
        if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsContingentLiabilityRateTextboxId(),
                testData.TestParameters.get("Contingent Liability Rate")))
         {
            return false;
         }
        //click buttn rate
        if  (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId()))
         {
            return false;
         }
      //</editor-fold >
        System.out.println("Credit Shortfall...");
        //<editor-fold defaultstate="collapsed" desc="Credit Shortfall">
        //check Credit Shortfall
        if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsCreditShortallCheckBoxId(), true))
         {
            return false;
         }
        //enter Credit Shortfall rate
        if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsCreditShortfallRateTextboxId(),
                testData.TestParameters.get("Credit Shortfall Rate")))
         {
            return false;
         }
        //click buttn rate
        if  (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId()))
         {
            return false;
         }
      //</editor-fold>
        System.out.println("Excess Waiver...");
        //<editor-fold defaultstate="collapsed" desc="Excess Waiver">
        //check excess waiver
        if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsExcessWaiverCheckBoxId(), true))
         {
            return false;
         }
        //enter Excess Waiver premium
         if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsExcessWaiverPremiumTextboxId()
                ,testData.TestParameters.get("Excess Waiver Premium")))
         {
            return false;
         }
        //click buttn rate
        if  (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId()))
         {
            return false;
         }
      //</editor-fold>
        System.out.println("Fire and explosion...");
        //<editor-fold defaultstate="collapsed" desc="Fire and Explosion">
        //enter fire and explosion Rate
         if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsFireAndexplosionRateTextboxId(),
                testData.TestParameters.get("Fire & Explosion Rate")))
        {
            return false;
        }
        //enter fire and explosion Premium
        if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsFireAndexplosionPremiumTextboxId(),
                testData.TestParameters.get("Fire & Explosion Premium")))
        {
            return false;
        }
        //click buttn rate
        if  (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId()))
         {
            return false;
         }
        //</editor-fold>
        System.out.println("Loss of keys...");
        //<editor-fold defaultstate="collapsed" desc="Loss of keys">
        //check Loss of keys
        if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsLossofKeysCheckBoxId(), true))
         {
            return false;
         }
        //enter Loss of keys limit of indemnity
        if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsLossofKeysLimitofIndemnityTextboxId(), 
                testData.TestParameters.get("Loss of Key Limit of Indemnity")))
        {
            return false;
        }
        //enter Loss of keys Premium
        if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsLossofKeysPremiumTextboxId(), 
                testData.TestParameters.get("Loss of Key Premium")))
        {
            return false;
        }
        //click buttn rate
        if  (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId()))
         {
            return false;
         }
         //enter Loss of keys FAP Amount
        if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.LossOfKeyFAPAmount(), 
                testData.TestParameters.get("Loss of Key FAP Amount")))
        {
            return false;
        }
        //</editor-fold>
        System.out.println("Packing Facility...");
        //<editor-fold defaultstate="collapsed" desc="Packing Facility">
        //check Packing Facility
        if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsParkingFacilitiesCheckBoxId(), true))
         {
            return false;
         }
        //enter Packing Facility limit indemnity
        if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsParkingFacilitiesLimitofIndemnityTextboxId(),
                 testData.TestParameters.get("Parking Facilities Limit of Indemnity")))
        {
            return false;
        }
        //</editor-fold>
        System.out.println("Riot and Strike...");
        //<editor-fold defaultstate="collapsed" desc="Riot and Strike">
        //check Riot and Strike
        if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsRiotandStrikeCheckBoxId(),
                true))
         {
            return false;
         }
        //</editor-fold>
        System.out.println("Road Side Assistance...");
        //<editor-fold defaultstate="collapsed" desc="Road Side Assistance">
        //check Road Side Assistance
        if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsRoadsideAssistCheckBoxId(), true))
         {
            return false;
         }
        //</editor-fold>
        System.out.println("Theft of car radio...");
        //<editor-fold defaultstate="collapsed" desc="Theft of car radio">
        //check Theft of car radio
        
        if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsTheftofCarRadioCheckBoxId(), true))
         {
            return false;
         }
         //enter Theft of car radio rate
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsTheftofCarRadioRateTextboxId(), 
                 testData.TestParameters.get("Theft of Car Radio Rate"))) {
            return false;
        }
        //Click Rate button.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId())) {
            return false;
        }
        //</editor-fold>
        System.out.println("Third party liability...");
        //<editor-fold defaultstate="collapsed" desc="Third party liability">
        //enter Third party Rate
        if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsThirdPartyLiabilityRateTextboxId(), 
              testData.TestParameters.get("Third Party Liability Rate")))
        {
            return false;
        }
        //enter Third party Premium
        if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ThirdPartyLiabilityPremiumMotorChangesTextBoxId(),
              testData.TestParameters.get("Third Party Premium")))
        {
            return false;
        }
        //click buttn rate
        if  (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId()))
         {
            return false;
         }
        //</editor-fold>
        System.out.println("Voluntary Excess...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu Voluntary Excess Methods">
     
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryExcessPercentageTextBoxId(),
                testData.TestParameters.get("Voluntary Excess Percentage")))
         {
            return false;
         } 
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryMinimumAmountTextBoxId(),
                testData.TestParameters.get("Voluntary Minimum Amount")))
         {
            return false;
         } 
        //</editor-fold>
        System.out.println("Additional Compulsory Excess...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu Additional Compulsory Excess Methods">

        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryExcessPercentageTextBoxId(),
                testData.TestParameters.get("Compulsory Excess Percentage")))
            {
              return false;
            }
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryMinimumAmountTextBoxId(),
                testData.TestParameters.get("Compulsory Minimum Amount")))
            {
              return false;
            }
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
        //</editor-fold>
        
        System.out.println("Wreckage Removal...");
        //<editor-fold defaultstate="collapsed" desc="Wreckage Removal">
        //check Wreckage Removal
//        if  (!SeleniumDriverInstance.checkBoxSelectionById(null, true))
//        {
//            return false;
//        }
//        //enter Wreckage Removal limited liability
//        if  (!SeleniumDriverInstance.enterTextById("", ""))
//        {
//            return false;
//        }
//        //enter Wreckage Removal rate
//        if  (!SeleniumDriverInstance.enterTextById("", ""))
//        {
//            return false;
//        }
//        //click buttn rate
//        if  (!SeleniumDriverInstance.clickElementById(""))
//         {
//            return false;
//         }
        //</editor-fold>
        System.out.println("main Driver...");
        //<editor-fold defaultstate="collapsed" desc="Ekundu main Driver">
//        if (!SeleniumDriverInstance.enterTextById("",testData.TestParameters.get("Driver Name")))
//          {
//            return false;
//          }
//         if (!SeleniumDriverInstance.enterTextById("",testData.TestParameters.get("License Issue")))
//          {
//            return false;
//          }
//        SeleniumDriverInstance.pause(3000);
//        //click next
//         if (!SeleniumDriverInstance.clickElementById(""))
//          {
//            return false;
//          }
        //</editor-fold>
        //</editor-fold>
        return true;
    } 
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu First Amount Payable Special Type car page Methods">
    private boolean FirstAmountPayablepageSpecialType()
    {
      System.out.println("Own Damage...");
      //<editor-fold defaultstate="collapsed" desc="Own Damage">
      //enter Own Damage maximum%
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPOwnDamageMaxPercentageTextBoxId(),
              testData.TestParameters.get("Own Damage Maximum Per")))
        {
            return false;
        }
      //enter Own Damage maximum amount
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPOwnDamageMaxAmountTextBoxId(),
              testData.TestParameters.get("Own Damage Maximum Amount")))
        {
            return false;
        }
      //</editor-fold >
      System.out.println("third party...");
      //<editor-fold defaultstate="collapsed" desc="third party">
      //enter third party minimum%
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPThirdPartyMinPercetage(), 
              testData.TestParameters.get("Third Party Minimum Per")))
        {
            return false;
        }
      //enter third party maximum%
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPThirdPartyMaxPercetage(),  
              testData.TestParameters.get("Third Party Maximum Per")))
        {
            return false;
        }
      //enter third party minimum amount
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPThirdPardyMinimumAmountTextBoxId(), 
              testData.TestParameters.get("Third Party Minimum Amount")))
        {
            return false;
        }
      //enter third party maximum amount
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPThirdPardyMaxAmountTextBoxId(),
              testData.TestParameters.get("Third Party Maximum Amount")))
        {
            return false;
        }
      //</editor-fold>
      System.out.println("Fire and Explosion...");
      //<editor-fold defaultstate="collapsed" desc="Fire and Explosion">
      //enter fire and explosion minimum%
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPFireandExplosionMinPercTextBoxId(), 
              testData.TestParameters.get("Fire and Explosion Minimum Per")))
        {
            return false;
        }
      //enter fire and explosion maximum%
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPFireandExplosionMaxPercTextBoxId(), 
              testData.TestParameters.get("Fire and Explosion Maximum Per")))
        {
            return false;
        }
      //enter fire and explosion minimum amount
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPTFireandExplosionTextboxId(), 
               testData.TestParameters.get("Fire and Explosion Minimum Amount")))
        {
            return false;
        }
      //enter fire and explosion maximum amount
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPFireandExplosionMaxAmountTextBoxId(),  
              testData.TestParameters.get("Fire and Explosion Maximum Amount")))
        {
            return false;
        }
       //</editor-fold>
      System.out.println("Theft Hijack...");
      //<editor-fold defaultstate="collapsed" desc="Theft Hijack">
       //check Theft Hijack minimum%
     if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.FAPAdditionalTheftCheckBoxId(),
              true))
        {
            return false;
        }
      //enter Theft Hijack minimum%
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPAdditionalTheftMinPecentageTextBoxId(),  
              testData.TestParameters.get("Theft Hijack Minimum Per")))
        {
            return false;
        }
      //enter Theft Hijack maximum%
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AdditionalTheftMaxPercetageTextBoxID(),  
              testData.TestParameters.get("Theft Hijack Minimum Per")))
        {
            return false;
        }
      //enter Theft Hijack minimum amount
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPAdditionalTheftMinAmountTextBoxId(), 
              testData.TestParameters.get("Theft Hijack Minimum Amount")))
        {
            return false;
        }
      //enter Theft Hijack maximum amount
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPHijackMaxAmountTextBoxId(),  
              testData.TestParameters.get("Theft Hijack Maximum Amount")))
        {
            return false;
        }
       //</editor-fold>
      //click next amount payable
      if  (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BasicDetailsNextButtonName()))
        {
            return false;
        }
       return true;
     }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu endossments page methods">
    private boolean enterEndorsements()
    {
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementsSelectButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementsAddAllButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementsApplyButtonId()))
         {
             return false;
         }
         
         
         SeleniumDriverInstance.takeScreenShot("Endorsement details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementNextButtonId()))
         {
             return false;
         }
            return true;
 
     }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu specified driver Methods">
    private boolean specifiedDriverPage()
    {
     System.out.println("Driver1...");
      //<editor-fold defaultstate="collapsed" desc="driver">
       //click add driver button
      if  (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddSpecifiedDriverButtonName()))
        {
            return false;
        }
      //enter driver name
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverNameTextBoxId(),
              testData.TestParameters.get("Driver")))
        {
            return false;
        }
      //select id type
      if  (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverIDTypeDropdownId(),
              testData.TestParameters.get("ID Type")))
        {
            return false;
        }
      //enter id number
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverIDNumberTextBoxId(), 
              testData.TestParameters.get("ID Number")))
        {
            return false;
        }
      //click next driver button
      if  (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BasicDetailsNextButtonName()))
        {
            return false;
        }
       //</editor-fold>
      //click next driver button
      if  (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BasicDetailsNextButtonName()))
        {
            return false;
        }
      
      //click next driver button
      if  (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.MSAddInterestedPartiesButtonName()))
        {
            return false;
        }
      return true;
    }
    //</editor-fold> 
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu enter Interested Parties Methods">
    private boolean enterInterestedParties()
    {
          //SeleniumDriverInstance.Driver.navigate().refresh();
          if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.InterestedPartyInstitutionNameDropdownListId(),
                testData.TestParameters.get("Institution Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MSTypeofAgreementTypeDropdownListId(),
                testData.TestParameters.get("Type Of Agreement")))
          {
            return false;
          }
         //enter Description
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EBPMotorIPDescriptionTextBoxID(), 
             testData.TestParameters.get("Description")))
        {
            return false;
        }
      if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Interested parties details entered successfully", false);
        return true;
        
      }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu enter Interested Party Notes Methods">
    private boolean enterInterestedPartyNotes()
    { 
        //enter Interested Party comment
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.NotesCommentsTextboxId(),
                testData.TestParameters.get("Interested Party Comment")))
         {
             return false;
         }
        
        //check add notes to policy
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.NotesAddNotesCheckboxId(), true))
         {
             return false;
         }
        //enter Interested Party printed and schedule
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.NotesNotesTexBboxId(),
                testData.TestParameters.get("Interested Parties Notes")))
         {
             return false;
         }
        
         SeleniumDriverInstance.takeScreenShot("interested parties notes entered successfully", false);
        
         //click intrested party next
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
         {
             return false;
         }
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
         {
             return false;
         }  
        return true;      
    }
     //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu click Financial Over View Next Methods">
     private boolean clickFinancialOverViewNextButton()
     {
        System.out.println("Adjustment...");
         //select premium adjusment basis
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.AdjustmentBasisDropdownListId(),
                  testData.TestParameters.get("Premium Adjustment Basis")))
         {
            return false;
         }
         //enter adjustment percentage
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MotorSpecifiedAdjustmentPercentageTextBoxId()
                 ,testData.TestParameters.get("Adjustment Percentage")))
         {
            return false;
         }
         //click the next button
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BasicDetailsNextButtonName()))
         {
            return false;
         }
         return true;
     }
     //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu enter MotorS pecified Notes Methods">
    private boolean enterMotorSpecifiedNotes()
    { 
        //enter Motor Specified comment
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesCommentsTextAreaId()
                , testData.TestParameters.get("Specified Notes Comment")))
         {
             return false;
         }
        //check add notes to policy
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.NotesCheckBoxId(), true))
         {
             return false;
         }
        //enter Motor Specified printed and schedule
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.NotesTextAreaID(),
                testData.TestParameters.get("Specified Party Notes")))
         {
             return false;
         }
         //click the next button  
         SeleniumDriverInstance.takeScreenShot("Motor Specified notes entered successfully", false);
        
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BasicDetailsNextButtonName()))
         {
             return false;
         } 
        return true;      
    }
     //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu click Rating Details Methods">
     private boolean clickRatingDetails()
     {
         //click Rating details tab
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ratingDetailsTabId()))
         {
             return false;
         } 
         return true;
     }
     //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Ekundu click Reinsurance Details Methods">
     private boolean clickReinsuranceDetails()
     {
         //click Reinsurance details tab
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabId()))
         {
             return false;
         } 
          //click Reinsurance details tab
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BandDropdownListId(),
                 testData.TestParameters.get("Reinsurance Band1")))
         {
             return false;
         }
         SeleniumDriverInstance.pause(2000);
         //click Reinsurance details tab
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabId()))
         {
             return false;
         }
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BandDropdownListId(),
                 testData.TestParameters.get("Reinsurance Band2")))
         {
             return false;
         }
          SeleniumDriverInstance.pause(2000);
          //click Reinsurance details tab
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabId()))
         {
             return false;
         }
         //click Reinsurance Add FAC XOL 
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceOkButtonId()))
         {
             return false;
         } 
         return true;
     }
    //</editor-fold>
     
    //<editor-fold defaultstate="collapsed" desc="Ekundu click Summary Of Cover Tab Methods">
     private boolean clickSummaryOfCoverTab()
     {
         //click fees details tab
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabId()))
         {
             return false;
         } 
         //click commision details tab
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.commisionDetailsTabId()))
         {
             return false;
         } 
        System.out.println("Debit Order Details...");
        //<editor-fold defaultstate="collapsed" desc="debit orders Methods">
         //click debit orders tab
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.DebitOrderDetailsTabId()))
         {
             return false;
         } 
         //select bank account 
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.selecteBankAccountDropdownListTabId(),
                 testData.TestParameters.get("Bank Account")))
         {
             return false;
         }
         SeleniumDriverInstance.pause(3000);
        //</editor-fold>
        System.out.println("Documents...");
        //<editor-fold defaultstate="collapsed" desc="Documents Methods">
         //click document tab
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.documentDetailsTabId()))
         {
             return false;
         } 
        //click generate pdf
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.generatePdfDocuemntListTabId()))
         {
             return false;
         } 
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.clickPopUpOkByID()))
         {
             return false;
         }
        //click generate doc
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.GenerateDocButtonId()))
         {
             return false;
         } 
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.clickPopUpOkByID()))
         {
             return false;
         }
        //click make live
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MakePolicyLiveButtonId()))
         {
             return false;
         } 
        //</editor-fold>
         return true;
     }
    //</editor-fold>
}
