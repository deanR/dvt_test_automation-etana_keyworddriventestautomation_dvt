/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

/**
 *
 * @author ferdinandN
 */
public class EkunduSADCMotorSpecifiedRiskTest extends BaseClass
{
    TestEntity testData;
    TestResult testResultt;
    
    public EkunduSADCMotorSpecifiedRiskTest(TestEntity testData)
    {
        this.testData = testData;
    }  
    
     public TestResult executeTest()
    {  
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();
        if(!verifyRisksDialogisPresent())
        {
           SeleniumDriverInstance.takeScreenShot("Failed to verify that the risks dialog is present", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to verify that the risks dialog is present - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterVehicleTypeDetails())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter vehicle type details", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter vehicle type details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterAccessories())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Motor Accessories", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Motor Accessories - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }      
    
        if(!enterMotorDetails())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Motor details", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Motor details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterVehicleRegistrationDetails())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Vehicle registration details", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Vehicle registration details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterSpecifiedDriverDetails())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Specified Driver details", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Specified Driver details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterEndorsements())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Motor Endorsements", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Motor Endorsements - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterNotes())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Motor Notes", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Motor Notes - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
            SeleniumDriverInstance.takeScreenShot("SADC - Motor Specified risk added successfully", false);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "SADC - Motor Specified risk added successfully", this.getTotalExecutionTime());
            
        
    }
     
      private boolean verifyRisksDialogisPresent()
    {  
            SeleniumDriverInstance.pause(2000);
       
            if(SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
            {
                    selectMotorSpecifiedRiskType();
                    return true;

            }
            else if(SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
                {
                    SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
                    selectMotorSpecifiedRiskType();
                    return true;
                }
            
            else
                {
                    SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
                    System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");
                    return false;

                }

    }
     
    private boolean selectMotorSpecifiedRiskType() 
    {
        if(!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectRiskType(testData.TestParameters.get("Risk Name")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return false;
        }
            SeleniumDriverInstance.pause(2000);
            SeleniumDriverInstance.takeScreenShot( "Motor Specified Risk type selected successfully", false);
            return true;
    }
    

    private boolean enterVehicleTypeDetails()
    {
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.VehicleTypeDropdownListId(), testData.TestParameters.get("Vehicle Type")))
        {
           return false;
        }
            SeleniumDriverInstance.pause(2000);
        
        
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.VehicleCategoryDropdownListId(),  testData.TestParameters.get("Vehicle Category")))
        {
           return false;
        }
         
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.CurrentInsurancePeriodDropdownListId(),  testData.TestParameters.get("Current Insurance Period")))
        {
           return false;
        }
         
         System.out.println("Claims History");
         
         if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleTypeNumberofIncidents12MonthsTextBoxId(),
                testData.TestParameters.get("Number of Incidents last 12 Months")))
            {
              return false;
            }

          if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleTypeNumberofIncidents12To24MonthsTextBoxId(),
            testData.TestParameters.get("Number of Incidents last 12-24 Months")))
            {
              return false;
            }

          if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleTypeNumberofIncidents24To36MonthsTextBoxId(),
            testData.TestParameters.get("Number of Incidents last 24-36 Months")))
            {
              return false;
            }
          
          System.out.println("Main Driver");
          
          if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegularDriverNameTextBoxId(),
            testData.TestParameters.get("Regular Driver Name")))
            {
              return false;
            }
          
          if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.MainDriverGenderDropdownListId(),  testData.TestParameters.get("Gender")))
            {
               return false;
            }
          
          if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MainDriverDateofBirthTextBoxId(),
            testData.TestParameters.get("Date of Birth")))
            {
              return false;
            }
          
          if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.MainDriverMaritalStatusDropdownListId(),  testData.TestParameters.get("Marital Status")))
            {
               return false;
            }
          
          if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.MainDriverIDTypeDropdownListId(),  testData.TestParameters.get("ID Type")))
            {
               return false;
            }
          
          if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.MainDriverLicenceTypeDropdownListId(),  testData.TestParameters.get("Drivers Licence Type")))
            {
               return false;
            }
          
          if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MainDriverIDNumberTextBoxId(),
            testData.TestParameters.get("ID Number")))
            {
              return false;
            }
          
          if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MainDriverDateLicenceIssuedTextBoxId(),
            testData.TestParameters.get("Date Licence Issued")))
            {
              return false;
            }
          

          SeleniumDriverInstance.pause(5000);
          
          if(!SeleniumDriverInstance.enableDisabledElementById(EkunduCreateNewPolicyForNewClientPage.AccessoriesCheckBoxId()))
         {
             return false;
         }
          
          
          

          if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.AccessoriesCheckBoxId(),true))
            {
              return false;
            }
          
          if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriversCheckBoxId(),true))
            {
              return false;
            }
          
          SeleniumDriverInstance.takeScreenShot("Vehicle type details entered successfully", false);
          
          if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
          
          return true;
       
    }
    
   private boolean enterAccessories()
   {
       System.out.println("Entering Risk Accessories...");
       if(!SeleniumDriverInstance.clickElementbyName(EkunduViewClientDetailsPage.AddAccessoryItemsButtonName()))
         {
             return false;
         }
       
       if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessoryDescriptionTextBoxId(),
            testData.TestParameters.get("Accessory Description")))
            {
              return false;
            }
       
       if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessorySumInsuredTextBoxId(),
            testData.TestParameters.get("Accessory Sum Insured")))
            {
              return false;
            }
       
       SeleniumDriverInstance.takeScreenShot("Accessory Item added successfully", false);
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
         {
             return false;
         }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
       
       return true;
   
   }
   
   private boolean enterMotorDetails()
   {
        if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.DetailsVehicleSearchIconId()))
        {
           return false;
        }

        
        SeleniumDriverInstance.pause(3000);
        SeleniumDriverInstance.WaitUntilDropDownListPopulatedById(EkunduMotorSpecifiedRiskPage.DetailsVehicleMakeDropdownListId());

            SeleniumDriverInstance.WaitUntilDropDownListPopulatedById(EkunduMotorSpecifiedRiskPage.DetailsVehicleMakeDropdownListId());

        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.DetailsVehicleMakeDropdownListId(), testData.TestParameters.get("Vehicle Make")))
        {
           return false;
        }
            SeleniumDriverInstance.pause(2000);
        
        
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.DetailsVehicleYearDropdownListId(), testData.TestParameters.get("Vehicle Year")))
        {
           return false;
        }

         
        SeleniumDriverInstance.WaitUntilDropDownListPopulatedById(EkunduMotorSpecifiedRiskPage.DetailsVehicleModelDropdownListId());

         
         SeleniumDriverInstance.pause(5000);

        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.DetailsVehicleModelDropdownListId(), testData.TestParameters.get("Vehicle Model")))
        {
           return false;
        }
        
            SeleniumDriverInstance.pause(3000);
        

            this.SelectVehicle();

            SelectVehicle();

            
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.DetailsVehicleClassofUseDropdownListId(), testData.TestParameters.get("Vehicle Class of Use")))
        {
           return false;
        }
        
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.VehicleTrackingDeviceFittedCheckBoxId(),true))
            {
              return false;
            }
        
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.VehicleImmobiliserCheckBoxId(),true))
            {
              return false;
            }
        
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.VehicleAlarmCheckBoxId(),true))
            {
              return false;
            }
        
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.VehicleGearlockCheckBoxId(),true))
            {
              return false;
            }
        
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.VehicleGaragedatNightCheckBoxId(),true))
            {
              return false;
            }
        
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.VehicleUnsupportedMotorCheckBoxId(),true))
            {
              return false;
            }
        
        SeleniumDriverInstance.takeScreenShot("Vehicle details entered successfully", false);
        
        return true;
   
   }
   
    private boolean SelectVehicle()
    { 
        try
        {
            WebElement VehiclesTable = SeleniumDriverInstance.Driver.findElement(By.id("popupGrid"));
            VehiclesTable.click();
            System.out.println("[Info] Vehicle selected, - Year - 2011");
            return true;
        }
        catch(Exception e)
        {
            System.err.println("[Error] Failed to select vehicle " + e.getMessage());
            return false;
        }
    }
    
    private boolean enterVehicleRegistrationDetails()
    {
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.RegistrationDetailsEngineNumberTextBoxId(), testData.TestParameters.get("Engine Number")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.RegistrationDetailsChassisNumberTextBoxId(), testData.TestParameters.get("Chassis Number")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.DetailsRegistrationDetailsRegNumberTextBoxId(), testData.TestParameters.get("Registration Number")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.DetailsRegistrationDetailsRegOwnerTextBoxId(), testData.TestParameters.get("Registered Owner")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.RegistrationDetailsOriginalRegistrationDateTextBoxId(), testData.TestParameters.get("Original Registration Date")))
        {
           return false;
        }
           
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.DetailsRegistrationDetailsNATISCodeDropdownListId(), testData.TestParameters.get("NATIS Code")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.DetailsMotorCoverTypeDropdownListId(), testData.TestParameters.get("Motor Cover Type")))
        {
           return false;
        }
        
        System.out.println("Entering Non Factory-Fitted Radio...");
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.NonFactoryFittedRadioValueTextBoxId(), testData.TestParameters.get("Non Factory-Fitted Radio Value")))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.NonFactoryFittedRadioDescriptionTextBoxId(), testData.TestParameters.get("Non Factory-Fitted Radio Description")))
        {
           return false;
        }
        
        System.out.println("Excesses...");
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.SelectedExcessDropdownListId(), testData.TestParameters.get("Excesses - Selected Excess")))
        {
           return false;
        }
        
        System.out.println("Extensions...");
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.ExtensionsCarHireCheckboxId(), true))
        {
           return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.ExtensionsCreditShortfallCheckboxId(), true))
        {
           return false;
        }
        
        System.out.println("Entering Adjustment details...");
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MotorSpecifiedAdjustmentPercentageTextBoxId(),
       testData.TestParameters.get("Adjustment Percentage")))
       {
         return false;
       }

       if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorSpecifiedAdjustmentReasonDropdownListId()))
       {
         return false;
       }

       System.out.println("Attempting to select Adjustment Reason");
       
      if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorSpecifiedAdjustmentReasonDropdownListId(),
              testData.TestParameters.get("Adjustment Reason")))
       {
         return false;
       }
      

      if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
          {
            return false;
          }
      
      SeleniumDriverInstance.pause(3000);
      

      SeleniumDriverInstance.takeScreenShot("Vehicle details entered successfully", false);
      
      this.enterInterestedParties();
      
      return true;
        
    
    }
    
    private boolean enterInterestedParties()
      {
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.MSAddInterestedPartiesButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MSTypeofAgreementTypeDropdownListId(),
                testData.TestParameters.get("Type of Agreement")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MSInstitutionNameDropdownListId(),
                testData.TestParameters.get("Institution Name")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MSInterestedPartiesDescriptionTextAreaId(),
                testData.TestParameters.get("Interested Party Description")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.InterestedPartyNotesTextBoxId(),
                testData.TestParameters.get("Interested Party Comments")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Interested parties details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
        
        return true;

      }
    
    private boolean enterSpecifiedDriverDetails()
    {

        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddSpecifiedDriverButtonName()))
          {
            return false;
          }
        

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverNameTextBoxId(),
            testData.TestParameters.get("Regular Driver Name")))
            {
              return false;
            }
          
          if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.SpecifiedDriverGenderDropdownListId(),  testData.TestParameters.get("Gender")))
            {
               return false;
            }
          
          if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverDateofBirthTextBoxId(),
            testData.TestParameters.get("Date of Birth")))
            {
              return false;
            }
          
          if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.SpecifiedDriverMaritalStatusDropdownListId(),  testData.TestParameters.get("Marital Status")))
            {
               return false;
            }
          
          if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.SpecifiedDriverIDTypeDropdownListId(),  testData.TestParameters.get("ID Type")))
            {
               return false;
            }
          
          if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.SpecifiedDriverLicenceTypeDropdownListId(),  testData.TestParameters.get("Drivers Licence Type")))
            {
               return false;
            }
          
          if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverIDNumberTextBoxId(),
            testData.TestParameters.get("ID Number")))
            {
              return false;
            }
          
          if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverDateLicenceIssuedTextBoxId(),
            testData.TestParameters.get("Date Licence Issued")))
            {
              return false;
            }
          
          if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
          

          SeleniumDriverInstance.takeScreenShot("Specified driver details entered successfully", false);

          SeleniumDriverInstance.takeScreenShot("Specified drivr details entered successfully", false);

          
          if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
          
          return true;
         
    }
    
    private boolean enterEndorsements()
    {
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsSelectButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsAddAllButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsApplyButtonId()))
         {
             return false;
         }
         
         //this.editEndorsementDocument();
         
         SeleniumDriverInstance.takeScreenShot("Endorsement details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        
            return true;
         
         
     }
    
    private boolean editEndorsementDocument()
    {
        if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EditMotorEndorsementsLinkId()))
         {
             return false;
         }
        
        SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId());
        
        SeleniumDriverInstance.pause(3000);
        
        SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.DocumentEditorFrameId());
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EndorsementsBodyId(),
                ("Motor Endorsement")))
          {
            return false;
          }
        
        SeleniumDriverInstance.switchToDefaultContent();
        
        SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId());
        
//         if(!SeleniumDriverInstance.clickElementbyXpath(EkunduCreateNewPolicyForNewClientPage.BulletListLinkXpath()))
//         {
//             return false;
//         }
         
         SeleniumDriverInstance.pause(10000);
      
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementsSaveButtonId()))
         {
             return false;
         }
        
        SeleniumDriverInstance.switchToDefaultContent();
        
        return true;
        
    }
    
    private boolean enterNotes()
    { 
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedNotesTextBoxId(), testData.TestParameters.get("Motor Specified Notes")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedAddNotesCheckboxId(), true))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedPolicyNotesTextBoxId(), testData.TestParameters.get("Motor Specified Notes")))
         {
             return false;
         }
            
            SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);
        
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
       
          
            SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

            SeleniumDriverInstance.pause(3000);
            
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.ReinsuranceBandDropDownListId(),"MS Own Damage (Section A)"))
         {
             return false;
         }
            

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
            
        return true;
              
    }
    
    
    
}
