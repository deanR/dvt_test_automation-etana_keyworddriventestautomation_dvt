/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author deanR
 */
public class EkunduEBPMotorSpecifiedCaravanTest extends BaseClass {

    TestEntity testData;
    TestResult testResultt;

    public EkunduEBPMotorSpecifiedCaravanTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

//        if (!navigateToFindClientPage())
//          {
//            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find  client page", true);
//
//            return new TestResult(testData, Enums.ResultStatus.FAIL,
//                                  "Failed to navigate to the find  client page- "
//                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//          }
//
//        if (!findClient())
//          {
//            SeleniumDriverInstance.takeScreenShot("Failed to find client", true);
//
//            return new TestResult(testData, Enums.ResultStatus.FAIL,
//                                  "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail,
//                                  this.getTotalExecutionTime());
//          }
//
//        if (!enterMidTermAdjustmentData())
//          {
//            SeleniumDriverInstance.takeScreenShot("Failed to enter mid term adjustment data", true);
//
//            return new TestResult(testData, Enums.ResultStatus.FAIL,
//                                  "Failed to enter mid term adjustment data- "
//                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//          }
        if (!verifyRisksDialogisPresent()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the risks dialog is present", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the risks dialog is present - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterMotorDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Motor details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Motor details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterFAPDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter First Amount Payable details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter First Amount Payable details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterNotes()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Notes", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Notes - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!ReinsuranceDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to Complete Reinsurance Details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Complete Reinsurance Details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Motor Specified risk details entered successfully", this.getTotalExecutionTime());
    }

    private String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");
            return "";
        }
    }

    private boolean navigateToFindClientPage() {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),
                EKunduHomePage.FindClientTabXPath())) {
            return false;
        }

        return true;
    }

    private boolean findClient() {
        String clientCode = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                "Retrieved Client Code");

        if (clientCode.equals("parameter not found")) {
            clientCode = testData.TestParameters.get("Client Code");
        } else {
            testData.updateParameter("Client Code", clientCode);
        }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId())) {
            return false;
        }

        return true;
    }

    private boolean enterMidTermAdjustmentData() {
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText())) {
            return false;
        }

        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                "Retrieved Policy Number");

        if (policyNumber.equals("parameter not found")) {
            policyNumber = testData.TestParameters.get("Policy Number");
        } else {
            testData.updateParameter("Policy Number", policyNumber);
        }

        SeleniumDriverInstance.pause(2000);

//        if (!SeleniumDriverInstance.changePolicy(policyNumber, "Change"))
//          {
//            return false;
//          }
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ChangePolicyLinkText())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MTATypeofChangeDropDownListId(),
                this.getData("Type of Change"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MIAEffectiveDateTextBoxId(),
                this.getData("Effective Date"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId())) {
            return false;
        }

        return true;

    }

    private boolean verifyRisksDialogisPresent() {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            selectMotorSpecifiedRiskType();
            return true;

        } else if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectMotorSpecifiedRiskType();
            return true;
        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");
            return false;

        }

    }

    private boolean selectMotorSpecifiedRiskType() {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Motor Specified Risk type selected successfully", false);
        return true;
    }

    private boolean enterMotorDetails() {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.DetailsVehicleSearchIconId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleSourceDropdownListId(),
                testData.TestParameters.get("Source"))) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.VehicleModelSelectedRowId2())) {
            return false;
        }

        System.out.println("Options...");

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.EndorsementsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesCheckBoxId(), true)) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.ClaimRebateDropdownListId(), testData.TestParameters.get("No Claim Rebate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.DetailsRegistrationDetailsRegNumberTextBoxId(), generateDateTimeString())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.DetailsRegistrationDetailsRegOwnerTextBoxId(), this.getData("Registered Owner"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationDateTextboxId(),
                testData.TestParameters.get("Registration Date"))) {
            return false;
        }

        System.out.println("Motor Cover...");

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.DetailsMotorCoverSumInsuredTextBoxId(), this.getData("Motor Cover Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.DetailsMotorCoverTypeDropdownListId(), this.getData("Motor Cover Type"))) {
            return false;
        }

        System.out.println("Extensions...");

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.CreditShortfallCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.CreditShortfallRateTextboxId(), this.getData("Extensions - Credit shortfall - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsFireandExplosionRateTextboxId(), this.getData("Extensions - Fire and Explosion - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsFireandExplosionPremiumTextboxId(), this.getData("Extensions - Fire and Explosion - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsThirdPartLiabilityRateTextBoxId(), this.getData("Extensions - Third Party Liability Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsThirdPartLiabilityPremiumTextBoxId(), this.getData("Extensions - Third Party Liability Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.AdjustmentPercentageTextBoxId(), this.getData("Adjustment Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.OverrideReasonDropdownlistID(), this.getData("Reason for Override"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterFAPDetails() {
        if (!SeleniumDriverInstance.uncheckCheckBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.SubB1CheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.uncheckCheckBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.SubB2CheckBoxId(), true)) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("First Amount Payable details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        System.out.println("Endorsements");

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsSelectButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsAddAllButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsApplyButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Endorsement details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        System.out.println("Interested parties");

        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.MSAddInterestedPartiesButtonName())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MSTypeofAgreementTypeDropdownListId(),
                testData.TestParameters.get("Type of Agreement"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.InterestedPartyInstitutionNameDropdownListId(),
                testData.TestParameters.get("Institution Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Interested parties details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterNotes() {

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedAddNotesCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedPolicyNotesTextBoxId(), this.getData("Motor Specified Comments"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

//            SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
//            SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        return true;

    }

    private boolean ReinsuranceDetails() {
        SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BandDropdownListId(),
                testData.TestParameters.get("Reinsurance Band"))) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId())) {
            return false;
        }

        return true;
    }

}
