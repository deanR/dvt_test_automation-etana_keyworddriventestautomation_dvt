
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.CreateNewClaimPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreatePersonalClientClaimPage;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

//~--- JDK imports ------------------------------------------------------------

import java.awt.Robot;
import java.awt.event.KeyEvent;

/**
 *
 * @author FerdinandN
 */
public class EkunduCreatePersonalClientClaimTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduCreatePersonalClientClaimTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToClaimPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the Claim Cases page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the Claim Cases page - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!verifyClaimCasePageHasLoaded())
          {
            SeleniumDriverInstance.takeScreenShot("Claim Cases page failed to load", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Claim Cases page failed to load - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!clickOk())
          {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Ok button was not clicked - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!selectClaimsChampion())
          {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Claims champion not selected - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!submitClaimDetails())
          {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Claim details not submitted" + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!OpenClaim())
          {
            SeleniumDriverInstance.takeScreenShot("Claim could not be found", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Claim could not be found" + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.clearTextById(EkunduCreatePersonalClientClaimPage.lossDateTextBoxId());

        if (!findPolicy())
          {
            SeleniumDriverInstance.takeScreenShot("Policy could not be found", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Policy could not be found" + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!clickFindNowButtonId())
          {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Find now button was not clicked - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!selectPolicyId())
          {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Policy was not selected - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterClaimOverviewDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Claim overview details could not be entered", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Claim overview details could not be entered"
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterPremiumDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Premium details could not be entered", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Premium details could not be entered"
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterWorkClaimPerils())
          {
            SeleniumDriverInstance.takeScreenShot("Claim perils could not be entered", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Claim perils could not be entered" + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!verifyClaimHasBeenOpened())
          {
            SeleniumDriverInstance.takeScreenShot("Claim could not be opened", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Claim could not be opened" + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Personal Client Claim successfully opened",
                              this.getTotalExecutionTime());
      }

    private boolean navigateToClaimPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(
                EkunduCreatePersonalClientClaimPage.claimHoverTabId(),
                EkunduCreatePersonalClientClaimPage.newCaseLinkXPath()))
          {
            return false;
          }

        return true;
      }

    private boolean verifyClaimCasePageHasLoaded()
      {
        try
          {
            if (!SeleniumDriverInstance.waitForElementById(EkunduCreatePersonalClientClaimPage.contentSpanId()))
              {
                return false;
              }

            SeleniumDriverInstance.takeScreenShot("Claims page loaded successfully", false);

            return true;
          }
        catch (Exception e)
          {
            return false;
          }
      }

    private boolean clickOk()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientClaimPage.OkButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean selectClaimsChampion()
      {
        SeleniumDriverInstance
            .WaitUntilDropDownListPopulatedById(EkunduCreatePersonalClientClaimPage.claimsChampionDropDownListId());

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduCreatePersonalClientClaimPage.claimsChampionDropDownListId(),
                testData.TestParameters.get("Claims Champion")))
          {
            return false;
          }

        return true;
      }

    private boolean submitClaimDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientClaimPage.submitButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean OpenClaim()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientClaimPage.openClaimButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Claim opened successfully", false);

        return true;

      }

    private boolean findPolicy()
      {
        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                                  "Policy Reference Number");

        if (policyNumber.equals("parameter not found"))
          {
            policyNumber = testData.TestParameters.get("Policy Number");
          }
        else
          {
            testData.updateParameter("Policy Reference Number", policyNumber);
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientClaimPage.policynumberTextBoxId(),
                policyNumber))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientClaimPage.lossDateTextBoxId(),
                testData.TestParameters.get("Loss Date")))
          {
            return false;
          }

        return true;

      }

    private boolean clickFindNowButtonId()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientClaimPage.findnowButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.waitForElementById(CreateNewClaimPage.SelectPolicyLinkId()))
          {
            SeleniumDriverInstance.takeScreenShot("Policy doesn't exist", true);

            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Policy found", false);

        return true;
      }

    private boolean selectPolicyId()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientClaimPage.SelectPolicyLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Policy selected Successfully", false);

        return true;
      }

    private boolean enterClaimOverviewDetails()
      {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduCreatePersonalClientClaimPage.RiskTypeDropdownListId(), testData.TestParameters.get("Risk Type")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduCreatePersonalClientClaimPage.ProgressStatusDropdownListId(),
                testData.TestParameters.get("Progress Status")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduCreatePersonalClientClaimPage.PrimaryCauseDropdownListId(),
                testData.TestParameters.get("Primary Cause")))
          {
            return false;
          }

        JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;

        js.executeScript("__doPostBack('UpdateSecondaryCause', '');setTimeout('__doPostBack(\\'ctl00$cntMainBody$CONTROL__PRIMARY_CAUSE\\',\\'\\')', 0)");

        SeleniumDriverInstance
            .WaitUntilDropDownListPopulatedById(EkunduCreatePersonalClientClaimPage.SecondaryCauseDropdownListId());

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduCreatePersonalClientClaimPage.SecondaryCauseDropdownListId(),
                testData.TestParameters.get("Secondary Cause")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduCreatePersonalClientClaimPage.ClaimsHandlerDropdownListId(),
                testData.TestParameters.get("Claims Handler")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientClaimPage.ClaimDescriptionTextBoxId(),
                testData.TestParameters.get("Claim Description")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Claim overview details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientClaimPage.ClaimNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterPremiumDetails()
      {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduCreatePersonalClientClaimPage.PremiumConfirmerDropdownListId(),
                testData.TestParameters.get("Premium Confirmer")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Premium details entered successfully", false);

        JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;

        js.executeScript("document.getElementById('" + EkunduCreatePersonalClientClaimPage.ClaimNextButtonId()
                         + "').click();");

        SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientClaimPage.ClaimNextButtonId());

        SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientClaimPage.ClaimNextButtonId());

        SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientClaimPage.ClaimNextButtonId());

        SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientClaimPage.ClaimNextButtonId());

        SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientClaimPage.ClaimNextButtonId());

        SeleniumDriverInstance.checkPresenceofElementById(EkunduCreatePersonalClientClaimPage.LoadReserveEditLinkId(), EkunduCreatePersonalClientClaimPage.ClaimNextButtonId());
        return true;
      }

    private boolean enterWorkClaimPerils()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientClaimPage.LoadReserveEditLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientClaimPage.ClaimReserverNewValueTextBoxId(),
                testData.TestParameters.get("Reserve New Value")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientClaimPage.ReserverFinishButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientClaimPage.ReserverFinishButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Claim perils entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientClaimPage.PerilsSubmitButtonId()))
          {
            return false;
          }

        /*
         * JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;
         *
         * js.executeScript("document.getElementById('"+ EkunduCreatePersonalClientClaimPage.PerilsSubmitButtonId() + "').click();");
         */

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientClaimPage.ClaimReinsuranceOkButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);

//        SeleniumDriverInstance.clickElementbyXpath(CreateNewClaimPage.ClaimConfirmationNoButtonXpath());
//
//        SeleniumDriverInstance.pause(2000);
//
//        SeleniumDriverInstance.clickElementbyXpath(CreateNewClaimPage.ClaimConfirmationNoButtonXpath());

        return true;
      }

    private boolean verifyClaimHasBeenOpened()
      {
        if (!SeleniumDriverInstance.waitForElementById(EkunduCreatePersonalClientClaimPage.ClaimNumberSpanId()))
          {
            return false;
          }

        testData.addParameter("Retrieved Personal Claim Number",
                              SeleniumDriverInstance.retrieveTextById(
                                  EkunduCreatePersonalClientClaimPage.ClaimNumberSpanId()));
        SeleniumDriverInstance.pause(3000);
        SeleniumDriverInstance.takeScreenShot("Claim opened successfully", false);

        return true;
      }

    private boolean openWorkClaimPeril()
      {
        String workClaimPeril = "";

        try
          {
            workClaimPeril = testData.TestParameters.get("Work Claim Peril");

            WebElement workClaimPerilsDiv = SeleniumDriverInstance.Driver.findElement(By.id(
                                                EkunduCreatePersonalClientClaimPage.workPerilsTableDivId()));
            WebElement workClaimPerilsTable = workClaimPerilsDiv.findElement(By.tagName("table"));

            for (WebElement row : workClaimPerilsTable.findElements(By.tagName("tr")))
              {
                if (row.findElements(By.tagName("td")).get(0).getText().equals(workClaimPeril))
                  {
                    row.findElement(By.linkText("Edit")).click();
                    System.out.println("[Info] Selected work claim peril, - Description - " + workClaimPeril);

                    return true;
                  }
              }

            System.err.println("[Error] Failed to select work claim peril, description not found - Description - "
                               + workClaimPeril);

            return false;

          }
        catch (Exception e)
          {
            SeleniumDriverInstance.DriverExceptionDetail = e.getMessage();
            System.err.println("[Error] Failed to select work claim peril, exception detected - Fault - "
                               + e.getMessage());

            return false;
          }
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
