
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPageNew;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPageNew;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author ferdinandN
 */
public class EkunduEPLMTAEditDomesticandPropertyRiskTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduEPLMTAEditDomesticandPropertyRiskTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!editDomesticandPropertyRisk())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to edit Domestic and Property"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to edit Domestic and Property - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!editFACPropPlacement())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to edit FAC Prop placements"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to edit FAC Prop placements - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!finalisePolicy())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to finalise Personal Client Policy"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to finalise Personal Client Policy- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Domestic and Property risk edited successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Domestic and Property risk edited successfully",
                              this.getTotalExecutionTime());

      }

    private boolean editDomesticandPropertyRisk()
      {
        if ((SeleniumDriverInstance.checkRiskActionElement()))
          {
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPageNew.DomesticandPropertyEditLinkId()))
              {
                return false;
              }
          }
        else
          {
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.DomesticandPropertyRiskActionDropdownListXpath(),
                    "Edit"))
              {
                return false;
              }
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduCreateNewPolicyForNewClientPage.BuildingSumInsuredTextBoxId(),
                testData.TestParameters.get("Building Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Buildings sum insured edited successfull", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduCreateNewPolicyForNewClientPage.ContentsSumInsuredTextBoxId(),
                testData.TestParameters.get("Contents Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Contents sum insured edited successfull", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        this.clickEditAllRiskItemsLink1();

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduCreateNewPolicyForNewClientPage.DAPRiskItemSumInsuredTestboxId(),
                testData.TestParameters.get("Risk Item - Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("All risk item 1 sum insured edited successfull", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

//      this.clickEditAllRiskItemsLink2();
//        
//         if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduCreateNewPolicyForNewClientPage.DAPRiskItemSumInsuredTestboxId(), testData.TestParameters.get("Risk Item - Sum Insured 2")))
//        {
//        return false;
//        }
//         
//         if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
//        {
//        return false;
//        }
//         
//      SeleniumDriverInstance.takeScreenShot("All risk item 2 sum insured edited successfull", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        return true;

      }

    private boolean editFACPropPlacement()
      {
        SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_ReInsurance2007Cntrl_ddlReinsurance",
                "EPL - Personal Property");

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clearTextAndEnterValueById("ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl03_txtThisPerc",
                "5"))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById("ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl04_txtThisPerc",
                "15"))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("FAC Prop placements edited successfully", false);

        SeleniumDriverInstance.pause(5000);

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(5000);

        return true;
      }

    private boolean finalisePolicy()
      {
        if (SeleniumDriverInstance.checkRiskActionElement())
          {
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalLinesEditLinkId()))
              {
                return false;
              }
          }
        else
          {
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(),
                    "Edit"))
              {
                return false;
              }
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalLinesNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalLinesNextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.scrollElementIntoViewById(EkunduCreateNewPolicyForNewClientPage.MakePolicyLiveButtonId());

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MakePolicyLiveButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(50000);

        if (!SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.PolicyCreationConfirmationSpanId()))
          {
            return false;
          }

        String PolicyRef =
            SeleniumDriverInstance.retrieveTextById(EkunduCreateNewPolicyForNewClientPage.PolicyRefLabelId());

        testData.addParameter("Policy Reference Number", PolicyRef);

        if (PolicyRef.isEmpty())
          {
            System.err.println("Failed to retrieve policy number");
          }
        else
          {
            System.out.println("Policy number retrieved - " + PolicyRef);
          }

        SeleniumDriverInstance.takeScreenShot("Policy created successfully", false);

        return true;
      }

    private boolean clickEditAllRiskItemsLink1()
      {
        WebElement cell =
            SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_ALL_RISKS__ALL_RISK_ITEM\"]/table/tbody/tr/td[7]/a"));

        if (cell != null)
          {
            cell.click();
            System.out.println("[TestInfo] Edit Risk Item Link Clicked");

            return true;
          }
        else
          {
            System.err.println("[Error] Failed to click Edit risk item Link");

            return false;
          }

      }

    private boolean clickEditAllRiskItemsLink2()
      {
        WebElement cell =
            SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_ALL_RISKS__ALL_RISK_ITEM\"]/table/tbody/tr2/td[7]/a"));

        if (cell != null)
          {
            cell.click();
            System.out.println("[TestInfo] Edit Risk Item Link Clicked");

            return true;
          }
        else
          {
            System.err.println("[Error] Failed to click Edit risk item Link");

            return false;
          }

      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
