/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBroadFormLiabilityPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author ferdinandN
 */
public class EkunduMachineryBreakDownRiskTest extends BaseClass
{
    TestEntity testData;
    TestResult testResult;
  
    
    public EkunduMachineryBreakDownRiskTest(TestEntity testData)
    {
        this.testData = testData;

    }
    
     public TestResult executeTest()
     {
        this.setStartTime();
        
         if(!verifyAddRiskButtonisPresent())
        {
           SeleniumDriverInstance.takeScreenShot("Failed to verify that the risks dialog is present", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to verify that the risks dialog is present - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!addMachineryBreakdownItem())
        {
           SeleniumDriverInstance.takeScreenShot("Failed to add Machinery Breakdown Item", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to add Machinery Breakdown Item - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
                
        if(!enterConsequentialLossRatingBasis())
        {
           SeleniumDriverInstance.takeScreenShot("Failed to enter Consequential Loss Rating Basis", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Consequential Loss Rating Basis - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
                
        if(!enterMachineryExtensions())
        {
           SeleniumDriverInstance.takeScreenShot("Failed to enter Machinery Extensions", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Machinery Extensions - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
                
        if(!enterConsequentialLossExtensions())
        {
           SeleniumDriverInstance.takeScreenShot("Failed to enter Consequential Loss Extensions", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Consequential Loss Extensions - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterNotes())
        {
           SeleniumDriverInstance.takeScreenShot("Failed to enter risk notes", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter risk notes - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
     
          return new TestResult(testData, Enums.ResultStatus.PASS, "Machinery Breakdown risk added successfully", this.getTotalExecutionTime());
     }
     
     private boolean verifyAddRiskButtonisPresent()         
    {
            SeleniumDriverInstance.pause(2000);

        if(SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectMachineryBreakdownRiskType();
            
            return true;
        }
        
        else if(SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
        {
            selectMachineryBreakdownRiskType();
            return true;
        }
        
        else
        { 
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");
            return false;
        }
    }
 
    private boolean selectMachineryBreakdownRiskType() 
    {        
        if(!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            return false;
        }
       
        
       if(!SeleniumDriverInstance.selectRiskType(testData.TestParameters.get("Risk Name")))
       {
           return false;
       }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return false;
        }
            SeleniumDriverInstance.pause(5000);
            SeleniumDriverInstance.takeScreenShot( "Machinery Breakdown Risk type selected successfully", false);
            return true;
    }
        
     private boolean addMachineryBreakdownItem()
     {
         if(!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddMachineryBreakdownItemeButtonName()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ConsequetialLossCheckBoxId(), true))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.DeteriorationofStockCheckBoxId(), true))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.DescriptionofMachineryTextboxId(), getData("Description of Machinery")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ChangeAddressButtonId()))
         {
             return false;
         }
         
         System.out.println("Machinery Item details..");
         
         SeleniumDriverInstance.pause(3000);
         
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.RiskAddressSelectedRowId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MachineryItemSumInsuredTextboxId(), getData("Machinery Item - Sum Insured")))
         {
             return false;
         }
         
          if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BasisofInsuranceDropdownListId(), getData("Machinery Item - Basis of Insurance")))
         {
             return false;
         }
          
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MachineryItemRateTextboxId(), getData("Machinery Item - Rate")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MachineryItemPremiumTextboxId(), getData("Machinery Item - Premium")))
         {
             return false;
         }
           
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MachineryItemDeductiblePercentageTextboxId(), getData("Machinery Item - Deductible Percentage")))
         {
             return false;
         }
            
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MachineryItemMinimumAmountTextboxId(), getData("Machinery Item - Minimum Amount")))
         {
             return false;
         }
             
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MachineryItemMaximumAmountTextboxId(), getData("Machinery Item - Maximum Amount")))
         {
             return false;
         }
              
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BasisofDeductibleDropdownListId(), getData("Machinery Item - Basis of Deductible")))
         {
             return false;
         }
        
        System.out.println("Consiquentisl Loss...");
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.GrossProfitBasisCheckBoxId(), true))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.GrossProfitIndemnityPeriodUnitDropdownListId(), getData("Consiquential Loss - Indemnity Period Unit")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.GrossProfitIndemnityPeriodTextboxId(), getData("Consiquential Loss - Indemnity Period")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TimeDeductibleUnitDropdownListId(), getData("Consiquential Loss - Time Deductible Unit")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.TimeDeductibleTextboxId(), getData("Consiquential Loss - Time Deductible")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.IncreasedCostofWorkingCheckBoxId(), true))
         {
             return false;
         }
        
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.IncreasedCostofWorkingIndemnityPeriodUnitDropdownListId(), getData("Consiquential Loss - Indemnity Period Unit")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.IncreasedCostofWorkingIndemnityPeriodTextboxId(), getData("Consiquential Loss - Indemnity Period")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.IncreasedCostofWorkingTimeDeductibleUnitDropdownListId(), getData("Consiquential Loss - Time Deductible Unit")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.IncreasedCostofWorkingTimeDeductibleTextboxId(), getData("Consiquential Loss - Time Deductible")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.AdditionalIncreasedCostofWorkingCheckBoxId(), true))
         {
             return false;
         }
        
             if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.AdditionalIncreasedCostofWorkingIndemnityPeriodUnitDropdownListId(), getData("Consiquential Loss - Indemnity Period Unit")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AdditionalIncreasedCostofWorkingIndemnityPeriodTextboxId(), getData("Consiquential Loss - Indemnity Period")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.AdditionalIncreasedCostofWorkingTimeDeductibleUnitDropdownListId(), getData("Consiquential Loss - Time Deductible Unit")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AdditionalIncreasedCostofWorkingTimeDeductibleTextboxId(), getData("Consiquential Loss - Time Deductible")))
         {
             return false;
         }
        
        
        SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        SeleniumDriverInstance.takeScreenShot("Machinery Item details entered successfully", false);
        
        return true;
        
     }
     
     private boolean enterConsequentialLossRatingBasis()
     {
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.GrossProfitRatingBasisTypeDropdownListId(), getData("Gross Profit Basis - Rating Basis Type")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.GrossProfitAnnualGrossProfitTextboxId(), getData("Gross Profit Basis - Annual Gross Profit")))
         {
             return false;
         }
        
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.GrossProfitRateTextboxId(), getData("Gross Profit Basis - Rate")))
         {                                                                      
             return false;                              
         }
         
          if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.GrossProfitPreiumTextboxId(), getData("Gross Profit Basis - Premium")))
         {
             return false;
         }
          
           if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.IncreasedCostofWorkingSumInsuredTextboxId(), getData("Increased Cost of Working - Sum Insured")))
         {
             return false;
         }
        
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.IncreasedCostofWorkingRateTextboxId(), getData("Increased Cost of Working - Rate")))
         {                                                                      
             return false;                              
         }
         
          if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.IncreasedCostofWorkingPremiumTextboxId(), getData("Increased Cost of Working - Premium")))
         {
             return false;
         }
          
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AdditionalIncreasedCostofWorkingSumInsuredTextboxId(), getData("Additional Increased Cost of Working - Sum Insured")))
         {
             return false;
         }
        
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AdditionalIncreasedCostofWorkingRateTextboxId(), getData("Additional Increased Cost of Working - Rate")))
         {                                                                      
             return false;                              
         }
         
          if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AdditionalIncreasedCostofWorkingPremiumTextboxId(), getData("Additional Increased Cost of Working - Premium")))
         {
             return false;
         }
          
          SeleniumDriverInstance.takeScreenShot("Consequential Loss Rating Basis details added successfully", false);
          
          System.out.println("Deterioration of Stock");
          
           if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.DeteriorationofStockSumInsuredTextboxId(), getData("Deterioration of Stock - Sum Insured")))
         {
             return false;
         }
          
          if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.DeteriorationofStockRateTextboxId(), getData("Deterioration of Stock - Rate")))
         {
             return false;
         }
          
          if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.DeteriorationofStockPremiumTextboxId(), getData("Deterioration of Stock - Premium")))
         {
             return false;
         }
          
          if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.DeteriorationofStockDedPercentageTextboxId(), getData("Deterioration of Stock - Ded Percentage")))
         {
             return false;
         }
          
          if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.DeteriorationofStockDedMinimumAmountTextboxId(), getData("Deterioration of Stock - Ded Min Amount")))
         {
             return false;
         }
          
          if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.DeteriorationofStockDedMaximumAmountTextboxId(), getData("Deterioration of Stock - Ded Max Amount")))
         {
             return false;
         }
       
          SeleniumDriverInstance.takeScreenShot("Deterioration of Stock details added successfully", false);
          
          if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
          
          
          
          return true;

     }
     
     private boolean enterMachineryExtensions()
     {
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.AddClaimPrepCheckBoxId(), true))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AddClaimPrepSumInsuredTextBoxId(), getData("Add Claim Prep Costs - Sum Insured")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AddClaimPrepRateTextBoxId(), getData("Add Claim Prep Costs - Rate")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AddClaimPrepDedPercentageTextBoxId(), getData("Add Claim Prep Costs - Ded Percentage")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AddClaimPrepPedMinAmountTextBoxId(), getData("Add Claim Prep Costs - Ded Min Amount")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AddClaimPrepPedMaxAmountTextBoxId(), getData("Add Claim Prep Costs - Ded Max Amount")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ProfessionalFeesCheckBoxId(), true))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ProfessionalFeesSumInsuredTextBoxId(), getData("Professional Fees - Sum Insured")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ProfessionalFeesRateTextBoxId(), getData("Professional Fees - Rate")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ProfessionalFeesDedPercentageTextBoxId(), getData("Professional Fees - Ded Percentage")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ProfessionalFeesDedMinAmountTextBoxId(), getData("Professional Fees - Ded Min Amount")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ProfessionalFeesMaxAmountTextBoxId(), getData("Professional Fees - Ded Max Amount")))
         {
             return false;
         }
         
         SeleniumDriverInstance.takeScreenShot("Machinery Breakdown Extensions entered successfully", false);
         
         System.out.println("Other Extensions...");
         
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.AdditionalExtensionsCheckBoxId(), true))
         {
             return false;
         }
         
        if(!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddOtherExtensionsButtonName()))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.OtherExtensionsDescriptionTextBoxId(), getData("Other Extensions - Description")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.OtherExtensionsSumInsuredTextBoxId(), getData("Other Extensions - Sum Insured")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.OtherExtensionsRateTextBoxId(), getData("Other Extensions - Rate")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.OtherExtensionsDeductiblePercentageTextBoxId(), getData("Other Extensions - Deductible Percentage")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.OtherExtensionsMinimumAmountTextBoxId(), getData("Other Extensions - Minimum Amount")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.OtherExtensionsMaximumAmountTextBoxId(), getData("Other Extensions - Maximum Amount")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
         
         SeleniumDriverInstance.takeScreenShot("Machinery Extensions entered successfully", false);
         
         if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
         
         SeleniumDriverInstance.pause(2000);
         return true;
     }
     
     
     private boolean enterConsequentialLossExtensions()
     {
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.CLExtenionssaAddClaimPrepCheckBoxId(), true))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CLAddClaimPrepSumInsuredTextBoxId(), getData("Consequential Loss Extensions - Add Claim Prep Costs - Sum Insured")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CLAddClaimPrepRateTextBoxId(), getData("Consequential Loss Extensions - Add Claim Prep Costs - Rate")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CLAddClaimPrepDedPercentageTextBoxId(), getData("Consequential Loss Extensions - Add Claim Prep Costs - Ded Percentage")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CLAddClaimPrepPedMinAmountTextBoxId(), getData("Consequential Loss Extensions - Add Claim Prep Costs - Ded Min Amount")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CLAddClaimPrepPedMaxAmountTextBoxId(), getData("Consequential Loss Extensions - Add Claim Prep Costs - Ded Max Amount")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.CLProfessionalFeesCheckBoxId(), true))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CLProfessionalFeesSumInsuredTextBoxId(), getData("Consequential Loss Extensions - Professional Fees - Sum Insured")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CLProfessionalFeesRateTextBoxId(), getData("Consequential Loss Extensions - Professional Fees - Rate")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CLProfessionalFeesDedPercentageTextBoxId(), getData("Consequential Loss Extensions - Professional Fees - Ded Percentage")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CLProfessionalFeesDedMinAmountTextBoxId(), getData("Consequential Loss Extensions - Professional Fees - Ded Min Amount")))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CLProfessionalFeesMaxAmountTextBoxId(), getData("Consequential Loss Extensions - Professional Fees - Ded Max Amount")))
         {
             return false;
         }
         
         SeleniumDriverInstance.takeScreenShot("Consequential Loss Extensions entered successfully", false);
         
         System.out.println("Other Extensions...");
         
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.AdditionalExtensionsCheckBoxId(), true))
         {
             return false;
         }
         
        if(!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.CLAddOtherExtensionsButtonName()))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CLOtherExtensionsDescriptionTextBoxId(), getData("Consequential Loss - Other Extensions - Description")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CLOtherExtensionsSumInsuredTextBoxId(), getData("Consequential Loss - Other Extensions - Sum Insured")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CLOtherExtensionsRateTextBoxId(), getData("Consequential Loss - Other Extensions - Rate")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CLOtherExtensionsDeductiblePercentageTextBoxId(), getData("Consequential Loss - Other Extensions - Deductible Percentage")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CLOtherExtensionsMinimumAmountTextBoxId(), getData("Consequential Loss - Other Extensions - Minimum Amount")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CLOtherExtensionsMaximumAmountTextBoxId(), getData("Consequential Loss - Other Extensions - Maximum Amount")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
         if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
         
          if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
          
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
           
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
         
         SeleniumDriverInstance.takeScreenShot("Consequential Loss Other Extensions entered successfully", false);
         
         
         
         SeleniumDriverInstance.pause(5000);
         return true;
     }
     
      private boolean enterNotes()
    { 
        if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.RiskCommentsTextBoxId(), this.getData("Risk Comments")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduBroadFormLiabilityPage.NotesCheckboxId(), true))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.RiskNotesTextBoxId2(), this.getData("Risk Notes")))
         {
             return false;
         }
            
            SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);
        
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
       
            SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
            SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
            
            SeleniumDriverInstance.pause(1000);
        
        SeleniumDriverInstance.clickElementbyLinkText("Documents");
        
        SeleniumDriverInstance.pause(1000);
        
         SeleniumDriverInstance.clickElementById("btnGeneratePdf");
         
         SeleniumDriverInstance.pause(1000);
         
         SeleniumDriverInstance.clickElementById("popup_ok");
         
         
         SeleniumDriverInstance.pause(1000);
         
         SeleniumDriverInstance.clickElementbyLinkText("Risks");
        return true;
              
    }
     
      private boolean enterFACPropPlacementData()
      {
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(),
                this.getData("Addison Reinsurer")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);

//        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId());
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        SeleniumDriverInstance.pause(2000);
        
        SeleniumDriverInstance.clickElementbyPartialLinkText("Reinsurances");
        
        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId(),
                this.getData("Reinsurance Details - FAC Placement - Everestre Percentage")))
          {
            return false;
          }
        
         SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
          {
            return false;
          }

        SeleniumDriverInstance.pause(8000);

        System.out.println("Company 1 added successfully");
        return true;

      }
     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
     
     
     
     
}
     
     
