
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.CreateNewClaimPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorFleetRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduMotorFleetRiskTest extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public EkunduMotorFleetRiskTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the add risk button is present ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify that the add risk button is present - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterIndustryDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter industry details ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter industry details  - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterFleetDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Fleet details ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Fleet details  - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterFAP()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter First Amount Payable details ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter First Amount Payable details  - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterExtensions()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Extensions", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Extensions  - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!financialOverview()) {
            SeleniumDriverInstance.takeScreenShot("Failed to click next button on the financial overview section", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to click next button on the financial overview section  - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }
        
        
        if (!enterEndorsements()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Endorsements", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Endorsements  - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterRiskNotes()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Notes", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Notes  - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }
        
        if (!verifyPremium()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify premium", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify premium  - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        return new TestResult(testData, Enums.ResultStatus.PASS,
                "Motor Fleet risk details entered successfully - "
                + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());

    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean financialOverview() {
        if (!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.FinancialOverviewNextButtonId())) {
                return false;
        }

        return true;
    }

    private boolean verifyAddRiskButtonisPresent() {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectMotorFleetRisk();

            return true;
        } else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            selectMotorFleetRisk();
        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err
                    .println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");

            return false;
        }

        return true;
    }

    private boolean selectMotorFleetRisk() {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.pause(10000);
        SeleniumDriverInstance.takeScreenShot("Motor Fleet Risk selected sucessfully", false);

        return true;
    }

    private boolean enterIndustryDetails() {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(),
                this.getData("Risk Item Industry"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(
                EKunduCreateCorporateClientPage.IndustrySearchResultsSelectedRowId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Industry details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.MotorFleetNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterFleetDetails() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduMotorFleetRiskPage.FleetDetailsTypeofFleetDropdownListId(),
                this.getData("Fleet Details Type of Fleet"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduMotorFleetRiskPage.FleetDetailsCoverTypeDropdownListId(),
                this.getData("Fleet Details Cover Type"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FleetMotorFleetReinsuranceLimitTextboxId(),
                this.getData("Motor Fleet Reinsurance Limit"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduMotorFleetRiskPage.FleetMotorFleetMinimumLimitofIndemnityTextboxId(),
                this.getData("Motor Fleet Minimum Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduMotorFleetRiskPage.FleetMotorFleetMaximumLimitofIndemnityTextboxId(),
                this.getData("Motor Fleet Maximum Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FleetMotorFleetNumberofUnitsTextboxId(),
                this.getData("Motor Fleet Number of Units"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FleetMotorFleetPremiumPerUnitTextboxId(),
                this.getData("Motor Fleet Premium Per Unit"))) {
            return false;
        }
        
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.MotorFleetFlatPremiumCheckBoxId(), true)) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Fleet details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.MotorFleetNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterFAP() {
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPOwnDamageMinimumPercentageTextboxId(),
                this.getData("FAP Minimum Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPOwnDamageMinimumAmountTextboxId(),
                this.getData("FAP Minimum Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPThirdPartyMinimumPercentageTextboxId(),
                this.getData("FAP Minimum Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPThirdPartyMinimumAmountTextboxId(),
                this.getData("FAP Minimum Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorFleetRiskPage.FAPTheftHijackCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPTheftHijackMinimumPercentageTextboxId(),
                this.getData("FAP Minimum Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPTheftHijackMinimumAmountTextboxId(),
                this.getData("FAP Minimum Amount"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("First Amount Payable details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.MotorFleetNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.MotorFleetNextButtonId());

        return true;

    }

    private boolean enterExtensions() {
//        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorFleetRiskPage.ExtensionsCarHireCheckboxId(), true))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
//                EkunduMotorFleetRiskPage.ExtensionsCarHireLimitofIndemnityDropdownListId(),
//                this.getData("Extensions Car Hire Limit of Indemnity")))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.ExtensionsCarHirePremiumTextboxId(),
//                this.getData("Extensions Car Hire Premium")))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
//                EkunduMotorFleetRiskPage.ExtensionsCarHireConditionsDropdownListId(),
//                this.getData("Extensions Car Hire Conditions")))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.checkBoxSelectionById(
//                EkunduMotorFleetRiskPage.ExtensionsContingentLiabilityCheckboxId(), true))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.enterTextById(
//                EkunduMotorFleetRiskPage.ExtensionsContingentLiabilityLimitofIndemnityTextboxId(),
//                this.getData("Extensions Limit of Indemnity")))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.enterTextById(
//                EkunduMotorFleetRiskPage.ExtensionsContingentLiabilityRateTextboxId(), this.getData("Extensions Rate")))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.checkBoxSelectionById(
//                EkunduMotorFleetRiskPage.ExtensionsCreditShortFallCheckboxId(), true))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorFleetRiskPage.ExtensionsExcessWaiverCheckboxId(),
//                true))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.ExtensionsExcessWaiverPremiumTextboxId(),
//                this.getData("Extensions Premium")))
//          {
//            return false;
//          }
//
        if (!SeleniumDriverInstance.enterTextById(
                EkunduMotorFleetRiskPage.ExtensionsFireAndExplosionRateTextboxId(),
                this.getData("Extensions Rate")))
          {
            return false;
          }

//        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorFleetRiskPage.ExtensionsLossofKeysCheckboxId(),
//                true))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.enterTextById(
//                EkunduMotorFleetRiskPage.ExtensionsLossofKeysLimitofIndemnityTextboxId(),
//                this.getData("Extensions Limit of Indemnity")))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.ExtensionsLossofKeysPremiumTextboxId(),
//                this.getData("Extensions Premium")))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.enterTextById(
//                EkunduMotorFleetRiskPage.ExtensionsLossofKeysFAPPercentageTextboxId(),
//                this.getData("FAP Minimum Percentage")))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.ExtensionsLossofKeysFAPAmountTextboxId(),
//                this.getData("FAP Minimum Amount")))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.checkBoxSelectionById(
//                EkunduMotorFleetRiskPage.ExtensionsParkingFacilitiesCheckboxId(), true))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.enterTextById(
//                EkunduMotorFleetRiskPage.ExtensionsParkingFacilitiesLimitofIndemnityTextboxId(),
//                this.getData("Extensions Limit of Indemnity")))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorFleetRiskPage.ExtensionsRiotAndStrikeCheckboxId(),
//                true))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.checkBoxSelectionById(
//                EkunduMotorFleetRiskPage.ExtensionsRoadSideAssistCheckboxId(), true))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.checkBoxSelectionById(
//                EkunduMotorFleetRiskPage.ExtensionsTheftofCarRadioCheckboxId(), true))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.ExtensionsTheftofCarRadioRateTextboxId(),
//                this.getData("Extensions Rate")))
//          {
//            return false;
//          }
//
        if (!SeleniumDriverInstance.enterTextById(
                EkunduMotorFleetRiskPage.ExtensionsThirdPartyLiabilityRateTextboxId(), this.getData("Extensions Rate"))) {
            return false;
        }

//        if (!SeleniumDriverInstance.enterTextById(
//                EkunduMotorFleetRiskPage.ExtensionsThirdPartyLiabilityPremiumTextboxId(),
//                this.getData("Extensions Premium")))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.checkBoxSelectionById(
//                EkunduMotorFleetRiskPage.ExtensionsWreckageRemovalCheckboxId(), true))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.enterTextById(
//                EkunduMotorFleetRiskPage.ExtensionsWreckageRemovalLimitofIndemnityTextboxId(),
//                this.getData("Extensions Limit of Indemnity")))
//          {
//            return false;
//          }
//
//        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.ExtensionsWreckageRemovalRateTextboxId(),
//                this.getData("Extensions Rate")))
//          {
//            return false;
//          }
//
        SeleniumDriverInstance.takeScreenShot("Extensions entered successfully", true);

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.MotorFleetNextButtonId())) {
            return false;
        }

        return true;

    }

    private boolean enterEndorsements() {
//        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.MotorFleetNextButtonId())) {
//            return false;
//        }

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.EndorsementsSelectButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.EndorsementsAddAllButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.EndorsementsApplyButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.MotorFleetNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterRiskNotes() {
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.RiskCommentsTextboxId(),
                this.getData("Risk Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.RiskAddPolicyNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.RiskPolicyNotesTextboxId(),
                this.getData("Risk Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.MotorFleetNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        
        return true;
        
    }
       private boolean verifyPremium() {
        if (!SeleniumDriverInstance.verifyPremiumDetails()) {
            return false;
        }
        
        

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Documents");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementById("btnGeneratePdf");
//
//        SeleniumDriverInstance.pause(3000);
//
        SeleniumDriverInstance.clickElementById("popup_ok");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Risks");

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
