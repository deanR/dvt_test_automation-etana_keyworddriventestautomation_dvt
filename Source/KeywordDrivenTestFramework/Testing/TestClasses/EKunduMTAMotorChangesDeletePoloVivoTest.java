/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author ferdinandN
 */
public class EKunduMTAMotorChangesDeletePoloVivoTest extends BaseClass
{
    TestEntity testData;
    TestResult testResult;
    
    public EKunduMTAMotorChangesDeletePoloVivoTest(TestEntity testData)
    {
        this.testData = testData;

    }
    
    public TestResult executeTest()
    {  
         SeleniumDriverInstance.DriverExceptionDetail = "";
         this.setStartTime();
         
         if(!navigateToFindClientPage())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to navigate to the find client page", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to navigate to the find client page - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
    
        if(!findClient())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to find client", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to find client - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterMidTermAdjustmentData())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to enter Mid Term Adjustment data", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Mid Term Adjustment data - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }  
       
       
        if(!deletePoloVivo())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to copy Motor risk", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to copy Motor risk - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

            return new TestResult(testData, Enums.ResultStatus.PASS, "Motor Risk (Rolls Royce) edited successfully", this.getTotalExecutionTime());

        
}
    
    private boolean navigateToFindClientPage()
    {       
        if(!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),EKunduHomePage.FindClientTabXPath()))
        {
            return false;
        }
            return true;       
    }
    
    private boolean findClient()
    { 
            String clientCode = SeleniumDriverInstance.retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"), "Retrieved Client Code");
        
        if(clientCode.equals("parameter not found"))
        {
            clientCode = testData.TestParameters.get("Client Code");
        }
        else
            testData.updateParameter("Client Code", clientCode);
            
        if(!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Client Found", false);
        
        if(!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean enterMidTermAdjustmentData()
    {
         if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText()))
        {
            return false;
        } 
         
        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                                  "Policy Reference Number");

        if (policyNumber.equals("parameter not found"))
          {
            policyNumber = testData.TestParameters.get("Policy Number");
          }
        else
          {
            testData.updateParameter("Policy Number", policyNumber);
          }
//         
//         if(!SeleniumDriverInstance.changePolicy(policyNumber, "Change"))
//        {
//            return false;
//        }
        
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ChangePolicyLinkText())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MTATypeofChangeDropDownListId(), testData.TestParameters.get("Type of Change")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MIAEffectiveDateTextBoxId(), testData.TestParameters.get("Effective Date")))
        {
            return false;
        }
         
          if(!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId()))
        {
            return false;
        }
       
          return true;
    }
    
    private boolean deletePoloVivo()
    {
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.DeleteCopyofPoloVivoDropdownListXpath(), "Delete"))
        {
            return false;
        } 
       
       SeleniumDriverInstance.acceptAlertDialog();
       
       SeleniumDriverInstance.takeScreenShot("Motor (Polo Vivo) deleted successfully", false);
       
        SeleniumDriverInstance.pause(5000);
        
        return true;

    }
    }
