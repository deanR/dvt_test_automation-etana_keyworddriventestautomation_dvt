
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author fnell
 */
public class EkunduFinalisePolicyTest extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public EkunduFinalisePolicyTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!performRoadSideAssistQuotation()) {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to perform Road Side Assistance quotation  - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!finalisePolicy()) {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to finalise the policy  - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!verifyPolicyIsLive()) {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify policy is live  - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!retrievePolicyReferenceNumber()) {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to retrieve the PolicyReference Number  - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Client policy finalised successfully", this.getTotalExecutionTime());
    }

    private boolean retrievePolicyReferenceNumber() {
        String retrievedPRef
                = SeleniumDriverInstance.retrieveTextById(EkunduViewClientDetailsPage.PolicyReferenceNumberLabelId());

        this.testData.addParameter("Retrieved Policy Number", retrievedPRef);

        System.out.println("[Info] policy number retrieved  - Ref. - " + retrievedPRef);

        SeleniumDriverInstance.takeScreenShot("Policy reference number retrieved successfully - " + retrievedPRef,
                false);

        return true;
    }

    private boolean finalisePolicy() {
        SeleniumDriverInstance.scrollElementIntoViewById(EkunduViewClientDetailsPage.MakeLiveButtonId());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.MakeLiveButtonId())) {
            return false;
        }

        SeleniumDriverInstance.acceptAlertDialog();
        
        SeleniumDriverInstance.pause(3000);
        SeleniumDriverInstance.takeScreenShot("Client policy finalised", false);

        SeleniumDriverInstance.pause(1000);

        return true;
    }

    private boolean verifyPolicyIsLive() {
        
        if (!SeleniumDriverInstance.validateElementTextValueById(EkunduViewClientDetailsPage.PolicyConfimationLabelId(),
                "Thank You")) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.takeScreenShot("Client Policy is live ", false);

        return true;
    }

    private boolean performRoadSideAssistQuotation() {

        //SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(),
                "Edit")) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Road Side Assist risk quoted successfully", false);

        return true;
    }
}



//~ Formatted by Jindent --- http://www.jindent.com
