/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorFleetRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author ferdinandN
 */
public class EkunduMotoSectionDetailsTest extends BaseClass
{
    TestEntity testData;
    TestResult testResultt;

    public EkunduMotoSectionDetailsTest(TestEntity testData)
      {
        this.testData = testData;
      }
    
     public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the add risk button is present ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the add risk button is present - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }
        
        if (!enterMainDriverClaimsHistoryDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Main driver details ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Main driver details - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }
        
        if (!enterEndorsements())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk endorsements ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk endorsements - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }
        
        return new TestResult(testData, Enums.ResultStatus.PASS,
                              "Motor Section Details risk added successfully - "
                              + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());

      
        
      }
     
      public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
    
    private boolean verifyAddRiskButtonisPresent()
      {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectMotorSectionDetailsRisk();

            return true;
          }
        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectMotorSectionDetailsRisk();

            return true;
          }
        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");

            return false;
          }

      }
    
   
    private boolean selectMotorSectionDetailsRisk()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(10000);

        SeleniumDriverInstance.takeScreenShot("Business All Risk selected sucessfully", false);

        return true;
      }

    private boolean enterMainDriverClaimsHistoryDetails()
    {
        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.NumberofVehiclesTextBoxId(), this.getData("Number of Vehicles")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementbyName(EkunduViewClientDetailsPage.AddSpecifiedDriverButtonName()))
          {
            return false;
          }
        
        System.out.println("Main Driver");
          
          if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MainDriverNameTextBoxId(),
            testData.TestParameters.get("Regular Driver Name")))
            {
              return false;
            }
          
          if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.EPLMainDriverGenderDropdownListId(),  testData.TestParameters.get("Main Driver - Gender")))
            {
               return false;
            }
          
          if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EPLMainDriverDateofBirthTextBoxId(),
            testData.TestParameters.get("Main Driver - Date of Birth")))
            {
              return false;
            }
          
          if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.EPLMainDriverIDTypeDropdownListId(),  testData.TestParameters.get("Main Driver - ID Type")))
            {
               return false;
            }
          
          if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.EPLMainDriverLicenceTypeDropdownListId(),  testData.TestParameters.get("Main Driver - Drivers Licence Type")))
            {
               return false;
            }
          
          if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EPLMainDriverIDNumberTextBoxId(),
            testData.TestParameters.get("Main Driver - ID Number")))
            {
              return false;
            }
          
          if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EPLMainDriverDateLicenceIssuedTextBoxId(),
            testData.TestParameters.get("Date Licence Issued")))
            {
              return false;
            }
          
          System.out.println("Claims History");
          
          if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingName(EkunduCreateNewPolicyForNewClientPage.NumberofClaims12MonthsDropdownListName(),
                testData.TestParameters.get("Number of Claims Incurred in the Previous 12 Months")))
                {
                  return false;
                }
              
              if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NumberofClaims12To24MonthsDropdownListId(),
                testData.TestParameters.get("Number of Claims Incurred in the Previous 13-24 Months")))
                {
                  return false;
                }
              
              if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NumberofIClaims24To36MonthsTextBoxId(),
                testData.TestParameters.get("Number of Claims Incurred in the Previous 25-36 Months")))
                {
                  return false;
                }
              
              SeleniumDriverInstance.takeScreenShot("Main Driver / Claims history details entered successfully", false);
              
           if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }   
           
           if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }
           
           if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RoadsideAssistCheckBoxId(), true))
          {
            return false;
          }
           
           return true;
           
    }
    
       private boolean enterEndorsements()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.MotorFleetNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.EndorsementsSelectButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.MotorSectionEndorsementsAddAllButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.MotorSectionEndorsementsApplyButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.MotorFleetNextButtonId()))
          {
            return false;
          }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;
      }
    
    
    
    
}
