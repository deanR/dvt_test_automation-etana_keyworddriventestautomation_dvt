
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduPersonalClientPersonalAccidentRiskTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduPersonalClientPersonalAccidentRiskTest(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!verifyRisksDialogisPresent())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the riss dialog is present"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the riss dialog is present- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterBeneficiaryDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter beneficiary details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter beneficiary details- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterIndividualDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter individual details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter individual details- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterEndorsements())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk  endorsements"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk endorsements- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterNotes())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk notes"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk notes- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Personal Accident risk added successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Personal Accident risk added successfully",
                              this.getTotalExecutionTime());

      }

    private boolean verifyRisksDialogisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            selectPersonalAccidentRiskType();

            return true;

          }
        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectPersonalAccidentRiskType();

            return true;
          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

      }

    private boolean selectPersonalAccidentRiskType()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(testData.TestParameters.get("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Personal Accident Risk type selected successfully", false);

        return true;
      }

    private boolean enterBeneficiaryDetails()
      {
          
          
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddPersonalAccidentRiskButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddBeneficiaryButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BeneficiaryNameTextBoxId(),
                testData.TestParameters.get("Beneficiary Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BeneficiarySurnameTextBoxId(),
                testData.TestParameters.get("Beneficiary Surname")))
          {
            return false;
          }

        SeleniumDriverInstance.clearTextById(EkunduCreateNewPolicyForNewClientPage.BeneficiaryDOBTextBoxId());

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BeneficiaryDOBTextBoxId(),
                testData.TestParameters.get("Beneficiary DOB")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BeneficiaryIDTypeRequiredDropdownListId(),
                testData.TestParameters.get("Beneficiary ID Type Required")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BeneficiaryIDNumberTextBoxId(),
                testData.TestParameters.get("Beneficiary ID Number")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BeneficiaryPercentageTextBoxId(),
                testData.TestParameters.get("Beneficiary Percentage")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Beneficiary details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BeneficiaryNextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterIndividualDetails()
      {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.OccupationDropdownListId(),
                testData.TestParameters.get("Occupation")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SumInsuredDeathTextBoxId(),
                testData.TestParameters.get("Sum Insured Death")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SumInsuredPermanentDisablityTextBoxId(),
                testData.TestParameters.get("Sum Insured Permanent Disability")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SumInsuredTemporaryDisablityTextBoxId(),
                testData.TestParameters.get("Sum Insured Temporary Disability")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SumInsuredMedicalExpensesTextBoxId(),
                testData.TestParameters.get("Sum Insured Medical Expenses")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.SumInsuredNumberofWeeksDropdownListId(),
                testData.TestParameters.get("Sum Insured Number of Weeks")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalAccidentRateButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Individual details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalAccidentNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterEndorsements()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementsSelectButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.SelectClauseDropdownListId(),
                testData.TestParameters.get("Clause Selection")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddEndorsementClauseButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ClauseApplyButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterNotes()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalAccidentNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.PersonalAccidentNotesTextBoxId(),
                testData.TestParameters.get("Risk Notes")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalAccidentNextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        
//        SeleniumDriverInstance.pause(3000);
//        
//        SeleniumDriverInstance.clickElementbyLinkText("Documents");
//        
//         SeleniumDriverInstance.clickElementById("btnGeneratePdf");
//         
//         SeleniumDriverInstance.clickElementById("popup_ok");
//         
//         SeleniumDriverInstance.pause(3000);
//         
//         SeleniumDriverInstance.clickElementbyLinkText("Risks");
         
        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
