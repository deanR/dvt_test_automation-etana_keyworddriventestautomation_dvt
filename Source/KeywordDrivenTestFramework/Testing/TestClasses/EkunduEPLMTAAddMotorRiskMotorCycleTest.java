/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.JavascriptExecutor;

/**
 * 
 * @author deanR
 */
public class EkunduEPLMTAAddMotorRiskMotorCycleTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduEPLMTAAddMotorRiskMotorCycleTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        this.setStartTime();
        
            if (!verifyRisksDialogisPresent())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to verify that the risk dialog is present"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the risk dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            
            
            if (!enterMotorRiskTypeDetailsMotorCycle())
             {
                SeleniumDriverInstance.takeScreenShot(("Failed to enter risk type details"), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter risk type details- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
             }
            
           
        SeleniumDriverInstance.takeScreenShot(("Motor risk added successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Motor risk added successfully", this.getTotalExecutionTime());

      }

        private boolean verifyRisksDialogisPresent()
         {
            SeleniumDriverInstance.pause(2000);

            if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
              {
                SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
                selectMotorRiskType();

                return true;
              }

            else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
              {
                selectMotorRiskType();

                return true;

              }

            else
              {
                SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
                System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

                return false;

              }

          }

        private boolean selectMotorRiskType()
         {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());

            if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
              {
                return false;
              }

            if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
              {
                return false;
              }

            if (!SeleniumDriverInstance.switchToDefaultContent())
              {
                return false;
              }

            SeleniumDriverInstance.takeScreenShot("Motor risk type selected successfully", false);

            return true;

          }

         public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
        
        private boolean enterMotorRiskTypeDetailsMotorCycle()
         {
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
              {
                return false;
              }

            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SearchVehicleModelIconId()))
              {
                return false;
              }

            SeleniumDriverInstance.pause(3000);
            
          
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleSourceDropdownListId(),
                    testData.TestParameters.get("Source")))
                {
                  return false;
                }
                
                SeleniumDriverInstance.pause(3000);
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MakeDropdownListId(),
                    testData.TestParameters.get("Vehicles Make")))
              {
                return false;
              }

            SeleniumDriverInstance.pause(3000);

            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleYearDropdownListId(),
                    testData.TestParameters.get("Vehicles Year")))
              {
                return false;
              }

            SeleniumDriverInstance.pause(3000);

            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.ModelDropdownListId(),
                    testData.TestParameters.get("Vehicles Model")))
              {
                return false;
              }
            
//            SeleniumDriverInstance.pause(2000);

            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.VehicleModelSelectedRowId()))
              {
                return false;
              }

            SeleniumDriverInstance.pause(3000);
             
             System.out.println("Options...");
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.EndorsementsCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesCheckBoxId(), true))
            {
              return false;
            }
            
//            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriversCheckBoxId(), true))
//            {
//              return false;
//            }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MainDriverDropdownListId(),
                testData.TestParameters.get("Main Driver")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.ClaimFreeGroupDropdownListId(),
                testData.TestParameters.get("Claim free group")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleOvernightParkingDropdownListId(),
                testData.TestParameters.get("Vehicle parking overnight")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
            {
              return false;
            }
            
            System.out.println("Vehicle Cover Details...");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AreaCodeTextboxId(),
                testData.TestParameters.get("Area Code")))
            {
              return false;
            }
            
            System.out.println("Registration Details...");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EngineNumberTextboxId(),
                testData.TestParameters.get("Engine Number")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ChassisNumberTextboxId(),
                testData.TestParameters.get("Chassis Number")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationNumberTextboxId(),
                generateDateTimeString()))
            {
              return false;
            }
            
            try
            {
                 JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver  ;
                 js.executeScript("VerifyEPLRegistrationNumber();");
            }
            catch(Exception e)
            {
                System.err.println("Failed to verify Registration number");
            }
          
            SeleniumDriverInstance.pause(3000);
            
            //SeleniumDriverInstance.checkPresenceofElementById("popup_ok", "popup_ok");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegOwnerTextBoxId(),
                testData.TestParameters.get("Registered Owner")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationDateTextboxId(),
                testData.TestParameters.get("Registration Date")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NATISCodeDropdownListId(),
                testData.TestParameters.get("NATIS Code")))
            {
              return false;
            }
             System.out.println("Motor Cover...........");
              if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleSumInsuredTextBoxId(),
                testData.TestParameters.get("Vehicle Sum Insured")))
            {
              return false;
            }
             
            System.out.println("Motor Accessories");
            
            if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddAccessoriesButtonName()))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessoryDescriptionTextBoxId(),
            testData.TestParameters.get("Accessory Description")))
            {
              return false;
            }
       
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessorySumInsuredTextBoxId(),
            testData.TestParameters.get("Accessory Sum Insured")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
            {
              return false;
            }
            
            System.out.println("Voluntary Excess");
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.CreditShortfallCheckBoxId(), true))
            {
              return false;
            }   
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ShortfallTextBoxId(),
            testData.TestParameters.get("Shortfall Rate")))
            {
              return false;
            }
             
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryExcessPercentageTextBoxId(),testData.TestParameters.get("Voluntary Excess Percentage")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryMinimumAmountTextBoxId(),testData.TestParameters.get("Voluntary Minimum Amount")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryExcessDiscountTextBoxId(),testData.TestParameters.get("Voluntary Excess Discount")))
            {
              return false;
            }
            
             System.out.println("Additional Compulsory Excess");
            
             if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryExcessPercentageTextBoxId(),testData.TestParameters.get("Compulsory Excess Percentage")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryMinimumAmountTextBoxId(),testData.TestParameters.get("Compulsory Minimum Amount")))
            {
              return false;
            }
            
            SeleniumDriverInstance.takeScreenShot("Motor Details entered successfully", false);
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
            System.out.println("Endorsements");
            
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsSelectButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsAddAllButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsApplyButtonId()))
         {
             return false;
         }
         
         
         SeleniumDriverInstance.takeScreenShot("Endorsement details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        System.out.println("Interested parties");
        
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.MSAddInterestedPartiesButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MSTypeofAgreementTypeDropdownListId(),
                testData.TestParameters.get("Type of Agreement")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.InterestedPartyInstitutionNameDropdownListId(),
                testData.TestParameters.get("Institution Name")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }

            SeleniumDriverInstance.takeScreenShot("Interested parties details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
        
        System.out.println("Financial Overview");
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.PremAdjBasisDropdownListId(),
                testData.TestParameters.get("Premium Adjustment Basis")))
          {
            return false;
          }
        
        SeleniumDriverInstance.clearTextById(EkunduCreateNewPolicyForNewClientPage.FlatPremiumAmountTextBoxId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduCreateNewPolicyForNewClientPage.FlatPremiumAmountTextBoxId(),testData.TestParameters.get("Flat Premium Amount")))
            {
              return false;
            }
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.PremAdjReasonDropdownListId(),
                testData.TestParameters.get("Premium Adjustment Reason")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
        
       
        
        System.out.println("Notes");
        
            
            SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);
        
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        
        System.out.println("Reinsurance Details");
       
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceOkButtonId()))
          {
            return false;
          }

//        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceOkButtonId());

        SeleniumDriverInstance.takeScreenShot("Reinsurance Details entered successfully", false);
            
           return true;

         }
        
  }
