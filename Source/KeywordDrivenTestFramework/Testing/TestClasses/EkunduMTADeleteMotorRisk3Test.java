
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author deanR
 */
public class EkunduMTADeleteMotorRisk3Test extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduMTADeleteMotorRisk3Test(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToFindClientPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find client page", true);

            return new TestResult(testData,  Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the find client page - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findClient())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to find client", true);

            return new TestResult(testData,  Enums.ResultStatus.FAIL,
                                  "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterMidTermAdjustmentData())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Mid Term Adjustment data", true);

            return new TestResult(testData,  Enums.ResultStatus.FAIL,
                                  "Failed to enter Mid Term Adjustment data- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!deleteMotor3Risk())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to delete Motor Risk 3", true);

            return new TestResult(testData,  Enums.ResultStatus.FAIL,
                                  "Failed to delete Motor Risk 3- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

//      if(!quoteRoadsideAssistRisk())
//      {
//          SeleniumDriverInstance.takeScreenShot( "Failed to quote Roadside Assist Risk", true);
//          return new TestResult(testData,false, "Failed to quote Roadside Assist Risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//      }

//        if (!makePolicyLive())
//          {
//            SeleniumDriverInstance.takeScreenShot("Failed to make policy live", true);
//
//            return new TestResult(testData,  Enums.ResultStatus.FAIL,
//                                  "Failed to  make policy live- " + SeleniumDriverInstance.DriverExceptionDetail,
//                                  this.getTotalExecutionTime());
//          }

        SeleniumDriverInstance.takeScreenShot("EEPL MTA Delete 3rd motor risk completed successfully", false);

        return new TestResult(testData,  Enums.ResultStatus.PASS, "EEPL MTA Delete 3rd motor risk completed successfully",
                              this.getTotalExecutionTime());

      }

    private boolean navigateToFindClientPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),
                EKunduHomePage.FindClientTabXPath()))
          {
            return false;
          }

        return true;
      }

    private boolean findClient()
      {
        String clientCode = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                                "Retrieved Client Code");

        if (clientCode.equals("parameter not found"))
          {
            clientCode = testData.TestParameters.get("Client Code");
          }
        else
          {
            testData.updateParameter("Client Code", clientCode);
          }
        
        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
          {
            return false;
          }

        return true;
      }

    private boolean verifyClientDetailsPageHasLoaded()
      {
        if (!SeleniumDriverInstance.validateElementTextValueById(EkunduViewClientDetailsPage.ViewClientDetailsLabelId(),
                "View Client Details"))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Details page successfully loaded", false);

        return true;
      }

    private boolean enterMidTermAdjustmentData()
      {
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementbyLinkText("Change"))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MTATypeofChangeDropDownListId(),
                this.getData("Type of Change")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MIAEffectiveDateTextBoxId(),
                this.getData("Effective Date")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.acceptAlertDialog();

        return true;

      }

    private boolean deleteMotor3Risk()
      {
        SeleniumDriverInstance.pause(5000);

        
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.Motor3RiskDeleteActionDropdownListXpath(),
                    "Delete"))
              {
                return false;
              }
          

        SeleniumDriverInstance.acceptAlertDialog();

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Motor Risk 3 deleted succesfully", false);

        return true;
      }

    private boolean makePolicyLive()
      {
        
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(),
                    "Edit"))
              {
                return false;
              }
          

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.ReinsuranceBandDropDownListId(),
                testData.TestParameters.get("Reinsurance Band")))
          {
            return false;
          }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.scrollElementIntoViewById(EkunduViewClientDetailsPage.MakeLiveButtonId());

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.MakeLiveButtonId()))
          {
            return false;
          }

        // SeleniumDriverInstance.acceptAlertDialog();

        SeleniumDriverInstance.takeScreenShot("Policy made live", false);

        SeleniumDriverInstance.pause(5000);

        return true;
      }

     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
    
  }


//~ Formatted by Jindent --- http://www.jindent.com
