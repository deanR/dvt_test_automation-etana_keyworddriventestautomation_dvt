
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduGlassRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author deanR
 */
public class EkunduPersonalClientRenewalSelectionScenario3Test extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduPersonalClientRenewalSelectionScenario3Test(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToPolicyRenewalPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the Renewal Selection page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the Renewal Selection page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findPolicy())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to find policy", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find policy- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!navigateToFindClientPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find  client page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the find  client page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findClient())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to find client", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterMidTermAdjustmentData())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter mid term adjustment data", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter mid term adjustment data- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!editDomesticandPropertySumInsured())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to edit Domestic and property risk", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to edit Domestic and property risk- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!makePolicyLive())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to finalise client policy", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to finalise client policy- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Renewal selection completed succesfully", this.getTotalExecutionTime());
      }

    private boolean navigateToPolicyRenewalPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.PolicyHoverTabId(),
                EKunduHomePage.RenewalSelectionTabXPath()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.waitForElementById(EKunduHomePage.RenewalSelectionDivId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Navigation to the Renewal Selection page was successful", false);

        return true;
      }

    private boolean findPolicy()
      {
        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase1"),
                                  "Policy Reference Number");

        if (policyNumber.equals("parameter not found"))
          {
            policyNumber = testData.TestParameters.get("PolicyRef");
          }
        else
          {
            testData.updateParameter("Policy Reference Number", policyNumber);
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PolicyReferenceTextBoxId(), policyNumber))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FindNowButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.RenewalSelectionSelectPolicyLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Policy Found", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RenewalSelectionSelectPolicyLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(10000);

        return true;
      }

    private boolean navigateToFindClientPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),
                EKunduHomePage.FindClientTabXPath()))
          {
            return false;
          }

        return true;
      }

    private boolean findClient()
      {
        String clientCode = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                                "Retrieved Client Code");

        if (clientCode.equals("parameter not found"))
          {
            clientCode = testData.TestParameters.get("Client Code");
          }
        else
          {
            testData.updateParameter("Client Code", clientCode);
          }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterMidTermAdjustmentData()
      {
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText()))
          {
            return false;
          }
        
        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase1"),"Policy Reference Number");

        if (policyNumber.equals("parameter not found"))
          {
            policyNumber = testData.TestParameters.get("Policy Number");
          }
        else
          {
            testData.updateParameter("Policy Number", policyNumber);
          }

        if (!SeleniumDriverInstance.changePolicy(policyNumber, "Details"))
          {
            return false;
          }

        return true;
      }

    private boolean editDomesticandPropertySumInsured()
      {
        
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.BuildingsAndContentsRiskActionDropdownListXpath(),
                    "Edit"))
              {
                return false;
              }
          

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduCreateNewPolicyForNewClientPage.BuildingSumInsuredTextBoxId(),
                this.getData("Buildings - Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduGlassRiskPage.RiskItemRateButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.takeScreenShot("Domestic and Property risk data entered successfully", false);

        return true;
      }

    private boolean makePolicyLive()
      {
        
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(),
                    "Edit"))
              {
                return false;
              }
          

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }

      SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

//        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.ReinsuranceBandDropDownListId(),
//                this.getData("Reinsurance Band")))
//          {
//            return false;
//          }

//        SeleniumDriverInstance.pause(3000);

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.scrollDownByOnePage("ctl00_cntMainBody_SummaryCoverCntrl_ctl00_AgentCommissionCntrl_lblAgentDisplay");

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.MakeLiveButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.acceptAlertDialog();

        SeleniumDriverInstance.pause(5000);

        SeleniumDriverInstance.takeScreenShot("Policy made live", false);

        return true;
      }
    
     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

  }


//~ Formatted by Jindent --- http://www.jindent.com
