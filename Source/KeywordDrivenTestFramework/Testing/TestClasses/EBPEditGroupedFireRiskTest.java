
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author FerdinandN
 */
public class EBPEditGroupedFireRiskTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EBPEditGroupedFireRiskTest(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!editFireDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to edit fire details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to edit fire details - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!editFACPropPlacement())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to edit FAC Prop Placement", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to edit FAC Prop Placement - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot("Grouped Fire risk edited successfully", false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Grouped Fire risk edited successfully-", this.getTotalExecutionTime());

      }

    private boolean editFireDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.editGroupedFireRiskLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireOverviewFlatPremiumCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireRiskDataBuildingsCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.FireRiskDataBuildingsRiskSumInsuredTextBoxId(),
                this.getData("Fire - Risk Data - Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.FireRiskDataBuildingsRiskPremiumTextBoxId(),
                this.getData("Fire - Risk Data - Premium")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Fire risk entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        return true;
      }
    
    public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean editFACPropPlacement()
      {
        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.MunichReinsurancePercTextBoxId(),
                this.getData("Reinsurance Details - FAC Placement - Munich Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("FAC Prop Placement edited successfully", false);

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;
      }

//    private String getData(String parameterName)
//      {
//        if (testData.TestParameters.containsKey(parameterName))
//          {
//            return testData.TestParameters.get(parameterName);
//          }
//        else
//          {
//            System.err.println("Parameter - " + parameterName + " was not defined");
//
//            return "";
//          }
//      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
