/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class EkunduMachineryBreakdownTest extends BaseClass 
{
    TestEntity testData;
    TestResult testResultt;

    public EkunduMachineryBreakdownTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyRisksDialogisPresent())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the Risk Dialog is present - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }
        return new TestResult(testData, Enums.ResultStatus.PASS,
                              "Directors And Offices Risk details entered successfully- "
                              + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
    
    private boolean verifyRisksDialogisPresent()
      {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectDirectorsandOfficersLiabilityRiskType();

            return true;
          }
        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectDirectorsandOfficersLiabilityRiskType();

            return true;
          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

      }
    
    private boolean selectDirectorsandOfficersLiabilityRiskType()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType("EBP - Machinery Breakdown / Consequential Loss"))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.takeScreenShot("Directors and offices Risk type selected successfully", false);

        return true;
      }

    private boolean addMachineryItem()
            
    {
        if (!SeleniumDriverInstance.clickElementbyName("ctl00$cntMainBody$MBCL__MACH_ITEMS$ctl02$ctl00"))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.checkBoxSelectionById("ctl00_cntMainBody_MACH_ITEMS__APPL_CL", true))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.checkBoxSelectionById("ctl00_cntMainBody_MACH_ITEMS__APPL_CL", true))
          {
            return false;
          }
        
                  
        if (!SeleniumDriverInstance.clickElementById("changeButton"))
          {
            return false;
          }
        
        SeleniumDriverInstance.pause(3000);
        
        if (!SeleniumDriverInstance.clickElementById("1"))
          {
            return false;
          }
        return true;
        
    }

}
