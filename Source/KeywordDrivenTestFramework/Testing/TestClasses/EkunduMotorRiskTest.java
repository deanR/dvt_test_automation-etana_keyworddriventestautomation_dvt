
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduMotorRiskTest extends BaseClass {

    TestEntity testData;
    TestResult testResultt;

    public EkunduMotorRiskTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        this.setStartTime();

        if (!verifyRisksDialogisPresent()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the risk dialog is present"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify that the riss dialog is present- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterMotorRiskTypeDetails()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk type details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter risk type details- " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterVehicleDetails()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter vehicle details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter vehicle details- " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }
        if (!enterMotorFAP()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter Motor FAP details"), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Motor FAP details- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if (!enterRiskNotes()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk notes"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter risk notes- " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }
//        if (!enterPropFAC())
//          {
//            SeleniumDriverInstance.takeScreenShot(("Failed to enter Prop FAC"), true);
//
//            return new TestResult(testData, Enums.ResultStatus.FAIL,
//                                  "Failed to enter Prop FAC- " + SeleniumDriverInstance.DriverExceptionDetail,
//                                  this.getTotalExecutionTime());
//          }
        SeleniumDriverInstance.takeScreenShot(("Motor risk added successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Motor risk added successfully", this.getTotalExecutionTime());

    }

    private boolean verifyRisksDialogisPresent() {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            selectMotorRiskType();

            return true;
        } else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            selectMotorRiskType();

            return true;

        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

        }

    }

    private boolean selectMotorRiskType() {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (SeleniumDriverInstance.selectRiskType(testData.TestParameters.get("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Motor risk type selected successfully", false);

        return true;

    }

    private boolean enterMotorRiskTypeDetails() {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SearchVehicleModelIconId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehiclesSourceDropdownListId(),
                testData.TestParameters.get("Vehicles Source"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById("2")) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NoClaimRebateDropdownListId(),
                testData.TestParameters.get("No Claim Rebate"))) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleTrackingDeviceDropdownListId(),
                testData.TestParameters.get("Parking"))) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MainDriverDropdownListId(),
                testData.TestParameters.get("Main driver"))) {
            return false;
        }
        SeleniumDriverInstance.takeScreenShot("Vehicle type details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);
        return true;
    }

    private boolean enterVehicleDetails() {
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleRegNumberTextBoxId(),
                generateDateTimeString())) {
            return false;

        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleRegOwnerTextBoxId(),
                testData.TestParameters.get("Registered Owner"))) {
            return false;
        }

        SeleniumDriverInstance.clearTextById(EkunduCreateNewPolicyForNewClientPage.VehicleRegDateTextBoxId());

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleRegDateTextBoxId(),
                testData.TestParameters.get("Original Registration Date"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleSumInsuredTextBoxId(),
                testData.TestParameters.get("Motor Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleCoverTypeDropdownListId(),
                testData.TestParameters.get("Motor Cover Type"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleExtensionsRateTextBoxId(),
                testData.TestParameters.get("Motor Extensions Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleExtensionsPremiumTextBoxId(),
                testData.TestParameters.get("Motor Extensions Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorRateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Vehicle details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterMotorFAP() {
//        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPThirdPartyMinAmountTextboxId(),
//                testData.TestParameters.get("FAP Minimum Amount"))) {
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPTFireandExplosionTextboxId(),
//                testData.TestParameters.get("FAP Minimum Amount"))) {
//            return false;
//        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterRiskNotes() {
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MotorCommentsTextBoxId(),
                testData.TestParameters.get("Motor Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceOkButtonId())) {
            return false;
        }

//        SeleniumDriverInstance.pause(3000);
//        
//        SeleniumDriverInstance.clickElementbyLinkText("Documents");
//        
//         SeleniumDriverInstance.clickElementById("btnGeneratePdf");
//         
//         SeleniumDriverInstance.clickElementById("popup_ok");
//         
//         SeleniumDriverInstance.pause(3000);
//         
//         SeleniumDriverInstance.clickElementbyLinkText("Risks");
        return true;

    }

    private boolean enterPropFAC() {
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddPropFACButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SelectReinsurerLinkId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances")) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        String ReinssuranceSumInsured
                = SeleniumDriverInstance.retrieveTextById(EkunduCreateNewPolicyForNewClientPage.ReinssuranceUnallocatedAmountLabelId());

        try {
            ReinssuranceSumInsured = ReinssuranceSumInsured.substring(0, (ReinssuranceSumInsured.length() - 3));
        } catch (Exception e) {
            System.err.println("Error formatting Reinsurance sum insured - " + e.getMessage());
        }

        System.out.println("Retrieved Reinssurance sum insured - " + ReinssuranceSumInsured);

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduCreateNewPolicyForNewClientPage.ReinssuranceSumInsuredTextBoxId(),
                ReinssuranceSumInsured)) {
            return false;
        }

        /*
         * if(SeleniumDriverInstance.acceptAlertDialog())
         * {
         *   System.err.println("Error entering Reinssurance sum instured, warning dialog detected");
         *   SeleniumDriverInstance.takeScreenShot("Error entering Reinssurance sum instured, warning dialog detected", true);
         *   return false;
         * }
         */
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceOkButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceOkButtonId());

        SeleniumDriverInstance.takeScreenShot("Prop FAC details entered successfully", false);

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
