/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorFleetRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.JavascriptExecutor;

/**
 * 
 * @author DeanR
 */
public class EkunduEBPMTAAddMotorRiskClassicImportsTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduEBPMTAAddMotorRiskClassicImportsTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        this.setStartTime();
        
            if (!verifyRisksDialogisPresent())
            {
              SeleniumDriverInstance.takeScreenShot(("Failed to verify that the risk dialog is present"), true);
              return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the risk dialog is present- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            
            
                if (!enterMotorRiskTypeDetailsClassic_Imports())
              {
                SeleniumDriverInstance.takeScreenShot(("Failed to enter risk type details"), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter risk type details- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
              }
            
           
        SeleniumDriverInstance.takeScreenShot(("Motor risk added successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Motor risk added successfully", this.getTotalExecutionTime());

      }

        private boolean verifyRisksDialogisPresent()
         {
            SeleniumDriverInstance.pause(2000);

            if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
              {
                SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
                selectMotorRiskType();

                return true;
              }

            else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
              {
                selectMotorRiskType();

                return true;

              }

            else
              {
                SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
                System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

                return false;

              }

          }

        private boolean selectMotorRiskType()
         {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());

            if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
              {
                return false;
              }

            if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
              {
                return false;
              }

            if (!SeleniumDriverInstance.switchToDefaultContent())
              {
                return false;
              }

            SeleniumDriverInstance.takeScreenShot("Motor risk type selected successfully", false);

            return true;

          }

         public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
        
        private boolean enterMotorRiskTypeDetailsClassic_Imports()
         {
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
              {
                return false;
              }

            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SearchVehicleModelIconId()))
              {
                return false;
              }

            SeleniumDriverInstance.pause(3000);
            
          
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleSourceDropdownListId(),
                    testData.TestParameters.get("Source")))
                {
                  return false;
                }
                
                SeleniumDriverInstance.pause(3000);
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleMakeOlderVehiclesDropdownListId(),
                    testData.TestParameters.get("Vehicles Make")))
              {
                return false;
              }

            SeleniumDriverInstance.pause(3000);

            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleYearTextBoxId(),
                    testData.TestParameters.get("Vehicles Year")))
              {
                return false;
              }

            SeleniumDriverInstance.pause(3000);

            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleModel2TextBoxId(),
                    testData.TestParameters.get("Vehicles Model")))
              {
                return false;
              }
            
            SeleniumDriverInstance.clearTextById(EkunduCreateNewPolicyForNewClientPage.CCTextBoxId());
            
            if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduCreateNewPolicyForNewClientPage.CCTextBoxId(),
                    testData.TestParameters.get("CC")))
              {
                return false;
              }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.GVMTextBoxId(),
                    testData.TestParameters.get("GVM")))
              {
                return false;
              }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BodyTypeDropdownListId(),
                    testData.TestParameters.get("Body Type")))
              {
                return false;
              }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleTypeDropdownListId(),
                    testData.TestParameters.get("Vehicle Type")))
              {
                return false;
              }

            SeleniumDriverInstance.pause(3000);
            
             if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.VehicleNextButtonId()))
            {
              return false;
            }
             
             System.out.println("Options...");
             
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.EndorsementsCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriversCheckBoxId(), true))
            {
              return false;
            }
//            
//            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MainDriverDropdownListId(),
//                testData.TestParameters.get("Main Driver")))
//            {
//              return false;
//            }
//            
//            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleOvernightParkingDropdownListId(),
//                testData.TestParameters.get("Vehicle parking overnight")))
//            {
//              return false;
//            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
            {
              return false;
            }
            
            System.out.println("Vehicle Cover Details...");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AreaCodeTextboxId(),
                testData.TestParameters.get("Area Code")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorTrackingDeviceDropdownListId(),
                testData.TestParameters.get("Tracking Device")))
            {
              return false;
            }
            
            System.out.println("Registration Details...");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EngineNumberTextboxId(),
                testData.TestParameters.get("Engine Number")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ChassisNumberTextboxId(),
                testData.TestParameters.get("Chassis Number")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationNumberTextboxId(),
                generateDateTimeString()))
            {
              return false;
            }
            
            try
            {
                 JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver  ;
                 js.executeScript("VerifyEPLRegistrationNumber();");
            }
            catch(Exception e)
            {
                System.err.println("Failed to verify Registration number");
            }
          
            SeleniumDriverInstance.pause(3000);
            
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegOwnerTextBoxId(),
                testData.TestParameters.get("Registered Owner")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationDateTextboxId(),
                testData.TestParameters.get("Registration Date")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NATISCodeDropdownListId(),
                testData.TestParameters.get("NATIS Code")))
            {
              return false;
            }
            
            System.out.println("Motor Cover");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleSumInsuredTextBoxId(),
            testData.TestParameters.get("Motor Cover Sum Insured")))
            {
              return false;
            }
            
             System.out.println("Motor Accessories");
            
            if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddAccessoriesButtonName()))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessoryDescriptionTextBoxId(),
            testData.TestParameters.get("Accessory Description")))
            {
              return false;
            }
       
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessorySumInsuredTextBoxId(),
            testData.TestParameters.get("Accessory Sum Insured")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
            {
              return false;
            }
            
             System.out.println("Motor Extensions");
            
            if  (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.ExtensionsExcessWaiverCheckBoxId(), true))
            {
              return false;
            }
                                                
             if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsExcessWaiverTextBoxId(),testData.TestParameters.get("Extensions - Excess Waiver - Premium")))
            {
              return false;
            }
            
              if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ThirdPartyLiabilityRateTextBoxId(),testData.TestParameters.get("Extensions - Third Party Liability - Rate")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
            {
              return false;
            }
            
            System.out.println("Voluntary Excess");
           
             
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryExcessPercentageTextBoxId(),testData.TestParameters.get("Voluntary Excess Percentage")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryMinimumAmountTextBoxId(),testData.TestParameters.get("Voluntary Minimum Amount")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryExcessDiscountTextBoxId(),testData.TestParameters.get("Voluntary Excess Discount")))
            {
              return false;
            }
            
             System.out.println("Additional Compulsory Excess");
            
             if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryExcessPercentageTextBoxId(),testData.TestParameters.get("Compulsory Excess Percentage")))
            {
              return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryMinimumAmountTextBoxId(),testData.TestParameters.get("Compulsory Minimum Amount")))
            {
              return false;
            }
            
            SeleniumDriverInstance.takeScreenShot("Motor Details entered successfully", false);
            
            if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
            System.out.println("Navigating to the First Amount Payable tab...");
            
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPThirdPartyMinimumPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Third Party - Minimum Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPThirdPartyMaximumPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Third Party - Max Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPThirdPardyMinimumAmountTextBoxId(),
                testData.TestParameters.get("FAP - Third Party -  Minimum Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPThirdPardyMaxAmountTextBoxId(),
                testData.TestParameters.get("FAP - Third Party - Max Amount")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPFireAndExplosionMinimumPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Fire and Explosion - Minimum Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPFireAndExplosionMaxPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Fire and Explosion - Max Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPFireAndExplosionMinimumAmountMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Fire and Explosion -  Minimum Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPFireAndExplosionMaxAmountMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Fire and Explosion - Max Amount")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblTheftHijack"))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPHijackMinimumPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Theft Hijack - Minimum Percentage")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPHijackMaximumPercentageMotorChangesTextboxId(),
                testData.TestParameters.get("FAP - Theft Hijack - Max Percentage")))
          {
            return false;
          }
        
         if (!SeleniumDriverInstance.enterTextById(EkunduMotorFleetRiskPage.FAPHijackMinimumAmountTextboxId(),
                testData.TestParameters.get("FAP - Theft Hijack -  Minimum Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPHijackMaxAmountTextBoxId(),
                testData.TestParameters.get("FAP - Theft Hijack - Max Amount")))
          {
            return false;
          }
        
        SeleniumDriverInstance.takeScreenShot("First Amount Payable details entered successfully", false);
        
         if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
            {
              return false;
            }
            
            System.out.println("Endorsements");
            
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsSelectButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsAddAllButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsApplyButtonId()))
         {
             return false;
         }
         
         
         SeleniumDriverInstance.takeScreenShot("Endorsement details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        System.out.println("Specified Driver");
        
        if  (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddSpecifiedDriverButtonName()))
        {
            return false;
        }
      //enter driver name
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverNameTextBoxId(),
              testData.TestParameters.get("Driver")))
        {
            return false;
        }
      //select id type
      if  (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverIDTypeDropdownId(),
              testData.TestParameters.get("ID Type")))
        {
            return false;
        }
      //enter id number
      if  (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverIDNumberTextBoxId(), 
              testData.TestParameters.get("ID Number")))
        {
            return false;
        }
      //click next driver button
      if  (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BasicDetailsNextButtonName()))
        {
            return false;
        }
        
        System.out.println("Interested parties");
        
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.MSAddInterestedPartiesButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MSTypeofAgreementTypeDropdownListId(),
                testData.TestParameters.get("Type of Agreement")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.InterestedPartyInstitutionNameDropdownListId(),
                testData.TestParameters.get("Institution Name")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartyAddNotesLabelId()))
          {
            return false;
          }
                  
        if (!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.InterestedPartyNotesTextBoxId(),
                testData.TestParameters.get("Interested Party Comments")))
          {
            return false;
          }

            SeleniumDriverInstance.takeScreenShot("Interested parties details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
        
        System.out.println("Financial Overview");
        
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.PremAdjBasisDropdownListId(),
                testData.TestParameters.get("Premium Adjustment Basis")))
          {
            return false;
          }
        
        SeleniumDriverInstance.clearTextById(EkunduCreateNewPolicyForNewClientPage.FlatPremiumAmountTextBoxId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduCreateNewPolicyForNewClientPage.MotorSpecifiedAdjustmentPercentageTextBoxId(),testData.TestParameters.get("Financial Overview - Adjustment Percentage")))
            {
              return false;
            }
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.PremAdjReasonDropdownListId(),
                testData.TestParameters.get("Premium Adjustment Reason")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
        
        
        System.out.println("Notes");
        
//        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedNotesTextBoxId(), testData.TestParameters.get("Motor Specified Comments")))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedAddNotesCheckboxId(), true))
//         {
//             return false;
//         }
//        
//        if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.MotorSpecifiedPolicyNotesTextBoxId(), testData.TestParameters.get("Motor Specified Notes")))
//         {
//             return false;
//         }
            
            SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);
        
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
         {
             return false;
         }
        
        
        System.out.println("FAC");
       
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BandDropdownListId(),
                testData.TestParameters.get("Reinsurance Band")))
          {
            return false;
          }
        
        SeleniumDriverInstance.pause(2000);
        
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddPropFACButtonId()))
          {
            return false;
          }
            SeleniumDriverInstance.pause(3000);
            
            if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(),
                this.getData("FAC Prop - Addison")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

//        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId());
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceSumInsuredTextBoxId(),
                this.getData("Unallocated Amount")))
          {
            return false;
          }
        
         SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
          {
            return false;
          }

        SeleniumDriverInstance.pause(3000);

        System.out.println("Company 1 added successfully");
        
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
          {
            return false;
          }

//        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceOkButtonId()))
          {
            return false;
          }

//        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceOkButtonId());

        SeleniumDriverInstance.takeScreenShot("Prop FAC details entered successfully", false);
            
           return true;

       }
      
//      public String generateDateTimeString()
//    {
//      Date dateNow = new Date( );
//      SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd_hh-mm-ss");
//      
//      return dateFormat.format(dateNow).toString();
//    }
        
  }
