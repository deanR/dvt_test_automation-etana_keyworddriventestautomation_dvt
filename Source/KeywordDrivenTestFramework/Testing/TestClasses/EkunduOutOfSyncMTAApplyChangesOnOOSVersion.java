/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author FerdinandN
 */
public class EkunduOutOfSyncMTAApplyChangesOnOOSVersion extends BaseClass
{
    TestEntity testData;
    TestResult testResult;
    
    public EkunduOutOfSyncMTAApplyChangesOnOOSVersion(TestEntity testData)
    {
        this.testData = testData;

    }
   
    public TestResult executeTest()
    {  
         SeleniumDriverInstance.DriverExceptionDetail = "";
         this.setStartTime();
         
        if(!initiateMTAChanges1())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to initiate MTA changes", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to initiate MTA changes- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!applyMTAChangesOnEBP_GroupedFire())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to apply MTA Changes On EBP_GroupedFire risk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to apply MTA Changes On EBP_GroupedFire risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
//         if(!applyMTAChangesOnEBP_MoneyRisk())
//        {
//            SeleniumDriverInstance.takeScreenShot( "Failed to apply MTA Changes On EBP_money risk", true);
//            return new TestResult(testData,false, "Failed to apply MTA Changes On EBP_money risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//        }
         
         if(!editEBP_BusinessAllRisk())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to edit EBP_BusinessAllRisk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to edit EBP_BusinessAllRisk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!quoteEBPRoadsideAssist())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to quote EBP Roadside Assist", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to quote EBP Roadside Assist- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
          if(!initiateMTAChanges2())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to initiate MTA changes", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to initiate MTA changes- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!applyMTAChangesOnEBP_GroupedFire())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to apply MTA Changes On EBP_GroupedFire risk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to apply MTA Changes On EBP_GroupedFire risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
//         if(!applyMTAChangesOnEBP_MoneyRisk())
//        {
//            SeleniumDriverInstance.takeScreenShot( "Failed to apply MTA Changes On EBP_money risk", true);
//            return new TestResult(testData,false, "Failed to apply MTA Changes On EBP_money risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//        }
         
         if(!editEBP_BusinessAllRisk())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to edit EBP_BusinessAllRisk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to edit EBP_BusinessAllRisk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!quoteEBPRoadsideAssist())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to quote EBP Roadside Assist", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to quote EBP Roadside Assist- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!initiateMTAChanges3())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to initiate MTA changes", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to initiate MTA changes- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!applyMTAChangesOnEBP_GroupedFire())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to apply MTA Changes On EBP_GroupedFire risk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to apply MTA Changes On EBP_GroupedFire risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
//         if(!applyMTAChangesOnEBP_MoneyRisk())
//        {
//            SeleniumDriverInstance.takeScreenShot( "Failed to apply MTA Changes On EBP_money risk", true);
//            return new TestResult(testData,false, "Failed to apply MTA Changes On EBP_money risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//        }
         
         if(!editEBP_BusinessAllRisk())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to edit EBP_BusinessAllRisk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to edit EBP_BusinessAllRisk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!quoteEBPRoadsideAssist())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to quote EBP Roadside Assist", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to quote EBP Roadside Assist- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!initiateMTAChanges4())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to initiate MTA changes", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to initiate MTA changes- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!applyMTAChangesOnEBP_GroupedFire())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to apply MTA Changes On EBP_GroupedFire risk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to apply MTA Changes On EBP_GroupedFire risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
//         if(!applyMTAChangesOnEBP_MoneyRisk())
//        {
//            SeleniumDriverInstance.takeScreenShot( "Failed to apply MTA Changes On EBP_money risk", true);
//            return new TestResult(testData,false, "Failed to apply MTA Changes On EBP_money risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//        }
         
         if(!editEBP_BusinessAllRisk())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to edit EBP_BusinessAllRisk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to edit EBP_BusinessAllRisk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!quoteEBPRoadsideAssist())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to quote EBP Roadside Assist", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to quote EBP Roadside Assist- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
         if(!initiateMTAChanges5())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to initiate MTA changes", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to initiate MTA changes- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!applyMTAChangesOnEBP_GroupedFire())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to apply MTA Changes On EBP_GroupedFire risk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to apply MTA Changes On EBP_GroupedFire risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
//         if(!applyMTAChangesOnEBP_MoneyRisk())
//        {
//            SeleniumDriverInstance.takeScreenShot( "Failed to apply MTA Changes On EBP_money risk", true);
//            return new TestResult(testData,false, "Failed to apply MTA Changes On EBP_money risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//        }
         
         if(!editEBP_BusinessAllRisk())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to edit EBP_BusinessAllRisk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to edit EBP_BusinessAllRisk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!quoteEBPRoadsideAssist())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to quote EBP Roadside Assist", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to quote EBP Roadside Assist- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
     
         if(!initiateMTAChanges6())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to initiate MTA changes", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to initiate MTA changes- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!applyMTAChangesOnEBP_GroupedFire())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to apply MTA Changes On EBP_GroupedFire risk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to apply MTA Changes On EBP_GroupedFire risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
//         if(!applyMTAChangesOnEBP_MoneyRisk())
//        {
//            SeleniumDriverInstance.takeScreenShot( "Failed to apply MTA Changes On EBP_money risk", true);
//            return new TestResult(testData,false, "Failed to apply MTA Changes On EBP_money risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//        }
         
         if(!editEBP_BusinessAllRisk())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to edit EBP_BusinessAllRisk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to edit EBP_BusinessAllRisk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!quoteEBPRoadsideAssist())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to quote EBP Roadside Assist", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to quote EBP Roadside Assist- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
         if(!makeOOSLive())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to make OOS live", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to make OOS live- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
   
          return new TestResult(testData, Enums.ResultStatus.PASS, "OOS changes completed successfully ", this.getTotalExecutionTime());
    }
    
    
    
    private boolean initiateMTAChanges1()
    {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.BackdatedEditPolicy1LinkId())) 
        {
            return false;
        }
        return true;
        
    }
    
    private boolean initiateMTAChanges2()
    {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.BackdatedEditPolicy2LinkId())) 
        {
            return false;
        }
         return true;
    }
        
    private boolean initiateMTAChanges3()
    {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.BackdatedEditPolicy3LinkId())) 
        {
            return false;
        }
         return true;
    }
    
    private boolean initiateMTAChanges4()
    {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.BackdatedEditPolicy4LinkId())) 
        {
            return false;
        }
         return true;
    }
    
    private boolean initiateMTAChanges5()
    {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.BackdatedEditPolicy5LinkId())) 
        {
            return false;
        }
         return true;
    }
    
    private boolean initiateMTAChanges6()
    {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.BackdatedEditPolicy6LinkId())) 
        {
            return false;
        }
        return true;
    } 

    private boolean applyMTAChangesOnEBP_GroupedFire() 
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.GroupedFireRiskActionDropdownListXpath(), "Edit"))
        {
            return false;
        }
        
        
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) 
        {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) 
        {
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) 
        {
            return false;
        }


        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.FireRiskDataBuildingsRiskSumInsuredTextBoxId(), this.getData("GF - Fire - Risk Data - Sum Insured")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.FireRiskDataBuildingsRiskPremiumTextBoxId(), this.getData("GF - Fire - Risk Data - Premium")))
        {
            return false;
        }
        
        SeleniumDriverInstance.takeScreenShot("Grouped Fire risk edited successfully", false);
        
                   
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
        {
            return false;
        }
        
         if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.MunichReinsurancePercTextBoxId(), this.getData("Reinsurance - Munich Africa - This Percentage")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsLabelId()))
           {
               return false;
           }  
         
         
         SeleniumDriverInstance.pause(2000);
         
         if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
        {
            return false;
        }
                           
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId()))
        {
                return false;
        }
        
        
        SeleniumDriverInstance.takeScreenShot("MTA Changes On EBP Grouped fire Applied successfully", false);

        
        return true;

    }
    
    private boolean applyMTAChangesOnEBP_MoneyRisk()
    {        
        if(SeleniumDriverInstance.checkRiskActionElement())
        {
            if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.editMoneyRiskLinkId()))
            {
                return false;
            }
        }
        else
        {
            if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.MoneyRiskActionDropdownListXpath(), "Edit"))
            {
                return false;
            }
        }
        
        SeleniumDriverInstance.pause(20000);
        
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.MoneyCoverMajorLimitSumInsuredTextBoxId(), this.getData("Cover - Major Limit - Sum Insured")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.MoneyCoverMajorLimitPremiumTextBoxId(), this.getData("Cover - Major Limit - Premium")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
                           
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId()))
        {
                return false;
        }

        SeleniumDriverInstance.takeScreenShot("MTA Changes On EBP Money Applied successfully", false);
        
           return true; 
        }
    
    private boolean clickAddRiskItemButton()
    {       
        WebElement cell = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_BAR__ITEM\"]/table/tfoot/tr/td[7]/input[@value = \"Add\"]"));
        if(cell != null)
            {         
                cell.click();
                System.out.println("[TestInfo] Add Risk Item Button Clicked");
                return true;
            }
        else
            {
                System.err.println("[Error] Failed to click add risk item button"); 
                return false;
            }

    }
    
    private boolean editEBP_BusinessAllRisk()
    {  
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.BusinessAllRiskActionDropdownListXpath(), "Edit"))
        {
            return false;
        }
         
        //SeleniumDriverInstance.Driver.navigate().refresh();
         
         SeleniumDriverInstance.pause(1000);
        
//        if(!SeleniumDriverInstance.clickElementbyXpath(EkunduBusinessAllRiskPage.AddRiskItemButtonXpath()))
//        {
//            return false;
//        }
//        
         if (!SeleniumDriverInstance.clickElementbyName(EkunduBusinessAllRiskPage.BusinessAllRiskItemAddItemButtonName2())) {
            return false;
        }
         SeleniumDriverInstance.pause(1000);
        //this.clickAddRiskItemButton();
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduBusinessAllRiskPage.BusinessAllRiskItemCategoryDropdownListId(), this.getData("Business All Risk Item Category")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemSumInsuredTextboxId(), this.getData("Business All Risk Item Sum Insured")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskItemDescriptionTextboxId(), this.getData("Business All Risk Item Description")))
        {
            return false;
        }
        
           
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumCheckboxId(), true))
        {
            return false;
        } 
        
        if(!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.RiskItemFlatPremiumTextboxId(), this.getData("Risk Item - Flat Premium")))
        {
            return false;
        } 
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
        {
            return false;
        }
                           
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.takeScreenShot("Business Risk Item details entered successfully", false);

            return true; 
        }
    
    private boolean quoteEBPRoadsideAssist()
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(), "Edit"))
        {
            return false;
        }
        
        
            SeleniumDriverInstance.pause(2000);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
        {
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
           
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.takeScreenShot("Road Side Assist quoted successfully", false);
    
        SeleniumDriverInstance.scrollElementIntoViewById(EkunduViewClientDetailsPage.SaveQuoteButtonId());
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SaveQuoteButtonId()))
        {
            return false;
        }
        

            return true; 
    }
    
    private boolean makeOOSLive()
    {
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OOSOKButtonId()))
        {
            return false;
        }
           
           SeleniumDriverInstance.pause(5000);
           
           SeleniumDriverInstance.acceptAlertDialog();
           
           SeleniumDriverInstance.takeScreenShot("Out of Sync cancellation made live", false);
           return true;
    }
    
     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
   
}
