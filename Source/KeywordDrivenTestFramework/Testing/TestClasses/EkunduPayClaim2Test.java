
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.CreateNewClaimPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMaintainCorporateClientClaim1Page;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author FerdinandN
 */
public class EkunduPayClaim2Test extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduPayClaim2Test(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!navigateToFindClaimPage())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to navigate to the find claim page"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the find claim page- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findClaim())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to find claim"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find claim- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!InitiateClaimPayment())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to initiate claim payment"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to initiate claim payment- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterClaimPayment())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to complete claim payment"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to complete claim payment- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Corporate client claim mantenance completed successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Corporate client claim mantenance completed successfully",
                              this.getTotalExecutionTime());

      }
    
     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean navigateToFindClaimPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EkunduMaintainCorporateClientClaim1Page.ClaimHoverTabId(),
                EkunduMaintainCorporateClientClaim1Page.ClaimSearchLinkXpath()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.waitForElementById(EkunduMaintainCorporateClientClaim1Page.ClaimReferenceTextBoxId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Navigation to the Find Claim page was successful", false);

        return true;
      }

    private boolean findClaim()
      {
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.OkButtonId()))
          {
            return false;
          }

        String claimref = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                              "Retrieved Corporate Claim Number");

        if (claimref.equals("parameter not found"))
          {
            claimref = testData.TestParameters.get("Claim Reference Number");
          }
        else
          {
            testData.updateParameter("Claim Reference Number", claimref);
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.ClaimReferenceTextBoxId(),
                claimref))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimFindNowButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.waitForElementById(EkunduMaintainCorporateClientClaim1Page.PayClaimLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Claim found", false);

        return true;

      }

    private boolean InitiateClaimPayment()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.PayClaimLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMaintainCorporateClientClaim1Page.ClaimReasonforChangeDropdowlistId(),
                this.getData("Reason for Change")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimOkButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.scrollDownByOnePage("Div6");

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Claim payment initiated successfully", false);

        return true;
      }

    private boolean enterClaimPayment()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.PerilsBuildingsDetailsLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduMaintainCorporateClientClaim1Page.PaymentDetailsTabLinkText()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.PaymentDetailsInsuredRadioButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.PaymentDetailsOwnDmageEditLinkId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToFrameById(EkunduMaintainCorporateClientClaim1Page.PaymentDetailsDialogId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduMaintainCorporateClientClaim1Page.PaymentDetailsPaymentAmountTextboxId(),
                this.getData("Payment Amount")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMaintainCorporateClientClaim1Page.PaymentDetailsTaxGroupDropdownListId(),
                this.getData("Tax Group")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.PaymentDetailsOkButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduMaintainCorporateClientClaim1Page.PaymentDetailsThisPaymentTabLinkTextId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMaintainCorporateClientClaim1Page.PaymentDetailsMediaTypeDropdownListId(),
                this.getData("Media Type")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimFinishButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimFinishButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimSubmitButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ReinsuranceOkButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Claim payment completed successfully", false);

        return true;

      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
