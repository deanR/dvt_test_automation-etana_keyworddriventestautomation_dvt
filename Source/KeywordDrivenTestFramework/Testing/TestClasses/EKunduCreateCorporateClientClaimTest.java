
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.CreateNewClaimPage;
import org.openqa.selenium.JavascriptExecutor;

//~--- JDK imports ------------------------------------------------------------
/**
 *
 * @author FerdinandN
 */
public class EKunduCreateCorporateClientClaimTest extends BaseClass {

    TestEntity testData;
    TestResult testResultt;

    public EKunduCreateCorporateClientClaimTest(TestEntity testData) {
        this.testData = testData;

    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToClaimPage()) {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the Claim Cases page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to navigate to the Claim Cases page - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!verifyClaimCasePageHasLoaded()) {
            SeleniumDriverInstance.takeScreenShot("Claim Cases page failed to load", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Claim Cases page failed to load - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!clickOk()) {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Ok button was not clicked - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!selectClaimsChampion()) {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Claims champion not selected - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!submitClaimDetails()) {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Claim details not submitted" + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!OpenClaim()) {
            SeleniumDriverInstance.takeScreenShot("Claim could not be found", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Claim could not be found" + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        SeleniumDriverInstance.clearTextById2(CreateNewClaimPage.lossDateTextBoxId());

        if (!findPolicy()) {
            SeleniumDriverInstance.takeScreenShot("Policy could not be found", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Policy could not be found" + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!clickFindNowButtonId()) {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Find now button was not clicked - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!selectPolicyId()) {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Policy was not selected - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterClaimOverviewDetails()) {
            SeleniumDriverInstance.takeScreenShot("Claim overview details could not be entered", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Claim overview details could not be entered"
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterPremiumDetails()) {
            SeleniumDriverInstance.takeScreenShot("Premium details could not be entered", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Premium details could not be entered"
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterClaimPerils()) {
            SeleniumDriverInstance.takeScreenShot("Claim perils could not be entered", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Claim perils could not be entered" + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!verifyClaimHasBeenOpened()) {
            SeleniumDriverInstance.takeScreenShot("Claim could not be opened", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Claim could not be opened" + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Corporate Client Claim successfully opened",
                this.getTotalExecutionTime());
    }

    private boolean navigateToClaimPage() {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(CreateNewClaimPage.claimHoverTabId(),
                CreateNewClaimPage.newCaseLinkXPath())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Navigation to the claims page was successful", false);

        return true;
    }

    private boolean verifyClaimCasePageHasLoaded() {
        try {
            if (!SeleniumDriverInstance.waitForElementById(CreateNewClaimPage.contentSpanId())) {
                return false;
            }

            SeleniumDriverInstance.takeScreenShot("Claims page loaded successfully", false);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean clickOk() {
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.OkButtonId())) {
            return false;
        }

        return true;
    }

    private boolean selectClaimsChampion() {
        SeleniumDriverInstance.WaitUntilDropDownListPopulatedById(CreateNewClaimPage.claimsChampionDropDownListId());

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.claimsChampionDropDownListId(),
                testData.TestParameters.get("Claims Champion"))) {
            return false;
        }

        return true;
    }

    private boolean submitClaimDetails() {
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.submitButtonId())) {
            return false;
        }

        return true;
    }

    private boolean OpenClaim() {
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.openClaimButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Claim opened successfully", false);

        return true;

    }

    private boolean findPolicy() {
        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                "Retrieved Policy Number");

        if (policyNumber.equals("parameter not found")) {
            policyNumber = testData.TestParameters.get("Policy Number");
        } else {
            testData.updateParameter("Policy Number", policyNumber);
        }

        if (!SeleniumDriverInstance.enterTextById(CreateNewClaimPage.policynumberTextBoxId(), policyNumber)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(CreateNewClaimPage.lossDateTextBoxId(),
                testData.TestParameters.get("Loss Date"))) {
            return false;
        }

        return true;

    }

    private boolean clickFindNowButtonId() {
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.findnowButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.waitForElementById(CreateNewClaimPage.SelectPolicyLinkId())) {
            SeleniumDriverInstance.takeScreenShot("Policy doesn't exist", true);

            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Policy found", false);

        return true;
    }

    private boolean selectPolicyId() {
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.SelectPolicyLinkId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Policy selected", false);

        return true;
    }

    private boolean enterClaimOverviewDetails() {
        SeleniumDriverInstance.pause(3000);
        SeleniumDriverInstance.WaitUntilDropDownListPopulatedById(CreateNewClaimPage.RiskTypeDropdownListId());

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.RiskTypeDropdownListId(),
                testData.TestParameters.get("Risk Type"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.ProgressStatusDropdownListId(),
                testData.TestParameters.get("Progress Status"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.PrimaryCauseDropdownListId(),
                testData.TestParameters.get("Primary Cause"))) {
            return false;
        }

        JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;

        js.executeScript("__doPostBack('UpdateSecondaryCause', '');setTimeout('__doPostBack(\\'ctl00$cntMainBody$CONTROL__PRIMARY_CAUSE\\',\\'\\')', 0)");

        SeleniumDriverInstance.WaitUntilDropDownListPopulatedById(CreateNewClaimPage.SecondaryCauseDropdownListId());

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.SecondaryCauseDropdownListId(),
                testData.TestParameters.get("Secondary Cause"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.ClaimsHandlerDropdownListId(),
                testData.TestParameters.get("Claims Handler"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(CreateNewClaimPage.ClaimDescriptionTextBoxId(),
                testData.TestParameters.get("Claim Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_CONTROL__REPORTED_DATE")) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        SeleniumDriverInstance.clearTextById("ctl00_cntMainBody_CONTROL__REPORTED_DATE");
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_CONTROL__REPORTED_DATE",testData.TestParameters.get("Report Date"))) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);
        SeleniumDriverInstance.takeScreenShot("Claim overview details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterPremiumDetails() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.PremiumConfirmerDropdownListId(),
                testData.TestParameters.get("Premium Confirmer"))) {
            return false;
        }

        JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;

        js.executeScript("document.getElementById('" + CreateNewClaimPage.ClaimNextButtonId() + "').click();");

        SeleniumDriverInstance.takeScreenShot("Premium details entered successfully", false);

        SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

        SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

        SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

        SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

        SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

        SeleniumDriverInstance.checkPresenceofElementById(CreateNewClaimPage.LoadReserveEditLinkId(),
                CreateNewClaimPage.ClaimNextButtonId());

        return true;
    }

    private boolean enterClaimPerils() {
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.LoadReserveEditLinkId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(CreateNewClaimPage.ClaimReserverOwnDamageNewValueTextBoxId(),
                testData.TestParameters.get("Reserve New Value"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ReserverFinishButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ReserverFinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementById(CreateNewClaimPage.PerilsSubmitButtonId());

        /*
         *      JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;
         *
         * js.executeScript("document.getElementById('"+ CreateNewClaimPage.PerilsSubmitButtonId() + "').click();");
         */
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimReinsuranceOkButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(5000);

        SeleniumDriverInstance.clickElementbyXpath(CreateNewClaimPage.ClaimConfirmationNoButtonXpath());

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.clickElementbyXpath(CreateNewClaimPage.ClaimConfirmationNoButtonXpath());

        SeleniumDriverInstance.takeScreenShot("Claim perils entered successfully", false);

        return true;
    }

    private boolean verifyClaimHasBeenOpened() {
//      if(!SeleniumDriverInstance.waitForElementById(CreateNewClaimPage.ClaimNumberSpanId()))
//       {
//          return false;
//       } 

        testData.addParameter("Retrieved Corporate Claim Number",
                SeleniumDriverInstance.retrieveTextById(CreateNewClaimPage.ClaimNumberSpanId()));

        SeleniumDriverInstance.takeScreenShot("Corporate Claim opened successfully", false);

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
