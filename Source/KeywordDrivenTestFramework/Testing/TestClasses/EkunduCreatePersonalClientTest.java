/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreatePersonalClientPage;

/**
 *
 * @author ferdinandn
 */
public class EkunduCreatePersonalClientTest extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public EkunduCreatePersonalClientTest(TestEntity testData) {
        this.testData = testData;

    }

    public TestResult executeTest() {
        this.setStartTime();

        if (!navigateToPersonalClientCreationPage()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to navigate to the Personal client creation page "), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to navigate to the Personal client creation page - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterBasicDetails()) {
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter basic details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if (!navigateToAddressDetailsTab()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to navigate to the address details tab "), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to navigate to the address details tab  - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterCorrespondantAddressDetails()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter correspondent address "), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter correspondent address  - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        SeleniumDriverInstance.pause(5000);
        if (!enterHomeAddressDetails()) {
            SeleniumDriverInstance.takeScreenShot(("Could not enter Home address suburb "), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Could not enter Home address suburb  - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!closeAddressDialog()) {
            SeleniumDriverInstance.takeScreenShot("Failed to close the Addresses dialog", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to close the Addresses dialog  - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!verifyAddressesHaveBeenAdded()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that addresses have been added", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that addresses have been added  - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if (!navigateToTaxDetailsTab()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to navigate to the tax details tab"), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to navigate to the tax details tab  - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterTaxDetails()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter tax details"), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter tax details  - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!navigateToBankDetailsTab()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to navigate to the bank details tab"), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to navigate to the bank details tab  - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterBankDetails()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter bank details"), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter bank details  - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!submitPersonalClientDetails()) {

            return new TestResult(testData, Enums.ResultStatus.FAIL, "Personal Client details submitted successfully  - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!updateClientDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to click the update client details button n", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to click the update client details button  - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!verifyClientDetailsHaveBeenSubmitted()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify client details submittion", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify client details submittion  - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        SeleniumDriverInstance.takeScreenShot(("Personal Client creation completed successfully"), false);
        return new TestResult(testData, Enums.ResultStatus.PASS, "Personal Client creation completed successfully", this.getTotalExecutionTime());

    }

    // <editor-fold defaultstate="collapsed" desc="verifyHomePageHasLoaded">
    private boolean verifyHomePageHasLoaded() {
        try {
            if (!SeleniumDriverInstance.waitForElementById(EKunduHomePage.HomeDivId())) {
                return false;
            } else {
                SeleniumDriverInstance.takeScreenShot("Home page successfully loaded", false);
            }
            return true;

        } catch (Exception e) {
            return false;
        }
    }
     // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="NavigateToCreatePersonalClientPage">
    private boolean navigateToPersonalClientCreationPage() {
        if (!SeleniumDriverInstance.navigateTo(EKunduHomePage.ClientHoverTabId(), "New Personal Client")) {
            return false;
        }

//        if(!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),EKunduHomePage.NewPersonalClientXPath()))
//        {
//            return false;
//        }
        return true;
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="EnterBasicDetails">
    private boolean enterBasicDetails() {
        SeleniumDriverInstance.takeScreenShot("Navigation to the Create Personal client page was successful", false);
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreatePersonalClientPage.titleDropDownListId(), testData.TestParameters.get("Title"))) {
            return false;
        }
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreatePersonalClientPage.genderDropDownListId(), testData.TestParameters.get("Gender"))) {
            return false;
        }
        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.firstNameTextBoxId(), testData.TestParameters.get("First Name"))) {
            return false;
        }
        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.lastNameTextBoxId(), testData.TestParameters.get("Last Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.initialsTextBoxId(), testData.TestParameters.get("Initials"))) {
            return false;
        }

        SeleniumDriverInstance.clearTextById(EkunduCreatePersonalClientPage.dobTextBoxId());
        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.dobTextBoxId(), testData.TestParameters.get("DOB"))) {
            return false;
        }
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreatePersonalClientPage.nationalityDropDownListId(), testData.TestParameters.get("Nationality"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.taxRegNoTextBoxId(), testData.TestParameters.get("Tax Registration NO"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreatePersonalClientPage.maritalStatusDropDownListId(), testData.TestParameters.get("Marital Status"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreatePersonalClientPage.paymentCurrencyDropDownListId(), testData.TestParameters.get("Payment  Currency"))) {
            return false;
        }
        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.vatNoTextBoxId(), testData.TestParameters.get("VAT Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.idNoTextBoxId(), testData.TestParameters.get("ID Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreatePersonalClientPage.idTypeDropDownListId(), testData.TestParameters.get("ID Type Required"))) {
            return false;
        }
        return true;
    }
       // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="NavigatetoAddressDetailsTab">
    private boolean navigateToAddressDetailsTab() {
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduCreatePersonalClientPage.addressesTabPartialLinkText())) {
            return false;
        }
        return true;
    }

     // </editor-fold> 
    // </editor-fold> 
    // <editor-fold defaultstate="collapsed" desc="verifyAddressesHaveBeenAdded">
    private boolean verifyAddressesHaveBeenAdded() {
        if (!SeleniumDriverInstance.waitForElementById(EkunduCreatePersonalClientPage.CorrespondentAddressEditLinkId())) {
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementById(EkunduCreatePersonalClientPage.HomeAddressEditLinkId())) {
            return false;
        }
        SeleniumDriverInstance.takeScreenShot("Addresses added successfully", true);
        return true;
    }
      // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="seachCorrespondentAddressDetails">
    private boolean enterCorrespondantAddressDetails() {

        SeleniumDriverInstance.takeScreenShot("Navigation to the address details tab was successful", false);

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreatePersonalClientPage.AddressAddLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameById(EkunduCreatePersonalClientPage.AddressFrameId())) {
            return false;
        }

        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.addressLine1TextBoxId(), testData.TestParameters.get("Correspondance Address Line 1"))) {
            return false;
        }
         SeleniumDriverInstance.pause(15000);
        if (!SeleniumDriverInstance.ArrowdownToElementById(EkunduCreatePersonalClientPage.addressLine1TextBoxId())) {
            return false;
        }
        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.AddressAddButtonId())) {
            return false;
        }
        return true;
    }
   // </editor-fold>     

    // <editor-fold defaultstate="collapsed" desc="enterHomeAddressDetails">
    private boolean enterHomeAddressDetails() {

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreatePersonalClientPage.addressTypeDropDownListId(), testData.TestParameters.get("Address Type"))) {
            return false;
        }

//
//        if (!SeleniumDriverInstance.switchToFrameById(EkunduCreatePersonalClientPage.AddressFrameId())) {
//            return false;
//        }
        SeleniumDriverInstance.pause(5000);
        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.addressLine1TextBoxId(), testData.TestParameters.get("Home Address Line 1"))) {
            return false;
        }

        SeleniumDriverInstance.pause(15000);
        if (!SeleniumDriverInstance.ArrowdownToElementById(EkunduCreatePersonalClientPage.addressLine1TextBoxId())) {
            return false;
        }
        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.AddressAddButtonId())) {
            return false;
        }
        return true;
    }
      // </editor-fold>  

    // <editor-fold defaultstate="collapsed" desc="searchHomeAddressDetails">
    // <editor-fold defaultstate="collapsed" desc="closeAddressDialog">
    private boolean closeAddressDialog() {
        SeleniumDriverInstance.pause(2000);
        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(EkunduCreatePersonalClientPage.CloseAddressDialog2ButtonXPath())) {
            return false;
        }

        return true;

    }
         // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="navigateToTaxDetailsTab">
    private boolean navigateToTaxDetailsTab() {
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduCreatePersonalClientPage.taxTabPartialLinkText())) {
            return false;
        }
        return true;
    }
          // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="enterTaxDetails">
    private boolean enterTaxDetails() {
        SeleniumDriverInstance.takeScreenShot("Navigation to the Tax details tab was successful", false);

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.taxNoTextBoxId(), testData.TestParameters.get("Tax Number"))) {
            return false;
        }

        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.taxpercentageTextBoxId(), testData.TestParameters.get("Tax Percentage"))) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Tax details entered successfully", false);
        return true;
    }
       // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="navigateToBankDetailsTab">
    private boolean navigateToBankDetailsTab() {
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduCreatePersonalClientPage.bankDetailsTabPartialLinkText())) {
            return false;
        }

        return true;
    }
          // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="enterBankDetails">
    private boolean enterBankDetails() {
        SeleniumDriverInstance.takeScreenShot("Navigationto the bank details tab was successful", false);
        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.addBankLinkId())) {
            return false;
        }

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.switchToLastDuplicateFrameById(EkunduCreatePersonalClientPage.BankFrameId())) {
            return false;
        }

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreatePersonalClientPage.bankPaymentTypeDropDownListId(), testData.TestParameters.get("Bank Payment Type"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.accountHolderNameTextBoxId(), testData.TestParameters.get("Account Holder Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.accountTypeTextBoxId(), testData.TestParameters.get("Account Type"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.accountNoTextBoxId(), testData.TestParameters.get("Account Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreatePersonalClientPage.branchCodeTextBoxId(), testData.TestParameters.get("Bank Branch Code"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreatePersonalClientPage.bankNameDropDownListId(), testData.TestParameters.get("Bank Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.addBankButtonId())) {
            return false;
        }
        if (!SeleniumDriverInstance.acceptAlertDialog()) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementById(EkunduCreatePersonalClientPage.SelectBankDetailsLinkId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Bank details entered successfully", false);

        return true;
    }
       // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="submitPersonalClientDetails">
    private boolean submitPersonalClientDetails() {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.submitClientButtonId())) {
            return false;
        }
        SeleniumDriverInstance.pause(5000);
        SeleniumDriverInstance.takeScreenShot("Personal Client details submitted successfully", false);

        return true;
    }
        // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="updateClientDetails">
    private boolean updateClientDetails() {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreatePersonalClientPage.updateClientButtonId())) {
            return false;
        }
        return true;
    }
         // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="verifyClientDetailsHvaveBeenSubmitted">
    private boolean verifyClientDetailsHaveBeenSubmitted() {
        if (!SeleniumDriverInstance.validateElementTextValueById(EkunduCreatePersonalClientPage.clientCodeSpanId(), SeleniumDriverInstance.retrieveTextById(EkunduCreatePersonalClientPage.clientCodeSpanId()))) {
            return false;
        }

        SeleniumDriverInstance.pause(5000);
        testData.addParameter("Retrieved Client Code", SeleniumDriverInstance.retrieveTextById(EkunduCreatePersonalClientPage.clientCodeSpanId()));
        SeleniumDriverInstance.takeScreenShot("Verification of client details submition was successful ", false);

        return true;
    }
    // </editor-fold>

}
