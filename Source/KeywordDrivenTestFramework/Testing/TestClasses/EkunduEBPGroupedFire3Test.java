
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduEBPGroupedFire3Test extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduEBPGroupedFire3Test(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk button is present", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the Add Risk button is present - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterIndustryDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter industry details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter industry details - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterRiskSelectionDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk selection details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk selection details - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterFireDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter fire details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter fire details - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterFACPropPlacementData())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter FAC Prop Placement data", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter FAC Prop Placement data - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot("Grouped Fire risk added successfully", false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Grouped Fire risk added successfully", this.getTotalExecutionTime());

      }

    private boolean verifyAddRiskButtonisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            System.out.println("Add risk button clicked.");

            return selectGroupedFireRiskType();
          }

        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return selectGroupedFireRiskType();
          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }
      }

    private boolean selectGroupedFireRiskType()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(10000);
        SeleniumDriverInstance.takeScreenShot("Grouped Fire Risk type selected successfully", false);

        return true;
      }

    private boolean enterIndustryDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(),
                this.getData("Industry")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.IndustrySearchRowId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Industry details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterRiskSelectionDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RSFireSectionsFireCheckBoxId()))
          {
            return false;
          }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.TypeOfConstructionDropDownListId(),
                this.getData("Type Of Construction")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk selection details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterFireDetails()
      {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireOverviewFlatPremiumCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireRiskDataBuildingsCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireRiskDataBuildingsRiskSumInsuredTextBoxId(),
                this.getData("Fire - Risk Data - Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireRiskDataBuildingsRiskPremiumTextBoxId(),
                this.getData("Fire - Risk Data - Premium")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Fire risk entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.checkPresenceofElementById("ctl00_cntMainBody_pnlRiskDetls",
                EkunduViewClientDetailsPage.NextButtonId());

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        return true;
      }

    private boolean enterFACPropPlacementData()
      {
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(),
                this.getData("Reinsurance Details - FAC Placement Reinsurer Code 1")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);

//        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId());
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        SeleniumDriverInstance.pause(2000);
        
        SeleniumDriverInstance.clickElementbyPartialLinkText("Reinsurances");
        
        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId(),
                this.getData("Reinsurance Details - FAC Placement - Everestre Percentage")))
          {
            return false;
          }
        
         SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
          {
            return false;
          }

        SeleniumDriverInstance.pause(8000);

        System.out.println("Company 1 added successfully");

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(),
                this.getData("Reinsurance Details - FAC Placement Reinsurer Code 2")))
          {
            return false;
          }
        
        

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
          {
            return false;
          }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        SeleniumDriverInstance.pause(2000);
        
        SeleniumDriverInstance.clickElementbyPartialLinkText("Reinsurances");

        SeleniumDriverInstance.pause(5000);

//        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.HannoverreReinsurancePercTextBoxId());
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        SeleniumDriverInstance.clickElementbyPartialLinkText("Reinsurances");
        
        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.HannoverreReinsurancePercTextBoxId(),
                this.getData("Reinsurance Details - FAC Placement - Hannoverrre  Percentage")))
          {
            return false;
          }
        
        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        SeleniumDriverInstance.pause(2000);
        
        SeleniumDriverInstance.clickElementbyPartialLinkText("Reinsurances");

        System.out.println("Company 2 added successfully");

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(),
                this.getData("Reinsurance Details - FAC Placement Reinsurer Code 3")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectMunichReinsurerLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);

//        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.MunichReinsurancePercTextBoxId());
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        
        SeleniumDriverInstance.clickElementbyPartialLinkText("Reinsurances");

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.MunichReinsurancePercTextBoxId(),
                this.getData("Reinsurance Details - FAC Placement - Munich Percentage")))
          {
            return false;
          }
        
        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
          {
            return false;
          }

        SeleniumDriverInstance.pause(5000);

        System.out.println("Company 2 added successfully");

        SeleniumDriverInstance.takeScreenShot("FAC Proc Placement data entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId()))
          {
            return false;
          }

        return true;
      }

     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
    
  }


//~ Formatted by Jindent --- http://www.jindent.com
