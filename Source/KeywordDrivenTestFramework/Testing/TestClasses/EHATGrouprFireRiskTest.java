/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduElectronicEquipmentRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author ferdinandN
 */
public class EHATGrouprFireRiskTest extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public EHATGrouprFireRiskTest(TestEntity testData) {
        this.testData = testData;

    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk button is present", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to verify that the Add Risk button is present - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterIndustryDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter industry details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter industry details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        if (!enterRiskDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter risk details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterLoadingandDiscountCriteriaData()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Loading and Discount Criteria", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Loading and Discount Criteria - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterSurveyReport()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter survey report", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter survey report- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterBlockedRisks()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter blocked risk", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter blocked risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterBusinessInterruptionCoverDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Business Interruption cover details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Business Interruption cover details- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterBusinessInterruptionExtensions()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Business Interruption extensions", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Business Interruption extensions- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!addBusinessInterruptionSpecifiedCustomerAndSupplier()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Business Interruption pecified customers and suppliers", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Business Interruption pecified customers and suppliers- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!addPropertiesCombinedFirstItem()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Properties Combined 1st item", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Properties Combined 1st item- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!addPropertiesCombinedSecondItem()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Properties Combined 2nd item", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Properties Combined 2nd item- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!addPropertiesCombinedThirdItem()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Properties Combined 3rd item", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Properties Combined 3rd item- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!addPropertiesCombinedFourthItem()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Properties Combined 4th item", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Properties Combined 4th item- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!addPropertiesCombinedFifthItem()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Properties Combined 5th item", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Properties Combined 5th item- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterPCMiscellaneousItems()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Properties Combined Miscellanous items", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Properties Combined Miscellanous items- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterPCFirstAmountPayableDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Properties Combined First Amount Payable Details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Properties Combined First Amount Payable Details- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterPropertyCombinedExtensionsDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Properties Combined extensions", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Properties Combined extensions- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterPCEndorsements()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Properties Combined endorsements", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Properties Combined endorsements- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterARDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Accidental Damage Details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Accidental Damage Details- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterAREndorsements()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Accidental Damage Endorsements", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Accidental Damage Endorsements- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterOCDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Office Contents details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Office Contents details- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterADDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Accidental Damage details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Accidental Damage details- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterGIDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Greens and Irrigation details", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Greens and Irrigation details- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterNotes()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk notes / comments", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter risk notes / comments-" + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Grouped Fire risk details successfully entered", this.getTotalExecutionTime());
    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean verifyAddRiskButtonisPresent() {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectGroupedFireRiskType();

            return true;
        } else if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId())) {
            selectGroupedFireRiskType();
            return true;
        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");
            return false;
        }
    }

    private boolean selectGroupedFireRiskType() {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SelectEHATGroupedFireRiskId())) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }
        SeleniumDriverInstance.pause(10000);
        SeleniumDriverInstance.takeScreenShot("Grouped Fire Risk type selected successfully", false);
        return true;
    }

    private boolean enterIndustryDetails() {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(), this.getData("Industry"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId())) {
            return false;
        }
        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.IndustrySearchResultsSelectedRowId())) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Industry details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterRiskDetails() {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RSFireSectionsRiskBlockedCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RSFireSectionsBusinessInterruptionCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RSFireSectionsPropertyCombinedCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RSFireSectionsOfficeContentsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RSFireSectionsAccountsRecievableCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RSFireSectionsAccidentalDamageCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RSFireSectionGreensandIrrigationCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.ManualReinsuranceLimitDropDownListId(), this.getData("Manual Insurance Limit"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.TypeOfConstructionDropDownListId(), this.getData("Type Of Construction"))) {
            return false;
        }
        SeleniumDriverInstance.takeScreenShot("Fire Sections and Premises Assessment data entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }
        return true;

    }

    private boolean enterLoadingandDiscountCriteriaData() {

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.LoadingandDiscountCriteria3YearLossRatioDropDownListId(), this.getData("3 year Loss"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.LoadingandDiscountCriteriaHOMarketAdjustmentDropDownListId(), this.getData("H/O Market Adjustment"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.LoadingandDiscountCriteriaBranchMarketAdjustmentDropDownListId(), this.getData("Branch Market Adjustment"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.LoadingandDiscountCriteriaMaliciousDamageDropDownListId(), this.getData("Malicous Damage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.LoadingandDiscountCriteriaSpecialPerilsDropDownListId(), this.getData("Special Perils"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.LoadingandDiscountCriteriaSCIDropDownListId(), this.getData("SCI"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.LoadingandDiscountCriteriaFEADropDownListId(), this.getData("FEA"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.LoadingandDiscountCriteriaLCEDropDownListId(), this.getData("LCE"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Loading and Discount Criteria entered successfully", false);

        return true;

    }

    private boolean enterSurveyReport() {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.SuveryReportApplicableCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.LastSurveyDateTextBoxId(), this.getData("Last Survey Date"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SurveyNumberTextBoxId(), this.getData("Survey Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.DateSurveyRequestedTextBoxId(), this.getData("Date Survey Requested"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.SurveyFrequencyDropDownListId(), this.getData("Survey Frequency"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Survey report entered successfully", false);

        return true;

    }

    private boolean enterBlockedRisks() {
        SeleniumDriverInstance.scrollElementIntoViewById(EkunduViewClientDetailsPage.DateSurveyRequestedTextBoxId());
        SeleniumDriverInstance.pause(5000);
        if (!SeleniumDriverInstance.clickElementbyName(EkunduViewClientDetailsPage.BlocledPoliciesAddButtonName())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BlockedPoliciesClientNameTextBoxId(), this.getData("Blocked Risk - Client Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BlockedPoliciesPolicyNumberTextBoxId(), this.getData("Blocked Risk - Policy Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BlockedRisksBICheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BISumInsuredValueTextBoxId(), this.getData("Business Interruption Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BlockedRisksBCCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BCSumInsuredValueTextBoxId(), this.getData("Business Combined Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BlockedRisksOCCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCSumInsuredValueTextBoxId(), this.getData("Office Contents Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BlockedRisksARCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ARSumInsuredValueTextBoxId(), this.getData("Accounts Receivable Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BlockedRisksADCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADSumInsuredValueTextBoxId(), this.getData("Accidental Damage Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BlockedRisksGICheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GISumInsuredValueTextBoxId(), this.getData("Greens and Irrigation Sum Insured"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Blocked risk details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;

    }

    private boolean enterBusinessInterruptionCoverDetails() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.BusinessInterruptionOverviewTypeOfCoverDropDownListId(), this.getData("Business Interruption - Overview - Type of Cover"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.BIOverviewIndemnityPeriodDropDownListId(), this.getData("Business Interruption - Overview - Indemnity Period"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BIOverviewFlatPremiumCheckBoxId(), true)) {
            return false;
        }

//         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBIDepositAndDeclarationCheckBoxId(), true))
//        {
//            return false;
//        }
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBICoverGrossProfitBasisCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBICoverGrossProfitBasisSumInsuredTextBoxId(), this.getData("Business Interruption - Cover - Gross Profit Basis - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.EHATBICoverGrossProfitBasisDeclarationBasisDropDownListId(), this.getData("Business Interruption - Cover - Gross Profit Basis - Declaration Basis"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBICoverGrossRentalsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBICoverGrossRentalsMonthsTextBoxId(), this.getData("Business Interruption - Cover - Gross Rentals - Months"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBICoverGrossRentalsSumInsuredTextBoxId(), this.getData("Business Interruption - Cover - Gross Rentals - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.EHATBICoverGrossRentalsDeclarationBasisDropDownListId(), this.getData("Business Interruption - Cover - Gross Rentals - Declaration Basis"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBICoverRevenueCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBICoverRevenueSumInsuredTextBoxId(), this.getData("Business Interruption - Cover - Revenue - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.EHATBICoverRevenueDeclarationBasisDropDownListId(), this.getData("Business Interruption - Cover - Revenue - Declaration Basis"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBICoverAdditionalIncreaseCostOfWorkingCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.EHATBICoverAdditionalIncreaseCostOfWorkingTypeDropDownListId(), this.getData("Business Interruption - Cover - Additional Increased Cost of Working - Type"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBICoverAdditionalIncreaseCostOfWorkingSumInsuredTextBoxId(), this.getData("Business Interruption - Cover - Additional Increased Cost of Working - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBICoverLossofLeviesCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBICoverLossofLeviesTypeDropDownListId(), this.getData("Business Interruption - Cover - Loss of Levies - Number"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBICoverLossofLeviesSumInsuredTextBoxId(), this.getData("Business Interruption - Cover - Loss of Levies - Sum Insured"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Business interruption cover details entered successfully", false);

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBICoverLossofTouristAttractionCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBICoverLossofTouristAttractionSumInsuredTextboxId(), this.getData("Business Interruption - Cover - Loss of Tourist Attraction - Sum Insured"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Business Interruption Cover details entered successfully", false);

        return true;

    }

    private boolean enterBusinessInterruptionExtensions() {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBIExtensionsAccidentalDamageCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsAccidentalDamageLimitTextBoxId(), this.getData("Business Interruption - Extensions - Accidental Damage - Limit"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsAccidentalDamageRateTextBoxId(), this.getData("Business Interruption - Extensions - Accidental Damage - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsAccidentalDamageFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Accidental Damage - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsAccidentalDamageFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Accidental Damage - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBIExtensionsAdditionalPermisesClauseCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsAdditionalPremisesClauseLimitTextBoxId(), this.getData("Business Interruption - Extensions - Additional Premises - Limit"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsAdditionalPremisesClauseRateTextBoxId(), this.getData("Business Interruption - Extensions - Additional Premises - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsAddionalPremisesClauseFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Additional Premises - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsAddionalPremisesClauseFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Additional Premises - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBIExtensionsClaimsPreparationCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsClaimsLimitTextBoxId(), this.getData("Business Interruption - Extensions - Claims - Limit"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsClaimsRateTextBoxId(), this.getData("Business Interruption - Extensions - Claims - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBIExtensionsDisposalOfSalvageCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBIExtensionsFinesAndPenaltiesCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsFinesLimitTextBoxId(), this.getData("Business Interruption - Extensions - Fines - Limit"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsFinesRateTextBoxId(), this.getData("Business Interruption - Extensions - Fines - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsFinesFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Fines - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsFinesFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Fines - FAP Amount"))) {
            return false;
        }

        //START - Prevention of Access checkbox
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBIExtensionsPreventionOfAccessCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsPreventionOfAccesLimitTextBoxId(), this.getData("Business Interruption - Extensions - Prevention of Access - Limit"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsPreventionOfAccesRateTextBoxId(), this.getData("Business Interruption - Extensions - Prevention of Access - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsPreventionOfAccesFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Prevention of Access - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsPreventionOfAccesFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Prevention of Access - FAP Amount"))) {
            return false;
        }
        //END - Prevention of Access checkbox

        SeleniumDriverInstance.scrollDownByOnePage(EkunduViewClientDetailsPage.EHATBIExtensionsPublicTelecommunicationsExtendedCheckBoxId());

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBIExtensionsPublicTelecommunicationsExtendedCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsPublicTelecommunicationsExtendedLimitTextBoxId(), this.getData("Business Interruption - Extensions - Public Telecom Extended - Limit"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsPublicTelecommunicationsExtendedRateTextBoxId(), this.getData("Business Interruption - Extensions - Public Telecom Extended - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsPublicTelecommunicationsExtendedFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Public Telecom Extended - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsPublicTelecommunicationsExtendedFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Public Telecom Extended - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBIExtensionsPublicUtilitiesExtendedCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsPublicUtilitiesExtendedLimitTextBoxId(), this.getData("Business Interruption - Extensions - Public Util Extended - Limit"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsPublicUtilitiesExtendedRateTextBoxId(), this.getData("Business Interruption - Extensions - Public Util Extended - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsPublicUtilitiesExtendedFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Public Util Extended - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsPublicUtilitiesExtendedFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Public Util Extended - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBIExtensionsSpecifiedCustomersCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsSpecifiedCustomersLimitTextBoxId(), this.getData("Business Interruption - Extensions - Specific Customers - Limit"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsSpecifiedCustomersRateTextBoxId(), this.getData("Business Interruption - Extensions - Specific Customers - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsSpecifiedCustomersFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Specific Customers - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsSpecifiedCustomersFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Specific Customers - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBIExtensionsSpecifiedSuppliersCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsSpecifiedSuppliersLimitTextBoxId(), this.getData("Business Interruption - Extensions - Specific Suppliers - Limit"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsSpecifiedSuppliersRateTextBoxId(), this.getData("Business Interruption - Extensions - Specific Suppliers - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsSpecifiedSuppliersFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Specific Suppliers - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsSpecifiedSuppliersFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Specific Suppliers - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBIExtensionsUnSpecifiedCustomersCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsUnSpecifiedCustomersLimitTextBoxId(), this.getData("Business Interruption - Extensions - Un Specified Customers - Limit"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsUnSpecifiedCustomersRateTextBoxId(), this.getData("Business Interruption - Extensions - Un Specified Customers - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsUnSpecifiedCustomersFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Un Specified Customers - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsUnSpecifiedCustomersFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Un Specified Customers - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBIExtensionsUnSpecifiedSuppliersCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsUnSpecifiedSuppliersLimitOfIndemnityTextBoxId(), this.getData("Business Interruption - Extensions - Un Specified Suppliers - Limit"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsUnSpecifiedSuppliersRateTextBoxId(), this.getData("Business Interruption - Extensions - Un Specified Suppliers - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsUnSpecifiedSuppliersFAPPercentageTextBoxId(), this.getData("Business Interruption - Extensions - Un Specified Suppliers - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATBIExtensionsUnSpecifiedSuppliersFAPAmountTextBoxId(), this.getData("Business Interruption - Extensions - Un Specified Suppliers - FAP Amount"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Extensions entered successfully", false);

        return true;
    }

    private boolean addBusinessInterruptionSpecifiedCustomerAndSupplier() {
        SeleniumDriverInstance.scrollDownByOnePage(EkunduViewClientDetailsPage.EHATBIExtensionsUnSpecifiedSuppliersFAPAmountTextBoxId());

        if (!SeleniumDriverInstance.clickElementbyName(EkunduViewClientDetailsPage.EHATBIAddSpecifiedSupplierButtonName())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BISpecifiedSuppliersSupplierNameTextBoxId(), this.getData("Business Interruption - Add Specified Supplier - Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BISpecifiedSuppliersSupplierPercentageTextBoxId(), this.getData("Business Interruption - Add Specified Supplier - Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.scrollDownByOnePage(EkunduViewClientDetailsPage.EHATBIExtensionsUnSpecifiedSuppliersFAPAmountTextBoxId());

        SeleniumDriverInstance.scrollDownByOnePage(EkunduViewClientDetailsPage.EHATBIExtensionsUnSpecifiedSuppliersFAPAmountTextBoxId());

        if (!SeleniumDriverInstance.clickElementbyName(EkunduViewClientDetailsPage.EHATBIAddSpecifiedCustomerButtonName())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BISpecifiedCustomersCustomerNameTextBoxId(), this.getData("Business Interruption - Add Specified Customer - Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BISpecifiedCustomersCustomerPercentageTextBoxId(), this.getData("Business Interruption - Add Specified Customer - Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.scrollDownByOnePage(EkunduViewClientDetailsPage.EHATBIExtensionsUnSpecifiedSuppliersFAPAmountTextBoxId());

        SeleniumDriverInstance.scrollDownByOnePage(EkunduViewClientDetailsPage.EHATBIExtensionsUnSpecifiedSuppliersFAPAmountTextBoxId());

        SeleniumDriverInstance.takeScreenShot("Business interruption specified customer and supplier details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean addPropertiesCombinedFirstItem() {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATFlatPremiumCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyName(EkunduViewClientDetailsPage.EHATBCADDPropertyButtonName())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCDescriptionTextBoxId(), this.getData("Properties Combined - Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCSumInsuredTextBoxId(), this.getData("Properties Combined - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCRateTextBoxId(), this.getData("Properties Combined - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBuildingsEscalationorInflationCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCEscalationPercentageTextBoxId(), this.getData("Properties Combined - Escalation / Inflation - Escalation Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCInflation1stYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 1st Year Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCInflation2ndYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 2nd Year Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATPCContentsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsSumInsuredTextBoxId(), this.getData("Properties Combined - Contents - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsRateTextBoxId(), this.getData("Properties Combined - Contents - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATContentsEscalationorInflationCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsEscalationPercentageTextBoxId(), this.getData("Properties Combined - Escalation / Inflation - Escalation Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsInflation1stYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 1st Year Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsInflation2ndYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 2nd Year Percentage"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Properties combined details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean addPropertiesCombinedSecondItem() {
        if (!SeleniumDriverInstance.clickElementbyName(EkunduViewClientDetailsPage.EHATBCADDSecondPropertyButtonName())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCDescriptionTextBoxId(), this.getData("Properties Combined - Description 2"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCSumInsuredTextBoxId(), ("Properties Combined - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCRateTextBoxId(), this.getData("Properties Combined - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBuildingsEscalationorInflationCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCEscalationPercentageTextBoxId(), this.getData("Properties Combined - Escalation / Inflation - Escalation Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCInflation1stYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 1st Year Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCInflation2ndYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 2nd Year Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATPCContentsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsSumInsuredTextBoxId(), this.getData("Properties Combined - Contents - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsRateTextBoxId(), this.getData("Properties Combined - Contents - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATContentsEscalationorInflationCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsEscalationPercentageTextBoxId(), this.getData("Properties Combined - Escalation / Inflation - Escalation Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsInflation1stYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 1st Year Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsInflation2ndYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 2nd Year Percentage"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Properties combined 2nd item added successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean addPropertiesCombinedThirdItem() {
        if (!SeleniumDriverInstance.clickElementbyName(EkunduViewClientDetailsPage.EHATBCADDThirdPropertyButtonName())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCDescriptionTextBoxId(), this.getData("Properties Combined - Description 3"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCSumInsuredTextBoxId(), this.getData("Properties Combined - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCRateTextBoxId(), this.getData("Properties Combined - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBuildingsEscalationorInflationCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCEscalationPercentageTextBoxId(), this.getData("Properties Combined - Escalation / Inflation - Escalation Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCInflation1stYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 1st Year Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCInflation2ndYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 2nd Year Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATPCContentsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsSumInsuredTextBoxId(), this.getData("Properties Combined - Contents - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsRateTextBoxId(), this.getData("Properties Combined - Contents - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATContentsEscalationorInflationCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsEscalationPercentageTextBoxId(), this.getData("Properties Combined - Escalation / Inflation - Escalation Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsInflation1stYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 1st Year Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsInflation2ndYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 2nd Year Percentage"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Properties combined 3rd item added successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean addPropertiesCombinedFourthItem() {
        if (!SeleniumDriverInstance.clickElementbyName(EkunduViewClientDetailsPage.EHATBCADDFourthPropertyButtonName())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCDescriptionTextBoxId(), this.getData("Properties Combined - Description 4"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCSumInsuredTextBoxId(), this.getData("Properties Combined - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCRateTextBoxId(), this.getData("Properties Combined - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBuildingsEscalationorInflationCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCEscalationPercentageTextBoxId(), this.getData("Properties Combined - Escalation / Inflation - Escalation Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCInflation1stYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 1st Year Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCInflation2ndYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 2nd Year Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATPCContentsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsSumInsuredTextBoxId(), this.getData("Properties Combined - Contents - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsRateTextBoxId(), this.getData("Properties Combined - Contents - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATContentsEscalationorInflationCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsEscalationPercentageTextBoxId(), this.getData("Properties Combined - Escalation / Inflation - Escalation Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsInflation1stYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 1st Year Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsInflation2ndYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 2nd Year Percentage"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Properties combined 4th item added successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean addPropertiesCombinedFifthItem() {
        if (!SeleniumDriverInstance.clickElementbyName(EkunduViewClientDetailsPage.EHATBCADDFifthPropertyButtonName())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCDescriptionTextBoxId(), this.getData("Properties Combined - Description 5"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCSumInsuredTextBoxId(), this.getData("Properties Combined - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCRateTextBoxId(), this.getData("Properties Combined - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATBuildingsEscalationorInflationCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCEscalationPercentageTextBoxId(), this.getData("Properties Combined - Escalation / Inflation - Escalation Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCInflation1stYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 1st Year Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCInflation2ndYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 2nd Year Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATPCContentsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsSumInsuredTextBoxId(), this.getData("Properties Combined - Contents - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsRateTextBoxId(), this.getData("Properties Combined - Contents - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATContentsEscalationorInflationCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsEscalationPercentageTextBoxId(), this.getData("Properties Combined - Escalation / Inflation - Escalation Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsInflation1stYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 1st Year Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCContentsInflation2ndYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 2nd Year Percentage"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Properties combined 4th item added successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterPCMiscellaneousItems() {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATPCMiscellaneousItemsOverviewCheckBoxId(), true)) {
            return false;
        }
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATPCAdditionalRentCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCAdditionalRentSumInsuredTextBoxId(), this.getData("Properties Combined - Additional Rent - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCAdditionalRentRateTextBoxId(), this.getData("Properties Combined - Additional Rent - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCAdditionalRentPremiumTextBoxId(), this.getData("Properties Combined - Additional Rent - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyName(EkunduViewClientDetailsPage.PCAddMiscellaneousItemButtonName())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCMiscItemDescriptionTextBoxId(), this.getData("Properties Combined - Miscellaneous - Miscellaneous Item Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATMiscItemSumInsuredTextBoxId(), this.getData("Properties Combined - Miscellaneous -Miscellaneous Item Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATMiscellaneousItemsFlatPremiumCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATFireFirstAmountPayableCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATFireMinimumPercentageFirstAmountPayableTextBoxId(), this.getData("Miscellaneous - First Amount Payable - Min Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATFireMinimumAmountFirstAmountPayableTextBoxId(), this.getData("Miscellaneous - First Amount Payable - Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATFireMaximumAmountFirstAmountPayableTextBoxId(), this.getData("Miscellaneous - First Amount Payable - Max Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATMiscellaneousItemsEscalationorInflationCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCMiscItemsEscalationPercentageTextBoxId(), this.getData("Properties Combined - Escalation / Inflation - Escalation Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCMiscItemsInflation1stYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 1st Year Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCMISCItemsInflation2ndYearPercentageTextBoxId(), this.getData("Properties Combined - Inflation 2nd Year Percentage"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Miscellaneous Data, Inflation / Escalation Data and First Amount Payable data entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;

    }

    private boolean enterPCFirstAmountPayableDetails() {
        if (!SeleniumDriverInstance.clickElementbyName(EkunduViewClientDetailsPage.EHATPCAddFAPButtonName())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCFirstAmountPayableDescriptonTextBoxId(), this.getData("Property Combined - First Amount Payable - Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCMinimumPercentageFirstAmountPayableTextBoxId(), this.getData("Property Combined - First Amount Payable - Min Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCMinimumAmountFirstAmountPayableTextBoxId(), this.getData("Property Combined - First Amount Payable - Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATPCMaximumAmountFirstAmountPayableTextBoxId(), this.getData("Property Combined - First Amount Payable - Max Amount"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("First Amount Payable details antered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterPropertyCombinedExtensionsDetails() {
        SeleniumDriverInstance.scrollToElement(EkunduViewClientDetailsPage.NextButtonId());

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATPCClaimsPreparationCheckBoxId(), true)) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATClaimsPrepLimitOfIndemnityTextBoxId(), this.getData("Property Combined - Claims Preparation - Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATClaimsPrepRateTextBoxId(), this.getData("Property Combined - Claims Preparation - Rate"))) {

        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATPreventionofAccessCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATClaimsPrepLimitOfIndemnityTextBoxId(), this.getData("Property Combined - Claims Preparation - Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATClaimsPrepRateTextBoxId(), this.getData("Property Combined - Claims Preparation - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATClaimsPrepPremiumTextBoxId(), this.getData("Property Combined - Claims Preparation - Premium"))) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATPreventionofAccessCheckBoxId(), true)) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PCPreventionofAccessLimitOfIndemnityTextBoxId(), this.getData("Property Combined - Prevention of Access - Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PCPreventionofAccessRateTextBoxId(), this.getData("Property Combined - Prevention of Access - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PCPreventionofAccessPremiumTextBoxId(), this.getData("Property Combined - Prevention of Access - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PCPreventionofAccessFAPPercTextBoxId(), this.getData("Property Combined - Prevention of Access - FAP Percentage"))) {

        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PCPreventionofAccessFAPAmountTextBoxId(), this.getData("Property Combined - Prevention of Access - FAP Amount"))) {

        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.PCRiotAndStrikeCheckBoxId(), true)) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PCRiotPremiumTextBoxId(), this.getData("Property Combined - Riot - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PCRiotFAPPercentageTextBoxId(), this.getData("Property Combined - Riot - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PCRiotFAPAmountTextBoxId(), this.getData("Property Combined - Riot - FAP Amount"))) {

        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.PCSubsidenceAndLandslipCheckBoxId(), true)) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PCSubsidenceLimitOfIndemnityTextBoxId(), this.getData("Property Combined - Subsidence - Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PCSubsidenceRateTextBoxId(), this.getData("Property Combined - Subsidence - Rate"))) {

        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PCSubsidencePremiumTextBoxId(), this.getData("Property Combined - Subsidence - Premium"))) {

        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PCSubsidenceFAPPercentageTextBoxId(), this.getData("Property Combined - Subsidence - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.PCSubsidenceFAPAmountTextBoxId(), this.getData("Property Combined - Subsidence - FAP Amount"))) {

        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.PCSubsidenceCoverTypeDropDownListId(), this.getData("Property Combined - Subsidence - Cover Type"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.PCSubsidenceGeoTechDropDownListId(), this.getData("Property Combined - Subsidence - GeoTech"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Property Combined extensions entered successfully", false);

        return true;
    }

    private boolean enterPCEndorsements() {
        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EndorsementsSelectButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EHATPCEndorsementsAddAllButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EHATPCEndorsementsApplyButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        return true;

    }

    private boolean enterARDetails() {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATARFlatPremiumCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.AROutstandingDebitBalancesCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.AROutstandingDebitBalancesSumInsuredTextBoxId(), this.getData("AR - Outstanding Debit Balances - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.AROutstandingDebitBalancesPremiumTextBoxId(), this.getData("AR - Outstanding Debit Balances - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ARFirstAmountPayableCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ARFirstAmountPayableMinPercentageTextBoxId(), this.getData("AR - First Amount Payable - Min Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ARFirstAmountPayableMinAmountTextBoxId(), this.getData("AR - First Amount Payable - Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ARFirstAmountPayableMaxAmountTextBoxId(), this.getData("AR - First Amount Payable - Max Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ARExtensionsClaimsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ARExtensionsClaimsLimitTextBoxId(), this.getData("AR - Extensions  - Claims - Limit"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ARExtensionsClaimsPremiumTextBoxId(), this.getData("AR - Extensions  - Claims - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ARExtensionsDuplicateRecordsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ARExtensionsProtectionsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ARExtensionsRiotCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ARExtensionsRiotPremiumTextBoxId(), this.getData("AR - Extensions - Riot - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ARExtensionsFAPPercentageTextBoxId(), this.getData("AR - Extensions - Riot - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ARExtensionsFAPAmountTextBoxId(), this.getData("AR - Extensions - Riot - FAP Amount"))) {
            return false;
        }

        //SeleniumDriverInstance.scrollDownByOnePage(EkunduViewClientDetailsPage.ARExtensionsRiotPremiumTextBoxId());
        SeleniumDriverInstance.takeScreenShot("AR details entered successfully", false);

        return true;
    }

    private boolean enterAREndorsements() {
        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EndorsementsSelectButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EHATAREndorsementsAddAllButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EHATAREndorsementsApplyButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId());

        return true;

    }

    private boolean enterOCDetails() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.OCAlarmWarrantyDropDownListId(), this.getData("OC - Alarm Warranty"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.OCFlatPremiumCheckBoxId(), true)) {
            return false;
        }

        // Cover
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATOCCoverOfficeContentsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCCoverOfficeContentsSumInsuredTextBoxId(), this.getData("OC - Cover - Office Contents - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCCoverOfficeContentsPremiumTextBoxId(), this.getData("OC - Cover - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATOCCoverLossOfDocumentsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCCoverLossOfDocumentsSumInsuredTextBoxId(), this.getData("OC - Cover - Loss of Documentation - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCCoverLossOfDocumentsPremiumTextBoxId(), this.getData("OC - Cover - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATOCCoverLiabilityForDocumentsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCCoverLiabilityForDocumentsSumInsuredTextBoxId(), this.getData("OC - Cover - Liability for Documents - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCCoverLiabilityForDocumentsPremiumTextBoxId(), this.getData("OC - Cover - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATOCCoverTheftNonForcibleCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCCoverTheftNonForcibleSumInsuredTextBoxId(), this.getData("OC - Cover - Theft Non Forcible - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCCoverTheftNonForciblePremiumTextBoxId(), this.getData("OC - Cover - Premium"))) {
            return false;
        }

//        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATOCCoverTheftByForcibleEntryCheckBoxId(), true))
//        {
//            return false;
//        }
//        
//        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCCoverTheftForcibleSumInsuredTextBoxId(), this.getData("OC - Cover - Theft by Forcible Entry - Sum Insured")))
//        {
//            return false;
//        }
//        
//        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCCoverTheftForciblePremiumTextBoxId(), this.getData("OC - Cover - Premium")))
//        {
//            return false;
//        }
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATOCCoverDebrisRemovalCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCCoverDebrisRemovalSumInsuredTextBoxId(), this.getData("OC - Cover - Debris Removal - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCCoverDebrisRemovalPremiumTextBoxId(), this.getData("OC - Cover - Premium"))) {
            return false;
        }

        // First Amount Payable
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableOfficeContentsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableOfficeContentsMinPercentageTextBoxId(), this.getData("OC - First Amount Payable - Office Contents - Min Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableOfficeContentsMinAmountTextBoxId(), this.getData("OC - First Amount Payable - Office Contents - Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableOfficeContentsMaxAmountTextBoxId(), this.getData("OC - First Amount Payable - Office Contents - Max Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableLossofDocumentsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableLossOfDocumentsMinPercentageTextBoxId(), this.getData("OC - First Amount Payable - Loss of Documents - Min Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableLossOfDocumentsMinAmountTextBoxId(), this.getData("OC - First Amount Payable - Loss of Documents - Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableLossOfDocumentsMaxAmountTextBoxId(), this.getData("OC - First Amount Payable - Loss of Documents - Max Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableLiabilityForDocumentsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableLiabilityForDocumentsMinPercentageTextBoxId(), this.getData("OC - First Amount Payable - Liability for Documents - Min Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableLiabilityForDocumentsMinAmountTextBoxId(), this.getData("OC - First Amount Payable - Liability for Documents - Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableLiabilityForDocumentsMaxAmountTextBoxId(), this.getData("OC - First Amount Payable - Liability for Documents - Max Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableTheftNonForcibleCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableTheftNonForcibleMinPercentageTextBoxId(), this.getData("OC - First Amount Payable - Theft non Forcible - Min Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableTheftNonForcibleMinAmountTextBoxId(), this.getData("OC - First Amount Payable - Theft non Forcible - Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableTheftNonForcibleMaxAmountTextBoxId(), this.getData("OC - First Amount Payable - Theft non Forcible - Max Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableTheftByForcibleEntryCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableTheftForcibleMinPercentageTextBoxId(), this.getData("OC - First Amount Payable - Theft Forcible - Min Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableTheftForcibleMinAmountTextBoxId(), this.getData("OC - First Amount Payable - Theft Forcible - Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableTheftForcibleMaxAmountTextBoxId(), this.getData("OC - First Amount Payable - Theft Forcible - Max Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableDebrisRemovalCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableDebrisRemovalMinPercentageTextBoxId(), this.getData("OC - First Amount Payable - Debris Removal - Min Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableDebrisRemovalMinAmountTextBoxId(), this.getData("OC - First Amount Payable - Debris Removal - Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableDebrisRemovalMaxAmountTextBoxId(), this.getData("OC - First Amount Payable - Debris Removal - Max Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableLightningStrikesCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCFirstAmountPayableLightningStrikesMaxAmountTextBoxId(), this.getData("OC - First Amount Payable - Lightning Strikes  - Max Amount"))) {
            return false;
        }

        //Extensions
        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCExtensionsAdditionalIncreasedCostRateTextBoxId(), this.getData("OC - Extensions - Additional Increased Cost - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCExtensionsAdditionalIncreasedCostFAPPercentageTextBoxId(), this.getData("OC - Extensions - Additional Increased Cost - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCExtensionsAdditionalIncreasedCostFAPAmountTextBoxId(), this.getData("OC - Extensions - Additional Increased Cost - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.OCExtensionsClaimsPremiumTextBoxId(), this.getData("OC - Extensions - Claims - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATOCExtentionsFirstLossAverageCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATOCExtentionsMaliciousDamageCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCExtensionsMaliciousDamageLimitTextBoxId(), this.getData("OC - Extensions - Malicious Damage - Limit"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCExtensionsMaliciousDamageRateTextBoxId(), this.getData("OC - Extensions - Malicious Damage - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCExtensionsMaliciousDamageFAPPercentageTextBoxId(), this.getData("OC - Extensions - Malicious Damage - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCExtensionsMaliciousDamageFAPAmountTextBoxId(), this.getData("OC - Extensions - Malicious Damage - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.EHATOCExtentionsRiotCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCExtensionsRiotPremiumTextBoxId(), this.getData("OC - Extensions - Riot - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCExtensionsRiotFAPPercentageTextBoxId(), this.getData("OC - Extensions - Riot - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EHATOCExtensionsRiotFAPAmountTextBoxId(), this.getData("OC - Extensions - Riot - FAP Amount"))) {
            return false;
        }

        //Endorsements
        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EndorsementsSelectButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EHATOCEndorsementsAddAllButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EHATOCEndorsementsApplyButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("OC details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterADDetails() {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADFlatPremiumCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADCoverAccidentalDamageCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.ADAccidentalDamageBasisOfCoverDropDownListId(), this.getData("AD - Accidental Damage Basis of Cover"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADCoverAccidentalDamageFullValueTextBoxId(), this.getData("AD -  Cover - Accidental Damage - Full value"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADCoverAccidentalDamageFirstLossTextBoxId(), this.getData("AD -  Cover - Accidental Damage - First loss"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADCoverAccidentalDamageAverageCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADCoverLeakageCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADCoverLeakageFirstLossTextBoxId(), this.getData("AD -  Cover - Leakage - First loss"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADCoverLeakageAverageCheckBoxId(), true)) {
            return false;
        }

        //First Amount Payable
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADFirstAmountPayableAccidentalDamageCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADFirstAmountPayableAccidentalDamageMinPercentageTextBoxId(), this.getData("AD -  First Amount Payable - Accidental Damage - Min Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADFirstAmountPayableAccidentalDamageMinAmountTextBoxId(), this.getData("AD -  First Amount Payable - Accidental Damage - Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADFirstAmountPayableAccidentalDamageMaxAmountTextBoxId(), this.getData("AD -  First Amount Payable - Accidental Damage - Max Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADFirstAmountPayableLeakageCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADFirstAmountPayableLeakageMinPercentageTextBoxId(), this.getData("AD -  First Amount Payable - Leakage - Min Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADFirstAmountPayableLeakageMinAmountTextBoxId(), this.getData("AD -  First Amount Payable - Leakage - Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADFirstAmountPayableLeakageMaxAmountTextBoxId(), this.getData("AD -  First Amount Payable - Leakage - Max Amount"))) {
            return false;
        }

        //Extensions
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADExtensionsAdditionalIncreasedCostOfWorkingCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADExtensionsAdditionalIncreasedCostLimitTextBoxId(), this.getData("AD - Extensions - Additional Increased Cost - Limit"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADExtensionsAdditionalIncreasedCostRateTextBoxId(), this.getData("AD - Extensions - Additional Increased Cost - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADExtensionsAdditionalIncreasedCostFAPPercentageTextBoxId(), this.getData("AD - Extensions - Additional Increased Cost - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADExtensionsAdditionalIncreasedCostFAPAmountTextBoxId(), this.getData("AD - Extensions - Additional Increased Cost - Fap Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADExtensionsClaimsPreparationCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADExtensionsClaimsPreparationLimitTextBoxId(), this.getData("AD - Extensions - Claims Preparation - Limit"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADExtensionsClaimsPreparationPremiumTextBoxId(), this.getData("AD - Extensions - Claims Preparation - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADMemorandaReinstatementCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADMemorandaFirstLossAverageCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.ADMemorandaExcludedPropertyCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ADMemorandaNotesTextBoxId(), this.getData("AD - Memoranda - Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ADExtensionsClaimsPreparationRateTextBoxId())) {
            return false;
        }

        //Endorsements
        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EndorsementsSelectButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EHATADEndorsementsAddAllButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EHATADEndorsementsApplyButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("AD details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterGIDetails() {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.GIFlatPremiumCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GICoverGreensSumInsuredTextBoxId(), this.getData("GI - Cover - Greens - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GICoverGreensPremiumTextBoxId(), this.getData("GI - Cover - Greens - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GICoverIrrigationSumInsuredTextBoxId(), this.getData("GI - Cover - Irrigation - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GICoverIrrigationPremiumTextBoxId(), this.getData("GI - Cover - Irrigation - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.GICoverClaimsPrepCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GICoverClaimsPrepSumInsuredTextBoxId(), this.getData("GI - Cover - Claims - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GICoverClaimsPrepPremiumTextBoxId(), this.getData("GI - Cover - Claims - Premium"))) {
            return false;
        }

        //First Amount Payable
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.GIFirstAmountPayableGreensCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIFirstAmountPayableGreensMinPercentageTextBoxId(), this.getData("GI -  First Amount Payable - Greens - Min Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIFirstAmountPayableGreensMinAmountTextBoxId(), this.getData("GI -  First Amount Payable - Greens - Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIFirstAmountPayableGreensMaxAmountTextBoxId(), this.getData("GI -  First Amount Payable - Greens - Max Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.GIFirstAmountPayableIrrigationCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIFirstAmountPayableIrrigationMinPercentageTextBoxId(), this.getData("GI -  First Amount Payable - Irrigation - Min Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIFirstAmountPayableIrrigationMinAmountTextBoxId(), this.getData("GI -  First Amount Payable - Irrigation - Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIFirstAmountPayableIrrigationMaxAmountTextBoxId(), this.getData("GI -  First Amount Payable - Irrigation - Max Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.GIFirstAmountPayableClaimsPrepCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIFirstAmountPayableClaimsPrepMinPercentageTextBoxId(), this.getData("GI -  First Amount Payable - Claims Prep - Min Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIFirstAmountPayableClaimsPrepMinAmountTextBoxId(), this.getData("GI -  First Amount Payable - Claims Prep - Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIFirstAmountPayableClaimsPrepMaxAmountTextBoxId(), this.getData("GI -  First Amount Payable - Claims Prep - Max Amount"))) {
            return false;
        }

        //extensions 
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.GIExtensionsDemolitionCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIExtensionsDemolitionSumInsuredTextBoxId(), this.getData("GI -  Extensions - Demolition - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIExtensionsDemolitionPremiumTextBoxId(), this.getData("GI - Extensions - Demolition - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.GIExtensionsLossofRevenueCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIExtensionsLossofRevenueSumInsuredTextBoxId(), this.getData("GI -  Extensions - Loss of Revenue - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIExtensionsLossofRevenuePremiumTextBoxId(), this.getData("GI - Extensions - Loss of Revenue - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.GIExtensionsLossofRevenueTypeofCoverDropdownlistId(), this.getData("GI - Extensions - Loss of Revenue - Type of Cover"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.GIExtensionsPublicUtilitiesCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIExtensionsPublicUtilitiesSumInsuredTextBoxId(), this.getData("GI -  Extensions - Public Utilities  - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIExtensionsPublicUtilitiesPremiumTextBoxId(), this.getData("GI - Extensions - Public Utilities - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.GIExtensionsRiotCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIExtensionsRiotSumInsuredTextBoxId(), this.getData("GI -  Extensions - Riot  - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIExtensionsRiotPremiumTextBoxId(), this.getData("GI - Extensions - Riot - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.GIExtensionsSubsidenceCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIExtensionsSubsidenceSumInsuredTextBoxId(), this.getData("GI -  Extensions - Subsidence  - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.GIExtensionsSubsidencePremiumTextBoxId(), this.getData("GI - Extensions - Subsidence - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EndorsementsSelectButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EHATGIEndorsementsAddAllButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EHATGIEndorsementsApplyButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Greens and Irrigation details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        return true;

    }

    private boolean enterNotes() {
        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskCommentsTextboxId(), this.getData("Risk Comments"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskPolicyNotesTextboxId(), this.getData("Risk Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Notes entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        return true;
    }

}
