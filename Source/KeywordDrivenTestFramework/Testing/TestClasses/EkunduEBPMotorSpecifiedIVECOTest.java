/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.JavascriptExecutor;

/**
 * 
 * @author deanR
 */
public class EkunduEBPMotorSpecifiedIVECOTest extends BaseClass
{
    TestEntity testData;
    TestResult testResultt;
   
    
    public EkunduEBPMotorSpecifiedIVECOTest(TestEntity testData)
    {
        this.testData = testData;
    }    
    
    public TestResult executeTest()
    {  
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();
        
        if(!verifyRisksDialogisPresent())
        {
           SeleniumDriverInstance.takeScreenShot("Failed to verify that the risks dialog is present", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to verify that the risks dialog is present - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

    
        if(!enterMotorDetails())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Motor details", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Motor details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterFAPDetails())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter First Amount Payable details", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter First Amount Payable details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterEndorsements())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Endorsements", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Endorsements - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterSpecifiedDriverDetails())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Specified Driver details", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Specified Driver details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterInterestedPartiesDetails())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Interested Party details", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Interested Party details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterNotes())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Notes", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Notes - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        return new TestResult(testData, Enums.ResultStatus.PASS, "Motor Specified risk details entered successfully", this.getTotalExecutionTime());
    }
    
     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      } 
    
     private boolean verifyRisksDialogisPresent()
    {  
            SeleniumDriverInstance.pause(2000);
       
            if(SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
            {
                    selectMotorSpecifiedRiskType();
                    return true;

            }
            else if(SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
                {
                    SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
                    selectMotorSpecifiedRiskType();
                    return true;
                }
            
            else
                {
                    SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
                    System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");
                    return false;

                }

    }
     
    private boolean selectMotorSpecifiedRiskType() 
    {
        if(!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            return false;
        }
        
        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return false;
        }
            SeleniumDriverInstance.pause(2000);
            SeleniumDriverInstance.takeScreenShot( "Motor Specified Risk type selected successfully", false);
            return true;
    }

    private boolean enterMotorDetails()
    {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
              {
                return false;
              }

            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SearchVehicleModelIconId()))
              {
                return false;
              }

            SeleniumDriverInstance.pause(3000);
            
          
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleSourceDropdownListId(),
                    testData.TestParameters.get("Source")))
                {
                  return false;
                }
                
                SeleniumDriverInstance.pause(3000);
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MakeDropdownListId(),
                    testData.TestParameters.get("Vehicles Make")))
              {
                return false;
              }

            SeleniumDriverInstance.pause(3000);

            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.VehicleYearDropdownListId(),
                    testData.TestParameters.get("Vehicles Year")))
              {
                return false;
              }

            SeleniumDriverInstance.pause(3000);

            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.ModelDropdownListId(),
                    testData.TestParameters.get("Vehicles Model")))
              {
                return false;
              }
            

            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.VehicleModelSelectedRowId()))
              {
                return false;
              }

            SeleniumDriverInstance.pause(3000);
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.VehicleTypeOptionsEndorsementsCheckboxId(), true))
                {
                    return false;
                }
            
        System.out.println("Options...");
        
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.EndorsementsCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesCheckBoxId(), true))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriversCheckBoxId(), true))
            {
              return false;
            }
            
        System.out.println("Claims history...");

            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.ClaimRebateDropdownListId(),
                    testData.TestParameters.get("No Claim Rebate")))
              {
                return false;
              }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
           return false;
        }
            
        System.out.println("Registration Details...");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EngineNumberTextboxId(),
                testData.TestParameters.get("Engine Number")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ChassisNumberTextboxId(),
                testData.TestParameters.get("Chassis Number")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationNumberTextboxId(),
                generateDateTimeString()))
            {
              return false;
            }
            
            try
            {
                 JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver  ;
                 js.executeScript("VerifyEPLRegistrationNumber();");
            }
            catch(Exception e)
            {
                System.err.println("Failed to verify Registration number");
            }
          
            SeleniumDriverInstance.pause(3000);
            
            SeleniumDriverInstance.checkPresenceofElementById("popup_ok", "popup_ok");
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegOwnerTextBoxId(),
                testData.TestParameters.get("Registered Owner")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationDateTextboxId(),
                testData.TestParameters.get("Registration Date")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NATISCodeDropdownListId(),
                testData.TestParameters.get("NATIS Code")))
            {
              return false;
            }
            
            System.out.println("Motor Cover...");
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.CoverTypeDropdownListId(),
                testData.TestParameters.get("Cover Type")))
            {
              return false;
            }
            
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TradeClassDropdownListId(),
                testData.TestParameters.get("Trade Class")))
            {
              return false;
            }
            
            System.out.println("Extensions...");
            
            if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsFireandExplosionRateTextboxId(), this.getData("Extensions - Fire and Explosion - Rate")))
            {
            return false;
            }
            
            if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsFireandExplosionPremiumTextboxId(), this.getData("Extensions - Fire and Explosion - Premium")))
            {
            return false;
            }
           
            if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsThirdPartLiabilityRateTextBoxId(),  this.getData("Extensions - Third Party Liability Rate")))
            {
            return false;
            }
        
         if(!SeleniumDriverInstance.enterTextById(EkunduMotorSpecifiedRiskPage.ExtensionsThirdPartLiabilityPremiumTextBoxId(),  this.getData("Extensions - Third Party Liability Premium")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
        {
           return false;
        }
         
         SeleniumDriverInstance.pause(3000);
         
         if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
           return false;
        }
  
           return true;  
    }
    
    private boolean enterFAPDetails()
    {
         if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduMotorSpecifiedRiskPage.VoluntaryFAPCheckBoxId(), true))
        {
            return false;
        }
         
         if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.VoluntaryDropdownListId(),
                testData.TestParameters.get("Voluntary FAP")))
            {
              return false;
            }
         
            SeleniumDriverInstance.takeScreenShot("First Amount Payable details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
         return true;
     }
     
    private boolean enterEndorsements()
    {
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsSelectButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsAddAllButtonId()))
         {
             return false;
         }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.EndorsementsApplyButtonId()))
         {
             return false;
         }
         
         SeleniumDriverInstance.takeScreenShot("Endorsement details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        
            return true;
         
     }
    
    private boolean enterSpecifiedDriverDetails()
    {

        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddSpecifiedDriverButtonName()))
          {
            return false;
          }
        

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverNameTextBoxId(),
            testData.TestParameters.get("Regular Driver Name")))
            {
              return false;
            }
          
          if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMotorSpecifiedRiskPage.EBPSpecifiedDriverIDTypeDropdownListId(),  testData.TestParameters.get("ID Type")))
            {
               return false;
            }
          
          if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverIDNumberTextBoxId(),
            testData.TestParameters.get("ID Number")))
            {
              return false;
            }         
          
          SeleniumDriverInstance.takeScreenShot("Specified driver details entered successfully", false);
          
          if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
          
          SeleniumDriverInstance.takeScreenShot("Specified driver details entered successfully", false);
          
          if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
          
          return true;
         
    }
    
    private boolean enterInterestedPartiesDetails()
    { 
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.MSAddInterestedPartiesButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.EBPMotorIPTypeofAgreementDropdownListId(),
                testData.TestParameters.get("Type of Agreement")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.EBPMotorIPInstitutionNameDropdownListId(),
                testData.TestParameters.get("Institution Name")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EBPMotorIPDescriptionTextBoxID(),
            testData.TestParameters.get("IP - Description")))
            {
              return false;
            }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }

            SeleniumDriverInstance.takeScreenShot("Interested parties details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
          {
            return false;
          }
            
        return true;      
    }
    
     private boolean enterNotes()
    {   
      
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
        
        
        System.out.println("Reinsurance Details");
       
        SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        
        SeleniumDriverInstance.pause(2000);
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BandDropdownListId(),
                testData.TestParameters.get("Reinsurance Band")))
          {
            return false;
          }   
        
        SeleniumDriverInstance.pause(2000);
        
        SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        
        SeleniumDriverInstance.pause(3000);
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceOkButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Reinsurance Details entered successfully", false);
            
        return true;
              
    }
     
}
