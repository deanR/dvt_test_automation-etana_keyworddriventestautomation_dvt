
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduPublicLiabilityRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduPublicLiabilityRiskTest extends BaseClass {

    TestEntity testData;
    TestResult testResultt;

    public EkunduPublicLiabilityRiskTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the add risk button is present ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify that the add risk button is present - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterIndustryDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter industry details ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter industry details - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterGeneralTenantsDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter General Tenants details ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter General Tenants details - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterDefectiveWorkmanshipDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Defective Workmanship details ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Defective Workmanship details - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterProductsLiabilityDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Products Liability details ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Products Liability details - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterExtensions()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk Extension", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter risk Extension - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterRiskEndorsements()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk Endorsements", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter risk Endorsements - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterRiskNotes()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk notes", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter risk notes - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        SeleniumDriverInstance.takeScreenShot("Public Liability risk entered successfully", false);

        return new TestResult(testData, Enums.ResultStatus.PASS,
                "Public Liability risk entered successfully"
                + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());

    }

    private boolean verifyAddRiskButtonisPresent() {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectPublicLiabilityRisk();

            return true;
        } else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            selectPublicLiabilityRisk();
        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");

            return false;
        }

        return true;
    }

    private boolean selectPublicLiabilityRisk() {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Public Liability risk selected sucessfully", false);

        return true;
    }

    private boolean enterIndustryDetails() {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(),
                this.getData("Risk Addresses Industry"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.IndustrySearchResultsSelectedRowId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Industry details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduPublicLiabilityRiskPage.RiskDefectiveWorkmanshipCheckboxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduPublicLiabilityRiskPage.RiskProductsCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityNextButtonId())) {
            return false;
        }

        return true;

    }

    private boolean enterGeneralTenantsDetails() {
        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.GeneralTernantsLimitofIndemnityTextboxId(),
                this.getData("General Tenants Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.GeneralTernantsPremiumTextboxId(),
                this.getData("General Tenants Premium"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("General Tenants details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityNextButtonId())) {
            return false;
        }

        return true;
    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean enterDefectiveWorkmanshipDetails() {
        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.DefectiveWorkmanshipLimitofIndemnityTextboxId(),
                this.getData("Defective Workmanship Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.DefectiveWorkmanshipTurnoverTextboxId(),
                this.getData("Defective Workmanship Turnover"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduPublicLiabilityRiskPage.DefectiveWorkmanshipQuestionnaireCheckboxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityRateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Defective Workmanship details entered successfully", true);

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityNextButtonId())) {
            return false;
        }

        return true;

    }

    private boolean enterProductsLiabilityDetails() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduPublicLiabilityRiskPage.ProductsLiabilityBasisDropdownListId(),
                this.getData("Products Liability Basis"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.ProductsLiabilityLimitofIndemnityTextboxId(),
                this.getData("Products Liability Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.ProductsLiabilityTurnoverTextboxId(),
                this.getData("Products Liability Turnover"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduPublicLiabilityRiskPage.ProductsLiabilityFoodPoisoningOnlyCheckboxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduPublicLiabilityRiskPage.ProductsLiabilityQuestionnairesReceivedCheckboxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityRateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Products Liability details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterExtensions() {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduPublicLiabilityRiskPage.ExtensionsLegalDefenceCheckboxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.ExtensionsLegalDefenceRateTextboxId(),
                this.getData("Extensions Legal Defence Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.ExtensionsLegalDefencePremiumTextboxId(),
                this.getData("Extensions Legal Defence Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduPublicLiabilityRiskPage.ExtensionsSpreadofFireCheckboxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.ExtensionsSpreadofFireLimitofIndemnityTextboxId(),
                this.getData("Extensions Spread of Fire Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.ExtensionsSpreadofFireRateTextboxId(),
                this.getData("Extensions Spread of Fire Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.ExtensionsSpreadofFAPPercentageTextboxId(),
                this.getData("Extensions Spread of Fire FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.ExtensionsSpreadofFAPAmountTextboxId(),
                this.getData("Extensions Spread of Fire FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduPublicLiabilityRiskPage.ExtensionsWrongfulArrestCheckboxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.ExtensionsWrongfulArrestRateTextboxId(),
                this.getData("Extensions Wrongful Arrest Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.ExtensionsWrongfulArrestPremiumTextboxId(),
                this.getData("Extensions Wrongful Arrest Premium"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Risk extensions entered succesffully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityNextButtonId());

        return true;
    }

    private boolean enterRiskEndorsements() {
        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.EndorsementsSelectButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.EndorsementsAddAllButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.EndorsementsApplyButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Risk Endorsements entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityNextButtonId())) {
            return false;
        }

        return true;

    }

    private boolean enterRiskNotes() {
        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.RiskCommentsTextboxId(),
                this.getData("Risk Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.AddPolicyNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduPublicLiabilityRiskPage.RiskNotesTextboxId(),
                this.getData("Risk Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduPublicLiabilityRiskPage.PublicLiabilityNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Documents");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementById("btnGeneratePdf");

        SeleniumDriverInstance.clickElementById("popup_ok");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Risks");

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
