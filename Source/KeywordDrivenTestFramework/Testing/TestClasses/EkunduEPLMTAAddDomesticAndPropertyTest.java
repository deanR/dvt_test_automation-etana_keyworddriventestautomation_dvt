
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author ferdinandN
 */
public class EkunduEPLMTAAddDomesticAndPropertyTest extends BaseClass {

    TestEntity testData;
    TestResult testResultt;

    public EkunduEPLMTAAddDomesticAndPropertyTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        this.setStartTime();

        if (!navigateToFindClientPage()) {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find  client page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to navigate to the find  client page- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!findClient()) {
            SeleniumDriverInstance.takeScreenShot("Failed to find client", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterMidTermAdjustmentData()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter mid term adjustment data", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter mid term adjustment data- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!selectDomesticandPropertyRiskType()) {
            SeleniumDriverInstance.takeScreenShot("Failed to select Domestic and Property Risk", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to select Domestic and Property Risk- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!addSurveyType()) {
            SeleniumDriverInstance.takeScreenShot("Failed to add survey", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to add survey- " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterRiskCoverage()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk coverage", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter risk coverage- " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterBuildingsDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter building details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter building details- " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        SeleniumDriverInstance.takeScreenShot(("Domestic and Property risk added successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Domestic and Property risk added successfully",
                this.getTotalExecutionTime());

    }

    private boolean navigateToFindClientPage() {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),
                EKunduHomePage.FindClientTabXPath())) {
            return false;
        }

        return true;
    }

    private boolean findClient() {
        String clientCode = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                "Retrieved Client Code");

        if (clientCode.equals("parameter not found")) {
            clientCode = testData.TestParameters.get("Client Code");
        } else {
            testData.updateParameter("Client Code", clientCode);
        }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId())) {
            return false;
        }

        return true;
    }

    private boolean enterMidTermAdjustmentData() {
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText())) {
            return false;
        }

        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                "Policy Reference Number");

        if (policyNumber.equals("parameter not found")) {
            policyNumber = testData.TestParameters.get("Policy Number");
        } else {
            testData.updateParameter("Policy Number", policyNumber);
        }

//         if(!SeleniumDriverInstance.changePolicy(policyNumber, "Change"))
//        {
//            return false;
//        }
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ChangePolicyLinkText())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MTATypeofChangeDropDownListId(),
                testData.TestParameters.get("Type of Change"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MIAEffectiveDateTextBoxId(),
                testData.TestParameters.get("Effective Date"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId())) {
            return false;
        }

        return true;

    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean selectDomesticandPropertyRiskType() {
        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"));

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Domestic and Property Risk type selected successfully", false);

        return true;
    }

    private boolean addSurveyType() {
        // this.clickAddRiskItemButton();

        //SeleniumDriverInstance.Driver.navigate().refresh();
        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddSurveyButtonName())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.SelectSurveyTypeDropdownListId(),
                testData.TestParameters.get("Survey Type"))) {
            return false;
        }

        SeleniumDriverInstance.clearTextById(EkunduCreateNewPolicyForNewClientPage.SurveyDateTextBoxId());

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SurveyDateTextBoxId(),
                testData.TestParameters.get("Survey Date"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SurveyReferenceTextBoxId(),
                testData.TestParameters.get("Survey Reference"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SurveyReportCommentsTextAreaId(),
                testData.TestParameters.get("Comments"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Survey type entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterRiskCoverage() {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.CheckBuildingCheckBoxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TypeofResidenceDropdownListId(),
                testData.TestParameters.get("Type of Residence"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TypeofConstructionDropdownListId(),
                testData.TestParameters.get("Type of Construction"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.OccupancyDropdownListId(),
                testData.TestParameters.get("Occupancy"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.LocalityDropdownListId(),
                testData.TestParameters.get("Locality"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Risk coverage entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.RiskCoverageNextButtonName())) {
            return false;
        }

        return true;
    }

    private boolean enterBuildingsDetails() {
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BuildingSumInsuredTextBoxId(),
                testData.TestParameters.get("Building Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Building details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.RiskCoverageNextButtonName())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_ReInsurance2007Cntrl_ddlReinsurance",
                "EPL - Personal Property")) {
            return false;
        }

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
