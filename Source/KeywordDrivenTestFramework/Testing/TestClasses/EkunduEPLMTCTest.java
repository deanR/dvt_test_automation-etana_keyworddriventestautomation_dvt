
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPageNew;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPageNew;

import org.openqa.selenium.JavascriptExecutor;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author ferdinandN
 */
public class EkunduEPLMTCTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduEPLMTCTest(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToFindClientPage())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find client page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to navigate to the find client page - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!findClient())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to find client", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterMidTermAdjustmentData())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Mid Term Adjustment data", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Mid Term Adjustment data- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!quotePolicyRisks())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to quote policy risks", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to quote policy risks- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!finalisePolicy())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to finalise Personal Client Policy"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to finalise Personal Client Policy- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Policy Mid-term Cancellation completed successfully",
                              this.getTotalExecutionTime());

      }

    private boolean navigateToFindClientPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),
                EKunduHomePage.FindClientTabXPath()))
          {
            return false;
          }

        return true;
      }

    private boolean findClient()
      {
        String clientCode =
            SeleniumDriverInstance.retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                "Retrieved Client Code");

        if (clientCode.equals("parameter not found"))
          {
            clientCode = testData.TestParameters.get("Client Code");
          }
        else
          {
            testData.updateParameter("Client Code", clientCode);
          }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
          {
            return false;
          }

        return true;
      }

    private boolean verifyClientDetailsPageHasLoaded()
      {
        if (!SeleniumDriverInstance.validateElementTextValueById(EkunduViewClientDetailsPage.ViewClientDetailsLabelId(),
                "View Client Details"))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Details page successfully loaded", false);

        return true;
      }

    private boolean enterMidTermAdjustmentData()
      {
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementbyLinkText("Change"))
          {
            SeleniumDriverInstance.pause(5000);

            JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;

            js.executeScript("javascript:__doPostBack('ctl00$cntMainBody$ClientPolicies$grdvQuotes','Page$2')");

            SeleniumDriverInstance.clickElementbyLinkText("Change");
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MTATypeofChangeDropDownListId(),
                this.getData("Type of Change")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MIAEffectiveDateTextBoxId(),
                this.getData("Effective Date")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.acceptAlertDialog();

        return true;

      }

     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean quotePolicyRisks()
      {
          System.out.println("Quoting Domestic and Property risk");
          if(SeleniumDriverInstance.checkRiskActionElement())
         {
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPageNew.DomesticandPropertyEditLinkId()))
              {
                return false;
              }
         }
          else
          {
              
            if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.DomesticandPropertyRiskActionDropdownListXpath(), "Edit"))
                {
                    return false;
                }
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.takeScreenShot("Domestic and Property risk quoted successfully", false);

        System.out.println("Quoting Personal Accident risk");
        if(SeleniumDriverInstance.checkRiskActionElement())
        {
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPageNew.PersonalAccidentEditLinkId()))
              {
                return false;
              }
        }
        else
        {
            if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.PersonalAccidentRiskActionDropdownListXpath(), "Edit"))
            {
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.takeScreenShot("Personal Accident risk quoted successfully", false);

        System.out.println("Quoting Enroute  risk");
    
        if(SeleniumDriverInstance.checkRiskActionElement())
        {
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPageNew.EnrouteRiskEditLinkId()))
              {
                return false;
              }
        }
        else
        {
            if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.EnrouteRiskActionDropdownListXpath(), "Edit"))
            {
                return false;
            }
            
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.takeScreenShot("Enroute risk quoted successfully", false);

         System.out.println("Quoting Water and Pleasure Craft risk");
        
        if(SeleniumDriverInstance.checkRiskActionElement())
        {
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPageNew.WaterandPleasureCraftRiskEditLinkId()))
              {
                return false;
              }
        }
        else
        {
            if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.WaterandPleasureCraftRiskActionDropdownListXpath(), "Edit"))
        {
            return false;
        }
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.takeScreenShot("Water and Pleasure Craft risk quoted successfully", false);

//      SeleniumDriverInstance.pause(5000);
//      
//      JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;
//
//        js.executeScript("javascript:__doPostBack('ctl00$cntMainBody$MultiRisk1$grdvRisk','Page$2')");
//        
//        SeleniumDriverInstance.pause(5000);
//    
        if(SeleniumDriverInstance.checkRiskActionElement())
        {
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPageNew.MotorRiskEditLinkId()))
            {
                return false;
            }
        }
        else
        {
            if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.MotorRiskActionDropdownListXpath(), "Edit"))
            {
                return false;
            }
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
        {
            return false;
        }
        
         if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
         
      SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
      SeleniumDriverInstance.pause(2000);
      SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
      
      SeleniumDriverInstance.takeScreenShot("Motor risk quoted successfully", false);
    
      SeleniumDriverInstance.pause(5000);
      
       System.out.println("Quoting Domestic and Property Risk");
        
        if(SeleniumDriverInstance.checkRiskActionElement())
        {
            if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl08_lnkbtnEdit"))
            {
              return false;
            }
        }
        else
        {
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath("//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[7]/td[12]/select[@id = \"actionDDL\"]",
                    "Edit"))
            {
                return false;
            }
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BuildingsNextButtonId()))
        {
            return false;
        }
        
         if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
         
      SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
      SeleniumDriverInstance.pause(2000);
      SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
      
      SeleniumDriverInstance.takeScreenShot("Domestic and Propert risk quoted successfully", false);
    

        SeleniumDriverInstance.takeScreenShot("Policy risks quoted successfully", false);

        return true;

      }

    private boolean finalisePolicy()
      {
        if (SeleniumDriverInstance.checkRiskActionElement())
          {
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalLinesEditLinkId()))
              {
                return false;
              }
          }
        else
          {
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(),
                    "Edit"))
              {
                return false;
              }
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalLinesNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalLinesNextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.scrollElementIntoViewById(EkunduCreateNewPolicyForNewClientPage.MakePolicyLiveButtonId());

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MakePolicyLiveButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(120000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OOSOKButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Policy finalised successfully", false);

        SeleniumDriverInstance.pause(60000);

        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
