
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.CurrentEnvironment;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduElectronicEquipmentRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

/**
 *
 * @author FerdinandN
 */
public class EkunduMidtermAdjustment4Test extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public EkunduMidtermAdjustment4Test(TestEntity testData) {
        this.testData = testData;

    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToFindClientPage()) {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find client page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to navigate to the find client page - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!findClient()) {
            SeleniumDriverInstance.takeScreenShot("Failed to find client", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!verifyClientDetailsPageHasLoaded()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the client details page has loaded", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify that the client details page has loaded- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterMidTermAdjustmentData()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Mid Term Adjustment data", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Mid Term Adjustment data- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!deleteGroupedFireRisk()) {
            SeleniumDriverInstance.takeScreenShot("Failed to delete grouped fire risks", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to delete grouped fire risks- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!editEletctronicEquipmentRisk()) {
            SeleniumDriverInstance.takeScreenShot("Failed to edit Electronic Equipment risk", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to edit Electronic Equipment risk- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

//        if (!quoterisks())
//          {
//            SeleniumDriverInstance.takeScreenShot("Failed to quote policy risks", true);
//
//            return new TestResult(testData, false,
//                                  "Failed to quote policy risks risks- "
//                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//          }
        return new TestResult(testData, Enums.ResultStatus.PASS, "Mid Term Adjustment 4 completed successfully",
                this.getTotalExecutionTime());
    }

    private boolean navigateToFindClientPage() {
        if (!SeleniumDriverInstance.navigateToFindClientPage(EkunduViewClientDetailsPage.ClientHoverTabId(), "Find Client")) {
            return false;
        }

        return true;
    }

    private boolean findClient() {
        String clientCode
                = SeleniumDriverInstance.retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                        "Retrieved Client Code");

        if (clientCode.equals("parameter not found")) {
            clientCode = testData.TestParameters.get("Client Code");
        } else {
            testData.updateParameter("Client Code", clientCode);
        }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId())) {
            return false;
        }

        return true;
    }

    private boolean verifyClientDetailsPageHasLoaded() {
        if (!SeleniumDriverInstance.validateElementTextValueById(
                EkunduViewClientDetailsPage.ViewClientDetailsLabelId(), "View Client Details")) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Client Details page successfully loaded", false);

        return true;
    }

    private boolean enterMidTermAdjustmentData() {
        if (!SeleniumDriverInstance.clickElementbyLinkText(
                EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText())) {
            return false;
        }

        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                "Retrieved Policy Number");

        if (policyNumber.equals("parameter not found")) {
            policyNumber = testData.TestParameters.get("Policy Number");
        } else {
            testData.updateParameter("Policy Number", policyNumber);
        }

//        if (!SeleniumDriverInstance.changePolicy(policyNumber, "Change")) {
//            return false;
//        }
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ChangePolicyLinkText())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduViewClientDetailsPage.MTATypeofChangeDropDownListId(), this.getData("Type of Change"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MIAEffectiveDateTextBoxId(),
                this.getData("Effective Date"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId())) {
            return false;
        }

        return true;

    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean quoterisks() {
        JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;

        js.executeScript("javascript:__doPostBack('ctl00$cntMainBody$MultiRisk1$grdvRisk','Page$2')");

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.editGroupedFireRisk5LinkId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance
                .clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        js.executeScript("javascript:__doPostBack('ctl00$cntMainBody$MultiRisk1$grdvRisk','Page$2')");

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.editGroupedFireRisk6LinkId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance
                .clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        js.executeScript("javascript:__doPostBack('ctl00$cntMainBody$MultiRisk1$grdvRisk','Page$3')");

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.editGroupedFireRisk7LinkId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance
                .clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;
    }

    private boolean deleteGroupedFireRisk() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(
                EkunduViewClientDetailsPage.GroupedFireRiskDelete1ActionDropdownListXpath(), "Delete")) {
            return false;
        }

        SeleniumDriverInstance.acceptAlertDialog();
        System.out.println("Duplicate Grouped Fire 1 deleted successfully");

        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(
                EkunduViewClientDetailsPage.GroupedFireRiskDelete2ActionDropdownListXpath(), "Delete")) {
            return false;
        }

        SeleniumDriverInstance.acceptAlertDialog();
        System.out.println("Duplicate Grouped Fire 2 deleted successfully");
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(
                EkunduViewClientDetailsPage.GroupedFireRiskDelete3ActionDropdownListXpath(), "Delete")) {
            return false;
        }

        SeleniumDriverInstance.acceptAlertDialog();
        System.out.println("Duplicate Grouped Fire 3 deleted successfully");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.takeScreenShot("Grouped fire duplate risks deleted succesfully", false);

        return true;
    }

    private boolean editEletctronicEquipmentRisk() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(
                EkunduViewClientDetailsPage.ElectronicEquipmentRiskActionDropdownListXpath(), "Edit")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        // SeleniumDriverInstance.Driver.navigate().refresh();
        if (!SeleniumDriverInstance.clickElementbyLinkText("Delete")) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        //.clickAddEERiskItemButton();
        if (!SeleniumDriverInstance.clickElementbyName(EkunduElectronicEquipmentRiskPage.AddEERiskItemButtonName())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduElectronicEquipmentRiskPage.ItemDetailsCategoryDropdownListId(),
                this.getData("EE - Item Details - Category"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsDescriptionTextboxId(),
                this.getData("EE - Item Details - Description"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsSumInsuredTextboxId(),
                this.getData("EE - Item Details - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduElectronicEquipmentRiskPage.ItemDetailsFlatPremiumCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduElectronicEquipmentRiskPage.ItemDetailsFlatPremiumTextboxId(),
                this.getData("EE - Item Details - Flat Premium"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Item Details Entered Sucessfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        SeleniumDriverInstance
                .clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(5000);

        return true;
    }

    private boolean clickAddEERiskItemButton() {
        WebElement cell = null;
        if (CurrentEnvironment == Enums.TestEnvironments.QA) {
            cell = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_EE__ITEM\"]/table/tfoot/tr/td[9]/input[@value = \"Add\"]"));
        }

        if (cell == null) {
            cell = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_EE__ITEM\"]/table/tfoot/tr/td[10]/input[@value = \"Add\"]"));
        }

        if (cell != null) {
            cell.click();
            System.out.println("[TestInfo] Add Risk Item Button Clicked");

            return true;
        } else {
            System.err.println("[Error] Failed to click add risk item button");

            return false;
        }

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
