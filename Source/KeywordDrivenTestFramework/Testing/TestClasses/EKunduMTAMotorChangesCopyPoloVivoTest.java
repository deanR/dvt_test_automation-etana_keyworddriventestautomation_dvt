/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author ferdinandN
 */
public class EKunduMTAMotorChangesCopyPoloVivoTest extends BaseClass
{
    TestEntity testData;
    TestResult testResult;
    
    public EKunduMTAMotorChangesCopyPoloVivoTest(TestEntity testData)
    {
        this.testData = testData;

    }
    
    public TestResult executeTest()
    {  
         SeleniumDriverInstance.DriverExceptionDetail = "";
         this.setStartTime();
         
         if(!navigateToFindClientPage())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to navigate to the find client page", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to navigate to the find client page - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
    
        if(!findClient())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to find client", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to find client - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterMidTermAdjustmentData())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to enter Mid Term Adjustment data", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Mid Term Adjustment data - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }  
        
        if(!copyMotorPoloVivo())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to copy Motor risk", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to copy Motor risk - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

            return new TestResult(testData, Enums.ResultStatus.PASS, "Motor Risk (Rolls Royce) edited successfully", this.getTotalExecutionTime());
        
}
    
    private boolean navigateToFindClientPage()
    {       
        if(!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),EKunduHomePage.FindClientTabXPath()))
        {
            return false;
        }
            return true;       
    }
    
    private boolean findClient()
    { 
            String clientCode = SeleniumDriverInstance.retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"), "Retrieved Client Code");
        
        if(clientCode.equals("parameter not found"))
        {
            clientCode = testData.TestParameters.get("Client Code");
        }
        else
            testData.updateParameter("Client Code", clientCode);
            
        if(!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Client Found", false);
        
        if(!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean enterMidTermAdjustmentData()
    {
         if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText()))
        {
            return false;
        } 
         
        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                                  "Policy Reference Number");

        if (policyNumber.equals("parameter not found"))
          {
            policyNumber = testData.TestParameters.get("Policy Number");
          }
        else
          {
            testData.updateParameter("Policy Number", policyNumber);
          }
         
//         if(!SeleniumDriverInstance.changePolicy(policyNumber, "Change"))
//        {
//            return false;
//        }
         
        SeleniumDriverInstance.pause(1000);
        if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ChangePolicyLinkText()))
        {
            return false;
        } 
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MTATypeofChangeDropDownListId(), testData.TestParameters.get("Type of Change")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MIAEffectiveDateTextBoxId(), testData.TestParameters.get("Effective Date")))
        {
            return false;
        }
         
          if(!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId()))
        {
            return false;
        }
       
          return true;
    }
    
    private boolean copyMotorPoloVivo()
    {
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.EditPoloDropdownListXpath(), "Copy"))
        {
            return false;
        }
        
         SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.CopyRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.CopyRiskDropDownListId(),
                testData.TestParameters.get("Copy Risk - Risk Type")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.CopyRiskSubmitButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }
        
//        System.out.println("Adding copied risk to policy");
//        
//        SeleniumDriverInstance.Driver.navigate().refresh();
//
//        try
//        {
//            WebElement checkbox = SeleniumDriverInstance.Driver.findElement(By.id("ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl12_chkRiskSelect"));
//            JavascriptExecutor js = (JavascriptExecutor)  SeleniumDriverInstance.Driver;
//            js.executeScript("arguments[0].click();", checkbox);
//        }
//        catch(Exception e)
//        {
//            System.err.println("Failed to add risk to policy - " + e.getMessage());
//        }
//        
//       //if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.SelectRiskCheckboxXpath(), true))
////          {
////            return false;
////          }
//        
        SeleniumDriverInstance.Driver.navigate().refresh();
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(),
                "Edit"))
          {
            return false;
          }
       
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalLinesNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText()))
            {
              return false;
            }

          if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances"))
            {
              return false;
            }
          
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        
//        if (!SeleniumDriverInstance.checkBoxSelectionById("ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl12_chkRiskSelect", true))
//          {
//            return false;
//          }
        
        
       
        SeleniumDriverInstance.pause(2000);
        
        
        
       SeleniumDriverInstance.takeScreenShot("Motor (Polo Vivo) copied successfully", false);
       
       return true;

    }
    
    }
