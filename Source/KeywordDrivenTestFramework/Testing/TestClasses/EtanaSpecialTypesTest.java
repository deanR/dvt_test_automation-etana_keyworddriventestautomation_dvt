package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPageNew;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

public class EtanaSpecialTypesTest extends BaseClass {

    TestEntity testData;
    TestResult testResultt;

    public EtanaSpecialTypesTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        this.setStartTime();

        if (testData.TestMethod.toUpperCase().trim().contains("EBP-Special Types".toUpperCase().trim())) {

            if (!verifyRisksDialogisPresent()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to select Motor type."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to select Motor type. - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);

            if (!vehicleSelection()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to complete vehicle selection section."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to complete vehicle selection section. - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);

            if (!Options()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Check all Checkboxes in the Options section."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Check all Checkboxes in the Options section. - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);

            if (!verifyingClaimsHistory()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Select No Claim rebate."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Select No Claim rebate.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);

            if (!vehicleCoverDetails()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Enter and verify Vehicle cover details."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Enter and verify Vehicle cover details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);

            if (!registrationDetails()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully enter registration details."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully enter registration details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);

            if (!motorCover()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully enter and verify Motor cover details."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully enter and verify Motor cover details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);

            if (!accesoriesAndEquipments()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully capture accessories and equipment details."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully capture accessories and equipment details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);

            if (!extentions()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully capture extention details."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully capture extention details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);

            if (!voluntaryExcess()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully capture Voluntary Excess details."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully capture Voluntary Excess details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);

            if (!additionalCompulsoryExcess()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully capture Additinal Voluntary Excess details."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully capture Additinal Voluntary Excess details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);

            if (!firstAmountPayableSection()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully capture First Amount Payable section details."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully capture First Amount Payable section details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);

            if (!endosements()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully complete the endosements section details."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully complete the endosements section details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);
            
            if (!specifiedDriverDetails()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully complete Specified Driver details section."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully complete Specified Driver details section.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);

            if (!interestedParty()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully complete Interested party details section."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully complete Interested party details section.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);
            
            if (!notes()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully complete notes details section."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully complete notes details section.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);
            
            if (!notes2()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully complete notes2 details section."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully complete notes2 details section.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);
            
            if (!reinsuranceDetails()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully complete Reinsurance details section."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully complete reninsurance details section.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            
            SeleniumDriverInstance.pause(2000);

        }
        return new TestResult(testData, Enums.ResultStatus.PASS, "Add special types was successful.", this.getTotalExecutionTime());
    }

    private String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");
            return "";
        }
    }

    /**
     * ******Clicking add Risk Button and selecting the risk type And Verifying
     * that the Risk type dialog is present*****
     */
    private boolean verifyRisksDialogisPresent() {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            selectMotorRiskType();
            return true;

        } else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            selectMotorRiskType();
            return true;

        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");
            return false;

        }

    }

    //secting risk type from Dialog
    private boolean selectMotorRiskType() {
        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        //next button
        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPageNew.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Motor risk type selected successfully", false);
        return true;
    }

    //******EBP - Motor Specified********//
    //Vehicles section
    private boolean vehicleSelection() {

        //next button
//        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId())) {
//            return false;
//        }
        //Model search Icon
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SearchVehicleModelIconId())) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);

        //Source
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("source",
                testData.TestParameters.get("Source"))) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        //Code and Model in the List
        if (!SeleniumDriverInstance.selectVehicleFromList("popupGrid")) {
            return false;
        }
        SeleniumDriverInstance.takeScreenShot("Vehicle Sellection was successful.", false);
        return true;
    }

    //Options
    private boolean Options() {

        //Endorsement CheckBox
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_MS__ENDORSE_APPL")) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        //Specified Drivers CheckBox
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_MS__SPEC_DRIVERS_APPL")) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        //Interested CheckBox
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_MS__INT_PARTIES_APPL")) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Ticked all the checkboxes under Options section.", false);
        return true;
    }

    //Claims History and Underwriting Criteria
    private boolean verifyingClaimsHistory() {

//        //Claims in Last Months Textbox
//        if (!SeleniumDriverInstance.clickElementById("")) {
//            return false;
//        }
//
//        //Number of Vehicle on policy
//        if (!SeleniumDriverInstance.clickElementById("")) {
//            return false;
//        }
//
//        //Interested Textbox
//        if (!SeleniumDriverInstance.clickElementById("")) {
//            return false;
//        }
        //No Claim Rebate Drop down 
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_MS__NCR_REBATE",
                testData.TestParameters.get("No Claim Rebate"))) {
            return false;
        }

        //Next Button
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnNext")) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Selected No Claim Rebate.", false);
        return true;
    }

    private boolean vehicleCoverDetails() {
        //Area code text box
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__AREA_CODE", testData.TestParameters.get("Area Code"))) {
            return false;
        }

//        //verify Tracking Device Drop down
//        if (!SeleniumDriverInstance.clickElementById("")) {
//            return false;
//        }
        SeleniumDriverInstance.takeScreenShot("Vehicale Cover Details were entered and verified successfull.", false);
        return true;
    }

    private boolean registrationDetails() {
        //Engine Number text box
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__ENGINE_NUM", testData.TestParameters.get("Engine Number"))) {
            return false;
        }

        //Registration Number details
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__REG_NUM", testData.TestParameters.get("Registration Number"))) {
            return false;
        }

        //Chasis number textbox
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__CHASSIS_NUM", testData.TestParameters.get("Chassis Number"))) {
            return false;
        }

        //Registered owner 
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__REG_OWNER",
                testData.TestParameters.get("Registered Owner"))) {
            return false;
        }

        //original registration date
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__REG_ORIGINAL_DATE", this.getData("Original Registration Date"))) {
            return false;
        }

        //NATIS Code
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_MS__NATIS_CODE",
                testData.TestParameters.get("NATIS code"))) {
            return false;
        }
        SeleniumDriverInstance.takeScreenShot("Registration details captured successfully.", false);
        return true;
    }

    private boolean motorCover() {

        //Verify that M&M Reatail type textbox is greyed out
        //Verify that M&M Trade price textbox is greyed out
        //Verify that Vehicle sum insured textbox textbox is greyed out
        //Verify that Basis of Settlement drop down is greyed out
        //Vehicle type drop down
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_MS__SPEC_VEH_TYPE",
                testData.TestParameters.get("Vehicle Type"))) {
            return false;
        }

        //Cover Type drop down (Third Party only)
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_MS__COVER_TYPE",
                testData.TestParameters.get("Cover Type"))) {
            return false;
        }

        //clicking add button
        if (!SeleniumDriverInstance.clickElementbyName("ctl00$cntMainBody$MS__ACCESSORIES$ctl02$ctl00")) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Motor cover details entered and verified successfully.", false);
        return true;
    }

    private boolean accesoriesAndEquipments() {

        //enter description in the textbox
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_ACCESSORIES__DESCRIPTION", this.getData("Description"))) {
            return false;
        }

        //enter sum insured in the textbox
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_ACCESSORIES__SUM_INSURED", this.getData("Sum Insured"))) {
            return false;
        }

        //clicking Finish Button
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnFinish")) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Accessories and equipment details captured successfully.", false);
        return true;
    }

    private boolean verifyAccessoryItems() {

        return true;
    }

    private boolean extentions() {
        //ticking excess waiver checkbox
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_CHECK_10")) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        //Capturing Premium in the premium textbox.
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_PREMIUM_10", this.getData("Premium"))) {
            return false;
        }

        //Clicking Rate button
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnRate")) {
            return false;
        }

        //Capturing Rate
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_AGREED_RATE_4", this.getData("Rate"))) {
            return false;
        }

        //Clicking Rate button
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnRate")) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Extension details captured successfully.", false);

        return true;
    }

    private boolean verifyingPostingPremium() {
        //posting premium textbox value verification
        if (!SeleniumDriverInstance.clickElementById("")) {
            return false;
        }

        //Verifying that Third Party Liability checkbox is already ticked and is greyed out.
        if (!SeleniumDriverInstance.clickElementById("")) {
            return false;
        }

        //Verifying limit of indeminity is auto generated
        if (!SeleniumDriverInstance.clickElementById("")) {
            return false;
        }

        //Capture rate
        if (!SeleniumDriverInstance.clickElementById("")) {
            return false;
        }

        //Verifying that the Premium is generated
        //clicking rate button
        if (!SeleniumDriverInstance.clickElementById("")) {
            return false;
        }

        //Verifying that the Posting Premium is being generated
        return true;
    }

    //Voluntary Excess section
    private boolean voluntaryExcess() {
        //Capturing Excess Percentage
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__FAP_VOL_MIN_PERC", this.getData("Excess Percentage"))) {
            return false;
        }

        //Capturing Minimum Amount 
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__FAP_VOL_MIN_AMT", this.getData("Minimum Amount"))) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Vuluntary Excess details captured successfully.", false);

        return true;
    }

    //Additional Compulsory Excess section
    private boolean additionalCompulsoryExcess() {
        //Capturing Excess Percentage
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__FAP_COMP_PERC", this.getData("Excess Percentage Additional"))) {
            return false;
        }

        //Capturing Minimum Amount 
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__FAP_COMP_AMT", this.getData("Minimum Amount Additional"))) {
            return false;
        }

        //clicking next button
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnNext")) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Vuluntary Excess Additional details captured successfully.", false);

        return true;
    }

    private boolean firstAmountPayableSection() {

        //Verify that the Basic – Own Damage (Sub-Section A) checkbox is ticked by default.
        //Verify that the Minimum% is populated by default.
        //Verify that the Minimum Amount is populated by default.
        //Capturing Maximum% 
        //own damage
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__FAP_OWN_DAMAGE_MAX_PERC", this.getData("Own damage max percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__FAP_OWN_DAMAGE_MAX_AMT", this.getData("Own Max Amount"))) {
            return false;
        }

        //Third party 
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__FAP_3RD_PARTY_MIN_PERC", this.getData("Third Party Min Percentage Amount"))) {
            return false;
        }
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__FAP_3RD_PARTY_MAX_PERC", this.getData("Third Party max Percentage Amount"))) {
            return false;
        }
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__FAP_3RD_PARTY_MAX_AMT", this.getData("Third Party max Amount"))) {
            return false;
        }

        //Fire and Exception(Sub- section B)
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__FAP_FI_MIN_PERC", this.getData("Fire min percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__FAP_FI_MAX_PERC", this.getData("Fire max percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__FAP_FI_MAX_AMT", this.getData("Fire max amount"))) {
            return false;
        }

        //Additional Theft
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_MS__FAP_TH_APPL")) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__FAP_TH_MIN_PERC", this.getData("Additional Theft min Pecentage"))) {
            return false;
        }
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__FAP_TH_MAX_PERC", this.getData("Additional Theft  max percentage"))) {
            return false;
        }
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__FAP_TH_MIN_AMT", this.getData("Additional Theft min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_MS__FAP_TH_MAX_AMT", this.getData("Additional Theft Max Amount"))) {
            return false;
        }

        //clicking continue button
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnNext")) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("First Amount Payable section details captured successfully.", false);

        return true;
    }

    private boolean endosements() {
        //clicking select button
        if (!SeleniumDriverInstance.clickElementById("_btnShowSelect")) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);
        //clicking add all button
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_MS__ENDORSEMENTS_PckTemplates_RemoveAllCmd")) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        //Verify that the clause you selected from the available section are now moved to the Selected section
        //clicking aply button to continue
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_MS__ENDORSEMENTS_btnApplySelection")) {
            return false;
        }
        //Verify that all the clause you selected are populated on the Endorsements section
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Edorsement section was successful.", false);

        //Clicking Next button to continue.
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnNext")) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Edorsement section Verification was successful.", false);

        return true;
    }

    private boolean specifiedDriverDetails() {
        //clicking add button
        if (!SeleniumDriverInstance.clickElementbyName("ctl00$cntMainBody$MS__DRIVER$ctl02$ctl00")) {
            return false;
        }

        //Capturing Driver on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_DRIVER__DRIVER", this.getData("Driver"))) {
            return false;
        }
        //Select ID Type on the dropdown menu.
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_DRIVER__ID_TYPE", this.getData("ID Type"))) {
            return false;
        }

        //Capture ID Number on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_DRIVER__ID_NR", this.getData("ID Number"))) {
            return false;
        }
        //Clicking Next button to continue.
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnNext")) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Specified Driver details captured successfully.", false);

        //Verify that driver’s details are being populated on the Specified Driver section.
        //Clicking Next button to continue.
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnNext")) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Specified driver details Verified successfully.", false);

        return true;
    }

    private boolean interestedParty() {
        //clicking add button
        if (!SeleniumDriverInstance.clickElementbyName("ctl00$cntMainBody$MS__EPL_IPARTIES$ctl02$ctl00")) {
            return false;
        }

        //Select Institution Name from the dropdown menu.
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_EPL_IPARTIES__IN_PARTY", this.getData("Institution Name"))) {
            return false;
        }
        //Select Type of Agreement from the dropdown menu.
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_EPL_IPARTIES__TOA", this.getData("Type of Agreement"))) {
            return false;
        }

        //Verify that the Description textbox is greyed out.
        //Click Next button to continue.
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnNext")) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Interested party details captured successfully.", false);

        return true;
    }

    private boolean notes() {
        //comments(Not Printed on schedule)
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_EPL_IPARTIES__COMMENTS", this.getData("comments Not Printed on schedule"))) {
            return false;
        }

        //Tick Add Notes to this Policy checkbox
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_EPL_IPARTIES__ADD_NOTES")) {
            return false;
        }

        //Capture notes Printed on schedule on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_EPL_IPARTIES__NOTES", this.getData("Notes Printed on schedule"))) {
            return false;
        }

        //Click Next button to continue.
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnNext")) {
            return false;
        }

        //Verify that the Interested Party details are being populated on the Interested Party section.  
        //Click Next button to continue.
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnNext")) {
            return false;
        }

        //Verify the Financial Overview section.
        //Click Next button to continue.
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnNext")) {
            return false;
        }

        //Verify that the Rating Details tab is already clicked.
        //Verify that you on the rating details screen.
        //Verify the financial transaction under rating details section.
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Notes details captured and verified successfully.", false);

        return true;
    }

    
    private boolean notes2() {
        //comments(Not Printed on schedule)
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS", this.getData("comments Not Printed on schedule"))) {
            return false;
        }

        //Tick Add Notes to this Policy checkbox
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_cntrlNotes_CONTROL__ADD_NOTES")) {
            return false;
        }

        //Capture notes Printed on schedule on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES", this.getData("Notes Printed on schedule"))) {
            return false;
        }

        //Click Next button to continue.
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnNext")) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Notes for motor specified details captured successfully.", false);

        return true;
    }

    private boolean reinsuranceDetails() {
        //Clicking Reinsurances Details tab.
        if (!SeleniumDriverInstance.clickElementById("ui-id-2")) {
            return false;
        }

        //Verify that you on the Reinsurances screen.
        //Verify that the Reinsurance band “MS Liability (Section B)” is already selected.
        //Verify the reinsurer financial transactions.
        //Select the Reinsurance Band “MS Own Damage (Section A)” from the dropdown menu.
        if (!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_ReInsurance2007Cntrl_ddlReinsurance", this.getData("Reinsurance Band"))) {
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        
        //Clicking Reinsurances Details tab.
        if (!SeleniumDriverInstance.clickElementById("ui-id-2")) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        //Verify the reinsurer financial transactions.
        //Click ok button to continue.
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_btnOk")) {
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Reinsurance details captured successfully.", false);

        return true;
    }

}
