
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;

/**
 *
 * @author FerdinandN
 */
public class InitiateExistingPolicy extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public InitiateExistingPolicy(TestEntity testData)
      {
        this.testData = testData;

      }

    private boolean navigateToFindClientPage()
      {
        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),
                EKunduHomePage.FindClientTabXPath()))
          {
            return false;
          }

        return true;
      }

    private boolean verifyFindClientPageHasLoaded()
      {
        SeleniumDriverInstance.takeScreenShot("Navigation to the Find Client page was successful", false);

        if (!SeleniumDriverInstance.validateElementTextValueById(EKunduFindClientPage.PageHeaderSpanId(),
                "Search Criteria"))
          {
            return false;
          }
        else
          {
            SeleniumDriverInstance.takeScreenShot("Find Client page was successfully loaded", false);
          }

        return true;
      }

    private boolean findClient()
      {
        String clientCode =
            SeleniumDriverInstance.retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                "Retrieved Client Code");

        if (clientCode.equals("parameter not found"))
          {
            clientCode = testData.TestParameters.get("Client Code");
          }
        else
          {
            testData.updateParameter("Client Code", clientCode);
          }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
          {
            return false;
          }

        return true;
      }

    private boolean verifyClientDetailsPageHasLoaded()
      {
        if (!SeleniumDriverInstance.validateElementTextValueById(EkunduViewClientDetailsPage.ViewClientDetailsLabelId(),
                "View Client Details"))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Client Details page successfully loaded", false);

        return true;
      }

    private boolean beginNewQuoteCreation()
      {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.SelectProductDropDownListId(),
                this.getData("Product")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NewQuoteButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(1000);
        SeleniumDriverInstance.takeScreenShot("Begin new ", false);

        return true;
      }

    private boolean enterSummaryDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AgentCodeButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.FindAgentDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchAgentsButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SelectAgentInResultsId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.CoverStartDateTextBoxId());

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.CoverStartDateTextBoxId(),
                this.getData("Cover Start Date")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.PaymentTermDropDownListId(),
                this.getData("Payment Term")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.CollectionFrequencyDropDownListId(),
                this.getData("Collection Frequency")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.CoverStartDateTextBoxId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Summary details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        return true;
      }
    
    public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

  }


//~ Formatted by Jindent --- http://www.jindent.com
