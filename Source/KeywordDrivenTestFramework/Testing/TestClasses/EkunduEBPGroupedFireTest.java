
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import org.openqa.selenium.JavascriptExecutor;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author FerdinandN
 */
public class EkunduEBPGroupedFireTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduEBPGroupedFireTest(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk button is present", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the Add Risk button is present - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterIndustryDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter industry details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter industry details - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterRiskSelectionDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk selection details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk selection details - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterFireDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter fire details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter fire details - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterBusinessInteruptionDetails())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Business Interuption details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Business Interuption details - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterFACPropPlacementData())
          {
            SeleniumDriverInstance.takeScreenShot("Failed to enter FAC Prop Placement data", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter FAC Prop Placement data - "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot("Grouped Fire risk details successfully entered", false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Grouped Fire risk details successfully entered ",
                              this.getTotalExecutionTime());
      }

    public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean verifyAddRiskButtonisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectGroupedFireRiskType();

            return true;
          }

        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectGroupedFireRiskType();

            return true;
          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");

            return false;
          }
      }

    private boolean selectGroupedFireRiskType()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SelectRiskTypeResultsId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Grouped Fire Risk type selected successfully", false);

        return true;
      }

    private boolean enterIndustryDetails()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(),
                this.getData("Industry")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.IndustrySearchRowId()))
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Industry details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterRiskSelectionDetails()
      {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RSFireSectionsFireCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.RSFireSectionsBusinessInterruptionCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.TypeOfConstructionDropDownListId(),
                this.getData("Type Of Construction")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk selection details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterFireDetails()
      {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireOverviewFlatPremiumCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireRiskDataBuildingsCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireRiskDataBuildingsRiskSumInsuredTextBoxId(),
                this.getData("Fire - Risk Data - Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FireRiskDataBuildingsRiskPremiumTextBoxId(),
                this.getData("Fire - Risk Data - Premium")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Fire risk entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterBusinessInteruptionDetails()
      {
        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BIOverviewFlatPremiumCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.BIOverviewIndemnityPeriodDropDownListId(),
                this.getData("BI - Overview - Indemnity Period")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.BICoverGrossProfitBasisCheckBoxId(),
                true))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BICoverGrossProfitBasisSumInsuredTextBoxId(),
                this.getData("BI - Cover - GPB Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.BICoverGrossProfitBasisPRemiumTextBoxId(),
                this.getData("BI - Cover - GPB Premium")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Business Interruption data entered successfully ", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterFACPropPlacementData()
      {
        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(),
                this.getData("Reinsurance Details - FAC Placement Reinsurer Code 1")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId());

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId(),
                this.getData("Reinsurance Details - FAC Placement - Everestre Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(),
                this.getData("Reinsurance Details - FAC Placement Reinsurer Code 2")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.HannoverreReinsurancePercTextBoxId());

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.HannoverreReinsurancePercTextBoxId(),
                this.getData("Reinsurance Details - FAC Placement - Hannoverrre  Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(),
                this.getData("Reinsurance Details - FAC Placement Reinsurer Code 3")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectMunichReinsurerLinkId()))
          {
            return false;
          }

        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.MunichReinsurancePercTextBoxId());

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MunichReinsurancePercTextBoxId(),
                this.getData("Reinsurance Details - FAC Placement - Munich Percentage")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("FAC Proc Placement data entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId()))
          {
            return false;
          }

        return true;
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
