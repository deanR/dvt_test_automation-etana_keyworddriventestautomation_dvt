/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduGoodsInTransitRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPageNew;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduGoodsInTransitRiskPageNew;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPageNew;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

/**
 *
 * @author ferdinandN
 */
public class EkunduEPLRenewalSelection_6Months_February_Test extends BaseClass
{
     TestEntity testData;
    TestResult testResult;
    
    public EkunduEPLRenewalSelection_6Months_February_Test(TestEntity testData)
    {
        this.testData = testData;

    }
    
     public TestResult executeTest()
    {  
         SeleniumDriverInstance.DriverExceptionDetail = "";
         this.setStartTime();
         
         if(!navigateToFindClientPage())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to navigate to the find client page", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to navigate to the find client page- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
         
          if(!findClient())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to find client", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterPolicyRenewalData())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to enter Mid Term Adjustment data", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Mid Term Adjustment data- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!editWaterandPleasureCraftRisk())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to edit Water and Pleasure Craft risk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to edit Water and Pleasure Craft risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
                
        if(!quoteDomesticAndPropertyRisk())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to quote risk", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to quote risk- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
                
        if(!finalisePolicy())
        {
            SeleniumDriverInstance.takeScreenShot( "Failed to finalise policy", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to finalise policy- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
            return new TestResult(testData,Enums.ResultStatus.PASS, "Policy Renewal Selection for 6 months completed succesfully", this.getTotalExecutionTime());
           
    }
         
       private boolean navigateToFindClientPage()
    {       
        if(!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(EKunduHomePage.ClientHoverTabId(),EKunduHomePage.FindClientTabXPath()))
        {
            return false;
        }
        
         SeleniumDriverInstance.pause(6000);
            return true;       
    }
    
    private boolean findClient()
    {    
        String clientCode = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),"Retrieved Client Code");
      
      if(clientCode.equals("parameter not found"))
      {
          clientCode = testData.TestParameters.get("Client Code");
      }
      else
      {
          testData.updateParameter("Client Code", clientCode);
      }
        
        if(!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Client Found", false);
        
        if(!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
        {
            return false;
        }
        
        return true;
    }
    
    private boolean findClientAndExtractPolicyNumber()
    {    
        String clientCode = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),"Retrieved Client Code");
      
      if(clientCode.equals("parameter not found"))
      {
          clientCode = testData.TestParameters.get("Client Code");
      }
      else
      {
          testData.updateParameter("Client Code", clientCode);
      }
        
        if(!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId()))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Client Found", false);
        
        if(!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText()))
        {
            return false;
        }
        
        this.RetrievePolicyNumber();
       
        return true;
    }
    
    private boolean RetrievePolicyNumber()
    {
        WebElement cell = SeleniumDriverInstance.Driver.findElement(By.xpath("//div[@id = \"ctl00_cntMainBody_ClientPolicies_updPanelClientQuotes\"]/div[2]/div/table/tbody/tr/td[1]"));
        
        String policynumber = cell.getText();
        
        System.out.println("Policy number retrieved - " +policynumber );
      
        testData.addParameter("Policy Number", policynumber);
        
        
        return true;
    }
    
    private boolean verifyClientDetailsPageHasLoaded() 
    {
        if(!SeleniumDriverInstance.validateElementTextValueById(EkunduViewClientDetailsPage.ViewClientDetailsLabelId(), "View Client Details"))
        {
            return false;
        }
            SeleniumDriverInstance.takeScreenShot( "Client Details page successfully loaded", false);
        return true;
    }
    
    private boolean enterPolicyRenewalData()
    {
         if(!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText()))
        {
            return false;
        }
         
                JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;

               js.executeScript("javascript:__doPostBack('ctl00$cntMainBody$ClientPolicies$grdvQuotes','Page$2')");  

            
         if(!SeleniumDriverInstance.clickElementbyLinkText("Details"))
        {
            return false;
        }
                 
        SeleniumDriverInstance.pause(5000);
     
          return true;
         
    }
    
    private boolean editWaterandPleasureCraftRisk()
    {
        if(SeleniumDriverInstance.checkRiskActionElement())
        {
            if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPageNew.WaterandPleasureCraftRiskEditLinkId()))
            {
                return false;
            }
        }
        else
        {
            if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.WaterandPleasureCraftRiskActionDropdownListXpath(), "Edit"))
            {
                return false;
            } 
        }
        
        SeleniumDriverInstance.scrollDownByOnePage(EkunduCreateNewPolicyForNewClientPage.HullUseDropdownListId());

        
         if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
         
         SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText());
         
         //SeleniumDriverInstance.Driver.navigate().refresh();
         SeleniumDriverInstance.pause(3000);
         
          if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
          {
               return false;
           }
        
           if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(),testData.TestParameters.get("Reinsurance Details - FAC Placement Reinsurer Code 1")))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
           {
               return false;
           }
           
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
           {
               return false;
           }
           SeleniumDriverInstance.pause(2000);
           
            SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText());

           
           SeleniumDriverInstance.pause(5000);
           
            SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId());
           
           if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId(), testData.TestParameters.get("Reinsurance Details - FAC Placement - Addison Percentage")))
           {
               return false;
           }
           
            if(!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
           {
               return false;
           }
            
            SeleniumDriverInstance.pause(5000);
            
            System.out.println("FAC Prop Placement 1 added successfully");
        
          SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPageNew.ReinsuranceBandDropdownListId(), "EPL - PC Own Damage (Section A)");
        
        SeleniumDriverInstance.pause(3000);
            
        if(!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.AddFACXOLButtonId()))
        {
            return false;
        }
                
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), testData.TestParameters.get("Munich Reinsurer Code")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPageNew.FACSelectReinsurer3LinkId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), testData.TestParameters.get("Etana Reinsurer Code")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPageNew.FACSelectReinsurer3LinkId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId());
        
         if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), testData.TestParameters.get("Med Reinsurer Code")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
        {
            return false;
        }
        
         if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLLowerLimitId(),testData.TestParameters.get("FAP LOX Lower Limit")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        
        //SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId(), testData.TestParameters.get("FAP LOX Upper Limit")))
        {
            return false;
        }
        
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        //SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId());
        
        SeleniumDriverInstance.pause(5000);
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId(),testData.TestParameters.get("Munich Participation Percentage")))
        {
            return false;
        }
        
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
         
       // SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId(), testData.TestParameters.get("Munich Premium")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPageNew.FACXOLParticipationPerc2TextboxId(), testData.TestParameters.get("Etana MOTBD Participation Percentage")))
        {
            return false;
        }
       
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
       
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPageNew.FACXOLPremium2TextboxId(), testData.TestParameters.get("Etana MOTBD Premium")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
          if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPageNew.FACXOLParticipationPerc3TextboxId(), testData.TestParameters.get("Med Participation Percentage")))
        {
            return false;
        }
       
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
       
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPageNew.FACXOLPremium3TextboxId(), testData.TestParameters.get("Med Premium")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
      
        SeleniumDriverInstance.pause(5000);
        
        SeleniumDriverInstance.takeScreenShot("FAC XOL Placement 1 completed successfully", false);
        
        SeleniumDriverInstance.pause(5000);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.FACXOLOkButtonId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.pause(3000);
            
        if(!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.AddFACXOLButtonId()))
        {
            return false;
        }
                
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), testData.TestParameters.get("Africare Reinsurer Code")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), testData.TestParameters.get("Absa Reinsurer Code")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
        {
            return false;
        }
        
        
         if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLLowerLimitId(),testData.TestParameters.get("FAP LOX Lower Limit 2")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        
        //SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId(), testData.TestParameters.get("FAP LOX Upper Limit 2")))
        {
            return false;
        }
        
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        //SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId());
        
        SeleniumDriverInstance.pause(5000);
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId(),testData.TestParameters.get("Africare Participation Percentage")))
        {
            return false;
        }
        
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
         
       // SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId(), testData.TestParameters.get("Africare Premium")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPageNew.FACXOLParticipationPerc2TextboxId(), testData.TestParameters.get("Absa ParAticipation Percentage")))
        {
            return false;
        }
       
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
       
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPageNew.FACXOLPremium2TextboxId(), testData.TestParameters.get("Absa Premium")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
         
        SeleniumDriverInstance.pause(5000);
         
         if(!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.FACXOLOkButtonId()))
        {
            return false;
        }
         
         SeleniumDriverInstance.pause(5000);
         
         SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        
        return true;
    }
    
     private boolean finalisePolicy()
     {
        if (SeleniumDriverInstance.checkRiskActionElement())
          {
            if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalLinesEditLinkId()))
              {
                return false;
              }
          }
        else
          {
            if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.RiskActionDropdownListXpath(),
                    "Edit"))
              {
                return false;
              }
          }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalLinesNextButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalLinesNextButtonId()))
        {
            return false;
        }
            
            SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
            SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
            
            SeleniumDriverInstance.scrollDownByOnePage("ctl00_cntMainBody_SummaryCoverCntrl_ctl00_AgentCommissionCntrl_lblAgentDisplay");
        
            
            
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MakePolicyLiveButtonId()))
        {
            return false;
        }
        
            SeleniumDriverInstance.acceptAlertDialog();
        
            SeleniumDriverInstance.pause(5000);
        
        if(!SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.PolicyCreationConfirmationSpanId()))
        {
            return false;
        }
        
            String PolicyRef = SeleniumDriverInstance.retrieveTextById( EkunduCreateNewPolicyForNewClientPage.PolicyRefLabelId());
     
            testData.addParameter("Policy Reference Number", PolicyRef);
            if (PolicyRef.isEmpty())      
            {
                System.err.println("Failed to retrieve policy number");
            }
            else 
            {
                System.out.println("Policy number retrieved - " + PolicyRef);
            }
            
   
            SeleniumDriverInstance.takeScreenShot("Policy created successfully", false);
        return true;
     }
     
     private boolean quoteDomesticAndPropertyRisk()
     {
         if(SeleniumDriverInstance.checkRiskActionElement())
         {
             if(!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnEdit"))
                {
                    return false;
                }
         }
         
         else
         {
             if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.DomesticandPropertyRiskActionDropdownListXpath(), "Edit"))
                {
                    return false;
                }
         }
         
         
         
         if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId()))
        {
            return false;
        }
         
         SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
         
          if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_ReInsurance2007Cntrl_ddlReinsurance", "EPL - Personal Property"))
        {
            return false;
        }
          
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
        {
            return false;
        }
           
           if(!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_grdvSearchResults_ctl06_btnSelect"))
        {
            return false;
        }
           
            if(!SeleniumDriverInstance.enterTextById("ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl03_txtThisPerc", "60"))
        {
            return false;
        }
            
            SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain");
            
            SeleniumDriverInstance.pause(5000);
            
            SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

            return true;
           
     }
    
    
    
}
