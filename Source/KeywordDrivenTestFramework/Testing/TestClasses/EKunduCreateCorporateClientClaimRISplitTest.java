
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.CreateNewClaimPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduPublicLiabilityRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.JavascriptExecutor;

//~--- JDK imports ------------------------------------------------------------
/**
 *
 * @author FerdinandN
 */
public class EKunduCreateCorporateClientClaimRISplitTest extends BaseClass {

    TestEntity testData;
    TestResult testResultt;

    public EKunduCreateCorporateClientClaimRISplitTest(TestEntity testData) {
        this.testData = testData;

    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToClaimPage()) {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the Claim Cases page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to navigate to the Claim Cases page - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        SeleniumDriverInstance.pause(3000);
        if (!verifyClaimCasePageHasLoaded()) {
            SeleniumDriverInstance.takeScreenShot("Claim Cases page failed to load", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Claim Cases page failed to load - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

//        if (!verifyCaseNumberAndVersionAreDisabled()) {
//            SeleniumDriverInstance.takeScreenShot("Claim Case Number and Version are not disabled", true);
//
//            return new TestResult(testData, Enums.ResultStatus.FAIL,
//                    "Failed to Verify that the Case Number and version are disablied on Claim Cases page - " + SeleniumDriverInstance.DriverExceptionDetail,
//                    this.getTotalExecutionTime());
//        }
        if (!clickOk()) {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Ok button was not clicked - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!selectClaimsChampion()) {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Claims champion not selected - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!submitClaimDetails()) {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Claim details not submitted" + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!OpenClaim()) {
            SeleniumDriverInstance.takeScreenShot("Claim could not be found", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Claim could not be found" + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        SeleniumDriverInstance.clearTextById(CreateNewClaimPage.lossDateTextBoxId());

        if (!findPolicy()) {
            SeleniumDriverInstance.takeScreenShot("Policy could not be found", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Policy could not be found" + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!clickFindNowButtonId()) {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Find now button was not clicked - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!selectPolicyId()) {
            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Policy was not selected - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterClaimOverviewDetails()) {
            SeleniumDriverInstance.takeScreenShot("Claim overview details could not be entered", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Claim overview details could not be entered"
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterPremiumDetails()) {
            SeleniumDriverInstance.takeScreenShot("Premium details could not be entered", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Premium details could not be entered"
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
//        if (!enterClaimPerils()) {
//            SeleniumDriverInstance.takeScreenShot("Claim perils could not be entered", true);
//
//            return new TestResult(testData, Enums.ResultStatus.FAIL,
//                    "Claim perils could not be entered" + SeleniumDriverInstance.DriverExceptionDetail,
//                    this.getTotalExecutionTime());
//        }

        if (!verifyClaimHasBeenOpened()) {
            SeleniumDriverInstance.takeScreenShot("Claim could not be opened", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Claim could not be opened" + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }
        return new TestResult(testData, Enums.ResultStatus.PASS, "Corporate Client Claim successfully opened",
                this.getTotalExecutionTime());
    }

    private boolean enterClaimPerils() {
        SeleniumDriverInstance.clickElementById(CreateNewClaimPage.PerilsSubmitButtonId());

        /*
         *      JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;
         *
         * js.executeScript("document.getElementById('"+ CreateNewClaimPage.PerilsSubmitButtonId() + "').click();");
         */
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimReinsuranceOkButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimConfirmationNoButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);

        // SeleniumDriverInstance.clickElementbyXpath(CreateNewClaimPage.ClaimConfirmationNoButtonXpath());
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.OkButtonId())) {
            return false;
        }
        return true;
    }

    private boolean navigateToClaimPage() {

        if (!SeleniumDriverInstance.hoverOverElementAndClickSubElementbyIdAndXpath(CreateNewClaimPage.claimHoverTabId(),
                CreateNewClaimPage.newCaseLinkXPath())) {
            return false;
        }

//        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.claimHoverTabId())) {
//            return false;
//        }
//
//        SeleniumDriverInstance.pause(1000);
//        if (!SeleniumDriverInstance.ArrowdownToElementById(CreateNewClaimPage.claimHoverTabId())) {
//            return false;
//        }
////        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.OkButtonId())) {
////            return false;
////        }
        SeleniumDriverInstance.takeScreenShot("Navigation to the claims page was successful", false);

        return true;
    }

    private boolean verifyClaimCasePageHasLoaded() {
        SeleniumDriverInstance.pause(2000);
        try {
            if (!SeleniumDriverInstance.waitForElementById(CreateNewClaimPage.contentSpanId())) {
                return false;
            }

            SeleniumDriverInstance.takeScreenShot("Claims page loaded successfully", false);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean verifyCaseNumberAndVersionAreDisabled() {
        if (SeleniumDriverInstance.verifyElementDisabledElementById(CreateNewClaimPage.caseNumberTextboxId())) {
            return false;
        }
        if (SeleniumDriverInstance.verifyElementDisabledElementById(CreateNewClaimPage.caseVersionTextboxId())) {
            return false;
        }
        return true;
    }

    private boolean clickOk() {
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.OkButtonId())) {
            return false;
        }

        return true;
    }

    private boolean selectClaimsChampion() {
        SeleniumDriverInstance.WaitUntilDropDownListPopulatedById(CreateNewClaimPage.claimsChampionDropDownListId());

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.claimsChampionDropDownListId(),
                testData.TestParameters.get("Claims Champion"))) {
            return false;
        }

        return true;
    }

    private boolean submitClaimDetails() {
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.submitButtonId())) {
            return false;
        }

        return true;
    }

    private boolean verifyClaimHasBeenOpened() {
        SeleniumDriverInstance.pause(2000);
        if (!SeleniumDriverInstance.waitForElementById(CreateNewClaimPage.ClaimNumberSpanId())) {
            return false;
        }

        testData.addParameter("Retrieved Corporate Claim Number", SeleniumDriverInstance.retrieveTextById(CreateNewClaimPage.ClaimNumberSpanId()));

        SeleniumDriverInstance.takeScreenShot("Corporate Claim opened successfully", false);
        return true;
    }

    private boolean OpenClaim() {
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.openClaimButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Claim opened successfully", false);

        return true;

    }

    private boolean findPolicy() {
        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                "Retrieved Policy Number");

        if (policyNumber.equals("parameter not found")) {
            policyNumber = testData.TestParameters.get("Policy Number");
        } else {
            testData.updateParameter("Policy Number", policyNumber);
        }

        if (!SeleniumDriverInstance.enterTextById(CreateNewClaimPage.policynumberTextBoxId(), policyNumber)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(CreateNewClaimPage.lossDateTextBoxId(),
                testData.TestParameters.get("Loss Date"))) {
            return false;
        }

        return true;

    }

    private boolean clickFindNowButtonId() {
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.findnowButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.waitForElementById(CreateNewClaimPage.SelectPolicyLinkId())) {
            SeleniumDriverInstance.takeScreenShot("Policy doesn't exist", true);

            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Policy found", false);

        return true;
    }

    private boolean selectPolicyId() {
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.SelectPolicyLinkId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Policy selected", false);

        return true;
    }

    private boolean enterClaimOverviewDetails() {
        SeleniumDriverInstance.pause(3000);
        SeleniumDriverInstance.WaitUntilDropDownListPopulatedById(CreateNewClaimPage.RiskTypeDropdownListId());

//        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.RiskTypeDropdownListId(),
//                testData.TestParameters.get("Risk Type"))) {
//            return false;
//        }

        if (!SeleniumDriverInstance.enterTextById(CreateNewClaimPage.RiskTypeDropdownListId(),
                testData.TestParameters.get("Risk Type"))) {
            return false;
        }
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.ProgressStatusDropdownListId(),
                testData.TestParameters.get("Progress Status"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.PrimaryCauseDropdownListId(),
                testData.TestParameters.get("Primary Cause"))) {
            return false;
        }

        JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;

        js.executeScript("__doPostBack('UpdateSecondaryCause', '');setTimeout('__doPostBack(\\'ctl00$cntMainBody$CONTROL__PRIMARY_CAUSE\\',\\'\\')', 0)");

        SeleniumDriverInstance.WaitUntilDropDownListPopulatedById(CreateNewClaimPage.SecondaryCauseDropdownListId());

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.SecondaryCauseDropdownListId(),
                testData.TestParameters.get("Secondary Cause"))) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.ClaimsHandlerDropdownListId(),
                testData.TestParameters.get("Claims Handler"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(CreateNewClaimPage.ClaimDescriptionTextBoxId(),
                testData.TestParameters.get("Claim Description"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Claim overview details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterPremiumDetails() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.PremiumConfirmerDropdownListId(),
                testData.TestParameters.get("Premium Confirmer"))) {
            return false;
        }

        JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;

        js.executeScript("document.getElementById('" + CreateNewClaimPage.ClaimNextButtonId() + "').click();");

        SeleniumDriverInstance.takeScreenShot("Premium details entered successfully", false);

        SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

        SeleniumDriverInstance.pause(1000);
        SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId());

        //Liabilty Details
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(CreateNewClaimPage.TertiaryCauseDropdownListId(),
                testData.TestParameters.get("Tertiary Cause"))) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId())) {
            return false;
        }

        //Claim Documents
        if (!(SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId()))) {
            return false;
        }

        //Accident/Theft details
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId())) {
            return false;
        }

        //FAP
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId())) {
            return false;
        }

        //Payee check
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId())) {
            return false;
        }

        //Aggregates
        verifySumInsuredIsZero();
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimNextButtonId())) {
            return false;
        }

        //Reserves
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.PublicLiabilityGeneneralLinkId())) {
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementById(CreateNewClaimPage.PublicLiabilityReserveGridId())) {
            return false;
        }
        if (!SeleniumDriverInstance.enterTextById(CreateNewClaimPage.ClaimReserverOwnDamageNewValueTextBoxId(), testData.getData("Own Damage Amount"))) {
            return false;
        }

        //clicking away
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.PublicLiabilityReserveGridId())) {
            return false;
        }

        //finish
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ReserverFinishButtonId())) {
            return false;
        }

        //verify
        //finish again
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ReserverFinishButtonId())) {
            return false;
        }

        //submit
        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.PerilsSubmitButtonId())) {
            return false;
        }

        //ok
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.clickElementById(CreateNewClaimPage.ClaimReinsuranceOkButtonId()))
          {
            return false;
          }

        return true;
    }

    private boolean verifySumInsuredIsZero() {
        if (!SeleniumDriverInstance.validateElementTextValueById(CreateNewClaimPage.AgreegateTotalInsuredTextboxId(), "0.00")) {
            return false;
        }
        return true;
    }

    private boolean verifyExclusiveSumInsured() {

//        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
//
//        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
//            return false;
//        }
//        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduPublicLiabilityRiskPage.PublicLiabilityReinsuranceBandddlId2(), "EBP - Public Liability")) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }
//        SeleniumDriverInstance.pause(1000);
//        if (!SeleniumDriverInstance.validateElementTextValueById(EkunduPublicLiabilityRiskPage.SumInsuredTextBoxId(), "10,000,000.00")) {
//            return false;
//        }
        //ToDo Verify This Reserve Column

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId2())) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);
//        if (!SeleniumDriverInstance.clickElementbyLinkText("Risks")) {
//            return false;
//        }
//        SeleniumDriverInstance.pause(1000);
        return true;
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
