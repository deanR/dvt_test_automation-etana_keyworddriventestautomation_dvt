
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author ferdinandN
 */
public class EkunduEPLPersonalAccidentTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResult;

    public EkunduEPLPersonalAccidentTest(TestEntity testData)
      {
        this.testData = testData;

      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!selectPersonalAccidentRiskType())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to select Personal Accident Risk"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to select Personal Accident Risk- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterBeneficiaryDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter beneficiary details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter beneficiary details- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterIndividualDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter individual details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter individual details- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterEndorsements())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk  endorsements"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk endorsements- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!enterNotes())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk notes"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk notes- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!addPropFAC())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter FAC Prop Placemet data"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter FAC Prop Placemet data- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Personal Accident risk added successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Personal Accident risk added successfully",
                              this.getTotalExecutionTime());

      }

    private boolean verifyRisksDialogisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            selectPersonalAccidentRiskType();

            return true;
          }

        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectPersonalAccidentRiskType();

            return true;

          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

      }

     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
    
    private boolean selectPersonalAccidentRiskType()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"));

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Personal Accident Risk type selected successfully", false);

        return true;
      }

    private boolean enterBeneficiaryDetails()
      {
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddPersonalAccidentRiskButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddBeneficiaryButtonName()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BeneficiaryNameTextBoxId(),
                testData.TestParameters.get("Beneficiary Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BeneficiarySurnameTextBoxId(),
                testData.TestParameters.get("Beneficiary Surname")))
          {
            return false;
          }

        SeleniumDriverInstance.clearTextById(EkunduCreateNewPolicyForNewClientPage.BeneficiaryDOBTextBoxId());

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BeneficiaryDOBTextBoxId(),
                testData.TestParameters.get("Beneficiary DOB")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BeneficiaryIDTypeRequiredDropdownListId(),
                testData.TestParameters.get("Beneficiary ID Type Required")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BeneficiaryPercentageTextBoxId(),
                testData.TestParameters.get("Beneficiary Percentage")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Beneficiary details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BeneficiaryNextButtonId()))
          {
            return false;
          }

        return true;

      }

    private boolean enterIndividualDetails()
      {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.OccupationDropdownListId(),
                testData.TestParameters.get("Occupation")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SumInsuredDeathTextBoxId(),
                testData.TestParameters.get("Sum Insured Death")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalAccidentRateButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Individual details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalAccidentNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterEndorsements()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementsSelectButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.SelectClauseDropdownListId(),
                testData.TestParameters.get("Clause Selection")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddEndorsementClauseButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ClauseApplyButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterNotes()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalAccidentNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.PersonalAccidentNotesTextBoxId(),
                testData.TestParameters.get("Risk Notes")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PersonalAccidentNextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        return true;
      }

    private boolean addPropFAC()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(),
                testData.TestParameters.get("Reinsurance Details - FAC Placement Reinsurer Code1")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
          {
            return false;
          }
        
        SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.clearTextAndEnterValueById("ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl03_txtSumInsured",
                testData.TestParameters.get("Reinsurance Details - Africare RI Sum Insured")))
          {
            return false;
          }
        
         SeleniumDriverInstance.pause(5000);

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Prop FAC Placement completed successfully", false);

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;

      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
