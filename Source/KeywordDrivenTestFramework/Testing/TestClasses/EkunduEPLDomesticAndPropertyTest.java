/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import static KeywordDrivenTestFramework.Entities.Enums.BrowserType.Chrome;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPageNew;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPageNew;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduGoodsInTransitRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduGoodsInTransitRiskPageNew;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPageNew;

/**
 *
 * @author ferdinandN
 */
public class EkunduEPLDomesticAndPropertyTest extends BaseClass
{
     TestEntity testData;
    TestResult testResultt;
    
    public EkunduEPLDomesticAndPropertyTest(TestEntity testData)
    {
        this.testData = testData;
    }  
    
    public TestResult executeTest()
    {
        this.setStartTime();
        
      if(!selectDomesticandPropertyRiskType())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to Select Domestic and Property"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL , "Failed to Select Domestic and Property - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
        
      if(!addSurveyType())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to enter survey type"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter survey type - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      } 
      
      if(!enterRiskCoverage())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to enter risk coverage"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter risk coverage - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      } 
      
      if(!enterBuildingsDetails())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to enter building details"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter building details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      } 
      
      if(!BuildingsInterestedParties())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to enter Buildings interested parties"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Buildings interested parties - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      } 
      
      if(!enterContents())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to enter contents"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter contents - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
      
      if(!ContentsInterestedParties())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to enter Contents interested parties"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Contents interested parties - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
      
      if(!AllRiskItems())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to enter All Risk Details"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter All Risk Details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
      if(!AllRiskItems2())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to enter All Risk 2 Details"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter All Risk 2 Details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
      
      if(!enterEndorsements())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to enter endorsements"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter endorsements- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
      
      if(!enterFACPropPlacementData())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter FAC Prop Placement data", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter FAC Prop Placement data - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
      
      if(!enterFACXOLPlacementData())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter FAC XOL Placement Data", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter FAC XOL Placement Data - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
      
        SeleniumDriverInstance.takeScreenShot(("Domestic and Property risk added successfully"), false);
        return new TestResult(testData,Enums.ResultStatus.PASS, "Domestic and Property risk added successfully", this.getTotalExecutionTime());  
  
    }
     
    private boolean selectDomesticandPropertyRiskType() 
    {
//        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
//        {
//            return false;
//        }
      
        if(!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"));
       
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return false;
        }
            SeleniumDriverInstance.pause(2000);
            SeleniumDriverInstance.takeScreenShot( "Domestic and Property Risk type selected successfully", false);
            return true;
    }
    
   
    private boolean addSurveyType()
     {
         //this.clickAddRiskItemButton();
         
         if(browserType == Chrome)
         {
            SeleniumDriverInstance.Driver.navigate().refresh();
         
            SeleniumDriverInstance.pause(3000);
         }
         
         else
         {
            if(!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddSurveyButtonName()))
            {
                return false;
            }

            if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.SelectSurveyTypeDropdownListId(), testData.TestParameters.get("Survey Type")))
            {
                return false;
            }

                SeleniumDriverInstance.clearTextById(EkunduCreateNewPolicyForNewClientPage.SurveyDateTextBoxId());

            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SurveyDateTextBoxId(), testData.TestParameters.get("Survey Date")))
            {
                return false;
            }

            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SurveyReferenceTextBoxId(), testData.TestParameters.get("Survey Reference")))
            {
                return false;
            }

            if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyNextButtonId()))
            {
                return false;
            }

            if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SurveyReportCommentsTextAreaId(), testData.TestParameters.get("Comments")))
            {
                return false;
            }

                SeleniumDriverInstance.takeScreenShot("Survey type entered successfully", false);

            if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyNextButtonId()))
            {
                return false;
            }

            if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyNextButtonId()))
            {
                return false;
            }
        
         }
            return true;
    }
    
    private boolean enterRiskCoverage()
     {
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.CheckBuildingCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.CheckContentsCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.CheckAllRiskItemsCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TypeofResidenceDropdownListId(), testData.TestParameters.get("Type of Residence")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TypeofConstructionDropdownListId(), testData.TestParameters.get("Type of Construction")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.OccupancyDropdownListId(), testData.TestParameters.get("Occupancy")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.LocalityDropdownListId(), testData.TestParameters.get("Locality")))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Risk coverage entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.RiskCoverageNextButtonName()))
        {
            return false;
        } 
            return true;
    }
    
    private boolean enterBuildingsDetails()
     {
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BuildingSumInsuredTextBoxId(), testData.TestParameters.get("Building Sum Insured")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AdjustmentPercentageTextBoxId(), testData.TestParameters.get("Adjustment Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPageNew.AdjustmentReasonDropdownListId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPageNew.AdjustmentReasonDropdownListId(), testData.TestParameters.get("Adjustment Reason")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
        {
            return false;
        }
        
         SeleniumDriverInstance.takeScreenShot("Building details entered successfully", false);
        return true;
    }
    
    private boolean BuildingsInterestedParties()
     {
         //SeleniumDriverInstance.Driver.navigate().refresh();
         
         SeleniumDriverInstance.pause(3000);
         
        if(!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddInterestedPartiesButtonName()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TypeofAgreementTypeDropdownListId(), testData.TestParameters.get("Type of Agreement")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.InstitutionNameDropdownListId(), testData.TestParameters.get("Institution Name")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BuildingsInterestedPartiesCommentsTextAreaId(), testData.TestParameters.get("Interested Party Comments")))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Interested parties details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
        {
            return false;
        }
            return true;
        
    }
    
    private boolean enterContents()
     {
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ContentsSumInsuredTextBoxId(), testData.TestParameters.get("Contents Sum Insured")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NoClaimDiscountDropdownListId(), testData.TestParameters.get("Claim Discount")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.SecurityDiscountsLevel5AlarmCheckBoxId(), true))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ContentsAdjustmentPercentageTextBoxId(), testData.TestParameters.get("Contents Adjustment Percentage")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.ContentsAdjustmentReasonDropdownListId(), testData.TestParameters.get("Adjustment Reason")))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Contents entered successfully", false);
            
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
        {
            return false;
        }
        
            return true;
        
    }
    
    private boolean ContentsInterestedParties()
     {
         //SeleniumDriverInstance.Driver.navigate().refresh();
         
         SeleniumDriverInstance.pause(3000);
        if(!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPageNew.ContentsAddInterestedPartiesButtonName()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPageNew.TypeofAgreement2DropdownListId(), testData.TestParameters.get("Type of Agreement 2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPageNew.InstitutionName2DropdownListId(), testData.TestParameters.get("Institution Name 2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPageNew.InterestedPartiesComments2TextAreaId(), testData.TestParameters.get("Interested Party Comments 2")))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("Interested parties details entered successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
        {
            return false;
        }
            return true;
        
    }
    
    private boolean AllRiskItems()
     {
       //  SeleniumDriverInstance.Driver.navigate().refresh();
         
         SeleniumDriverInstance.pause(3000);
         
        if(!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPageNew.AddAllRiskButtonName()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.DAPRiskItemCategoryDropdownListId(), this.getData("Risk Item - Category")))
        {
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.DAPRiskItemDescriptionTestboxId(), this.getData("Risk Item - Description")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.DAPRiskItemDescriptionTestboxId(), this.getData("Serial/IMEI")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.DAPRiskItemSumInsuredTestboxId(), this.getData("Risk Item - Sum Insured")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPageNew.AllRiskAdjPercentageTextBoxId(), this.getData("All Risk Adj Percentage")))
        {
            return false;
        }
         
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPageNew.AllRiskAdjustmentReasonDropdownListId()))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPageNew.AllRiskAdjustmentReasonDropdownListId(), this.getData("All Risk Reason")))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("All Risk details entered successfully", false);
            
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
        {
            return false;
        }
        
            return true;
        
    }
    
    private boolean AllRiskItems2()
     {
       //  SeleniumDriverInstance.Driver.navigate().refresh();
         
         SeleniumDriverInstance.pause(3000);
        if(!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPageNew.AddAllRisk2ButtonName()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.DAPRiskItemCategoryDropdownListId(), this.getData("Risk Item - Category 2")))
        {
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.DAPRiskItemDescriptionTestboxId(), this.getData("Risk Item - Description 2")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.DAPRiskItemSumInsuredTestboxId(), this.getData("Risk Item - Sum Insured 2")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPageNew.AllRiskAdjPercentageTextBoxId(), this.getData("All Risk Adj Percentage 2")))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPageNew.AllRiskAdjustmentReasonDropdownListId()))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPageNew.AllRiskAdjustmentReasonDropdownListId(), this.getData("All Risk Reason 2")))
        {
            return false;
        }
        
            SeleniumDriverInstance.takeScreenShot("All Risk 2 details entered successfully", false);
            
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.RateButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesNextButtonId()))
        {
            return false;
        }
        
        
            return true;
        
    }
    
    private boolean enterEndorsements()
     {
         
         if(browserType == Chrome)
         {
           // SeleniumDriverInstance.Driver.navigate().refresh();

            SeleniumDriverInstance.pause(3000);
         }
         
         else
         {
         
         
            if(!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddEndorsementButtonName()))
           {
               return false;
           }

           if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.EndorsementSectionsDropdownListId(), testData.TestParameters.get("Endorsement Sections")))
           {
               return false;
           }

           if(!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EndorsementsSelectButtonId()))
           {
               return false;
           }

            if(!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPageNew.DAPEndorsementsAddAllButtonId()))
           {
               return false;
           }

            if(!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPageNew.DAPEndorsementsApplyButtonId()))
           {
               return false;
           }

               SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

           if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementNextButtonId()))
           {
               return false;
           }

           if(!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EndorsementNotesTextBoxId(), testData.TestParameters.get("Endorsement Notes")))
           {
               return false;
           }

           if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementNextButtonId()))
           {
               return false;
           }

           if(!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementNextButtonId()))
           {
               return false;
           }

               SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

           if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("ctl00_cntMainBody_ReInsurance2007Cntrl_ddlReinsurance", "EPL - Personal Property"))
           {
               return false;
           }
           
         }
            
            SeleniumDriverInstance.pause(10000);
        
            return true;
        
    }
    
    private boolean enterFACPropPlacementData()
       {
         if(browserType == Chrome)
         {
            SeleniumDriverInstance.Driver.navigate().refresh();
         
            SeleniumDriverInstance.pause(3000);
         }
         
         else
         {
             
          
           
            if(! SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
            {
                return false;
            }

            SeleniumDriverInstance.pause(5000);

              if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), this.getData("Reinsurance Details - FAC Placement Reinsurer Code 1")))
              {
                  return false;
              }

              if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
              {
                  return false;
              }

              if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
              {
                  return false;
              }

              SeleniumDriverInstance.pause(5000);

                 //  SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId());

               SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());


              if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.EveresteReinsurancePercTextBoxId(), this.getData("Reinsurance Details - FAC Placement - ACE Percentage")))
              {
                  return false;
              }

                 SeleniumDriverInstance.pause(8000);

               if(!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
              {
                  return false;
              }

               SeleniumDriverInstance.pause(8000);

               System.out.println("Company 1 added successfully");


              if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AddProcFACButtonId()))
              {
                  return false;
              }

              SeleniumDriverInstance.pause(5000);

              if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), this.getData("Reinsurance Details - FAC Placement Reinsurer Code 2")))
              {
                  return false;
              }

              if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
              {
                  return false;
              }

              if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
              {
                  return false;
              }

              SeleniumDriverInstance.pause(8000);

           //SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.HannoverreReinsurancePercTextBoxId());

              if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.HannoverreReinsurancePercTextBoxId(), this.getData("Reinsurance Details - FAC Placement - ADDISON  Percentage")))
              {
                  return false;
              }

                 SeleniumDriverInstance.pause(8000);

                   if(!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain"))
              {
                  return false;
              }    
                   
         }

                SeleniumDriverInstance.pause(10000);
                
                System.out.println("Company 2 added successfully");
                                
           return true;
       }
    
    private boolean enterFACXOLPlacementData()
    {
        if(!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.AddFACXOLButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), this.getData("Reinsurer Code")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId());
        
         if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), this.getData("Reinsurer Code2")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
        {
            return false;
        }
        
       // SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLLowerLimitId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLLowerLimitId(), this.getData("FAP LOX Lower Limit")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        
        //SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId(), this.getData("FAP LOX Upper Limit")))
        {
            return false;
        }
        
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        //SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId());
        
        SeleniumDriverInstance.pause(5000);
        
     
        
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId(), this.getData("FAP LOX Participation Percentage1")))
        {
            return false;
        }
        
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
         
       // SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId(), this.getData("FAP LOX Premium1")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPageNew.FACXOLParticipationPerc2TextboxId(), this.getData("FAP LOX Participation Percentage2")))
        {
            return false;
        }
       
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
       
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPageNew.FACXOLPremium2TextboxId(), this.getData("FAP LOX Premium2")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
         
//        WebElement participationPerc2Textbox = SeleniumDriverInstance.Driver.findElement(By.id(EkunduGoodsInTransitRiskPage.FACXOLCaisseParticipationPercTextboxId()));
//        String participationPerc2 = participationPerc2Textbox.getAttribute("value");
//        
//        System.out.println("Participation percentage 2 retrieved - " + participationPerc2 );
//         
//        WebElement participationPercTextbox = SeleniumDriverInstance.Driver.findElement(By.id(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId()));
//        String participationPerc = participationPercTextbox.getAttribute("value");
//        
//        System.out.println("Participation percentage 1 retrieved - " + participationPerc );
         
//         do
//         {
//             if (participationPerc.equals("0.0000"))
//             {
//                SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId(), this.getData("FAP LOX Participation Percentage"));
//                SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
//                participationPerc = participationPercTextbox.getAttribute("value");
//                SeleniumDriverInstance.pause(5000);
//                
//             
//             }
//             else if(participationPerc2.equals("0.0000"))
//             {
//                 SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLCaisseParticipationPercTextboxId(), this.getData("FAP LOX Participation Percentage 2"));
//                 SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
//                 participationPerc2 = participationPerc2Textbox.getAttribute("value");
//                SeleniumDriverInstance.pause(5000);
//                 
//             }
//         }
//             
//         while (participationPerc.equals("0.0000") ||  participationPerc2.equals("0.0000"));
        
      
        
         SeleniumDriverInstance.pause(5000);
        
        
        SeleniumDriverInstance.takeScreenShot("FAC Placement 1 completed successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.FACXOLOkButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.AddFACXOLButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.FACReinsurerCodeTextBoxId(), this.getData("Reinsurer Code3")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSearchReinsurerButtonId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FACSelectReinsurerLinkId()))
        {
            return false;
        }
        
        SeleniumDriverInstance.pause(5000);
        
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPageNew.FACSelectReinsurer2LinkId()))
        {
            return false;
        }
        
       // SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLLowerLimitId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLLowerLimitId(), this.getData("FAP LOX Lower Limit2")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        
        //SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId());
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLUpperLimitId(), this.getData("FAP LOX Upper Limit2")))
        {
            return false;
        }
        
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
        //SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId());
        
        SeleniumDriverInstance.pause(5000);
        
     
        
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLParticipationPercTextboxId(), this.getData("FAP LOX Participation Percentage3")))
        {
            return false;
        }
        
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
         
       // SeleniumDriverInstance.clearTextById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId());
         SeleniumDriverInstance.pause(5000);
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPage.FACXOLPremiumTextboxId(), this.getData("FAP LOX Premium3")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
         
          SeleniumDriverInstance.pause(5000);
        
        
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPageNew.FACXOLParticipationPerc2TextboxId(), this.getData("FAP LOX Participation Percentage3")))
        {
            return false;
        }
       
        SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
         SeleniumDriverInstance.pause(5000);
       
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduGoodsInTransitRiskPageNew.FACXOLPremium2TextboxId(), this.getData("FAP LOX Premium3")))
        {
            return false;
        }
        
         SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_lblFindReinsurer");
        
      
        
         SeleniumDriverInstance.pause(5000);
         
         SeleniumDriverInstance.takeScreenShot("FAC Placement 2 completed successfully", false);
        
        if(!SeleniumDriverInstance.clickElementById(EkunduGoodsInTransitRiskPage.FACXOLOkButtonId()))
        {
            return false;
        }
        
         SeleniumDriverInstance.pause(5000);
        
        
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId()))
        {
            return false;
        }
        
         SeleniumDriverInstance.pause(10000);
        
        return  true;
   
    }
    
   public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
}
