
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCombinedLiabilityRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduElectronicEquipmentRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMoneyRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduCombinedLiabilityRiskTest extends BaseClass {

    TestEntity testData;
    TestResult testResult;

    public EkunduCombinedLiabilityRiskTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk button is present", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify that the add risk button is present - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterIndustryDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Industry details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Industry details - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterRiskDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter risk details - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterPublicLiabilityDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Public Liability details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Public Liability details - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterProductsLiabilityDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Products Liability details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Products Liability details - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterExtensions()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Extensions", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Extensions - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterRatingDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Rating details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Rating details - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterEndorsements()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Endorsements", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Endorsements - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterNotes()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Notes", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Notes - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        SeleniumDriverInstance.takeScreenShot("Combined Liability risk entered successfully", false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Combined Liability risk entered successfully",
                this.getTotalExecutionTime());
    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean verifyAddRiskButtonisPresent() {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectStatedBenefitsRisk();

            return true;
        } else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            selectStatedBenefitsRisk();
        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err
                    .println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");

            return false;
        }

        return true;
    }

    private boolean selectStatedBenefitsRisk() {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.pause(10000);
        SeleniumDriverInstance.takeScreenShot("Stated Benefits risk selected sucessfully", false);

        return true;
    }

    private boolean enterIndustryDetails() {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(), this.getData("Industry"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(
                EKunduCreateCorporateClientPage.IndustrySearchResultsSelectedRowId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Industry details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.RiskNextButtonId())) {
            return false;
        }

        return true;

    }

    private boolean enterRiskDetails() {
        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduCombinedLiabilityRiskPage.RiskProductLiabilityCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduCombinedLiabilityRiskPage.RiskQuestionnaireReceivedCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduCombinedLiabilityRiskPage.RiskBasisofCoverDropdownListId(),
                this.getData("Risk - Basis of Cover"))) {
            return false;
        }

        // SeleniumDriverInstance.clearTextAndEnterValueById(EkunduCombinedLiabilityRiskPage.RiskBasisofCoverRetroActiveDateTextboxId(), this.getData("Basis of Cover Retro Active Date"));
        if (!SeleniumDriverInstance.enterTextById(EkunduCombinedLiabilityRiskPage.RiskTurnoverTextboxId(),
                this.getData("Risk - Turnover"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduCombinedLiabilityRiskPage.RiskLoadingsRetroActivOccurrenceTextboxId(),
                this.getData("Loadings & Discounts - Retro Active"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCombinedLiabilityRiskPage.RiskLoadingsRevenueTextboxId(),
                this.getData("Loadings & Discounts"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCombinedLiabilityRiskPage.RiskLoadingsDeductibleTextboxId(),
                this.getData("Loadings & Discounts"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduCombinedLiabilityRiskPage.RiskLoadingsClaimsExperienceTextboxId(),
                this.getData("Loadings & Discounts"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCombinedLiabilityRiskPage.RiskLoadingsUWDiscretionTextboxId(),
                this.getData("Loadings & Discounts"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.RiskFAPSectionCheckboxId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Risk details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.RiskNextButtonId())) {
            return false;
        }

        return true;

    }

    private boolean enterPublicLiabilityDetails() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduCombinedLiabilityRiskPage.PublicLiabilityLimitofIndemnityDropdownListId(),
                this.getData("Products Liability - Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.RiskRateButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(
                EkunduCombinedLiabilityRiskPage.PublicLiabilityFAPSectionCheckboxId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Public Liability details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.RiskNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterProductsLiabilityDetails() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(
                EkunduCombinedLiabilityRiskPage.ProductsLiabilityLimitofIndemnityDropdownListId(),
                this.getData("Products Liability - Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.RiskRateButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Products Liability details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.RiskNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterExtensions() {
        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduCombinedLiabilityRiskPage.ExtensionsAdvertisingLiabilityCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduCombinedLiabilityRiskPage.ExtensionsAdvertisingLiabilityLimitofIndemnityTextboxId(),
                this.getData("Extensions - Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduCombinedLiabilityRiskPage.ExtensionsAdvertisingLiabilityRateTextboxId(),
                this.getData("Extensions - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduCombinedLiabilityRiskPage.ExtensionsAdvertisingLiabilityFAPPercentageTextboxId(),
                this.getData("Extensions - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduCombinedLiabilityRiskPage.ExtensionsAdvertisingLiabilityFAPAmountTextboxId(),
                this.getData("Extensions - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduCombinedLiabilityRiskPage.ExtensionsClaimsPreparationCostsCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduCombinedLiabilityRiskPage.ExtensionsClaimsPreparationCostsLimitofIndemnityTextboxId(),
                this.getData("Extensions - Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduCombinedLiabilityRiskPage.ExtensionsClaimsPreparationCostsRateTextboxId(),
                this.getData("Extensions - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduCombinedLiabilityRiskPage.ExtensionsClaimsPreparationCostsFAPPercentageTextboxId(),
                this.getData("Extensions - FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduCombinedLiabilityRiskPage.ExtensionsClaimsPreparationCostsFAPAmountTextboxId(),
                this.getData("Extensions - FAP Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduCombinedLiabilityRiskPage.ExtensionsContractorLiabilityCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduCombinedLiabilityRiskPage.ExtensionsLegalDefenceCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduCombinedLiabilityRiskPage.ExtensionsLegalDefenseRateTextboxId(),
                this.getData("Extensions - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduCombinedLiabilityRiskPage.ExtensionsLegalDefensePremiumTextboxId(),
                this.getData("Extensions - Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(
                EkunduCombinedLiabilityRiskPage.ExtensionsWrongfulArrestCheckboxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduCombinedLiabilityRiskPage.ExtensionsWrongfulArrestRateTextboxId(),
                this.getData("Extensions - Rate"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(
                EkunduCombinedLiabilityRiskPage.ExtensionsWrongfulArrestPremiumTextboxId(),
                this.getData("Extensions - Premium"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Extensions entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.RiskNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterRatingDetails() {
        if (!SeleniumDriverInstance.enterTextById(
                EkunduCombinedLiabilityRiskPage.RatingReinsuranceLimitofLiabilitysTextboxId(),
                this.getData("Ratings - Reinsurance Limit of Liability"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Rating details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.RiskNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterEndorsements() {
        if (!SeleniumDriverInstance.clickElementById(EkunduMoneyRiskPage.EndorsementsSelectButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.EndorsementsAddAllButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.EndorsementsApplyButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.RiskNextButtonId())) {
            return false;
        }

        return true;

    }

    private boolean enterNotes() {
        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskCommentsTextboxId(),
                this.getData("Risk Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduElectronicEquipmentRiskPage.RiskItemAddNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduBusinessAllRiskPage.BusinessAllRiskPolicyNotesTextboxId(),
                this.getData("Risk Notes"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Notes entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance
                .clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Documents");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementById("btnGeneratePdf");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementById("popup_ok");
//         
//         
         SeleniumDriverInstance.pause(1000);
//         
        SeleniumDriverInstance.clickElementbyLinkText("Risks");

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
