/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBroadFormLiabilityPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCombinedLiabilityRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author deanR
 */
public class EkunduBroadFormLiabilityRiskTest extends BaseClass
{
    TestEntity testData;
    TestResult testResultt;
   
    
    public EkunduBroadFormLiabilityRiskTest(TestEntity testData)
    {
        this.testData = testData;
    }    
    
     public TestResult executeTest()
    {  
        SeleniumDriverInstance.DriverExceptionDetail = "";
        
        this.setStartTime();
        
        if(!verifyRisksDialogisPresent())
        {
           SeleniumDriverInstance.takeScreenShot("Failed to verify that the risks dialog is present", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to verify that the risks dialog is present - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterIndustryDetails())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter industry details", true );
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter industry details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if(!enterBroadFormDetails())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Broad Form liability details", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Broad Form liability details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterPublicLiabilityDetails())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Public Liability details", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Public Liability details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterProductsLiabilityDetails())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Products Liability details", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Products Liability details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterPollutionLiabilityDetails())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Pollution Liability details", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Pollution Liability details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterProductDefectiveLiabilityDetails())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Product and Defective Workmanship Liability details", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Product and Defective Workmanship Liability details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterNegligentAdvice())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Negligent Advice", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Negligent Advice - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterExtensions())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Extensions", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Extensions - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterRatingDetails())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Rating details", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Rating details - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
         if(!enterEndorsements())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Endorsements", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Endorsements - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        if(!enterNotes())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Notes", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Notes - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        
        return new TestResult(testData, Enums.ResultStatus.PASS, "Broad Form Liability risk details entered successfully", this.getTotalExecutionTime());
    }
    
     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
     
     private boolean verifyRisksDialogisPresent()
    {       
            if(SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId()))
            {
                    SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
                    selectBroadFormLiabilityRiskType();
                    return true;

            }
            else if(SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
                {
                    selectBroadFormLiabilityRiskType();
                    return true;
                }
            
            else
                {
                    SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
                    System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");
                    return false;
                }
    }
     
     private boolean selectBroadFormLiabilityRiskType() 
    {
        if(!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return false;
        }
            SeleniumDriverInstance.pause(1000);
            SeleniumDriverInstance.takeScreenShot( "Broad Form Liability Risk type selected successfully", false);
            return true;
    }

     private boolean enterIndustryDetails()
    {
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId()))
        {
            return false;
        }
    
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(), this.getData("Industry")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId()))
        {
            return false;
        }
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.IndustrySearchRowId()))
        {
            return false;
        }
            SeleniumDriverInstance.pause(2000);

            SeleniumDriverInstance.takeScreenShot("Industry details entered successfully", false);
            
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
        
            return true;  
     }
    
     private boolean enterBroadFormDetails()
    {
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduBroadFormLiabilityPage.CoverTypePublicProductLiabilityCheckboxId(), true))
    {
        return false;
    }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduBroadFormLiabilityPage.CoverTypePollutionLiabilityCheckboxId(), true))
    {
        return false;
    }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduBroadFormLiabilityPage.CoverTypeProductDefectiveWorkmanshipCheckboxId(), true))
    {
        return false;
    }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduBroadFormLiabilityPage.CoverTypeNegligentAdviceCheckboxId(), true))
    {
        return false;
    }
        
        if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduBroadFormLiabilityPage.RiskBasisofCoverDropdownListId(), this.getData("Risk - Basis of Cover")))
    {
        return false;
    }
        
        if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduBroadFormLiabilityPage.RiskBasisofCoverRetroActiveDateTextboxId(), this.getData("Basis of Cover Retro Active Date")))
    {
        return false;
    }
    
        if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.RiskTurnoverTextboxId(), this.getData("Risk - Turnover")))
    {
        return false;
    }
    
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
    {
        return false;
    }

            SeleniumDriverInstance.takeScreenShot("Broad Form Liability details entered successfully", false);
            
            return true;  
     }
    
     private boolean enterPublicLiabilityDetails()
    {
    if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.LimitOfIndemnityTextBoxId(), this.getData("Limit of indemnity")))
    {
        return false;
    }
    
    if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.PublicLiabilityPremiumTextBoxId(), this.getData("Public Liability - Premium")))
    {
        return false;
    }
    
     if(!SeleniumDriverInstance.clickElementById(EkunduBroadFormLiabilityPage.PublicLiabilityFAPSectionCheckboxId()))
    {
        return false;
    }
     
        SeleniumDriverInstance.takeScreenShot("Public Liability details entered successfully", false);
        
    if(!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.RiskNextButtonId()))
    {
        return false;
    }
    
    return true;
}

     private boolean enterProductsLiabilityDetails()
    {
    if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.ProductsLimitOfIndemnityTextBoxId(), this.getData("Products - Limit of indemnity")))
    {
        return false;
    }
    
    if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.ProductsPublicLiabilityPremiumTextBoxId(), this.getData("Products - Public Liability - Premium")))
    {
        return false;
    }
    
     if(!SeleniumDriverInstance.clickElementById(EkunduBroadFormLiabilityPage.ProductsPublicLiabilityFAPSectionCheckboxId()))
    {
        return false;
    }
     
        SeleniumDriverInstance.takeScreenShot("Products Liability details entered successfully", false);
        
    if(!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.RiskNextButtonId()))
    {
        return false;
    }
    
    return true;
}
     
     private boolean enterPollutionLiabilityDetails()
    {
    if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.PollutionLimitOfIndemnityTextBoxId(), this.getData("Pollution - Limit of indemnity")))
    {
        return false;
    }
    
    if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.PollutionPublicLiabilityPremiumTextBoxId(), this.getData("pollution - Public Liability - Premium")))
    {
        return false;
    }
    
     if(!SeleniumDriverInstance.clickElementById(EkunduBroadFormLiabilityPage.PollutionPublicLiabilityFAPSectionCheckboxId()))
    {
        return false;
    }
     
        SeleniumDriverInstance.takeScreenShot("Pollution Liability details entered successfully", false);
        
    if(!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.RiskNextButtonId()))
    {
        return false;
    }
    
    return true;
}
     
     private boolean enterProductDefectiveLiabilityDetails()
    {
    if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.DefectiveLimitOfIndemnityTextBoxId(), this.getData("Defective Products - Limit of indemnity")))
    {
        return false;
    }
    
    if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.DefectivePublicLiabilityPremiumTextBoxId(), this.getData("Defective Products - Public Liability - Premium")))
    {
        return false;
    }
    
    if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.DefectiveWorkmanshipLimitOfIndemnityTextBoxId(), this.getData("Workmanship - Limit of indemnity")))
    {
        return false;
    }
    
    if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.DefectiveorkmanshipPublicLiabilityPremiumTextBoxId(), this.getData("Workmanship - Public Liability - Premium")))
    {
        return false;
    }
    
     if(!SeleniumDriverInstance.clickElementById(EkunduBroadFormLiabilityPage.DefectivePublicLiabilityFAPSectionCheckboxId()))
    {
        return false;
    }
     
        SeleniumDriverInstance.takeScreenShot("Product and Defective Workmanship Liability details entered successfully", false);
        
    if(!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.RiskNextButtonId()))
    {
        return false;
    }
    
    return true;
}
     
     private boolean enterNegligentAdvice()
    {
    if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.NegligentLimitOfIndemnityTextBoxId(), this.getData("Negligent - Limit of indemnity")))
    {
        return false;
    }
    
    if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.NegligentPublicLiabilityPremiumTextBoxId(), this.getData("Negligent - Public Liability - Premium")))
    {
        return false;
    }
    
     if(!SeleniumDriverInstance.clickElementById(EkunduBroadFormLiabilityPage.NegligentPublicLiabilityFAPSectionCheckboxId()))
    {
        return false;
    }
     
        SeleniumDriverInstance.takeScreenShot("Negligent Advice details entered successfully", false);
        
    if(!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.RiskNextButtonId()))
    {
        return false;
    }
    
    return true;
}

     private boolean enterExtensions()
    {
    if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduBroadFormLiabilityPage.ExtensionPublicLiabilityFAPSectionCheckboxId(), true))
    {
        return false;
    }
    
    if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.ExtensionLimitOfIndemnityTextBoxId(), this.getData("Extensions - Limit of Indemnity")))
    {
        return false;
    }
    
    if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.ExtensionPublicLiabilityPremiumTextBoxId(), this.getData("Extensions - premium")))
    {
        return false;
    }
    
    if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduBroadFormLiabilityPage.ExtensionClaimsPublicLiabilityFAPSectionCheckboxId(), true))
    {
        return false;
    }
    
    if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.ExtensionClaimsLimitOfIndemnityTextBoxId(), this.getData("Extensions Claims - Limit of Indemnity")))
    {
        return false;
    }
    
    if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.ExtensionClaimsPublicLiabilityPremiumTextBoxId(), this.getData("Extensions Claims - Premium")))
    {
        return false;
    }
    
    if(!SeleniumDriverInstance.uncheckCheckBoxSelectionById(EkunduBroadFormLiabilityPage.ExtensionGuestPublicLiabilityFAPSectionCheckboxId(), true))
    {
        return false;
    }
    
    if(!SeleniumDriverInstance.uncheckCheckBoxSelectionById(EkunduBroadFormLiabilityPage.ExtensionLegalDefencePublicLiabilityFAPSectionCheckboxId(), true))
    {
        return false;
    }
    
    if(!SeleniumDriverInstance.uncheckCheckBoxSelectionById(EkunduBroadFormLiabilityPage.ExtensionWrongfulArrestPublicLiabilityFAPSectionCheckboxId(), true))
    {
        return false;
    }
    
        SeleniumDriverInstance.takeScreenShot("Extensions entered successfully", false);
        
    if(!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.RiskNextButtonId()))
    {
        return false;
    }
    
    return true;
}

     private boolean enterRatingDetails()
    {
    if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.ReinsuranceLimitOfLiabilityTextBoxId(), this.getData("Ratings - Reinsurance Limit of Liability")))
    {
        return false;
    }
        SeleniumDriverInstance.takeScreenShot("Rating details entered successfully", false);
        
    if(!SeleniumDriverInstance.clickElementById(EkunduCombinedLiabilityRiskPage.RiskNextButtonId()))
    {
        return false;
    }
    return true;
}
    
     private boolean enterEndorsements()
    {
         if(!SeleniumDriverInstance.clickElementById(EkunduBroadFormLiabilityPage.EndorsementsSelectButtonId()))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduBroadFormLiabilityPage.EndorsementsAddAllButtonId()))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduBroadFormLiabilityPage.EndorsementsApplyButtonId()))
        {
            return false;
        }
         
            SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);
         
         if(!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.BusinessAllRiskNextButtonId()))
        {
            return false;
        }
         
         return true;
         
         
     }
    
     private boolean enterNotes()
    { 
        if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.RiskCommentsTextBoxId(), this.getData("EHAT Specified Notes")))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduBroadFormLiabilityPage.NotesCheckboxId(), true))
         {
             return false;
         }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduBroadFormLiabilityPage.RiskNotesTextBoxId2(), this.getData("EHAT Specified Notes 2")))
         {
             return false;
         }
            
            SeleniumDriverInstance.takeScreenShot("Risk notes entered successfully", false);
        
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
         {
             return false;
         }
       
            SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
            SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
            
        return true;
              
    }
     
}
