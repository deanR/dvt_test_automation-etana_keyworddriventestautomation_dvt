
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduPersonalClientEnrouteRiskTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduPersonalClientEnrouteRiskTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!verifyRisksDialogisPresent())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the risks dialog is present"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the risks dialog is present- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterEnrouteRiskDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter Enroute risk details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to nter Enroute risk details- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterRiskNotes())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk notes"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk notes- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Enroute risk added successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Enroute risk added successfully", this.getTotalExecutionTime());

      }

    private boolean verifyRisksDialogisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            selectEnrouteRiskType();

            return true;
          }

        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectEnrouteRiskType();

            return true;

          }
        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

      }

     public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }

    private boolean selectEnrouteRiskType()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Enroute Risk type selected successfully", false);

        return true;
      }

    private boolean enterEnrouteRiskDetails()
      {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.PlanTypeDropdownListId(),
                testData.TestParameters.get("Plan Type")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EnrouteNumberofPersonsTextBoxId(),
                testData.TestParameters.get("No of Persons")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TempDisabilityWeeksDropdownListId(),
                testData.TestParameters.get("Temp Disablitiy Weeks")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EnrouteRegNumberTextBoxId(),
                testData.TestParameters.get("Registration Number")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EnrouteVINChassisNumberTextBoxId(),
                testData.TestParameters.get("VIN/Chassis Number")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk details entered sucessfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EnrouteNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterRiskNotes()
      {
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EnrouteNextButtonId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EnrouteCommentsTextAreaId(),
                testData.TestParameters.get("Enroute Comments")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Risk notes entered sucessfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EnrouteNextButtonId()))
          {
            return false;
          }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());
        
//        SeleniumDriverInstance.pause(3000);
//        
//        SeleniumDriverInstance.clickElementbyLinkText("Documents");
//        
//        SeleniumDriverInstance.pause(3000);
//        
//         SeleniumDriverInstance.clickElementById("btnGeneratePdf");
//         
//         SeleniumDriverInstance.pause(3000);
//         
//         SeleniumDriverInstance.clickElementById("popup_ok");
//         
//         
//         SeleniumDriverInstance.pause(3000);
//         
//         SeleniumDriverInstance.clickElementbyLinkText("Risks");

        return true;

      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
