
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduGlassRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorFleetRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMotorSpecifiedRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduTheftRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author FerdinandN
 */
public class EkunduTheftRiskTest extends BaseClass {

    TestEntity testData;
    TestResult testResultt;

    public EkunduTheftRiskTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!verifyAddRiskButtonisPresent()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the add risk button is present ", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify that the add risk button is present - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterIndustryDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter industry details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter industry details - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterRiskItemDetails()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk item details", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter risk item details - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterEndorsements()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter endorsements", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter endorsements - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!enterComments()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter risk comments", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter risk comments - " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        return new TestResult(testData, Enums.ResultStatus.PASS,
                "Theft risk details entered successfully" + SeleniumDriverInstance.DriverExceptionDetail,
                this.getTotalExecutionTime());

    }

    private boolean verifyAddRiskButtonisPresent() {
        if (SeleniumDriverInstance.waitForElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduMotorSpecifiedRiskPage.AddRiskButtonId());
            selectTheftRiskType();

            return true;
        } else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            selectTheftRiskType();
        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Add Risk Button is present", true);
            System.err.println("[Error] Failed to verify that the Add Risk Button is present, exception detected - Fault - ");

            return false;
        }

        return true;
    }

    private boolean selectTheftRiskType() {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.pause(10000);
        SeleniumDriverInstance.takeScreenShot("Theft risk selected sucessfully", false);

        return true;
    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    private boolean enterIndustryDetails() {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustryButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.SearchIndustryTextBoxId(),
                this.getData("Risk Item Industry"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchIndustrySpanId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.IndustrySearchResultsSelectedRowId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduTheftRiskPage.MaliciousDamageIndustrialConstructionDropdownListId(),
                this.getData("Malicious Damage Industrial Construction"))) {
            return false;
        }

        JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;

        js.executeScript("processIndConsLogic();");

        // SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Industry details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduMotorFleetRiskPage.MotorFleetNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterRiskItemDetails() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduTheftRiskPage.RiskItemAlarmWarrantyDropdownListId(),
                this.getData("Alarm Warranty"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduTheftRiskPage.RiskItemAllContentsCheckboxId(), true)) {
            return false;
        }

//        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduTheftRiskPage.RiskItemFlatPremiumCheckboxId(), true))
//          {
//            return false;
//          }
        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.RiskItemAllContentsSumInsuredTextboxId(),
                this.getData("All Contents Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.RiskItemAllContentsPremiumTextboxId(),
                this.getData("All Contents Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.RiskItemAllContentsFAPPercentageTextboxId(),
                this.getData("All Contents FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.RiskItemAllContentsFAPMinAmountTextboxId(),
                this.getData("All Contents FAP Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.RiskItemAllContentsFAPMaxAmountTextboxId(),
                this.getData("All Contents FAP Max Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduTheftRiskPage.RiskItemIncreasedBuildingsLimitCheckboxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.RiskItemIncreasedBuildingsLimitSumInsuredTextboxId(),
                this.getData("All Contents Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.RiskItemIncreasedBuildingsLimitPremiumTextboxId(),
                this.getData("All Contents Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.RiskItemIncreasedBuildingsLimitFAPPercentageTextboxId(),
                this.getData("All Contents FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.RiskItemIncreasedBuildingsLimitFAPMinAmountTextboxId(),
                this.getData("All Contents FAP Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.RiskItemIncreasedBuildingsLimitFAPMaxAmountTextboxId(),
                this.getData("All Contents FAP Max Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduTheftRiskPage.RiskItemIMaliciousDamageCheckboxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.RiskItemMaliciousDamageSumInsuredTextboxId(),
                this.getData("All Contents Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.RiskItemMaliciousDamagePremiumTextboxId(),
                this.getData("All Contents Premium"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.RiskItemMaliciousDamageFAPPercentageTextboxId(),
                this.getData("All Contents FAP Percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.RiskItemMaliciousDamageFAPMinAmountTextboxId(),
                this.getData("All Contents FAP Min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.RiskItemMaliciousDamageFAPMaxAmountTextboxId(),
                this.getData("All Contents FAP Max Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduTheftRiskPage.ExtensionsAdditionalClaimsPrepCheckboxId(),
                true)) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.ExtensionsAdditionalClaimsPrepLimitofIndemnityTextboxId(),
                this.getData("Extensions Additional Claims Limit of Indemnity"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.ExtensionsAdditionalClaimsPrepRateTextboxId(),
                this.getData("Extensions Additional Claims Rate"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Risk Item details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduTheftRiskPage.TheftNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterEndorsements() {
        if (!SeleniumDriverInstance.clickElementById(EkunduTheftRiskPage.TheftNextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduGlassRiskPage.EndorsementsSelectButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduTheftRiskPage.EndorsementsAddAllButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduTheftRiskPage.EndorsementsApplyButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduTheftRiskPage.TheftNextButtonId())) {
            return false;
        }

        return true;
    }

    private boolean enterComments() {
        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.RiskCommentsTextboxId(),
                this.getData("Risk Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduTheftRiskPage.AddPolicyNotesLabelId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduTheftRiskPage.PolicyNotesTextboxId(),
                this.getData("Risk Notes"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduTheftRiskPage.TheftNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Documents");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementById("btnGeneratePdf");

//         SeleniumDriverInstance.pause(3000);
//         
        SeleniumDriverInstance.clickElementById("popup_ok");

        SeleniumDriverInstance.pause(1000);

        SeleniumDriverInstance.clickElementbyLinkText("Risks");

        return true;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
