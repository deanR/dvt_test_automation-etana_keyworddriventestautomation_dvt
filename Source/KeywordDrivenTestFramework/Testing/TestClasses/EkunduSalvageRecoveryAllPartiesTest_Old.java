/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduMaintainCorporateClientClaim1Page;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author deanR
 */
public class EkunduSalvageRecoveryAllPartiesTest_Old extends BaseClass
{
    TestEntity testData;
    TestResult testResultt;
    
     public EkunduSalvageRecoveryAllPartiesTest_Old(TestEntity testData)
    {
        this.testData = testData;
    }
    
     public TestResult executeTest()
    {  
         SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();
        
         if(!startAgentClaimSalvage())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to initiate Agent claim Salvage"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to initiate Agent claim Salvage- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
         
         if(!startClientClaimSalvage())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to initiate Client claim Salvage"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to initiate Client claim Salvage- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
         
         if(!startClaimReceivableClaimSalvage())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to initiate Claim Receivable claim Salvage"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to initiate Claim Receivable claim Salvage- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
         
         if(!startInsurerClaimSalvage())
      {
          SeleniumDriverInstance.takeScreenShot(("Failed to initiate Insurer claim Salvage"), true);
          return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to initiate Insurer claim Salvage- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
      }
         
        return new TestResult(testData, Enums.ResultStatus.PASS, "Salvage recovery against all parties completed successfully", this.getTotalExecutionTime());
     }
    
    private boolean startAgentClaimSalvage()
    {
        String claimref = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                              "Retrieved Corporate Claim Number");

        if (claimref.equals("parameter not found"))
          {
            claimref = testData.TestParameters.get("Claim Reference Number");
          }
        else
          {
            testData.updateParameter("Claim Reference Number", claimref);
          }
        
        if (!SeleniumDriverInstance.Payclaim(claimref, "Salvage"))
          {
            return false;
          }
        
//       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SalvageClaimLinkId()))
//       {
//           return false;
//       } 
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilsSalvageLinkId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
       
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.AgentRadioButtonId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
        
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.EditPartyLinkId()))
       {
           return false;
       }
       //**********************************************************************************************************
       if(!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            return false;
        }
       
       if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.PaymentDetailsRecievedAmountTextboxId(), testData.TestParameters.get("Agent Recieved Amount")))
         {
             return false;
         }
       
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.PaymentDetailsTaxGroupDropDownListId(), testData.TestParameters.get("Tax Group")))
         {
             return false;
         }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PaymentDetailsOkButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return false;
        }
       
       //***********************************************************************************************************
       SeleniumDriverInstance.pause(2000);
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ThisReceiptTabLinkText());
       
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MediaRefDropDownListId(), testData.TestParameters.get("Media Ref")))
         {
             return false;
         }
       
       SeleniumDriverInstance.pause(2000);
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilDetailsNextButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilDetailsNextButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMaintainCorporateClientClaim1Page.ClaimReasonforChangeDropdowlistId(), this.getData("Reason")))
       {
           return false;
       } 
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimOkButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimReinsuranceOkButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ReturnToCaseButtonId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.pause(2000);
       
       SeleniumDriverInstance.takeScreenShot( "Agent Salvage Recovery completed successfully", false);
       
       return true;
    }
    
    private boolean startClientClaimSalvage()
    {
        String claimref = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                              "Retrieved Corporate Claim Number");

        if (claimref.equals("parameter not found"))
          {
            claimref = testData.TestParameters.get("Claim Reference Number");
          }
        else
          {
            testData.updateParameter("Claim Reference Number", claimref);
          }
        
        if (!SeleniumDriverInstance.Payclaim(claimref, "Salvage"))
          {
            return false;
          }
        
//       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SalvageClaimLinkId()))
//       {
//           return false;
//       } 
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilsSalvageLinkId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
       
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ClientRadioButtonId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PartyButtonId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.EditPartyLinkId()))
       {
           return false;
       }
       //**********************************************************************************************************
       if(!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            return false;
        }
       
       if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.PaymentDetailsRecievedAmountTextboxId(), testData.TestParameters.get("Client Recieved Amount")))
         {
             return false;
         }
       
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.PaymentDetailsTaxGroupDropDownListId(), testData.TestParameters.get("Tax Group")))
         {
             return false;
         }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PaymentDetailsOkButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return false;
        }
       
       //***********************************************************************************************************
       SeleniumDriverInstance.pause(2000);
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ThisReceiptTabLinkText());
       
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MediaRefDropDownListId(), testData.TestParameters.get("Media Ref")))
         {
             return false;
         }
       
       SeleniumDriverInstance.pause(2000);
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilDetailsNextButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilDetailsNextButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMaintainCorporateClientClaim1Page.ClaimReasonforChangeDropdowlistId(), this.getData("Reason")))
       {
           return false;
       } 
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimOkButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimReinsuranceOkButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ReturnToCaseButtonId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.pause(2000);
       
       SeleniumDriverInstance.takeScreenShot( "Client Salvage Recovery completed successfully", false);
       
       return true;
    }
    
    private boolean startClaimReceivableClaimSalvage()
    {
        String claimref = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                              "Retrieved Corporate Claim Number");

        if (claimref.equals("parameter not found"))
          {
            claimref = testData.TestParameters.get("Claim Reference Number");
          }
        else
          {
            testData.updateParameter("Claim Reference Number", claimref);
          }
        
        if (!SeleniumDriverInstance.Payclaim(claimref, "Salvage"))
          {
            return false;
          }
        
//       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SalvageClaimLinkId()))
//       {
//           return false;
//       } 
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilsSalvageLinkId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
       
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ClaimRecievableRadioButtonId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.EditPartyLinkId()))
       {
           return false;
       }
       //**********************************************************************************************************
       if(!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            return false;
        }
       
       if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.PaymentDetailsRecievedAmountTextboxId(), testData.TestParameters.get("Claim Recieved Amount")))
         {
             return false;
         }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PaymentDetailsOkButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return false;
        }
       
       //***********************************************************************************************************
       SeleniumDriverInstance.pause(2000);
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ThisReceiptTabLinkText());
       
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MediaRefDropDownListId(), testData.TestParameters.get("Media Ref")))
         {
             return false;
         }
       
       SeleniumDriverInstance.pause(2000);
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilDetailsNextButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilDetailsNextButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMaintainCorporateClientClaim1Page.ClaimReasonforChangeDropdowlistId(), this.getData("Reason")))
       {
           return false;
       } 
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimOkButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimReinsuranceOkButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ReturnToCaseButtonId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.pause(2000);
       
       SeleniumDriverInstance.takeScreenShot( "Claim Recievable Salvage Recovery completed successfully", false);
       
       return true;
    }
    
    private boolean startInsurerClaimSalvage()
    {
        String claimref = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                              "Retrieved Corporate Claim Number");

        if (claimref.equals("parameter not found"))
          {
            claimref = testData.TestParameters.get("Claim Reference Number");
          }
        else
          {
            testData.updateParameter("Claim Reference Number", claimref);
          }
        
        if (!SeleniumDriverInstance.Payclaim(claimref, "Salvage"))
          {
            return false;
          }
        
//       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SalvageClaimLinkId()))
//       {
//           return false;
//       } 
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilsSalvageLinkId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
       
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.InsurerRadioButtonId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PartyButtonId()))
       {
           return false;
       }
       
       //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
       if(!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.ReinsurerSearchReinsurerCodeTextBoxId(), this.getData("Reinsurer Code")))
        {
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.SearchPartyCodeButtonId()))
       {
           return false;
       }
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.SelectPartyLinkText());
        
 
        if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return false;
        }
       //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
        
        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReceiptDetailsTabPartialLinkText());
        
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.EditPartyLinkId()))
       {
           return false;
       }
       //**********************************************************************************************************
       if(!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
        {
            return false;
        }
       
       if(!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.PaymentDetailsRecievedAmountTextboxId(), testData.TestParameters.get("Insurer Recieved Amount")))
         {
             return false;
         }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PaymentDetailsOkButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.switchToDefaultContent())
        {
            return false;
        }
       
       //***********************************************************************************************************
       SeleniumDriverInstance.pause(2000);
       
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ThisReceiptTabLinkText());
       
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MediaRefDropDownListId(), testData.TestParameters.get("Media Ref")))
         {
             return false;
         }
       
       SeleniumDriverInstance.pause(2000);
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilDetailsNextButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PerilDetailsNextButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduMaintainCorporateClientClaim1Page.ClaimReasonforChangeDropdowlistId(), this.getData("Reason")))
       {
           return false;
       } 
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimOkButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ClaimReinsuranceOkButtonId()))
       {
           return false;
       }
       
       if(!SeleniumDriverInstance.clickElementById(EkunduMaintainCorporateClientClaim1Page.ReturnToCaseButtonId()))
       {
           return false;
       }
       
       SeleniumDriverInstance.pause(2000);
       
       SeleniumDriverInstance.takeScreenShot( "Insurer Salvage Recovery completed successfully", false);
       
       return true;
    }
   
    public String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
}
