
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

import org.openqa.selenium.JavascriptExecutor;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author fnell
 */
public class EkunduMTAExtendExpiryDateTest extends BaseClass {

    JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;
    TestEntity testData;
    TestResult testResult;

    public EkunduMTAExtendExpiryDateTest(TestEntity testData) {
        this.testData = testData;

    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToFindClientPage()) {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find client page", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to navigate to the find client page - "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!findClient()) {
            SeleniumDriverInstance.takeScreenShot("Failed to find client", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!verifyClientDetailsPageHasLoaded()) {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the client details page has loaded", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to verify that the client details page has loaded- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterMidTermAdjustmentData()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Mid Term Adjustment data", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to enter Mid Term Adjustment data- "
                    + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!extendExpiryDate()) {
            SeleniumDriverInstance.takeScreenShot("Failed to Extend Expiry Date", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to Extend Expiry Date- " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        if (!quotePolicyRisks()) {
            SeleniumDriverInstance.takeScreenShot("Failed to quote policy risks", true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                    "Failed to quote policy risks- " + SeleniumDriverInstance.DriverExceptionDetail,
                    this.getTotalExecutionTime());
        }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Extend expiry date completed successfully",
                this.getTotalExecutionTime());
    }

    private boolean navigateToFindClientPage() {
        if (!SeleniumDriverInstance.navigateToFindClientPage(EkunduViewClientDetailsPage.ClientHoverTabId(), "Find Client")) {
            return false;
        }

        return true;
    }

    private boolean findClient() {
        String clientCode
                = SeleniumDriverInstance.retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"),
                        "Retrieved Client Code");

        if (clientCode.equals("parameter not found")) {
            clientCode = testData.TestParameters.get("Client Code");
        } else {
            testData.updateParameter("Client Code", clientCode);
        }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode)) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId())) {
            return false;
        }

        return true;
    }

    private boolean verifyClientDetailsPageHasLoaded() {
        if (!SeleniumDriverInstance.validateElementTextValueById(EkunduViewClientDetailsPage.ViewClientDetailsLabelId(),
                "View Client Details")) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Client Details page successfully loaded", false);

        return true;
    }

    private boolean enterMidTermAdjustmentData() {
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText())) {
            return false;
        }

        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                "Retrieved Policy Number");

        if (policyNumber.equals("parameter not found")) {
            policyNumber = testData.TestParameters.get("Policy Number");
        } else {
            testData.updateParameter("Policy Number", policyNumber);
        }

//         if(!SeleniumDriverInstance.changePolicy(policyNumber, "Change"))
//        {
//            return false;
//        }
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ChangePolicyLinkText())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MTATypeofChangeDropDownListId(),
                this.getData("Type of Change"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MIAEffectiveDateTextBoxId(),
                this.getData("Effective Date"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId())) {
            return false;
        }

        return true;
    }

    private boolean extendExpiryDate() {
        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.PolicyHeaderTabId())) {
            return false;
        }

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.CancelMTAButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clearTextById(EkunduViewClientDetailsPage.CoverEndDateTextBoxId());

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.CoverEndDateTextBoxId(),
                this.getData("Cover End Date"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("End Date changed successfully", false);

        return true;
    }

    private boolean quotePolicyRisks() {
        System.out.println("[Info] Quoting Business All risk");

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.BusinessAllRiskActionDropdownListXpath(),
                "Edit")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Business All Risk Quoted successfully", false);

        System.out.println("[Info] Quoting Grouped Fire risk");

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.GroupedFireRiskActionDropdownListXpath(),
                "Edit")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(3000);

        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Grouped Fire Risk Quoted successfully", false);

        SeleniumDriverInstance.pause(5000);

        System.out.println("[Info] Quoting Electronic Equipment risk");

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.ElectronicEquipmentRiskActionDropdownListXpath(),
                "Edit")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Electronic Equipment Risk Quoted successfully", false);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.GoodsinTransitActionDropdownListXpath(),
                "Edit")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        System.out.println("[Info] Quoting Grouped Fire risk 2");

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.GroupedFireRiskEditActionDropdownListXpath(),
                "Edit")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        System.out.println("[Info] Quoting Grouped Fire risk ");

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.GroupedFireRisk5DeleteActionDropdownListXpath(),
                "Edit")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId())) {
            return false;
        }

        System.out.println("[Info] Quoting Money risk");

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.MoneyRiskActionDropdownListXpath(), "Edit")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.FinishButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyPartialLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Policy Risks quoted successfully", false);

        return true;

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
