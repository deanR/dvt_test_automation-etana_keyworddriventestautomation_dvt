
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

/**
 *
 * @author FerdinandN
 */
public class EkunduBuildingsAndContentSectionDetailsSASRIATest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduBuildingsAndContentSectionDetailsSASRIATest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!verifyRisksDialogisPresent())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the risks dialod is presenet"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to select risk type - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!EnterDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to do Buildings and Contents Section Details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to do Buildings and Contents Section Details - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        if (!ReinsuranceDetailsTab())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter Reinsurance Details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter Reinsurance Details - " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Buildings and Contents Section Details risk added successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Buildings and Contents Section Details risk added successfully",
                              this.getTotalExecutionTime());

      }

    private boolean verifyRisksDialogisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            BuildingsandContentsSectionDetailsRisk();

            return true;

          }
        else if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());

            BuildingsandContentsSectionDetailsRisk();

            return true;
          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

        
      }
    
    private boolean BuildingsandContentsSectionDetailsRisk()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectRiskType(testData.TestParameters.get("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Buildings and Contents Section Details Risk type selected successfully", false);
        
        return true;
      }

    private boolean EnterDetails()
      {

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyNextButtonId()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyNextButtonId()))
          {
            return false;
          }
        
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.buildingsSectionsuminsuredDropdownListId(),
                testData.TestParameters.get("Sum Insured")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean ReinsuranceDetailsTab()
      {

        SeleniumDriverInstance.clickElementbyLinkText(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabLinkText());
        
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

         return true;
      }

      
    
    private String getData(String parameterName)
      {
        if (testData.TestParameters.containsKey(parameterName))
          {
            return testData.TestParameters.get(parameterName);
          }
        else
          {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
          }
      }
  }



