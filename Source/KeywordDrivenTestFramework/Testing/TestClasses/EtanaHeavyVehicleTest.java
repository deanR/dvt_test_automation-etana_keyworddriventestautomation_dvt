package KeywordDrivenTestFramework.Testing.TestClasses;

//~--- non-JDK imports --------------------------------------------------------
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPageNew;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;

public class EtanaHeavyVehicleTest extends BaseClass {

    TestEntity testData;
    TestResult testResultt;

    public EtanaHeavyVehicleTest(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() 
    {
        this.setStartTime();

        //if (testData.TestMethod.toUpperCase().trim().contains("EBP-Heavy Vehicle".toUpperCase().trim())) {
        if (!verifyRisksDialogisPresent()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to select Motor type."), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to select Motor type. - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        SeleniumDriverInstance.pause(2000);

        if (!vehicleSelection()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to complete vehicle selection section."), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to complete vehicle selection section. - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        SeleniumDriverInstance.pause(2000);

        if (!Options()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to Check all Checkboxes in the Options section."), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Check all Checkboxes in the Options section. - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        SeleniumDriverInstance.pause(2000);

//            if (!verifyingClaimsHistory()) {
//                SeleniumDriverInstance.takeScreenShot(("Failed to Select No Claim rebate."), true);
//                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Select No Claim rebate.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
//            }
//            SeleniumDriverInstance.pause(2000);
        if (!vehicleCoverDetails()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to Enter and verify Vehicle cover details."), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Enter and verify Vehicle cover details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        SeleniumDriverInstance.pause(2000);

        if (!registrationDetails()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to Successfully enter registration details."), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully enter registration details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        SeleniumDriverInstance.pause(2000);
//
        if (!motorCover()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to Successfully enter and verify Motor cover details."), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully enter and verify Motor cover details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        SeleniumDriverInstance.pause(2000);

        if (!accesoriesAndEquipments()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to Successfully capture accessories and equipment details."), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully capture accessories and equipment details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        SeleniumDriverInstance.pause(2000);

        if (!extentions()) {
            SeleniumDriverInstance.takeScreenShot(("Failed to Successfully capture extention details."), true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully capture extention details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }
        SeleniumDriverInstance.pause(2000);

            if (!voluntaryExcess()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully capture Voluntary Excess details."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully capture Voluntary Excess details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);

            if (!additionalCompulsoryExcess()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully capture Additinal Voluntary Excess details."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully capture Additinal Voluntary Excess details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);

            if (!firstAmountPayableSection()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully capture First Amount Payable section details."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully capture First Amount Payable section details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);

            if (!endosements()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully complete the endosements section details."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully complete the endosements section details.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);
            
            if (!specifiedDriverDetails()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully complete Specified Driver details section."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully complete Specified Driver details section.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);

            if (!interestedParty()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully complete Interested party details section."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully complete Interested party details section.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);
            
            if (!notes()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully complete notes details section."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully complete notes details section.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);
            
            if (!notes2()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully complete notes2 details section."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully complete notes2 details section.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);
            
            if (!reinsuranceDetails()) {
                SeleniumDriverInstance.takeScreenShot(("Failed to Successfully complete Reinsurance details section."), true);
                return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to Successfully complete reninsurance details section.- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
            }
            SeleniumDriverInstance.pause(2000);
         //}
        return new TestResult(testData, Enums.ResultStatus.PASS, "Add special types was successful.", this.getTotalExecutionTime());
    }

    private String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");
            return "";
        }
    }

    /**
     * ******Clicking add Risk Button and selecting the risk type And Verifying
     * that the Risk type dialog is present*****
     */
    private boolean verifyRisksDialogisPresent() {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId())) {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            selectMotorRiskType();
            return true;

        } else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            selectMotorRiskType();
            return true;

        } else {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");
            return false;

        }

    }

    //secting risk type from Dialog
    private boolean selectMotorRiskType() {
        SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId())) {
            return false;
        }

        if (!SeleniumDriverInstance.selectRiskType(this.getData("Risk Name"))) {
            return false;
        }

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        //next button
        if (!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPageNew.BusinessAllRiskNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Motor risk type selected successfully", false);
        return true;
    }

    //******EBP - Motor Specified********//
    //Vehicles section
    private boolean vehicleSelection() {

        //next button
//        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId())) {
//            return false;
//        }
        //Model search Icon
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SearchVehicleModelIconId())) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);

        //Source
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("source",
                testData.TestParameters.get("Source"))) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        //Select the Make of the vehicle from the dropdown menu.
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("make", this.getData("Make"))) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        //Select the Year from the dropdown menu.
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("year", this.getData("Year"))) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        //Select the Model of the vehicle form the dropdown menu.
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId("model", this.getData("Model"))) {
            return false;
        }

        //Select on the vehicle details to populate it in the Vehicle Selection section.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.VehicleModelSelectedRowId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Vehicle details were catured successfully.", true);
        return true;
    }

    //Options
    private boolean Options() {

        //Endorsement CheckBox
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementsCheckBoxId())) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        //Specified Drivers CheckBox
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriversCheckBoxId())) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        //Interested CheckBox
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.InterestedPartiesCheckBoxId())) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Ticked all the checkboxes under Options section.", false);

       //Verifying claim History
        //Next Button
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BasicDetailsNextButtonName())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Claim history verified.", false);

        return true;
    }

    //Claims History and Underwriting Criteria
    private boolean verifyingClaimsHistory() {

//        //Claims in Last Months Textbox
//        if (!SeleniumDriverInstance.clickElementById("")) {
//            return false;
//        }
//
//        //Number of Vehicle on policy
//        if (!SeleniumDriverInstance.clickElementById("")) {
//            return false;
//        }
//
//        //Interested Textbox
//        if (!SeleniumDriverInstance.clickElementById("")) {
//            return false;
//        }
        //No Claim Rebate Drop down 
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.ClaimRebateDropdownListId(),
                testData.TestParameters.get("No Claim Rebate"))) {
            return false;
        }

        //Next Button
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_btnNext")) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Selected No Claim Rebate.", false);
        return true;
    }

    private boolean vehicleCoverDetails() {
        //Area code text box
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AreaCodeTextboxId(), testData.TestParameters.get("Area Code"))) {
            return false;
        }

        //Select Tracking Device from the dropdown menu
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorTrackingDeviceDropdownListId(), this.getData("Tracking Device"))) {
            return false;
        }
        SeleniumDriverInstance.takeScreenShot("Vehicale Cover Details were entered and verified successfull.", false);
        return true;
    }

    private boolean registrationDetails() {
        //Engine Number text box
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.EngineNumberTextboxId(), testData.TestParameters.get("Engine Number"))) {
            return false;
        }

        //Registration Number details
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VehicleRegNumberTextBoxId(), testData.TestParameters.get("Registration Number"))) {
            return false;
        }

        //Chasis number textbox
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ChassisNumberTextboxId(), testData.TestParameters.get("Chassis Number"))) {
            return false;
        }

        //Registered owner 
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegOwnerTextBoxId(),
                testData.TestParameters.get("Registered Owner"))) {
            return false;
        }

        //original registration date
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.RegistrationDateTextboxId(), this.getData("Original Registration Date"))) {
            return false;
        }

        //NATIS Code
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.NATISCodeDropdownListId(),
                testData.TestParameters.get("NATIS code"))) {
            return false;
        }
        SeleniumDriverInstance.takeScreenShot("Registration details captured successfully.", false);
        return true;
    }

    private boolean motorCover() {
        //Verify that the M&M Retail Price is generated by default on the relevant textbox.
        //Verify that the M&M Trail Price is generated by default on the relevant textbox.
        //Verify that the Vehicle Sum Insured s generated by default on the relevant textbox.
        //Select the Cover Type from the dropdown menu.
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.CoverTypeDropdownListId(),
                testData.TestParameters.get("Cover Type"))) {
            return false;
        }

        //Select Basis of Settlement from the dropdown menu
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.BasisofSettlementDropdownListId(),
                testData.TestParameters.get("Basis of Settlement"))) {
            return false;
        }

        //Select Trade Class from the dropdown menu
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.TradeClassDropdownListId(),
                testData.TestParameters.get("Trade Class"))) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Motor cover details entered and verified successfully.", false);
        //clicking add button
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddEquipmentButtonName())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Clicked Add button successfully.", false);
        return true;
    }

    private boolean accesoriesAndEquipments() {

        //enter description in the textbox
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessoryDescriptionTextBoxId(), this.getData("Description"))) {
            return false;
        }

        //enter sum insured in the textbox
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AccessorySumInsuredTextBoxId(), this.getData("Sum Insured"))) {
            return false;
        }

        //clicking Finish Button
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AccessoryFinishButtonID())) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Accessories and equipment details captured successfully.", false);
        return true;
    }

    private boolean verifyAccessoryItems() {

        return true;
    }

    //Extentions
    private boolean extentions()
    {

        //<editor-fold defaultstate="collapsed" desc="Contingent liability">
//********************************************Contingent liability****************************************************************************************//
        //Verify that the Commercial Windscreen checkbox is greyed out and it already ticked.
        //Verify that the FAP% is generated by default on the relevant textbox.
        //Verify that the FAP Amount is generated by default on the relevant textbox.
        //Tick on Contingent liability checkbox.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ExtensionsContingentLiabilityCheckBoxId())) {
            return false;
        }

        //Capture Limit of Indemnity on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsContingentLiabilityLimitofIndemnityTextboxId(),
                this.getData("Contingent Limit of Indemnity"))) {
            return false;
        }

        //Capture Rate on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsContingentLiabilityRateTextboxId(), this.getData("Contingent Rate"))) {
            return false;
        }
        //Verify that the Premium is being generated on the relevant textbox.
        //Click Rate button.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId())) {
            return false;
        }
        //Verify that the Post Premium is being generated on the relevant textbox.
        //</editor-fold>

        
        //<editor-fold defaultstate="collapsed" desc="Credit Shortfall">
//********************************************Credit Shortfall****************************************************************************************//
        //Tick on Credit Shortfall checkbox.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ExtensionsCreditShortallCheckBoxId())) {
            return false;
        }

        //Capture Rate on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsCreditShortfallRateTextboxId(),
                this.getData("Credit Shortfall Rate"))) {
            return false;
        }

        //Click on Rate button.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId())) {
            return false;
        }

        //Verify that the Posting Premium is being populated on the relevant textbox
        //</editor-fold>
        
        //<editor-fold defaultstate="collapsed" desc="Excess waiver">
//********************************************excess waiver****************************************************************************************// 
        //ticking excess waiver checkbox
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ExtensionsExcessWaiverCheckBoxId())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        //Capturing Premium in the premium textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsExcessWaiverPremiumTextboxId(), this.getData("Premium"))) {
            return false;
        }

        //Clicking Rate button
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId())) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        //Verify that the Posting Premium is being generated on the relevant textbox.
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Fire & Explosion">
//********************************************Fire and Explosion****************************************************************************************//
        //Verify that the Fire & Explosion checkbox is greyed out and it already ticked.
        //Verify that the Limit of Indemnity is populated by default on the relevant textbox.
        //Capture Rate on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsFireAndexplosionRateTextboxId(), 
                this.getData("Fire and Explosion Rate"))) {
            return false;
        }

        //Capture Premium on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsFireAndexplosionPremiumTextboxId(), 
                this.getData("Fire and Explosion Premium"))) {
            return false;
        }
        //Click Rate button
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId())) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Loss of Key">
        //********************************************Loss of Key****************************************************************************************//
        //Verify that the Posting Premium is generated on the relevant textbox.
        //Tick on Loss of Key checkbox.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ExtensionsLossofKeysCheckBoxId())) {
            return false;
        }

        //Capture Limit of Indemnity on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsLossofKeysLimitofIndemnityTextboxId(), this.getData("Loss of Key Limit of Indemnity"))) {
            return false;
        }
        //Capture Premium on the relevant textbox
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsLossofKeysPremiumTextboxId(), 
                this.getData("Loss of Key Premium"))) {
            return false;
        }
        //Click Rate button.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId())) {
            return false;
        }

        //Verify that the Posting Premium is being generated on the relevant textbox.
        //Capture FAP Amount on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.LossOfKeyFAPAmount(), this.getData("Loss of Key FAP Amount"))) {
            return false;
        }
        // </editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Parking Facility">
        //********************************************Parking Facility****************************************************************************************//
        //Tick on Parking Facility checkbox.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ExtensionsParkingFacilitiesCheckBoxId())) {
            return false;
        }
        //Capture Limit of Indemnity on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsParkingFacilitiesLimitofIndemnityTextboxId(), this.getData("Parking Facility Limit of Indemnity"))) {
            return false;
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Passenger Liability">
        //********************************************Passenger Liability****************************************************************************************//
        //Tick on Passenger Liability checkbox.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.PercengerLiabilityCheckBoxId())) {
            return false;
        }

        //Select Limit of Indemnity from the dropdown menu.
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.PercengerLiabilityDropDownID(),
                testData.TestParameters.get("Passenger Liability Limit of Indemnity"))) {
            return false;
        }

        //Click Rate button.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId())) {
            return false;
        }

        //Verify that the Posting Premium is being generated on the relevant textbox.
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Riot & Strike">
        //********************************************Riot & Strike****************************************************************************************//
        //Tick on Riot & Strike checkbox.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ExtensionsRiotandStrikeCheckBoxId())) {
            return false;
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Theft of Car Radio">
        //********************************************Theft of Car Radio****************************************************************************************//
        //Tick on Theft of Car Radio checkbox.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ExtensionsTheftofCarRadioCheckBoxId())) {
            return false;
        }
        //Verify that the Limit of Indemnity is generated by default on the relevant textbox.
        //Capture Rate on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsTheftofCarRadioRateTextboxId(), 
                this.getData("Theft of Car Radio Rate"))) {
            return false;
        }
        //Click Rate button.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId())) {
            return false;
        }
        //Verify that the FAP Amount is being generated on the relevant textbox.
        //Verify that Third Party Liability checkbox is greyed out and it already ticked.
        //Verify that Limit of Indemnity is already generated by default on the relevant textbox.
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Third Party Liability">
        //********************************************Third Party Liability****************************************************************************************//
        //Capture Rate on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsThirdPartyLiabilityRateTextboxId(), this.getData("Third Party Liability Rate"))) {
            return false;
        }
        //Capture Premium on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ThirdPartyLiabilityPremiumMotorChangesTextBoxId(), this.getData("Third Party Liability premium"))) {
            return false;
        }

        //Click Rate button.
       if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId())) {
            return false;
        }
        //Verify that the Posting Premium is being generated on the relevant.
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Unauthorized Passenger Liability">
        //********************************************Unauthorized Passenger Liability****************************************************************************************//
        //Tick on Unauthorized Passenger Liability checkbox.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.UnAthorisedPersengerLiabilityChecBoxID())) {
            return false;
        }
        //Select Limit of Indemnity from the dropdown menu.
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.UnAuthorisedPersengerLiabilityDropDownID(),
                testData.TestParameters.get("Unauthorized Passenger Liability Limit of Indemnity"))) {
            return false;
        }
        //Click Rate button
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId())) {
            return false;
        }
        //Verify that the Posting Premium is being generated on the relevant textbox.
        //</editor-fold>
        
        //<editor-fold defaultstate="collapsed" desc="Wreckage and Removal">
        //********************************************Wreckage and Removal****************************************************************************************//
        //Tick on the Wreckage and Removal checkbox.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ExtensionsWreckageRemovalCheckBoxId())) {
            return false;
        }
        //Capture Limit of Indemnity on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsWreckageRemovalLimitofIndemnityTextboxId(),
                testData.TestParameters.get("Wreckage and Removal Limit of Indemnity"))) {
            return false;
        }
        //Capture Rate on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.ExtensionsWreckageRemovalRateTextboxId(),
                testData.TestParameters.get("Wreckage and Removal Rate"))) {
            return false;
        }
        //Verify that the premium is being generated on the relevant textbox.
        //Click Rate button.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyRateButtonId())) {
            return false;
        }
        //</editor-fold>
        //Verify that the Posting Premium is being generated on the relevant textbox.
        
        
        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.takeScreenShot("Extension details captured successfully.", false);

        return true;
    }

    private boolean verifyingPostingPremium() {
        //posting premium textbox value verification
        if (!SeleniumDriverInstance.clickElementById("")) {
            return false;
        }

        //Verifying that Third Party Liability checkbox is already ticked and is greyed out.
        if (!SeleniumDriverInstance.clickElementById("")) {
            return false;
        }

        //Verifying limit of indeminity is auto generated
        if (!SeleniumDriverInstance.clickElementById("")) {
            return false;
        }

        //Capture rate
        if (!SeleniumDriverInstance.clickElementById("")) {
            return false;
        }

        //Verifying that the Premium is generated
        //clicking rate button
        if (!SeleniumDriverInstance.clickElementById("")) {
            return false;
        }

        //Verifying that the Posting Premium is being generated
        return true;
    }

    //Voluntary Excess section
    private boolean voluntaryExcess() {
        //Capturing Excess Percentage
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryExcessPercentageTextBoxId(),
                this.getData("Excess Percentage"))) {
            return false;
        }

        //Capturing Minimum Amount 
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.VoluntaryMinimumAmountTextBoxId(), this.getData("Minimum Amount"))) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Vuluntary Excess details captured successfully.", false);

        return true;
    }

    //Additional Compulsory Excess section
    private boolean additionalCompulsoryExcess() {
        //Capturing Excess Percentage
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryExcessPercentageTextBoxId(), 
                this.getData("Excess Percentage Additional"))) {
            return false;
        }

        //Capturing Minimum Amount 
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.CompulsoryMinimumAmountTextBoxId(),
                this.getData("Minimum Amount Additional"))) {
            return false;
        }

        //clicking next button
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.SurveyNextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Vuluntary Excess Additional details captured successfully.", false);

        return true;
    }

    private boolean firstAmountPayableSection() {

        //Verify that the Basic – Own Damage (Sub-Section A) checkbox is ticked by default.
        //Verify that the Minimum% is populated by default.
        //Verify that the Minimum Amount is populated by default.
        //Capturing Maximum% 
        //own damage
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPOwnDamageMaxPercentageTextBoxId(),
                this.getData("Own damage max percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPOwnDamageMaxAmountTextBoxId(), 
                this.getData("Own Max Amount"))) {
            return false;
        }

        //Third party 
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPThirdPartyMinPercetage(),
                this.getData("Third Party Min Percentage Amount"))) {
            return false;
        }
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPThirdPartyMaxPercetage(),
                this.getData("Third Party max Percentage Amount"))) {
            return false;
        }
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPThirdPardyMaxAmountTextBoxId(),
                this.getData("Third Party max Amount"))) {
            return false;
        }

        //Fire and Exception(Sub- section B)
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPFireandExplosionMinPercTextBoxId(),
                this.getData("Fire min percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPFireandExplosionMaxPercTextBoxId(), 
                this.getData("Fire max percentage"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPFireandExplosionMaxAmountTextBoxId(),
                this.getData("Fire max amount"))) {
            return false;
        }

        //Additional Theft
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.FAPAdditionalTheftCheckBoxId())) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPAdditionalTheftMinPecentageTextBoxId(), 
                this.getData("Additional Theft min Pecentage"))) {
            return false;
        }
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.AdditionalTheftMaxPercetageTextBoxID(),
                this.getData("Additional Theft  max percentage"))) {
            return false;
        }
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPAdditionalTheftMinAmountTextBoxId(), 
                this.getData("Additional Theft min Amount"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.FAPHijackMaxAmountTextBoxId(), 
                this.getData("Additional Theft Max Amount"))) {
            return false;
        }

        //clicking continue button
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BasicDetailsNextButtonName())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("First Amount Payable section details captured successfully.", false);

        return true;
    }

    private boolean endosements() {
        //clicking select button
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementsSelectButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(1000);
        //clicking add all button
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementsAddAllButtonId())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);
        //Verify that the clause you selected from the available section are now moved to the Selected section
        //clicking aply button to continue
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.EndorsementsApplyButtonId())) {
            return false;
        }
        //Verify that all the clause you selected are populated on the Endorsements section
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Edorsement section was successful.", false);

        //Clicking Next button to continue.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.IndividualNextButtonId())) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Edorsement section Verification was successful.", false);

        return true;
    }

    private boolean specifiedDriverDetails() {
        //clicking add button
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.AddSpecifiedDriverButtonName()) ){
            return false;
        }

        //Capturing Driver on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverNameTextBoxId(), 
                this.getData("Driver"))) {
            return false;
        }
        //Select ID Type on the dropdown menu.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverIDTypeDropdownId(), 
                this.getData("ID Type"))) {
            return false;
        }

        //Capture ID Number on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.SpecifiedDriverIDNumberTextBoxId(),
                this.getData("ID Number"))) {
            return false;
        }
        //Clicking Next button to continue.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BasicDetailsNextButtonName())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Specified Driver details captured successfully.", false);

        //Verify that driver’s details are being populated on the Specified Driver section.
        //Clicking Next button to continue.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BasicDetailsNextButtonName())) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Specified driver details Verified successfully.", false);

        return true;
    }

    private boolean interestedParty() {
        //clicking add button
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.MSAddInterestedPartiesButtonName())) {
            return false;
        }

        //Select Institution Name from the dropdown menu.
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.InterestedPartyInstitutionNameDropdownListId(), 
                this.getData("Institution Name"))) {
            return false;
        }
        //Select Type of Agreement from the dropdown menu.
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.EBPMotorIPTypeofAgreementDropdownListId(), 
                this.getData("Type of Agreement"))) {
            return false;
        }

        //Verify that the Description textbox is greyed out.
        //Click Next button to continue.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BasicDetailsNextButtonName())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Interested party details captured successfully.", false);

        return true;
    }

    private boolean notes() {
        //comments(Not Printed on schedule)
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.NotesCommentsTextboxId(), 
                this.getData("comments"))) {
            return false;
        }

        //Tick Add Notes to this Policy checkbox
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.NotesAddNotesCheckboxId())) {
            return false;
        }

        //Capture notes Printed on schedule on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.NotesNotesTexBboxId(),
                this.getData("Notes Printed on schedule"))) {
            return false;
        }

        //Click Next button to continue.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BasicDetailsNextButtonName())) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Notes details captured successfully.", false);

        //Verify that the Interested Party details are being populated on the Interested Party section.  
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Interested Party details verified successfully.", false);

        
        //Click Next button to continue.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BasicDetailsNextButtonName())) {
            return false;
        }

        //Verify the Financial Overview section.
         SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Financial Overview section verified successfully.", false);

        //Click Next button to continue.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BasicDetailsNextButtonName())) {
            return false;
        }

        //Verify that the Rating Details tab is already clicked.
        //Verify that you on the rating details screen.
        //Verify the financial transaction under rating details section.
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Notes was successfully.", false);

        return true;
    }

    private boolean notes2() {
        //comments(Not Printed on schedule)
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.PersonalAccidentNotesTextBoxId(), this.getData("comments"))) {
            return false;
        }

        //Tick Add Notes to this Policy checkbox
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.NotesCheckBoxId())) {
            return false;
        }

        //Capture notes Printed on schedule on the relevant textbox.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.NotesTextAreaID(), this.getData("Notes Printed on schedule"))) {
            return false;
        }

        //Click Next button to continue.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.BasicDetailsNextButtonName())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Notes for motor specified details captured successfully.", false);

        return true;
    }

    private boolean reinsuranceDetails() {
        //Clicking Reinsurances Details tab.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabId())) {
            return false;
        }

        //Verify that you on the Reinsurances screen.
        //Verify that the Reinsurance band “Motor Specified Passenger” is already selected.
        //Verify the reinsurer financial transactions.
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Reinsurance details Motor Specified Passenger  already selected.", false);

        
        //Select Reinsurance band “MS Contingent Liability” from the dropdown menu.
        //Verify the reinsurer financial transactions.

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BandDropdownListId(), this.getData("Reinsurance band MS Contingent"))) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Reinsurance band MS Contingent Liability selected successfully.", false);

        //Clicking Reinsurances Details tab.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabId())) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        //Select Reinsurance band “MS Liability (Section B)” from the dropdown menu.
        //Verify the reinsurer financial transactions.
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BandDropdownListId(), this.getData("Reinsurance band MS Liability"))) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Reinsurance band MS Liability(Section B) selected successfully.", false);

        //Clicking Reinsurances Details tab.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabId())) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        //Select Reinsurance band “MS Own Damage (Section A)” from the dropdown menu.
        //Verify the reinsurer financial transactions.

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BandDropdownListId(), this.getData("Reinsurance band Ms own"))) {
          return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Reinsurance band MS Own Damage (Section A) selected successfully.", false);

         //Clicking Reinsurances Details tab.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabId())) {
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        //Select Reinsurance Band “MS Unauthorised Passenger” from the dropdown menu.
        //Verify the reinsurer financial transactions
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.BandDropdownListId(), this.getData("MS Unauthorised Passenger"))) {
          return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Reinsurance band MS Unauthorised Passenger selected successfully.", false);


        //Clicking Reinsurances Details tab.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceDetailsTabId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        //Verify the reinsurer financial transactions.
        //Click ok button to continue.
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.ReinsuranceOkButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);
        SeleniumDriverInstance.takeScreenShot("Reinsurance details captured successfully.", false);

        return true;
    }

}
