/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduBusinessAllRiskPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduCreateNewPolicyForNewClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;


public class EkunduEPLMotorSectionDetailsTest extends BaseClass
  {
    TestEntity testData;
    TestResult testResultt;

    public EkunduEPLMotorSectionDetailsTest(TestEntity testData)
      {
        this.testData = testData;
      }

    public TestResult executeTest()
      {
        this.setStartTime();

        if (!verifyRisksDialogisPresent())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to verify that the risk dialog is present"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to verify that the riss dialog is present- "
                                  + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
          }

        if (!enterVehicleDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter vehicle details"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter vehicle details- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }
        
        if(!enterMSDEndorsements())
        {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Motor Section Details endorsements", true);
            return new TestResult(testData,Enums.ResultStatus.FAIL, "Failed to enter Motor Section Details endorsements- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!ReinsuranceDetails())
          {
            SeleniumDriverInstance.takeScreenShot(("Failed to enter risk notes"), true);

            return new TestResult(testData, Enums.ResultStatus.FAIL,
                                  "Failed to enter risk notes- " + SeleniumDriverInstance.DriverExceptionDetail,
                                  this.getTotalExecutionTime());
          }

        SeleniumDriverInstance.takeScreenShot(("Motor Section Details risk added successfully"), false);

        return new TestResult(testData, Enums.ResultStatus.PASS, "Motor Section Details risk added successfully", this.getTotalExecutionTime());

      }

    private boolean verifyRisksDialogisPresent()
      {
        SeleniumDriverInstance.pause(2000);

        if (SeleniumDriverInstance.waitForElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId()))
          {
            SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.AddRiskButtonId());
            selectMotorRiskType();

            return true;
          }

        else if (SeleniumDriverInstance.waitForElementById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            selectMotorRiskType();

            return true;

          }

        else
          {
            SeleniumDriverInstance.takeScreenShot("Failed to verify that the Risk Dialog is present", true);
            System.err.println("[Error] Failed to verify that the Risk Dialog is present, exception detected - Fault - ");

            return false;

          }

      }

    private boolean selectMotorRiskType()
      {
        if (!SeleniumDriverInstance.switchToFrameById(EkunduViewClientDetailsPage.SelectRiskTypeDialogFrameId()))
          {
            return false;
          }

        if (SeleniumDriverInstance.selectRiskType(testData.TestParameters.get("Risk Name")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.switchToDefaultContent())
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Motor risk type selected successfully", false);

        return true;

      }

    private boolean enterVehicleDetails()
      {
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.NumberOfVehiclesTextBoxId(),
                testData.TestParameters.get("Number of vehicles on policy")))
          {
            return false;

          }
        
        if (!SeleniumDriverInstance.clickElementbyName(EkunduCreateNewPolicyForNewClientPage.DriverDetailsAddButtonName()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MotorSectionRegularDriverTextboxId(),
                testData.TestParameters.get("Regular Driver")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorSectionGenderDropdownListId(),
                testData.TestParameters.get("Gender")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MotorSectionDateofBirthTextboxId(),
                testData.TestParameters.get("Date of Birth")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MotorSectionDateLicenseIssuedTextboxId(),
                testData.TestParameters.get("Date License Issued")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorSectionIDTypeRequiredDropdownListId(),
                testData.TestParameters.get("ID Type Required")))
          {
            return false;
          }

        if (!SeleniumDriverInstance.enterTextById(EkunduCreateNewPolicyForNewClientPage.MotorSectionIDNumberTextboxId(),
                testData.TestParameters.get("ID Number")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorSectionDriverLicenseTypeDropdownListId(),
                testData.TestParameters.get("Driver license Type")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorSectionClaim1DropdownListId(),
                testData.TestParameters.get("Claims 0-12")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorSectionClaim2DropdownListId(),
                testData.TestParameters.get("Claims 13-24")))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduCreateNewPolicyForNewClientPage.MotorSectionClaim3DropdownListId(),
                testData.TestParameters.get("Claims 25-36")))
          {
            return false;
          }

        SeleniumDriverInstance.takeScreenShot("Vehicle details entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
          {
            return false;
          }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
          {
            return false;
          }
        
        if(!SeleniumDriverInstance.checkBoxSelectionById(EkunduCreateNewPolicyForNewClientPage.CheckRoadsideCheckBoxId(), true))
        {
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementById(EkunduCreateNewPolicyForNewClientPage.MotorNextButtonId()))
          {
            return false;
          }

        return true;
      }

    private boolean enterMSDEndorsements()
    {
        if(!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.EndorsementsSelectButtonId()))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.MotorSectionEndorsementsAddAllButtonId()))
        {
            return false;
        }
         
         if(!SeleniumDriverInstance.clickElementById(EkunduBusinessAllRiskPage.MotorSectionEndorsementsApplyButtonId()))
        {
            return false;
        }
         
           if(!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId()))
        {
            return false;
        }
         
            SeleniumDriverInstance.takeScreenShot("Endorsements entered successfully", false);
         
         return true;
     
     }
    
    private boolean ReinsuranceDetails()
    {
       SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.OKButtonId());

        SeleniumDriverInstance.takeScreenShot("Risk Added successfully", false);

        SeleniumDriverInstance.pause(5000); 
        
      return true;  
    }

    
  }
