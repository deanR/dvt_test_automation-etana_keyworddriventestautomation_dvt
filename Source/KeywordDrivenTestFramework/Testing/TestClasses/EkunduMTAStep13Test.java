/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduCreateCorporateClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduFindClientPage;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;
import KeywordDrivenTestFramework.Testing.PageObjects.EkunduViewClientDetailsPage;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author ferdinandN
 */
public class EkunduMTAStep13Test extends BaseClass {

    TestEntity testData;
    TestResult testResult;
    JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;

    public EkunduMTAStep13Test(TestEntity testData) {
        this.testData = testData;
    }

    public TestResult executeTest() {
        SeleniumDriverInstance.DriverExceptionDetail = "";
        this.setStartTime();

        if (!navigateToFindClientPage()) {
            SeleniumDriverInstance.takeScreenShot("Failed to navigate to the find client page", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to navigate to the find client page - " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!findClient()) {
            SeleniumDriverInstance.takeScreenShot("Failed to find client", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to find client- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!enterMidTermAdjustmentData()) {
            SeleniumDriverInstance.takeScreenShot("Failed to enter Mid Term Adjustment data", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to enter Mid Term Adjustment data- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        if (!editGroupedFire1()) {
            SeleniumDriverInstance.takeScreenShot("Failed to edit grouped fire 1", true);
            return new TestResult(testData, Enums.ResultStatus.FAIL, "Failed to edit grouped fire 1- " + SeleniumDriverInstance.DriverExceptionDetail, this.getTotalExecutionTime());
        }

        return new TestResult(testData, Enums.ResultStatus.PASS, "Mid Term Adjustment step 13 completed successfully", this.getTotalExecutionTime());

    }

    private boolean navigateToFindClientPage() {
        if (!SeleniumDriverInstance.navigateToFindClientPage(EKunduHomePage.ClientHoverTabId(), "Find Client")) {
            return false;
        }

        return true;
    }

    private boolean findClient() {
        String clientCode = SeleniumDriverInstance.retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase"), "Retrieved Client Code");

        if (clientCode.equals("parameter not found")) {
            clientCode = testData.TestParameters.get("Client Code");
        } else {
            testData.updateParameter("Client Code", clientCode);
        }

        if (!SeleniumDriverInstance.enterTextById(EKunduFindClientPage.ClientCodeTextBoxId(), clientCode)) {
            return false;
        }
        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.SearchButtonId())) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Client Found", false);

        if (!SeleniumDriverInstance.clickElementById(EKunduFindClientPage.ResultsSelectFirstLinkId())) {
            return false;
        }

        return true;
    }

    private boolean enterMidTermAdjustmentData() {
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ViewClientDetailsPoliciesTabPartialLinkText())) {
            return false;
        }

        String policyNumber = retrieveTestParameterUsingTestCaseId(testData.TestParameters.get("Linked Testcase2"),
                "Retrieved Policy Number");

        if (policyNumber.equals("parameter not found")) {
            policyNumber = testData.TestParameters.get("Policy Number");
        } else {
            testData.updateParameter("Policy Number", policyNumber);
        }

//         if(!SeleniumDriverInstance.changePolicy(policyNumber, "Change"))
//        {
//            return false;
//        }
        SeleniumDriverInstance.pause(1000);
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ChangePolicyLinkText())) {
            return false;
        }
        SeleniumDriverInstance.pause(1000);

        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingId(EkunduViewClientDetailsPage.MTATypeofChangeDropDownListId(), this.getData("Type of Change"))) {
            return false;
        }

        if (!SeleniumDriverInstance.enterTextById(EkunduViewClientDetailsPage.MIAEffectiveDateTextBoxId(), this.getData("Effective Date"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EKunduCreateCorporateClientPage.SubmitCorporateClientButtonId())) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        return true;

    }

    private boolean editGroupedFire1() {
        if (!SeleniumDriverInstance.selectByTextFromDropDownListUsingXpath(EkunduViewClientDetailsPage.GroupedFireRisk5ActionDropdownListXpath(), "Edit")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId());

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireOverviewFlatPremiumCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.checkBoxSelectionById(EkunduViewClientDetailsPage.FireRiskDataBuildingsCheckBoxId(), true)) {
            return false;
        }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.FireRiskDataBuildingsRiskSumInsuredTextBoxId(), this.getData("GF - Fire - Risk Data - Sum Insured"))) {
            return false;
        }

        if (!SeleniumDriverInstance.clearTextAndEnterValueById(EkunduViewClientDetailsPage.FireRiskDataBuildingsRiskPremiumTextBoxId(), this.getData("GF - Fire - Risk Data - Premium"))) {
            return false;
        }

        SeleniumDriverInstance.takeScreenShot("Fire risk entered successfully", false);

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.NextButtonId())) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());
        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances")) {
            return false;
        }

        this.deleteFACPlacements();

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances")) {
            return false;
        }

        SeleniumDriverInstance.pause(2000);

        SeleniumDriverInstance.clickElementById(EkunduViewClientDetailsPage.ReinsuranceDetailsOKButtonId());

        return true;
    }

    private boolean deleteFACPlacements() {
        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl03_lnkDelete")) {
            return false;
        }

        SeleniumDriverInstance.acceptAlertDialog();

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances")) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl03_lnkDelete")) {
            return false;
        }

        SeleniumDriverInstance.acceptAlertDialog();

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances")) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementById("ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl03_lnkDelete")) {
            return false;
        }

        SeleniumDriverInstance.acceptAlertDialog();

        SeleniumDriverInstance.pause(2000);

        if (!SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText())) {
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyLinkText("Reinsurances")) {
            return false;
        }

        SeleniumDriverInstance.clickElementbyLinkText(EkunduViewClientDetailsPage.ReinsuranceDetailsTabPartialLinkText());

        SeleniumDriverInstance.takeScreenShot("Reinsurance Placements deleted successfully", false);
        return true;
    }

    public String getData(String parameterName) {
        if (testData.TestParameters.containsKey(parameterName)) {
            return testData.TestParameters.get(parameterName);
        } else {
            System.err.println("Parameter - " + parameterName + " was not defined");

            return "";
        }
    }

}
