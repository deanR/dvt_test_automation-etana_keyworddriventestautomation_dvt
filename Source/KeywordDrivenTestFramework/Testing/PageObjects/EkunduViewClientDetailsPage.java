/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author fnell
 */
public class EkunduViewClientDetailsPage {
// <editor-fold defaultstate="collapsed" desc="Labels">

    public static String QuoteReferenceLabelId() {
        return "ctl00_ClientInfoCtrl_lblPolicyRef";
    }

    public static String ReinsuranceDetailsLabelId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_lblReinsuranceMain";
    }

    public static String InsuredPerilsDataTableTitleLabelId() {
        return "FireRateLDTitle";
    }

    public static String ClientHoverTabId() {
        return "s-client";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="TextBoxes">
    public static String MotorTradersExternalExtensionsContingentLiabilityLOITextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_254";
    }

    public static String ReinsurerSearchReinsurerCodeTextBoxId() {
        return "ctl01_cntMainBody_txtReinsurerCode";
    }

    public static String NumberofVehiclesTextBoxId() {
        return "ctl00_cntMainBody_MSD__NUM_VEHICLES";
    }

    public static String NumberofClaimsIncurredTextBoxId() {
        return "ctl00_cntMainBody_MSD__CLAIMS_INCURRED_12_MONTHS";
    }

    public static String PaymentDetailsRecievedAmountTextboxId() {
        return "ctl01_cntMainBody_txtPaymentAmount";
    }

    public static String FindOtherPartyPartyCodeTextBoxId() {
        return "ctl01_cntMainBody_txtClientCode";
    }

    public static String MotorTradersExternalExtensionsContingentLiabilityRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_254";
    }

    public static String MotorTradersExternalExtensionsContingentLiabilityFAPPercTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_254";
    }

    public static String MotorTradersExternalExtensionsContingentLiabilityFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_254";
    }

    public static String MotorTradersExternalExtensionsLossofKeysFAPPercTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_255";
    }

    public static String MotorTradersExternalExtensionsLossofKeysFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_255";
    }

    public static String MotorTradersExternalExtensionsLossofUsePremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_249";
    }

    public static String MotorTradersExternalExtensionsLossofUseFAPPercTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_249";
    }

    public static String MotorTradersExternalExtensionsLossofUseFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_249";
    }

    public static String MotorTradersExternalExtensionsMotorCycleFAPPercTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_256";
    }

    public static String MotorTradersExternalExtensionsMotorCycleFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_256";
    }

    public static String MotorTradersExternalExtensionsPassangerLiabilityLOITextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_251";
    }

    public static String MotorTradersExternalExtensionsPassangerLiabilityRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_251";
    }

    public static String MotorTradersExternalExtensionsPassangerLiabilityFAPPercTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_251";
    }

    public static String MotorTradersExternalExtensionsPassangerLiabilityFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_251";
    }

    public static String MotorTradersExternalExtensionsSocialDomesticPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_248";
    }

    public static String MotorTradersExternalExtensionsSocialDomesticFAPPercTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_248";
    }

    public static String MotorTradersExternalExtensionsSocialDomesticFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_248";
    }

    public static String MotorTradersExternalExtensionsSpecialTypeVehiclesFAPPercTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_257";
    }

    public static String MotorTradersExternalExtensionsSpecialTypeVehiclesFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_257";
    }

    public static String MotorTradersExternalExtensionsUnauthorisedUsePremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_250";
    }

    public static String MotorTradersExternalExtensionsUnauthorisedUseFAPPercTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_250";
    }

    public static String MotorTradersExternalExtensionsUnauthorisedUseFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_250";
    }

    public static String MotorTradersExternalExtensionsVehicleLentPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_253";
    }

    public static String MotorTradersExternalExtensionsVehicleLentFAPPercTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_253";
    }

    public static String MotorTradersExternalExtensionsVehicleLentFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_253";
    }

    public static String MotorTradersExternalExtensionsWindscreenCoverPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_252";
    }

    public static String MotorTradersExternalExtensionsWindscreenCoverFAPPercTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_252";
    }

    public static String MotorTradersExternalExtensionsWindscreenCoverFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_252";
    }

    public static String MotorTradersExternalExtensionsWreckageRemovalPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_261";
    }

    public static String MotorTradersExternalExtensionsWreckageRemovalFAPPercTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_261";
    }

    public static String MotorTradersExternalExtensionsWreckageRemovalFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_261";
    }

    public static String MotorTradersExtensionsCarHoistsPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_264";
    }

    public static String MotorTradersExtensionsCarHoistsFAPPercTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_264";
    }

    public static String MotorTradersExtensionsCarHoistsFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_264";
    }

    public static String MotorTradersExtensionsLossofKeysPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_265";
    }

    public static String MotorTradersExtensionsLossofKeysFAPPercTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_265";
    }

    public static String MotorTradersExtensionsLossofKeysFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_265";
    }

    public static String MotorTradersExtensionsWindscreenPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_252";
    }

    public static String MotorTradersExtensionsWindscreenFAPPercTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_252";
    }

    public static String MotorTradersExtensionsWindscreenFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_252";
    }

    public static String MotorTradersExtensionsWorkAwayPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_263";
    }

    public static String MotorTradersExtensionsWorkAwayFAPPercTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_263";
    }

    public static String MotorTradersExtensionsWorkAwayFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_263";
    }

    public static String MotorTradersExtensionsWreckagePremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_261";
    }

    public static String MotorTradersExtensionsWreckageFAPPercTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_261";
    }

    public static String MotorTradersExtensionsWreckageFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_261";
    }

    public static String SubSectionASumInsuredTextBoxId() {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_SI";
    }

    public static String SubSectionARateTextBoxId() {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_RATE";
    }

    public static String SubSectionAPremiumTextBoxId() {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_PREMIUM";
    }

    public static String SubSectionAFAPPercTextBoxId() {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_FAP_PERC";
    }

    public static String SubSectionAFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_FAP_AMOUNT";
    }

    public static String SubSectionBFAPPercTextBoxId() {
        return "ctl00_cntMainBody_MT__SUB_SECTION_B_FAP_PERC";
    }

    public static String SubSectionBFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_MT__SUB_SECTION_FAP_AMOUNT";
    }

    public static String SubSectionAAnnualWageAmountTextBoxId() {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_ANNUAL_WAGE_AMOUNT";
    }

    public static String SubSectionBLimitofIndemnityTextBoxId() {
        return "ctl00_cntMainBody_MT__SUB_SECTION_B_LOI";
    }

    public static String SubSectionBRateTextBoxId() {
        return "ctl00_cntMainBody_MT__SUB_SECTION_B_RATE";
    }

    public static String CoverVoluntaryFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_MT__VOLUNTARY_FAP_PERC";
    }

    public static String CoverVoluntaryFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_MT__VOLUNTARY_FAP_AMOUNT";
    }

    public static String PolicyRefTextBoxId() {
        return "ctl00_cntMainBody_txtReference";
    }

    public static String CoverStartDateTextBoxId() {
        return "ctl00_cntMainBody_POLICYHEADER__COVERSTARTDATE";
    }

    public static String CoverEndDateTextBoxId() {
        return "ctl00_cntMainBody_POLICYHEADER__COVERENDDATE";
    }

    public static String IndustryTextBoxId() {
        return "industry";
    }

    public static String RatesFinalAgreedRateTextBoxId() {
        return "ctl00_cntMainBody_GROUP_FIRE__AVG_RATE_AGREED_RATE";
    }

    public static String TotalSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_FIRE__TOTAL_SI";
    }

    public static String TotalPremiumTextBoxId() {
        return "ctl00_cntMainBody_FIRE__TOTAL_PREM";
    }

    public static String FireRiskDataBuildingsRiskSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_FIRE__COL1_SI";
    }

    public static String FireRiskDataBuildingsRiskPremiumTextBoxId() {
        return "ctl00_cntMainBody_FIRE__COL1_PREM";
    }

    public static String FireRiskDataRentRiskSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_FIRE__COL2_SI";
    }

    public static String FireRiskDataRentRiskPremiumTextBoxId() {
        return "ctl00_cntMainBody_FIRE__COL2_PREM";
    }

    public static String FireRiskDataPlantRiskSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_FIRE__COL3_SI";
    }

    public static String FireRiskDataPlantRiskPremiumTextBoxId() {
        return "ctl00_cntMainBody_FIRE__COL3_PREM";
    }

    public static String FireRiskDataStockRiskSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_FIRE__COL4_SI";
    }

    public static String FireRiskDataStockRiskPremiumTextBoxId() {
        return "ctl00_cntMainBody_FIRE__COL4_PREM";
    }

    public static String FireRiskDataPLRiskSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_FIRE__COL7_SI";
    }

    public static String FireRiskDataPLRiskPremiumTextBoxId() {
        return "ctl00_cntMainBody_FIRE__COL7_PREM";
    }

    public static String FireRiskNumberOfMonthsTextBoxId() {
        return "ctl00_cntMainBody_FIRE__COL2_NO_MONTHS";
    }

    public static String FireBuildingsExcalationPercentTextBoxId() {
        return "ctl00_cntMainBody_FIRE__BUILDINGS_ESC_PERC";
    }

    public static String FireBuildingsExcalationPremiumTextBoxId() {
        return "ctl00_cntMainBody_FIRE__COL1_INFL_APPL";
    }

    public static String FirePlantMachineryExcalationPercentTextBoxId() {
        return "ctl00_cntMainBody_FIRE__PLANT_MACH_ESCPERC";
    }

    public static String FirePlantMachineryExcalationPremiumTextBoxId() {
        return "ctl00_cntMainBody_FIRE__COL3_INFL_APPL";
    }

    public static String EHATPCAdditionalRentPremiumTextBoxId() {
        return "ctl00_cntMainBody_PC__ADD_RENT_PREMIUM";
    }

    public static String MiscItemDescriptionTextBoxId() {
        return "ctl00_cntMainBody_FI_RISK_MICS_ITEMS__DESCRIP";
    }

    public static String EHATPCMiscItemDescriptionTextBoxId() {
        return "ctl00_cntMainBody_PC_MISC_ITEMS__ITEM_DESCRIPTION";
    }

    public static String MiscItemSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_FI_RISK_MICS_ITEMS__SI";
    }

    public static String EHATMiscItemSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_PC_MISC_ITEMS__ITEM_SI";
    }

    public static String FireMinimumPercentageFirstAmountPayableTextBoxId() {
        return "ctl00_cntMainBody_FIRE__FAP_MIN_PERC";
    }

    public static String EHATFireMinimumPercentageFirstAmountPayableTextBoxId() {
        return "ctl00_cntMainBody_PC_MISC_ITEMS__FAP_MIN_PERC";
    }

    public static String EHATPCMinimumPercentageFirstAmountPayableTextBoxId() {
        return "ctl00_cntMainBody_PC_FAPS__FAP_MIN_PERC";
    }

    public static String EHATPCFirstAmountPayableDescriptonTextBoxId() {
        return "ctl00_cntMainBody_PC_FAPS__FAP_DESCRIPTION";
    }

    public static String FireMinimumAmountFirstAmountPayableTextBoxId() {
        return "ctl00_cntMainBody_FIRE__FAP_MIN_AMT";
    }

    public static String EHATFireMinimumAmountFirstAmountPayableTextBoxId() {
        return "ctl00_cntMainBody_PC_MISC_ITEMS__FAP_MIN_AMT";
    }

    public static String EHATPCMinimumAmountFirstAmountPayableTextBoxId() {
        return "ctl00_cntMainBody_PC_FAPS__FAP_MIN_AMT";
    }

    public static String FireMaximumAmountFirstAmountPayableTextBoxId() {
        return "ctl00_cntMainBody_FIRE__FAP_MAX_AMT";
    }

    public static String EHATFireMaximumAmountFirstAmountPayableTextBoxId() {
        return "ctl00_cntMainBody_PC_MISC_ITEMS__FAP_MAX_AMT";
    }

    public static String EHATPCMaximumAmountFirstAmountPayableTextBoxId() {
        return "ctl00_cntMainBody_PC_FAPS__FAP_MAX_AMT";
    }

    public static String FireClaimsPrepLimitOfIndemnityTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
    }

    public static String EHATClaimsPrepLimitOfIndemnityTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_102";
    }

    public static String FireClaimsPrepRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_25";
    }

    public static String EHATClaimsPrepRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_102";
    }

    public static String EHATClaimsPrepPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_102";
    }

    public static String FireLeakgeLimitOfIndemnityTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_101";
    }

    public static String PCPreventionofAccessLimitOfIndemnityTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_289";
    }

    public static String PCPreventionofAccessRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_289";
    }

    public static String PCPreventionofAccessFAPPercTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_289";
    }

    public static String PCPreventionofAccessFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_289";
    }

    public static String PCPreventionofAccessPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_289";
    }

    public static String FireLeakgeRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_101";
    }

    public static String FireRiotPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_35";
    }

    public static String PCRiotPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_275";
    }

    public static String FireSubsidenceLimitOfIndemnityTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_100";
    }

    public static String PCSubsidenceLimitOfIndemnityTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_106";
    }

    public static String PCRiotFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_275";
    }

    public static String PCSubsidenceFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_106";
    }

    public static String PCSubsidenceFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_106";
    }

    public static String PCRiotFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_275";
    }

    public static String FireSubsidenceRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_100";
    }

    public static String PCSubsidenceRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_106";
    }

    public static String PCSubsidencePremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_106";
    }

    public static String BIOverviewConversionFactorTextBoxId() {
        return "ctl00_cntMainBody_BI__ADJ_CONVERSION_FACTOR";
    }

    public static String BICoverGrossProfitBasisSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_BI__GROSS_PROFIT_BASIS_SI";
    }

    public static String EHATBICoverGrossProfitBasisSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_144";
    }

    public static String BICoverGrossProfitBasisRateTextBoxId() {
        return "ctl00_cntMainBody_BI__GROSS_PROFIT_BASIS_RATE";
    }

    public static String BICoverGrossProfitBasisPRemiumTextBoxId() {
        return "ctl00_cntMainBody_BI__GROSS_PROFIT_BASIS_PREM";
    }

    public static String EkunduAppVersionLabelId() {
        return "ctl00_app_ver";
    }

    public static String BICoverGrossRentalsMonthsTextBoxId() {
        return "ctl00_cntMainBody_BI__GROSS_RENTALS_NUM";
    }

    public static String EHATBICoverGrossRentalsMonthsTextBoxId() {
        return "ctl00_cntMainBody_COVER_TYPE_GR_145";
    }

    public static String BICoverGrossRentalsSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_BI__GROSS_RENTALS_SI";
    }

    public static String EHATBICoverGrossRentalsSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_145";
    }

    public static String BICoverGrossRentalsRateTextBoxId() {
        return "ctl00_cntMainBody_BI__GROSS_RENTALS_RATE";
    }

    public static String BICoverGrossRentalsPremiumTextBoxId() {
        return "ctl00_cntMainBody_BI__GROSS_RENTALS_PREM";
    }

    public static String BICoverRevenueSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_BI__REVENUE_SI";
    }

    public static String EHATBICoverRevenueSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_146";
    }

    public static String BICoverRevenueRateTextBoxId() {
        return "ctl00_cntMainBody_BI__REVENUE_RATE";
    }

    public static String BICoverRevenuePremiumTextBoxId() {
        return "ctl00_cntMainBody_BI__REVENUE_PREM";
    }

    public static String BICoverAdditionalIncreaseCostOfWorkingSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_BI__ICOW_INSURED";
    }

    public static String EHATBICoverAdditionalIncreaseCostOfWorkingSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_147";
    }

    public static String BICoverAdditionalIncreaseCostOfWorkingRateTextBoxId() {
        return "ctl00_cntMainBody_BI__ICOW_INSURED";
    }

    public static String BICoverAdditionalIncreaseCostOfWorkingPremiumTextBoxId() {
        return "ctl00_cntMainBody_BI__ICOW_PREM";
    }

    public static String BICoverWagesSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_BI__WAGES_SI";
    }

    public static String EHATBICoverLossofLeviesSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_204";
    }

    public static String EHATBICoverLossofTouristAttractionSumInsuredTextboxId() {
        return "ctl00_cntMainBody_SUM_INSURED_205";
    }

    public static String BICoverWagesRateTextBoxId() {
        return "ctl00_cntMainBody_BI__WAGES_RATE";
    }

    public static String BICoverWagesPremiumTextBoxId() {
        return "ctl00_cntMainBody_BI__WAGES_PREM";
    }

    public static String BIFirstAmountPayableMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_BI__FAP_MIN_PERC";
    }

    public static String MoneyCoverMajorLimitSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_MONEY__MAJOR_SI";
    }

    public static String MoneyCoverMajorLimitPremiumTextBoxId() {
        return "ctl00_cntMainBody_MONEY__MAJOR_PREM";
    }

    public static String BIFirstAmountPayableMinAmountTextBoxId() {
        return "ctl00_cntMainBody_BI__FAP_MIN_AMT";
    }

    public static String BIFirstAmountPayableMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_BI__FAP_MAX_AMT";
    }

    public static String BIExtensionsAccidentalDamageLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_108";
    }

    public static String EHATBIExtensionsAccidentalDamageLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_107";
    }

    public static String EHATBIExtensionsAdditionalPremisesClauseLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_225";
    }

    public static String EHATBIExtensionsAdditionalPremisesClauseRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_225";
    }

    public static String BIExtensionsAccidentalDamageRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_108";
    }

    public static String EHATBIExtensionsAccidentalDamageRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_107";
    }

    public static String BIExtensionsAccidentalDamageFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_108";
    }

    public static String EHATBIExtensionsAccidentalDamageFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_107";
    }

    public static String EHATBIExtensionsAddionalPremisesClauseFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_225";
    }

    public static String EHATBIExtensionsAddionalPremisesClauseFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_225";
    }

    public static String BIExtensionsAccidentalDamageFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_108";
    }

    public static String EHATBIExtensionsAccidentalDamageFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_107";
    }

    public static String BIExtensionsClaimsLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
    }

    public static String EHATBIExtensionsClaimsLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_102";
    }

    public static String BIExtensionsClaimsRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_25";
    }

    public static String EHATBIExtensionsClaimsRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_102";
    }

    public static String BIExtensionsFinesLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_107";
    }

    public static String EHATBIExtensionsFinesLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_108";
    }

    public static String BIExtensionsFinesRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_107";
    }

    public static String EHATBIExtensionsFinesRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_108";
    }

    public static String BIExtensionsFinesFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_107";
    }

    public static String EHATBIExtensionsFinesFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_108";
    }

    public static String BIExtensionsFinesFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_107";
    }

    public static String EHATBIExtensionsFinesFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_108";
    }

    public static String BIExtensionsPreventionOfAccesRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_105";
    }

    public static String EHATBIExtensionsPreventionOfAccesRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_281";
    }

    public static String EHATBIExtensionsPreventionOfAccesLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_281";
    }

    public static String BIExtensionsPreventionOfAccesPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_105";
    }

    public static String BIExtensionsPreventionOfAccesFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_105";
    }

    public static String EHATBIExtensionsPreventionOfAccesFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_281";
    }

    public static String BIExtensionsPreventionOfAccesFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_105";
    }

    public static String EHATBIExtensionsPreventionOfAccesFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_281";
    }

    public static String BIExtensionsPublicTelecommunicationsExtendedRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_104";
    }

    public static String EHATBIExtensionsPublicTelecommunicationsExtendedRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_282";
    }

    public static String EHATBIExtensionsPublicTelecommunicationsExtendedLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_282";
    }

    public static String BIExtensionsPublicTelecommunicationsExtendedPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_104";
    }

    public static String BIExtensionsPublicTelecommunicationsExtendedFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_104";
    }

    public static String EHATBIExtensionsPublicTelecommunicationsExtendedFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_282";
    }

    public static String BIExtensionsPublicTelecommunicationsExtendedFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_104";
    }

    public static String EHATBIExtensionsPublicTelecommunicationsExtendedFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_282";
    }

    public static String BIExtensionsPublicUtilitiesExtendedRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_113";
    }

    public static String EHATBIExtensionsPublicUtilitiesExtendedRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_284";
    }

    public static String EHATBIExtensionsPublicUtilitiesExtendedLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_284";
    }

    public static String BIExtensionsPublicUtilitiesExtendedPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_113";
    }

    public static String BIExtensionsPublicUtilitiesExtendedFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_113";
    }

    public static String EHATBIExtensionsPublicUtilitiesExtendedFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_284";
    }

    public static String BIExtensionsPublicUtilitiesExtendedFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_113";
    }

    public static String EHATBIExtensionsPublicUtilitiesExtendedFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_284";
    }

    public static String BIExtensionsSpecifiedCustomersRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_112";
    }

    public static String EHATBIExtensionsSpecifiedCustomersRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_286";
    }

    public static String EHATBIExtensionsSpecifiedCustomersLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_286";
    }

    public static String BIExtensionsSpecifiedCustomersPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_112";
    }

    public static String BIExtensionsSpecifiedCustomersFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_112";
    }

    public static String EHATBIExtensionsSpecifiedCustomersFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_286";
    }

    public static String BIExtensionsSpecifiedCustomersFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_112";
    }

    public static String EHATBIExtensionsSpecifiedCustomersFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_286";
    }

    public static String BIExtensionsSpecifiedSuppliersRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_110";
    }

    public static String EHATBIExtensionsSpecifiedSuppliersRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_287";
    }

    public static String EHATBIExtensionsSpecifiedSuppliersLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_287";
    }

    public static String BIExtensionsSpecifiedSuppliersPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_110";
    }

    public static String BIExtensionsSpecifiedSuppliersFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_110";
    }

    public static String BIExtensionsSpecifiedSuppliersFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_110";
    }

    public static String EHATBIExtensionsSpecifiedSuppliersFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_287";
    }

    public static String EHATBIExtensionsSpecifiedSuppliersFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_287";
    }

    public static String BIExtensionsUnSpecifiedCustomersRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_111";
    }

    public static String EHATBIExtensionsUnSpecifiedCustomersRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_288";
    }

    public static String EHATBIExtensionsUnSpecifiedCustomersLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_288";
    }

    public static String BIExtensionsUnSpecifiedCustomersPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_111";
    }

    public static String BIExtensionsUnSpecifiedCustomersFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_111";
    }

    public static String EHATBIExtensionsUnSpecifiedCustomersFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_288";
    }

    public static String BIExtensionsUnSpecifiedCustomersFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_111";
    }

    public static String EHATBIExtensionsUnSpecifiedCustomersFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_288";
    }

    public static String GICoverGreensSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_214";
    }

    public static String GICoverClaimsPrepSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_216";
    }

    public static String GICoverGreensPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_214";
    }

    public static String GICoverClaimsPrepPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_216";
    }

    public static String GICoverIrrigationSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_215";
    }

    public static String GICoverIrrigationPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_215";
    }

    public static String BIExtensionsUnSpecifiedSuppliersLimitOfIndemnityTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_109";
    }

    public static String EHATBIExtensionsUnSpecifiedSuppliersLimitOfIndemnityTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_117";
    }

    public static String BIExtensionsUnSpecifiedSuppliersRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_109";
    }

    public static String EHATBIExtensionsUnSpecifiedSuppliersRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_117";
    }

    public static String BIExtensionsUnSpecifiedSuppliersFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_109";
    }

    public static String EHATBIExtensionsUnSpecifiedSuppliersFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_117";
    }

    public static String BIExtensionsUnSpecifiedSuppliersFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_109";
    }

    public static String EHATBIExtensionsUnSpecifiedSuppliersFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_117";
    }

    public static String BISpecifiedSuppliersSupplierNameTextBoxId() {
        return "ctl00_cntMainBody_SPEC_SUPPLIER__SPEC_NAME";
    }

    public static String EHATBISpecifiedSuppliersSupplierNameTextBoxId() {
        return "ctl00_cntMainBody_SPEC_SUPPLIER__SPEC_NAME";
    }

    public static String BISpecifiedSuppliersSupplierPercentageTextBoxId() {
        return "ctl00_cntMainBody_SPEC_SUPPLIER__SPEC_PERC";
    }

    public static String BISpecifiedCustomersCustomerNameTextBoxId() {
        return "ctl00_cntMainBody_SPEC_CUSTOMER__SPEC_NAME";
    }

    public static String BISpecifiedCustomersCustomerPercentageTextBoxId() {
        return "ctl00_cntMainBody_SPEC_CUSTOMER__SPEC_PERC";
    }

    public static String BCBuildingsCombinedSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_BC__BC_SI";
    }

    public static String BCBuildingsCombinedAgreedRateTextBoxId() {
        return "ctl00_cntMainBody_BC__BC_AGREED_RATE";
    }

    public static String BCBuildingsCombinedEscalationPercentageTextBoxId() {
        return "ctl00_cntMainBody_BC__ESC_PERC";
    }

    public static String BCBuildingsCombinedInflation1stYearPercentageTextBoxId() {
        return "ctl00_cntMainBody_BC__INFL_1YR_PERC";
    }

    public static String BCBuildingsCombinedInflation2ndYearPercentageTextBoxId() {
        return "ctl00_cntMainBody_BC__INFL_2YR_PERC";
    }

    public static String BCAdditionalRentSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_BC__ADD_RENT_SI";
    }

    public static String BCAdditionalRentRateTextBoxId() {
        return "ctl00_cntMainBody_BC__ADD_RENT_RATE";
    }

    public static String BCMiscellaneousItemDescriptionTextBoxId() {
        return "ctl00_cntMainBody_BC_ITEMS__ITEMS_DESC";
    }

    public static String BCMiscellaneousItemSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_BC_ITEMS__ITEMS_SI";
    }

    public static String BCMiscellaneousItemRateTextBoxId() {
        return "ctl00_cntMainBody_BC_ITEMS__ITEMS_RATE";
    }

    public static String BCFirstAmountPayablePercentageTextBoxId() {
        return "ctl00_cntMainBody_BC__FAP_MIN_PERC";
    }

    public static String BCFirstAmountPayableAmountTextBoxId() {
        return "ctl00_cntMainBody_BC__FAP_MIN_AMT";
    }

    public static String BCFirstAmountPayableMaximumTextBoxId() {
        return "ctl00_cntMainBody_BC__FAP_MAX_AMT";
    }

    public static String BCExtentionsClaimsPreparationLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
    }

    public static String BCExtentionsRiotPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_35";
    }

    public static String BCExtentionsRiotFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_35";
    }

    public static String BCExtentionsRiotFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_35";
    }

    public static String BCExtentionsSubsidenceLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_100";
    }

    public static String BCExtentionsSubsidenceRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_100";
    }

    public static String BCExtentionsSubsidenceFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_100";
    }

    public static String BCExtentionsSubsidenceFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_100";
    }

    public static String AROutstandingDebitBalancesSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_AR__OD_SI";
    }

    public static String AROutstandingDebitBalancesPremiumTextBoxId() {
        return "ctl00_cntMainBody_AR__OD_PREM";
    }

    public static String ARFirstAmountPayableMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_AR__FAP_MIN_PERC";
    }

    public static String ARFirstAmountPayableMinAmountTextBoxId() {
        return "ctl00_cntMainBody_AR__FAP_MIN_AMT";
    }

    public static String ARFirstAmountPayableMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_AR__FAP_MAX_AMT";
    }

    public static String ARExtensionsClaimsLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
    }

    public static String ARExtensionsClaimsPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_25";
    }

    public static String ARExtensionsRiotPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_35";
    }

    public static String ARExtensionsFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_35";
    }

    public static String ARExtensionsFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_35";
    }

    public static String OCCoverOfficeContentsSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_OC__OC_SI";
    }

    public static String EHATOCCoverOfficeContentsSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_136";
    }

    public static String EHATOCCoverOfficeContentsPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_136";
    }

    public static String OCCoverOfficeContentsPremiumTextBoxId() {
        return "ctl00_cntMainBody_OC__OC_PREM";
    }

    public static String OCCoverLossOfDocumentsSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_OC__LOD_SI";
    }

    public static String EHATOCCoverLossOfDocumentsSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_137";
    }

    public static String EHATOCCoverLossOfDocumentsPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_137";
    }

    public static String OCCoverLiabilityForDocumentsSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_OC__LIABDSI";
    }

    public static String EHATOCCoverLiabilityForDocumentsSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_138";
    }

    public static String EHATOCCoverLiabilityForDocumentsPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_138";
    }

    public static String OCCoverTheftNonForcibleSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_OC__THEFTNON_SI";
    }

    public static String EHATOCCoverTheftNonForcibleSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_139";
    }

    public static String EHATOCCoverTheftNonForciblePremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_139";
    }

    public static String OCCoverTheftForcibleSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_OC__THEFT_SI";
    }

    public static String EHATOCCoverTheftForcibleSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_140";
    }

    public static String EHATOCCoverTheftForciblePremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_140";
    }

    public static String EHATOCCoverDebrisRemovalSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_211";
    }

    public static String EHATOCCoverDebrisRemovalPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_211";
    }

    public static String OCFirstAmountPayableOfficeContentsMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_OC__OC_FAP_MIN_PERC";
    }

    public static String EHATOCFirstAmountPayableOfficeContentsMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_MIN_PERC_131";
    }

    public static String OCFirstAmountPayableOfficeContentsMinAmountTextBoxId() {
        return "ctl00_cntMainBody_OC__OC_FAP_MIN_AMT";
    }

    public static String EHATOCFirstAmountPayableOfficeContentsMinAmountTextBoxId() {
        return "ctl00_cntMainBody_MIN_AMT_131";
    }

    public static String OCFirstAmountPayableOfficeContentsMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_OC__OC_FAP_MAX_AMT";
    }

    public static String EHATOCFirstAmountPayableOfficeContentsMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MAX_AMT_131";
    }

    public static String OCFirstAmountPayableLossOfDocumentsMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_OC__LOD_MIN_PERC";
    }

    public static String EHATOCFirstAmountPayableLossOfDocumentsMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_MIN_PERC_132";
    }

    public static String OCFirstAmountPayableLossOfDocumentsMinAmountTextBoxId() {
        return "ctl00_cntMainBody_OC__LOD_MIN_AMT";
    }

    public static String EHATOCFirstAmountPayableLossOfDocumentsMinAmountTextBoxId() {
        return "ctl00_cntMainBody_MIN_AMT_132";
    }

    public static String OCFirstAmountPayableLossOfDocumentsMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_OC__LOD_MAX_AMT";
    }

    public static String EHATOCFirstAmountPayableLossOfDocumentsMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MAX_AMT_132";
    }

    public static String OCFirstAmountPayableLiabilityForDocumentsMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_OC__LIABDMIN_PERC";
    }

    public static String EHATOCFirstAmountPayableLiabilityForDocumentsMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_MIN_PERC_133";
    }

    public static String OCFirstAmountPayableLiabilityForDocumentsMinAmountTextBoxId() {
        return "ctl00_cntMainBody_OC__LIABDMIN_AMT";
    }

    public static String EHATOCFirstAmountPayableLiabilityForDocumentsMinAmountTextBoxId() {
        return "ctl00_cntMainBody_MIN_AMT_133";
    }

    public static String OCFirstAmountPayableLiabilityForDocumentsMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_OC__LIABDMAX_AMT";
    }

    public static String EHATOCFirstAmountPayableLiabilityForDocumentsMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MAX_AMT_133";
    }

    public static String OCFirstAmountPayableTheftNonForcibleMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_OC__THEFTNON_MIN_PERC";
    }

    public static String EHATOCFirstAmountPayableTheftNonForcibleMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_MIN_PERC_134";
    }

    public static String OCFirstAmountPayableTheftNonForcibleMinAmountTextBoxId() {
        return "ctl00_cntMainBody_OC__THEFTNON_MIN_AMT";
    }

    public static String EHATOCFirstAmountPayableTheftNonForcibleMinAmountTextBoxId() {
        return "ctl00_cntMainBody_MIN_AMT_134";
    }

    public static String OCFirstAmountPayableTheftNonForcibleMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_OC__THEFTNON_MAX_AMT";
    }

    public static String EHATOCFirstAmountPayableTheftNonForcibleMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MAX_AMT_134";
    }

    public static String OCFirstAmountPayableTheftForcibleMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_OC__THEFT_MIN_PERC";
    }

    public static String EHATOCFirstAmountPayableTheftForcibleMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_MIN_PERC_135";
    }

    public static String EHATOCFirstAmountPayableDebrisRemovalMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_MIN_PERC_212";
    }

    public static String OCFirstAmountPayableTheftForcibleMinAmountTextBoxId() {
        return "ctl00_cntMainBody_OC__THEFT_MIN_AMT";
    }

    public static String EHATOCFirstAmountPayableTheftForcibleMinAmountTextBoxId() {
        return "ctl00_cntMainBody_MIN_AMT_135";
    }

    public static String EHATOCFirstAmountPayableDebrisRemovalMinAmountTextBoxId() {
        return "ctl00_cntMainBody_MIN_AMT_212";
    }

    public static String OCFirstAmountPayableTheftForcibleMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_OC__THEFT_MAX_AMT";
    }

    public static String EHATOCFirstAmountPayableTheftForcibleMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MAX_AMT_135";
    }

    public static String EHATOCFirstAmountPayableDebrisRemovalMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MAX_AMT_212";
    }

    public static String EHATOCFirstAmountPayableLightningStrikesMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MAX_AMT_213";
    }

    public static String OCExtensionsAdditionalIncreasedCostLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_118";
    }

    public static String OCExtensionsAdditionalIncreasedCostRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_118";
    }

    public static String EHATOCExtensionsAdditionalIncreasedCostRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_128";
    }

    public static String OCExtensionsAdditionalIncreasedCostFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_118";
    }

    public static String EHATOCExtensionsAdditionalIncreasedCostFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_128";
    }

    public static String OCExtensionsAdditionalIncreasedCostFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_118";
    }

    public static String EHATOCExtensionsAdditionalIncreasedCostFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_128";
    }

    public static String OCExtensionsClaimsLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
    }

    public static String OCExtensionsClaimsRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_25";
    }

    public static String OCExtensionsClaimsPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_102";
    }

    public static String EHATOCExtensionsClaimsPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_102";
    }

    public static String OCExtensionsMaliciousDamageLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_119";
    }

    public static String EHATOCExtensionsMaliciousDamageLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_130";
    }

    public static String OCExtensionsMaliciousDamageRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_119";
    }

    public static String EHATOCExtensionsMaliciousDamageRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_130";
    }

    public static String OCExtensionsMaliciousDamageFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_119";
    }

    public static String EHATOCExtensionsMaliciousDamageFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_130";
    }

    public static String OCExtensionsMaliciousDamageFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_119";
    }

    public static String EHATOCExtensionsMaliciousDamageFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_130";
    }

    public static String OCExtensionsRiotPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_35";
    }

    public static String EHATOCExtensionsRiotPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_105";
    }

    public static String OCExtensionsRiotFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_35";
    }

    public static String EHATOCExtensionsRiotFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_105";
    }

    public static String OCExtensionsRiotFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_35";
    }

    public static String EHATOCExtensionsRiotFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMT_105";
    }

    public static String ADCoverAccidentalDamageFullValueTextBoxId() {
        return "ctl00_cntMainBody_AD__EVENT1_FULL_VALUE";
    }

    public static String ADCoverAccidentalDamageFirstLossTextBoxId() {
        return "ctl00_cntMainBody_AD__EVENT1_FIRST_LOSS";
    }

    public static String ADCoverLeakageFirstLossTextBoxId() {
        return "ctl00_cntMainBody_AD__EVENT2_FIRST_LOSS";
    }

    public static String ADFirstAmountPayableAccidentalDamageMinAmountTextBoxId() {
        return "ctl00_cntMainBody_AD__EVENT1_FAP_MIN_AMT";
    }

    public static String GIFirstAmountPayableGreensMinAmountTextBoxId() {
        return "ctl00_cntMainBody_MIN_AMT_217";
    }

    public static String GIFirstAmountPayableIrrigationMinAmountTextBoxId() {
        return "ctl00_cntMainBody_MIN_AMT_218";
    }

    public static String GIFirstAmountPayableClaimsPrepMinAmountTextBoxId() {
        return "ctl00_cntMainBody_MIN_AMT_219";
    }

    public static String ADFirstAmountPayableAccidentalDamageMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_AD__EVENT1_FAP_MIN_PERC";
    }

    public static String GIFirstAmountPayableGreensMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_MIN_PERC_217";
    }

    public static String GIFirstAmountPayableIrrigationMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_MIN_PERC_218";
    }

    public static String GIFirstAmountPayableClaimsPrepMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_MIN_PERC_219";
    }

    public static String GIExtensionsDemolitionSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_223";
    }

    public static String GIExtensionsLossofRevenueSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_222";
    }

    public static String GIExtensionsPublicUtilitiesSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_224";
    }

    public static String GIExtensionsRiotSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_221";
    }

    public static String GIExtensionsSubsidenceSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_220";
    }

    public static String GIExtensionsLossofRevenuePremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_222";
    }

    public static String GIExtensionsPublicUtilitiesPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_224";
    }

    public static String GIExtensionsRiotPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_221";
    }

    public static String GIExtensionsSubsidencePremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_220";
    }

    public static String GIExtensionsDemolitionPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_223";
    }

    public static String ADFirstAmountPayableAccidentalDamageMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_AD__EVENT1_FAP_MAX_AMT";
    }

    public static String GIFirstAmountPayableGreensMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MAX_AMT_217";
    }

    public static String GIFirstAmountPayableIrrigationMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MAX_AMT_218";
    }

    public static String GIFirstAmountPayableClaimsPrepMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MAX_AMT_219";
    }

    public static String ADFirstAmountPayableLeakageMinAmountTextBoxId() {
        return "ctl00_cntMainBody_AD__EVENT2_FAP_MIN_AMT";
    }

    public static String ADFirstAmountPayableLeakageMinPercentageTextBoxId() {
        return "ctl00_cntMainBody_AD__EVENT2_FAP_MIN_PERC";
    }

    public static String ADFirstAmountPayableLeakageMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_AD__EVENT2_FAP_MAX_AMT";
    }

    public static String ADExtensionsAdditionalIncreasedCostLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_118";
    }

    public static String ADExtensionsAdditionalIncreasedCostRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_118";
    }

    public static String ADExtensionsAdditionalIncreasedCostFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_118";
    }

    public static String ADExtensionsAdditionalIncreasedCostFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_118";
    }

    public static String ADExtensionsClaimsPreparationLimitTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
    }

    public static String ADExtensionsClaimsPreparationRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_25";
    }

    public static String ADExtensionsClaimsPreparationPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_25";
    }

    public static String ADMemorandaNotesTextBoxId() {
        return "ctl00_cntMainBody_AD__EXT_EX_PROP_MEMO_COMMENT";
    }

    public static String NotesCommentsTextBoxId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }

    public static String SearchIndustryTextBoxId() {
        return "txtSearch";
    }

    public static String FACReinsurerCodeTextBoxId() {
        return "ctl00_cntMainBody_txtReinsurerCode";
    }

    public static String EveresteReinsurancePercTextBoxId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl03_txtThisPerc";
    }

    public static String CaisseReinsurancePercTextBoxId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl06_txtThisPerc";
    }

    public static String HannoverreReinsurancePercTextBoxId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl04_txtThisPerc";
    }

    public static String MunichReinsurancePercTextBoxId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl05_txtThisPerc";
    }

    public static String MIAEffectiveDateTextBoxId() {
        return "ctl00_cntMainBody_txtEffectiveDate";
    }

    public static String PolicyReferenceTextBoxId() {
        return "ctl00_cntMainBody_txtReference";
    }

    public static String DORetroActiveDateTextBoxId() {
        return "ctl00_cntMainBody_DO__RETROACTIVE_DATE";
    }

    public static String PolicyNumberTextBoxId() {
        return "ctl00_cntMainBody_txtPolicyNumber";
    }

    public static String AgentCodeTextBoxId() {
        return "ctl01_cntMainBody_txtAgent_code";
    }

    public static String LastSurveyDateTextBoxId() {
        return "ctl00_cntMainBody_GROUP_FIRE__LAST_SURVEY_DATE";
    }

    public static String SurveyNumberTextBoxId() {
        return "ctl00_cntMainBody_GROUP_FIRE__SURVEY_NUM";
    }

    public static String DateSurveyRequestedTextBoxId() {
        return "ctl00_cntMainBody_GROUP_FIRE__DATE_SURVEY_REQUESTED";
    }

    public static String BlockedPoliciesClientNameTextBoxId() {
        return "ctl00_cntMainBody_BLOCKED_RISK__CLIENT_NAME";
    }

    public static String BlockedPoliciesPolicyNumberTextBoxId() {
        return "ctl00_cntMainBody_BLOCKED_RISK__POLICY_NUMBER";
    }

    public static String BISumInsuredValueTextBoxId() {
        return "ctl00_cntMainBody_BLOCKED_RISK__BI_SI_AMT";
    }

    public static String GISumInsuredValueTextBoxId() {
        return "ctl00_cntMainBody_BLOCKED_RISK__GI_SI_AMT";
    }

    public static String BCSumInsuredValueTextBoxId() {
        return "ctl00_cntMainBody_BLOCKED_RISK__PC_SI_AMT";
    }

    public static String OCSumInsuredValueTextBoxId() {
        return "ctl00_cntMainBody_BLOCKED_RISK__OC_SI_AMT";
    }

    public static String ARSumInsuredValueTextBoxId() {
        return "ctl00_cntMainBody_BLOCKED_RISK__AR_SI_AMT";
    }

    public static String ADSumInsuredValueTextBoxId() {
        return "ctl00_cntMainBody_BLOCKED_RISK__AD_SI_AMT";
    }

    public static String EHATPCDescriptionTextBoxId() {
        return "ctl00_cntMainBody_PC_PROPERTIES__BLD_DESCRIPTION";
    }

    public static String EHATPCSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_PC_PROPERTIES__BLD_SI";
    }

    public static String EHATPCRateTextBoxId() {
        return "ctl00_cntMainBody_PC_PROPERTIES__BLD_AGREED_RATE";
    }

    public static String EHATPCEscalationPercentageTextBoxId() {
        return "ctl00_cntMainBody_PC_PROPERTIES__BLD_ESC_PERC";
    }

    public static String EHATPCContentsEscalationPercentageTextBoxId() {
        return "ctl00_cntMainBody_PC_PROPERTIES__CNT_ESC_PERC";
    }

    public static String EHATPCMiscItemsEscalationPercentageTextBoxId() {
        return "ctl00_cntMainBody_PC_MISC_ITEMS__ESC_PERC";
    }

    public static String EHATPCInflation1stYearPercentageTextBoxId() {
        return "ctl00_cntMainBody_PC_PROPERTIES__BLD_INF1_PERC";
    }

    public static String EHATPCContentsInflation1stYearPercentageTextBoxId() {
        return "ctl00_cntMainBody_PC_PROPERTIES__CNT_INF1_PERC";
    }

    public static String EHATPCMiscItemsInflation1stYearPercentageTextBoxId() {
        return "ctl00_cntMainBody_PC_MISC_ITEMS__INFL_1YR_PERC";
    }

    public static String EHATPCMISCItemsInflation2ndYearPercentageTextBoxId() {
        return "ctl00_cntMainBody_PC_MISC_ITEMS__INFL_2YR_PERC";
    }

    public static String EHATPCInflation2ndYearPercentageTextBoxId() {
        return "ctl00_cntMainBody_PC_PROPERTIES__BLD_INF2_PERC";
    }

    public static String EHATPCContentsInflation2ndYearPercentageTextBoxId() {
        return "ctl00_cntMainBody_PC_PROPERTIES__CNT_INF2_PERC";
    }

    public static String EHATPCContentsSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_PC_PROPERTIES__CNT_SI";
    }

    public static String EHATPCContentsRateTextBoxId() {
        return "ctl00_cntMainBody_PC_PROPERTIES__CNT_AGREED_RATE";
    }

    public static String EHATPCAdditionalRentSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_PC__ADD_RENT_SI";
    }

    public static String EHATPCAdditionalRentRateTextBoxId() {
        return "ctl00_cntMainBody_PC__ADD_RENT_RATE";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Buttons">
    public static String EHATPCAddFAPButtonName() {
        return "ctl00$cntMainBody$PC__PC_FAPS$ctl02$ctl00";
    }

    public static String AddSpecifiedDriverButtonName() {
        return "ctl00$cntMainBody$MSD__MAIN_DRIVERS$ctl02$ctl00";
    }

    public static String GeneratePDFDocumentButtonId() {
        return "btnGeneratePdf";
    }

    public static String EndorsementsRemoveClauseButtonId() {
        return "ctl00_cntMainBody_MS__ENDORSEMENTS_PckTemplates_RemoveCmd";
    }

    public static String ChangeAddressButtonId() {
        return "changeButton";
    }

    public static String EHATBCADDPropertyButtonName() {
        return "ctl00$cntMainBody$PC__PC_PROPERTIES$ctl02$ctl00";
    }

    public static String EHATBCADDSecondPropertyButtonName() {
        return "ctl00$cntMainBody$PC__PC_PROPERTIES$ctl03$ctl00";
    }

    public static String EHATBCADDThirdPropertyButtonName() {
        return "ctl00$cntMainBody$PC__PC_PROPERTIES$ctl04$ctl00";
    }

    public static String EHATBCADDFourthPropertyButtonName() {
        return "ctl00$cntMainBody$PC__PC_PROPERTIES$ctl05$ctl00";
    }

    public static String EHATBCADDFifthPropertyButtonName() {
        return "ctl00$cntMainBody$PC__PC_PROPERTIES$ctl06$ctl00";
    }

    public static String PartyButtonId() {
        return "ctl00_cntMainBody_PayClaim_ctrl_btnParty";
    }

    public static String RenewalInvitePrintButtonId() {
        return "ctl00_cntMainBody_btnPrint";
    }

    public static String FindNowButtonId() {
        return "ctl00_cntMainBody_btnFindNow";
    }

    public static String RenewalManagerSearchButtonId() {
        return "ctl00_cntMainBody_btnFilter";
    }

    public static String RenewalManagerUpdateStatusButtonId() {
        return "ctl01_cntMainBody_btnUpdateStatus";
    }

    public static String RenewalManagerStatusButtonId() {
        return "ctl00_cntMainBody_btnStatus";
    }

    public static String FACSearchReinsurerButtonId() {
        return "ctl00_cntMainBody_btnSearch";
    }

    public static String SearchCaseButtonId() {
        return "ctl00_cntMainBody_btnSearch";
    }

    public static String AddProcFACButtonId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_btnAddFacProp";
    }

    public static String NewQuoteButtonId() {
        return "ctl00_cntMainBody_ctrlNewQuote_btnNewQuote";
    }

    public static String AgentCodeButtonId() {
        return "ctl00_cntMainBody_btnAgentCode";
    }

    public static String SearchAgentsButtonId() {
        return "ctl01_cntMainBody_btnSearch";
    }

    public static String PaymentDetailsOkButtonId() {
        return "ctl01_cntMainBody_btnOk";
    }

    public static String SearchPartyCodeButtonId() {
        return "ctl01_cntMainBody_btnSearch";
    }
 public static String NextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }
    public static String NextButtonId2() {
        return "ctl00_cntMainBody_btn_Next";
    }

    public static String SpecifiedDriverAddButtonName() {
        return "ctl00$cntMainBody$MS__DRIVER$ctl02$ctl00";
    }

    public static String AddAccessoryItemsButtonName() {
        return "ctl00$cntMainBody$MS__ACCESSORIES$ctl02$ctl00";
    }

    public static String PerilDetailsNextButtonId() {
        return "ctl00_cntMainBody_btn_Next";
    }

    public static String DetailsNextButtonId() {
        return "ctl00$cntMainBody$btn_Next";
    }

    public static String RateButtonId() {
        return "ctl00_cntMainBody_btnRate";
    }

    public static String AddMiscItemButtonName() {
//        if (CurrentEnvironment == Enums.TestEnvironments.QA) {
//            return "ctl00$cntMainBody$FIRE__FI_RISK_MICS_ITEMS$ctl03$ctl00";
//        }

        //ctl00$cntMainBody$FIRE__FI_RISK_MICS_ITEMS$ctl02$ctl00
        return "ctl00$cntMainBody$FIRE__FI_RISK_MICS_ITEMS$ctl02$ctl00";

    }

    public static String BIAddSpecifiedSupplierButtonName() {
        return "ctl00$cntMainBody$BI__SPEC_SUPPLIER$ctl02$ctl00";
    }

    public static String EHATBIAddSpecifiedCustomerButtonName() {
        return "ctl00$cntMainBody$BI__SPEC_CUSTOMER$ctl02$ctl00";
    }

    public static String EHATBIAddSpecifiedSupplierButtonName() {
        return "ctl00$cntMainBody$BI__SPEC_SUPPLIER$ctl02$ctl00";
    }

    public static String BIAddSpecifiedCustomerButtonName() {
        return "ctl00$cntMainBody$BI__SPEC_CUSTOMER$ctl02$ctl00";
    }

    public static String BIRateButtonId() {
        return "ctl00_cntMainBody_btnRate";
    }

    public static String BCAddMiscellaneousItemButtonName() {
        return "ctl00$cntMainBody$BC__BC_ITEMS$ctl02$ctl00";
    }

    public static String ReinsuranceDetailsOKButtonId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_btnOk";
    }

    public static String ReinsuranceDetailsOKButtonId2() {
        return "ctl00_cntMainBody_ctrl_RI2007_btnNext";
    }

    public static String OKButtonId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_btnOk";
    }

    public static String OOSOKButtonId() {
        return "ctl00_cntMainBody_btnOK";
    }

    public static String FinishButtonId() {
        return "ctl00_cntMainBody_btnFinish";
    }

    public static String MakeLiveButtonId() {
        return "ctl00_cntMainBody_btnBuy";
    }

    public static String SaveQuoteButtonId() {
        return "ctl00_cntMainBody_btnSaveQuote";
    }

    public static String SearchIndustrySpanId() {
        return "searchIndustryIcon";
    }

    public static String SearchIndustryButtonId() {
        return "btnIndustrySearch";
    }

    public static String OpenClaimButtonId() {
        return "ctl00_cntMainBody_btnOpen";
    }

    public static String ChangeAddressSecondRowId() {
        return "2";
    }

    public static String IndustrySearchRowId() {
        return "1";
    }

    public static String CopyRiskSubmitButtonId() {
        return "ctl00_cntMainBody_btnSubmit";
    }

    public static String CancelMTAButtonId() {
        return "ctl00_cntMainBody_btnCancelMTA";
    }

    public static String DirectorsandOfficesRiskRateButtonId() {
        return "ctl00_cntMainBody_btnRate";
    }

    public static String BlocledPoliciesAddButtonName() {
        return "ctl00$cntMainBody$GROUP_FIRE__BLOCKED_RISK$ctl02$ctl00";
    }

    public static String PCAddMiscellaneousItemButtonName() {
        return "ctl00$cntMainBody$PC__PC_MISC_ITEMS$ctl02$ctl00";
    }
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="HoverTabs">
// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Tabs">
    public static String ReinsuranceDetailsTabPartialLinkText() {
        return "Reinsurance Details";
    }

    public static String DebitOrdersTabPartialLinkText() {
        return "Debit Orders";
    }

    public static String ThirdPartyTabPartialLinkText() {
        return "Third Party";
    }

    public static String ReceiptDetailsTabPartialLinkText() {
        return "Receipt Details";
    }

    public static String PaymentDetailsTabPartialLinkText() {
        return "Payment Details";
    }

    public static String ThisReceiptTabLinkText() {
        return "This Receipt";
    }

    public static String SelectPartyLinkText() {
        return "select";
    }

    public static String ViewClientDetailsPoliciesTabPartialLinkText() {
        return "Policies";
    }

    public static String ViewClientDetailsQuotesTabPartialLinkText() {
        return "Quotes";
    }

    public static String PolicyHeaderTabId() {
        return "ctl00_cntMainBody_ctrlTabIndex_lbtnMainDetail";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Check Boxes">
    public static String GIFlatPremiumCheckBoxId() {
        return "ctl00_cntMainBody_GI__CVR_FLAT_PREM_APPL";
    }

    public static String RoadsideAssistCheckBoxId() {
        return "ctl00_cntMainBody_MSD__ROAD_ASSIST_APPL";
    }

    public static String GICoverClaimsPrepCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_216";
    }

    public static String SelectRiskPoloVivoCheckBoxId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl12_chkRiskSelect";
    }

    public static String FAPThirdPartyMinCheckBoxId() {
        return "ctl00_cntMainBody_MS__FAP_3RD_PARTY_APPL";
    }

    public static String FAPFire_ExplosionMinCheckBoxId() {
        return "ctl00_cntMainBody_MS__FAP_FI_APPL";
    }

    public static String RenewalManagerCheckPolicy2CheckBoxId() {
        return "ctl00_cntMainBody_grdvRenQuotes_ctl03_chkSelection";
    }

    public static String MotorTradersExternalExtensionsContingentLiabilityCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_254";
    }

    public static String MotorTradersExternalExtensionsLossofKeysCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_255";
    }

    public static String MotorTradersExternalExtensionsLossofUseCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_249";
    }

    public static String MotorTradersExternalExtensionsMotorCycleCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_256";
    }

    public static String MotorTradersExternalExtensionsPassangerLiabilityCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_251";
    }

    public static String MotorTradersExternalExtensionRiotandStrikeCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_262";
    }

    public static String MotorTradersExternalExtensionSocialDomesticCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_248";
    }

    public static String MotorTradersExternalExtensionSpecialTypeVehiclesCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_257";
    }

    public static String MotorTradersExternalExtensionUnauthorisedUseCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_250";
    }

    public static String MotorTradersExternalExtensionVehicleLentCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_253";
    }

    public static String MotorTradersExternalExtensionWindscreenCoverCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_252";
    }

    public static String MotorTradersExternalExtensionWreckageRemovalCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_261";
    }

    public static String MotorTradersExtensionsCarHoistsCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_264";
    }

    public static String MotorTradersExtensionsLossofKeysCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_265";
    }

    public static String MotorTradersExtensionsWindscreenCoverCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_252";
    }

    public static String MotorTradersExtensionsWorkAwayCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_263";
    }

    public static String MotorTradersExtensionsWreckageRemovalCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_261";
    }

    public static String SubSectionAThirdPartyOnlyCheckBoxId() {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_TYPE_OF_WAGES";
    }

    public static String SubSectionAFireAndTheftCheckBoxId() {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_THIRD_PARTY_FIRE_THEFT";
    }

    public static String SubSectionADeletionOfDemonstrationCheckBoxId() {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_DELETION_OF_DEMONSTRATION";
    }

    public static String SubSectionAExclusionOfOwnVehiclesCheckBoxId() {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_EXCLUSION_OWN_VEH";
    }

    public static String CoverVoluntarySubsectionBCheckBoxId() {
        return "ctl00_cntMainBody_MT__SUB_SECTION_B_APPL_LIABILITY_THIRD_PARTIES";
    }

    public static String CoverVoluntarySubsectionCCheckBoxId() {
        return "ctl00_cntMainBody_MT__SUB_SECTION_C_MEDICAL_APPL";
    }

    public static String RiskItemFlatPremiumCheckBoxId() {
        return "ctl00_cntMainBody_GIT__RATING_FLAT_PREM_APPL";
    }

    public static String RenewalManagerCheckPolicyCheckBoxId() {
        return "ctl00_cntMainBody_grdvRenQuotes_ctl02_chkSelection";
    }

    public static String ViewAllPoliciesCheckboxId() {
        return "ctl00_cntMainBody_ClientPolicies_chkViewAllPolicies";
    }

    public static String RSFireSectionsFireCheckBoxId() {
        return "ctl00_cntMainBody_GROUP_FIRE__SECTION_FIRE_APPL";
    }

    public static String RSFireSectionsRiskBlockedCheckBoxId() {
        return "ctl00_cntMainBody_GROUP_FIRE__RISK_BLOCKED_APPL";
    }

    public static String RSFireSectionsBusinessInterruptionCheckBoxId() {
        return "ctl00_cntMainBody_GROUP_FIRE__SECTION_BI_APPL";
    }

    public static String RSFireSectionsBuildingsCombinedCheckBoxId() {
        return "ctl00_cntMainBody_GROUP_FIRE__SECTION_BC_APPL";
    }

    public static String RSFireSectionsPropertyCombinedCheckBoxId() {
        return "ctl00_cntMainBody_GROUP_FIRE__SECTION_PC_APPL";
    }

    public static String RSFireSectionsOfficeContentsCheckBoxId() {
        return "ctl00_cntMainBody_GROUP_FIRE__SECTION_OC_APPL";
    }

    public static String OfficeContentsFlatPremiumCheckBoxId() {
        return "ctl00_cntMainBody_OC__OC_FLAT_PREM_APPL";
    }

    public static String RSFireSectionsAccountsRecievableCheckBoxId() {
        return "ctl00_cntMainBody_GROUP_FIRE__SECTION_AR_APPL";
    }

    public static String RSFireSectionsAccidentalDamageCheckBoxId() {
        return "ctl00_cntMainBody_GROUP_FIRE__SECTION_AD_APPL";
    }

    public static String RSFireSectionGreensandIrrigationCheckBoxId() {
        return "ctl00_cntMainBody_GROUP_FIRE__SECTION_GI_APPL";
    }

    public static String FireRiskDataBuildingsCheckBoxId() {
        return "ctl00_cntMainBody_FIRE__COL1_APPL";
    }

    public static String FireRiskDataRentCheckBoxId() {
        return "ctl00_cntMainBody_FIRE__COL2_APPL";
    }

    public static String FireOverviewFlatPremiumCheckBoxId() {
        return "ctl00_cntMainBody_FIRE__FIRE_FLAT_PREM_APPL";
    }

    public static String BIOverviewFlatPremiumCheckBoxId() {
        return "ctl00_cntMainBody_BI__BI_FLAT_PREM_APPL";
    }

    public static String FireRiskDataPlantandMachCheckBoxId() {
        return "ctl00_cntMainBody_FIRE__COL3_APPL";
    }

    public static String FireRiskDataStockCheckBoxId() {
        return "ctl00_cntMainBody_FIRE__COL4_APPL";
    }

    public static String FireRiskDataMiscCheckBoxId() {
        return "ctl00_cntMainBody_FIRE__COL5_APPL";
    }

    public static String FireBuildingsExcalationCheckBoxId() {
        return "ctl00_cntMainBody_FIRE__COL1_INFL_APPL";
    }

    public static String FirePlantMachineryExcalationCheckBoxId() {
        return "ctl00_cntMainBody_FIRE__COL3_INFL_APPL";
    }

    public static String FireStockDeclarationCheckBoxId() {
        return "ctl00_cntMainBody_FIRE__COL4_DECLARATION_APPL";
    }

    public static String FireFirstAmountPayableCheckBoxId() {
        return "ctl00_cntMainBody_FIRE__FAP_APPL";
    }

    public static String EHATFireFirstAmountPayableCheckBoxId() {
        return "ctl00_cntMainBody_PC_MISC_ITEMS__FAP_APPL";
    }

    public static String EHATFlatPremiumCheckBoxId() {
        return "ctl00_cntMainBody_PC__FLAT_PREM_APPL";
    }

    public static String FireClaimsPreparationCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_25";
    }

    public static String EHATPCClaimsPreparationCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_102";
    }

    public static String FireDisposalOfSalvageCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_102";
    }

    public static String EHATPreventionofAccessCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_289";
    }

    public static String FireLeakageCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_101";
    }

    public static String FireRiotAndStrikeCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_35";
    }

    public static String PCRiotAndStrikeCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_275";
    }

    public static String FireSubsidenceAndLandslipCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_100";
    }

    public static String PCSubsidenceAndLandslipCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_106";
    }

    public static String BICoverGrossProfitBasisCheckBoxId() {
        return "ctl00_cntMainBody_BI__GROSS_PROFIT_BASIS_APPL";
    }

    public static String EHATBICoverGrossProfitBasisCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_144";
    }

    public static String BICoverGrossRentalsCheckBoxId() {
        return "ctl00_cntMainBody_BI__GROSS_RENTALS_APPL";
    }

    public static String EHATBICoverGrossRentalsCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_145";
    }

    public static String BICoverRevenueCheckBoxId() {
        return "ctl00_cntMainBody_BI__REVENUE_APPL";
    }

    public static String EHATBIDepositAndDeclarationCheckBoxId() {
        return "ctl00_cntMainBody_BI__DEP_DECL_APPL";
    }

    public static String EHATBICoverRevenueCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_146";
    }

    public static String BICoverAdditionalIncreaseCostOfWorkingCheckBoxId() {
        return "ctl00_cntMainBody_BI__ICOW_APPL";
    }

    public static String EHATBICoverAdditionalIncreaseCostOfWorkingCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_147";
    }

    public static String BICoverWagesCheckBoxId() {
        return "ctl00_cntMainBody_BI__WAGES_APPL";
    }

    public static String EHATBICoverLossofLeviesCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_204";
    }

    public static String EHATBICoverLossofTouristAttractionCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_205";
    }

    public static String BICoverLossofLeviesCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_204";
    }

    public static String BIFirstAmountPayableCheckBoxId() {
        return "ctl00_cntMainBody_BI__FAP_APPL";
    }

    public static String BIExtensionsAccidentalDamageCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_108";
    }

    public static String EHATBIExtensionsAccidentalDamageCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_107";
    }

    public static String EHATBIExtensionsAdditionalPermisesClauseCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_225";
    }

    public static String BIExtensionsClaimsPreparationCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_25";
    }

    public static String EHATBIExtensionsClaimsPreparationCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_102";
    }

    public static String BIExtensionsDisposalOfSalvageCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_102";
    }

    public static String EHATBIExtensionsDisposalOfSalvageCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_103";
    }

    public static String BIExtensionsFinesAndPenaltiesCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_107";
    }

    public static String EHATBIExtensionsFinesAndPenaltiesCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_108";
    }

    public static String BIExtensionsPreventionOfAccessCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_105";
    }

    public static String EHATBIExtensionsPreventionOfAccessCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_281";
    }

    public static String BIExtensionsPublicTelecommunicationsExtendedCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_104";
    }

    public static String EHATBIExtensionsPublicTelecommunicationsExtendedCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_282";
    }

    public static String BIExtensionsPublicUtilitiesExtendedCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_113";
    }

    public static String EHATBIExtensionsPublicUtilitiesExtendedCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_284";
    }

    public static String BIExtensionsSpecifiedCustomersCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_112";
    }

    public static String EHATBIExtensionsSpecifiedCustomersCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_286";
    }

    public static String BIExtensionsSpecifiedSuppliersCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_110";
    }

    public static String EHATBIExtensionsSpecifiedSuppliersCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_287";
    }

    public static String BIExtensionsUnSpecifiedCustomersCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_111";
    }

    public static String EHATBIExtensionsUnSpecifiedCustomersCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_288";
    }

    public static String BIExtensionsUnSpecifiedSuppliersCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_109";
    }

    public static String EHATBIExtensionsUnSpecifiedSuppliersCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_117";
    }

    public static String BCBuildingsCombinedEscalationInflationCheckBoxId() {
        return "ctl00_cntMainBody_BC__BC_ESC";
    }

    public static String BCMiscellaneousItemsOverviewCheckBoxId() {
        return "ctl00_cntMainBody_BC__MISC_ITEMS_APPL";
    }

    public static String BCAdditionalRentCheckBoxId() {
        return "ctl00_cntMainBody_BC__ADD_RENT_APPL";
    }

    public static String BCFirstAmountPayableCheckBoxId() {
        return "ctl00_cntMainBody_BC__FAP_APPL";
    }

    public static String BCExtentionsClaimsPreparationCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_25";
    }

    public static String BCExtentionsPreventionOfAccessCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_125";
    }

    public static String BCExtentionsRiotCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_35";
    }

    public static String BCExtentionsSubsidenceCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_100";
    }

    public static String AROutstandingDebitBalancesCheckBoxId() {
        return "ctl00_cntMainBody_AR__OD_APPL";
    }

    public static String EHATARFlatPremiumCheckBoxId() {
        return "ctl00_cntMainBody_AR__AR_FLAT_PREM_APPL";
    }

    public static String ARFirstAmountPayableCheckBoxId() {
        return "ctl00_cntMainBody_AR__FAP_APPL";
    }

    public static String ARExtensionsClaimsCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_25";
    }

    public static String ARExtensionsDuplicateRecordsCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_115";
    }

    public static String ARExtensionsProtectionsCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_116";
    }

    public static String ARExtensionsRiotCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_35";
    }

    public static String ARExtensionsTransitCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_117";
    }

    public static String OCCoverOfficeContentsCheckBoxId() {
        return "ctl00_cntMainBody_OC__OC_APPL";
    }

    public static String EHATOCCoverOfficeContentsCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_136";
    }

    public static String OCFlatPremiumCheckBoxId() {
        return "ctl00_cntMainBody_OC__OC_FLAT_PREM_APPL";
    }

    public static String OCCoverLossOfDocumentsCheckBoxId() {
        return "ctl00_cntMainBody_OC__LOD_APPL";
    }

    public static String EHATOCCoverLossOfDocumentsCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_137";
    }

    public static String OCCoverLiabilityForDocumentsCheckBoxId() {
        return "ctl00_cntMainBody_OC__LIAB_DOCS_APPL";
    }

    public static String EHATOCCoverLiabilityForDocumentsCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_138";
    }

    public static String OCCoverTheftNonForcibleCheckBoxId() {
        return "ctl00_cntMainBody_OC__THEFT_NON_FORCIBLE_APPL";
    }

    public static String EHATOCCoverTheftNonForcibleCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_139";
    }

    public static String OCCoverTheftByForcibleEntryCheckBoxId() {
        return "ctl00_cntMainBody_OC__THEFT_FORCIBLE_APPL";
    }

    public static String EHATOCCoverTheftByForcibleEntryCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_140";
    }

    public static String EHATOCCoverDebrisRemovalCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_211";
    }

    public static String OCFirstAmountPayableOfficeContentsCheckBoxId() {
        return "ctl00_cntMainBody_OC__OC_FAP_APPL";
    }

    public static String EHATOCFirstAmountPayableOfficeContentsCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_131";
    }

    public static String OCFirstAmountPayableLossofDocumentsCheckBoxId() {
        return "ctl00_cntMainBody_OC__LOF_FAP_APPL";
    }

    public static String EHATOCFirstAmountPayableLossofDocumentsCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_132";
    }

    public static String OCFirstAmountPayableLiabilityForDocumentsCheckBoxId() {
        return "ctl00_cntMainBody_OC__LIABDFAP_APPL";
    }

    public static String EHATOCFirstAmountPayableLiabilityForDocumentsCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_133";
    }

    public static String OCFirstAmountPayableTheftNonForcibleCheckBoxId() {
        return "ctl00_cntMainBody_OC__THEFTNON_FAP_APPL";
    }

    public static String EHATOCFirstAmountPayableTheftNonForcibleCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_134";
    }

    public static String OCFirstAmountPayableTheftByForcibleEntryCheckBoxId() {
        return "ctl00_cntMainBody_OC__FAP_THEFT_APPL";
    }

    public static String EHATOCFirstAmountPayableTheftByForcibleEntryCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_135";
    }

    public static String EHATOCFirstAmountPayableDebrisRemovalCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_212";
    }

    public static String EHATOCFirstAmountPayableLightningStrikesCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_213";
    }

    public static String OCExtentionsAdditionalIncresedCostOfWorkingCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_118";
    }

    public static String OCExtentionsClaimsPreparationCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_25";
    }

    public static String OCExtentionsFirstLossAverageCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_120";
    }

    public static String EHATOCExtentionsFirstLossAverageCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_129";
    }

    public static String OCExtentionsMaliciousDamageCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_119";
    }

    public static String EHATOCExtentionsMaliciousDamageCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_130";
    }

    public static String OCExtentionsRiotCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_35";
    }

    public static String EHATOCExtentionsRiotCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_105";
    }

    public static String ADCoverAccidentalDamageCheckBoxId() {
        return "ctl00_cntMainBody_AD__EVENT1_APPL";
    }

    public static String ADCoverAccidentalDamageAverageCheckBoxId() {
        return "ctl00_cntMainBody_AD__EVENT1_AVG_APPL";
    }

    public static String ADCoverLeakageAverageCheckBoxId() {
        return "ctl00_cntMainBody_AD__EVENT1_AVG_APPL";
    }

    public static String ADFlatPremiumCheckBoxId() {
        return "ctl00_cntMainBody_AD__AD_FLAT_PREM_APPL";
    }

    public static String ADCoverLeakageCheckBoxId() {
        return "ctl00_cntMainBody_AD__EVENT2_APPL";
    }

    public static String ADFirstAmountPayableAccidentalDamageCheckBoxId() {
        return "ctl00_cntMainBody_AD__EVENT1_FAP_APPL";
    }

    public static String GIFirstAmountPayableGreensCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_217";
    }

    public static String GIFirstAmountPayableIrrigationCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_217";
    }

    public static String GIFirstAmountPayableClaimsPrepCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_219";
    }

    public static String GIExtensionsDemolitionCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_223";
    }

    public static String GIExtensionsLossofRevenueCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_222";
    }

    public static String GIExtensionsPublicUtilitiesCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_224";
    }

    public static String GIExtensionsRiotCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_221";
    }

    public static String GIExtensionsSubsidenceCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_220";
    }

    public static String ADFirstAmountPayableLeakageCheckBoxId() {
        return "ctl00_cntMainBody_AD__EVENT2_FAP_APPL";
    }

    public static String ADExtensionsAdditionalIncreasedCostOfWorkingCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_118";
    }

    public static String ADExtensionsClaimsPreparationCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_25";
    }

    public static String ADMemorandaReinstatementCheckBoxId() {
        return "ctl00_cntMainBody_AD__EXT_REINST_MEMO_APPL";
    }

    public static String ADMemorandaFirstLossAverageCheckBoxId() {
        return "ctl00_cntMainBody_AD__EXT_FLA_MEMO_APPL";
    }

    public static String ADMemorandaExcludedPropertyCheckBoxId() {
        return "ctl00_cntMainBody_AD__EXT_EX_PROP_MEMO_APPL";
    }

    public static String DORiskNoChangesCheckBoxId() {
        return "ctl00_cntMainBody_DO__CHECK_NO_CHANGES";
    }

    public static String DORiskIsSolventCheckBoxId() {
        return "ctl00_cntMainBody_DO__CHECK_IS_SOLVENT";
    }

    public static String DORiskOpInSaCheckBoxId() {
        return "ctl00_cntMainBody_DO__CHECK_OP_IN_SA";
    }
    
    public static String MotorFleetFlatPremiumCheckBoxId() {
        return "ctl00_cntMainBody_MF__FLAT_PREM_APPL";
    }

    public static String DORiskNotListedCheckBoxId() {
        return "ctl00_cntMainBody_DO__CHECK_NOT_LISTED";
    }

    public static String DORiskCompanyTypeCheckBoxId() {
        return "ctl00_cntMainBody_DO__CHECK_COMPANY_TYPE";
    }

    public static String DORiskClaimsCheckBoxId() {
        return "ctl00_cntMainBody_DO__CHECK_CLAIMS";
    }

    public static String DORiskDirectorCheckBoxId() {
        return "ctl00_cntMainBody_DO__CHECK_DIRECTOR";
    }
    
    public static String RetroActiveDateCheckBoxId() {
        return "ctl00_cntMainBody_DO__RETROACTIVE_DATE_CHECK";
    }

    public static String SuveryReportApplicableCheckBoxId() {
        return "ctl00_cntMainBody_GROUP_FIRE__SR_RECEIVED";
    }

    public static String BlockedRisksBICheckBoxId() {
        return "ctl00_cntMainBody_BLOCKED_RISK__BI_APPL";
    }

    public static String BlockedRisksBCCheckBoxId() {
        return "ctl00_cntMainBody_BLOCKED_RISK__PC_APPL";
    }

    public static String BlockedRisksOCCheckBoxId() {
        return "ctl00_cntMainBody_BLOCKED_RISK__OC_APPL";
    }

    public static String BlockedRisksARCheckBoxId() {
        return "ctl00_cntMainBody_BLOCKED_RISK__AR_APPL";
    }

    public static String BlockedRisksADCheckBoxId() {
        return "ctl00_cntMainBody_BLOCKED_RISK__AD_APPL";
    }

    public static String BlockedRisksGICheckBoxId() {
        return "ctl00_cntMainBody_BLOCKED_RISK__GI_APPL";
    }

    public static String EHATMiscellaneousItemsFlatPremiumCheckBoxId() {
        return "ctl00_cntMainBody_PC_MISC_ITEMS__FLAT_PREM_APPL";
    }

    public static String EHATBuildingsEscalationorInflationCheckBoxId() {
        return "ctl00_cntMainBody_PC_PROPERTIES__BLD_INF_APPL";
    }

    public static String EHATPCContentsCheckBoxId() {
        return "ctl00_cntMainBody_PC_PROPERTIES__CNT_APPL";
    }

    public static String EHATContentsEscalationorInflationCheckBoxId() {
        return "ctl00_cntMainBody_PC_PROPERTIES__CNT_INF_APPL";
    }

    public static String EHATMiscellaneousItemsEscalationorInflationCheckBoxId() {
        return "ctl00_cntMainBody_PC_MISC_ITEMS__INFL_APPL";
    }

    public static String EHATPCMiscellaneousItemsOverviewCheckBoxId() {
        return "ctl00_cntMainBody_PC__MISC_ITEMS_APPL";
    }

    public static String EHATPCAdditionalRentCheckBoxId() {
        return "ctl00_cntMainBody_PC__ADD_RENT_APPL";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Iframes">
    public static String FindAgentDialogFrameId() {
        return "modalDialog";
    }

    public static String DocumentEditorFrameId() {
        return "ctl00_cntMainBody_txtDocumentEditor_ifr";
    }

    public static String RenewalStatusFrameId() {
        return "modalDialog";
    }

    public static String AddMiscellaneousItemsDialogFrameId() {
        return "modalDialog";
    }

    public static String SelectRiskTypeDialogFrameId() {
        return "modalDialog";
    }

    public static String CopyRiskTypeDialogFrameId() {
        return "modalDialog";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Links">
    public static String ChangePolicyLinkText() {
        return "Change";
    }

    public static String DeleteDomesticAndPropertyLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl06_lnkbtnDelete";
    }

    public static String DocumentsLinkText() {
        return "Documents";
    }

    public static String DeleteElectronicEquipmentLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnDelete";
    }

    public static String ClientInfoLinkId() {
        return "ctl00_ClientInfoCtrl_hypCompanyName";
    }

    public static String PersonalClientInfoLinkId() {
        return "ctl00_ClientInfoCtrl_hypClientName";
    }

    public static String RecoveryLinkId() {
        return "ctl00_cntMainBody_grdvLinkedClaims_ctl03_btnTPRecovery";
    }

    public static String SalvageClaimLinkId() {
        return "ctl00_cntMainBody_grdvLinkedClaims_ctl02_btnSalvage";
    }

    public static String ClaimPayLinkId() {
        return "ctl00_cntMainBody_grdvLinkedClaims_ctl03_btnPay";
    }

    public static String PerilsSalvageLinkId() {
//        return "ctl00_cntMainBody_perils_ctrl_grdvPerils_ctl32_lnkSalvageClaim";
        return "ctl00_cntMainBody_perils_ctrl_grdvPerils_ctl33_lnkSalvageClaim";

    }

    public static String PerilsTPRecoveryLinkId() {

//        return "ctl00_cntMainBody_perils_ctrl_grdvPerils_ctl32_lnkTPRecovery";
        return "ctl00_cntMainBody_perils_ctrl_grdvPerils_ctl33_lnkTPRecovery";

    }

    public static String EditPartyLinkId() {
        return "ctl00_cntMainBody_PayClaim_ctrl_gvSalvageDetails_ctl02_hypEditPayment";

    }

    public static String EditParty1LinkId() {
        return "ctl00_cntMainBody_PayClaim_ctrl_gvPaymentDetails_ctl02_hypEditPayment";

    }

    public static String EditCaseLinkId() {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_btnEdit";
    }

    public static String DeleteMotor3LinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnDelete";
    }

    public static String EditBackDatedMTALinkId() {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl10_lnkbtnSelect";
    }

    public static String EditSASRIARoadsideAssistMTALinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnEdit";
    }

    public static String EditDomesticandPropertyRiskLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnEdit";
    }

    public static String SelecMotorTradersInternalRikTypeLinkId() {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl10_lnkbtnSelect";
    }

    public static String SelecMotorTradersExternalRikTypeLinkId() {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl11_lnkbtnSelect";
    }

    public static String SelectAgentInResultsId() {
        return "ctl01_cntMainBody_grdvSearchResults_ctl02_lnkbtnSelect";
    }

    public static String DeleteElectronicEquipmentId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnDelete";
    }

    public static String RenewalSelectionSelectPolicyLinkId() {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_lnkSelect";
    }

    public static String BackdatedEditPolicy1LinkId() {
        return "ctl00_cntMainBody_grdBackdatedMTA_ctl03_lnkbtnEdit";
    }

    public static String BackdatedEditPolicy2LinkId() {
        return "ctl00_cntMainBody_grdBackdatedMTA_ctl04_lnkbtnEdit";
    }

    public static String BackdatedEditPolicy3LinkId() {
        return "ctl00_cntMainBody_grdBackdatedMTA_ctl05_lnkbtnEdit";
    }

    public static String BackdatedEditPolicy4LinkId() {
        return "ctl00_cntMainBody_grdBackdatedMTA_ctl06_lnkbtnEdit";
    }

    public static String BackdatedEditPolicy5LinkId() {
        return "ctl00_cntMainBody_grdBackdatedMTA_ctl07_lnkbtnEdit";
    }

    public static String BackdatedEditPolicy6LinkId() {
        return "ctl00_cntMainBody_grdBackdatedMTA_ctl08_lnkbtnEdit";
    }

    public static String BackdatedEditPolicy7LinkId() {
        return "ctl00_cntMainBody_grdBackdatedMTA_ctl09_lnkbtnEdit";
    }

    public static String BackdatedEditPolicy8LinkId() {
        return "ctl00_cntMainBody_grdBackdatedMTA_ctl0_lnkbtnEdit";
    }

    public static String DeleteGoodsInTransitLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl06_lnkbtnDelete";
    }

    public static String DeleteGoodsInTransitStep14LinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnDelete";
    }

    public static String DeleteBusinessAllRiskLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnDelete";
    }

    public static String RenewalSelectionSelectPolicyResultsId() {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_lnkSelect";
    }

    public static String ReinstatePolicyLinkText() {
        return "Reinstatement";
    }

    public static String ViewClientDetailsEditQuoteLinkId() {
        return "ctl00_cntMainBody_ctrlClientQuotes_grdvQuotes_ctl02_lnkbtnSelect";
    }

    public static String ViewClientDetailsChangePolicyLinkId() {
        return "ctl00_cntMainBody_ClientPolicies_grdvQuotes_ctl03_lnkbtnSelect";
    }

    public static String DeleteGroupedFireRisk3LinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl05_lnkbtnDelete";
    }

    public static String DeleteGroupedFireRisk2LinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl04_lnkbtnDelete";
    }

    public static String DeleteGroupedFireRisk1LinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnDelete";
    }

    public static String DeleteWaterandPleasureCraftLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl04_lnkbtnDelete";
    }

    public static String DeleteEveLinkId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl03_lnkDelete";
    }

    public static String DeleteHannLinkId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl04_lnkDelete";
    }

    public static String DeleteMunichLinkId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl05_lnkDelete";
    }

    public static String SelectRiskTypeResultsId() {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl03_lnkbtnSelect";
    }

    public static String SelectEHATGroupedFireRiskId() {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl10_lnkbtnSelect";
    }

    public static String editGroupedFireRiskLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl06_lnkbtnEdit";
    }

    public static String editGroupedFireRisk1LinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl05_lnkbtnEdit";
    }

    public static String editGroupedFireRisk4LinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnEdit";
    }

    public static String editGroupedFireRisk5LinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl05_lnkbtnEdit";
    }

    public static String editGroupedFireRisk6LinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl06_lnkbtnEdit";
    }

    public static String editGroupedFireRisk7LinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnEdit";
    }

    public static String editGroupedFireRisk2LinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnEdit";
    }

    public static String editGroupedFireRisk3LinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl04_lnkbtnEdit";
    }

    public static String editElectronicEquipmentLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnEdit";
    }

    public static String editElectronicEquipment2LinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl06_lnkbtnEdit";
    }

    public static String copyGroupedFireRiskLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl06_lnkbtnCopy";
    }

    public static String SelectRoadSideAssistQuoteEditLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnEdit";
    }

    public static String FACSelectReinsurerLinkId() {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_btnSelect";
    }

    public static String FACSelectMunichReinsurerLinkId() {
        return "ctl00_cntMainBody_grdvSearchResults_ctl03_btnSelect";
    }

    public static String FACSelectCaisseReinsurerLinkId() {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_btnSelect";
    }

    public static String editBusinessAllRiskLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl05_lnkbtnEdit";
    }

    public static String editElectronicEquipmentRiskLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnEdit";
    }

    public static String editMoneyRiskLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl04_lnkbtnEdit";
    }

    public static String editGoodsInTransitLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnEdit";
    }

    public static String selectEBPDORiskLinkId() {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl09_lnkbtnSelect";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Drop Down Lists">
    public static String DeleteWaterAndPleasureCraftDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[4]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String EndorsementsClauseSelecionDropdownListXpath() {
        return "ctl00_cntMainBody_MS__ENDORSEMENTS_PckTemplates_CurrentList";
    }

    public static String EditRollsRoyceDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[8]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String EditPoloDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[3]/td[12]/select[@id = \"actionDDL\"]";

    }

    public static String DeleteCopyofPoloVivoDropdownListXpath() {             //div[@id = "DataTables_Table_0_wrapper"]/table/tbody/tr[11]/td[12]/select[@id = "actionDDL"] 
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[3]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String SelectRiskCheckboxXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[11]/td[1]/span/input";
    }

    //div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[11]/td[12]/select[@id = \"actionDDL\"]
    public static String RiskActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String DAPRiskActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[2]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String BusinessAllRiskMonthlyActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[2]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String DomesticandPropertyRiskActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[2]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String BuildingsAndContentsRiskActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[3]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String DomesticandProperty2RiskActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[7]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String PersonalAccidentRiskActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[3]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String EnrouteRiskActionDropdownListXpath() {
        //div[@id = "DataTables_Table_0_wrapper"]/table/tbody/tr[4]/td[12]/select[@id = "actionDDL"])
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[4]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String WaterandPleasureCraftRiskActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[5]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String MotorRiskActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[6]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String Motor2RiskActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[8]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String BusinessAllRiskActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[4]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String GroupedFireRiskActionDropdownListXpath() {
     //div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[5]/td[12]/select[@id = \"actionDDL\"]"
     return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[5]/td[12]/select[@id = \"actionDDL\"]";
        //return "//*[@id=\"actionDDL\"]/option[3]";
    }

    public static String ElectronicEquipmentRiskActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[6]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String GoodsInTransitRiskActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[2]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String ElectronicEquipmentRiskStep13ActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[5]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String GroupedFireRiskDeleteActionDropdownListXpath() {

        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr/td[12]/select[@id = \"actionDDL\"]";

    }

    public static String Motor3RiskDeleteActionDropdownListXpath() {

        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[7]/td[12]/select[@id = \"actionDDL\"]";

    }

    public static String GroupedFireRisk2DeleteActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[3]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String GroupedFireRisk3DeleteActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[4]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String GroupedFireRisk4DeleteActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[6]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String MotorSpecifiedRisk1DeleteActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[2]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String GroupedFireRisk5DeleteActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[7]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String GoodsinTransitActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[2]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String MoneyRiskActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[3]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String GroupedFireRisk3ActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[5]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String GroupedFireRisk5ActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[4]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String GroupedFireRisk4ActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[2]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String GroupedFireRiskStep13ActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[6]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String GroupedFireRiskStep7ActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[7]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String ElectronicEquipment2Risk2ActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[5]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String GoodsinTransitDeleteActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[2]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String GroupedFireRiskDelete2ActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[10]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String GroupedFireRiskDelete1ActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[11]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String GroupedFireRiskDelete3ActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[9]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String GroupedFireRiskEditActionDropdownListXpath() {
        return "//div[@id = \"DataTables_Table_0_wrapper\"]/table/tbody/tr[8]/td[12]/select[@id = \"actionDDL\"]";
    }

    public static String BankAccountDropDownListId() {
        return "ddlBA";
    }

    public static String ReinsuranceBandDropDownListId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_ddlReinsurance";
    }

    public static String LiabilityReinsuranceBandDropDownListId() {
        return "ctl00_cntMainBody_ctrl_RI2007_ddlReinsurance";
    }

    public static String BusinessSourceDropDownListId() {
        return "ctl00_cntMainBody_POLICYHEADER__BUSINESSTYPE";
    }

    public static String GIExtensionsLossofRevenueTypeofCoverDropdownlistId() {
        return "ctl00_cntMainBody_COVER_TYPE_222";
    }

    public static String PaymentDetailsTaxGroupDropDownListId() {
        return "ctl01_cntMainBody_ddlTaxGroup";
    }

    public static String MediaRefDropDownListId() {
        return "ctl00_cntMainBody_PayClaim_ctrl_ddlMediaType";
    }

    public static String SelectProductDropDownListId() {
        return "ctl00_cntMainBody_ctrlNewQuote_ddlProductlst";
    }

    public static String RenewalManagerRenewalStatusDropDownListId() {
        return "ctl00_cntMainBody_RenewalStatusType";
    }

    public static String RenewalStatusDropDownListId() {
        return "ctl01_cntMainBody_RenewalStatusType";
    }

    public static String RenewalManagerProductTypeDropDownListId() {
        return "ctl00_cntMainBody_ddlProductType";
    }

    public static String RenewalManagerBranchDropDownListId() {
        return "ctl00_cntMainBody_BranchCode";
    }

    public static String MTATypeofChangeDropDownListId() {
        return "ddlMTAs";
    }

    public static String RiskItemBasisofCoverDropDownListId() {
        return "ctl00_cntMainBody_GIT__BASIS_COVER";
    }

    public static String PaymentTermDropDownListId() {
        return "ctl00_cntMainBody_POLICYHEADER__PAYMENTTERM";
    }

    public static String CollectionFrequencyDropDownListId() {
        return "ctl00_cntMainBody_POLICYHEADER__COLLECTIONFREQUENCY";
    }

    public static String ManualReinsuranceLimitDropDownListId() {
        return "ctl00_cntMainBody_GROUP_FIRE__OVERRIDE_RI_LIMIT_NR";
    }

    public static String LoadingandDiscountCriteria3YearLossRatioDropDownListId() {
        return "ctl00_cntMainBody_GROUP_FIRE__THREE_YR_LOSS_RATIO";
    }

    public static String LoadingandDiscountCriteriaHOMarketAdjustmentDropDownListId() {
        return "ctl00_cntMainBody_GROUP_FIRE__HO_MKT_ADJUSTMENT";
    }

    public static String LoadingandDiscountCriteriaBranchMarketAdjustmentDropDownListId() {
        return "ctl00_cntMainBody_GROUP_FIRE__BRANCH_MKT_ADJUSTMENT";
    }

    public static String LoadingandDiscountCriteriaMaliciousDamageDropDownListId() {
        return "ctl00_cntMainBody_GROUP_FIRE__MALICIOUS_DAMAGE";
    }

    public static String LoadingandDiscountCriteriaSpecialPerilsDropDownListId() {
        return "ctl00_cntMainBody_GROUP_FIRE__SPECIAL_PERILS";
    }

    public static String LoadingandDiscountCriteriaSCIDropDownListId() {
        return "ctl00_cntMainBody_GROUP_FIRE__SCI";
    }

    public static String LoadingandDiscountCriteriaFEADropDownListId() {
        return "ctl00_cntMainBody_GROUP_FIRE__FEA";
    }

    public static String LoadingandDiscountCriteriaLCEDropDownListId() {
        return "ctl00_cntMainBody_GROUP_FIRE__LCE";
    }

    public static String TypeOfConstructionDropDownListId() {
        return "ctl00_cntMainBody_GROUP_FIRE__TYPE_CONSTRUCTION";
    }

    public static String FireSubsidenceCoverTypeDropDownListId() {
        return "ctl00_cntMainBody_COVER_100";
    }

    public static String PCSubsidenceCoverTypeDropDownListId() {
        return "ctl00_cntMainBody_COVER_106";
    }

    public static String FireSubsidenceGeoTechDropDownListId() {
        return "ctl00_cntMainBody_REPORT_100";
    }

    public static String PCSubsidenceGeoTechDropDownListId() {
        return "ctl00_cntMainBody_REPORT_106";
    }

    public static String BusinessInterruptionOverviewTypeOfCoverDropDownListId() {
        return "ctl00_cntMainBody_BI__TYPE_OF_COVER";
    }

    public static String BIOverviewIndemnityPeriodDropDownListId() {
        return "ctl00_cntMainBody_BI__INDEMNITY_PERIOD";
    }

    public static String BICoverAdditionalIncreaseCostOfWorkingTypeDropDownListId() {
        return "ctl00_cntMainBody_BI__ICOW_TYPE";
    }

    public static String EHATBICoverAdditionalIncreaseCostOfWorkingTypeDropDownListId() {
        return "ctl00_cntMainBody_COVER_TYPE_AICOW_147";
    }

    public static String BICoverWagesMonthsDropDownListId() {
        return "ctl00_cntMainBody_BI__WAGES_TYPE";
    }

    public static String EHATBICoverLossofLeviesTypeDropDownListId() {
        return "ctl00_cntMainBody_COVER_TYPE_LEVIES_204";
    }

    public static String EHATBICoverLossofTouristAttractionDropDownListId() {
        return "ctl00_cntMainBody_COVER_TYPE_LEVIES_205";
    }

    public static String BCBuildingsCombinedOccupationDropDownListId() {
        return "ctl00_cntMainBody_BC__BC_OCC";
    }

    public static String BCExtentionsSubsidenceCoverTypeDropDownListId() {
        return "ctl00_cntMainBody_COVER_100";
    }

    public static String BCExtentionsSubsidenceGeoTechDropDownListId() {
        return "ctl00_cntMainBody_REPORT_100";
    }

    public static String OCAlarmWarrantyDropDownListId() {
        return "ctl00_cntMainBody_OC__ALARM_APPL";
    }

    public static String ADAccidentalDamageBasisOfCoverDropDownListId() {
        return "ctl00_cntMainBody_AD__EVENT1_BASIS_COVER";
    }

    public static String CopyRiskDropDownListId() {
        return "ctl00_cntMainBody_ddlCopyRiskType";
    }

    public static String DORatingCriteriaGrossAssetsDropDownListId() {
        return "ctl00_cntMainBody_DO__GROSS_ASSETS";
    }

    public static String DORatingCriteriaLimitofIndemnityDropDownListId() {
        return "ctl00_cntMainBody_DO__LIMIT_OF_INDEMNITY";
    }

    public static String SubSectionATypeOfWages() {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_TYPE_OF_WAGES";
    }

    public static String SurveyFrequencyDropDownListId() {
        return "ctl00_cntMainBody_GROUP_FIRE__FREQUENCY";
    }

    public static String EHATBICoverGrossProfitBasisDeclarationBasisDropDownListId() {
        return "ctl00_cntMainBody_DEC_BASIS_144";
    }

    public static String EHATBICoverGrossRentalsDeclarationBasisDropDownListId() {
        return "ctl00_cntMainBody_DEC_BASIS_145";
    }

    public static String EHATBICoverRevenueDeclarationBasisDropDownListId() {
        return "ctl00_cntMainBody_DEC_BASIS_146";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Validation Items">
    public static String ViewClientDetailsLabelId() {
        return "ctl00_cntMainBody_lblViewClient";
    }

    public static String RenewalSelectionMessageDivId() {
        return "ctl00_cntMainBody_lblMessage";
    }

    public static String RecommendClaimValidationSummaryDivId() {
        return "ctl00_cntMainBody_ValidationSummary";
    }

    public static String BasicDetailsPageHeaderClassName() {
        return "panelLegend";
    }

    public static String RatingDetailsLabelId() {
        return "ctl00_cntMainBody_RatingDetails1_lblRatingDetails";
    }

    public static String PolicyReferenceNumberLabelId() {
        return "ctl00_ClientInfoCtrl_lblPolicyRef";
    }

    public static String ClaimReferenceNumberLabelId() {
        return "ctl00_cntMainBody_lblClaimNumber";
    }

    public static String PolicyConfimationLabelId() {
        return "ctl00_cntMainBody_lblTransactionSubHeading";
    }

    public static String PolicyRenewalDivId() {
        return "ctl00_cntMainBody_PnlRenewalSelectionFile";
    }

// </editor-fold> 
// <editor-fold defaultstate="collapsed" desc="Radio Buttons">
    public static String PartyRadioButtonId() {
        return "ctl00_cntMainBody_PayClaim_ctrl_rblPayee_1";
    }

    public static String AgentRadioButtonId() {
        return "ctl00_cntMainBody_PayClaim_ctrl_rblPayee_2";
    }

    public static String ClientRadioButtonId() {
        return "ctl00_cntMainBody_PayClaim_ctrl_rblPayee_3";
    }

    public static String InsurerRadioButtonId() {
        return "ctl00_cntMainBody_PayClaim_ctrl_rblPayee_4";
    }

    public static String ClaimRecievableRadioButtonId() {
        return "ctl00_cntMainBody_PayClaim_ctrl_rblPayee_0";
    }

    // </editor-fold>
}
