
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class EkunduEnrouteRiskPage
  {
    public static String SelectEnrouteRiskLinkId()
      {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl07_lnkbtnSelect";
      }

    public static String RiskPlanTypeDropdownListId()
      {
        return "ctl00_cntMainBody_EBPENROUTE__PLANT";
      }

    public static String RiskNumberofPersonsTextboxId()
      {
        return "ctl00_cntMainBody_EBPENROUTE__NO_O_PERS";
      }

    public static String RiskDisabilityWeeksDropdownListId()
      {
        return "ctl00_cntMainBody_EBPENROUTE__TD_WEEKS";
      }

    public static String RiskVINNumberTextboxId()
      {
        return "ctl00_cntMainBody_EBPENROUTE__VIN_CHASSIS_NUMBER";
      }

    public static String RiskRateButtonId()
      {
        return "ctl00_cntMainBody_btnRate";
      }

    public static String RiskNextButtonId()
      {
        return "ctl00_cntMainBody_btnNext";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
