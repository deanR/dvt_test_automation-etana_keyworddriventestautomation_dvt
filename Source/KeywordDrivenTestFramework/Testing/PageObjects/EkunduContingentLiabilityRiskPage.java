
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class EkunduContingentLiabilityRiskPage
  {
    public static String SelectContingentLiabilityRiskTypeLinkId()
      {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl08_lnkbtnSelect";
      }

    public static String RiskWagesCheckboxId()
      {
        return "ctl00_cntMainBody_chkWages";
      }

    public static String RiskWagesTotalTextboxId()
      {
        return "ctl00_cntMainBody_EGR__ITEM_VALUE";
      }

    public static String RiskRateTextboxId()
      {
        return "ctl00_cntMainBody_EGR__RATE";
      }

    public static String RiskTotalPremiumTextboxId()
      {
        return "ctl00_cntMainBody_EGR__PREMIUM";
      }

    public static String RiskLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_EGR__SI_LOI";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
