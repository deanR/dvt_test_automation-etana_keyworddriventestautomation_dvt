
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import static KeywordDrivenTestFramework.Core.BaseClass.CurrentEnvironment;
import static KeywordDrivenTestFramework.Entities.Enums.TestEnvironments.QA;
import static KeywordDrivenTestFramework.Entities.Enums.TestEnvironments.UAT;

/**
 *
 * @author FerdinandN
 */
public class EkunduMaintainCorporateClientClaim1Page {

    public static String ClaimAcceptButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String ReservesSubmitButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String AuthoriseClaimOkButtonId() {
        return "ctl00_cntMainBody_btnOk";
    }

    public static String AuthoriseClaimButtonId() {
        return "ctl00_cntMainBody_btnAuthorise";
    }
 public static String FinishButtonId() {
        return "ctl00$cntMainBody$btn_Finish";
    }

    public static String CaseManagementLinkXPath() {
        return "//span/div/ul/li[5]/ul/li[2]/a";
    }

    public static String ClaimSearchLinkXpath() {
        return "//span/div/ul/li[5]/ul/li[3]/a";
    }

    public static String maintainClainLinkId() {
        return "ctl00_cntMainBody_grdvLinkedClaims_ctl02_btnEdit";
    }

    public static String payClainLinkId() {
        return "ctl00_cntMainBody_grdvLinkedClaims_ctl02_btnPay";
        //return "ctl00_cntMainBody_grdvSearchResults_ctl02_btnPay";
    }

    public static String AuthoriseClaimLinkId() {
        return "ctl00_cntMainBody_grdvAuthoriseclaimpayments_ctl02_lnkAuthorise";
    }

    public static String AuthoriseClaimLinkXpath() {
        return "//span/div/ul/li[5]/ul/li[4]/a";
    }

    public static String ClaimHoverTabId() {
        return "s-case";
    }

    public static String ClaimReferenceTextBoxId() {
        return "ctl00_cntMainBody_txtClaimReference";
    }

    public static String CaseNumberTextBoxId() {
        return "ctl00_cntMainBody_txtCaseNumber";
    }

    public static String ClaimFindNowButtonId() {
        return "ctl00_cntMainBody_btnFindNow";
    }

    public static String EditClaimLinkId() {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_btnEdit";
    }

    public static String SalvageClaimLinkId() {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_btnSalvage";
        //return "ctl00_cntMainBody_grdvSearchResults_ctl02_btnSalvage";
    }
    
     public static String SalvageClaimLinkId2() {
        return "ctl00_cntMainBody_perils_ctrl_grdvPerils_ctl02_lnkSalvageClaim";
        //return "ctl00_cntMainBody_grdvSearchResults_ctl02_btnSalvage";
    }

    public static String TPRecoveryLinkId() {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_btnTPRecovery";
    }

    public static String PerilsTPRecoveryLinkId() {
        return "ctl00_cntMainBody_perils_ctrl_grdvPerils_ctl02_lnkTPRecovery";
    }

//  public static String PerilsTPRecoveryLinkId()
//  {
//      return "ctl00_cntMainBody_perils_ctrl_grdvPerils_ctl02_lnkTPRecovery";
//  }
    public static String ClaimReasonforChangeDropdowlistId() {
        return "ctl00_cntMainBody_ddlReason";
    }

    public static String payDetailsTabId() {
        return "ui-id-2";
    }

    public static String publicLiabilityGeneneralLinkId() {
        return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl02_lnkReserves";
    }

    public static String ClaimOkButtonId() {
        return "ctl00_cntMainBody_btnOk";
    }
 public static String OkButtonId() {
        return "ctl00$cntMainBody$btnOk";
    }

    public static String ClaimReinsuranceOkButtonId() {
        return "ctl00_cntMainBody_ctrl_RI2007_btnNext";
    }

    public static String ReturnToCaseButtonId() {
        return "ctl00_cntMainBody_btnReturnToCase";
    }

    public static String nextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String ReservesOkButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String ClaimSubmitButtonId() {
        return "ctl00_cntMainBody_btnSubmit";
    }

    public static String ReinsuranceOkButtonId() {
        return "ctl00_cntMainBody_ctrl_RI2007_btnNext";
    }

    public static String PerilsBuildingsEditLinkId() {

        if (CurrentEnvironment == QA) {
            return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl33_lnkReserves";
        }
        if (CurrentEnvironment == UAT) {
            return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl32_lnkReserves";
        } else {
            return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl32_lnkReserves";
        }

    }

    public static String PerilsBuildingsRecoveriesLinkId() {
        if (CurrentEnvironment == QA) {
            return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl32_lnkRecoveries";
        }
        if (CurrentEnvironment == UAT) {
            return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl33_lnkRecoveries";
        } else {
            return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl33_lnkRecoveries";
        }

//        
    }

    public static String ReservesBuildingsOwnDamageNewValueTextBoxId() {
        return "newValue02";
    }

    public static String ReservesBuildingsThirdPartyRecoveryNewValueTextBoxId() {
        //return "newValue03";
        return "newValue07";
    }

    public static String ReservesBuildingsSalvageRecoverNewValueTextBoxId() {
        return "newValue09";
    }

    public static String PerilsPlantandMachineryEditLinkId() {

        if (CurrentEnvironment == QA) {
            return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl35_lnkReserves";
        }
        if (CurrentEnvironment == UAT) {
            return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl35_lnkReserves";
        } else {
            return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl35_lnkReserves";
        }

    }

    public static String ReservesPlantandMachineryOwnDamageNewValueTextBoxId() {
        return "newValue02";
    }

    public static String ReservesPlantandMachineryThirdPartyRecoveryNewValueTextBoxId() {
        return "newValue07";
    }

    public static String ReservesPlantandMachinerySalvageRecoverNewValueTextBoxId() {
        return "newValue09";
    }

    public static String PerilsStockEditLinkId() {
        if (CurrentEnvironment == QA) {
            return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl34_lnkReserves";
        }
        if (CurrentEnvironment == UAT) {
            return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl34_lnkReserves";
        } else {
            return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl34_lnkReserves";
        }

    }

    public static String RecommendClaimLinkId() {
        return "ctl00_cntMainBody_grdvAuthoriseclaimpayments_ctl02_lnkRecommend";
    }

    public static String ReservesStockOwnDamageNewValueTextBoxId() {
        return "newValue02";
    }

    public static String ReservesStockThirdPartyRecoveryNewValueTextBoxId() {
        return "newValue07";
    }

    public static String ReservesStockSalvageRecoverNewValueTextBoxId() {
        return "newValue09";
    }

    public static String PayClaimLinkId() {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_btnPay";
    }

    public static String PerilsBuildingsDetailsLinkId() {
        return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl32_lnkReserves";
    }

    public static String PaymentDetailsTabLinkText() {
        return "Payment Details";
    }

    public static String PaymentDetailsInsuredRadioButtonId() {
        return "ctl00_cntMainBody_PayClaim_ctrl_rblPayee_3";
    }

    public static String PaymentDetailsOwnDmageEditLinkId() {
        return "ctl00_cntMainBody_PayClaim_ctrl_gvPaymentDetails_ctl02_hypEditPayment";
    }

    public static String PaymentDetailsTaxGroupDropdownListId() {
        return "ctl01_cntMainBody_ddlTaxGroup";
    }

    public static String PaymentDetailsPaymentAmountTextboxId() {
        return "ctl01_cntMainBody_txtPaymentAmount";
    }

    public static String PaymentDetailsOkButtonId() {
        return "ctl01_cntMainBody_btnOk";
    }

    public static String PaymentDetailsThisPaymentTabLinkTextId() {
        return "This Payment";
    }

    public static String PaymentDetailsMediaTypeDropdownListId() {
        return "ctl00_cntMainBody_PayClaim_ctrl_ddlMediaType";
    }

    public static String ClaimFinishButtonId() {
        return "ctl00_cntMainBody_btnFinish";
    }

    public static String PaymentDetailsDialogId() {
        return "modalDialog";
    }

    public static String RecommendClaimButtonId() {
        return "ctl00_cntMainBody_btnRecommend";
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
