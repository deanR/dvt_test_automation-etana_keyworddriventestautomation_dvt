
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.PageObjects;

import static KeywordDrivenTestFramework.Core.BaseClass.CurrentEnvironment;
import static KeywordDrivenTestFramework.Entities.Enums.TestEnvironments.*;

/**
 *
 * @author deanR
 */
public class EkunduDomesticAndPropertyLiabilityRiskPage
  {
    public static String EndorsementsSelectButtonId()
      {
        return "_btnShowSelect";
      }

    public static String EndorsementsAddAllButtonId()
      {
        return "ctl00_cntMainBody_S_ENDORSEMENT__ENDORSEMENT_PckTemplates_RemoveAllCmd";
      }

    public static String EndorsementsApplyButtonId()
      {
        return "ctl00_cntMainBody_S_ENDORSEMENT__ENDORSEMENT_btnApplySelection";
      }

    public static String buildingSumInsuredTextBoxId()
      {
        return "ctl00_cntMainBody_BUILDING__BUILD_SI";
      }

    public static String ChangeAddressButtonName()
      {
        return "changeButton";
      }

    public static String AddressRowId()
      {
        return "1";
      }

    public static String AddSurveyButtonName()
      {
        return "ctl00$cntMainBody$PREMISES__PREM_SURVEY_REPORT$ctl02$ctl00";
      }

    public static String AddEndorsementButtonID()
      {
        return "ctl00$cntMainBody$PREMISES__S_ENDORSEMENT$ctl02$ctl00";
      }

    public static String SurveyNextButtonId()
      {
        return "ctl00_cntMainBody_btnNext";
      }

    public static String SurveyRateButtonId()
      {
        return "ctl00_cntMainBody_btnRate";
      }

    public static String SurveyDateTextBoxId()
      {
        return "ctl00_cntMainBody_PREM_SURVEY_REPORT__SRD";
      }

    public static String SurveyReferenceTextBoxId()
      {
        return "ctl00_cntMainBody_PREM_SURVEY_REPORT__SRREF";
      }

    public static String SurveyReportCommentsTextAreaId()
      {
        return "ctl00_cntMainBody_PREM_SURVEY_REPORT__COMMENTS";
      }

    public static String SelectSurveyTypeDropdownListId()
      {
        return "ctl00_cntMainBody_PREM_SURVEY_REPORT__SRT";
      }

    public static String CheckBuildingCheckBoxId()
      {
        return "ctl00_cntMainBody_PREMISES__BUILDINGS_APPL";
      }

    public static String CheckContentsCheckBoxId()
      {
        return "ctl00_cntMainBody_PREMISES__CONT_APPL";
      }

    public static String TypeofResidenceDropdownListId()
      {
        return "ctl00_cntMainBody_PREMISES__TOR";
      }

    public static String TypeofConstructionDropdownListId()
      {
        return "ctl00_cntMainBody_PREMISES__TOC";
      }

    public static String OccupancyDropdownListId()
      {
        return "ctl00_cntMainBody_PREMISES__OCC";
      }

    public static String LocalityDropdownListId()
      {
        return "ctl00_cntMainBody_PREMISES__LOC";
      }

    public static String AddInterestedPartiesButtonID()
      {
        return "ctl00$cntMainBody$BUILDING__BUILD_IP$ctl02$ctl00";
      }

    public static String AddInterestedParties2ButtonID()
      {
        return "ctl00$cntMainBody$HOUSEHOLD__HH_IP$ctl02$ctl00";
      }

    public static String TypeofAgreement2TypeDropdownListId()
      {
        return "ctl00_cntMainBody_HH_IP__HH_IP_TOA";
      }

    public static String InstitutionName2DropdownListId()
      {
        return "ctl00_cntMainBody_HH_IP__HH_IP_INST_NAME";
      }

    public static String SectionsDropdownListId()
      {
        return "ctl00_cntMainBody_S_ENDORSEMENT__SECTIONS";
      }

    public static String TypeofAgreementTypeDropdownListId()
      {
          if(CurrentEnvironment == NamQA)
          {
              return "ctl00_cntMainBody_INTERESTED_PARTIES__AGREEMENT_TYPE";
          }
          else
          {
              return "ctl00_cntMainBody_BUILD_IP__BUILD_IP_TOA";
          }
                
      }

    public static String InstitutionNameDropdownListId()
      {
        if(CurrentEnvironment == NamQA)
        {  
            return "ctl00_cntMainBody_INTERESTED_PARTIES__INSTITUTION_NAME";
        }      
        else 
        {
            return "ctl00_cntMainBody_BUILD_IP__BUILD_IP_INST_NAME";
        }
      }

    public static String AddInterestedPartiesButtonName()
      {
          if(CurrentEnvironment == NamQA)
          {
              return "ctl00$cntMainBody$PC__INTERESTED_PARTIES$ctl02$ctl00";
          }
          else
          {
              return "ctl00$cntMainBody$BUILDING__BUILD_IP$ctl02$ctl00";
          }
        
      }

    public static String InterestedPartiesCommentsTextAreaId()
      {
          if(CurrentEnvironment == NamQA)
          {
              return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
          }
          else
          {
                return "ctl00_cntMainBody_BUILD_IP__COMMENTS";
          }
      }

    public static String InterestedParties2CommentsTextAreaId()
      {
        return "ctl00_cntMainBody_HH_IP__COMMENTS";
      }

    public static String EndorsementsCommentsTextAreaId()
      {
        return "ctl00_cntMainBody_S_ENDORSEMENT__NOTES";
      }

    public static String ContentsSumInsuredTextBoxId()
      {
        return "ctl00_cntMainBody_HOUSEHOLD__HH_SI";
      }

    public static String NoClaimDiscountDropdownListId()
      {
        return "ctl00_cntMainBody_HOUSEHOLD__NO_CLAIM_DISCOUNT";
      }

    public static String SecurityDiscountsLevel5AlarmCheckBoxId()
      {
        return "ctl00_cntMainBody_HOUSEHOLD__SAIDSA_ALARM_APPL";
      }

    public static String SecurityDiscountsSecurityDoorsCheckBoxId()
      {
        return "ctl00_cntMainBody_HOUSEHOLD__SEC_DOORS_APPL";
      }

    public static String SecurityDiscountsGuardDogsCheckBoxId()
      {
        return "ctl00_cntMainBody_HOUSEHOLD__DOGS_APPL";
      }

    public static String SecurityDiscountsburglabarsCheckBoxId()
      {
        return "ctl00_cntMainBody_HOUSEHOLD__BURGLAR_BARS_APPL";
      }

    public static String SecurityDiscountsElectricFencingCheckBoxId()
      {
        return "ctl00_cntMainBody_HOUSEHOLD__ELEC_FENC_APPL";
      }

    public static String SecurityDiscountsGuardedAccessCheckBoxId()
      {
        return "ctl00_cntMainBody_HOUSEHOLD__GUARD_ACCESS_APPL";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
