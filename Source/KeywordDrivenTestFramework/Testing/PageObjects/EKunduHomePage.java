
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Entities.Enums.TestEnvironments.*;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

/**
 *
 * @author fnell
 */
public class EKunduHomePage extends BaseClass
  {
    public static String PageUrl()
      {
        return ApplicationConfig.EkunduPageUrl();
      }
    
    

//  <editor-fold defaultstate="collapsed" desc="Labels">
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="TextBoxes">

    public static String UsernameTextBoxId()
      {
            return "ctl00_cntMainBody_LoginCntrl_txtUsername";
      }
    
     public static String NameofVesselTextBoxId()
    {
        if(CurrentEnvironment == NamQA)
        {
            return "ctl00_cntMainBody_PC__VESSEL_NAME";
        }
        else
        {
            return "ctl00_cntMainBody_VESSEL__NAME";
        }
    }
    
     public static String NameofVesselSADCTextBoxId()
    {
        return "ctl00_cntMainBody_PC__VESSEL_NAME";

    }
    
    public static String MakeandModelTextBoxId()
    {
        if(CurrentEnvironment == NamQA)
        {
            return "ctl00_cntMainBody_PC__VESSEL_MAKE";
        }
        else 
        {
            return "ctl00_cntMainBody_VESSEL__MAKE";
        }
    }
    
    
    public static String MakeandModelSADCTextBoxId()
    {
        return "ctl00_cntMainBody_PC__VESSEL_MAKE";

    }
    
    public static String VesselDescTextBoxId()
    {
        return "ctl00_cntMainBody_VESSEL__TYPE_DESC";
    }
    
    public static String MaterialofHullTextBoxId()
    {
        if(CurrentEnvironment == NamQA )
        {
            return "ctl00_cntMainBody_PC__HULL_MATERIAL";
        }
        else
        {
            return "ctl00_cntMainBody_HULL__MAT_OF_HULL";
        }
    }
    
    public static String InterestedPartyDescriptionSADCTextboxId()
    {
        return "ctl00_cntMainBody_INTERESTED_PARTIES__DESCRIPTION";
    }
    
    public static String MaterialofHullSADCTextBoxId()
    {
        
        return "ctl00_cntMainBody_PC__HULL_MATERIAL";
    }
    
    public static String LengthofHullSADCTextBoxId()
    {
        return "ctl00_cntMainBody_PC__HULL_LENGTH";
    }
    
    public static String HullYearofManufactureSADCTextBoxId()
    {
        return "ctl00_cntMainBody_PC__HULL_YEAR";
    }
    
    public static String HullSumInsuredTextBoxId()
    {
        if(CurrentEnvironment == NamQA )
        {
            return "ctl00_cntMainBody_PC__HULL_SI";
        }
        else 
        {
            return "ctl00_cntMainBody_HULL__SI";
        }
    }
    
    public static String HullSumInsuredSADCTextBoxId()
    {
        return "ctl00_cntMainBody_PC__HULL_SI";
    }

    public static String PasswordTextBoxId()
      {
        return "ctl00_cntMainBody_LoginCntrl_txtPassword";
      }

    public static String PolicyReferenceTextBoxId()
      {
        return "ctl00_cntMainBody_txtReference";
      }

//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="Buttons">

    public static String SelectBranchOkButtonId()
      {
        return "ctl00_cntMainBody_btnOK";
      }

    public static String LoginButtonId()
      {
        return "ctl00_cntMainBody_LoginCntrl_btnSubmit";
      }
    
      public static String EndorsementsAddAllButtonID()
    {
        return "ctl00_cntMainBody_PL__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
    }
    
    public static String EndorsementsApplyButtonID()
    {
        return "ctl00_cntMainBody_PL__ENDORSEMENTS_btnApplySelection";
    }

    // </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="HoverTabs">

    public static String ClientHoverTabId()
      {
        return "s-client";
      }

    public static String HomeHoverTabId()
      {
        return "s-home";
      }

    public static String PolicyHoverTabId()
      {
        return "s-policy";
      }
    
    public static String ClaimHoverTabId()
      {
        return "s-case";
      }

//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="Tabs">
    public static String NewCorporateClientTabXPath()
      {
        return "//*[@id=\"ctl00_menuDiv\"]/ul/li[3]/ul/li[3]/a";
      }

    public static String RenewalSelectionTabXPath()
      {
        return "//*[@id=\"ctl00_menuDiv\"]/ul/li[4]/ul/li[3]/a";
      }

    public static String RenewalManagerTabXPath()
      {
        return "//*[@id=\"ctl00_menuDiv\"]/ul/li[4]/ul/li[2]/a";
      }

    public static String FindClientTabXPath()
      {
        return "//*[@id=\"ctl00_menuDiv\"]/ul/li[3]/ul/li[1]/a";
      }

    public static String NewPersonalClientXPath()
      {
        return "//*[@id=\"ctl00_menuDiv\"]/ul/li[3]/ul/li[2]/a";
      }

//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="Iframes">
    public static String ChangeBranchFrameId()
      {
        return "modalDialog";
      }
    
    public static String EditDocumentFrameId()
      {
        return "ctl00_cntMainBody_txtDocumentEditor_ifr";
      }

//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="Links">
    public static String ChangeBranchLinkId()
      {
        return "ctl00_statusBar_changeBranchLink";
      }

    public static String WorkManagerLinkText()
      {
        return "Work Manager";
      }

    public static String ChangeBranchLinkXpath()
      {
        return "//div[3]/div/div[2]/div/a[2]";
      }

    public static String LogoutLinkId()
      {
        return "ctl00_statusBar_lnkLogOut";
      }

//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="Drop Down Lists">
    public static String SelectBranchDropDownListId()
      {
        return "ctl00_cntMainBody_ddlBranchCode";
      }

//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="Validation Items">
    public static String HomeDivId()
      {
        return "etana_home";
      }

    public static String RenewalSelectionDivId()
      {
        return "ctl00_cntMainBody_PnlRenewalSelectionFile";
      }

//  </editor-fold> 
  }


//~ Formatted by Jindent --- http://www.jindent.com
