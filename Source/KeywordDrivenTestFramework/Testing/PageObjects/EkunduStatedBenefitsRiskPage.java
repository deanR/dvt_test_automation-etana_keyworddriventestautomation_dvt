
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class EkunduStatedBenefitsRiskPage
  {
    public static String SelectStatedBenefitsRiskLinkId()
      {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl03_lnkbtnSelect";
      }

    public static String RiskNextButtonId()
      {
        return "ctl00_cntMainBody_btnNext";
      }

    public static String RiskRateButtonId()
      {
        return "ctl00_cntMainBody_btnRate";
      }

    public static String RiskItemsReinsuranceLimitofLiabilityTextboxId()
      {
        return "ctl00_cntMainBody_SB__RI_LIMIT_OF_LIABILITY";
      }

    public static String RiskItemsInsuredPersonsAddButtonXpath()
      {
        return "//div[@id = \"ctl00_cntMainBody_SB__ITEM\"]/table/tfoot/tr/td[8]/input[@value = \"Add\"]";
      }

    public static String EmployeeDetailsTotalAnnualEarningsTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__TOTAL_SI";
      }

    public static String EmployeeDetailsOccupationDropdownListId()
      {
        return "ctl00_cntMainBody_ITEM__OCCUPATION";
      }

    public static String EmployeeDetailsOccupationDescriptionTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__OCC_DESC";
      }

    public static String EmployeeDetailsNumberofYearsTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__NO_OF_YEARS";
      }

    public static String EmployeeDetailsCoverDeathLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__DEATH_LOL";
      }

    public static String EmployeeDetailsCoverDeathAgreedRateTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__DEATH_AGREED_RATE";
      }

    public static String EmployeeDetailsCoverPermanentDisabilityCheckboxId()
      {
        return "ctl00_cntMainBody_ITEM__PD_APPL";
      }

    public static String EmployeeDetailsCoverPermanentDisabilityLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__PD_LOL";
      }

    public static String EmployeeDetailsCoverPermanentDisabilityAgreedRateTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__PD_AGREED_RATE";
      }

    public static String EmployeeDetailsCoverTTDCheckboxId()
      {
        return "ctl00_cntMainBody_ITEM__TTD_APPL";
      }

    public static String EmployeeDetailsCoverTTDLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__TTD_LOL";
      }

    public static String EmployeeDetailsCoverTTDNumberofWeeksDropdownListId()
      {
        return "ctl00_cntMainBody_ITEM__TTD_NUM_WEEKS";
      }

    public static String EmployeeDetailsCoverTTDAgreedRateTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__TTD_AGREED_RATE";
      }

    public static String EmployeeDetailsCoverMedicalExpensesCheckboxId()
      {
        return "ctl00_cntMainBody_ITEM__ME_APPL";
      }

    public static String EmployeeDetailsCoverMedicalExpensesLimitofIndemnityDropdownListId()
      {
        return "ctl00_cntMainBody_ITEM__ME_LIMIT";
      }

    public static String EmployeeDetailsCoverMedicalExpensesAgreedRateTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__ME_APPL";
      }

    public static String EmployeeDetailsCoverMedicalExpensesFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__ME_FAP";
      }

    public static String ExtensionsBodyTransportationCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_73";
      }

    public static String ExtensionsBodyTransportationRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_73";
      }

    public static String ExtensionsBodyTransportationPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_73";
      }

    public static String ExtensionsBodyTransportationFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_73";
      }

    public static String ExtensionsBodyTransportationFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_73";
      }

    public static String ExtensionsDisappearanceCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_83";
      }

    public static String ExtensionsFuneralCostsCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_72";
      }

    public static String ExtensionsFuneralCostsRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_72";
      }

    public static String ExtensionsFuneralCostsPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_72";
      }

    public static String ExtensionsFuneralCostsFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_72";
      }

    public static String ExtensionsFuneralCostsFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_72";
      }

    public static String ExtensionsRepatriationCostsCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_74";
      }

    public static String ExtensionsRepatriationCostsRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_74";
      }

    public static String ExtensionsRepatriationCostsPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_74";
      }

    public static String ExtensionsRepatriationCostsFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_74";
      }

    public static String ExtensionsRepatriationCostsFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_74";
      }

    public static String ExtensionsSeriousIllnessCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_70";
      }

    public static String ExtensionsSeriousIllnessRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_70";
      }

    public static String ExtensionsSeriousIllnessPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_70";
      }

    public static String ExtensionsSeriousIllnessFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_70";
      }

    public static String ExtensionsSeriousIllnessFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_70";
      }

    public static String ExtensionsSicknessCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_71";
      }

    public static String ExtensionsSicknessRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_71";
      }

    public static String ExtensionsSicknessFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_71";
      }

    public static String ExtensionsSicknessFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_71";
      }

    public static String ExtensionsSicknessLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_71";
      }

    public static String TerrorismCheckboxId()
      {
        return "ctsl00_cntMainBody_CHECK_82";
      }

    public static String RiskCommentsTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
      }

    public static String RiskPolicyNotesTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
      }

    public static String RiskAddNotesLabelId()
      {
        return "ctl00_cntMainBody_cntrlNotes_lbl_CONTROL__ADD_NOTES";
      }

    public static String EmployeeDetailsNameTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__NAME";
      }

    public static String ExtensionsTerrorismCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_82";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
