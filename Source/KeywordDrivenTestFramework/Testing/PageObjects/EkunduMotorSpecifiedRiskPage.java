
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import static KeywordDrivenTestFramework.Core.BaseClass.CurrentEnvironment;
import static KeywordDrivenTestFramework.Entities.Enums.TestEnvironments.NamQA;

/**
 *
 * @author FerdinandN
 */
public class EkunduMotorSpecifiedRiskPage
  {
    public static String SelectMotorSpecifiedRiskLinkId()
      {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl02_lnkbtnSelect";
      }

    public static String VehicleTypeSelectVehicleTypeDropdownListId()
      {
        return "ctl00_cntMainBody_MS__VEHICLE_TYPE";
      }

    public static String VehicleTypeOptionsEndorsementsCheckboxId()
      {
        return "ctl00_cntMainBody_MS__ENDORSE_APPL";
      }

    public static String VehicleTypeClaimsAccidentDropdownListId()
      {
        return "ctl00_cntMainBody_MS__CFG_ACCIDENT";
      }

    public static String VehicleTypeClaimsOtherDropdownListId()
      {
        return "ctl00_cntMainBody_MS__CFG_OTHER";
      }

    public static String DetailsVehicleSearchIconId()
      {
        return "search";
      }
    
    public static String FinancialOverviewNextButtonId()
      {
        return "ctl00_cntMainBody_btnNext";
      }

    public static String DetailsVehicleMakeDropdownListId()
      {
        return "make";
      }
    
    public static String VehicleTypeDropdownListId()
      {
        return "ctl00_cntMainBody_MS__VEHICLE_TYPE";
      }

    public static String DetailsVehicleYearDropdownListId()
      {
        return "year";
      }
    
    public static String VehicleCategoryDropdownListId()
      {
        return "ctl00_cntMainBody_MS__VEHICLE_CATEGORY";
      }
    
    public static String CurrentInsurancePeriodDropdownListId()
      {
        return "ctl00_cntMainBody_MS__CURRENT_INSURANCE_PERIOD";
      }
    
    public static String MainDriverLicenceTypeDropdownListId()
      {
        return "ctl00_cntMainBody_MS__DRIVER_LICENSE_TYPE";
      }
    
    public static String EPLMainDriverLicenceTypeDropdownListId()
      {
        return "ctl00_cntMainBody_MAIN_DRIVERS__LICENSE_TYPE";
      }
    
    public static String SpecifiedDriverLicenceTypeDropdownListId()
      {
        return "ctl00_cntMainBody_DRIVER__SADC_LICENSE_TYPE";
      }
    
    public static String MainDriverIDTypeDropdownListId()
      {
        return "ctl00_cntMainBody_MS__SADC_DRIVER_ID_TYPE";
      }
    
    public static String EPLMainDriverIDTypeDropdownListId()
      {
        return "ctl00_cntMainBody_MAIN_DRIVERS__ID_TYPE";
      }
    
    public static String SpecifiedDriverIDTypeDropdownListId()
      {
        //return "ctl00_cntMainBody_DRIVER__SADC_DRIVER_ID_TYPE";
        
        return "ctl00_cntMainBody_DRIVER__ID_TYPE";
      }         
    
    public static String EPLSpecifiedDriverIDTypeDropdownListId()
      {
        return "ctl00_cntMainBody_DRIVER__ID_TYPE";
      }
    
    public static String EBPSpecifiedDriverIDTypeDropdownListId()
      {
        return "ctl00_cntMainBody_DRIVER__ID_TYPE";
      }
    
    public static String MainDriverMaritalStatusDropdownListId()
      {
        return "ctl00_cntMainBody_MS__DRIVER_MARITAL_STATUS";
      }
    
    public static String SpecifiedDriverMaritalStatusDropdownListId()
      {
        return "ctl00_cntMainBody_DRIVER__MARITAL_STATUS";
      }
    
    public static String MainDriverGenderDropdownListId()
      {
        return "ctl00_cntMainBody_MS__GENDER";
      }

    public static String EPLMainDriverGenderDropdownListId()
      {
        return "ctl00_cntMainBody_MAIN_DRIVERS__GENDER";
      }
    
    public static String SpecifiedDriverGenderDropdownListId()
      {
        return "ctl00_cntMainBody_DRIVER__GENDER";
      }

    public static String DetailsVehicleModelDropdownListId()
      {
        return "model";
      }
    
    public static String DetailsVehicleClassofUseDropdownListId()
      {
        return "ctl00_cntMainBody_MS__VEHICLE_TYPE_OF_USE";
      }

    public static String DetailsRegistrationDetailsRegNumberTextBoxId()
      {
        return "ctl00_cntMainBody_MS__REG_NUM";
      }
    
    public static String RegistrationDetailsOriginalRegistrationDateTextBoxId()
      {
        return "ctl00_cntMainBody_MS__REG_ORIGINAL_DATE";
      }
    
    public static String RegistrationDetailsChassisNumberTextBoxId()
      {
        return "ctl00_cntMainBody_MS__CHASSIS_NUM";
      }
    
    public static String RegistrationDetailsEngineNumberTextBoxId()
      {
        return "ctl00_cntMainBody_MS__ENGINE_NUM";
      }

    public static String DetailsRegistrationDetailsRegOwnerTextBoxId()
      {
        return "ctl00_cntMainBody_MS__REG_OWNER";
      }
    
    public static String NonFactoryFittedRadioValueTextBoxId()
      {
        return "ctl00_cntMainBody_MS__NON_FACTORY_FITTED_RADIO_VALUE";
      }
    
    public static String NonFactoryFittedRadioDescriptionTextBoxId()
      {
        return "ctl00_cntMainBody_MS__NON_FACTORY_FITTED_RADIO_DESCRIPTION";
      }

    public static String DetailsRegistrationDetailsNATISCodeDropdownListId()
      {
        return "ctl00_cntMainBody_MS__NATIS_CODE";
      }

    public static String DetailsMotorCoverSumInsuredTextBoxId()
      {
        return "ctl00_cntMainBody_MS__MAX_LOI";
      }

    public static String DetailsMotorCoverBasisofSettlementDropdownListId()
      {
        return "ctl00_cntMainBody_MS__VEH_COVER_METHOD";
      }
    
    public static String VehicleTypeDropdownListId2()
      {
        return "ctl00_cntMainBody_MS__SPEC_VEH_TYPE";
      }

    public static String ExtensionsCarHireCheckboxId()
      {
        if(CurrentEnvironment == NamQA)
        {
            return "ctl00_cntMainBody_CHECK_124";
        }
        else
        {
            return "ctl00_cntMainBody_CHECK_7";
        }
      }

    public static String ExtensionsCarHireLimitofIndemnityDropdownListId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_7";
      }

    public static String ExtensionsCarHirePostingPremiumTextBoxId()
      {
        return "ctl00_cntMainBody_RI_POSTING_7";
      }

    public static String ExtensionsCarHireConditionsDropdownListId()
      {
        return "ctl00_cntMainBody_CAR_HIRE_CONDITIONS_7";
      }

    public static String ExtensionsContingentLiabilityCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_3";
      }

    public static String ExtensionsContingentLiabilityLimitofIndemnityTextBoxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_3";
      }

    public static String ExtensionsContingentLiabilityRateTextBoxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_3";
      }

    public static String ExtensionsContingentLiabilityPostingPremiumTextBoxId()
      {
        return "ctl00_cntMainBody_RI_POSTING_3";
      }

    public static String ExtensionsCreditShortfallCheckboxId()
      {
        if(CurrentEnvironment == NamQA)
        {
            return "ctl00_cntMainBody_CHECK_125";
        }
        else 
        {
            return "ctl00_cntMainBody_CHECK_122";
        }
      }

    public static String ExtensionsCreditShortfallRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_122";
      }

    public static String ExtensionsCreditShortfallPostingPremiumTextboxId()
      {
        return "ctl00_cntMainBody_RI_POSTING_122";
      }

    public static String ExtensionsExcessWaiverCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_10";
      }

    public static String ExtensionsExcessWaiverPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_10";
      }

    public static String ExtensionsExcessWaiverPostingPremiumTextboxId()
      {
        return "ctl00_cntMainBody_RI_POSTING_10";
      }

    public static String ExtensionsFireandExplosionRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_14";
      }
    
    public static String CreditShortfallRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_122";
      }
    
    public static String ExtensionsFireandExplosionPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_14";
      }
    
    public static String ExtensionsRadioTheftTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_6";
      }

    public static String ExtensionsLossofKeysCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_5";
      }

    public static String ExtensionsLossofKeysLimitofIndemnityTextBoxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_5";
      }

    public static String ExtensionsLossofKeysPremiumTextBoxId()
      {
        return "ctl00_cntMainBody_PREMIUM_5";
      }

    public static String ExtensionsLossofKeysPostingPremiumTextBoxId()
      {
        return "ctl00_cntMainBody_RI_POSTING_5";
      }

    public static String ExtensionsParkingFacilitiesCheckBoxId()
      {
        return "ctl00_cntMainBody_CHECK_12";
      }
    
    public static String VoluntaryFAPCheckBoxId()
      {
        return "ctl00_cntMainBody_MS__FAP_VOL_APPL";
      }
    
    public static String VoluntaryDropdownListId()
    {
        return "ctl00_cntMainBody_MS__FAP_VOL_DED_AMT";
    }

    public static String ExtensionsParkingFacilitiesLimitofIndemnityTextBoxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_12";
      }

    public static String ExtensionsRiotandStrikeCheckBoxId()
      {
        return "ctl00_cntMainBody_CHECK_11";
      }

    public static String ExtensionsTheftofCarRadioCheckBoxId()
      {
        return "ctl00_cntMainBody_CHECK_6";
      }

    public static String ExtensionsThirdPartLiabilityRateTextBoxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_4";
      }
    
    public static String AdjustmentPercentageTextBoxId()
      {
        return "ctl00_cntMainBody_MS__FLAT_ADJ_PERC";
      }
    
    public static String OverrideReasonDropdownlistID()
      {
        return "ctl00_cntMainBody_MS__FLAT_PREM_REASON";
      }

    public static String ExtensionsThirdPartLiabilityPremiumTextBoxId()
      {
        return "ctl00_cntMainBody_PREMIUM_4";
      }

    public static String ExtensionsThirdPartLiabilityPostingPremiumTextBoxId()
      {
        return "ctl00$cntMainBody$RI_POSTING_4";
      }

    public static String ExtensionsTheftofCarRadioRateTextBoxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_6";
      }

    public static String ExtensionsWreckageRemovalCheckBoxId()
      {
        return "ctl00_cntMainBody_CHECK_13";
      }

    public static String ExtensionsWreckageRemovalLimitofIndemnityTextBoxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_13";
      }

    public static String ExtensionsWreckageRemovalRateTextBoxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_13";
      }

    public static String ExtensionsWreckageRemovalPostingPremiumTextBoxId()
      {
        return "ctl00_cntMainBody_RI_POSTING_13";
      }
    
    public static String FAPBasic3rdpartyMinAmountTextBoxId()
      {
        return "ctl00_cntMainBody_MS__FAP_3RD_PARTY_MIN_AMT";
      }

    public static String FAPBasicFireandExplosionMinAmountTextBoxId()
      {
        return "ctl00_cntMainBody_MS__FAP_FI_MIN_AMT";
      }

    public static String FAPBasic3rdpartyMinPercTextBoxId()
      {
        return "ctl00_cntMainBody_MS__FAP_3RD_PARTY_MIN_PERC";
      }

    public static String FAPBasicFireandExplosionMinPercTextBoxId()
      {
        return "ctl00_cntMainBody_MS__FAP_FI_MIN_PERC";
      }
    
    public static String thirdPartySubBPercTextBoxId()
      {
        return "ctl00_cntMainBody_MS__FAP_3RD_PARTY_MIN_PERC";
      }

    public static String FireExplosionSubBPercTextBoxId()
      {
        return "ctl00_cntMainBody_MS__FAP_FI_MIN_PERC";
      }


    public static String EndorsementsSelectButtonId()
      {
        return "_btnShowSelect";
      }
    
    public static String EditEndorsementsLinkId()
      {
        return "ctl00_cntMainBody_PC__ENDORSEMENTS_grdWordings_ctl02_lnkEdit";
      }
    
     public static String EditDomesticEndorsementsLinkId()
      {
        return "ctl00_cntMainBody_S_ENDORSEMENT__ENDORSEMENT_grdWordings_ctl02_lnkEdit";
      }
     
     public static String EditPersonalAccidentEndorsementsLinkId()
      {
        return "ctl00_cntMainBody_INDIVIDUALS__ENDORSEMENTS_grdWordings_ctl02_lnkEdit";
      }
     
     public static String EditMotorEndorsementsLinkId()
      {
        return "ctl00_cntMainBody_MS__ENDORSEMENTS_grdWordings_ctl02_lnkEdit";
      }
     
     public static String EditPersonalLiabilityEndorsementsLinkId()
      {
        return "ctl00_cntMainBody_PL__ENDORSEMENTS_grdWordings_ctl02_lnkEdit";
      }

    public static String EndorsementsAddAllButtonId()
      {
        return "ctl00_cntMainBody_MS__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
      }

    public static String EndorsementsApplyButtonId()
      {
        return "ctl00_cntMainBody_MS__ENDORSEMENTS_btnApplySelection";
      }

    public static String MotorSpecifiedNotesTextBoxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
      }         
    
    public static String InterestedPartyNotesTextBoxId()
      {
        return "ctl00_cntMainBody_EPL_IPARTIES__NOTES";
      }

    public static String MotorSpecifiedPolicyNotesTextBoxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
      }

    public static String MotorSpecifiedReinsuranceDetailstabLinkText()
      {
        return "Reinsurance Details";
      }

    public static String MotorSpecifiedOkButtonId()
      {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_btnOk";
      }

    public static String MotorSpecifiedAddNotesCheckboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__ADD_NOTES";
      }         

    public static String AddRiskButtonId()
      {
        return "ctl00_cntMainBody_MultiRisk1_btnAddRisk";
      }

    public static String VehiclesdivId()
      {
        return "grid-dialog-modal";
      }

    public static String DetailsMotorCoverTypeDropdownListId()
      {
        return "ctl00_cntMainBody_MS__COVER_TYPE";
      }
    
    public static String SelectedExcessDropdownListId()
      {
        return "ctl00_cntMainBody_MS__SADC_BASIC_EXCESS";
      }

    public static String DetailsRateButtonId()
      {
        return "ctl00_cntMainBody_btnRate";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
