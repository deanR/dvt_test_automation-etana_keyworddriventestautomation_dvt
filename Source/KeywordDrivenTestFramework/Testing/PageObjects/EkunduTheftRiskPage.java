
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class EkunduTheftRiskPage
  {
    public static String SelectTheftRiskTypeLinkId()
      {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl11_lnkbtnSelect";
      }

    public static String MaliciousDamageIndustrialConstructionDropdownListId()
      {
        return "ctl00_cntMainBody_THEFT__UDL_IND_CONSTRUCTION";
      }

    public static String RiskItemAlarmWarrantyDropdownListId()
      {
        return "ctl00_cntMainBody_THEFT__ALARM_WARRANTY_APPL";
      }

    public static String RiskItemAllContentsCheckboxId()
      {
        return "ctl00_cntMainBody_THEFT__CONTENTS_APPL";
      }

    public static String RiskItemAllContentsSumInsuredTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__CONTENTS_SI";
      }

    public static String RiskItemAllContentsAgreedRateTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__CONTENTS_AGREED_RATE";
      }

    public static String RiskItemAllContentsPremiumTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__CONTENTS_PREM";
      }

    public static String RiskItemAllContentsFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__CONTENTS_FAP_PERC";
      }

    public static String RiskItemAllContentsFAPMinAmountTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__CONTENTS_FAP_AMT";
      }

    public static String RiskItemAllContentsFAPMaxAmountTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__CONTENTS_FAP_MAX_AMT";
      }

    public static String RiskItemIncreasedBuildingsLimitCheckboxId()
      {
        return "ctl00_cntMainBody_THEFT__BUILDINGS_APPL";
      }

    public static String RiskItemIncreasedBuildingsLimitSumInsuredTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__BUILDINGS_SI";
      }

    public static String RiskItemIncreasedBuildingsLimitAgreedRateTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__BUILDINGS_AGREED_RATE";
      }

    public static String RiskItemIncreasedBuildingsLimitPremiumTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__BUILDINGS_PREM";
      }

    public static String RiskItemIncreasedBuildingsLimitFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__BUILDINGS_FAP_PERC";
      }

    public static String RiskItemIncreasedBuildingsLimitFAPMinAmountTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__BUILDINGS_FAP_AMT";
      }

    public static String RiskItemIncreasedBuildingsLimitFAPMaxAmountTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__BUILDINGS_FAP_MAX_AMT";
      }

    public static String RiskItemIMaliciousDamageCheckboxId()
      {
        return "ctl00_cntMainBody_THEFT__DAMAGE_APPL";
      }

    public static String RiskItemMaliciousDamageSumInsuredTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__DAMAGE_SI";
      }

    public static String RiskItemMaliciousDamageAgreedRateTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__DAMAGE_AGREED_RATE";
      }

    public static String RiskItemMaliciousDamagePremiumTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__DAMAGE_PREM";
      }

    public static String RiskItemMaliciousDamageFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__DAMAGE_FAP_PERC";
      }

    public static String RiskItemMaliciousDamageFAPMinAmountTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__DAMAGE_FAP_AMT";
      }

    public static String RiskItemMaliciousDamageFAPMaxAmountTextboxId()
      {
        return "ctl00_cntMainBody_THEFT__DAMAGE_FAP_MAX_AMT";
      }

    public static String ExtensionsAdditionalClaimsPrepCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_25";
      }

    public static String ExtensionsAdditionalClaimsPrepLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
      }

    public static String ExtensionsAdditionalClaimsPrepRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_25";
      }

    public static String RiskItemFlatPremiumCheckboxId()
      {
        return "ctl00_cntMainBody_THEFT__FLAT_PREM_APPL";
      }

    public static String RiskCommentsTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
      }

    public static String AddPolicyNotesLabelId()
      {
        return "ctl00_cntMainBody_cntrlNotes_lbl_CONTROL__ADD_NOTES";
      }

    public static String PolicyNotesTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
      }

    public static String TheftNextButtonId()
      {
        return "ctl00_cntMainBody_btnNext";
      }

    public static String EndorsementsAddAllButtonId()
      {
        return "ctl00_cntMainBody_THEFT__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
      }

    public static String EndorsementsApplyButtonId()
      {
        return "ctl00_cntMainBody_THEFT__ENDORSEMENTS_btnApplySelection";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
