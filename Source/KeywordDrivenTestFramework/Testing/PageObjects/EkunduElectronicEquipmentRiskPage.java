
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class EkunduElectronicEquipmentRiskPage {
    public static String AddEERiskItemButtonName() {
        return "ctl00$cntMainBody$EE__ITEM$ctl03$ctl00";
    }
    public static String AddRiskItemButtonName() {
        return "ctl00$cntMainBody$EE__ITEM$ctl02$ctl00";
    }

    public static String selectElectronicEquipmentRiskLinkId() {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl07_lnkbtnSelect";
    }

    public static String PrimaryIndustryDropDownListId() {
        return "primaryIndustry";
    }

    public static String SecondaryIndustryDropDownListId() {
        return "secondaryIndustry";
    }

    public static String TertiaryIndustryDropDownListId() {
        return "tertiaryIndustry";
    }

    public static String RiskItemFAPPerSectionCheckboxId() {
        return "ctl00_cntMainBody_EE__FAP_SECTION_APPL";
    }

    public static String RiskItemFAPPerSectionMinimumPercTextboxId() {
        return "ctl00_cntMainBody_EE__FAP_SECTION_MIN_PERC";
    }

    public static String RiskItemFAPPerSectionMinimumAmountTextboxId() {
        return "ctl00_cntMainBody_EE__FAP_SECTION_MIN_AMT";
    }

    public static String RiskItemFAPPerSectionMaximumAmountTextboxId() {
        return "ctl00_cntMainBody_EE__FAP_SECTION_MAX_AMT";
    }

    public static String RiskItemFAPLightningCheckboxId() {
        return "ctl00_cntMainBody_EE__FAP_ADD_APPL";
    }

    public static String RiskItemFAPLightningMinimumPercTextboxId() {
        return "ctl00_cntMainBody_EE__FAP_ADD_MIN_PERC";
    }

    public static String RiskItemFAPLightningMinimumAmountTextboxId() {
        return "ctl00_cntMainBody_EE__FAP_ADD_MIN_AMT";
    }

    public static String RiskItemFAPLightningMaximumAmountTextboxId() {
        return "ctl00_cntMainBody_EE__FAP_ADD_MAX_AMT";
    }

    public static String ExtensionsAdditionalClaimsPreparationCheckboxId() {
        return "ctl00_cntMainBody_CHECK_25";
    }

    public static String ExtensionsAdditionalClaimsPreparationLimitofIndemnityTextboxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
    }

    public static String ExtensionsAdditionalClaimsPreparationRateTextboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_25";
    }

    public static String ExtensionsIncompatibilityCoverCheckboxId() {
        return "ctl00_cntMainBody_CHECK_30";
    }

    public static String ExtensionsIncompatibilityCoverLimitofIndemnityTextboxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_30";
    }

    public static String ExtensionsIncompatibilityCoverRateTextboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_30";
    }

    public static String ExtensionsIncompatibilityCoverFAPPercTextboxId() {
        return "ctl00_cntMainBody_FAP_PERC_30";
    }

    public static String ExtensionsIncompatibilityCoverFAPAmountTextboxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_30";
    }

    public static String ExtensionsIncreaseCostCheckboxId() {
        return "ctl00_cntMainBody_CHECK_32";
    }

    public static String ExtensionsIncreaseCostLimitofIndemnityTextboxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_32";
    }

    public static String ExtensionsIncreaseCostRateTextboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_32";
    }

    public static String ExtensionsIncreaseCostFAPPercTextboxId() {
        return "ctl00_cntMainBody_FAP_PERC_32";
    }

    public static String ExtensionsIncreaseCostFAPAmountTextboxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_32";
    }

    public static String ExtensionsIncreaseCostTimeFAPDropdownListId() {
        return "ctl00_cntMainBody_ICOW_TIME_FAP_32";
    }

    public static String ExtensionsLossofRevenueCheckboxId() {
        return "ctl00_cntMainBody_CHECK_29";
    }

    public static String ExtensionsLossofRevenueLimitofIndemnityTextboxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_29";
    }

    public static String ExtensionsLossofRevenueRateTextboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_29";
    }

    public static String ExtensionsLossofRevenueFAPPercTextboxId() {
        return "ctl00_cntMainBody_FAP_PERC_29";
    }

    public static String ExtensionsLossofRevenueFAPAmountTextboxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_29";
    }

    public static String ExtensionsReinstatementofDataCheckboxId() {
        return "ctl00_cntMainBody_CHECK_31";
    }

    public static String ExtensionsReinstatementofDataLimitofIndemnityTextboxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_31";
    }

    public static String ExtensionsReinstatementofDataRateTextboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_31";
    }

    public static String ExtensionsReinstatementofDataFAPPercTextboxId() {
        return "ctl00_cntMainBody_FAP_PERC_31";
    }

    public static String ExtensionsReinstatementofDataFAPAmountTextboxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_31";
    }

    public static String ExtensionsTelkomAccessLinesCheckboxId() {
        return "ctl00_cntMainBody_CHECK_26";
    }

    public static String ExtensionsTelkomAccessLinesLimitofIndeminityTextboxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_26";
    }

    public static String ExtensionsTelkomAccessLinesRateTextboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_26";
    }

    public static String ExtensionsTelkomAccessLinesFAPPercTextboxId() {
        return "ctl00_cntMainBody_FAP_PERC_26";
    }

    public static String ExtensionsTelkomAccessLinesFAPAmountTextboxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_26";
    }

    public static String ExtensionsTransitCheckboxId() {
        return "ctl00_cntMainBody_CHECK_27";
    }

    public static String ExtensionsTransitLimitofIndeminityTextboxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_27";
    }

    public static String ExtensionsTransitRateTextboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_27";
    }

    public static String ExtensionsTransitFAPPercTextboxId() {
        return "ctl00_cntMainBody_FAP_PERC_27";
    }

    public static String ExtensionsTransitFAPAmountTextboxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_27";
    }

    public static String ElectronicEquipmentItemsAddButtonName() {
        return "ctl00$cntMainBody$EE__ITEM$ctl02$ctl00";
    }

    public static String ItemDetailsCategoryDropdownListId() {
        return "ctl00_cntMainBody_ITEM__CATEGORY";
    }

    public static String ItemDetailsDescriptionTextboxId() {
        return "ctl00_cntMainBody_ITEM__DESCRIPTION";
    }

    public static String ItemDetailsSerialNumberTextboxId() {
        return "ctl00_cntMainBody_ITEM__SERIAL_NUM";
    }

    public static String ItemDetailsNumberofItemsTextboxId() {
        return "ctl00_cntMainBody_ITEM__NUM_ITEMS";
    }

    public static String ItemDetailsFlatPremiumTextboxId() {
        return "ctl00_cntMainBody_ITEM__FLAT_PREM_AMT";
    }

    public static String ItemDetailsFlatPremiumCheckboxId() {
        return "ctl00_cntMainBody_ITEM__FLAT_PREM_APPL";
    }

    public static String ItemDetailsSumInsuredTextboxId() {
        return "ctl00_cntMainBody_ITEM__SI";
    }

    public static String ItemDetailsRateTextboxId() {
        return "ctl00_cntMainBody_ITEM__AGREED_RATE";
    }

    public static String ItemDetailsRateButtonId() {
        return "ctl00_cntMainBody_btnRate";
    }

    public static String ElectronicEquipmentNextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String RiskItemCommentsTextboxId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }

    public static String RiskItemNotesTextboxId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
    }

    public static String RiskItemAddNotesLabelId() {
        return "ctl00_cntMainBody_cntrlNotes_lbl_CONTROL__ADD_NOTES";
    }

    public static String RiskAlarmWarrantyDropdownListId() {
        return "ctl00_cntMainBody_EE__ALARM_WARRANTY_APPL";
    }

    public static String EndorsementsSelectButtonId() {
        return "_btnShowSelect";
    }

    public static String EndorsementsAddAllButtonId() {
        return "ctl00_cntMainBody_EE__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
    }

    public static String EndorsementsApplyButtonId() {
        return "ctl00_cntMainBody_EE__ENDORSEMENTS_btnApplySelection";
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
