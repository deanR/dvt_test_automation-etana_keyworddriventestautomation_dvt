
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class EkunduMotorFleetRiskPage
  {
    public static String SelectMotorFleetRikTypeLinkId()
      {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl10_lnkbtnSelect";
      }

    public static String FleetDetailsTypeofFleetDropdownListId()
      {
        return "ctl00_cntMainBody_MF__TYPE";
      }

    public static String FleetDetailsCoverTypeDropdownListId()
      {
        return "ctl00_cntMainBody_MF__COVER";
      }

    public static String FleetMotorFleetReinsuranceLimitTextboxId()
      {
        return "ctl00_cntMainBody_MF__RI_LIMIT";
      }

    public static String FleetMotorFleetMinimumLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_MF__MIN_LOI";
      }

    public static String FleetMotorFleetMaximumLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_MF__MAX_LOI";
      }

    public static String FleetMotorFleetNumberofUnitsTextboxId()
      {
        return "ctl00_cntMainBody_MF__NUM_UNITS";
      }

    public static String FleetMotorFleetPremiumPerUnitTextboxId()
      {
        return "ctl00_cntMainBody_MF__PREMIUM_PER_UNIT";
      }

    public static String FAPOwnDamageMinimumPercentageTextboxId()
      {
        return "ctl00_cntMainBody_MF__FAP_OD_FAP_PERC";
      }

    public static String FAPOwnDamageMinimumAmountTextboxId()
      {
        return "ctl00_cntMainBody_MF__FAP_OD_FAP_AMT";
      }

    public static String FAPThirdPartyMinimumPercentageTextboxId()
      {
        return "ctl00_cntMainBody_MF__FAP_3RD_PARTY_FAP_PERC";
      }
    
    public static String FAPThirdPartyMinimumPercentageMotorChangesTextboxId()
      {
        return "ctl00_cntMainBody_MS__FAP_3RD_PARTY_MIN_PERC";
      }         
    
    public static String FAPHijackMinimumPercentageMotorChangesTextboxId()
      {
        return "ctl00_cntMainBody_MS__FAP_TH_MIN_PERC";
      }
    
    public static String FAPFireAndExplosionMinimumPercentageMotorChangesTextboxId()
      {
        return "ctl00_cntMainBody_MS__FAP_FI_MIN_PERC";
      }
    
    public static String FAPFireAndExplosionMaxPercentageMotorChangesTextboxId()
      {
        return "ctl00_cntMainBody_MS__FAP_FI_MAX_PERC";
      }
    
    public static String FAPFireAndExplosionMinimumAmountMotorChangesTextboxId()
      {
        return "ctl00_cntMainBody_MS__FAP_FI_MIN_AMT";
      }         
    
    public static String FAPFireAndExplosionMaxAmountMotorChangesTextboxId()
      {
        return "ctl00_cntMainBody_MS__FAP_FI_MAX_AMT";
      }
    
    public static String FAPThirdPartyMaximumPercentageMotorChangesTextboxId()
      {
        return "ctl00_cntMainBody_MS__FAP_3RD_PARTY_MAX_PERC";
      }
    
    public static String FAPHijackMaximumPercentageMotorChangesTextboxId()
      {
        return "ctl00_cntMainBody_MS__FAP_TH_MAX_PERC";
      }

    public static String FAPThirdPartyMinimumAmountTextboxId()
      {
        return "ctl00_cntMainBody_MF__FAP_3RD_PARTY_FAP_AMT";
      }
    
    public static String FAPHijackMinimumAmountTextboxId()
      {
        return "ctl00_cntMainBody_MS__FAP_TH_MIN_AMT";
      }

    public static String FAPTheftHijackCheckboxId()
      {
        return "ctl00_cntMainBody_MF__FAP_TFTHIJACK_APPL";
      }

    public static String FAPTheftHijackMinimumPercentageTextboxId()
      {
        return "ctl00_cntMainBody_MF__FAP_TFTHIJACK_MIN_PERC";
      }

    public static String FAPTheftHijackMinimumAmountTextboxId()
      {
        return "ctl00_cntMainBody_MF__FAP_TFTHIJACK_MIN_AMT";
      }

    public static String ExtensionsCarHireCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_180";
      }

    public static String ExtensionsCarHireConditionsDropdownListId()
      {
        return "ctl00_cntMainBody_HIRE_CONDITIONS_180";
      }

    public static String ExtensionsCarHirePremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_180";
      }

    public static String ExtensionsCarHireLimitofIndemnityDropdownListId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_180";
      }

    public static String ExtensionsContingentLiabilityCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_3";
      }

    public static String ExtensionsContingentLiabilityLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_3";
      }

    public static String ExtensionsContingentLiabilityRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_3";
      }

    public static String ExtensionsCreditShortFallCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_16";
      }

    public static String ExtensionsExcessWaiverCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_10";
      }

    public static String ExtensionsExcessWaiverPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_10";
      }

    public static String ExtensionsFireAndExplosionPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_14";
      }
    
    public static String ExtensionsFireAndExplosionRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_14";
      }

    public static String ExtensionsLossofKeysCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_5";
      }

    public static String ExtensionsLossofKeysLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_5";
      }

    public static String ExtensionsLossofKeysPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_5";
      }

    public static String ExtensionsLossofKeysFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_5";
      }

    public static String ExtensionsLossofKeysFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_5";
      }

    public static String ExtensionsParkingFacilitiesCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_12";
      }

    public static String ExtensionsParkingFacilitiesLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_12";
      }

    public static String ExtensionsRiotAndStrikeCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_11";
      }

    public static String ExtensionsRoadSideAssistCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_17";
      }

    public static String ExtensionsTheftofCarRadioCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_6";
      }

    public static String ExtensionsTheftofCarRadioRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_6";
      }

    public static String ExtensionsThirdPartyLiabilityRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_4";
      }

    public static String ExtensionsThirdPartyLiabilityPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_4";
      }

    public static String ExtensionsWreckageRemovalCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_13";
      }

    public static String ExtensionsWreckageRemovalLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_13";
      }

    public static String ExtensionsWreckageRemovalRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_13";
      }

    public static String EndorsementsSelectButtonId()
      {
        return "_btnShowSelect";
      }

    public static String EndorsementsAddAllButtonId()
      {
        return "ctl00_cntMainBody_MF__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
      }
    
    public static String MotorSectionEndorsementsAddAllButtonId()
      {
        return "ctl00_cntMainBody_MSD__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
      }

    public static String EndorsementsApplyButtonId()
      {
        return "ctl00_cntMainBody_MF__ENDORSEMENTS_btnApplySelection";
      }
    
    public static String MotorSectionEndorsementsApplyButtonId()
      {
        return "ctl00_cntMainBody_MSD__ENDORSEMENTS_btnApplySelection";
      }

    public static String RiskCommentsTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
      }

    public static String RiskAddPolicyNotesLabelId()
      {
        return "ctl00_cntMainBody_cntrlNotes_lbl_CONTROL__ADD_NOTES";
      }

    public static String RiskPolicyNotesTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
      }

    public static String MotorFleetNextButtonId()
      {
        return "ctl00_cntMainBody_btnNext";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
