/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class EkunduEhatElectronicEquipmentPage 
{
    public static String InterestedPartiesAddButtonName()
  {
      return "ctl00$cntMainBody$EE_ITEMS__EE_IP$ctl02$ctl00";
  }
    
    public static String ChangeButtonID()
  {
      return "changeButton";
  }
    
    public static String addressesSelectedRowId()
     {
        return "1";
     } 
    
    public static String ItemCategoryDropdownListId()
    {
        return "ctl00_cntMainBody_EE_ITEMS__CATEGORY";
    }
    
    public static String PremisesSumInsuredId()
    {
        return "ctl00_cntMainBody_ADDRESSES__SUM_INSURED";
    }
        
    public static String ItemDetailDescriptionTextboxId()
    {
        return "ctl00_cntMainBody_EE_ITEMS__DESCRIPTION";
    }
    
    public static String ItemDetailSerialNumberTextboxId()
    {
        return "ctl00_cntMainBody_EE_ITEMS__SERIAL_NO";
    }
    
    public static String ItemDetailDateAddedTextboxId()
    {
        return "ctl00_cntMainBody_EE_ITEMS__DATE_ADDED";
    }
    
    public static String ItemDetailSumInsuredTextboxId()
    {
        return "ctl00_cntMainBody_EE_ITEMS__SUM_INSURED";
    }
    
    public static String ItemDetailNoOfItemsTextboxId()
    {
        return "ctl00_cntMainBody_EE_ITEMS__NO_OF_ITEMS";
    }
    
    public static String ItemDetailRateTextboxId()
    {
        return "ctl00_cntMainBody_EE_ITEMS__RATE";
    }
    
    public static String FAPcheckboxId()
    {
        return "ctl00_cntMainBody_EE_ITEMS__FAP_APPL";
    }
    
    public static String FAPMinPercentageTextboxId()
    {
        return "ctl00_cntMainBody_EE_ITEMS__FAP_PERC";
    }
    
    public static String FAPMinAmountTextboxId()
    {
        return "ctl00_cntMainBody_EE_ITEMS__FAP_MIN_AMT";
    }
    
    public static String FAPMaxAmountTextboxId()
    {
        return "ctl00_cntMainBody_EE_ITEMS__FAP_MAX_AMT";
    }
    
    public static String InterestedPartiesTextboxId()
    {
        return "ctl00_cntMainBody_EE_IP__NAME";
    }
    
    public static String BusinessAllRiskAddButtonName2()
    {
       return "ctl00$cntMainBody$EE__EE_ITEMS$ctl02$ctl00";
    }
    
    public static String MultiAddressesAddButtonName()
    {
       return "ctl00$cntMainBody$EE__ADDRESSES$ctl02$ctl00";
    }
    
    public static String selectEHATElectronicEquipmentRiskLinkId()
    {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl06_lnkbtnSelect";
    }
    
    public static String PrimaryIndustryDropDownListId()
    {
        return "primaryIndustry";
    }
    
    public static String SecondaryIndustryDropDownListId()
    {
        return "secondaryIndustry";
    }
    
     public static String TertiaryIndustryDropDownListId()
    {
        return "tertiaryIndustry";
    }
     
     public static String RiskItemFAPPerSectionCheckboxId()
    {
        return "ctl00_cntMainBody_CHECK_155";
    }
     
     public static String RiskItemFAPPerSectionMinimumPercTextboxId()
    {
        return "ctl00_cntMainBody_MIN_PERC_155";
    }
     
     public static String RiskItemFAPPerSectionMinimumAmountTextboxId()
    {
        return "ctl00_cntMainBody_MIN_AMT_155";
    }
     
     public static String RiskItemFAPPerSectionMaximumAmountTextboxId()
    {
        return "ctl00_cntMainBody_MAX_AMT_155";
    }
     
     public static String RiskItemFAPLightningCheckboxId()
    {
        return "ctl00_cntMainBody_CHECK_156";
    }
     
     public static String RiskItemFAPLightningMinimumPercTextboxId()
    {
        return "ctl00_cntMainBody_MIN_PERC_156";
    }
     
     public static String RiskItemFAPLightningMinimumAmountTextboxId()
    {
        return "ctl00_cntMainBody_MIN_AMT_156";
    }
     
     public static String RiskItemFAPLightningMaximumAmountTextboxId()
    {
        return "ctl00_cntMainBody_MAX_AMT_156";
    }
     
     public static String ExtensionsAdditionalClaimsPreparationCheckboxId()
    {
        return "ctl00_cntMainBody_CHECK_157";
    }
     
     public static String ExtensionsAdditionalClaimsPreparationLimitofIndemnityTextboxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_157";
    }
     
     public static String ExtensionsAdditionalClaimsPreparationRateTextboxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_157";
    }
     
     public static String ExtensionsFinesAndPenaltiesCheckboxId()
    {
        return "ctl00_cntMainBody_CHECK_108";
    }
     
     public static String ExtensionsFinesAndPenaltiesLimitofIndemnityTextboxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_108";
    }
     
     public static String ExtensionsFinesAndPenaltiesRateTextboxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_108";
    }
     
     public static String ExtensionsFinesAndPenaltiesFAPPercTextboxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_108";
    }
     
     public static String ExtensionsFinesAndPenaltiesFAPAmountTextboxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_108";
    }
     
     public static String ExtensionsIncreaseCostCheckboxId()
    {
        return "ctl00_cntMainBody_CHECK_159";
    }
     
     public static String ExtensionsIncreaseCostLimitofIndemnityTextboxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_159";
    }
        
     public static String ExtensionsIncreaseCostRateTextboxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_159";
    }
     
     public static String ExtensionsIncreaseCostpremiumTextboxId()
    {
        return "ctl00_cntMainBody_PREMIUM_159";
    }
     
     public static String ExtensionsIncreaseCostFAPPercTextboxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_159";
    }
     
     public static String ExtensionsIncreaseCostFAPAmountTextboxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_159";
    }
     
     public static String ExtensionsIncreaseCostIndemnityPeriodDropdownListId()
    {
        return "ctl00_cntMainBody_INDEMNITY_PERIOD_159";
    }
      
     public static String ExtensionsIncreaseCostTimeFAPDropdownListId()
    {
        return "ctl00_cntMainBody_FAP_TIME_159";
    }
     
     public static String ExtensionsLossofRevenueCheckboxId()
    {
        return "ctl00_cntMainBody_CHECK_160";
    }
     
     public static String ExtensionsLossofRevenueLimitofIndemnityTextboxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_160";
    }
     
     public static String ExtensionsLossofRevenueRateTextboxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_160";
    }
     
     public static String ExtensionsLossofRevenuePremiumTextboxId()
    {
        return "ctl00_cntMainBody_PREMIUM_160";
    }
     
     public static String ExtensionsLossofRevenueFAPPercTextboxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_160";
    } 
     
     public static String ExtensionsLossofRevenueFAPAmountTextboxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_160";
    }
     
     public static String ExtensionsLossOfRevenueIndemnityPeriodDropdownListId()
    {
        return "ctl00_cntMainBody_INDEMNITY_PERIOD_160";
    }
     
     public static String ExtensionsReinstatementofDataCheckboxId()
    {
        return "ctl00_cntMainBody_CHECK_161";
    }
     
     public static String ExtensionsReinstatementofDataLimitofIndemnityTextboxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_161";
    }
     
     public static String ExtensionsReinstatementofDataRateTextboxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_161";
    }
     
     public static String ExtensionsReinstatementofDataFAPPercTextboxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_161";
    }
     
     public static String ExtensionsReinstatementofDataFAPAmountTextboxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_161";
    }
     
     public static String ExtensionsTelkomAccessLinesCheckboxId()
    {
        return "ctl00_cntMainBody_CHECK_162";
    }
     
     public static String ExtensionsTelkomAccessLinesLimitofIndeminityTextboxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_162";
    }
     
     public static String ExtensionsTelkomAccessLinesRateTextboxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_162";
    }
     
     public static String ExtensionsTelkomAccessLinesFAPPercTextboxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_162";
    }
     
     public static String ExtensionsTelkomAccessLinesFAPAmountTextboxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_162";
    }
     
     public static String ExtensionsTransitCheckboxId()
    {
        return "ctl00_cntMainBody_CHECK_163";
    }
     
     public static String ExtensionsTransitLimitofIndeminityTextboxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_163";
    }
     
     public static String ExtensionsTransitRateTextboxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_163";
    }
     
     public static String ExtensionsTransitFAPPercTextboxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_163";
    }
     
     public static String ExtensionsTransitFAPAmountTextboxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_163";
    }
     
     public static String ElectronicEquipmentItemsAddButtonName()
    {
        return "ctl00$cntMainBody$EE__ITEM$ctl02$ctl00";
    }
     
     public static String ItemDetailsCategoryDropdownListId()
    {
        return "ctl00_cntMainBody_ITEM__CATEGORY";
    }
     
     public static String ItemDetailsDescriptionTextboxId()
    {
        return "ctl00_cntMainBody_ITEM__DESCRIPTION";
    }
     
     public static String ItemDetailsSerialNumberTextboxId()
    {
        return "ctl00_cntMainBody_ITEM__SERIAL_NUM";
    }
     
     public static String ItemDetailsNumberofItemsTextboxId()
    {
        return "ctl00_cntMainBody_ITEM__NUM_ITEMS";
    }
     
     public static String ItemDetailsFlatPremiumTextboxId()
    {
        return "ctl00_cntMainBody_ITEM__FLAT_PREM_AMT";
    }
     
     public static String ItemDetailsFlatPremiumCheckboxId()
    {
        return "ctl00_cntMainBody_ITEM__FLAT_PREM_APPL";
    }
     
     public static String ItemDetailsSumInsuredTextboxId()
    {
        return "ctl00_cntMainBody_ITEM__SI";
    }
     
     public static String ItemDetailsRateTextboxId()
    {
        return "ctl00_cntMainBody_ITEM__AGREED_RATE";
    }
     
     public static String ItemDetailsRateButtonId()
    {
        return "ctl00_cntMainBody_btnRate";
    }
     
     public static String ElectronicEquipmentNextButtonId()
    {
        return "ctl00_cntMainBody_btnNext";
    }
     
     public static String RiskItemCommentsTextboxId()
    {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }
     
     public static String RiskItemNotesTextboxId()
    {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
    }
     
     public static String RiskItemAddNotesLabelId()
    {
        return "ctl00_cntMainBody_cntrlNotes_lbl_CONTROL__ADD_NOTES";
    }
     
     public static String RiskAlarmWarrantyDropdownListId()
    {
        return "ctl00_cntMainBody_EE__ALARM_WARR_APPL";
    }
     
     public static String EndorsementsSelectButtonId()
    {
        return "_btnShowSelect";
    }
     
     public static String EndorsementsAddAllButtonId()
    {
        return "ctl00_cntMainBody_EE__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
    }
     
     public static String EndorsementsApplyButtonId()
    {
        return "ctl00_cntMainBody_EE__ENDORSEMENTS_btnApplySelection";
    }
     
      public static String SearchIndustrySpanId()
     {
        return "searchIndustryIcon";
     }
   
    public static String SearchIndustryButtonId()
    {
        return "btnIndustrySearch";
    }
    
    public static String SearchIndustryTextBoxId()
    {
        return "txtSearch";
    }
    
    public static String IndustrySearchResultsSelectedRowId()
     {
        return "1";
     }
    
    public static String ItemNotes1TextboxId()
  {
      return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
  }
    
    public static String ItemNotes2TextboxId()
  {
      return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
  }
  
  public static String NotesCheckboxId()
  {
      return "ctl00_cntMainBody_cntrlNotes_CONTROL__ADD_NOTES";
  }
  
  public static String NextButtonId()
  {
      return "ctl00_cntMainBody_btnNext";
  }
  
  public static String TypeOfExposureCheckboxID()
  {
      return "ctl00_cntMainBody_EE__MPL_APPL";
  }
  
  public static String OverrideMPLTextBoxID()
  {
      return "ctl00_cntMainBody_EE__MPL_OVERRIDE";
  }
  
  public static String AssetRegisterBaseCheckboxID()
  {
      return "ctl00_cntMainBody_EE__ASSET_REGISTER_APPL";
  }
  
  public static String AssetRegRecievedDateTextBoxID()
  {
      return "ctl00_cntMainBody_EE__ASSET_REGISTER_DATE";
  }
  
  public static String TheftLimitCheckboxID()
  {
      return "ctl00_cntMainBody_EE__MPL_APPL";
  }
  
  public static String TheftLimitAmountTextBoxID()
  {
      return "ctl00_cntMainBody_EE__MPL_OVERRIDE";
  }
  
  public static String LimitOfIndemnityTextBoxID()
  {
      return "ctl00_cntMainBody_FPPL__LIMIT_OF_INDEMNITY";
  }
  
  public static String NoOfSeatsTextBoxID()
  {
      return "ctl00_cntMainBody_FPPL__NO_OF_SEATS";
  }
  
  public static String PremiumPerSeatTextBoxID()
  {
      return "ctl00_cntMainBody_FPPL__PREM_PER_SEAT";
  }
  
  public static String VehicleDescriptionTextBoxID()
  {
      return "ctl00_cntMainBody_FPPL__VEH_DESCRIPTION";
  }
  
  public static String RegNoTextBoxID()
  {
      return "ctl00_cntMainBody_FPPL__REG_NO";
  }
  
  public static String FarePayingMinPercTextBoxID()
  {
      return "ctl00_cntMainBody_FPPL__FAP_MIN_PERC";
  }
  
  public static String FarePayingMaxAMTextBoxID()
  {
      return "ctl00_cntMainBody_FPPL__FAP_MAX_AMT";
  }
  
  public static String FarePayingEndorsementsSelectButtonId()
    {
        return "_btnShowSelect";
    }
     
     public static String FarePayingEndorsementsAddAllButtonId()
    {
        return "ctl00_cntMainBody_FPPL__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
    }
     
     public static String FarePayingEndorsementsApplyButtonId()
    {
        return "ctl00_cntMainBody_FPPL__ENDORSEMENTS_btnApplySelection";
    }
     
     public static String FarePayingRiskItemCommentsTextboxId()
    {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }
     
     public static String FarePayingRiskItemNotesTextboxId()
    {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
    }
     
     public static String FarePayingNotesCheckboxId()
  {
      return "ctl00_cntMainBody_cntrlNotes_CONTROL__ADD_NOTES";
  }
  
  }
