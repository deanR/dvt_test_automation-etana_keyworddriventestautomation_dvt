
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author fnell
 */
public class EKunduFindClientPage
  {
//  <editor-fold defaultstate="collapsed" desc="Labels">
//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="TextBoxes">
    public static String ClientCodeTextBoxId()
      {         //ctl00_cntMainBody_txtClientCode
        return "ctl00_cntMainBody_txtClientCode";
      }

    public static String PolicyNumberTextBoxId()
      {
        return "ctl00_cntMainBody_txtPolicyNumber";
      }

//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="Buttons">

    public static String SearchButtonId()
      {
        return "ctl00_cntMainBody_btnSearch";
      }

//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="HoverTabs">

//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="Tabs">

//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="Iframes">

//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="Links">
    public static String ResultsSelectFirstLinkId()
      {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_lnkDetails";
      }

    public static String RenewalSelectionPolicyDetailsLinkId()
      {
        return "ctl00_cntMainBody_ClientPolicies_grdvQuotes_ctl03_lnkbtnSelect";
      }

//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="Drop Down Lists">

//  </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="Validation Items">
    public static String PageHeaderSpanId()
      {
        return "ctl00_cntMainBody_lblPageheader";
      }

//  </editor-fold> 
  }


//~ Formatted by Jindent --- http://www.jindent.com
