
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class EkunduMoneyRiskPage
  {
    public static String SelectMoneyRiskLinkId()
      {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl05_lnkbtnSelect";
      }

    public static String RiskItemTransitWarrantyCheckboxId()
      {
        return "ctl00_cntMainBody_MONEY__TRANSIT_APPL";
      }

    public static String RiskItemAlarmWarrantyDropdownListId()
      {
        return "ctl00_cntMainBody_MONEY__ALARM_WARRANTY_APPL";
      }

    public static String RiskItemTypeofSafeDropdownListId()
      {
        return "ctl00_cntMainBody_MONEY__SAFE";
      }

    public static String RiskItemCoverMajorLimitSumInsuredTextboxId()
      {
        return "ctl00_cntMainBody_MONEY__MAJOR_SI";
      }

    public static String RiskItemCoverMajorLimitPremiumTextboxId()
      {
        return "ctl00_cntMainBody_MONEY__MAJOR_PREM";
      }

    public static String RiskItemMinorLimitsInsuredPremisesCheckboxId()
      {
        return "ctl00_cntMainBody_MONEY__INS_PREMISES_APPL";
      }

    public static String RiskItemMinorLimitsInsuredPremisesNumberofPeopleTextboxId()
      {
        return "ctl00_cntMainBody_MONEY__INS_PREMISES_NUM_PEOPLE";
      }

    public static String RiskItemMinorLimitsInsuredPremisesLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_MONEY__INS_PREMISES_LOI";
      }

    public static String RiskItemMinorLimitsInsuredPremisesRateTextboxId()
      {
        return "ctl00_cntMainBody_MONEY__INS_PREMISES_RATE";
      }

    public static String RiskNextButtonId()
      {
        return "ctl00_cntMainBody_btnNext";
      }

    public static String RiskRateButtonId()
      {
        return "ctl00_cntMainBody_btnRate";
      }

    public static String ExtensionsAdditionalClaimsCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_37";
      }

    public static String RiskItemFlatPremiumCheckboxId()
      {
        return "ctl00_cntMainBody_MONEY__FLAT_PREM_APPL";
      }

    public static String ExtensionsAdditionalClaimsLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_37";
      }

    public static String ExtensionsAdditionalClaimsRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_37";
      }

    public static String ExtensionsClothingCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_38";
      }

    public static String ExtensionsClothingRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_38";
      }

    public static String ExtensionsClothingPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_38";
      }

    public static String ExtensionsClothingFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_38";
      }

    public static String ExtensionsClothingFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_38";
      }

    public static String ExtensionsDeathCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_40";
      }

    public static String ExtensionsDeathLImitofIndemnityboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_40";
      }

    public static String ExtensionsLockandKeysCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_39";
      }

    public static String ExtensionsLockandKeysRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_39";
      }

    public static String ExtensionsLockandKeysPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_39";
      }

    public static String ExtensionsLockandKeysFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_39";
      }

    public static String ExtensionsLockandKeysFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_39";
      }

    public static String ExtensionsMedicalExpensesCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_43";
      }

    public static String ExtensionsMedicalExpensesLImitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_43";
      }

    public static String ExtensionsPersonalAccidentNumberofEmployeesboxId()
      {
        return "ctl00_cntMainBody_NO_OF_EMP_45";
      }

    public static String ExtensionsRiotandStrikeCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_11";
      }

    public static String ExtensionsTotalTemporalDisabilityCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_42";
      }

    public static String ExtensionsTotalTemporalDisabilityLImitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_42";
      }

    public static String ExtensionsTotalTemporalDisabilityNumberofWeeksDropdownListId()
      {
        return "ctl00_cntMainBody_NO_OF_WEEKS_42";
      }

    public static String EndorsementsSelectButtonId()
      {
        return "_btnShowSelect";
      }

    public static String EndorsementsAddAllButtonId()
      {
        return "ctl00_cntMainBody_MONEY__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
      }

    public static String EndorsementsApplyButtonId()
      {
        return "ctl00_cntMainBody_MONEY__ENDORSEMENTS_btnApplySelection";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
