
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class CreateNewClaimPage {

    public static String caseNumberTextboxId() {
        return "ctl00_cntMainBody_txtCaseNumber";
    }

    public static String caseVersionTextboxId() {
        return "ctl00_cntMainBody_txtCaseVersion";
    }

    public static String claimHoverTabId() {
        return "s-case";
    }

    public static String newCaseLinkXPath() {
        return "//span/div/ul/li[5]/ul/li[1]/a";
    }

    public static String PublicLiabilityReserveGridId() {
        return "reserveGrid";
    }

    public static String PerilsBuildingsDetailsLinkId() {
        return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl32_lnkReserves";
//        return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl33_lnkReserves";
    }

    public static String CaseManagementLinkXPath() {
        return "//span/div/ul/li[5]/ul/li[2]/a";
    }

    public static String submitButtonId() {
        return "ctl00_cntMainBody_btnSubmit";
    }

    public static String AgreegateTotalInsuredTextboxId() {
        return "ctl00_cntMainBody_EBP_CLM_MOTOR__AGG_TOTAL_INCURRED_CR";
    }

    public static String OkButtonId() {
        return "popup_ok";
    }

    public static String claimsChampionDropDownListId() {
        return "ctl00_cntMainBody_drpAnalyst";
    }

    public static String openClaimButtonId() {
        return "ctl00_cntMainBody_btnOpen";
    }

    public static String policynumberTextBoxId() {
        return "ctl00_cntMainBody_txtPolicyNumber";
    }

    public static String lossDateTextBoxId() {
        return "ctl00_cntMainBody_Claims_FindInsuranceFile__Claim_Date";
    }

    public static String findnowButtonId() {
        return "ctl00_cntMainBody_btnFindNow";
    }

    public static String contentSpanId() {
        return "ctl00_cntMainBody_lblFindCase";
    }

    public static String SelectPolicyLinkId() {
        return "ctl00_cntMainBody_grdvInsuranceFile_ctl02_lnkDetails";
    }

    public static String RiskTypeDropdownListId() {
        return "ctl00_cntMainBody_ddlRiskType";
    }

    public static String ProgressStatusDropdownListId() {
        return "ctl00_cntMainBody_CONTROL__PROGRESS_STATUS";
    }

    public static String PrimaryCauseDropdownListId() {
        return "ctl00_cntMainBody_CONTROL__PRIMARY_CAUSE";
    }

    public static String SecondaryCauseDropdownListId() {
        return "ctl00_cntMainBody_CONTROL__SECONDARY_CAUSE";
    }

    public static String ClaimsHandlerDropdownListId() {
        return "ctl00_cntMainBody_CONTROL__HANDLER_CODE";
    }

    public static String ClaimDescriptionTextBoxId() {
        return "ctl00_cntMainBody_CONTROL__RISK_DESC";
    }

    public static String ClaimNextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String PremiumConfirmerDropdownListId() {
        return "ctl00_cntMainBody_EBP_CLM_MOTOR__PREM_CONF_BY";
    }

    public static String TertiaryCauseDropdownListId() {
        return "ctl00_cntMainBody_EBP_CLM_MOTOR__NMTR_TERTIARY_CAUSES";
    }

    public static String LoadReserveEditLinkId() {
        return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl32_lnkReserves";
//        return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl33_lnkReserves";
    }

    public static String PublicLiabilityGeneneralLinkId() {
        return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl02_lnkReserves";
    }

    public static String LiabilityProductsReserveEditLinkId() {
        return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl08_lnkReserves";
    }

    public static String ClaimLabelId() {
        return "ctl00_ClaimInfoCtrl_lblClaimNumber";
    }

    public static String ClaimReserverOwnDamageNewValueTextBoxId() {
        return "newValue02";
    }

    public static String ClaimReserverThirdPartyNewValueTextBoxId() {
        return "newValue07";
    }

    public static String ClaimReserverSalvageNewValueTextBoxId2() {
        return "newValue13";
    }

    public static String ClaimReserverSalvageNewValueTextBoxId() {
        return "newValue10";
    }

    public static String ReserverFinishButtonId() {
        return "ctl00_cntMainBody_btnFinish";
    }

    public static String PerilsSubmitButtonId() {
        return "ctl00_cntMainBody_btnSubmit";
    }

    public static String ClaimConfirmationNoButtonId() {
        return "btnGeneratePdf";
    }

    public static String ClaimConfirmationNoButtonXpath() {
        return "/html/body/div[3]/div[3]/div[1]/button[1]/span";
    }

    public static String ClaimReinsuranceOkButtonId() {
        return "ctl00_cntMainBody_ctrl_RI2007_btnNext";
    }

    public static String ClaimNumberSpanId() {
        return "ctl00_cntMainBody_lblClaimNumber";
    }

    public static String OverviewClaimNumberTextBoxId() {
        return "ctl00_cntMainBody_CONTROL__CLAIM_NUMBER";
    }

    public static String RiskTypeLabelId() {
        return "ctl00_cntMainBody_lblRiskType";
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
