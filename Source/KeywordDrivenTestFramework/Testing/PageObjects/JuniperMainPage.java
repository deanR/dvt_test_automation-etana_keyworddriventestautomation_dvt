
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author fnell
 */
public class JuniperMainPage
  {
    // <editor-fold defaultstate="collapsed" desc="Labels">
    public static String SearchAccountsLabelClassName()
      {
        return "moduleTitle";
      }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="TextBoxes">

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Buttons">

    public static String CreateAccountButtonXpath()
      {
        return "//*[@id=\"shortcuts\"]/span/span[1]/a";
      }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Tabs">
    public static String userMenuDashboardTab()
      {
        return "menuUserDashboard";
      }

    // </editor-fold>
  }


//~ Formatted by Jindent --- http://www.jindent.com
