
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import static KeywordDrivenTestFramework.Core.BaseClass.CurrentEnvironment;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author fnell
 */
public class EKunduCreateCorporateClientPage
  {
//  <editor-fold defaultstate="collapsed" desc="Tabs">

    public static String ContactID()
      {
        return "moduleTab_0_Contacts";
      }

    public static String CreateContactLinkXpath()
      {
        return "//*[@id=\"shortcuts\"]/span/span[1]/a";
      }

    public static String AddressesTabPartialLinkText()
      {
        return "Addresses";
      }

    public static String BusinessDescriptionTabPartialLinkText()
      {
        return "Business Description";
      }

    public static String AdditionalRegulatoryInformationTabPartialLinkText()
      {
        return "Additional Regulatory Information";
      }

    public static String AddAdressLinkId()
      {
        return "ctl00_cntMainBody_Addresses_hypAddress";
      }

    public static String EBPAggregatesTabLinkText()
      {
        return "EBP Aggregates";
      }

    public static String EBPTabLinkText()
      {
        return "EBP";
      }

    // </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="EditFields">

    public static String CompanyNameTextBoxId()
      {
        return "ctl00_cntMainBody_txtCompanyName";
      }

    public static String CompanyRegTextBoxId()
      {
        return "ctl00_cntMainBody_txtRegisteredName";
      }

    public static String MainContactTextBoxId()
      {
        return "ctl00_cntMainBody_txtMainContact";
      }

    public static String SuburbSearchTextBoxId()
      {
        return "searchText";
      }

    public static String AddressLine1TextBoxId()
      {
        return "txtAddressLine1";
      }

    public static String BusinessDescriptionTextBoxId()
      {
        return "ctl00_cntMainBody_GENERAL__BUS_DESCR";
      }

    public static String fileCodeTextBoxId()
      {
        return "ctl00_cntMainBody_txtFileCode";
      }

    public static String MotorFleetTotalFleetPremiumTextBoxId()
      {
        return "ctl00_cntMainBody_GENERAL__MF_AGG_TOT_F_PREM";
      }

    // </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="iFrames">
    public static String SuburbSearchFrameId()
      {
        return "BAJBOnBack";
      }

    public static String AddressFrameId()
      {
        return "modalDialog";
      }

    // </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="Drop Down Lists">            
    public static String NumberOfEmployeesDropDownId()
      {
        return "ctl00_cntMainBody_GISCorporate_Employees";
      }

    public static String PrimaryIndustryDropDownId()
      {
        return "primaryIndustry";
      }

    public static String SecondaryIndustryDropDownId()
      {
        return "secondaryIndustry";
      }

    public static String TertiaryIndustryDropDownId()
      {
        return "tertiaryIndustry";
      }

    public static String AddresssTypeDropDownId()
      {
        return "drpAddressType";
      }

    public static String CountryDropDownId()
      {
        return "drpCountry";
      }

    public static String MotorFleetTypeofPremiumDropDownListId()
      {
        return "ctl00_cntMainBody_GENERAL__MF_AGG_TOP";
      }

    // </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="Cells">
    public static String SuburbSearchResultsSelectedCellId()
      {
        return "1";
      }

    public static String IndustrySearchResultsSelectedRowId()
      {
        return "1";
      }

    public static String SuburbSearchResults2SelectedCellId()
      {
        return "2";
      }

    // </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="Tables">
    public static String SuburbSearchResultsTableId()
      {
        return "popupGrid";
      }

    // </editor-fold>

//  <editor-fold defaultstate="collapsed" desc="Buttons">

    public static String FindSuburbButtonId()
      {
        return "btnFindAddress";
      }

    public static String SuburbSearchButtonId()
      {
        return "suburbSearchSubmit";
      }

    public static String AddressAddButtonId()
      {
        return "btnAddAddress";
      }

    public static String SubmitCorporateClientButtonId()
      {
        return "ctl00_cntMainBody_btnSubmit";
      }

    public static String EditCorporateClientCreationButtonId()
      {
        return "ctl00_cntMainBody_btnEditClient";
      }
    
    public static String CloseAddressDialogButtonXPath2()
      {
        return "/html/body/div[2]/div[1]/button/span[1]";
      }

    public static String CloseAddressDialogButtonXPath()
      {

          if(CurrentEnvironment == Enums.TestEnvironments.QA)
        {
            return "//body/div[2]/div[1]/button/span";
        }
          if(CurrentEnvironment == Enums.TestEnvironments.Support)
        {
            return "//body/div[2]/div[1]/button/span";
        }
        else
        {
            return "//body/div[2]/div[1]/a";
        }
          
          
          
      }

//  </editor-fold>  

//  <editor-fold defaultstate="collapsed" desc="Validation Items">
    public static String AddCorporateClientSpanId()
      {
        return "ctl00_cntMainBody_lblNewCClient";
      }

    public static String ViewCorporateClientDetailsSpanId()
      {
        return "ctl00_cntMainBody_lblViewClient";
      }

    public static String CorporateClientReviewRequestSpanId()
      {
        return "ctl00_cntMainBody_lblReviewText";
      }

    public static String ClientIdSpanId()
      {
        return "ctl00_ClientInfoCtrl_ltClientCode";
      }

//  </editor-fold> 

//  <editor-fold defaultstate="collapsed" desc="Check Boxes">
    public static String EtanaBusinessPolicyCheckBoxId()
      {
        return "ctl00_cntMainBody_GENERAL__EBP_APPL";
      }

    public static String EtanaMotorFleetCheckBoxId()
      {
        return "ctl00_cntMainBody_GENERAL__MF_AGG_APPL";
      }

//  </editor-fold> 

  }


//~ Formatted by Jindent --- http://www.jindent.com
