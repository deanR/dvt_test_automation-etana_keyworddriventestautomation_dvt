/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class EkunduCreateNewPolicyForNewClientPageNew 
{
    
// <editor-fold defaultstate="collapsed" desc="TextBoxes">
    public static String ClientNameTextBoxId()
    {
        return "ctl00_cntMainBody_txtClientName";
    }
    
    public static String AccessoriesDescriptionTextBoxId()
    {
        return "ctl00_cntMainBody_ACCESSORIES_EQUIPMENT_ITEM__DESCR";
    }
    
    public static String AccessoriesSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_ACCESSORIES_EQUIPMENT_ITEM__SI";
    }
    
    public static String AccessoriesRateTextBoxId()
    {
        return "ctl00_cntMainBody_ACCESSORIES_EQUIPMENT_ITEM__RATE_PERC";
    }
    
    public static String AdditionalRateTextBoxId()
    {
        return "ctl00_cntMainBody_HULL__ADD_RATE_LOAD";
    }
    
    public static String AllRiskAdjPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__ADJ_PERC";
    }
    
     public static String DAPRiskItemDescriptionTestboxId()
    {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__DES";
    }
     
    public static String DAPRiskItemSumInsuredTestboxId()
    {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__AR_SI";
    }
    
    public static String MotorAdjustmentFinalPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_MS__PREM";
    }
    
    
    public static String SurveyDateTextBoxId()
    {
        return "ctl00_cntMainBody_PREM_SURVEY_REPORT__SRD";
    }
    
    public static String SurveyReferenceTextBoxId()
    {
        return "ctl00_cntMainBody_PREM_SURVEY_REPORT__SRREF";
    }
    
    public static String SurveyReportCommentsTextAreaId()
    {
        return "ctl00_cntMainBody_PREM_SURVEY_REPORT__COMMENTS";
    }
    
    public static String BuildingSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_BUILDING__BUILD_SI";
    }
    
    public static String AdjustmentPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_BUILDING__ADJ_PERC";
    }
    
    public static String ContentsAdjustmentPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_HOUSEHOLD__ADJ_PERC";
    }
    
    public static String SubsidenceSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_BUILDING__SL_SUM_INSURED";
    }
    
    public static String InterestedPartiesCommentsTextAreaId()
    {
        return "ctl00_cntMainBody_BUILD_IP__COMMENTS";
    }
    
    public static String InterestedPartiesComments3TextAreaId()
    {
        return "ctl00_cntMainBody_INTERESTED_PARTIES_ITEM__COMMENTS";
    }
    
    public static String InterestedPartiesComments2TextAreaId()
    {
        return "ctl00_cntMainBody_HH_IP__COMMENTS";
    }
    
    public static String ContentsSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_HOUSEHOLD__HH_SI";
    }
    
    public static String EndorsementNotesTextBoxId()
    {
        return "ctl00_cntMainBody_S_ENDORSEMENT__NOTES";
    }
    
    public static String EnsuredNameTextBoxId()
    {
        return "ctl00_cntMainBody_RISK_ITEM__INS_P";
    }
    
   public static String BeneficiaryNameTextBoxId()
    {
        return "ctl00_cntMainBody_BENEFICIARY__NAME";
    }
   
   public static String BeneficiarySurnameTextBoxId()
    {
        return "ctl00_cntMainBody_BENEFICIARY__SURNAME";
    }
   
   public static String BeneficiaryDOBTextBoxId()
    {
        return "ctl00_cntMainBody_BENEFICIARY__DOB";
    }
   
   public static String BeneficiaryIDNumberTextBoxId()
    {
        return "ctl00_cntMainBody_BENEFICIARY__ID_NUMBER";
    }
   
   public static String BeneficiaryPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_BENEFICIARY__PERC";
    }
   
   public static String SumInsuredDeathTextBoxId()
    {
        return "ctl00_cntMainBody_RISK_ITEM__D_SI";
    }
   
    public static String SumInsuredPermanentDisablityTextBoxId()
    {
        return "ctl00_cntMainBody_RISK_ITEM__PD_SI";
    }
    
    public static String SumInsuredTemporaryDisablityTextBoxId()
    {
        return "ctl00_cntMainBody_RISK_ITEM__TD_SI";
    }
    
    public static String SumInsuredMedicalExpensesTextBoxId()
    {
        return "ctl00_cntMainBody_RISK_ITEM__ME_SI";
    }
    
    public static String PersonalAccidentNotesTextBoxId()
    {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }
    
    
    public static String EnrouteNumberofPersonsTextBoxId()
    {
        return "ctl00_cntMainBody_PLENROUTE__NO_O_PERS";
    }
    
    public static String EnrouteRegNumberTextBoxId()
    {
        return "ctl00_cntMainBody_PLENROUTE__REG_NUMBER";
    }
    
    public static String EnrouteVINChassisNumberTextBoxId()
    {
        return "ctl00_cntMainBody_PLENROUTE__VIN_CHASSIS_NUMBER";
    }
    
    public static String EnrouteCommentsTextAreaId()
    {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }
    
    public static String NameofVesselTextBoxId()
    {
        return "ctl00_cntMainBody_VESSEL__NAME";
    }
    
    public static String MakeandModelTextBoxId()
    {
        return "ctl00_cntMainBody_VESSEL__MAKE";
    }
    
    public static String VesselDescTextBoxId()
    {
        return "ctl00_cntMainBody_VESSEL__TYPE_DESC";
    }
    
    public static String MaterialofHullTextBoxId()
    {
        return "ctl00_cntMainBody_HULL__MAT_OF_HULL";
    }
    
    public static String HullSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_HULL__SI";
    }
    
    public static String HullRateRequiredTextBoxId()
    {
        return "ctl00_cntMainBody_HULL__RATE_PERC";
    }
    
    public static String DroppingOffMotorSumTextBoxId()
    {
        return "ctl00_cntMainBody_EXTENSIONS__EXT_DROPOM_SI";
    }
    
    public static String SubmergerObjectsSumTextBoxId()
    {
        return "ctl00_cntMainBody_EXTENSIONS__EXT_SUBOB_SI";
    }
    
    public static String SubmergerObjectsRateTextBoxId()
    {
        return "ctl00_cntMainBody_EXTENSIONS__EXT_SUBOB_RATE_PERC";
    }
    
    public static String DroppingOffMotorRateTextBoxId()
    {
        return "ctl00_cntMainBody_EXTENSIONS__EXT_DROPOM_RATE_PERC";
    }
    
    public static String WaterandPleasureCraftCommentsTextAreaId()
    {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }
    
   
    public static String Engine1SumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_VESSEL__ENGINE_SI";
    }
    
    public static String Engine1RateTextBoxId()
    {
        return "ctl00_cntMainBody_VESSEL__ENGINE_R_PERC";
    }
    
    public static String VehicleModelTextBoxId()
    {
        return "ctl00_cntMainBody_MS__MODEL";
    }
    
    public static String VehicleMandMCodeTextBoxId()
    {
        return "ctl00_cntMainBody_MS__MM_CODE";
    }
    
    public static String VehicleRegNumberTextBoxId()
    {
        return "ctl00_cntMainBody_MS__REG_NUM";
    }
    
    public static String VehicleRegOwnerTextBoxId()
    {
        return "ctl00_cntMainBody_MS__REG_OWNER";
    }
    
    public static String VehicleRegDateTextBoxId()
    {
        return "ctl00_cntMainBody_MS__REG_ORIGINAL_DATE";
    }
    
    public static String VehicleSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_MS__MAX_LOI";
    }
    
    public static String VehicleExtensionsRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_192";
    }
    
    public static String VehicleExtensionsPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_192";
    }
    
    public static String SubsidenceandLandslipPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_BUILDING__SL_PREMIUM";
    }
    
    public static String ClientCodeTextBoxId()
    {
        return "ctl00_cntMainBody_txtClientCode";
    }
    
    public static String MotorCommentsTextBoxId()
    {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }
    
    public static String ReinssuranceSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl03_txtSumInsured";
    }
    
    public static String ReinssuranceUnallocatedAmountLabelId()
    {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl10_lblSumInsured";
    }
    
    public static String FAPThirdPartyMinAmountTextboxId()
    {
        return "ctl00_cntMainBody_MS__FAP_3RD_PARTY_MIN_AMT";
    }
    
    public static String FAPTFireandExplosionTextboxId()
    {
        return "ctl00_cntMainBody_MS__FAP_FI_MIN_AMT";
    }
    
     public static String MainDriverRegularDriverTextboxId()
    {
        return "ctl00_cntMainBody_MS__DRIVER_NAME";
    }
     
     public static String MainDriverDateofBirthTextboxId()
    {
        return "ctl00_cntMainBody_MS__DOB";
    }
     
     public static String MainDriverDateLicenseIssuedTextboxId()
    {
        return "ctl00_cntMainBody_MS__LICENCE_ISSUED_DATE";
    }
     
      public static String MainDriveIDNumberTextboxId()
    {
        return "ctl00_cntMainBody_MS__ID_NUMBER";
    }
    
// </editor-fold>

                
// <editor-fold defaultstate="collapsed" desc="Buttons">
    
    public static String SearchButtonId()
    {
        return "ctl00_cntMainBody_btnSearch";
    }
    
     public static String AccessoriesAddButtonName()
    {
        return "ctl00$cntMainBody$ACCESSORIES_EQUIPMENT__ACCESSORIES_EQUIPMENT_ITEM$ctl02$ctl00";
    }
    
    public static String GenerateDocButtonId()
    {
        return "btnGenerateDoc";
    }
    
    public static String NewQuoteButtonId()
    {
        return "ctl00_cntMainBody_ctrlNewQuote_btnNewQuote";
    }
    
    public static String AgentCodeButtonId()
    {
        return "ctl00_cntMainBody_btnAgentCode";
    }
    
    public static String SearchAgentButtonId()
    {
        return "ctl01_cntMainBody_btnSearch";
    }
    
    public static String BasicDetailsNextButtonName()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
    public static String AddSurveyButtonName()
    {
        return "ctl00$cntMainBody$PREMISES__PREM_SURVEY_REPORT$ctl02$ctl00";
    }
    
    public static String SurveyNextButtonId()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
    public static String SurveyCommentsNextButtonName()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
    public static String RiskCoverageNextButtonName()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
     public static String AddInterestedPartiesButtonName()
    {
        return "ctl00$cntMainBody$BUILDING__BUILD_IP$ctl02$ctl00";
    }
     
      public static String AddInterestedParties2ButtonName()
    {
        return "ctl00$cntMainBody$ACCESSORIES_EQUIPMENT__INTERESTED_PARTIES_ITEM$ctl02$ctl00";
    }
     
     public static String ContentsAddInterestedPartiesButtonName()
    {
        return "ctl00$cntMainBody$HOUSEHOLD__HH_IP$ctl02$ctl00";
    }
     
     public static String AddAllRiskButtonName()
    {
        return "ctl00$cntMainBody$ALL_RISKS__ALL_RISK_ITEM$ctl02$ctl00";
    }
     
     public static String AddAllRisk2ButtonName()
    {
        return "ctl00$cntMainBody$ALL_RISKS__ALL_RISK_ITEM$ctl03$ctl00";
    }
    
     public static String InterestedPartiesNextButtonId()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
    public static String InterestedPartiesCommentsNextButtonName()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
    public static String BuildingsNextButtonId()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
    public static String AddEndorsementButtonName()
    {
        return "ctl00$cntMainBody$PREMISES__S_ENDORSEMENT$ctl02$ctl00";
    }           
    
    public static String AddEndorsementClauseButtonId()
    {
        return "ctl00_cntMainBody_RISK_ITEM__ENDORSEMENTS_PckTemplates_AddCmd";
    }
    
    public static String EndorsementNextButtonId()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
    public static String AddRiskButtonId()
    {
        return "ctl00_cntMainBody_MultiRisk1_btnAddRisk";
    }
    
    public static String AddPersonalAccidentRiskButtonName()
    {
        return "ctl00$cntMainBody$RISK_DETAILS__RISK_ITEM$ctl02$ctl00";
    }
    
    public static String AddBeneficiaryButtonName()
    {
        return "ctl00$cntMainBody$RISK_ITEM__BENEFICIARY$ctl02$ctl00";
    }
    
    public static String BeneficiaryNextButtonId()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
    public static String PersonalAccidentRateButtonId()
    {
        return "ctl00_cntMainBody_btnRate";
    }
    
    public static String IndividualNextButtonId()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
    public static String EndorsementsSelectButtonId()
    {
        return "_btnShowSelect";
    }
    
    public static String EndorsementsAddButtonName()
    {
        return "ctl00$cntMainBody$PREMISES__S_ENDORSEMENT$ctl02$ctl00";
    }
    
    public static String AddClauseButtonId()
    {
        return "ctl00_cntMainBody_RISK_ITEM__ENDORSEMENTS_PckTemplates_AddCmd";
    }
    
    public static String ClauseApplyButtonId()
    {
        return "ctl00_cntMainBody_RISK_ITEM__ENDORSEMENTS_btnApplySelection";
    }
    
    public static String PersonalAccidentNextButtonId()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
    public static String ReinsuranceOkButtonId()
    {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_btnOk";
    }
    
    public static String RisksAddRiskButtonId()
    {
        return "ctl00_cntMainBody_MultiRisk1_btnAddRisk";
    }
    
    public static String EnrouteNextButtonId()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
    public static String HullNextButtonId()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
    public static String WaterandPleasureNextButtonId()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
    public static String PersonalLinesNextButtonId()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
    public static String AddPropFACButtonId()
    {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_btnAddFacProp";
    }
    
    public static String MakePolicyLiveButtonId()
    {
        return "ctl00_cntMainBody_btnBuy";
    }
    
    public static String MotorNextButtonId()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
    public static String MotorRateButtonId()
    {
        return "ctl00_cntMainBody_btnRate";
    }
    public static String SeaButtonName()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
    public static String PolicyRefLabelId()
    {
        return "ctl00_ClientInfoCtrl_lblPolicyRef";
    }
    
    
// </editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="Links">
    public static String SelectClientLinkId()
    {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_lnkDetails";
    }
    
    public static String SelectAgentLinkId()
    {
        return "menu_ABSA01B";
    }
    
    public static String SelectRiskTypeResultsId()
    {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl02_lnkbtnSelect";
    }
    
    public static String RiskTypePersonalAccidentLinkId()
    {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl03_lnkbtnSelect";
    }
    
    public static String RiskTypeEnrouteLinkId()
    {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl04_lnkbtnSelect";
    }
    
    public static String RiskTypeWaterandPleasureCraftLinkId()
    {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl05_lnkbtnSelect";
    }
    
    public static String SelectReinsurerLinkId()
    {
        return "ctl00_cntMainBody_grdvSearchResults_ctl03_btnSelect";
    }
    
    public static String RiskTypeMotorLinkId()
    {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl06_lnkbtnSelect";
    }
 
    public static String PersonalLinesEditLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnEdit";
    }
    
    public static String WaterandPleasureCraftRiskEditLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl06_lnkbtnEdit";
    }
    
    public static String DomesticandPropertyEditLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnEdit";
    }
    
    public static String WaterandPleasureCraftEditLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl04_lnkbtnEdit";
    }
    
     public static String PersonalAccidentEditLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl04_lnkbtnEdit";
    }
     
     public static String EnrouteRiskEditLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl05_lnkbtnEdit";
    }
     
     public static String MotorRiskEditLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnEdit";
    }
     
      public static String DomesticandProperty2EditLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnEdit";
    }
    // </editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="DropdwonLists">
    public static String SelectProductDropdownListId()
    {
        return "ctl00_cntMainBody_ctrlNewQuote_ddlProductlst";
    }
    
    public static String AdjustmentReasonDropdownListId()
    {
        return "ctl00_cntMainBody_BUILDING__ADJ_REAS";
    }
    
    public static String AllRiskAdjustmentReasonDropdownListId()
    {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__ADJ_REAS";
    }
    
    public static String VehicleModelDropdownListId()
    {
        return "model";
    }
    
    public static String MainDriverGenderDropdownListId()
    {
        return "ctl00_cntMainBody_MS__GENDER";
    }
    
    public static String MainDriverIDTypeRequiredDropdownListId()
    {
        return "ctl00_cntMainBody_MS__ID_TYPE";
    }
    
    public static String SelectPaymentTermDropdownListId()
    {
        return "ctl00_cntMainBody_POLICYHEADER__PAYMENTTERM";
    }
    
     public static String AccidentsDropdownListId()
    {
        return "ctl00_cntMainBody_MS__CFG_ACCIDENT";
    }
     
       public static String OtherClaimsDropdownListId()
    {
        return "ctl00_cntMainBody_MS__CFG_OTHER";
    }
    
    public static String SelectCollectionFrequencyDropdownListId()
    {
        return "ctl00_cntMainBody_POLICYHEADER__COLLECTIONFREQUENCY";
    }
    
    public static String SelectSurveyTypeDropdownListId()
    {
        return "ctl00_cntMainBody_PREM_SURVEY_REPORT__SRT";
    }
    
    public static String TypeofAgreementTypeDropdownListId()
    {
        return "ctl00_cntMainBody_BUILD_IP__BUILD_IP_TOA";
    }
    
     public static String TypeofAgreementType2DropdownListId()
    {
        return "ctl00_cntMainBody_INTERESTED_PARTIES_ITEM__HOUSEHOLD_IP_TOA";
    }
    
    public static String TypeofAgreement2DropdownListId()
    {
        return "ctl00_cntMainBody_HH_IP__HH_IP_TOA";
    }
    
    public static String InstitutionNameDropdownListId()
    {
        return "ctl00_cntMainBody_BUILD_IP__BUILD_IP_INST_NAME";
    }
    
    
    public static String InstitutionName3DropdownListId()
    {
        return "ctl00_cntMainBody_INTERESTED_PARTIES_ITEM__HOUSEHOLD_IP_INST_NAME";
    }
    
    public static String InstitutionName2DropdownListId()
    {
        return "ctl00_cntMainBody_HH_IP__HH_IP_INST_NAME";
    }
    
    public static String NoClaimDiscountDropdownListId()
    {
        return "ctl00_cntMainBody_HOUSEHOLD__NO_CLAIM_DISCOUNT";
    }
    
    public static String EndorsementSectionsDropdownListId()
    {
        return "ctl00_cntMainBody_S_ENDORSEMENT__SECTIONS";
    }
    
    public static String BeneficiaryIDTypeRequiredDropdownListId()
    {
        return "ctl00_cntMainBody_BENEFICIARY__ID_TYPE";
    }
    
    public static String SumInsuredNumberofWeeksDropdownListId()
    {
        return "ctl00_cntMainBody_RISK_ITEM__TD_NO_O_WEEKS";
    }
    
    public static String PlanTypeDropdownListId()
    {
        return "ctl00_cntMainBody_PLENROUTE__PLANT";
    }
    
    public static String TempDisabilityWeeksDropdownListId()
    {
        return "ctl00_cntMainBody_PLENROUTE__TD_WEEKS";
    }
    
    public static String TypeofVesselDropdownListId()
    {
        return "ctl00_cntMainBody_VESSEL__TYPE";
    }
    
    public static String MotorBoatSpeedDropdownListId()
    {
        return "ctl00_cntMainBody_VESSEL__MAX_SPEED";
    }
    
    public static String NumberofEnginesDropdownListId()
    {
        return "ctl00_cntMainBody_VESSEL__NO_OF_ENG";
    }
    
    public static String EngineTypeDropdownListId()
    {
        return "ctl00_cntMainBody_VESSEL__ENGINE_TYP";
    }
    
    public static String HullUseDropdownListId()
    {
        return "ctl00_cntMainBody_HULL__HULL_USE";
    }
    
    public static String TypeofVehicleDropdownListId()
    {
        return "ctl00_cntMainBody_MS__VEHICLE_TYPE";
    }
    
    public static String NoClaimRebateDropdownListId()
    {
        return "ctl00_cntMainBody_MS__NCR_REBATE";
    }
    
    public static String VehicleClassofUseDropdownListId()
    {
        return "ctl00_cntMainBody_MS__CLASS_USE";
    }
    
     public static String NATISCodeDropdownListId()
    {
        return "ctl00_cntMainBody_MS__NATIS_CODE";
    }
    
     public static String VehicleMakeDropdownListId()
    {
        return "make";
    }
     
     public static String BasisofSettlementDropdownListId()
    {
        return "ctl00_cntMainBody_MS__VEH_COVER_METHOD";
    }
     
     public static String VehicleYearDropdownListId()
    {
        return "year";
    }
    
    public static String VehicleCoverTypeDropdownListId()
    {
        return "ctl00_cntMainBody_MS__COVER_TYPE";
    }
    
    public static String TypeofResidenceDropdownListId()
    {
        return "ctl00_cntMainBody_PREMISES__TOR";
    }
    
    public static String TypeofConstructionDropdownListId()
    {
        return "ctl00_cntMainBody_PREMISES__TOC";
    }
    
    public static String OccupancyDropdownListId()
    {
        return "ctl00_cntMainBody_PREMISES__OCC";
    }
    
    public static String LocalityDropdownListId()
    {
        return "ctl00_cntMainBody_PREMISES__LOC";
    }
    
    public static String ContentsAdjustmentReasonDropdownListId()
    {
        return "ctl00_cntMainBody_HOUSEHOLD__ADJ_REAS";
    }
   
    public static String OccupationDropdownListId()
    {
        return "ctl00_cntMainBody_RISK_ITEM__OCCUPATION_LIST";
    }
    
    public static String DAPRiskItemCategoryDropdownListId()
    {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__CAT";
    }
    
    public static String ReinsuranceBandDropdownListId()
    {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_ddlReinsurance";
    }
    // </editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="CheckBoxes">
    public static String CheckBuildingCheckBoxId()
    {
        return "ctl00_cntMainBody_PREMISES__BUILDINGS_APPL";
    }
    
    public static String WaterSkierLiabilityCheckBoxId()
    {
        return "ctl00_cntMainBody_EXTENSIONS__EXT_WATLIB_APPL";
    }
    
    public static String DroppingOffOfMotorCheckBoxId()
    {
        return "ctl00_cntMainBody_EXTENSIONS__EXT_DROPOM_APPL";
    }
    
    public static String SubmergeObjectsExtCheckBoxId()
    {
        return "ctl00_cntMainBody_EXTENSIONS__EXT_SUBOB_APPL";
    }
    
     public static String VesselSelfBuiltCheckBoxId()
    {
        return "ctl00_cntMainBody_HULL__VES_SB_APPL";
    }
    
    public static String CheckContentsCheckBoxId()
    {
        return "ctl00_cntMainBody_PREMISES__CONT_APPL";
    }
    
    public static String CheckAllRiskItemsCheckBoxId()
    {
        return "ctl00_cntMainBody_PREMISES__ARISKS_APPL";
    }
    
    public static String SecurityDiscountsLevel5AlarmCheckBoxId()
    {
        return "ctl00_cntMainBody_HOUSEHOLD__SAIDSA_ALARM_APPL";
    }
    
    public static String SecurityDiscountsSecurityDoorsCheckBoxId()
    {
        return "ctl00_cntMainBody_HOUSEHOLD__SEC_DOORS_APPL";
    }
    
    public static String ExtensionsWaterSkierDoorsCheckBoxId()
    {
        return "ctl00_cntMainBody_EXTENSIONS__EXT_WATLIB_APPL";
    }
    
    public static String ExtensionsCreditShortFallCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_122";
    }
    
    public static String DroppingOffMotorCheckBoxId()
    {
        return "ctl00_cntMainBody_EXTENSIONS__EXT_DROPOM_APPL";
    }
    
    public static String SubmergeObjectsCheckBoxId()
    {
        return "ctl00_cntMainBody_EXTENSIONS__EXT_SUBOB_APPL";
    }
    
    
    // </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Tabs">
    public static String OverviewLinkText()
    {
        return "Overview";
    }
    
     
    public static String ReinsuranceDetailsTabLinkText()
    {
        return "Reinsurance Details";
    }
// </editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="Select">
    public static String SelectClauseDropdownListId()
    {
        return "ctl00_cntMainBody_RISK_ITEM__ENDORSEMENTS_PckTemplates_AvailList";
    }
   
    
    
    // </editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="Cells">
    public static String SuburbSearchResultsSelectedCellId()
     {
        return "1";
     }
    
    public static String VehicleModelSelectedRowId()
     {
        return "1";
     }
        // </editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="Tables">
    public static String VehicleModelSearchResultsTableId()
     {
        return "popupGrid";
     }
    
    
    
      // </editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="Spans">
    public static String SearchVehicleModelIconId()
     {
        return "search";
     }
    
    public static String VehiclesSourceDropdownListId()
     {
        return "source";
     }
    
    public static String PolicyCreationConfirmationSpanId()
     {
        return "ctl00_cntMainBody_lblTransactionSubHeading";
     }
      // </editor-fold>
    
}