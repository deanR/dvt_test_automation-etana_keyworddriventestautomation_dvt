
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class EkunduGlassRiskPage
  {
    public static String SelectGlassRiskLinkId()
      {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl04_lnkbtnSelect";
      }

    public static String RiskItemSumInsuredTextboxId()
      {
        return "ctl00_cntMainBody_GLASS__SI";
      }

    public static String RiskItemRateButtonId()
      {
        return "ctl00_cntMainBody_btnRate";
      }

    public static String NextButtonId()
      {
        return "ctl00_cntMainBody_btnNext";
      }

    public static String ExtensionsAdditionalClaimsCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_25";
      }

    public static String ExtensionsAdditionalClaimsLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
      }

    public static String ExtensionsAdditionalClaimsRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_25";
      }

    public static String ExtensionsRiotandStrikesCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_35";
      }

    public static String ExtensionsRiotandStrikesPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_35";
      }

    public static String ExtensionsRiotandStrikesFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_35";
      }

    public static String ExtensionsRiotandStrikesFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_35";
      }

    public static String ExtensionsSpecialReinstatementCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_36";
      }

    public static String ExtensionsSpecialReinstatementRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_36";
      }

    public static String ExtensionsSpecialReinstatementFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_36";
      }

    public static String ExtensionsSpecialReinstatementFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_36";
      }

    public static String EndorsementsSelectButtonId()
      {
        return "_btnShowSelect";
      }

    public static String EndorsementsAddAllButtonId()
      {
        return "ctl00_cntMainBody_GLASS__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
      }

    public static String EndorsementsApplyButtonId()
      {
        return "ctl00_cntMainBody_GLASS__ENDORSEMENTS_btnApplySelection";
      }

    public static String GlassNotesTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
      }

    public static String NotesAddNotesCheckboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__ADD_NOTES";
      }

    public static String NotesPolicyNotesTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
      }
    
     public static String AddRiskItemButtonName()
      {
        return "ctl00$cntMainBody$BC__BC_ITEMS$ctl02$ctl00";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
