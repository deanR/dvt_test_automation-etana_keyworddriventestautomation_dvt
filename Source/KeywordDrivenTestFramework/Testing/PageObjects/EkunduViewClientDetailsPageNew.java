/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author fnell
 */
public class EkunduViewClientDetailsPageNew 
{
// <editor-fold defaultstate="collapsed" desc="Labels">
    
    public static String QuoteReferenceLabelId()
    {
        return "ctl00_ClientInfoCtrl_lblPolicyRef";
    }
    
    public static String InsuredPerilsDataTableTitleLabelId()
    {
        return "FireRateLDTitle";
    }
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="TextBoxes">
     public static String MotorTradersExternalExtensionsContingentLiabilityLOITextBoxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_254";
    }
     
    public static String MotorTradersExternalExtensionsContingentLiabilityRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_254";
    }
    
    public static String MotorTradersExternalExtensionsContingentLiabilityFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_254";
    }
    
     public static String MotorTradersExternalExtensionsContingentLiabilityFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_254";
    }
     
       public static String MotorTradersExternalExtensionsLossofKeysFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_255";
    }
    
     public static String MotorTradersExternalExtensionsLossofKeysFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_255";
    }
     
     public static String MotorTradersExternalExtensionsLossofUsePremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_249";
    }
    
    public static String MotorTradersExternalExtensionsLossofUseFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_249";
    }
    
     public static String MotorTradersExternalExtensionsLossofUseFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_249";
    }
     
      
    public static String MotorTradersExternalExtensionsMotorCycleFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_256";
    }
    
     public static String MotorTradersExternalExtensionsMotorCycleFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_256";
    }
     
      public static String MotorTradersExternalExtensionsPassangerLiabilityLOITextBoxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_251";
    }
     
    public static String MotorTradersExternalExtensionsPassangerLiabilityRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_251";
    }
    
    public static String MotorTradersExternalExtensionsPassangerLiabilityFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_251";
    }
    
     public static String MotorTradersExternalExtensionsPassangerLiabilityFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_251";
    }
     
     public static String MotorTradersExternalExtensionsSocialDomesticPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_248";
    }
    
    public static String MotorTradersExternalExtensionsSocialDomesticFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_248";
    }
    
     public static String MotorTradersExternalExtensionsSocialDomesticFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_248";
    }
     
     public static String MotorTradersExternalExtensionsSpecialTypeVehiclesFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_257";
    }
    
     public static String MotorTradersExternalExtensionsSpecialTypeVehiclesFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_257";
    }
     
      public static String MotorTradersExternalExtensionsUnauthorisedUsePremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_250";
    }
    
    public static String MotorTradersExternalExtensionsUnauthorisedUseFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_250";
    }
    
     public static String MotorTradersExternalExtensionsUnauthorisedUseFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_250";
    }
     
      public static String MotorTradersExternalExtensionsVehicleLentPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_253";
    }
    
    public static String MotorTradersExternalExtensionsVehicleLentFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_253";
    }
    
     public static String MotorTradersExternalExtensionsVehicleLentFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_253";
    }
     
    public static String MotorTradersExternalExtensionsWindscreenCoverPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_252";
    }
    
    public static String MotorTradersExternalExtensionsWindscreenCoverFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_252";
    }
    
     public static String MotorTradersExternalExtensionsWindscreenCoverFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_252";
    }
     
     public static String MotorTradersExternalExtensionsWreckageRemovalPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_261";
    }
    
    public static String MotorTradersExternalExtensionsWreckageRemovalFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_261";
    }
    
     public static String MotorTradersExternalExtensionsWreckageRemovalFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_261";
    }
    
    public static String MotorTradersExtensionsCarHoistsPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_264";
    }
    
     public static String MotorTradersExtensionsCarHoistsFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_264";
    }
     
     public static String MotorTradersExtensionsCarHoistsFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_264";
    }
     
     public static String MotorTradersExtensionsLossofKeysPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_265";
    }
    
     public static String MotorTradersExtensionsLossofKeysFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_265";
    }
     
     public static String MotorTradersExtensionsLossofKeysFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_265";
    }
     
     public static String MotorTradersExtensionsWindscreenPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_252";
    }
    
     public static String MotorTradersExtensionsWindscreenFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_252";
    }
     
     public static String MotorTradersExtensionsWindscreenFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_252";
    }
     
     public static String MotorTradersExtensionsWorkAwayPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_263";
    }
    
     public static String MotorTradersExtensionsWorkAwayFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_263";
    }
     
     public static String MotorTradersExtensionsWorkAwayFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_263";
    }
     
          
     public static String MotorTradersExtensionsWreckagePremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_261";
    }
    
     public static String MotorTradersExtensionsWreckageFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_261";
    }
     
     public static String MotorTradersExtensionsWreckageFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_261";
    }
            
    public static String SubSectionASumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_SI";
    }
    
    public static String SubSectionARateTextBoxId()
    {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_RATE";
    }
    
    public static String SubSectionAPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_PREMIUM";
    }
    
     public static String SubSectionAFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_FAP_PERC";
    }
     
      public static String SubSectionAFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_FAP_AMOUNT";
    }
      
        public static String SubSectionBFAPPercTextBoxId()
    {
        return "ctl00_cntMainBody_MT__SUB_SECTION_B_FAP_PERC";
    }
     
      public static String SubSectionBFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_MT__SUB_SECTION_FAP_AMOUNT";
    }
      
    public static String SubSectionAAnnualWageAmountTextBoxId()
    {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_ANNUAL_WAGE_AMOUNT";
    }
    
    public static String SubSectionBLimitofIndemnityTextBoxId()
    {
        return "ctl00_cntMainBody_MT__SUB_SECTION_B_LOI";
    }
    
     public static String SubSectionBRateTextBoxId()
    {
        return "ctl00_cntMainBody_MT__SUB_SECTION_B_RATE";
    }
     
    public static String CoverVoluntaryFAPPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_MT__VOLUNTARY_FAP_PERC";
    }
    
    public static String CoverVoluntaryFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_MT__VOLUNTARY_FAP_AMOUNT";
    }
    
     public static String PolicyRefTextBoxId()
    {
        return "ctl00_cntMainBody_txtReference";
    }
    
    public static String CoverStartDateTextBoxId()
    {
        return "ctl00_cntMainBody_POLICYHEADER__COVERSTARTDATE";
    }
   
    public static String CoverEndDateTextBoxId()
    {
        return "ctl00_cntMainBody_POLICYHEADER__COVERENDDATE";
    }
    
    public static String IndustryTextBoxId()
    {
        return "industry";
    }  
    
    public static String RatesFinalAgreedRateTextBoxId()
    {
        return "ctl00_cntMainBody_GROUP_FIRE__AVG_RATE_AGREED_RATE";
    } 

    public static String TotalSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__TOTAL_SI";
    } 
    
    public static String TotalPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__TOTAL_PREM";
    } 
    
    public static String FireRiskDataBuildingsRiskSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL1_SI";
    } 
    
    public static String FireRiskDataBuildingsRiskPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL1_PREM";
    } 
    
    public static String FireRiskDataRentRiskSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL2_SI";
    } 
    
    public static String FireRiskDataRentRiskPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL2_PREM";
    } 
    
    public static String FireRiskDataPlantRiskSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL3_SI";
    } 
    
    public static String FireRiskDataPlantRiskPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL3_PREM";
    } 
    
    public static String FireRiskDataStockRiskSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL4_SI";
    } 
    
    public static String FireRiskDataStockRiskPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL4_PREM";
    } 
    
    public static String FireRiskDataPLRiskSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL7_SI";
    } 
    
    public static String FireRiskDataPLRiskPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL7_PREM";
    } 
    
    public static String FireRiskNumberOfMonthsTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL2_NO_MONTHS";
    } 
    
    public static String FireBuildingsExcalationPercentTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__BUILDINGS_ESC_PERC";
    }
    
    public static String FireBuildingsExcalationPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL1_INFL_APPL";
    }
    
    public static String FirePlantMachineryExcalationPercentTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__PLANT_MACH_ESCPERC";
    }
    
    public static String FirePlantMachineryExcalationPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL3_INFL_APPL";
    }
    
    public static String MiscItemDescriptionTextBoxId()
    {
        return "ctl00_cntMainBody_FI_RISK_MICS_ITEMS__DESCRIP";
    }
    public static String MiscItemSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_FI_RISK_MICS_ITEMS__SI";
    }
    
    public static String FireMinimumPercentageFirstAmountPayableTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__FAP_MIN_PERC";
    }
    
    public static String FireMinimumAmountFirstAmountPayableTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__FAP_MIN_AMT";
    }
    
    public static String FireMaximumAmountFirstAmountPayableTextBoxId()
    {
        return "ctl00_cntMainBody_FIRE__FAP_MAX_AMT";
    }
    
    public static String FireClaimsPrepLimitOfIndemnityTextBoxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
    }
    public static String FireClaimsPrepRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_25";
    }
    
    public static String FireLeakgeLimitOfIndemnityTextBoxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_101";
    }
    public static String FireLeakgeRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_101";
    }
    
    public static String FireRiotPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_35";
    }
    
    public static String FireSubsidenceLimitOfIndemnityTextBoxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_100";
    }
    
    public static String FireSubsidenceRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_100";
    }
        
    
    public static String BIOverviewConversionFactorTextBoxId()
    {
        return "ctl00_cntMainBody_BI__ADJ_CONVERSION_FACTOR";
    }
    
    public static String BICoverGrossProfitBasisSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_BI__GROSS_PROFIT_BASIS_SI";
    }
    
    public static String BICoverGrossProfitBasisRateTextBoxId()
    {
        return "ctl00_cntMainBody_BI__GROSS_PROFIT_BASIS_RATE";
    }
    
    public static String BICoverGrossProfitBasisPRemiumTextBoxId()
    {
        return "ctl00_cntMainBody_BI__GROSS_PROFIT_BASIS_PREM";
    }
    
    public static String EkunduAppVersionLabelId()
    {
        return "ctl00_app_ver";
    }
    
    
    public static String BICoverGrossRentalsMonthsTextBoxId()
    {
        return "ctl00_cntMainBody_BI__GROSS_RENTALS_NUM";
    }
    
    public static String BICoverGrossRentalsSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_BI__GROSS_RENTALS_SI";
    }
    
    public static String BICoverGrossRentalsRateTextBoxId()
    {
        return "ctl00_cntMainBody_BI__GROSS_RENTALS_RATE";
    }
    
    public static String BICoverGrossRentalsPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_BI__GROSS_RENTALS_PREM";
    }
    
    
    public static String BICoverRevenueSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_BI__REVENUE_SI";
    }
    
    public static String BICoverRevenueRateTextBoxId()
    {
        return "ctl00_cntMainBody_BI__REVENUE_RATE";
    }
    
    public static String BICoverRevenuePremiumTextBoxId()
    {
        return "ctl00_cntMainBody_BI__REVENUE_PREM";
    }
    
    
    public static String BICoverAdditionalIncreaseCostOfWorkingSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_BI__ICOW_INSURED";
    }
    
    public static String BICoverAdditionalIncreaseCostOfWorkingRateTextBoxId()
    {
        return "ctl00_cntMainBody_BI__ICOW_INSURED";
    }
    
    public static String BICoverAdditionalIncreaseCostOfWorkingPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_BI__ICOW_PREM";
    }
    
    
    public static String BICoverWagesSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_BI__WAGES_SI";
    }
    
    public static String BICoverWagesRateTextBoxId()
    {
        return "ctl00_cntMainBody_BI__WAGES_RATE";
    }
    
    public static String BICoverWagesPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_BI__WAGES_PREM";
    }
    
    public static String BIFirstAmountPayableMinPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_BI__FAP_MIN_PERC";
    }
    
     public static String MoneyCoverMajorLimitSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_MONEY__MAJOR_SI";
    }
     
     public static String MoneyCoverMajorLimitPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_MONEY__MAJOR_PREM";
    }
    
    public static String BIFirstAmountPayableMinAmountTextBoxId()
    {
        return "ctl00_cntMainBody_BI__FAP_MIN_AMT";
    }
    
    public static String BIFirstAmountPayableMaxAmountTextBoxId()
    {
        return "ctl00_cntMainBody_BI__FAP_MAX_AMT";
    }
    
    
    public static String BIExtensionsAccidentalDamageLimitTextBoxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_108";
    }
    
    public static String BIExtensionsAccidentalDamageRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_108";
    }
    
    public static String BIExtensionsAccidentalDamageFAPPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_108";
    }
    
    public static String BIExtensionsAccidentalDamageFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_108";
    }
    
    public static String BIExtensionsClaimsLimitTextBoxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
    }
    
    public static String BIExtensionsClaimsRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_25";
    }
    
    public static String BIExtensionsFinesLimitTextBoxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_107";
    }
    
    public static String BIExtensionsFinesRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_107";
    }
    
    public static String BIExtensionsFinesFAPPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_107";
    }
    
    public static String BIExtensionsFinesFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_107";
    }
    
    
    public static String BIExtensionsPreventionOfAccesRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_105";
    }
    
    public static String BIExtensionsPreventionOfAccesPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_105";
    }
    
    public static String BIExtensionsPreventionOfAccesFAPPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_105";
    }
    
    public static String BIExtensionsPreventionOfAccesFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_105";
    }
    
    
    public static String BIExtensionsPublicTelecommunicationsExtendedRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_104";
    }
    
    public static String BIExtensionsPublicTelecommunicationsExtendedPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_104";
    }
    
    public static String BIExtensionsPublicTelecommunicationsExtendedFAPPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_104";
    }
    
    public static String BIExtensionsPublicTelecommunicationsExtendedFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_104";
    }
    
    
    public static String BIExtensionsPublicUtilitiesExtendedRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_113";
    }
    
    public static String BIExtensionsPublicUtilitiesExtendedPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_113";
    }
    
    public static String BIExtensionsPublicUtilitiesExtendedFAPPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_113";
    }
    
    public static String BIExtensionsPublicUtilitiesExtendedFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_113";
    }
    
    
    public static String BIExtensionsSpecifiedCustomersRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_112";
    }
    
    public static String BIExtensionsSpecifiedCustomersPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_112";
    }
    
    public static String BIExtensionsSpecifiedCustomersFAPPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_112";
    }
    
    public static String BIExtensionsSpecifiedCustomersFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_112";
    }
    
    
    public static String BIExtensionsSpecifiedSuppliersRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_110";
    }
    
    public static String BIExtensionsSpecifiedSuppliersPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_110";
    }
    
    public static String BIExtensionsSpecifiedSuppliersFAPPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_110";
    }
    
    public static String BIExtensionsSpecifiedSuppliersFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_110";
    }
    
    
    public static String BIExtensionsUnSpecifiedCustomersRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_111";
    }
    
    public static String BIExtensionsUnSpecifiedCustomersPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_111";
    }
    
    public static String BIExtensionsUnSpecifiedCustomersFAPPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_111";
    }
    
    public static String BIExtensionsUnSpecifiedCustomersFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_111";
    }
    
    
    public static String BIExtensionsUnSpecifiedSuppliersLimitOfIndemnityTextBoxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_109";
    }
    
    public static String BIExtensionsUnSpecifiedSuppliersRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_109";
    }
    
    public static String BIExtensionsUnSpecifiedSuppliersFAPPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_109";
    }
    
    public static String BIExtensionsUnSpecifiedSuppliersFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_109";
    }
    
    public static String BISpecifiedSuppliersSupplierNameTextBoxId()
    {
        return "ctl00_cntMainBody_SPEC_SUPPLIER__SPEC_NAME";
    }
    
    public static String BISpecifiedSuppliersSupplierPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_SPEC_SUPPLIER__SPEC_PERC";
    }
    
     public static String BISpecifiedCustomersCustomerNameTextBoxId()
    {
        return "ctl00_cntMainBody_SPEC_CUSTOMER__SPEC_NAME";
    }
    
    public static String BISpecifiedCustomersCustomerPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_SPEC_CUSTOMER__SPEC_PERC";
    }
    
    public static String BCBuildingsCombinedSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_BC__BC_SI";
    }
    
    public static String BCBuildingsCombinedAgreedRateTextBoxId()
    {
        return "ctl00_cntMainBody_BC__BC_AGREED_RATE";
    }
 
    
    public static String BCBuildingsCombinedEscalationPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_BC__ESC_PERC";
    }
    
    public static String BCBuildingsCombinedInflation1stYearPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_BC__INFL_1YR_PERC";
    }
    
    public static String BCBuildingsCombinedInflation2ndYearPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_BC__INFL_2YR_PERC";
    }
    
    public static String BCAdditionalRentSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_BC__ADD_RENT_SI";
    }
    
    public static String BCAdditionalRentRateTextBoxId()
    {
        return "ctl00_cntMainBody_BC__ADD_RENT_RATE";
    }
    
    public static String BCMiscellaneousItemDescriptionTextBoxId()
    {
        return "ctl00_cntMainBody_BC_ITEMS__ITEMS_DESC";
    }
    
    public static String BCMiscellaneousItemSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_BC_ITEMS__ITEMS_SI";
    }
    
    public static String BCMiscellaneousItemRateTextBoxId()
    {
        return "ctl00_cntMainBody_BC_ITEMS__ITEMS_RATE";
    }
    
    public static String BCFirstAmountPayablePercentageTextBoxId()
    {
        return "ctl00_cntMainBody_BC__FAP_MIN_PERC";
    }
    
    public static String BCFirstAmountPayableAmountTextBoxId()
    {
        return "ctl00_cntMainBody_BC__FAP_MIN_AMT";
    }
    
    public static String BCFirstAmountPayableMaximumTextBoxId()
    {
        return "ctl00_cntMainBody_BC__FAP_MAX_AMT";
    }
    
    public static String BCExtentionsClaimsPreparationLimitTextBoxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
    }
    
    public static String BCExtentionsRiotPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_35";
    }
    
    public static String BCExtentionsRiotFAPPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_35";
    }
    
    public static String BCExtentionsRiotFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_35";
    }
    
    public static String BCExtentionsSubsidenceLimitTextBoxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_100";
    }
    
    public static String BCExtentionsSubsidenceRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_100";
    }
    
    public static String BCExtentionsSubsidenceFAPPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_100";
    }
    
    public static String BCExtentionsSubsidenceFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_100";
    }
    
    public static String AROutstandingDebitBalancesSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_AR__OD_SI";
    }
    
    public static String ARFirstAmountPayableMinPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_AR__FAP_MIN_PERC";
    }
    
    public static String ARFirstAmountPayableMinAmountTextBoxId()
    {
        return "ctl00_cntMainBody_AR__FAP_MIN_AMT";
    }
    
    public static String ARFirstAmountPayableMaxAmountTextBoxId()
    {
        return "ctl00_cntMainBody_AR__FAP_MAX_AMT";
    }
    
    public static String ARExtensionsClaimsLimitTextBoxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
    }
    
    public static String ARExtensionsRiotPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_35";
    }
    
    public static String ARExtensionsFAPPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_35";
    }
    
    public static String ARExtensionsFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_35";
    }
    
    public static String OCCoverOfficeContentsSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_OC__OC_SI";
    }
    
    public static String OCCoverOfficeContentsPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_OC__OC_PREM";
    }
    
    public static String OCCoverLossOfDocumentsSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_OC__LOD_SI";
    }
    
    public static String OCCoverLiabilityForDocumentsSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_OC__LIABDSI";
    }
    
    public static String OCCoverTheftNonForcibleSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_OC__THEFTNON_SI";
    }
    
    public static String OCCoverTheftForcibleSumInsuredTextBoxId()
    {
        return "ctl00_cntMainBody_OC__THEFT_SI";
    }
    
    
    public static String OCFirstAmountPayableOfficeContentsMinPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_OC__OC_FAP_MIN_PERC";
    }
    
    public static String OCFirstAmountPayableOfficeContentsMinAmountTextBoxId()
    {
        return "ctl00_cntMainBody_OC__OC_FAP_MIN_AMT";
    }
    
    public static String OCFirstAmountPayableOfficeContentsMaxAmountTextBoxId()
    {
        return "ctl00_cntMainBody_OC__OC_FAP_MAX_AMT";
    }
    
    public static String OCFirstAmountPayableLossOfDocumentsMinPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_OC__LOD_MIN_PERC";
    }
    
    public static String OCFirstAmountPayableLossOfDocumentsMinAmountTextBoxId()
    {
        return "ctl00_cntMainBody_OC__LOD_MIN_AMT";
    }
    
    public static String OCFirstAmountPayableLossOfDocumentsMaxAmountTextBoxId()
    {
        return "ctl00_cntMainBody_OC__LOD_MAX_AMT";
    }
    
    
    public static String OCFirstAmountPayableLiabilityForDocumentsMinPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_OC__LIABDMIN_PERC";
    }
    
    public static String OCFirstAmountPayableLiabilityForDocumentsMinAmountTextBoxId()
    {
        return "ctl00_cntMainBody_OC__LIABDMIN_AMT";
    }
    
    public static String OCFirstAmountPayableLiabilityForDocumentsMaxAmountTextBoxId()
    {
        return "ctl00_cntMainBody_OC__LIABDMAX_AMT";
    }
    
    
    public static String OCFirstAmountPayableTheftNonForcibleMinPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_OC__THEFTNON_MIN_PERC";
    }
    
    public static String OCFirstAmountPayableTheftNonForcibleMinAmountTextBoxId()
    {
        return "ctl00_cntMainBody_OC__THEFTNON_MIN_AMT";
    }
    
    public static String OCFirstAmountPayableTheftNonForcibleMaxAmountTextBoxId()
    {
        return "ctl00_cntMainBody_OC__THEFTNON_MAX_AMT";
    }
    
    
    public static String OCFirstAmountPayableTheftForcibleMinPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_OC__THEFT_MIN_PERC";
    }
    
    public static String OCFirstAmountPayableTheftForcibleMinAmountTextBoxId()
    {
        return "ctl00_cntMainBody_OC__THEFT_MIN_AMT";
    }
    
    public static String OCFirstAmountPayableTheftForcibleMaxAmountTextBoxId()
    {
        return "ctl00_cntMainBody_OC__THEFT_MAX_AMT";
    }
    
    public static String OCExtensionsAdditionalIncreasedCostLimitTextBoxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_118";
    }
    
    public static String OCExtensionsAdditionalIncreasedCostRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_118";
    }
    
    public static String OCExtensionsAdditionalIncreasedCostFAPPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_118";
    }
    
    public static String OCExtensionsAdditionalIncreasedCostFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_118";
    }
    
    public static String OCExtensionsClaimsLimitTextBoxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
    }
    
    public static String OCExtensionsClaimsRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_25";
    }
    
    public static String OCExtensionsMaliciousDamageLimitTextBoxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_119";
    }
    
    public static String OCExtensionsMaliciousDamageRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_119";
    }
    
    public static String OCExtensionsMaliciousDamageFAPPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_119";
    }
    
    public static String OCExtensionsMaliciousDamageFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_119";
    }
    
    public static String OCExtensionsRiotPremiumTextBoxId()
    {
        return "ctl00_cntMainBody_PREMIUM_35";
    }
    
    public static String OCExtensionsRiotFAPPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_35";
    }
    
    public static String OCExtensionsRiotFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_35";
    }
    
    public static String ADCoverAccidentalDamageFullValueTextBoxId()
    {
        return "ctl00_cntMainBody_AD__EVENT1_FULL_VALUE";
    }
    
    public static String ADCoverAccidentalDamageFirstLossTextBoxId()
    {
        return "ctl00_cntMainBody_AD__EVENT1_FIRST_LOSS";
    }
    
    public static String ADCoverLeakageFirstLossTextBoxId()
    {
        return "ctl00_cntMainBody_AD__EVENT2_FIRST_LOSS";
    }
    
    public static String ADFirstAmountPayableAccidentalDamageMinAmountTextBoxId()
    {
        return "ctl00_cntMainBody_AD__EVENT1_FAP_MIN_AMT";
    }
    
    public static String ADFirstAmountPayableAccidentalDamageMinPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_AD__EVENT1_FAP_MIN_PERC";
    }
    
    public static String ADFirstAmountPayableAccidentalDamageMaxAmountTextBoxId()
    {
        return "ctl00_cntMainBody_AD__EVENT1_FAP_MAX_AMT";
    }
    
    public static String ADFirstAmountPayableLeakageMinAmountTextBoxId()
    {
        return "ctl00_cntMainBody_AD__EVENT2_FAP_MIN_AMT";
    }
    
    public static String ADFirstAmountPayableLeakageMinPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_AD__EVENT2_FAP_MIN_PERC";
    }
    
    public static String ADFirstAmountPayableLeakageMaxAmountTextBoxId()
    {
        return "ctl00_cntMainBody_AD__EVENT2_FAP_MAX_AMT";
    }
    
    
    public static String ADExtensionsAdditionalIncreasedCostLimitTextBoxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_118";
    }
    
    public static String ADExtensionsAdditionalIncreasedCostRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_118";
    }
    
    public static String ADExtensionsAdditionalIncreasedCostFAPPercentageTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_PERC_118";
    }
    
    public static String ADExtensionsAdditionalIncreasedCostFAPAmountTextBoxId()
    {
        return "ctl00_cntMainBody_FAP_AMOUNT_118";
    }
        
    public static String ADExtensionsClaimsPreparationLimitTextBoxId()
    {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
    }
    
    public static String ADExtensionsClaimsPreparationRateTextBoxId()
    {
        return "ctl00_cntMainBody_AGREED_RATE_25";
    }
    
    public static String NotesCommentsTextBoxId()
    {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }
    
    public static String SearchIndustryTextBoxId()
    {
        return "txtSearch";
    }
    
    public static String FACReinsurerCodeTextBoxId()
    {
        return "ctl00_cntMainBody_txtReinsurerCode";
    }
    
    public static String EveresteReinsurancePercTextBoxId()
    {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl03_txtThisPerc";
    }
    
    public static String CaisseReinsurancePercTextBoxId()
    {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl06_txtThisPerc";
    }
    
    
    public static String HannoverreReinsurancePercTextBoxId()
    {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl04_txtThisPerc";
    }
    
    public static String MunichReinsurancePercTextBoxId()
    {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl05_txtThisPerc";
    }
    
     public static String MIAEffectiveDateTextBoxId()
    {
        return "ctl00_cntMainBody_txtEffectiveDate";
    }
     
      public static String PolicyReferenceTextBoxId()
    {
        return "ctl00_cntMainBody_txtReference";
    }
      
      public static String DORetroActiveDateTextBoxId()
    {
        return "ctl00_cntMainBody_DO__RETROACTIVE_DATE";
    }
      
       public static String PolicyNumberTextBoxId()
    {
        return "ctl00_cntMainBody_txtPolicyNumber";
    }
       
       public static String AgentCodeTextBoxId()
    {
        return "ctl01_cntMainBody_txtAgent_code";
    }
       
       
       
    
    
// </editor-fold>
                
// <editor-fold defaultstate="collapsed" desc="Buttons">
    public static String ChangeAddressButtonId()
    {
        return "changeButton";
    }
    
      public static String RenewalInvitePrintButtonId()
    {
        return "ctl00_cntMainBody_btnPrint";
    }
    
    public static String FindNowButtonId()
    {
        return "ctl00_cntMainBody_btnFindNow";
    }
    
     public static String RenewalManagerSearchButtonId()
    {
        return "ctl00_cntMainBody_btnFilter";
    }
     
     public static String RenewalManagerUpdateStatusButtonId()
    {
        return "ctl01_cntMainBody_btnUpdateStatus";
    }
     
     public static String RenewalManagerStatusButtonId()
    {
        return "ctl00_cntMainBody_btnStatus";
    }
    
    public static String FACSearchReinsurerButtonId()
    {
        return "ctl00_cntMainBody_btnSearch";
    }
    
      public static String AddProcFACButtonId()
    {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_btnAddFacProp";
    }           
      
      
    
    public static String NewQuoteButtonId()
    {
        return "ctl00_cntMainBody_ctrlNewQuote_btnNewQuote";
    }
    
    public static String AgentCodeButtonId()
    {
        return "ctl00_cntMainBody_btnAgentCode";
    }
    
    public static String SearchAgentsButtonId()
    {
        return "ctl01_cntMainBody_btnSearch";
    }
    
    public static String NextButtonId()
    {
        return "ctl00_cntMainBody_btnNext";
    }
    
    public static String RateButtonId()
    {
        return "ctl00_cntMainBody_btnRate";
    }
    
    public static String AddMiscItemButtonName()
    {
        return "ctl00$cntMainBody$FIRE__FI_RISK_MICS_ITEMS$ctl02$ctl00";
    } 
    
    public static String BIAddSpecifiedSupplierButtonName()
    {
        return "ctl00$cntMainBody$BI__SPEC_SUPPLIER$ctl02$ctl00";
    } 
    
    public static String BIAddSpecifiedCustomerButtonName()
    {
        return "ctl00$cntMainBody$BI__SPEC_CUSTOMER$ctl02$ctl00";
    } 
    
    public static String BIRateButtonId()
    {
        return "ctl00_cntMainBody_btnRate";
    } 
    
    public static String BCAddMiscellaneousItemButtonName()
    {
        return "ctl00$cntMainBody$BC__BC_ITEMS$ctl02$ctl00";
    } 
    
    public static String ReinsuranceDetailsOKButtonId()
    {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_btnOk";
    }
    
    public static String OKButtonId()
    {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_btnOk";
    }
    
    public static String OOSOKButtonId()
    {
        return "ctl00_cntMainBody_btnOK";
    }
    
    public static String FinishButtonId()
    {
        return "ctl00_cntMainBody_btnFinish";
    }
    
    public static String MakeLiveButtonId()
    {
        return "ctl00_cntMainBody_btnBuy";
    }
    
    public static String SaveQuoteButtonId()
    {
        return "ctl00_cntMainBody_btnSaveQuote";
    }
    
    public static String SearchIndustrySpanId()
     {
        return "searchIndustryIcon";
     }
   
    public static String SearchIndustryButtonId()
    {
        return "btnIndustrySearch";
    }
    
    public static String ChangeAddressSecondRowId()
    {
        return "2";
    }
    
     public static String IndustrySearchRowId()
    {
        return "41";
    }
     
     public static String CopyRiskSubmitButtonId()
    {
        return "ctl00_cntMainBody_btnSubmit";
    }
    
     public static String CancelMTAButtonId()
    {
        return "ctl00_cntMainBody_btnCancelMTA";
    }
     
     public static String DirectorsandOfficesRiskRateButtonId()
  {
      return "ctl00_cntMainBody_btnRate";
  }
    
// </editor-fold>
       
// <editor-fold defaultstate="collapsed" desc="HoverTabs">
       

// </editor-fold>
        
// <editor-fold defaultstate="collapsed" desc="Tabs">
    public static String ReinsuranceDetailsTabPartialLinkText()
    {
        return "Reinsurance Details";
    }
    
    public static String ViewClientDetailsPoliciesTabPartialLinkText()
    {
        return "Policies";
    }
    
    public static String ViewClientDetailsQuotesTabPartialLinkText()
    {
        return "Quotes";
    }
    
    public static String PolicyHeaderTabId()
    {
        return "ctl00_cntMainBody_ctrlTabIndex_lbtnMainDetail";
    }
    

// </editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="Check Boxes">
     public static String FAPThirdPartyMinCheckBoxId()
    {
        return "ctl00_cntMainBody_MS__FAP_3RD_PARTY_APPL";
    }
    
    public static String FAPFire_ExplosionMinCheckBoxId()
    {
        return "ctl00_cntMainBody_MS__FAP_FI_APPL";
    }
     public static String RenewalManagerCheckPolicy2CheckBoxId()
    {
        return "ctl00_cntMainBody_grdvRenQuotes_ctl08_chkSelection";
    }
     
    public static String MotorTradersExternalExtensionsContingentLiabilityCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_254";
    }
    
    public static String MotorTradersExternalExtensionsLossofKeysCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_255";
    }
    
     public static String MotorTradersExternalExtensionsLossofUseCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_249";
    }
     
    public static String MotorTradersExternalExtensionsMotorCycleCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_256";
    }
    
    public static String MotorTradersExternalExtensionsPassangerLiabilityCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_251";
    }
    
     public static String MotorTradersExternalExtensionRiotandStrikeCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_262";
    }
     
    public static String MotorTradersExternalExtensionSocialDomesticCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_248";
    }
    
    public static String MotorTradersExternalExtensionSpecialTypeVehiclesCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_257";
    }
    
    public static String MotorTradersExternalExtensionUnauthorisedUseCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_250";
    }
    
    public static String MotorTradersExternalExtensionVehicleLentCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_253";
    }
    
    public static String MotorTradersExternalExtensionWindscreenCoverCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_252";
    }
    
    public static String MotorTradersExternalExtensionWreckageRemovalCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_261";
    }
     
    public static String MotorTradersExtensionsCarHoistsCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_264";
    }
    
     public static String MotorTradersExtensionsLossofKeysCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_265";
    }
     
     public static String MotorTradersExtensionsWindscreenCoverCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_252";
    }
     
     public static String MotorTradersExtensionsWorkAwayCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_263";
    }
     
     public static String MotorTradersExtensionsWreckageRemovalCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_261";
    }
     
     public static String SubSectionAThirdPartyOnlyCheckBoxId()
    {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_TYPE_OF_WAGES";
    }
     
     public static String SubSectionAFireAndTheftCheckBoxId()
    {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_THIRD_PARTY_FIRE_THEFT";
    }
     
     public static String SubSectionADeletionOfDemonstrationCheckBoxId()
    {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_DELETION_OF_DEMONSTRATION";
    }
     
     public static String SubSectionAExclusionOfOwnVehiclesCheckBoxId()
    {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_EXCLUSION_OWN_VEH";
    }
    
     
    public static String CoverVoluntarySubsectionBCheckBoxId()
    {
        return "ctl00_cntMainBody_MT__SUB_SECTION_B_APPL_LIABILITY_THIRD_PARTIES";
    }
    
    public static String CoverVoluntarySubsectionCCheckBoxId()
    {
        return "ctl00_cntMainBody_MT__SUB_SECTION_C_MEDICAL_APPL";
    }
    
    public static String RiskItemFlatPremiumCheckBoxId()
    {
        return "ctl00_cntMainBody_GIT__RATING_FLAT_PREM_APPL";
    }
    
    public static String RenewalManagerCheckPolicyCheckBoxId()
    {
        return "ctl00_cntMainBody_grdvRenQuotes_ctl05_chkSelection";
    }
    
     public static String ViewAllPoliciesCheckboxId()
    {
        return "ctl00_cntMainBody_ClientPolicies_chkViewAllPolicies";
    }
    
    public static String RSFireSectionsFireCheckBoxId()
    {
        return "ctl00_cntMainBody_GROUP_FIRE__SECTION_FIRE_APPL";
    }
    
    public static String RSFireSectionsBusinessInterruptionCheckBoxId()
    {
        return "ctl00_cntMainBody_GROUP_FIRE__SECTION_BI_APPL";
    }
    
    public static String RSFireSectionsBuildingsCombinedCheckBoxId()
    {
        return "ctl00_cntMainBody_GROUP_FIRE__SECTION_BC_APPL";
    }
    
    public static String RSFireSectionsOfficeContentsCheckBoxId()
    {
        return "ctl00_cntMainBody_GROUP_FIRE__SECTION_OC_APPL";
    }
    
    public static String OfficeContentsFlatPremiumCheckBoxId()
    {
        return "ctl00_cntMainBody_OC__OC_FLAT_PREM_APPL";
    }
    
    public static String RSFireSectionsAccountsRecievableCheckBoxId()
    {
        return "ctl00_cntMainBody_GROUP_FIRE__SECTION_AR_APPL";
    }
    
    public static String RSFireSectionsAccidentalDamageCheckBoxId()
    {
        return "ctl00_cntMainBody_GROUP_FIRE__SECTION_AD_APPL";
    }
    
    public static String FireRiskDataBuildingsCheckBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL1_APPL";
    }
    
    public static String FireRiskDataRentCheckBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL2_APPL";
    }
    
    public static String FireOverviewFlatPremiumCheckBoxId()
    {
        return "ctl00_cntMainBody_FIRE__FIRE_FLAT_PREM_APPL";
    }
    
    public static String BIOverviewFlatPremiumCheckBoxId()
    {
        return "ctl00_cntMainBody_BI__BI_FLAT_PREM_APPL";
    }
    
    public static String FireRiskDataPlantandMachCheckBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL3_APPL";
    }
    
    public static String FireRiskDataStockCheckBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL4_APPL";
    }
    
    public static String FireRiskDataMiscCheckBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL5_APPL";
    }
    
    public static String FireBuildingsExcalationCheckBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL1_INFL_APPL";
    }
    
    public static String FirePlantMachineryExcalationCheckBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL3_INFL_APPL";
    }
    
    public static String FireStockDeclarationCheckBoxId()
    {
        return "ctl00_cntMainBody_FIRE__COL4_DECLARATION_APPL";
    }
    
    public static String FireFirstAmountPayableCheckBoxId()
    {
        return "ctl00_cntMainBody_FIRE__FAP_APPL";
    }
    
    public static String FireClaimsPreparationCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_25";
    }
    
    public static String FireDisposalOfSalvageCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_102";
    }
    
    public static String FireLeakageCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_101";
    }
    
    public static String FireRiotAndStrikeCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_35";
    }
    
    public static String FireSubsidenceAndLandslipCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_100";
    }
    
    public static String BICoverGrossProfitBasisCheckBoxId()
    {
        return "ctl00_cntMainBody_BI__GROSS_PROFIT_BASIS_APPL";
    }
    
    public static String BICoverGrossRentalsCheckBoxId()
    {
        return "ctl00_cntMainBody_BI__GROSS_RENTALS_APPL";
    }
    
    public static String BICoverRevenueCheckBoxId()
    {
        return "ctl00_cntMainBody_BI__REVENUE_APPL";
    }
    
    public static String BICoverAdditionalIncreaseCostOfWorkingCheckBoxId()
    {
        return "ctl00_cntMainBody_BI__ICOW_APPL";
    }
    
    public static String BICoverWagesCheckBoxId()
    {
        return "ctl00_cntMainBody_BI__WAGES_APPL";
    }
    
    public static String BIFirstAmountPayableCheckBoxId()
    {
        return "ctl00_cntMainBody_BI__FAP_APPL";
    }
    
    public static String BIExtensionsAccidentalDamageCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_108";
    }
    
    public static String BIExtensionsClaimsPreparationCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_25";
    }
    
    public static String BIExtensionsDisposalOfSalvageCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_102";
    }
    
    public static String BIExtensionsFinesAndPenaltiesCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_107";
    }
    
    public static String BIExtensionsPreventionOfAccessCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_105";
    }
    
    public static String BIExtensionsPublicTelecommunicationsExtendedCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_104";
    }
    
    public static String BIExtensionsPublicUtilitiesExtendedCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_113";
    }
    
    public static String BIExtensionsSpecifiedCustomersCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_112";
    }
    
    public static String BIExtensionsSpecifiedSuppliersCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_110";
    }
    
    public static String BIExtensionsUnSpecifiedCustomersCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_111";
    }
    
    public static String BIExtensionsUnSpecifiedSuppliersCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_109";
    }
    
    public static String BCBuildingsCombinedEscalationInflationCheckBoxId()
    {
        return "ctl00_cntMainBody_BC__BC_ESC";
    }
    
    public static String BCMiscellaneousItemsOverviewCheckBoxId()
    {
        return "ctl00_cntMainBody_BC__MISC_ITEMS_APPL";
    }
    
    public static String BCAdditionalRentCheckBoxId()
    {
        return "ctl00_cntMainBody_BC__ADD_RENT_APPL";
    }
    
    public static String BCFirstAmountPayableCheckBoxId()
    {
        return "ctl00_cntMainBody_BC__FAP_APPL";
    }
    
    public static String BCExtentionsClaimsPreparationCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_25";
    }
    
    public static String BCExtentionsPreventionOfAccessCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_125";
    }
    
    public static String BCExtentionsRiotCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_35";
    }
    
    public static String BCExtentionsSubsidenceCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_100";
    }
    
    public static String AROutstandingDebitBalancesCheckBoxId()
    {
        return "ctl00_cntMainBody_AR__OD_APPL";
    }
    
    public static String ARFirstAmountPayableCheckBoxId()
    {
        return "ctl00_cntMainBody_AR__FAP_APPL";
    }
    
    public static String ARExtensionsClaimsCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_25";
    }
    
    public static String ARExtensionsDuplicateRecordsCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_115";
    }
    
    public static String ARExtensionsProtectionsCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_116";
    }
    
    public static String ARExtensionsRiotCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_35";
    }
    
    public static String ARExtensionsTransitCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_117";
    }  
    
    public static String OCCoverOfficeContentsCheckBoxId()
    {
        return "ctl00_cntMainBody_OC__OC_APPL";
    }
    
    public static String OCCoverLossOfDocumentsCheckBoxId()
    {
        return "ctl00_cntMainBody_OC__LOD_APPL";
    }
    
    public static String OCCoverLiabilityForDocumentsCheckBoxId()
    {
        return "ctl00_cntMainBody_OC__LIAB_DOCS_APPL";
    }
    
    public static String OCCoverTheftNonForcibleCheckBoxId()
    {
        return "ctl00_cntMainBody_OC__THEFT_NON_FORCIBLE_APPL";
    }
    
    public static String OCCoverTheftByForcibleEntryCheckBoxId()
    {
        return "ctl00_cntMainBody_OC__THEFT_FORCIBLE_APPL";
    }
    
    public static String OCFirstAmountPayableOfficeContentsCheckBoxId()
    {
        return "ctl00_cntMainBody_OC__OC_FAP_APPL";
    }
    
    public static String OCFirstAmountPayableLossofDocumentsCheckBoxId()
    {
        return "ctl00_cntMainBody_OC__LOF_FAP_APPL";
    }
    
    public static String OCFirstAmountPayableLiabilityForDocumentsCheckBoxId()
    {
        return "ctl00_cntMainBody_OC__LIABDFAP_APPL";
    }
    
    public static String OCFirstAmountPayableTheftNonForcibleCheckBoxId()
    {
        return "ctl00_cntMainBody_OC__THEFTNON_FAP_APPL";
    }
    
    public static String OCFirstAmountPayableTheftByForcibleEntryCheckBoxId()
    {
        return "ctl00_cntMainBody_OC__FAP_THEFT_APPL";
    }
    
    public static String OCExtentionsAdditionalIncresedCostOfWorkingCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_118";
    }
    
    public static String OCExtentionsClaimsPreparationCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_25";
    }
    
    public static String OCExtentionsFirstLossAverageCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_120";
    }
    
    public static String OCExtentionsMaliciousDamageCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_119";
    }
    
    public static String OCExtentionsRiotCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_35";
    }
    
    public static String ADCoverAccidentalDamageCheckBoxId()
    {
        return "ctl00_cntMainBody_AD__EVENT1_APPL";
    }
    
    public static String ADCoverLeakageCheckBoxId()
    {
        return "ctl00_cntMainBody_AD__EVENT2_APPL";
    }
    
    public static String ADFirstAmountPayableAccidentalDamageCheckBoxId()
    {
        return "ctl00_cntMainBody_AD__EVENT1_FAP_APPL";
    }
    
    public static String ADFirstAmountPayableLeakageCheckBoxId()
    {
        return "ctl00_cntMainBody_AD__EVENT2_FAP_APPL";
    }
    
    public static String ADExtensionsAdditionalIncreasedCostOfWorkingCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_118";
    }
    
    public static String ADExtensionsClaimsPreparationCheckBoxId()
    {
        return "ctl00_cntMainBody_CHECK_25";
    }
    
    public static String ADMemorandaReinstatementCheckBoxId()
    {
        return "ctl00_cntMainBody_AD__EXT_REINST_MEMO_APPL";
    }
    
    public static String ADMemorandaFirstLossAverageCheckBoxId()
    {
        return "ctl00_cntMainBody_AD__EXT_FLA_MEMO_APPL";
    }
    
    public static String DORiskNoChangesCheckBoxId()
    {
        return "ctl00_cntMainBody_DO__CHECK_NO_CHANGES";
    }
    
    public static String DORiskIsSolventCheckBoxId()
    {
        return "ctl00_cntMainBody_DO__CHECK_IS_SOLVENT";
    }
    
    public static String DORiskOpInSaCheckBoxId()
    {
        return "ctl00_cntMainBody_DO__CHECK_OP_IN_SA";
    }
    
    public static String DORiskNotListedCheckBoxId()
    {
        return "ctl00_cntMainBody_DO__CHECK_NOT_LISTED";
    }
    
    public static String DORiskCompanyTypeCheckBoxId()
    {
        return "ctl00_cntMainBody_DO__CHECK_COMPANY_TYPE";
    }
    
    public static String DORiskClaimsCheckBoxId()
    {
        return "ctl00_cntMainBody_DO__CHECK_CLAIMS";
    }
    
    public static String DORiskDirectorCheckBoxId()
    {
        return "ctl00_cntMainBody_DO__CHECK_DIRECTOR";
    }

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Iframes">
    public static String FindAgentDialogFrameId()
    {
        return "modalDialog";
    }
    
    public static String RenewalStatusFrameId()
    {
        return "modalDialog";
    }
    
    public static String AddMiscellaneousItemsDialogFrameId()
    {
        return "modalDialog";
    }
    
    public static String SelectRiskTypeDialogFrameId()
    {
        return "modalDialog";
    }  
    
    public static String CopyRiskTypeDialogFrameId()
    {
        return "modalDialog";
    }

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Links">
    
     public static String DeleteDomesticAndPropertyLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl04_lnkbtnDelete";
    }
     
     public static String DeleteMotor3LinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnDelete";
    }
    public static String EditBackDatedMTALinkId()
    {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl10_lnkbtnSelect";
    }
    
    public static String EditSASRIARoadsideAssistMTALinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnEdit";
    }
    
    public static String EditDomesticandPropertyRiskLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnEdit";
    }
    
     public static String SelecMotorTradersInternalRikTypeLinkId()
    {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl06_lnkbtnSelect";
    }
     
     public static String SelecMotorTradersExternalRikTypeLinkId()
    {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl07_lnkbtnSelect";
    }
     
    public static String SelectAgentInResultsId()
    {
        return "ctl01_cntMainBody_grdvSearchResults_ctl02_lnkbtnSelect";
    }
    
    public static String DeleteElectronicEquipmentId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnDelete";
    }
    
     public static String RenewalSelectionSelectPolicyLinkId()
    {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_lnkSelect";
    }
    
     public static String BackdatedEditPolicy1LinkId()
    {
        return "ctl00_cntMainBody_grdBackdatedMTA_ctl03_lnkbtnEdit";
    }
     
      public static String BackdatedEditPolicy2LinkId()
    {
        return "ctl00_cntMainBody_grdBackdatedMTA_ctl04_lnkbtnEdit";
    }
      
      public static String BackdatedEditPolicy3LinkId()
    {
        return "ctl00_cntMainBody_grdBackdatedMTA_ctl05_lnkbtnEdit";
    }
      
      public static String BackdatedEditPolicy4LinkId()
    {
        return "ctl00_cntMainBody_grdBackdatedMTA_ctl06_lnkbtnEdit";
    }
      
      public static String BackdatedEditPolicy5LinkId()
    {
        return "ctl00_cntMainBody_grdBackdatedMTA_ctl07_lnkbtnEdit";
    }
      
      public static String BackdatedEditPolicy6LinkId()
    {
        return "ctl00_cntMainBody_grdBackdatedMTA_ctl08_lnkbtnEdit";
    }
      
      public static String BackdatedEditPolicy7LinkId()
    {
        return "ctl00_cntMainBody_grdBackdatedMTA_ctl09_lnkbtnEdit";
    }
      
      public static String BackdatedEditPolicy8LinkId()
    {
        return "ctl00_cntMainBody_grdBackdatedMTA_ctl0_lnkbtnEdit";
    }
    
     public static String DeleteGoodsInTransitLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl06_lnkbtnDelete";
    }
     
     public static String DeleteGoodsInTransitStep14LinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnDelete";
    }
     
      public static String DeleteBusinessAllRiskLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnDelete";
    }
    
    public static String RenewalSelectionSelectPolicyResultsId()
    {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_lnkSelect";
    }
    
     public static String ReinstatePolicyLinkText()
    {
        return "Reinstatement";
    }
    
    public static String ViewClientDetailsEditQuoteLinkId()
    {
        return "ctl00_cntMainBody_ctrlClientQuotes_grdvQuotes_ctl02_lnkbtnSelect";
    }
    
    
    public static String ViewClientDetailsChangePolicyLinkId()
    {
        return "ctl00_cntMainBody_ClientPolicies_grdvQuotes_ctl03_lnkbtnSelect";
    }
    
    public static String DeleteGroupedFireRisk3LinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl05_lnkbtnDelete";
    }
    
     public static String DeleteGroupedFireRisk2LinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl04_lnkbtnDelete";
    }
     
      public static String DeleteGroupedFireRisk1LinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnDelete";
    }
      
      public static String DeleteWaterandPleasureCraftLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl04_lnkbtnDelete";
    }
      
     public static String DeleteEveLinkId()
    {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl03_lnkDelete";
    }
     
     public static String DeleteHannLinkId()
    {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl04_lnkDelete";
    }
     
     public static String DeleteMunichLinkId()
    {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl05_lnkDelete";
    }
    
    public static String SelectRiskTypeResultsId()
    {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl03_lnkbtnSelect";
    }
    
    public static String editGroupedFireRiskLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl06_lnkbtnEdit";
    }
    
    public static String editGroupedFireRisk1LinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl05_lnkbtnEdit";
    }
    
     public static String editGroupedFireRisk4LinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnEdit";
    }
     
      public static String editGroupedFireRisk5LinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl05_lnkbtnEdit";
    }
      
       public static String editGroupedFireRisk6LinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl06_lnkbtnEdit";
    }
       
        public static String editGroupedFireRisk7LinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnEdit";
    }
     
     public static String editGroupedFireRisk2LinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnEdit";
    }
     
     public static String editGroupedFireRisk3LinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl04_lnkbtnEdit";
    }
     
    public static String editElectronicEquipmentLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnEdit";
    }  
    
     public static String editElectronicEquipment2LinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl06_lnkbtnEdit";
    }
    
    public static String copyGroupedFireRiskLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl06_lnkbtnCopy";
    }
    
    public static String SelectRoadSideAssistQuoteEditLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnEdit";
    }
    
    public static String FACSelectReinsurerLinkId()
    {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_btnSelect";
    }
    
     public static String FACSelectReinsurer3LinkId()
    {
        return "ctl00_cntMainBody_grdvSearchResults_ctl03_btnSelect";
    }
     
     public static String FACSelectFirstReinsurer3LinkId()
    {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_btnSelect";
    }
    
    public static String FACSelectReinsurer2LinkId()
    {
        return "ctl00_cntMainBody_grdvSearchResults_ctl03_btnSelect";
    }
    
    public static String FACSelectMunichReinsurerLinkId()
    {
        return "ctl00_cntMainBody_grdvSearchResults_ctl03_btnSelect";
    }
    
     public static String FACSelectCaisseReinsurerLinkId()
    {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_btnSelect";
    }
    
    public static String editBusinessAllRiskLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl05_lnkbtnEdit";
    }
    
    public static String editElectronicEquipmentRiskLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnEdit";
    }
    
    public static String editMoneyRiskLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl04_lnkbtnEdit";
    }
    
    public static String editGoodsInTransitLinkId()
    {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnEdit";
    }
    
    public static String selectEBPDORiskLinkId()
    {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl02_lnkbtnSelect";
    }
    

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Drop Down Lists">
    
    public static String ReinsuranceBandDropDownListId()
    {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_ddlReinsurance";
    }
    
    public static String SelectProductDropDownListId()
    {
        return "ctl00_cntMainBody_ctrlNewQuote_ddlProductlst";
    }
    
    public static String RenewalManagerRenewalStatusDropDownListId()
    {
        return "ctl00_cntMainBody_RenewalStatusType";
    }
    
    public static String RenewalStatusDropDownListId()
    {
        return "ctl01_cntMainBody_RenewalStatusType";
    }
    
    public static String RenewalManagerProductTypeDropDownListId()
    {
        return "ctl00_cntMainBody_ddlProductType";
    }
    
    public static String RenewalManagerBranchDropDownListId()
    {
        return "ctl00_cntMainBody_BranchCode";
    }
    
    public static String MTATypeofChangeDropDownListId()
    {
        return "ddlMTAs";
    }
    
     public static String RiskItemBasisofCoverDropDownListId()
    {
        return "ctl00_cntMainBody_GIT__BASIS_COVER";
    }
    
    public static String PaymentTermDropDownListId()
    {
        return "ctl00_cntMainBody_POLICYHEADER__PAYMENTTERM";
    }
    
    public static String CollectionFrequencyDropDownListId()
    {
        return "ctl00_cntMainBody_POLICYHEADER__COLLECTIONFREQUENCY";
    } 
    
    public static String ManualReinsuranceLimitDropDownListId()
    {
        return "ctl00_cntMainBody_GROUP_FIRE__OVERRIDE_RI_LIMIT_NR";
    }
    
    public static String TypeOfConstructionDropDownListId()
    {
        return "ctl00_cntMainBody_GROUP_FIRE__TYPE_CONSTRUCTION";
    }
    
    public static String FireSubsidenceCoverTypeDropDownListId()
    {
        return "ctl00_cntMainBody_COVER_100";
    }
    
    public static String FireSubsidenceGeoTechDropDownListId()
    {
        return "ctl00_cntMainBody_REPORT_100";
    }
    
    public static String BusinessInterruptionOverviewTypeOfCoverDropDownListId()
    {
        return "ctl00_cntMainBody_BI__TYPE_OF_COVER";
    }
    
    public static String BIOverviewIndemnityPeriodDropDownListId()
    {
        return "ctl00_cntMainBody_BI__INDEMNITY_PERIOD";
    }
    
    public static String BICoverAdditionalIncreaseCostOfWorkingTypeDropDownListId()
    {
        return "ctl00_cntMainBody_BI__ICOW_TYPE";
    }
    
    public static String BICoverWagesMonthsDropDownListId()
    {
        return "ctl00_cntMainBody_BI__WAGES_TYPE";
    }
    
    public static String BCBuildingsCombinedOccupationDropDownListId()
    {
        return "ctl00_cntMainBody_BC__BC_OCC";
    }
    
    public static String BCExtentionsSubsidenceCoverTypeDropDownListId()
    {
        return "ctl00_cntMainBody_COVER_100";
    }
    
    public static String BCExtentionsSubsidenceGeoTechDropDownListId()
    {
        return "ctl00_cntMainBody_REPORT_100";
    }
    
    public static String OCAlarmWarrantyDropDownListId()
    {
        return "ctl00_cntMainBody_OC__ALARM_APPL";
    }
    
    public static String ADAccidentalDamageBasisOfCoverDropDownListId()
    {
        return "ctl00_cntMainBody_AD__EVENT1_BASIS_COVER";
    }
    
    public static String CopyRiskDropDownListId()
    {
        return "ctl00_cntMainBody_ddlCopyRiskType";
    }
    
    public static String DORatingCriteriaGrossAssetsDropDownListId()
    {
        return "ctl00_cntMainBody_DO__GROSS_ASSETS";
    }
    
    public static String DORatingCriteriaLimitofIndemnityDropDownListId()
    {
        return "ctl00_cntMainBody_DO__LIMIT_OF_INDEMNITY";
    }
    
    public static String SubSectionATypeOfWages()
    {
        return "ctl00_cntMainBody_MT__SUB_SECTION_A_TYPE_OF_WAGES";
    }
    
    

    
// </editor-fold>
    
// <editor-fold defaultstate="collapsed" desc="Validation Items">
    public static String ViewClientDetailsLabelId()
    {
        return "ctl00_cntMainBody_lblViewClient";
    }
    
    public static String RenewalSelectionMessageDivId()
    {
        return "ctl00_cntMainBody_lblMessage";
    }
    
    public static String RecommendClaimValidationSummaryDivId()
    {
        return "ctl00_cntMainBody_ValidationSummary";
    }
    
    public static String BasicDetailsPageHeaderClassName()
    {
        return "panelLegend";
    }
    
    public static String RatingDetailsLabelId()
    {
        return "ctl00_cntMainBody_RatingDetails1_lblRatingDetails";
    }
    
    public static String PolicyReferenceNumberLabelId()
    {
        return "ctl00_ClientInfoCtrl_lblPolicyRef";
    }
    
    public static String PolicyConfimationLabelId()
    {
        return "ctl00_cntMainBody_lblTransactionSubHeading";
    }
    
    public static String PolicyRenewalDivId()
    {
        return "ctl00_cntMainBody_PnlRenewalSelectionFile";
    }
 
// </editor-fold> 
    
}
