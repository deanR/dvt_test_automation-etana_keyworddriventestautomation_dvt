
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author deanR
 */
public class EkunduEHATBusinessAllRiskPage
  {
    public static String ExtensionsClaimscheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_157";
      }

    public static String EBPExtensionsClaimscheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_25";
      }

    public static String ExtensionsRiotscheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_184";
      }

    public static String ExtensionsLimitOfIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_157";
      }

    public static String ExtensionsRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_157";
      }

    public static String BusinessAllRiskAddButtonName()
      {
        return "ctl00$cntMainBody$BAR__ITEMS$ctl02$ctl00";
      }

    public static String BusinessAllRiskAddButtonName2()
      {
        return "ctl00$cntMainBody$BAR__ITEMS$ctl03$ctl00";
      }

    public static String InterestedPartiesAddButtonName()
      {
        return "ctl00$cntMainBody$ITEMS__ITEM_INTERESTEDPARTIES$ctl02$ctl00";
      }

    public static String EBPInterestedPartiesAddButtonName()
      {
        return "ctl00$cntMainBody$ITEM__INT_PARTIES$ctl02$ctl00";
      }

    public static String RiskItemsAlarmWarrantyDropdownListId()
      {
        return "ctl00_cntMainBody_BAR__ALARM_WARRANTY_APPL";
      }

    public static String ItemCategoryDropdownListId()
      {
        return "ctl00_cntMainBody_ITEMS__CATEGORY";
      }

    public static String ItemDetailDescriptionTextboxId()
      {
        return "ctl00_cntMainBody_ITEMS__DESCRIPTION";
      }

    public static String ItemDetailSerialNumberTextboxId()
      {
        return "ctl00_cntMainBody_ITEMS__SERIAL_NO";
      }

    public static String EBPItemDetailSerialNumberTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__SERIAL_NUM";
      }
    
    public static String PCSerialNumberTextboxId()
      {
        return "ctl00_cntMainBody_PC_ITEMS__SADC_SERIAL_NUM";
      }
    
    public static String AllRiskItemSerialNumberTextboxId()
      {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__SERIAL";
      }
    
    public static String AllRiskItemBasicExcessTextboxId()
      {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__SADC_BASIC_EXCESS";
      }
    
    public static String PCSumInsuredTextboxId()
      {
        return "ctl00_cntMainBody_PC_ITEMS__SADC_SUM_INS";
      }
    
    public static String AllRiskItemSumInsuredTextboxId()
      {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__AR_SI";
      }

    public static String ItemDetailDateAddedTextboxId()
      {
        return "ctl00_cntMainBody_ITEMS__DATE_ADDED";
      }

    public static String EBPItemDetailDateAddedTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__DATE_ADDED";
      }

    public static String ItemDetailSumInsuredTextboxId()
      {
        return "ctl00_cntMainBody_ITEMS__SUM_INSURED";
      }

    public static String ItemDetailMaxRITextboxId()
      {
        return "ctl00_cntMainBody_ITEMS__MAX_RI_EXP";
      }

    public static String EBPItemDetailMaxRITextboxId()
      {
        return "ctl00_cntMainBody_ITEM__MAX_LIMIT_ANY_ITEM";
      }

    public static String ItemDetailRateTextboxId()
      {
        return "ctl00_cntMainBody_ITEMS__AGREED_RATE";
      }

    public static String FAPcheckboxId()
      {
        return "ctl00_cntMainBody_ITEMS__FAP_APPL";
      }

    public static String FAPMinPercentageTextboxId()
      {
        return "ctl00_cntMainBody_ITEMS__FAP_MIN_PERC";
      }

    public static String FAPMinAmountTextboxId()
      {
        return "ctl00_cntMainBody_ITEMS__FAP_MIN_AMT";
      }

    public static String FAPMaxAmountTextboxId()
      {
        return "ctl00_cntMainBody_ITEMS__FAP_MAX_AMT";
      }

    public static String InterestedPartiesTextboxId()
      {
        return "ctl00_cntMainBody_ITEM_INTERESTEDPARTIES__NAME";
      }

    public static String InterestedPartiesNameTextboxId()
      {
        return "ctl00_cntMainBody_INT_PARTIES__INT_NAME";
      }

    public static String BusinessAllRiskItemNotes1TextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
      }

    public static String BusinessAllRiskItemNotes2TextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
      }

    public static String BusinessAllRiskItemPolicyNotesCheckboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__ADD_NOTES";
      }

    public static String EndorsementsSelectButtonId()
      {
        return "_btnShowSelect";
      }

    public static String EndorsementsAddAllButtonId()
      {
        return "ctl00_cntMainBody_BAR__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
      }

    public static String EndorsementsApplyButtonId()
      {
        return "ctl00_cntMainBody_BAR__ENDORSEMENTS_btnApplySelection";
      }

    public static String BroadFormSpecifiedNotesTextBoxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
      }

    public static String BroadFormSpecifiedNotesTextBoxId2()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
      }

    public static String NotesCheckboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__ADD_NOTES";
      }

    public static String MotorSpecifiedReinsuranceDetailstabLinkText()
      {
        return "Reinsurance Details";
      }

    public static String BusinessAllRiskNextButtonId()
      {
        return "ctl00_cntMainBody_btnNext";
      }

    public static String AddisonReinsurancePercTextBoxId()
      {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl03_txtSumInsured";
      }         
  }


//~ Formatted by Jindent --- http://www.jindent.com
