
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class EkunduBusinessAllRiskPageNew
  {
    public static String SelectBusinessAllRiskLinkId()
      {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl05_lnkbtnSelect";
      }

    public static String RiskItemsDeleteItemLinkText()
      {
        return "Delete";
      }

    public static String EditBusinessAllRiskLinkId()
      {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl05_lnkbtnEdit";
      }

    public static String RiskItemFlatPremiumTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__FLAT_PREM_AMT";
      }

    public static String RiskItemFlatPremiumCheckboxId()
      {
        return "ctl00_cntMainBody_ITEM__FLAT_PREM_APPL";
      }

    public static String RiskItemsAlarmWarrantyDropdownListId()
      {
        return "ctl00_cntMainBody_BAR__ALARM_WARRANTY_APPL";
      }

    public static String RiskItemsExtensionsAdditionalClaimsCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_25";
      }

    public static String RiskItemsExtensionsAdditionalClaimsLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
      }

    public static String RiskItemsExtensionsAdditionalClaimsRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_25";
      }

    public static String RiskItemsExtensionsRiotandStrikeCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_11";
      }

    public static String RiskItemsAddRiskItemsButtonName()
      {
        return "ctl00$cntMainBody$BAR__ITEM$ctl02$ctl00v";
      }

    public static String BusinessAllRiskItemCategoryDropdownListId()
      {
        return "ctl00_cntMainBody_ITEM__CATEGORY";
      }

    public static String BusinessAllRiskItemSumInsuredTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__SI";
      }

    public static String BusinessAllRiskItemDescriptionTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__DESCRIPTION";
      }

    public static String BusinessAllRiskItemRateTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__AGREED_RATE";
      }

    public static String BusinessAllRiskExtensionsReplaceValueConditionsCheckboxId()
      {
        return "ctl00_cntMainBody_ITEM__EXT_REINST_VALUE_COND_APPL";
      }

    public static String BusinessAllRiskExtensionsIncreasedCostofWorkingCheckboxId()
      {
        return "ctl00_cntMainBody_ITEM__EXT_ICOW_APPL";
      }

    public static String BusinessAllRiskRateButtonId()
      {
        return "ctl00_cntMainBody_btnRate";
      }

    public static String BusinessAllRiskNextButtonId()
      {
        return "ctl00_cntMainBody_btnNext";
      }

    public static String BusinessAllRiskItemNotesTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
      }

    public static String BusinessAllRiskItemPolicyNotesCheckboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__ADD_NOTES";
      }

    public static String BusinessAllRiskItemPolicyNotesTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
      }

    public static String EndorsementsSelectButtonId()
      {
        return "_btnShowSelect";
      }

    public static String EndorsementsAddAllButtonId()
      {
        return "ctl00_cntMainBody_BAR__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
      }

    public static String DAPEndorsementsAddAllButtonId()
      {
        return "ctl00_cntMainBody_S_ENDORSEMENT__ENDORSEMENT_PckTemplates_RemoveAllCmd";
      }

    public static String EndorsementsApplyButtonId()
      {
        return "ctl00_cntMainBody_BAR__ENDORSEMENTS_btnApplySelection";
      }

    public static String DAPEndorsementsApplyButtonId()
      {
        return "ctl00_cntMainBody_S_ENDORSEMENT__ENDORSEMENT_btnApplySelection";
      }

    public static String BusinessAllRiskCommentsTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
      }

    public static String BusinessAllRiskPolicyNotesCheckboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__ADD_NOTES";
      }

    public static String BusinessAllRiskPolicyNotesTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
