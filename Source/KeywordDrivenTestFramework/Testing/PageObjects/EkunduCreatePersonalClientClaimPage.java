
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class EkunduCreatePersonalClientClaimPage
  {
    public static String workPerilsTableDivId()
      {
        return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils";
      }

    public static String claimHoverTabId()
      {
        return "s-case";
      }

    public static String newCaseLinkXPath()
      {
        return "//span/div/ul/li[5]/ul/li[1]/a";
      }

    public static String OkButtonId()
      {
        return "popup_ok";
      }

    public static String claimsChampionDropDownListId()
      {
        return "ctl00_cntMainBody_drpAnalyst";
      }

    public static String submitButtonId()
      {
        return "ctl00_cntMainBody_btnSubmit";
      }

    public static String openClaimButtonId()
      {
        return "ctl00_cntMainBody_btnOpen";
      }

    public static String policynumberTextBoxId()
      {
        return "ctl00_cntMainBody_txtPolicyNumber";
      }

    public static String lossDateTextBoxId()
      {
        return "ctl00_cntMainBody_Claims_FindInsuranceFile__Claim_Date";
      }

    public static String findnowButtonId()
      {
        return "ctl00_cntMainBody_btnFindNow";
      }

    public static String contentSpanId()
      {
        return "ctl00_cntMainBody_lblFindCase";
      }

    public static String SelectPolicyLinkId()
      {
        return "ctl00_cntMainBody_grdvInsuranceFile_ctl02_lnkDetails";
      }

    public static String RiskTypeDropdownListId()
      {
        return "ctl00_cntMainBody_ddlRiskType";
      }

    public static String ProgressStatusDropdownListId()
      {
        return "ctl00_cntMainBody_CONTROL__PROGRESS_STATUS";
      }

    public static String PrimaryCauseDropdownListId()
      {
        return "ctl00_cntMainBody_CONTROL__PRIMARY_CAUSE";
      }

    public static String SecondaryCauseDropdownListId()
      {
        return "ctl00_cntMainBody_CONTROL__SECONDARY_CAUSE";
      }

    public static String ClaimsHandlerDropdownListId()
      {
        return "ctl00_cntMainBody_CONTROL__HANDLER_CODE";
      }

    public static String ClaimDescriptionTextBoxId()
      {
        return "ctl00_cntMainBody_CONTROL__RISK_DESC";
      }

    public static String ClaimNextButtonId()
      {
        return "ctl00_cntMainBody_btnNext";
      }

    public static String PremiumConfirmerDropdownListId()
      {
        return "ctl00_cntMainBody_EBP_CLM_MOTOR__PREM_CONF_BY";
      }

    public static String LoadReserveEditLinkId()
      {
        return "ctl00_cntMainBody_Work_Claim_Peril_grdvPerils_ctl05_lnkReserves";
      }

    public static String ClaimReserverNewValueTextBoxId()
      {
        return "newValue02";
      }

    public static String ReserverFinishButtonId()
      {
        return "ctl00_cntMainBody_btnFinish";
      }

    public static String PerilsSubmitButtonId()
      {
        return "ctl00_cntMainBody_btnSubmit";
      }

    public static String ClaimReinsuranceOkButtonId()
      {
        return "ctl00_cntMainBody_ctrl_RI2007_btnNext";
      }

    public static String ClaimNumberSpanId()
      {
        return "ctl00_cntMainBody_lblClaimNumber";
      }

    public static String OverviewClaimNumberTextBoxId()
      {
        return "ctl00_cntMainBody_CONTROL__CLAIM_NUMBER";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
