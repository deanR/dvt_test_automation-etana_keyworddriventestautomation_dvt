
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author deanR
 */
public class EkunduBroadFormLiabilityPage
  {
    public static String SelectBroadFormLiabilityRiskLinkId()
      {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl02_lnkbtnSelect";
      }

    public static String RiskBasisofCoverDropdownListId()
      {
        return "ctl00_cntMainBody_BFL__BASIS_OF_COVER";
      }

    public static String RiskBasisofCoverRetroActiveDateTextboxId()
      {
        return "ctl00_cntMainBody_BFL__RETRO_DATE";
      }

    public static String RiskTurnoverTextboxId()
      {
        return "ctl00_cntMainBody_BFL__TURNOVER";
      }

    public static String CoverTypePublicProductLiabilityCheckboxId()
      {
        return "ctl00_cntMainBody_BFL__PUBLIC_APPL";
      }

    public static String CoverTypePollutionLiabilityCheckboxId()
      {
        return "ctl00_cntMainBody_BFL__POLLUTION_APPL";
      }

    public static String CoverTypeProductDefectiveWorkmanshipCheckboxId()
      {
        return "ctl00_cntMainBody_BFL__DEFECT_APPL";
      }

    public static String CoverTypeNegligentAdviceCheckboxId()
      {
        return "ctl00_cntMainBody_BFL__NEGLIGENCE_APPL";
      }

    public static String LimitOfIndemnityTextBoxId()
      {
        return "ctl00_cntMainBody_BFL__PUBLIC_LIMIT_OF_INDEMNITY";
      }

    public static String PublicLiabilityPremiumTextBoxId()
      {
        return "ctl00_cntMainBody_BFL__PUBLIC_PREMIUM";
      }

    public static String PublicLiabilityFAPSectionCheckboxId()
      {
        return "ctl00_cntMainBody_BFL__PUBLIC_FAP_APPL";
      }

    public static String ProductsLimitOfIndemnityTextBoxId()
      {
        return "ctl00_cntMainBody_BFL__PRODUCT_LIMIT_OF_INDEMNITY";
      }

    public static String ProductsPublicLiabilityPremiumTextBoxId()
      {
        return "ctl00_cntMainBody_BFL__PRODUCT_PREMIUM";
      }

    public static String ProductsPublicLiabilityFAPSectionCheckboxId()
      {
        return "ctl00_cntMainBody_BFL__PRODUCT_FAP_APPL";
      }

    public static String PollutionLimitOfIndemnityTextBoxId()
      {
        return "ctl00_cntMainBody_BFL__POLLUTION_LIMIT_OF_INDEMNITY";
      }

    public static String PollutionPublicLiabilityPremiumTextBoxId()
      {
        return "ctl00_cntMainBody_BFL__POLLUTION_PREMIUM";
      }

    public static String PollutionPublicLiabilityFAPSectionCheckboxId()
      {
        return "ctl00_cntMainBody_BFL__POLLUTION_FAP_APPL";
      }

    public static String DefectiveLimitOfIndemnityTextBoxId()
      {
        return "ctl00_cntMainBody_BFL__DEFECT_PROD_LIMIT_OF_INDEMNITY";
      }

    public static String DefectivePublicLiabilityPremiumTextBoxId()
      {
        return "ctl00_cntMainBody_BFL__DEFECT_PROD_PREMIUM";
      }

    public static String DefectiveWorkmanshipLimitOfIndemnityTextBoxId()
      {
        return "ctl00_cntMainBody_BFL__DEFECT_LIMIT_OF_INDEMNITY";
      }

    public static String DefectiveorkmanshipPublicLiabilityPremiumTextBoxId()
      {
        return "ctl00_cntMainBody_BFL__DEFECT_PREMIUM";
      }

    public static String DefectivePublicLiabilityFAPSectionCheckboxId()
      {
        return "ctl00_cntMainBody_BFL__DEFECT_FAP_APPL";
      }

    public static String NegligentLimitOfIndemnityTextBoxId()
      {
        return "ctl00_cntMainBody_BFL__NEGLIGENCE_LIMIT_OF_INDEMNITY";
      }

    public static String NegligentPublicLiabilityPremiumTextBoxId()
      {
        return "ctl00_cntMainBody_BFL__NEGLIGENCE_PREMIUM";
      }

    public static String NegligentPublicLiabilityFAPSectionCheckboxId()
      {
        return "ctl00_cntMainBody_BFL__NEGLIGENCE_FAP_APPL";
      }

    public static String ExtensionPublicLiabilityFAPSectionCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_150";
      }

    public static String ExtensionLimitOfIndemnityTextBoxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_150";
      }

    public static String ExtensionPublicLiabilityPremiumTextBoxId()
      {
        return "ctl00_cntMainBody_PREMIUM_150";
      }

    public static String ExtensionClaimsPublicLiabilityFAPSectionCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_151";
      }

    public static String ExtensionClaimsLimitOfIndemnityTextBoxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_151";
      }

    public static String ExtensionClaimsPublicLiabilityPremiumTextBoxId()
      {
        return "ctl00_cntMainBody_PREMIUM_151";
      }

    public static String ExtensionGuestPublicLiabilityFAPSectionCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_194";
      }

    public static String ExtensionLegalDefencePublicLiabilityFAPSectionCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_153";
      }

    public static String ExtensionWrongfulArrestPublicLiabilityFAPSectionCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_154";
      }

    public static String ReinsuranceLimitOfLiabilityTextBoxId()
      {
        return "ctl00_cntMainBody_BFL__RI_LIMIT_OF_INDEMNITY";
      }

    public static String EndorsementsSelectButtonId()
      {
        return "_btnShowSelect";
      }

    public static String EndorsementsAddAllButtonId()
      {
        return "ctl00_cntMainBody_BFL__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
      }

    public static String EndorsementsApplyButtonId()
      {
        return "ctl00_cntMainBody_BFL__ENDORSEMENTS_btnApplySelection";
      }

    public static String RiskCommentsTextBoxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
      }

    public static String RiskNotesTextBoxId2()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
      }

    public static String NotesCheckboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__ADD_NOTES";
      }

    public static String MotorSpecifiedReinsuranceDetailstabLinkText()
      {
        return "Reinsurance Details";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
