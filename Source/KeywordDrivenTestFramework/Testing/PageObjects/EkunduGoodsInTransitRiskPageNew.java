
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class EkunduGoodsInTransitRiskPageNew
  {
    public static String SelectGoodsInTransitLinkId()
      {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl06_lnkbtnSelect";
      }

    public static String GoodsInTransitNextButtonId()
      {
        return "ctl00_cntMainBody_btnNext";
      }

    public static String RiskItemSearchIndustryButtonId()
      {
        return "btnIndustrySearch";
      }

    public static String RiskItemSearchIndustryTextboxId()
      {
        return "txtSearch";
      }

    public static String RiskItemFinalPremiumTextboxId()
      {
        return "ctl00_cntMainBody_GIT__ANNUAL_CARRY_BAND1_PREM";
      }

    public static String RiskItemSearchIndustryIconId()
      {
        return "searchIndustryIcon";
      }

    public static String IndustrySearchResultsSelectedRowId()
      {
        return "1";
      }

    public static String RiskItemRoadCheckboxId()
      {
        return "ctl00_cntMainBody_GIT__ROAD_APPL";
      }

    public static String RiskItemAirCheckboxId()
      {
        return "ctl00_cntMainBody_GIT__AIR_APPL";
      }

    public static String RiskItemRailCheckboxId()
      {
        return "ctl00_cntMainBody_GIT__RAIL_APPL";
      }

    public static String RiskItemSumInsuredTextboxId()
      {
        return "ctl00_cntMainBody_GIT__ANNUAL_CARRY_LOAD_LIMIT_SI";
      }

    public static String RiskItemLoadLimitTextboxId()
      {
        return "ctl00_cntMainBody_GIT__ANNUAL_CARRY_LOAD_LIMIT";
      }

    public static String RiskItemAgreedRateTextboxId()
      {
        return "ctl00_cntMainBody_GIT__ANNUAL_CARRY_BAND1_ADJ_RATE";
      }

    public static String RiskItemFAPCheckboxId()
      {
        return "ctl00_cntMainBody_GIT__FAP_APPL";
      }

    public static String RiskItemFAPMaxAmountTextboxId()
      {
        return "ctl00_cntMainBody_GIT__FAP_MAX_AMT";
      }

    public static String RiskItemAdditionalFAPCheckboxId()
      {
        return "ctl00_cntMainBody_GIT__FAP_ADD_APPL";
      }

    public static String RiskItemAdditionalFAPMaxAmountTextboxId()
      {
        return "ctl00_cntMainBody_GIT__FAP_ADD_MAX_AMT";
      }

    public static String RiskItemTheftorHijackingCheckboxId()
      {
        return "ctl00_cntMainBody_GIT__FAP_THEFT_APPL";
      }

    public static String RiskItemTheftorHijackingMaxAmountTextboxId()
      {
        return "ctl00_cntMainBody_GIT__FAP_THEFT_MAX_AMT";
      }

    public static String RiskItemRateButtonId()
      {
        return "ctl00_cntMainBody_btnRate";
      }

    public static String ExtensionsAdditionalClaimCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_25";
      }

    public static String ExtensionsAdditionalClaimsLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
      }

    public static String ExtensionsAdditionalClaimsRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_25";
      }

    public static String ExtensionsDebrisRemovalCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_33";
      }

    public static String ExtensionsDebrisRemovalLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_33";
      }

    public static String ExtensionsDebrisRemovalRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_33";
      }

    public static String ExtensionsDebrisRemovalFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_33";
      }

    public static String ExtensionsDebrisRemovalFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_33";
      }

    public static String ExtensionsFireExtinguishingChargesCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_34";
      }

    public static String ExtensionsFireExtinguishingChargesLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_34";
      }

    public static String ExtensionsFireExtinguishingChargesRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_34";
      }

    public static String ExtensionsFireExtinguishingChargesFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_34";
      }

    public static String ExtensionsFireExtinguishingChargesFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_34";
      }

    public static String ExtensionsRiotandStrikeCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_35";
      }

    public static String ExtensionsRiotandStrikePremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_35";
      }

    public static String ExtensionsRiotandStrikeFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_35";
      }

    public static String ExtensionsRiotandStrikeFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_35";
      }

    public static String EndorsementsSelectButtonId()
      {
        return "_btnShowSelect";
      }

    public static String EndorsementsAddAllButtonId()
      {
        return "ctl00_cntMainBody_GIT__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
      }

    public static String EndorsementsApplyButtonId()
      {
        return "ctl00_cntMainBody_GIT__ENDORSEMENTS_btnApplySelection";
      }

    public static String NotesTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
      }

    public static String PolicyNotesTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
      }

    public static String AddPolicyNotesLabelId()
      {
        return "ctl00_cntMainBody_cntrlNotes_lbl_CONTROL__ADD_NOTES";
      }

    public static String AddFACXOLButtonId()
      {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_btnAddFacXOL";
      }

    public static String FACXOLLowerLimitId()
      {
        return "ctl00_cntMainBody_txtLower";
      }

    public static String FACXOLUpperLimitId()
      {
        return "ctl00_cntMainBody_txtUpper";
      }

    public static String FACXOLParticipationPercTextboxId()
      {
        return "ctl00_cntMainBody_grdPlacements_ctl02_txtParticipation";
      }

    public static String FACXOLParticipationPerc2TextboxId()
      {
        return "ctl00_cntMainBody_grdPlacements_ctl03_txtParticipation";
      }

    public static String FACXOLParticipationPerc3TextboxId()
      {
        return "ctl00_cntMainBody_grdPlacements_ctl04_txtParticipation";
      }

    public static String FACXOLCaisseParticipationPercTextboxId()
      {
        return "ctl00_cntMainBody_grdPlacements_ctl03_txtParticipation";
      }

    public static String FACXOLPremiumTextboxId()
      {
        return "ctl00_cntMainBody_grdPlacements_ctl02_txtPremium";
      }

    public static String FACXOLPremium2TextboxId()
      {
        return "ctl00_cntMainBody_grdPlacements_ctl03_txtPremium";
      }

    public static String FACXOLPremium3TextboxId()
      {
        return "ctl00_cntMainBody_grdPlacements_ctl04_txtPremium";
      }

    public static String FACXOLCiassePremiumTextboxId()
      {
        return "ctl00_cntMainBody_grdPlacements_ctl03_txtPremium";
      }

    public static String FACXOLOkButtonId()
      {
        return "ctl00_cntMainBody_btnOk";
      }

    public static String FACXOLCommisionTextboxId()
      {
        return "ctl00_cntMainBody_grdPlacements_ctl02_txtComPerc";
      }

    public static String FACXOLCaisseCommisionTextboxId()
      {
        return "ctl00_cntMainBody_grdPlacements_ctl03_txtComPerc";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
