
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import static KeywordDrivenTestFramework.Core.BaseClass.CurrentEnvironment;
import KeywordDrivenTestFramework.Entities.Enums;

/**
 *
 * @author ferdinandn
 */
public class EkunduCreatePersonalClientPage
  {
    // <editor-fold defaultstate="collapsed" desc="Tabs">
    public static String basicDetailsTabPartialLinkText()
      {
        return "Basic Details";
      }

    public static String addressesTabPartialLinkText()
      {
        return "Addresses";
      }

    public static String taxTabPartialLinkText()
      {
        return "Tax";
      }

    public static String bankDetailsTabPartialLinkText()
      {
        return "Bank Details";
      }

    public static String AddressTabPartialLinkText()
      {
        return "Addresses";
      }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="EditFields">

    public static String firstNameTextBoxId()
      {
        return "ctl00_cntMainBody_txtFirstName";
      }
    
    public static String bankDetailsLine1TextBoxId()
      {
        return "ctl00_cntMainBody_txtStreet";
      }
    
    public static String bankDetailsLine2TextBoxId()
      {
        return "ctl00_cntMainBody_txtLocality";
      }

    public static String lastNameTextBoxId()
      {
        return "ctl00_cntMainBody_txtLastname";
      }

    public static String initialsTextBoxId()
      {
        return "ctl00_cntMainBody_txtInitials";
      }

    public static String dobTextBoxId()
      {
        return "ctl00_cntMainBody_txtDOB";
      }

    public static String taxRegNoTextBoxId()
      {
        return "ctl00_cntMainBody_txtTaxRegistrationNO";
      }

    public static String vatNoTextBoxId()
      {
        return "ctl00_cntMainBody_GENERAL__VAT_NUMBER";
      }

    public static String idNoTextBoxId()
      {
        return "ctl00_cntMainBody_GENERAL__ID_NUMBER";
      }

    public static String addressLine1TextBoxId()
      {
        return "txtAddressLine1";
      }

    public static String suburbTextBoxId()
      {
        return "txtSuburb";
      }

    public static String cityTextBoxId()
      {
        return "txtCityProvince";
      }
    
    public static String bankDetailLine1TextBoxId()
      {
        return "txtCityProvince";
      }
    
     public static String bankDetailspostTownTextBoxId()
      {
        return "ctl00_cntMainBody_txtPostTown";
      }
    
     public static String bankDetailspostCodeTextBoxId()
      {
        return "ctl00_cntMainBody_txtPostCode";
      }
   

    public static String postalCodeTextBoxId()
      {
        return "txtPostalCode";
      }

    public static String CountryTextBoxId()
      {
        return "drpCountry";
      }

    public static String addressline2TextBoxId()
      {
        return "txtAddressLine2";
      }

    public static String suburbSearchTextTextBoxId()
      {
        return "searchText";
      }

    public static String taxNoTextBoxId()
      {
        return "ctl00_cntMainBody_txtTaxNumber";
      }

    public static String taxpercentageTextBoxId()
      {
        return "ctl00_cntMainBody_txtTaxPercentage";
      }

    public static String accountHolderNameTextBoxId()
      {
        return "ctl00_cntMainBody_txtAccountHolderName";
      }

    public static String accountTypeTextBoxId()
      {
        return "ctl00_cntMainBody_txtAccountType";
      }

    public static String accountNoTextBoxId()
      {
        return "ctl00_cntMainBody_txtAccountNo";
      }

    public static String branchCodeTextBoxId()
      {
        return "ctl00_cntMainBody_txtBankBranchCode";
      }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Drop Down Lists">
    public static String titleDropDownListId()
      {
        return "ctl00_cntMainBody_ddlTitle";
      }
        
    public static String addressCitiesDropDownListId()
      {
        return "ddlCities";
      }
    public static String genderDropDownListId()
      {
        return "ctl00_cntMainBody_ddlGender";
      }

    public static String nationalityDropDownListId()
      {
        return "ctl00_cntMainBody_GISCorporate_Nationality";
      }

    public static String idTypeDropDownListId()
      {
        return "ctl00_cntMainBody_GENERAL__ID_TYPE";
      }

    public static String addressTypeDropDownListId()
      {
        return "drpAddressType";
      }

    public static String HomeAddressCountryDropDownListId()
      {
        return "drpCountry";
      }

    public static String bankPaymentTypeDropDownListId()
      {
        return "ctl00_cntMainBody_GISNBankPaymentType";
      }

    public static String bankNameDropDownListId()
      {
        return "ctl00_cntMainBody_GISLookup_BankList";
      }

    public static String maritalStatusDropDownListId()
      {
        return "ctl00_cntMainBody_ddlMaritalStatus";
      }

    public static String paymentCurrencyDropDownListId()
      {
        return "ctl00_cntMainBody_ddlCurrency";
      }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="iFrames">
    public static String SuburbSearchFrameId()
      {
        return "grid-dialog-modal";
      }

    public static String AddressFrameId()
      {
        return "modalDialog";
      }

    public static String BankFrameId()
      {
        return "modalDialog";
      }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Buttons">

    public static String findSuburbButtonId()
      {
        return "btnFindAddress";
      }

    public static String AddressAddLinkText()
      {
        return "Add Address";
      }

    public static String AddressAddButtonId()
      {
        return "btnAddAddress";
      }

    public static String addBankButtonId()
      {
        return "ctl00_cntMainBody_btnAddBank";
      }

    public static String UpdateBankButtonId()
      {
        return "ctl00_cntMainBody_btnUpdateBank";
      }

    public static String UpdateAddressButtonId()
      {
        return "btnUpdateAddress";
      }
    
    public static String CloseAddressDialogNamibiaButtonXPath()
      {
        return "/html/body/div[3]/div[1]/button[1]/span[1]";
      }

    public static String CloseAddressDialogButtonXPath2()
      {
        return "/html/body/div[2]/div[1]/button/span";

      }
    
    public static String CloseAddressDialogButtonXPath()
      {
          if(CurrentEnvironment == Enums.TestEnvironments.QA)
        {
            return "/html/body/div[3]/div[1]/button/span[1]";
        }
        else
        {
            return "//body/div[2]/div[1]/a";
        }
          
      }
             
    public static String CloseAddressDialog2ButtonXPath()
      {
        return "/html/body/div[3]/div[1]/button/span[1]";

      }
               
    public static String submitClientButtonId()
      {
        return "ctl00_cntMainBody_btnSubmit";
      }

    public static String updateClientButtonId()
      {
        return "ctl00_cntMainBody_btnSubmit";
      }

    public static String searchSuburbButtonId()
      {
        return "suburbSearchSubmit";
      }

    public static String editClientButtonId()
      {
        return "ctl00_cntMainBody_btnEditClient";
      }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Links">
    public static String addAddressLinkId()
      {
        return "ctl00_cntMainBody_Addresses_hypAddress";
      }

    public static String addBankLinkId()
      {
        return "ctl00_cntMainBody_BankDetail_hypBank";
      }

    public static String DeleteAddressLinkId()
      {
        return "ctl00_cntMainBody_Addresses_drgAddresses_ctl04_hypAddressDelete";
      }

    public static String CorrespondentAddressEditLinkId()
      {
        return "ctl00_cntMainBody_Addresses_drgAddresses_ctl02_hypAddressEdit";
      }

    public static String HomeAddressEditLinkId()
      {
        return "ctl00_cntMainBody_Addresses_drgAddresses_ctl03_hypAddressEdit";
      }

    public static String SelectBankDetailsLinkId()
      {
        return "ctl00_cntMainBody_BankDetail_grdBankDetails_ctl02_btnSelect";
      }

    public static String EditBankDetailsLinkId()
      {
        return "ctl00_cntMainBody_BankDetail_grdBankDetails_ctl02_btnEdit";
      }

    public static String EditAddressDetailsLinkId()
      {
        return "ctl00_cntMainBody_Addresses_drgAddresses_ctl02_hypAddressEdit";
      }

    //
    ////*[@id=\"menu_1\"]/li[1]/a[2]

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Cells">
    public static String SuburbSearchResultsSelectedCellId()
      {
        return "1";
      }

    // </editor-fold>

    public static String SuburbSearchResultsTableId()
      {
        return "gbox_popupGrid";
      }

    public static String clientCodeSpanId()
      {
        return "ctl00_cntMainBody_lblCode_view";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
