/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.CurrentEnvironment;
import static KeywordDrivenTestFramework.Entities.Enums.TestEnvironments.NamQA;
import static KeywordDrivenTestFramework.Entities.Enums.TestEnvironments.QA;

/**
 *
 * @author FerdinandN
 */
public class EkunduCreateNewPolicyForNewClientPage extends BaseClass {

// <editor-fold defaultstate="collapsed" desc="TextBoxes">
    public static String ClientNameTextBoxId() {
        return "ctl00_cntMainBody_txtClientName";
    }

    public static String AddClaimPrepSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_227";
    }

    public static String PercengerLiabilityCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_1";
    }

    public static String PercengerLiabilityDropDownID() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_1";
    }

    public static String CLAddClaimPrepSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_229";
    }

    public static String UnAuthorisedPersengerLiabilityDropDownID() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_2";
    }

    public static String ProfessionalFeesSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_226";
    }

    public static String CLProfessionalFeesSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_228";
    }

    public static String AddClaimPrepPedMinAmountTextBoxId() {
        return "ctl00_cntMainBody_MIN_AMOUNT_227";
    }

    public static String CLAddClaimPrepPedMinAmountTextBoxId() {
        return "ctl00_cntMainBody_MIN_AMOUNT_229";
    }

    public static String ProfessionalFeesDedMinAmountTextBoxId() {
        return "ctl00_cntMainBody_MIN_AMOUNT_226";
    }

    public static String CLProfessionalFeesDedMinAmountTextBoxId() {
        return "ctl00_cntMainBody_MIN_AMOUNT_228";
    }

    public static String AddClaimPrepPedMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MAX_AMOUNT_227";
    }

    public static String CLAddClaimPrepPedMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MAX_AMOUNT_229";
    }

    public static String ProfessionalFeesMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MAX_AMOUNT_226";
    }

    public static String CLProfessionalFeesMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MAX_AMOUNT_228";
    }

    public static String OtherExtensionsDescriptionTextBoxId() {
        return "ctl00_cntMainBody_MBCL_EXTENSIONS__DESC_OTH";
    }

    public static String CLOtherExtensionsDescriptionTextBoxId() {
        return "ctl00_cntMainBody_CL_EXTENSIONS__DESC_OTH";
    }

    public static String OtherExtensionsSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_MBCL_EXTENSIONS__SUM_INSURED";
    }

    public static String CLOtherExtensionsSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_CL_EXTENSIONS__SUM_INSURED";
    }

    public static String OtherExtensionsRateTextBoxId() {
        return "ctl00_cntMainBody_MBCL_EXTENSIONS__AGREED_RATE";
    }

    public static String CLOtherExtensionsRateTextBoxId() {
        return "ctl00_cntMainBody_CL_EXTENSIONS__AGREED_RATE";
    }

    public static String OtherExtensionsDeductiblePercentageTextBoxId() {
        return "ctl00_cntMainBody_MBCL_EXTENSIONS__DED_PERC";
    }

    public static String CLOtherExtensionsDeductiblePercentageTextBoxId() {
        return "ctl00_cntMainBody_CL_EXTENSIONS__DED_PERC";
    }

    public static String OtherExtensionsMinimumAmountTextBoxId() {
        return "ctl00_cntMainBody_MBCL_EXTENSIONS__MIN_AMOUNT";
    }

    public static String CLOtherExtensionsMinimumAmountTextBoxId() {
        return "ctl00_cntMainBody_CL_EXTENSIONS__MIN_AMOUNT";
    }

    public static String OtherExtensionsMaximumAmountTextBoxId() {
        return "ctl00_cntMainBody_MBCL_EXTENSIONS__MAX_AMOUNT";
    }

    public static String CLOtherExtensionsMaximumAmountTextBoxId() {
        return "ctl00_cntMainBody_CL_EXTENSIONS__MAX_AMOUNT";
    }

    public static String AddClaimPrepDedPercentageTextBoxId() {
        return "ctl00_cntMainBody_DED_PERC_227";
    }

    public static String CLAddClaimPrepDedPercentageTextBoxId() {
        return "ctl00_cntMainBody_DED_PERC_229";
    }

    public static String ProfessionalFeesDedPercentageTextBoxId() {
        return "ctl00_cntMainBody_DED_PERC_226";
    }

    public static String CLProfessionalFeesDedPercentageTextBoxId() {
        return "ctl00_cntMainBody_DED_PERC_228";
    }

    public static String ProfessionalFeesRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_226";
    }

    public static String CLProfessionalFeesRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_228";
    }

    public static String AddClaimPrepRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_227";
    }

    public static String CLAddClaimPrepRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_229";
    }

    public static String ExtensionsContingentLiabilityLimitofIndemnityTextboxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_3";
    }

    public static String DescriptionofMachineryTextboxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__DESCRIPTION";
    }

    public static String MachineryItemSumInsuredTextboxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__MB_NRV";
    }

    public static String MachineryItemRateTextboxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__MB_RATE";
    }

    public static String MachineryItemPremiumTextboxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__MB_PREMIUM";
    }

    public static String MachineryItemDeductiblePercentageTextboxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__MB_DED_PERC";
    }

    public static String MachineryItemMinimumAmountTextboxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__MB_DED_MIN_AMT";
    }

    public static String MachineryItemMaximumAmountTextboxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__MB_DED_MAX_AMT";
    }

    public static String ExtensionsLossofKeysLimitofIndemnityTextboxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_5";
    }

    public static String ExtensionsParkingFacilitiesLimitofIndemnityTextboxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_12";
    }

    public static String ExtensionsWreckageRemovalLimitofIndemnityTextboxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_13";
    }

    public static String ExtensionsWreckageRemovalRateTextboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_13";
    }

    public static String ExtensionsContingentLiabilityRateTextboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_3";
    }

    public static String LossOfKeyFAPAmount() {
        return "ctl00_cntMainBody_FAP_AMOUNT_5";
    }

    public static String ExtensionsLossofKeysPremiumTextboxId() {
        return "ctl00_cntMainBody_PREMIUM_5";
    }

    public static String ExtensionsThirdPartyLiabilityPremiumTextboxId() {
        return "ctl00_cntMainBody_PREMIUM_4";
    }

    public static String ExtensionsCreditShortfallRateTextboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_122";
    }

    public static String ExtensionsTheftofCarRadioRateTextboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_6";
    }

    public static String GrossProfitIndemnityPeriodTextboxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__CL_IP";
    }

    public static String IncreasedCostofWorkingIndemnityPeriodTextboxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__CL_ICOW_IP_PERIOD";
    }

    public static String AdditionalIncreasedCostofWorkingIndemnityPeriodTextboxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__CL_AICOW_IP_PERIOD";
    }

    public static String TimeDeductibleTextboxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__CL_TIME_DED";
    }

    public static String IncreasedCostofWorkingTimeDeductibleTextboxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__CL_ICOW_TIME_DED";
    }

    public static String AdditionalIncreasedCostofWorkingTimeDeductibleTextboxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__CL_AICOW_TIME_DED";
    }

    public static String GrossProfitAnnualGrossProfitTextboxId() {
        return "ctl00_cntMainBody_MBCL__CLRB_ANN_GP";
    }

    public static String GrossProfitRateTextboxId() {
        return "ctl00_cntMainBody_MBCL__CLRB_GPB_RATE";
    }

    public static String GrossProfitPreiumTextboxId() {
        return "ctl00_cntMainBody_MBCL__CLRB_GPB_PREMIUM";
    }

    public static String IncreasedCostofWorkingSumInsuredTextboxId() {
        return "ctl00_cntMainBody_MBCL__CLRB_ICOW_SI";
    }

    public static String IncreasedCostofWorkingRateTextboxId() {
        return "ctl00_cntMainBody_MBCL__CLRB_ICOW_RATE";
    }

    public static String IncreasedCostofWorkingPremiumTextboxId() {
        return "ctl00_cntMainBody_MBCL__CLRB_ICOW_PREMIUM";
    }

    public static String AdditionalIncreasedCostofWorkingSumInsuredTextboxId() {
        return "ctl00_cntMainBody_MBCL__CLRB_AICOW_SI";
    }

    public static String AdditionalIncreasedCostofWorkingRateTextboxId() {
        return "ctl00_cntMainBody_MBCL__CLRB_AICOW_RATE";
    }

    public static String AdditionalIncreasedCostofWorkingPremiumTextboxId() {
        return "ctl00_cntMainBody_MBCL__CLRB_AICOW_PREMIUM";
    }

    public static String DeteriorationofStockSumInsuredTextboxId() {
        return "ctl00_cntMainBody_MBCL__DOS_SI";
    }

    public static String DeteriorationofStockRateTextboxId() {
        return "ctl00_cntMainBody_MBCL__DOS_RATE";
    }

    public static String DeteriorationofStockPremiumTextboxId() {
        return "ctl00_cntMainBody_MBCL__DOS_PREMIUM";
    }

    public static String DeteriorationofStockDedPercentageTextboxId() {
        return "ctl00_cntMainBody_MBCL__DOS_DED_PERC";
    }

    public static String DeteriorationofStockDedMinimumAmountTextboxId() {
        return "ctl00_cntMainBody_MBCL__DOS_DED_MIN_AMT";
    }

    public static String DeteriorationofStockDedMaximumAmountTextboxId() {
        return "ctl00_cntMainBody_MBCL__DOS_DED_MAX_AMT";
    }

    public static String ExtensionsFireAndexplosionRateTextboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_14";
    }

    public static String ExtensionsFireAndexplosionPremiumTextboxId() {
        return "ctl00_cntMainBody_PREMIUM_14";
    }

    public static String ExtensionsThirdPartyLiabilityRateTextboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_4";
    }

    public static String ExtensionsExcessWaiverPremiumTextboxId() {
        return "ctl00_cntMainBody_PREMIUM_10";
    }

    public static String CarHirePremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_19";
    }

    public static String CreditShortfallRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_122";
    }

    public static String ThirdPartyLiabilityRateTextBoxId() {
        if (CurrentEnvironment == QA) {
            return "ctl00_cntMainBody_AGREED_RATE_192";
            //return "ctl00_cntMainBody_AGREED_RATE_4";
        } else {
            return "ctl00_cntMainBody_AGREED_RATE_192";
        }
    }

    public static String ThirdPartyLiabilityRateMotorChangesTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_4";
    }

    public static String WindscreenFAPPercentageTextBoxId() {
        return "ctl00_cntMainBody_FAP_PERC_21";
    }

    public static String VehicleYearOlderVehiclesTextbox() {
        return "otherYear";
    }

    public static String VehicleModelOlderVehiclesTextbox() {
        return "otherModel";
    }

    public static String VehicleModelOlderVehiclesCCTextbox() {
        return "otherCc";
    }

    public static String VehicleModelOlderVehiclesGVMTextbox() {
        return "otherGvm";
    }

    public static String VehicleModelOlderVehiclesDropdownListId() {
        return "otherModel";
    }

    public static String VoluntaryExcessPercentageTextBoxId() {
        return "ctl00_cntMainBody_MS__FAP_VOL_MIN_PERC";
    }

    public static String FAPOwnDamageMaxPercentageTextBoxId() {
        return "ctl00_cntMainBody_MS__FAP_OWN_DAMAGE_MAX_PERC";
    }

    public static String FAPOwnDamageMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MS__FAP_OWN_DAMAGE_MAX_AMT";
    }

    public static String FAPThirdPardyMinimumAmountTextBoxId() {
        return "ctl00_cntMainBody_MS__FAP_3RD_PARTY_MIN_AMT";
    }

    public static String FAPThirdPartyMinPercetage() {
        return "ctl00_cntMainBody_MS__FAP_3RD_PARTY_MIN_PERC";
    }

    public static String FAPThirdPartyMaxPercetage() {
        return "ctl00_cntMainBody_MS__FAP_3RD_PARTY_MAX_PERC";
    }

    public static String FAPThirdPardyMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MS__FAP_3RD_PARTY_MAX_AMT";
    }

    public static String FAPFireandExplosionMinPercTextBoxId() {
        return "ctl00_cntMainBody_MS__FAP_FI_MIN_PERC";
    }

    public static String FAPFireandExplosionMaxPercTextBoxId() {
        return "ctl00_cntMainBody_MS__FAP_FI_MAX_PERC";
    }

    public static String FAPFireandExplosionMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MS__FAP_FI_MAX_AMT";
    }

    public static String FAPHijackMaxAmountTextBoxId() {
        return "ctl00_cntMainBody_MS__FAP_TH_MAX_AMT";
    }

    public static String FAPAdditionalTheftCheckBoxId() {
        return "ctl00_cntMainBody_MS__FAP_TH_APPL";
    }

    public static String FAPAdditionalTheftMinPecentageTextBoxId() {
        return "ctl00_cntMainBody_MS__FAP_TH_MIN_PERC";
    }

    public static String FAPAdditionalTheftMaxPecentageTextBoxId() {
        return "ctl00_cntMainBody_MS__FAP_TH_MIN_PERC";
    }

    public static String FAPAdditionalTheftMinAmountTextBoxId() {
        return "ctl00_cntMainBody_MS__FAP_TH_MIN_AMT";
    }

    public static String VoluntaryMinimumAmountTextBoxId() {
        return "ctl00_cntMainBody_MS__FAP_VOL_MIN_AMT";
    }

    public static String CompulsoryExcessPercentageTextBoxId() {
        return "ctl00_cntMainBody_MS__FAP_COMP_PERC";
    }

    public static String CompulsoryMinimumAmountTextBoxId() {
        return "ctl00_cntMainBody_MS__FAP_COMP_AMT";
    }

    public static String FlatPremiumAmountTextBoxId() {
        return "ctl00_cntMainBody_MS__FLAT_PREM_AMT";
    }

    public static String UnallocatedAmountTextBoxId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl03_txtSumInsured";
    }

    public static String VoluntaryExcessDiscountTextBoxId() {
        return "ctl00_cntMainBody_MS__FAP_VOL_DISC";
    }

    public static String WindscreenFAPAmountTextBoxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_21";
    }

    public static String ThirdPartyLiabilityLimitOfIndeminityTextBoxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_192";
        //return "ctl00_cntMainBody_PREMIUM_192";
    }

    public static String ThirdPartyLiabilityPremiumTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_192";
    }

    public static String UnAthorisedPersengerLiabilityChecBoxID() {
        return "ctl00_cntMainBody_CHECK_2";
    }

    public static String ThirdPartyLiabilityPremiumMotorChangesTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_4";
    }

    public static String NumberOfVehiclesTextBoxId() {
        return "ctl00_cntMainBody_MSD__NUM_VEHICLES";

    }

    public static String DAPRiskItemDescriptionTestboxId() {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__DES";
    }

    public static String VesselHorsePowerTestboxId() {
        return "ctl00_cntMainBody_PC__VESSEL_HORSE_POWER";
    }

    public static String DAPRiskItemSumInsuredTestboxId() {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__AR_SI";
    }

    public static String MotorAdjustmentFinalPremiumTextBoxId() {
        return "ctl00_cntMainBody_MS__PREM";
    }

    public static String SADCEditDocumentTextBoxId() {
        return "//*[@id=\"tinymce\"]/div/p";
    }

    //*[span[@id = \"ctl00_cntMainBody_txtDocumentEditor_parent\"]/table/tbody/tr[2]/div/p/span
    public static String SurveyDateTextBoxId() {
        return "ctl00_cntMainBody_PREM_SURVEY_REPORT__SRD";
    }

    public static String SurveyReferenceTextBoxId() {
        return "ctl00_cntMainBody_PREM_SURVEY_REPORT__SRREF";
    }

    public static String SurveyReportCommentsTextAreaId() {
        return "ctl00_cntMainBody_PREM_SURVEY_REPORT__COMMENTS";
    }

    public static String BuildingSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_BUILDING__BUILD_SI";
    }

    public static String NumberofIncidents12MonthsTextBoxId() {
        return "ctl00_cntMainBody_BUILDING__SADC_NO_INCIDENTS_12MONTHS";
    }

    public static String NumberofClaims12MonthsDropdownListName() {
        return "ctl00_cntMainBody_MAIN_DRIVERS__NUM_CLAIMS_12_MNTHS";
    }

    public static String NumberofClaims12To24MonthsDropdownListId() {
        return "ctl00_cntMainBody_MAIN_DRIVERS__NUM_CLAIMS_13_24_MNTHS";
    }

    public static String ContentsNumberofIncidents12To24MonthsTextBoxId() {
        return "ctl00_cntMainBody_CONTENT__SADC_NO_INCIDENTS_24MONTHS";
    }

    public static String ContentsNumberofIncidents12MonthsTextBoxId() {
        return "ctl00_cntMainBody_CONTENT__SADC_NO_INCIDENTS_12MONTHS";
    }

    public static String NumberofIncidents12To24MonthsTextBoxId() {
        return "ctl00_cntMainBody_BUILDING__SADC_NO_INCIDENTS_24MONTHS";
    }

    public static String VehicleTypeNumberofIncidents12MonthsTextBoxId() {
        return "ctl00_cntMainBody_MS__CLAIMS_LAST_12_MONTHS";
    }

    public static String VehicleTypeNumberofIncidents12To24MonthsTextBoxId() {
        return "ctl00_cntMainBody_MS__CLAIMS_LAST_12_TO_24_MONTHS";
    }

    public static String VehicleTypeNumberofIncidents24To36MonthsTextBoxId() {
        return "ctl00_cntMainBody_MS__CLAIMS_LAST_24_TO_36_MONTHS";
    }

    public static String NumberofIClaims24To36MonthsTextBoxId() {
        return "ctl00_cntMainBody_MAIN_DRIVERS__NUM_CLAIMS_25_36_MNTHS";
    }

    public static String MainDriverDateLicenceIssuedTextBoxId() {
        return "ctl00_cntMainBody_MS__LICENCE_ISSUED_DATE";
    }

    public static String EPLMainDriverDateLicenceIssuedTextBoxId() {
        return "ctl00_cntMainBody_MAIN_DRIVERS__DATE_LICENSE_ISSUED";
    }

    public static String SpecifiedDriverDateLicenceIssuedTextBoxId() {
        return "ctl00_cntMainBody_DRIVER__LICENSE_ISSUE_DATE";
    }

    public static String AccessoryDescriptionTextBoxId() {
        return "ctl00_cntMainBody_ACCESSORIES__DESCRIPTION";
    }

    public static String AccessorySumInsuredTextBoxId() {
        return "ctl00_cntMainBody_ACCESSORIES__SUM_INSURED";
    }

    public static String AccessoryFinishButtonID() {
        return "ctl00_cntMainBody_btnFinish";
    }

    public static String ShortfallTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_122";
    }

    public static String MainDriverIDNumberTextBoxId() {
        return "ctl00_cntMainBody_MS__ID_NUMBER";
    }

    public static String EPLMainDriverIDNumberTextBoxId() {
        return "ctl00_cntMainBody_MAIN_DRIVERS__ID_NUMBER";
    }

    public static String SpecifiedDriverIDNumberTextBoxId() {
        return "ctl00_cntMainBody_DRIVER__ID_NR";
    }

    public static String AccessoriesCheckBoxId() {
        return "ctl00_cntMainBody_MS__ACCESSORIES_APPL";
    }

    public static String SpecifiedDriversCheckBoxId() {
        return "ctl00_cntMainBody_MS__SPEC_DRIVERS_APPL";
    }

    public static String VehicleTrackingDeviceFittedCheckBoxId() {
        return "ctl00_cntMainBody_MS__TRACKING_DEVICE_APPL";
    }

    public static String VehicleImmobiliserCheckBoxId() {
        return "ctl00_cntMainBody_MS__IMMOBILISER_FITTED";
    }

    public static String VehicleAlarmCheckBoxId() {
        return "ctl00_cntMainBody_MS__ALARM_APPL";
    }

    public static String VehicleGearlockCheckBoxId() {
        return "ctl00_cntMainBody_MS__GEARLOCK_FITTED";
    }

    public static String VehicleGaragedatNightCheckBoxId() {
        return "ctl00_cntMainBody_MS__GARAGED_AT_NIGHT";
    }

    public static String VehicleUnsupportedMotorCheckBoxId() {
        return "ctl00_cntMainBody_MS__IS_UNS_MOTOR";
    }

    public static String MainDriverDateofBirthTextBoxId() {
        return "ctl00_cntMainBody_MS__DOB";
    }

    public static String EPLMainDriverDateofBirthTextBoxId() {
        return "ctl00_cntMainBody_MAIN_DRIVERS__DATE_OF_BIRTH";
    }

    public static String SpecifiedDriverDateofBirthTextBoxId() {
        return "ctl00_cntMainBody_DRIVER__DOB";
    }

    public static String RegularDriverNameTextBoxId() {
        return "ctl00_cntMainBody_MS__DRIVER_NAME";
    }

    public static String MainDriverNameTextBoxId() {
        return "ctl00_cntMainBody_MAIN_DRIVERS__DRIVER_NAME";
    }

    public static String SpecifiedDriverNameTextBoxId() {
        return "ctl00_cntMainBody_DRIVER__DRIVER";
    }

    public static String SpecifiedDriverIDTypeDropdownId() {
        return "ctl00_cntMainBody_DRIVER__ID_TYPE";
    }

    public static String NumberofIncidents24To36MonthsTextBoxId() {
        return "ctl00_cntMainBody_BUILDING__SADC_NO_INCIDENTS_36MONTHS";
    }

    public static String ContentsNumberofIncidents24To36MonthsTextBoxId() {
        return "ctl00_cntMainBody_CONTENT__SADC_NO_INCIDENTS_36MONTHS";
    }

    public static String AdjustmentPercentageTextBoxId() {
        return "ctl00_cntMainBody_BUILDING__ADJ_PERC";
    }

    public static String MotorSpecifiedAdjustmentPercentageTextBoxId() {
        return "ctl00_cntMainBody_MS__FLAT_ADJ_PERC";
    }

    public static String PCAdjustmentPercentageTextBoxId() {
        return "ctl00_cntMainBody_PC_ITEMS__SADC_ADJ_PERC";
    }

    public static String ratingDetailsTabId() {
        return "ui-id-3";
    }

    public static String commisionDetailsTabId() {
        return "ui-id-3";
    }

    public static String DebitOrderDetailsTabId() {
        return "ui-id-4";
    }

    public static String documentDetailsTabId() {
        return "ui-id-5";
    }

    public static String selecteBankAccountDropdownListTabId() {
        return "ddlBA";
    }

    public static String generatePdfDocuemntListTabId() {
        return "btnGeneratePdf";
    }

    public static String IndividualsAdjustmentPercentageTextBoxId() {
        return "ctl00_cntMainBody_INDIVIDUALS__ADJ_PERC";
    }

    public static String AllRiskItemAdjustmentPercentageTextBoxId() {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__ADJ_PERC";
    }

    public static String MechanicalBreakdowndjustmentPercentageTextBoxId() {
        return "ctl00_cntMainBody_MECH_AND_ELECT_BDOWN__SADC_ADJ_PERC";
    }

    public static String ContentsAdjustmentPercentageTextBoxId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_CONTENT__SADC_ADJ_PERC";
        } else {
            return "ctl00_cntMainBody_HOUSEHOLD__ADJ_PERC";
        }
    }

    public static String SubsidenceSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_BUILDING__SL_SUM_INSURED";
    }

    public static String InterestedPartiesCommentsTextAreaId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }

    public static String InterestedParties2CommentsTextAreaId() {
        return "ctl00_cntMainBody_BUILD_IP__COMMENTS";
    }

    public static String BuildingsInterestedPartiesCommentsTextAreaId() {
        return "ctl00_cntMainBody_BUILD_IP__COMMENTS";
    }

    public static String MSInterestedPartiesDescriptionTextAreaId() {
        return "ctl00_cntMainBody_EPL_IPARTIES__IN_PARTY_DESC";
    }

    public static String PCInterestedPartiesCommentsTextAreaId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }

    public static String ReinsuranceDetailsTabId() {
        return "ui-id-2";
    }

    public static String NotesTextAreaID() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
    }

    public static String NotesCheckBoxId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__ADD_NOTES";
    }

    public static String ContentsSumInsuredTextBoxId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_CONTENT__SADC_SUM_INS";
        } else {
            return "ctl00_cntMainBody_HOUSEHOLD__HH_SI";
        }

    }

    public static String EndorsementNotesTextBoxId() {
        return "ctl00_cntMainBody_S_ENDORSEMENT__NOTES";
    }

    public static String allRiskDescriptionTextBoxId() {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__DES";
    }

    public static String allRiskSerialTextBoxId() {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__SERIAL";
    }

    public static String allRiskSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__AR_SI";
    }

    public static String MechanicalBreakdownHouseHoldAppliacesValueTextBoxId() {
        return "ctl00_cntMainBody_MECH_AND_ELECT_BDOWN__SADC_HH_APPLIANCE_VALUE";
    }

    public static String MechanicalBreakdownAudioVisualEquipmentValueTextBoxId() {
        return "ctl00_cntMainBody_MECH_AND_ELECT_BDOWN__SADC_AV_EQUIPMENT_VALUE";
    }

    public static String ContentsExtensionsAccidentalDamageSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_120";
    }

    public static String ContentsExtensionsHomeIndustryCoverSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_SUM_INSURED_121";
    }

    public static String ContentsSubsidenceandLandslipGeoReportDropdownList() {
        return "ctl00_cntMainBody_SURVEY_REPORT_119";
    }

    public static String EnsuredNameTextBoxId() {
        return "ctl00_cntMainBody_RISK_ITEM__INS_P";
    }

    public static String BeneficiaryNameTextBoxId() {
        return "ctl00_cntMainBody_BENEFICIARY__NAME";
    }

    public static String BeneficiarySurnameTextBoxId() {
        return "ctl00_cntMainBody_BENEFICIARY__SURNAME";
    }

    public static String BeneficiaryDOBTextBoxId() {
        return "ctl00_cntMainBody_BENEFICIARY__DOB";
    }

    public static String BeneficiaryIDNumberTextBoxId() {
        return "ctl00_cntMainBody_BENEFICIARY__ID_NUMBER";
    }

    public static String BeneficiaryPercentageTextBoxId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_BENEFICIARY__PERCENTAGE";
        } else {
            return "ctl00_cntMainBody_BENEFICIARY__PERC";
        }

    }

    public static String SumInsuredDeathTextBoxId() {
        return "ctl00_cntMainBody_RISK_ITEM__D_SI";
    }

    public static String RegOwnerTextBoxId() {
        return "ctl00_cntMainBody_MS__REG_OWNER";
    }

    public static String SumInsuredDeathDropdownListId() {
        return "ctl00_cntMainBody_INDIVIDUALS__DEATH_COVER";
    }

    public static String SumInsuredPermanentDisablityTextBoxId() {
        return "ctl00_cntMainBody_RISK_ITEM__PD_SI";
    }

    public static String SumInsuredTemporaryDisablityTextBoxId() {
        return "ctl00_cntMainBody_RISK_ITEM__TD_SI";
    }

    public static String SumInsuredMedicalExpensesTextBoxId() {
        return "ctl00_cntMainBody_RISK_ITEM__ME_SI";
    }

    public static String PersonalAccidentNotesTextBoxId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }

    public static String EnrouteNumberofPersonsTextBoxId() {
        return "ctl00_cntMainBody_PLENROUTE__NO_O_PERS";
    }

    public static String EnrouteRegNumberTextBoxId() {
        return "ctl00_cntMainBody_PLENROUTE__REG_NUMBER";
    }

    public static String EnrouteVINChassisNumberTextBoxId() {
        return "ctl00_cntMainBody_PLENROUTE__VIN_CHASSIS_NUMBER";
    }

    public static String EnrouteCommentsTextAreaId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }

    public static String NameofVesselTextBoxId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_PC__VESSEL_NAME";
        } else {
            return "ctl00_cntMainBody_VESSEL__NAME";
        }
    }

    public static String NameofVesselSADCTextBoxId() {
        return "ctl00_cntMainBody_PC__VESSEL_NAME";

    }

    public static String MakeandModelTextBoxId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_PC__VESSEL_MAKE";
        } else {
            return "ctl00_cntMainBody_VESSEL__MAKE";
        }
    }

    public static String MakeandModelSADCTextBoxId() {
        return "ctl00_cntMainBody_PC__VESSEL_MAKE";

    }

    public static String VesselDescTextBoxId() {
        return "ctl00_cntMainBody_VESSEL__TYPE_DESC";
    }

    public static String MaterialofHullTextBoxId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_PC__HULL_MATERIAL";
        } else {
            return "ctl00_cntMainBody_HULL__MAT_OF_HULL";
        }
    }

    public static String InterestedPartyDescriptionSADCTextboxId() {
        return "ctl00_cntMainBody_INTERESTED_PARTIES__DESCRIPTION";
    }

    public static String MaterialofHullSADCTextBoxId() {

        return "ctl00_cntMainBody_PC__HULL_MATERIAL";
    }

    public static String LengthofHullSADCTextBoxId() {
        return "ctl00_cntMainBody_PC__HULL_LENGTH";
    }

    public static String HullYearofManufactureSADCTextBoxId() {
        return "ctl00_cntMainBody_PC__HULL_YEAR";
    }

    public static String HullSumInsuredTextBoxId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_PC__HULL_SI";
        } else {
            return "ctl00_cntMainBody_HULL__SI";
        }
    }

    public static String HullSumInsuredSADCTextBoxId() {
        return "ctl00_cntMainBody_PC__HULL_SI";
    }

    public static String VesselHorsePowerTextboxId() {
        return "ctl00_cntMainBody_PC__VESSEL_HORSE_POWER";
    }

    public static String SerialNumberTextBoxId() {
        return "ctl00_cntMainBody_PC__HULL_SN";

    }

    public static String HullRateRequiredTextBoxId() {
        return "ctl00_cntMainBody_HULL__RATE_PERC";
    }

    public static String DroppingOffMotorSumTextBoxId() {
        return "ctl00_cntMainBody_EXTENSIONS__EXT_DROPOM_SI";
    }

    public static String SubmergerObjectsSumTextBoxId() {
        return "ctl00_cntMainBody_EXTENSIONS__EXT_SUBOB_SI";
    }

    public static String DroppingOffMotorRateTextBoxId() {
        return "ctl00_cntMainBody_EXTENSIONS__EXT_DROPOM_RATE_PERC";
    }

    public static String WaterandPleasureCraftCommentsTextAreaId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }

    public static String Engine1SumInsuredTextBoxId() {
        return "ctl00_cntMainBody_VESSEL__ENGINE_SI";
    }

    public static String Engine1SumInsuredSADCTextBoxId() {
        return "ctl00_cntMainBody_PC__ENGINE_SI";
    }

    public static String Engine1YearTextBoxId() {
        return "ctl00_cntMainBody_PC__ENGINE_YEAR";
    }

    public static String Engine1MakeTextBoxId() {
        return "ctl00_cntMainBody_PC__ENGINE_MAKE";
    }

    public static String Engine1SerialNumberTextBoxId() {
        return "ctl00_cntMainBody_PC__ENGINE_SN";
    }

    public static String Engine2SumInsuredSADCTextBoxId() {
        return "ctl00_cntMainBody_PC__ENGINE2_SI";
    }

    public static String Engine2YearTextBoxId() {
        return "ctl00_cntMainBody_PC__ENGINE2_YEAR";
    }

    public static String Engine2MakeTextBoxId() {
        return "ctl00_cntMainBody_PC__ENGINE2_MAKE";
    }

    public static String Engine2SerialNumberTextBoxId() {
        return "ctl00_cntMainBody_PC__ENGINE2_SN";
    }

    public static String EndorsementsBodyId() {
        return "tinymce";
    }

    public static String EndorsementsLine2ParagraphXpath() {
        return "//*[@id=\"tinymce\"]/div/p[3]";
    }

    public static String EndorsementsLine3ParagraphXpath() {
        return "//*[@id=\"tinymce\"]/div/p[4]";
    }

    public static String EndorsementsLine4ParagraphXpath() {
        return "//*[@id=\"tinymce\"]/div/p[5]";
    }

    public static String Engine1RateTextBoxId() {
        return "ctl00_cntMainBody_VESSEL__ENGINE_R_PERC";
    }

    public static String VehicleModelTextBoxId() {
        return "ctl00_cntMainBody_MS__MODEL";
    }

    public static String VehicleMandMCodeTextBoxId() {
        return "ctl00_cntMainBody_MS__MM_CODE";
    }

    public static String VehicleRegNumberTextBoxId() {
        return "ctl00_cntMainBody_MS__REG_NUM";
    }

    public static String VehicleRegOwnerTextBoxId() {
        return "ctl00_cntMainBody_MS__REG_OWNER";
    }

    public static String VehicleRegDateTextBoxId() {
        return "ctl00_cntMainBody_MS__REG_ORIGINAL_DATE";
    }

    public static String VehicleSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_MS__MAX_LOI";
    }

    public static String VehicleExtensionsRateTextBoxId() {
        return "ctl00_cntMainBody_AGREED_RATE_192";
    }

    public static String VehicleExtensionsPremiumTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_192";
    }

    public static String SubsidenceandLandslipPremiumTextBoxId() {
        return "ctl00_cntMainBody_BUILDING__SL_PREMIUM";
    }

    public static String ClientCodeTextBoxId() {
        return "ctl00_cntMainBody_txtClientCode";
    }

    public static String MotorCommentsTextBoxId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }

    public static String ReinssuranceSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl03_txtSumInsured";
    }

    public static String ReinssuranceUnallocatedAmountLabelId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl10_lblSumInsured";
    }

    public static String FAPThirdPartyMinAmountTextboxId() {
        return "ctl00_cntMainBody_MS__FAP_3RD_PARTY_MIN_AMT";
    }

    public static String FAPTFireandExplosionTextboxId() {
        return "ctl00_cntMainBody_MS__FAP_FI_MIN_AMT";
    }

    public static String MainDriverRegularDriverTextboxId() {
        return "ctl00_cntMainBody_MS__DRIVER_NAME";
    }

    public static String AreaCodeTextboxId() {
        return "ctl00_cntMainBody_MS__AREA_CODE";
    }

    public static String MotorSectionRegularDriverTextboxId() {
        return "ctl00_cntMainBody_MAIN_DRIVERS__DRIVER_NAME";
    }

    public static String MainDriverDateofBirthTextboxId() {
        return "ctl00_cntMainBody_MS__DOB";
    }

    public static String MotorSectionDateofBirthTextboxId() {
        return "ctl00_cntMainBody_MAIN_DRIVERS__DATE_OF_BIRTH";
    }

    public static String MainDriverDateLicenseIssuedTextboxId() {
        return "ctl00_cntMainBody_MS__LICENCE_ISSUED_DATE";
    }

    public static String MainDriveIDNumberTextboxId() {
        return "ctl00_cntMainBody_MS__ID_NUMBER";
    }

    public static String InsuredDetailsDateofBirthTextboxId() {
        return "ctl00_cntMainBody_PREMISES__INS_DOB";
    }

    public static String EngineNumberTextboxId() {
        return "ctl00_cntMainBody_MS__ENGINE_NUM";
    }

    public static String GVMTextboxId() {
        return "ctl00_cntMainBody_MS__GVM";
    }

    public static String ChassisNumberTextboxId() {
        return "ctl00_cntMainBody_MS__CHASSIS_NUM";
    }

    public static String RegistrationNumberTextboxId() {
        return "ctl00_cntMainBody_MS__REG_NUM";
    }

    public static String VehicleCoverDetailsAreaCodeTextboxId() {
        return "ctl00_cntMainBody_MS__AREA_CODE";
    }

    public static String RegisteredOwnerTextboxId() {
        return "ctl00_cntMainBody_MS__REG_OWNER";
    }

    public static String RegistrationDateTextboxId() {
        return "ctl00_cntMainBody_MS__REG_ORIGINAL_DATE";
    }

    public static String MotorSectionDateLicenseIssuedTextboxId() {
        return "ctl00_cntMainBody_MAIN_DRIVERS__DATE_LICENSE_ISSUED";
    }

    public static String MotorSectionIDNumberTextboxId() {
        return "ctl00_cntMainBody_MAIN_DRIVERS__ID_NUMBER";
    }

    public static String AddSurveyButtonName() {
        return "ctl00$cntMainBody$PREMISES__PREM_SURVEY_REPORT$ctl02$ctl00";
    }

    public static String AdjustmentPercentageTextboxId() {
        return "ctl00_cntMainBody_MS__FLAT_ADJ_PERC";
    }

    public static String ReinsuranceSumInsuredTextBoxId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl03_txtSumInsured";
    }

    public static String ReinsuranceSumInsuredCompany2TextBoxId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl04_txtSumInsured";
    }

    public static String AddEquipmentButtonName() {
        return "ctl00$cntMainBody$MS__ACCESSORIES$ctl02$ctl00";

    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Buttons">
    public static String clickPopUpOkByID() {
        return "popup_ok";
    }

    public static String AddAccessoriesButtonName() {
        return "ctl00$cntMainBody$MS__ACCESSORIES$ctl02$ctl00";
    }

    public static String AddOtherExtensionsButtonName() {
        return "ctl00$cntMainBody$MBCL__MBCL_EXTENSIONS$ctl02$ctl00";
    }

    public static String CLAddOtherExtensionsButtonName() {
        return "ctl00$cntMainBody$MBCL__CL_EXTENSIONS$ctl02$ctl00";
    }

    public static String ChangeAddressButtonId() {
        return "changeButton";
    }

    public static String InterestedPartyAddNotesLabelId() {
        return "ctl00_cntMainBody_EPL_IPARTIES__ADD_NOTES";
    }

    public static String VehiclesCaptureCriteriaButtonId() {
        return "btnCaptureCriteria";
    }

    public static String AddSpecifiedDriverButtonName() {
        return "ctl00$cntMainBody$MS__DRIVER$ctl02$ctl00";
    }

    public static String AddSpecifiedDriverButtonXpath() {
        return "//*[@id=\"ctl00_cntMainBody_MS__DRIVER\"]/table/tfoot/tr/td[3]/input";
    }

    public static String AddSpecDriverButtonName() {
        return "ctl00$cntMainBody$MS__DRIVER$ctl03$ctl00";
    }

    public static String AddMachineryBreakdownItemeButtonName() {
        return "ctl00$cntMainBody$MBCL__MACH_ITEMS$ctl02$ctl00";
    }

    public static String SearchButtonId() {
        return "ctl00_cntMainBody_btnSearch";
    }

    public static String SelectReinsurereFromAlistById() {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_btnSelect";
    }

    public static String EndorsementsSaveButtonId() {
        return "ctl00_cntMainBody_btnSave";
    }

    public static String GenerateDocButtonId() {
        return "btnGenerateDoc";
    }

    public static String NewQuoteButtonId() {
        return "ctl00_cntMainBody_ctrlNewQuote_btnNewQuote";
    }

    public static String AgentCodeButtonId() {
        return "ctl00_cntMainBody_btnAgentCode";
    }

    public static String SearchAgentButtonId() {
        return "ctl01_cntMainBody_btnSearch";
    }

    public static String BasicDetailsNextButtonName() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String AddSurveyButtonID() {
        return "ctl00$cntMainBody$PREMISES__PREM_SURVEY_REPORT$ctl02$ctl00";
    }

    public static String AddInterestedPartiesButtonID() {
        return "ctl00$cntMainBody$BUILDING__BUILD_IP$ctl02$ctl00";
    }

    public static String SurveyNextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String SurveyRateButtonId() {
        return "ctl00_cntMainBody_btnRate";
    }

    public static String HomeAssistPremiumAjdBasisDropdownListId() {
        return "ctl00_cntMainBody_SD__HOME_ASSIST_ADJ_BASIS";
    }

    public static String HomeAssistFlatPremiumTextBoxId() {
        return "ctl00_cntMainBody_SD__HOME_ASSIST_FLAT_PREMIUM_AMOUNT";
    }

    public static String NotesCommentsTextboxId() {
        return "ctl00_cntMainBody_EPL_IPARTIES__COMMENTS";
    }

    public static String NotesAddNotesCheckboxId() {
        return "ctl00_cntMainBody_EPL_IPARTIES__ADD_NOTES";
    }

    public static String NotesNotesTexBboxId() {
        return "ctl00_cntMainBody_EPL_IPARTIES__NOTES";
    }

    public static String HomeAssistPremiumAjdReasonDropdownListId() {
        return "ctl00_cntMainBody_SD__HOME_ASSIST_ADJ_REASON";
    }

    public static String SurveyCommentsNextButtonName() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String RiskCoverageNextButtonName() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String AddInterestedPartiesButtonName() {
        return "ctl00$cntMainBody$BUILDING__BUILD_IP$ctl02$ctl00";
    }

    public static String AddAllRiskItemsButtonName() {
        return "ctl00$cntMainBody$ALL_RISKS__ALL_RISK_ITEM$ctl02$ctl00";
    }

    public static String MSAddInterestedPartiesButtonName() {
        return "ctl00$cntMainBody$MS__EPL_IPARTIES$ctl02$ctl00";
    }

//    public static String InterestedPartiesButtonName() {
//        return "ctl00$cntMainBody$MS__EPL_IPARTIES$ctl02$ctl00";
//    }

    public static String PCAddInterestedPartiesButtonName() {
        return "ctl00$cntMainBody$PC_ITEMS__PC_ITEMS_IP$ctl02$ctl00";
    }

    public static String AllRiskItemAddInterestedPartiesButtonName() {
        return "ctl00$cntMainBody$ALL_RISK_ITEM__INTERESTED_PARTY$ctl02$ctl00";
    }

    public static String ContentsAddInterestedPartiesButtonName() {
        return "ctl00$cntMainBody$CONTENT__CONTENT_IP$ctl02$ctl00";
    }

    public static String InterestedPartiesNextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String InterestedPartiesCommentsNextButtonName() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String BuildingsNextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String BuildingsRateButtonId() {
        return "ctl00_cntMainBody_btnRate";
    }

    public static String AddEndorsementButtonName() {
        return "ctl00$cntMainBody$PREMISES__S_ENDORSEMENT$ctl02$ctl00";
    }

    public static String AddPersonalComputerButtonName() {
        return "ctl00$cntMainBody$PERSONAL_COMPUTER__PC_ITEMS$ctl02$ctl00";
    }

    public static String AllRiskItemAddButtonName() {
        return "ctl00$cntMainBody$ALL_RISKS__ALL_RISK_ITEM$ctl02$ctl00";
    }

    public static String AddEndorsementClauseButtonId() {
        return "ctl00_cntMainBody_RISK_ITEM__ENDORSEMENTS_PckTemplates_AddCmd";
    }

    public static String EndorsementNextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String AddRiskButtonId() {
        return "ctl00_cntMainBody_MultiRisk1_btnAddRisk";
    }

    public static String AddPersonalAccidentRiskButtonName() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00$cntMainBody$PA__INDIVIDUALS$ctl02$ctl00";
        } else {
            return "ctl00$cntMainBody$RISK_DETAILS__RISK_ITEM$ctl02$ctl00";
        }

    }

    public static String AddBeneficiaryButtonName() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00$cntMainBody$INDIVIDUALS__BENEFICIARY$ctl02$ctl00";
        } else {
            return "ctl00$cntMainBody$RISK_ITEM__BENEFICIARY$ctl02$ctl00";
        }

    }

    public static String BeneficiaryNextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String PersonalAccidentRateButtonId() {
        return "ctl00_cntMainBody_btnRate";
    }

    public static String IndividualNextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String EndorsementsSelectButtonId() {
        return "_btnShowSelect";
    }

    public static String EndorsementsAddAllButtonId() {
        return "ctl00_cntMainBody_MS__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
    }

    public static String EndorsementsApplyButtonId() {
        return "ctl00_cntMainBody_MS__ENDORSEMENTS_btnApplySelection";
    }

    public static String EndorsementsAddButtonName() {
        return "ctl00$cntMainBody$PREMISES__S_ENDORSEMENT$ctl02$ctl00";
    }

    public static String EndorsementsAddAllButtonID() {
        return "ctl00_cntMainBody_PL__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
    }

    public static String EndorsementsApplyButtonID() {
        return "ctl00_cntMainBody_PL__ENDORSEMENTS_btnApplySelection";
    }

    public static String AddClauseButtonId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_INDIVIDUALS__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
        } else {
            return "ctl00_cntMainBody_RISK_ITEM__ENDORSEMENTS_PckTemplates_AddCmd";
        }
    }

    public static String ClauseApplyButtonId() {

        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_INDIVIDUALS__ENDORSEMENTS_btnApplySelection";
        } else {
            return "ctl00_cntMainBody_RISK_ITEM__ENDORSEMENTS_btnApplySelection";
        }
    }

    public static String PersonalAccidentNextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String ReinsuranceOkButtonId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_btnOk";
    }

    public static String CompanySelectId() {
        return "ctl00_cntMainBody_grdvSearchResults_ctl08_btnSelect";
    }

    public static String RisksAddRiskButtonId() {
        return "ctl00_cntMainBody_MultiRisk1_btnAddRisk";
    }

    public static String EnrouteNextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String HullNextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String WaterandPleasureNextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String PersonalLinesNextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String AddPropFACButtonId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_btnAddFacProp";
    }

    public static String MakePolicyLiveButtonId() {
        return "ctl00_cntMainBody_btnBuy";
    }

    public static String MotorNextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String VehicleNextButtonId() {
        return "btnCaptureCriteria";
    }

    public static String MotorRateButtonId() {
        return "ctl00_cntMainBody_btnRate";
    }

    public static String SeaButtonName() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String PolicyRefLabelId() {
        return "ctl00_ClientInfoCtrl_lblPolicyRef";
    }

    public static String DriverDetailsAddButtonName() {
        return "ctl00$cntMainBody$MSD__MAIN_DRIVERS$ctl02$ctl00";
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Links">
    public static String SelectClientLinkId() {
        return "ctl00_cntMainBody_grdvSearchResults_ctl02_lnkDetails";
    }

    public static String BulletListLinkId() {
        return "ctl00_cntMainBody_txtDocumentEditor_bullist";
    }

    public static String SADCEditEndorsementLinkId() {
        return "ctl00_cntMainBody_PC__ENDORSEMENTS_grdWordings_ctl02_lnkEdit";
    }

    public static String SelectAgentLinkId() {
        return "menu_ABSA01B";
    }

    public static String SelectRiskTypeResultsId() {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl02_lnkbtnSelect";
    }

    public static String SelectDomesticAndPropertyId() {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl05_lnkbtnSelect";
    }

    public static String RiskTypePersonalAccidentLinkId() {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl06_lnkbtnSelect";
    }

    public static String RiskTypeEnrouteLinkId() {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl03_lnkbtnSelect";
    }
    //ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl04_lnkbtnSelect

    public static String RiskTypeWaterandPleasureCraftLinkId() {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl07_lnkbtnSelect";
    }
    //ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl05_lnkbtnSelect

    public static String SelectReinsurerLinkId() {
        return "ctl00_cntMainBody_grdvSearchResults_ctl03_btnSelect";
    }

    public static String RiskTypeMotorLinkId() {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl04_lnkbtnSelect";
    }
    //ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl06_lnkbtnSelect

    public static String PersonalLinesEditLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnEdit";
    }

    public static String WaterandPleasureCraftEditLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl04_lnkbtnEdit";
    }

    public static String WaterandPleasureCraftScenario4_2_1EditLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl05_lnkbtnEdit";
    }
    // </editor-fold>

// <editor-fold defaultstate="collapsed" desc="DropdwonLists">
    public static String SelectProductDropdownListId() {
        return "ctl00_cntMainBody_ctrlNewQuote_ddlProductlst";
    }

    public static String BasisofInsuranceDropdownListId() {
        return "ctl00_cntMainBody_MACH_ITEMS__BASIS_OF_INS";
    }

    public static String BasisofDeductibleDropdownListId() {
        return "ctl00_cntMainBody_MACH_ITEMS__BOD";
    }

    public static String TimeDeductibleUnitDropdownListId() {
        return "ctl00_cntMainBody_MACH_ITEMS__CL_TIME_UNIT";
    }

    public static String IncreasedCostofWorkingTimeDeductibleUnitDropdownListId() {
        return "ctl00_cntMainBody_MACH_ITEMS__CL_ICOW_TIME_UNIT";
    }

    public static String AdditionalIncreasedCostofWorkingTimeDeductibleUnitDropdownListId() {
        return "ctl00_cntMainBody_MACH_ITEMS__CL_AICOW_TIME_UNIT";
    }

    public static String GrossProfitRatingBasisTypeDropdownListId() {
        return "ctl00_cntMainBody_MBCL__CLRB_GPB_TYPE";
    }

    public static String GrossProfitIndemnityPeriodUnitDropdownListId() {
        return "ctl00_cntMainBody_MACH_ITEMS__CL_IP_UNIT";
    }

    public static String IncreasedCostofWorkingIndemnityPeriodUnitDropdownListId() {
        return "ctl00_cntMainBody_MACH_ITEMS__CL_ICOW_IP_UNIT";
    }

    public static String AdditionalIncreasedCostofWorkingIndemnityPeriodUnitDropdownListId() {
        return "ctl00_cntMainBody_MACH_ITEMS__CL_AICOW_IP_UNIT";
    }

    public static String AdjustmentBasisDropdownListId() {
        return "cboPremAdjBasis";
    }

    public static String AdjustmentReasonDropdownListId() {
        return "ctl00_cntMainBody_MS__ADJUSTMENT_REASON";
    }

    public static String SumInsuredTemporalDisabilityDropdownListId() {
        return "ctl00_cntMainBody_INDIVIDUALS__TD_COVER";
    }

    public static String SumInsuredMedicalExpensesDropdownListId() {
        return "ctl00_cntMainBody_INDIVIDUALS__ME_COVER";
    }

    public static String VehicleModelDropdownListId() {
        return "model";
    }

    public static String VehicleModel2TextBoxId() {
        return "otherModel";
    }

    public static String CCTextBoxId() {
        return "otherCc";
    }

    public static String GVMTextBoxId() {
        return "otherGvm";
    }

    public static String MainDriverGenderDropdownListId() {
        return "ctl00_cntMainBody_MS__GENDER";
    }

    public static String MainDriverIDTypeRequiredDropdownListId() {
        return "ctl00_cntMainBody_MS__ID_TYPE";
    }

    public static String SelectPaymentTermDropdownListId() {
        return "ctl00_cntMainBody_POLICYHEADER__PAYMENTTERM";
    }

    public static String MainDriverDropdownListId() {
        return "cboSelectDriver";
    }

    public static String ClaimFreeGroupDropdownListId() {
        return "ctl00_cntMainBody_MS__CFG";
    }

    public static String VehicleOvernightParkingDropdownListId() {
        return "ctl00_cntMainBody_MS__PARKED_AT_NIGHT";
    }

    public static String VehicleTrackingDeviceDropdownListId() {
        return "ctl00_cntMainBody_MS__PARKED_AT_NIGHT";
    }

    public static String MotorTrackingDeviceDropdownListId() {
        return "ctl00_cntMainBody_MS__TRACKING_DEVICE";
    }

    public static String MotorClassofUseDropdownListId() {
        return "ctl00_cntMainBody_MS__CLASS_USE";
    }

    public static String MotorSectionGenderDropdownListId() {
        return "ctl00_cntMainBody_MAIN_DRIVERS__GENDER";
    }

    public static String buildingsSectionsuminsuredDropdownListId() {
        return "ctl00_cntMainBody_SD__PL_SUM_INSURED";
    }

    public static String BuildingsReasonDropdownListId() {
        return "ctl00_cntMainBody_SD__HOME_ASSIST_ADJ_REASON";
    }

    public static String MotorSectionIDTypeRequiredDropdownListId() {
        return "ctl00_cntMainBody_MAIN_DRIVERS__ID_TYPE";
    }

    public static String MotorSectionDriverLicenseTypeDropdownListId() {
        return "ctl00_cntMainBody_MAIN_DRIVERS__LICENSE_TYPE";
    }

    public static String AccidentsDropdownListId() {
        return "ctl00_cntMainBody_MS__CFG_ACCIDENT";
    }

    public static String OtherClaimsDropdownListId() {
        return "ctl00_cntMainBody_MS__CFG_OTHER";
    }

    public static String SelectCollectionFrequencyDropdownListId() {
        return "ctl00_cntMainBody_POLICYHEADER__COLLECTIONFREQUENCY";
    }

    public static String SelectSurveyTypeDropdownListId() {
        return "ctl00_cntMainBody_PREM_SURVEY_REPORT__SRT";
    }

    public static String TypeofAgreementTypeDropdownListId() {
        return "ctl00_cntMainBody_BUILD_IP__BUILD_IP_TOA";
    }

    public static String MSTypeofAgreementTypeDropdownListId() {
        return "ctl00_cntMainBody_EPL_IPARTIES__TOA";
    }

    public static String PremAdjBasisDropdownListId() {
        return "cboPremAdjBasis";
    }

    public static String addFACXOLBUttonId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_btnAddFacXOL";
    }

    public static String enterReInsuranceCode() {
        return "ctl00_cntMainBody_txtReinsurerCode";
    }

    public static String PremAdjReasonDropdownListId() {
        return "ctl00_cntMainBody_MS__ADJUSTMENT_REASON";
    }

    public static String BandDropdownListId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_ddlReinsurance";
    }

    public static String PCTypeofAgreementDropdownListId() {
        return "ctl00_cntMainBody_PC_ITEMS_IP__SADC_TYPE_OF_AGREEMENT";
    }

    public static String AllRiskItemTypeofAgreementDropdownListId() {
        return "ctl00_cntMainBody_INTERESTED_PARTY__TYPE_OF_AGREEMENT";
    }

    public static String ContentsTypeofAgreementTypeDropdownListId() {
        return "ctl00_cntMainBody_CONTENT_IP__SADC_TYPE_OF_AGREEMENT";
    }

    public static String InstitutionNameDropdownListId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_BUILD_IP__SADC_INSTITUTION";
        } else {
            return "ctl00_cntMainBody_BUILD_IP__BUILD_IP_INST_NAME";
        }
    }

    public static String AllRiskItemInstitutionNameDropdownListId() {
        return "ctl00_cntMainBody_INTERESTED_PARTY__INSTITUTION";
    }

    public static String PCInstitutionNameDropdownListId() {
        return "ctl00_cntMainBody_PC_ITEMS_IP__SADC_INSTITUTION";
    }

    public static String AdditionalTheftMaxPercetageTextBoxID() {
        return "ctl00_cntMainBody_MS__FAP_TH_MAX_PERC";
    }

    public static String MSInstitutionNameDropdownListId() {
        return "ctl00_cntMainBody_EPL_IPARTIES__SADC_INSTITUTION_NAME";
    }

    public static String InterestedPartyInstitutionNameDropdownListId() {
        return "ctl00_cntMainBody_EPL_IPARTIES__IN_PARTY";
    }

    public static String InterestedPartyDescriptionTextBoxId() {
        return "ctl00_cntMainBody_EPL_IPARTIES__IN_PARTY_DESC";
    }

    public static String ContentsInstitutionNameDropdownListId() {
        return "ctl00_cntMainBody_CONTENT_IP__SADC_INSTITUTION";
    }

    public static String EBPMotorIPInstitutionNameDropdownListId() {
        return "ctl00_cntMainBody_EPL_IPARTIES__IN_PARTY";
    }

    public static String EBPMotorIPTypeofAgreementDropdownListId() {
        return "ctl00_cntMainBody_EPL_IPARTIES__TOA";
    }

    public static String EBPMotorIPDescriptionTextBoxID() {
        return "ctl00_cntMainBody_EPL_IPARTIES__IN_PARTY_DESC";
    }

    public static String NoClaimDiscountDropdownListId() {
        return "ctl00_cntMainBody_HOUSEHOLD__NO_CLAIM_DISCOUNT";
    }

    public static String ContentsAdditionalVoluntaryExcessDropdownListId() {
        return "ctl00_cntMainBody_CONTENT__SADC_ADD_VOL_EXCESS";
    }

    public static String AllRiskCategoryDropdownListId() {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__CAT";
    }

    public static String EndorsementSectionsDropdownListId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_S_ENDORSEMENT__RISKS";
        } else {
            return "ctl00_cntMainBody_S_ENDORSEMENT__SECTIONS";
        }
    }

    public static String BeneficiaryIDTypeRequiredDropdownListId() {
        return "ctl00_cntMainBody_BENEFICIARY__ID_TYPE";
    }

    public static String SumInsuredNumberofWeeksDropdownListId() {
        return "ctl00_cntMainBody_RISK_ITEM__TD_NO_O_WEEKS";
    }

    public static String PlanTypeDropdownListId() {
        return "ctl00_cntMainBody_PLENROUTE__PLANT";
    }

    public static String TempDisabilityWeeksDropdownListId() {
        return "ctl00_cntMainBody_PLENROUTE__TD_WEEKS";
    }

    public static String TypeofVesselDropdownListId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_PC__VESSEL_TYPE";
        } else {
            return "ctl00_cntMainBody_VESSEL__TYPE";
        }

    }

    public static String TypeofVesselSADCDropdownListId() {
        return "ctl00_cntMainBody_PC__VESSEL_TYPE";

    }

    public static String MotorBoatSpeedDropdownListId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_PC__VESSEL_MAX_SPEED";
        } else {
            return "ctl00_cntMainBody_VESSEL__MAX_SPEED";
        }
    }

    public static String MotorBoatSpeedSADCDropdownListId() {
        return "ctl00_cntMainBody_PC__VESSEL_MAX_SPEED";

    }

    public static String NumberofEnginesDropdownListId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_PC__ENGINE_NO_OF";
        } else {
            return "ctl00_cntMainBody_VESSEL__NO_OF_ENG";
        }
    }

    public static String NumberofEnginesSADCDropdownListId() {
        return "ctl00_cntMainBody_PC__ENGINE_NO_OF";
    }

    public static String EngineTypeDropdownListId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_PC__ENGINE_TYPE";
        } else {
            return "ctl00_cntMainBody_VESSEL__ENGINE_TYP";
        }
    }

    public static String EngineTypeSADCDropdownListId() {
        return "ctl00_cntMainBody_PC__ENGINE_TYPE";
    }

    public static String HullUseDropdownListId() {
        return "ctl00_cntMainBody_HULL__HULL_USE";
    }

    public static String TypeofVehicleDropdownListId() {
        return "ctl00_cntMainBody_MS__VEHICLE_TYPE";
    }

    public static String NoClaimRebateDropdownListId() {
        return "ctl00_cntMainBody_MS__CFG";
//        /return "ctl00_cntMainBody_MS__NCR_REBATE";
    }

    public static String VehicleClassofUseDropdownListId() {
        return "ctl00_cntMainBody_MS__CLASS_USE";
    }
    
    public static String TrackerDropdownListId() {
        return "ctl00_cntMainBody_MS__TRACKING_DEVICE";
    }

    public static String NATISCodeDropdownListId() {
        return "ctl00_cntMainBody_MS__NATIS_CODE";
    }

    public static String CoverTypeDropdownListId() {
        return "ctl00_cntMainBody_MS__COVER_TYPE";
    }

    public static String TradeClassDropdownListId() {
        return "ctl00_cntMainBody_MS__TRADE_CLASS";
    }

//     public static String VehicleMakeDropdownListId()
//    {
//        return "otherMake";
//    }
    public static String VehicleMakeDropdownListId() {
        return "make";
    }

    public static String MakeDropdownListId() {
        return "make";
    }

    public static String VehicleMakeOlderVehiclesDropdownListId() {
        return "otherMake";
    }

    public static String VehicleBodyTypeOlderVehiclesDropdownListId() {
        return "otherBodyType";
    }

    public static String ModelDropdownListId() {
        return "model";
    }

    public static String ClaimRebateDropdownListId() {
        return "ctl00_cntMainBody_MS__NCR_REBATE";
    }

    public static String BodyTypeDropdownListId() {
        return "otherBodyType";
    }

    public static String VehicleTypeDropdownListId() {
        return "otherVehicleType";
    }

    public static String VehicleSourceDropdownListId() {
        return "source";
    }

    public static String BasisofSettlementDropdownListId() {
        return "ctl00_cntMainBody_MS__VEH_COVER_METHOD";
    }

    public static String VehicleYearDropdownListId() {
        return "year";
    }

    public static String VehicleYearTextBoxId() {
        return "otherYear";
    }

    public static String VehicleCoverTypeDropdownListId() {
        return "ctl00_cntMainBody_MS__COVER_TYPE";
    }

    public static String CarHireLimitofIndemnityDropdownListId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_7";
    }

    public static String CarHireConditionsDropdownListId() {
        return "ctl00_cntMainBody_CAR_HIRE_CONDITIONS_7";
    }

    public static String TypeofResidenceDropdownListId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_PREMISES__SADC_TYPE_OF_RESIDENCE";
        } else {
            return "ctl00_cntMainBody_PREMISES__TOR";
        }

    }

    public static String TypeofConstructionDropdownListId() {
        return "ctl00_cntMainBody_PREMISES__TOC";
    }

    public static String WallConstructionDropdownListId() {
        return "ctl00_cntMainBody_PREMISES__SADC_WALL_CONSTRUCTION";
    }

    public static String PeriodUnoccupiedDropdownListId() {
        return "ctl00_cntMainBody_PREMISES__SADC_PERIOD_UNOCCUPIED";
    }

    public static String SubsidenceAndLandslipGeotechReportDropdownListId() {
        return "ctl00_cntMainBody_SURVEY_REPORT_119";
    }

    public static String BuildingsAdjustmentReasonDropdownListId() {
        return "ctl00_cntMainBody_BUILDING__SADC_ADJ_REAS";
    }

    public static String SADCDLocalityropdownListId() {
        return "ctl00_cntMainBody_PREMISES__SADC_LOCALITY";
    }

    public static String enterFACLowerLimitId() {
        return "ctl00_cntMainBody_txtLower";
    }

    public static String enterFACUpperLimitId() {
        return "ctl00_cntMainBody_txtUpper";
    }

    public static String enterFACPaticipationId() {
        return "ctl00_cntMainBody_grdPlacements_ctl02_txtParticipation";
    }

    public static String enterFACPremiunId() {
        return "ctl00_cntMainBody_grdPlacements_ctl02_txtPremium";
    }

    public static String enterFACOkButtonById() {
        return "ctl00_cntMainBody_btnOk";
    }

    public static String RoofConstructionDropdownListId() {
        return "ctl00_cntMainBody_PREMISES__SADC_ROOF_CONSTRUCTION";
    }

    public static String OccupancyDropdownListId() {
        return "ctl00_cntMainBody_PREMISES__OCC";
    }

    public static String LocalityDropdownListId() {
        return "ctl00_cntMainBody_PREMISES__LOC";
    }

    public static String CurrentInsurancePeriodDropdownListId() {
        return "ctl00_cntMainBody_BUILDING__SADC_CUR_INS_PER";
    }

    public static String ContentsCurrentInsurancePeriodDropdownListId() {
        return "ctl00_cntMainBody_CONTENT__SADC_CUR_INS_PER";
    }

    public static String ContentsTypeofCoverDropdownListId() {
        return "ctl00_cntMainBody_CONTENT__SADC_COVER_TYPE";
    }

    public static String ContentsTypeofAlarmDropdownListId() {
        return "ctl00_cntMainBody_CONTENT__SADC_ALARM_TYPE";
    }

    public static String AdditionalVoluntaryExcessDropdownListId() {
        return "ctl00_cntMainBody_BUILDING__SADC_ADD_VOL_EXCESS";
    }

    public static String PCAdjustmentReasonDropdownListId() {
        return "ctl00_cntMainBody_PC_ITEMS__SADC_REASON";
    }

    public static String IndividualsAdjustmentReasonDropdownListId() {
        return "ctl00_cntMainBody_INDIVIDUALS__ADJ_REASON";
    }

    public static String MotorSpecifiedAdjustmentReasonDropdownListId() {
        return "ctl00_cntMainBody_MS__SADC_ADJ_REASON";
    }

    public static String AllRiskAdjustmentReasonDropdownListId() {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__SADC_ADJ_REAS";
    }

    public static String MechanicalBreakdownAdjustmentReasonDropdownListId() {
        return "ctl00_cntMainBody_MECH_AND_ELECT_BDOWN__SADC_REASON";
    }

    public static String ReasonDropdownListId() {
        return "ctl00_cntMainBody_BUILDING__ADJ_REAS";
    }

    public static String ContentsAdjustmentReasonDropdownListId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_CONTENT__SADC_REASON";
        } else {
            return "ctl00_cntMainBody_HOUSEHOLD__ADJ_REAS";
        }

    }

    public static String OccupationDropdownListId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_INDIVIDUALS__OCCUPATION";
        } else {
            return "ctl00_cntMainBody_RISK_ITEM__OCCUPATION_LIST";
        }

    }

    public static String DAPRiskItemCategoryDropdownListId() {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__CAT";
    }

    public static String SumInsuredDropdownListId() {
        return "ctl00_cntMainBody_ALL_RISK_ITEM__CAT";
    }
    // </editor-fold>

// <editor-fold defaultstate="collapsed" desc="CheckBoxes">
    public static String CheckBuildingCheckBoxId() {
        return "ctl00_cntMainBody_PREMISES__BUILDINGS_APPL";
    }

    public static String LegalCheckBoxId() {
        return "ctl00_cntMainBody_SD__LEGAL_COSTS_APPL";
    }

    public static String HomeAssistanceCheckBoxId() {
        return "ctl00_cntMainBody_SD__HOME_ASSISTANCE_APPL";
    }

    public static String BuildingsandContentsliabilityCheckBoxId() {
        return "ctl00_cntMainBody_PREMISES__LIAB_APPL";
    }

    public static String BuildingsandContentsLegalCheckBoxId() {
        return "ctl00_cntMainBody_PREMISES__LEGAL_APPL";
    }

    public static String AddClaimPrepCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_227";
    }

    public static String CLExtenionssaAddClaimPrepCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_229";
    }

    public static String ProfessionalFeesCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_226";
    }

    public static String CLProfessionalFeesCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_228";
    }

    public static String AdditionalExtensionsCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_OTHEREXT";
    }

    public static String GrossProfitBasisCheckBoxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__APPL_GPB";
    }

    public static String IncreasedCostofWorkingCheckBoxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__APPL_ICOW";
    }

    public static String AdditionalIncreasedCostofWorkingCheckBoxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__APPL_AICOW";
    }

    public static String ConsequetialLossCheckBoxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__APPL_CL";
    }

    public static String DeteriorationofStockCheckBoxId() {
        return "ctl00_cntMainBody_MACH_ITEMS__APPL_SHOW_DOS";
    }

    public static String ExtensionsCarHireCheckBoxId() {
        if (CurrentEnvironment == QA) {
            return "ctl00_cntMainBody_CHECK_7";
        } else {
            return "ctl00_cntMainBody_CHECK_11";
        }
    }

    public static String ExtensionsEBPMotorChangesCarHireCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_7";
    }

    public static String ExtensionsContingentLiabilityCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_3";
    }

    public static String ExtensionsCreditShortallCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_122";
    }

    public static String ExtensionsExcessWaiverCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_10";
    }

    public static String ExtensionsExcessWaiverTextBoxId() {
        return "ctl00_cntMainBody_PREMIUM_10";
    }

    public static String ExtensionsLossofKeysCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_5";
    }

    public static String ExtensionsParkingFacilitiesCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_12";
    }

    public static String ExtensionsRiotandStrikeCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_11";
    }

    public static String ExtensionsRoadsideAssistCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_17";
    }

    public static String ExtensionsTheftofCarRadioCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_6";
    }

    public static String ExtensionsWreckageRemovalCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_13";
    }

    public static String ExtensionsCreditShortFallCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_122";
    }

    public static String ExtensionsWindscreenCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_21";
    }

    public static String EndorsementsCheckBoxId() {
        return "ctl00_cntMainBody_MS__ENDORSE_APPL";
    }

    public static String InterestedPartiesCheckBoxId() {
        return "ctl00_cntMainBody_MS__INT_PARTIES_APPL";
    }

    public static String CreditShortfallCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_122";
    }

    public static String RadioTheftCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_6";
    }

    public static String SubB1CheckBoxId() {
        return "ctl00_cntMainBody_MS__FAP_3RD_PARTY_APPL";
    }

    public static String SubB2CheckBoxId() {
        return "ctl00_cntMainBody_MS__FAP_FI_APPL";
    }

    public static String SumInsuredPermanentDisabilityCheckBoxId() {
        return "ctl00_cntMainBody_INDIVIDUALS__PD_APPL";
    }

    public static String MotorSectionClaim1DropdownListId() {
        return "ctl00_cntMainBody_MAIN_DRIVERS__LICENSE_TYPE";
    }

    public static String MotorSectionClaim2DropdownListId() {
        return "ctl00_cntMainBody_MAIN_DRIVERS__LICENSE_TYPE";
    }

    public static String MotorSectionClaim3DropdownListId() {
        return "ctl00_cntMainBody_MAIN_DRIVERS__LICENSE_TYPE";
    }
    // </editor-fold>

// <editor-fold defaultstate="collapsed" desc="CheckBoxes">
    public static String CheckRoadsideCheckBoxId() {
        return "ctl00_cntMainBody_MSD__ROAD_ASSIST_APPL";
    }

    public static String CheckContentsCheckBoxId() {
        return "ctl00_cntMainBody_PREMISES__CONT_APPL";
    }

    public static String CheckAllRiskCheckBoxId() {
        return "ctl00_cntMainBody_PREMISES__ARISKS_APPL";
    }

    public static String LightningConductorCheckBoxId() {
        return "ctl00_cntMainBody_PREMISES__SADC_LIGHTNING_CONDUCTOR";
    }

    public static String ExtensionsBuildingMaterialCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_118";
    }

    public static String CheckMechanicalAndElectricalBreakdownCheckBoxId() {
        return "ctl00_cntMainBody_PREMISES__SADC_MECHANDELEC_APPL";
    }

    public static String SubsidenceAndLandslipCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_119";
    }

    public static String CheckPersonalComputersCheckBoxId() {
        return "ctl00_cntMainBody_PREMISES__SADC_PC_APPL";
    }

    public static String CheckAllRiskItemsCheckBoxId() {
        return "ctl00_cntMainBody_PREMISES__ARISKS_APPL";
    }

    public static String SecurityDiscountsLevel5AlarmCheckBoxId() {
        return "ctl00_cntMainBody_HOUSEHOLD__SAIDSA_ALARM_APPL";
    }

    public static String ContentsInventoryCheckBoxId() {
        return "ctl00_cntMainBody_CONTENT__SADC_INVENTORY_APPL";
    }

    public static String ContentsSecurityBuglarBarsCheckBoxId() {
        return "ctl00_cntMainBody_CONTENT__SADC_BURGLAR_BARS_APPL";
    }

    public static String ContentsSecurityElectricFenceCheckBoxId() {
        return "ctl00_cntMainBody_CONTENT__SADC_ELECTRIC_FENCE_APPL";
    }

    public static String ContentsExtentionsHomeIndustryCoverCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_121";
    }

    public static String ContentsSubsidenceandLandslipCoverCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_119";
    }

    public static String ContentsExtenstionsAccidentalDamageCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_120";
    }

    public static String ContentsSecurityGateCheckBoxId() {
        return "ctl00_cntMainBody_CONTENT__SADC_SECURITY_GATES_APPL";
    }

    public static String ContentsAccessControlCheckBoxId() {
        return "ctl00_cntMainBody_CONTENT__SADC_ACCESS_CONTROL_APPL";
    }

    public static String ContentsSecureComplexCheckBoxId() {
        return "ctl00_cntMainBody_CONTENT__SADC_SECURITY_COMPLEX_APPL";
    }

    public static String SecurityDiscountsSecurityDoorsCheckBoxId() {
        return "ctl00_cntMainBody_HOUSEHOLD__SEC_DOORS_APPL";
    }

    public static String PCExtensionsEnsureCompatibilityCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_122";
    }

    public static String PCExtensionsReinstatementofDataCheckBoxId() {
        return "ctl00_cntMainBody_CHECK_123";
    }

    public static String ExtensionsWaterSkierDoorsCheckBoxId() {
        return "ctl00_cntMainBody_EXTENSIONS__EXT_WATLIB_APPL";
    }

    public static String DroppingOffMotorCheckBoxId() {
        return "ctl00_cntMainBody_EXTENSIONS__EXT_DROPOM_APPL";
    }

    public static String SubmergeObjectsCheckBoxId() {
        return "ctl00_cntMainBody_EXTENSIONS__EXT_SUBOB_APPL";
    }

    // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Tabs">
    public static String OverviewLinkText() {
        return "Overview";
    }

    public static String ReinsuranceDetailsTabLinkText() {
        return "Reinsurance Details";
    }
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Select">
    public static String SelectClauseDropdownListId() {
        return "ctl00_cntMainBody_RISK_ITEM__ENDORSEMENTS_PckTemplates_AvailList";
    }

    // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Cells">
    public static String SuburbSearchResultsSelectedCellId() {
        return "1";
    }

    public static String VehicleModelSelectedRowId() {
        return "1";
    }

    public static String RiskAddressSelectedRowId() {
        return "1";
    }

    public static String VehicleModelSelectedRowId3() {
        return "3";
    }

    public static String VehicleModelSelectedRowId2() {
        return "2";
    }

    public static String TrailorVehicleSelectedRowId() {
        return "4";
    }
    // </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Tables">
    public static String VehicleModelSearchResultsTableId() {
        return "popupGrid";
    }

    // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Spans">
    public static String BulletListLinkXpath() {
        return "//*[@id=\\\"ctl00_cntMainBody_txtDocumentEditor_toolbar1\\\"]/tbody/tr/td[13]/a";
    }

    public static String SearchVehicleModelIconId() {
        return "search";
    }

    public static String VehiclesSourceDropdownListId() {
        return "source";
    }

    public static String PolicyCreationConfirmationSpanId() {
        return "ctl00_cntMainBody_lblTransactionSubHeading";
    }
    // </editor-fold>

}
