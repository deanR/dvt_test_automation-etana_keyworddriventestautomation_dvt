
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class EkunduPersonalAccidentRiskPage
  {
    public static String SelectPersonalAccidentRiskLinkId()
      {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl04_lnkbtnSelect";
      }

    public static String RiskNextButtonId()
      {
        return "ctl00_cntMainBody_btnNext";
      }

    public static String RiskRateButtonId()
      {
        return "ctl00_cntMainBody_btnRate";
      }

    public static String RiskItemsReinsuranceLimitofLiabilityTextboxId()
      {
        return "ctl00_cntMainBody_PA__RE_INS_LIMIT_LIAB";
      }

    public static String RiskItemsAddButtonXpath()
      {
        return "//div[@id = \"ctl00_cntMainBody_PA__ITEM\"]/table/tfoot/tr/td[7]/input[@value = \"Add\"]";
      }

    public static String EmployeeDetailsTotalLimitofLiabilityTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__TOTAL_SI";
      }

    public static String EmployeeDetailsNumberofEmployeesTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__NUM_EMPLOYEES";
      }

    public static String EmployeeDetailsCoverPeriodDropdownListId()
      {
        return "ctl00_cntMainBody_ITEM__COVER_PERIOD";
      }

    public static String CoverDetailsDeathCompensationTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__DEATH_COMP";
      }

    public static String CoverDetailsDeathAgreedRateTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__DEATH_YRS_AGREED_RATE";
      }

    public static String CoverDetailsPermanentDisabilityCheckboxId()
      {
        return "ctl00_cntMainBody_ITEM__PERM_DISABILITY_APPL";
      }

    public static String CoverDetailsPermanentDisabilitRateTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__DISABILITY_YRS_AGREED_RATE";
      }

    public static String CoverDetailsTDDCheckboxId()
      {
        return "ctl00_cntMainBody_ITEM__TTD_APPL";
      }

    public static String CoverDetailsTDDCompensationTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__TTD_COMP";
      }

    public static String CoverDetailsTDDNumberofWeeksDropdownListId()
      {
        return "ctl00_cntMainBody_ITEM__TTD_NUM_WEEKS";
      }

    public static String CoverDetailsTDDAgreedRateTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__TTD_WEEKS_AGREED_RATE";
      }

    public static String CoverDetailsMedicalExpensesCheckboxId()
      {
        return "ctl00_cntMainBody_ITEM__MED_EXPENSES_APPL";
      }

    public static String CoverDetailsMedicalExpensesSumInsuredDropdownListsId()
      {
        return "ctl00_cntMainBody_ITEM__MED_LIMIT";
      }

    public static String CoverDetailsMedicalExpensesRateTextboxId()
      {
        return "ctl00_cntMainBody_ITEM__MED_LIMIT_AGREED_RATE";
      }

    public static String ExtensionsBodyTransportationCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_61";
      }

    public static String ExtensionsBodyTransportationRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_61";
      }

    public static String ExtensionsBodyTransportationPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_61";
      }

    public static String ExtensionsBodyTransportationFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_61";
      }

    public static String ExtensionsBodyTransportationFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_61";
      }

    public static String ExtensionsFuneralCostsCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_60";
      }

    public static String ExtensionsFuneralCostsRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_60";
      }

    public static String ExtensionsFuneralCostsPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_60";
      }

    public static String ExtensionsFuneralCostsFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_60";
      }

    public static String ExtensionsFuneralCostsFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_60";
      }

    public static String ExtensionsRepatriationCostsCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_62";
      }

    public static String ExtensionsRepatriationCostsRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_62";
      }

    public static String ExtensionsRepatriationCostsPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_62";
      }

    public static String ExtensionsRepatriationCostsFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_62";
      }

    public static String ExtensionsRepatriationCostsFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_62";
      }

    public static String ExtensionsSeriousIllnessCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_58";
      }

    public static String ExtensionsSeriousIllnessRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_58";
      }

    public static String ExtensionsSeriousIllnessPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_58";
      }

    public static String ExtensionsSeriousIllnessFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_58";
      }

    public static String ExtensionsSeriousIllnessFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_58";
      }

    public static String ExtensionsSicknessCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_59";
      }

    public static String ExtensionsSicknessRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_59";
      }

    public static String ExtensionsSicknessFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_59";
      }

    public static String ExtensionsSicknessFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_59";
      }

    public static String ExtensionsSicknessLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_59";
      }

    public static String RiskCommentsTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
      }

    public static String RiskPolicyNotesTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
      }

    public static String RiskAddNotesLabelId()
      {
        return "ctl00_cntMainBody_cntrlNotes_lbl_CONTROL__ADD_NOTES";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
