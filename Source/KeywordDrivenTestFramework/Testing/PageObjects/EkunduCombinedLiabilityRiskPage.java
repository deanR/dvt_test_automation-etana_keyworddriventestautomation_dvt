
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class EkunduCombinedLiabilityRiskPage
  {
    public static String SelectCombinedLiabilityRiskLinkId()
      {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl06_lnkbtnSelect";
      }

    public static String RiskProductLiabilityCheckboxId()
      {
        return "ctl00_cntMainBody_CL__PRODUCTS_APCL";
      }

    public static String RiskQuestionnaireReceivedCheckboxId()
      {
        return "ctl00_cntMainBody_CL__QUESTION_RECVD";
      }

    public static String RiskFAPSectionCheckboxId()
      {
        return "ctl00_cntMainBody_CL__GENERAL_FAP_APCL";
      }

    public static String PublicLiabilityFAPSectionCheckboxId()
      {
        return "ctl00_cntMainBody_CL__DW_FAP_APPL";
      }

    public static String RiskBasisofCoverDropdownListId()
      {
        return "ctl00_cntMainBody_CL__BOC";
      }

    public static String RiskBasisofCoverRetroActiveDateTextboxId()
      {
        return "ctl00_cntMainBody_CL__RETRO_DATE";
      }

    public static String RiskTurnoverTextboxId()
      {
        return "ctl00_cntMainBody_CL__DW_TURNOVER_WAGES";
      }

    public static String RiskLoadingsRetroActivOccurrenceTextboxId()
      {
        return "ctl00_cntMainBody_CL__RETRO_OCCURRENCE";
      }

    public static String RiskLoadingsRevenueTextboxId()
      {
        return "ctl00_cntMainBody_CL__REVENUE";
      }

    public static String RiskLoadingsDeductibleTextboxId()
      {
        return "ctl00_cntMainBody_CL__DEDUCTIBLE";
      }

    public static String RiskLoadingsClaimsExperienceTextboxId()
      {
        return "ctl00_cntMainBody_CL__CLAIMS_EXPER";
      }

    public static String RiskLoadingsUWDiscretionTextboxId()
      {
        return "ctl00_cntMainBody_CL__UW_DISCRETION";
      }

    public static String RiskNextButtonId()
      {
        return "ctl00_cntMainBody_btnNext";
      }

    public static String RiskRateButtonId()
      {
        return "ctl00_cntMainBody_btnRate";
      }

    public static String PublicLiabilityLimitofIndemnityDropdownListId()
      {
        return "ctl00_cntMainBody_CL__DW_MAX_LOI";
      }

    public static String ProductsLiabilityLimitofIndemnityDropdownListId()
      {
        return "ctl00_cntMainBody_CL__PRODUCTS_MAX_LOI";
      }

    public static String ExtensionsAdvertisingLiabilityCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_131";
      }

    public static String ExtensionsAdvertisingLiabilityLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_131";
      }

    public static String ExtensionsAdvertisingLiabilityRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_131";
      }

    public static String ExtensionsAdvertisingLiabilityFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_131";
      }

    public static String ExtensionsAdvertisingLiabilityFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_131";
      }

    public static String ExtensionsClaimsPreparationCostsCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_133";
      }

    public static String ExtensionsClaimsPreparationCostsLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_133";
      }

    public static String ExtensionsClaimsPreparationCostsRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_133";
      }

    public static String ExtensionsClaimsPreparationCostsFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_133";
      }

    public static String ExtensionsClaimsPreparationCostsFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_133";
      }

    public static String ExtensionsContractorLiabilityCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_132";
      }

    public static String ExtensionsLegalDefenceCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_46";
      }

    public static String ExtensionsLegalDefensePremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_46";
      }

    public static String ExtensionsLegalDefenseRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_46";
      }

    public static String ExtensionsWrongfulArrestCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_47";
      }

    public static String ExtensionsWrongfulArrestPremiumTextboxId()
      {
        return "ctl00_cntMainBody_PREMIUM_47";
      }

    public static String ExtensionsWrongfulArrestRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_47";
      }

    public static String RatingReinsuranceLimitofLiabilitysTextboxId()
      {
        return "ctl00_cntMainBody_CL__RI_LOL";
      }

    public static String EndorsementsAddAllButtonId()
      {
        return "ctl00_cntMainBody_CL__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
      }

    public static String EndorsementsApplyButtonId()
      {
        return "ctl00_cntMainBody_CL__ENDORSEMENTS_btnApplySelection";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
