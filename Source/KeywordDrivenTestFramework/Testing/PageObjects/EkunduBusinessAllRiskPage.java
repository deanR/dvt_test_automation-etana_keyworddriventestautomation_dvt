
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import static KeywordDrivenTestFramework.Core.BaseClass.CurrentEnvironment;
import KeywordDrivenTestFramework.Entities.Enums;
import static KeywordDrivenTestFramework.Entities.Enums.TestEnvironments.NamQA;

/**
 *
 * @author FerdinandN
 */
public class EkunduBusinessAllRiskPage {

    public static String BusinessAllRiskIAddButtonLinkText() {
        return "Add";
    }

    public static String BusinessAllRiskItemAddItemButtonName4() {
        return "ctl00$cntMainBody$BAR__ITEM$ctl05$ctl00";
    }

    public static String BusinessAllRiskItemAddItemButtonName5() {
        return "ctl00$cntMainBody$BAR__ITEM$ctl06$ctl00";
    }

    public static String BusinessAllRiskItemAddItemButtonName6() {
        return "ctl00$cntMainBody$BAR__ITEM$ctl07$ctl00";
    }

    public static String BusinessAllRiskItemAddItemButtonName2() {
        return "ctl00$cntMainBody$BAR__ITEM$ctl04$ctl00";
    }

    public static String BusinessAllRiskItemAddItemButtonName() {
        return "ctl00$cntMainBody$BAR__ITEM$ctl03$ctl00";
    }

    public static String BusinessAllRiskIAddButtonName() {
        return "ctl00$cntMainBody$BAR__ITEM$ctl02$ctl00";
    }

    public static String SelectBusinessAllRiskLinkId() {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl05_lnkbtnSelect";
    }

    public static String editOfficeMachinesLinkXpath() {
        return "//div[@id = \"ctl00_cntMainBody_BAR__ITEM\"]/table/tbody/tr[2]/td[10]/a";
    }

    public static String deleteRadioEquipmentLinkXpath() {
        return "//div[@id = \"ctl00_cntMainBody_BAR__ITEM\"]/table/tbody/tr[4]/td[10]/a[2]";
    }

    public static String deleteFirstCellularLinkXpath() {
        return "//div[@id = \"ctl00_cntMainBody_BAR__ITEM\"]/table/tbody/tr/td[10]/a[2]";
    }

    public static String DeleteBusinessAllRiskLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl05_lnkbtnDelete";
    }

    public static String SelectEHATBusinessAllRiskLinkId() {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl03_lnkbtnSelect";
    }

    public static String RiskItemsDeleteItemLinkText() {
        return "Delete";
    }

    public static String AddRiskItemButtonXpath() {
        if (CurrentEnvironment == Enums.TestEnvironments.QA) {
            return "//div[@id = \"ctl00_cntMainBody_BAR__ITEM\"]/table/tfoot/tr/td[10]/input[@value = \"Add\"]";

        } else {
            return "//div[@id = \"ctl00_cntMainBody_BAR__ITEM\"]/table/tfoot/tr/td[7]/input[@value = \"Add\"]";

        }
    }
    
    public static String AddRiskItem1ButtonName() {
        
            return "ctl00$cntMainBody$BAR__ITEM$ctl02$ctl00";
    }
    
    public static String AddRiskItem2ButtonName() {
        
            return "ctl00$cntMainBody$BAR__ITEM$ctl03$ctl00";
    }
    
    public static String AddRiskItem3ButtonName() {
        
            return "ctl00$cntMainBody$BAR__ITEM$ctl04$ctl00";
    }
    
    public static String AddRiskItem4ButtonName() {
        
            return "ctl00$cntMainBody$BAR__ITEM$ctl05$ctl00";
    }
    
    public static String AddRiskItem5ButtonName() {
        
            return "ctl00$cntMainBody$BAR__ITEM$ctl06$ctl00";
    }
    
    public static String AddRiskItem6ButtonName() {
        
            return "ctl00$cntMainBody$BAR__ITEM$ctl07$ctl00";
    }
    
    public static String AddRiskItem7ButtonName() {
        
            return "ctl00$cntMainBody$BAR__ITEM$ctl08$ctl00";
    }

    public static String AddSecondRiskItemButtonXpath() {
        return "//div[@id = \"ctl00_cntMainBody_BAR__ITEM\"]/table/tfoot/tr/td[7]/input[@value = \"Add\"]";
    }

    public static String EditBusinessAllRiskLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl05_lnkbtnEdit";
    }

    public static String MTAEditBusinessAllRiskLinkId() {
        return "ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl03_lnkbtnEdit";
    }

    public static String EditSamsungRiskItemLinkXpath() {
        //*[@id="ctl00_cntMainBody_BAR__ITEM"]/table/tbody/tr[5]/td[10]/a[1]
        return "//div[@id = \"ctl00_cntMainBody_BAR__ITEM\"]/table/tbody/tr[5]/td[10]/a";
    }

    public static String RiskItemFlatPremiumTextboxId() {
        return "ctl00_cntMainBody_ITEM__FLAT_PREM_AMT";
    }

    public static String RiskItemFlatPremiumCheckboxId() {
        return "ctl00_cntMainBody_ITEM__FLAT_PREM_APPL";
    }

    public static String RiskItemsAlarmWarrantyDropdownListId() {
        return "ctl00_cntMainBody_BAR__ALARM_WARRANTY_APPL";
    }

    public static String RiskItemsExtensionsAdditionalClaimsCheckboxId() {
        return "ctl00_cntMainBody_CHECK_25";
    }

    public static String BARItemExtensionsReplacementCheckboxId() {
        return "ctl00_cntMainBody_ITEM__EXT_REINST_VALUE_COND_APPL";
    }

    public static String BARItemExtensionsIncreasedCostCheckboxId() {
        return "ctl00_cntMainBody_ITEM__EXT_ICOW_APPL";
    }

    public static String BARItemExtensionsFAPCheckboxId() {
        return "ctl00_cntMainBody_ITEM__FAP_APPL";
    }

    public static String RiskItemsExtensionsAdditionalClaimsLimitofIndemnityTextboxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_25";
    }

    public static String RiskItemsExtensionsAdditionalClaimsRateTextboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_25";
    }

    public static String RiskItemsExtensionsRiotandStrikeCheckboxId() {
        return "ctl00_cntMainBody_CHECK_11";
    }

    public static String RiskItemsAddRiskItemsButtonName() {
        return "ctl00$cntMainBody$BAR__ITEM$ctl02$ctl00";
    }

    public static String BusinessAllRiskItemCategoryDropdownListId() {
        return "ctl00_cntMainBody_ITEM__CATEGORY";
    }

    public static String BusinessAllRiskItemSumInsuredTextboxId() {
        return "ctl00_cntMainBody_ITEM__SI";
    }

    public static String BusinessAllRiskItemDescriptionTextboxId() {
        return "ctl00_cntMainBody_ITEM__DESCRIPTION";
    }

    public static String BusinessAllRiskItemRateTextboxId() {
        return "ctl00_cntMainBody_ITEM__AGREED_RATE";
    }

    public static String BusinessAllRiskItemExtensionsFAPMinPercTextboxId() {
        return "ctl00_cntMainBody_ITEM__FAP_MIN_PERC";
    }

    public static String BusinessAllRiskItemExtensionsFAPMinAmountTextboxId() {
        return "ctl00_cntMainBody_ITEM__FAP_MIN_AMT";
    }

    public static String BusinessAllRiskItemFAPMaxAmountTextboxId() {
        return "ctl00_cntMainBody_ITEM__FAP_MAX_AMT";
    }

    public static String BusinessAllRiskExtensionsReplaceValueConditionsCheckboxId() {
        return "ctl00_cntMainBody_ITEM__EXT_REINST_VALUE_COND_APPL";
    }

    public static String BusinessAllRiskExtensionsIncreasedCostofWorkingCheckboxId() {
        return "ctl00_cntMainBody_ITEM__EXT_ICOW_APPL";
    }

    public static String BusinessAllRiskRateButtonId() {
        return "ctl00_cntMainBody_btnRate";
    }

    public static String BusinessAllRiskNextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String BusinessAllRiskItemNotesTextboxId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }

    public static String BusinessAllRiskItemPolicyNotesCheckboxId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__ADD_NOTES";
    }

    public static String BusinessAllRiskItemPolicyNotesTextboxId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
    }

    public static String EndorsementsSelectButtonId() {
        return "_btnShowSelect";
    }

    public static String EndorsementsAddAllButtonId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_PC__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
        } else {
            return "ctl00_cntMainBody_BAR__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
        }
    }

    public static String EndorsementsBuildingsAddAllButtonId() {
        return "ctl00_cntMainBody_S_ENDORSEMENT__ENDORSEMENT_PckTemplates_RemoveAllCmd";
    }

    public static String MotorSectionEndorsementsAddAllButtonId() {
        return "ctl00_cntMainBody_MSD__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
    }

    public static String DAPLEndorsementsAddAllButtonId() {
        return "ctl00_cntMainBody_S_ENDORSEMENT__ENDORSEMENT_PckTemplates_RemoveAllCmd";
    }

    public static String EHATEndorsementsAddAllButtonId() {
        return "ctl00_cntMainBody_BI__BI_ENDORSEMENTS_PckTemplates_RemoveAllCmd";
    }

    public static String EHATPCEndorsementsAddAllButtonId() {
        return "ctl00_cntMainBody_PC__PC_ENDORSEMENTS_PckTemplates_RemoveAllCmd";
    }

    public static String EHATAREndorsementsAddAllButtonId() {
        return "ctl00_cntMainBody_AR__AR_ENDORSEMENTS_PckTemplates_RemoveAllCmd";
    }

    public static String EHATOCEndorsementsAddAllButtonId() {
        return "ctl00_cntMainBody_OC__OC_ENDORSEMENTS_PckTemplates_RemoveAllCmd";
    }

    public static String EHATGIEndorsementsAddAllButtonId() {
        return "ctl00_cntMainBody_GI__GI_ENDORSEMENTS_PckTemplates_RemoveAllCmd";
    }

    public static String EHATGIEndorsementsApplyButtonId() {
        return "ctl00_cntMainBody_GI__GI_ENDORSEMENTS_btnApplySelection";
    }

    public static String EHATADEndorsementsAddAllButtonId() {
        return "ctl00_cntMainBody_AD__AD_ENDORSEMENTS_PckTemplates_RemoveAllCmd";
    }

    public static String EHATEndorsementsApplyButtonId() {
        return "ctl00_cntMainBody_BI__BI_ENDORSEMENTS_btnApplySelection";
    }

    public static String EHATPCEndorsementsApplyButtonId() {
        return "ctl00_cntMainBody_PC__PC_ENDORSEMENTS_btnApplySelection";
    }

    public static String MotorSectionEndorsementsApplyButtonId() {
        return "ctl00_cntMainBody_MSD__ENDORSEMENTS_btnApplySelection";
    }

    public static String EHATDAPEndorsementsApplyButtonId() {
        return "ctl00_cntMainBody_S_ENDORSEMENT__ENDORSEMENT_btnApplySelection";
    }

    public static String EHATAREndorsementsApplyButtonId() {
        return "ctl00_cntMainBody_AR__AR_ENDORSEMENTS_btnApplySelection";
    }

    public static String EHATADEndorsementsApplyButtonId() {
        return "ctl00_cntMainBody_AD__AD_ENDORSEMENTS_btnApplySelection";
    }

    public static String EHATOCEndorsementsApplyButtonId() {
        return "ctl00_cntMainBody_OC__OC_ENDORSEMENTS_btnApplySelection";
    }

    public static String BuildingsAndContentsEndorsementsApplyButtonId() {
        return "ctl00_cntMainBody_S_ENDORSEMENT__ENDORSEMENT_btnApplySelection";
    }

    public static String EndorsementsApplyButtonId() {
        if (CurrentEnvironment == NamQA) {
            return "ctl00_cntMainBody_PC__ENDORSEMENTS_btnApplySelection";
        } else {
            return "ctl00_cntMainBody_BAR__ENDORSEMENTS_btnApplySelection";

        }
    }

    public static String BusinessAllRiskCommentsTextboxId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }

    public static String BusinessAllRiskPolicyNotesCheckboxId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__ADD_NOTES";
    }

    public static String BusinessAllRiskPolicyNotesTextboxId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
