
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class EkunduEmployersLiabilityRiskPage
  {
    public static String SelectEmployersLiabilityRiskLinkId()
      {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl08_lnkbtnSelect";
      }

    public static String RiskDetailsClassDropdownListId()
      {
        return "ctl00_cntMainBody_EL__CATEGORIES";
      }

    public static String RiskDetailsLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_EL__LOI_AMT";
      }

    public static String RiskDetailsAgreedRateTextboxId()
      {
        return "ctl00_cntMainBody_EL__AGREED_RATE";
      }

    public static String RiskDetailsRateButtonId()
      {
        return "ctl00_cntMainBody_btnRate";
      }

    public static String RiskDetailsAnnualEarningsTextboxId()
      {
        return "ctl00_cntMainBody_EL__ANN_EARNINGS";
      }

    public static String RiskDetailsEmployersLiabilityNextButtonId()
      {
        return "ctl00_cntMainBody_btnNext";
      }

    public static String RiskCommentsTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
      }

    public static String RiskNotesTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
      }

    public static String RiskAddNotesLabelId()
      {
        return "ctl00_cntMainBody_cntrlNotes_lbl_CONTROL__ADD_NOTES";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
