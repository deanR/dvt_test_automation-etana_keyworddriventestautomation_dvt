
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author deanR
 */
public class EkunduCrisis24Page
  {
    public static String SelectCrisis24RiskLinkId()
      {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl04_lnkbtnSelect";
      }

    public static String AssistancecheckboxId()
      {
        return "ctl00_cntMainBody_CRISIS__ASSIST_SERV_APPL";
      }

    public static String InterventioncheckboxId()
      {
        return "ctl00_cntMainBody_CRISIS__INTERV_SERV_APPL";
      }

    public static String AssistancecLimitOfIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_CRISIS__ASSIST_SERV_LOI";
      }

    public static String AssistancecRateTextboxId()
      {
        return "ctl00_cntMainBody_CRISIS__ASSIST_SERV_RATE";
      }

    public static String InterventionLimitOfIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_CRISIS__INTERV_SERV_LOI";
      }

    public static String InterventionRateTextboxId()
      {
        return "ctl00_cntMainBody_CRISIS__INTERV_SERV_RATE";
      }

    public static String EndorsementsSelectButtonId()
      {
        return "_btnShowSelect";
      }

    public static String EndorsementsAddAllButtonId()
      {
        return "ctl00_cntMainBody_CRISIS__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
      }

    public static String EndorsementsApplyButtonId()
      {
        return "ctl00_cntMainBody_CRISIS__ENDORSEMENTS_btnApplySelection";
      }

    public static String Crisis24RiskNextButtonId()
      {
        return "ctl00_cntMainBody_btnNext";
      }

    public static String Crisis24RiskItemCommentsTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
      }

    public static String Crisis24RiskItemPolicyNotesCheckboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__ADD_NOTES";
      }

    public static String Crisis24RiskItemNotesTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
      }

    public static String BusinessAllRiskAddButtonName()
      {
        return "ctl00$cntMainBody$BAR__ITEMS$ctl02$ctl00";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
