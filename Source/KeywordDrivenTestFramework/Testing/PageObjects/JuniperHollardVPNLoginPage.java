
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

/**
 *
 * @author fnell
 */
public class JuniperHollardVPNLoginPage
  {
    public static String PageUrl()
      {
        return ApplicationConfig.VPNPageUrl();
      }

    // <editor-fold defaultstate="collapsed" desc="TextBoxes">

    public static String UserNameTextBoxName()
      {
        return "username";
      }

    public static String PasswordTextBoxName()
      {
        return "password";
      }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Drop Down Lists">

    public static String RealmSelectionDropDownListName()
      {
        return "realm";
      }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Buttons">

    public static String SignInSubmitButtonName()
      {
        return "btnSubmit";
      }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Validation Items">
    public static String LoginFormName()
      {
        return "frmLogin";
      }

    // </editor-fold>
  }


//~ Formatted by Jindent --- http://www.jindent.com
