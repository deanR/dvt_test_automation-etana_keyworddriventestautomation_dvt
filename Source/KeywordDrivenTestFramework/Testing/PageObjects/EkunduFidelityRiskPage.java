
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class EkunduFidelityRiskPage
  {
    public static String RisksDialogNavigateToPage2LinkText()
      {
        return "2";
      }

    public static String SelectFedilityRiskTypeLinkId()
      {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl02_lnkbtnSelect";
      }

    public static String RiskItemBasisofCoverDropdownListId()
      {
        return "ctl00_cntMainBody_FID__BASIS_COVER";
      }

    public static String BlanketBasisNumberofEmployeesTextboxId()
      {         //ctl00_cntMainBody_FID__NUM_EMP
        return "ctl00_cntMainBody_FID__NUM_EMPL";
      }

    public static String BlanketBasisSumInsuredTextboxId()
      {
        return "ctl00_cntMainBody_FID__SI";
      }

    public static String RiskItemsProposalObtainedCheckboxId()
      {
        return "ctl00_cntMainBody_FID__PROPOSAL_OBTAINED";
      }

    public static String RiskItemsFAPCheckboxId()
      {
        return "ctl00_cntMainBody_FID__FAP_APPL";
      }

    public static String RiskItemsFAPMinimumPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FID__FAP_MIN_PERC";
      }

    public static String RiskItemsFAPMinimumAmountTextboxId()
      {
        return "ctl00_cntMainBody_FID__FAP_MIN_AMT";
      }

    public static String RiskItemsFAPMaximumAmountTextboxId()
      {
        return "ctl00_cntMainBody_FID__FAP_MAX_AMT";
      }

    public static String RiskItemsAdditionalFAPCheckboxId()
      {
        return "ctl00_cntMainBody_FID__FAP_ADDIT_APPL";
      }

    public static String RiskItemsAdditionalFAPMinimumPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FID__FAP_ADDIT_MIN_PERC";
      }

    public static String RiskItemsAdditionalFAPMinimumAmountTextboxId()
      {
        return "ctl00_cntMainBody_FID__FAP_ADDIT_MIN_AMT";
      }

    public static String RiskItemsAdditionalFAPMaximumAmountTextboxId()
      {
        return "ctl00_cntMainBody_FID__FAP_ADDIT_MAX_AMT";
      }

    public static String ExtensionsAdditionalClaimsPrepCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_49";
      }

    public static String ExtensionsAdditionalClaimsPrepLimitofIndemnityTextboxId()
      {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_49";
      }

    public static String ExtensionsAdditionalClaimsPrepRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_49";
      }

    public static String ExtensionsComputerExtensionLoadCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_54";
      }

    public static String ExtensionsComputerExtensionLoadRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_54";
      }

    public static String ExtensionsComputerExtensionLoadFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_54";
      }

    public static String ExtensionsComputerExtensionLoadFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_54";
      }

    public static String ExtensionsLossDiscoverGreaterThan24MonthsCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_50";
      }

    public static String ExtensionsLossDiscoverGreaterThan24MonthsRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_50";
      }

    public static String ExtensionsLossDiscoverGreaterThan24MonthsFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_50";
      }

    public static String ExtensionsLossDiscoverGreaterThan24MonthsFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_50";
      }

    public static String ExtensionsLossDiscover24To36MonthsCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_55";
      }

    public static String ExtensionsLossDiscover24To36MonthsRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_55";
      }

    public static String ExtensionsLossDiscover24To36MonthsFAPPercentageTextboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_55";
      }

    public static String ExtensionsLossDiscover24To36MonthsFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_55";
      }

    public static String ExtensionsRecoveryCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_53";
      }

    public static String ExtensionsRecoveryRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_53";
      }

    public static String ExtensionsRecoveryFAPPercentageTextxtboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_53";
      }

    public static String ExtensionsRecoveryFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_53";
      }

    public static String ExtensionsReductionorReinstatementCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_52";
      }

    public static String ExtensionsReductionorReinstatementRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_52";
      }

    public static String ExtensionsReductionorReinstatementFAPPercentageTextxtboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_52";
      }

    public static String ExtensionsReductionorReinstatementFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_52";
      }

    public static String ExtensionsRetroactiveCoverCheckboxId()
      {
        return "ctl00_cntMainBody_CHECK_51";
      }

    public static String ExtensionsRetroactiveCoverRateTextboxId()
      {
        return "ctl00_cntMainBody_AGREED_RATE_51";
      }

    public static String ExtensionsRetroactiveCoverFAPPercentageTextxtboxId()
      {
        return "ctl00_cntMainBody_FAP_PERC_51";
      }

    public static String ExtensionsRetroactiveCoverFAPAmountTextboxId()
      {
        return "ctl00_cntMainBody_FAP_AMOUNT_51";
      }

    public static String RiskCommentsTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
      }

    public static String AddNotesLabelId()
      {
        return "ctl00_cntMainBody_cntrlNotes_lbl_CONTROL__ADD_NOTES";
      }

    public static String RiskNotesTextboxId()
      {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
      }

    public static String FidelityRiskNextButtonId()
      {
        return "ctl00_cntMainBody_btnNext";
      }

    public static String FidelityRiskRateButtonId()
      {
        return "ctl00_cntMainBody_btnRate";
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
