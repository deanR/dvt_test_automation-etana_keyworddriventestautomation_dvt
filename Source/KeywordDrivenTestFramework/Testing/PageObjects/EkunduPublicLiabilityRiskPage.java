
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

/**
 *
 * @author FerdinandN
 */
public class EkunduPublicLiabilityRiskPage {

    public static String selectPublicLiabilityRiskLinkId() {
        return "ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType_ctl09_lnkbtnSelect";
    }

    public static String SumInsuredTextBoxId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance_ctl02_lblSumInsured";
    }

    public static String DefectiveWorkmanshipLimitOfIndeminityTextBoxId() {
        return "ctl00_cntMainBody_PL__DEF_WORK_PREM";
    }

    public static String RiskDefectiveWorkmanshipCheckboxId() {
        return "ctl00_cntMainBody_PL__DEF_WORK_APPL";
    }

    public static String RiskDefectiveWorkmanshipRateButtonId() {
        return "ctl00_cntMainBody_btnRate";
    }

    public static String RiskProductsCheckboxId() {
        return "ctl00_cntMainBody_PL__PRODUCTS_APPL";
    }

    public static String PublicLiabilityNextButtonId() {
        return "ctl00_cntMainBody_btnNext";
    }

    public static String GeneralTernantsLimitofIndemnityTextboxId() {
        return "ctl00_cntMainBody_PL__GT_LOI";
    }

    public static String PublicLiabilityFinishButtonId() {
        return "ctl00_cntMainBody_btnFinish";
    }

    public static String GeneralTernantsPremiumTextboxId() {
        return "ctl00_cntMainBody_PL__GT_PREM";
    }

    public static String PublicLiabilityRateButtonId() {
        return "ctl00_cntMainBody_btnRate";
    }

    public static String PublicLiabilityReinsuranceBandddlId2() {
        return "ctl00_cntMainBody_ctrl_RI2007_ddlReinsurance";
    }

    public static String PublicLiabilityReinsuranceBandddlId() {
        return "ctl00_cntMainBody_ReInsurance2007Cntrl_ddlReinsurance";
    }

    public static String DefectiveWorkmanshipLimitofIndemnityTextboxId() {
        return "ctl00_cntMainBody_PL__DEF_WORK_LOI";
    }

    public static String DefectiveWorkmanshipTurnoverTextboxId() {
        return "ctl00_cntMainBody_PL__DEF_WORK_WAGES";
    }

    public static String DefectiveWorkmanshipQuestionnaireCheckboxId() {
        return "ctl00_cntMainBody_PL__DEF_WORK_QUESTION";
    }

    public static String ProductsLiabilityTurnoverTextboxId() {
        return "ctl00_cntMainBody_PL__PRODUCTS_WAGES";
    }

    public static String ProductsLiabilityBasisDropdownListId() {
        return "ctl00_cntMainBody_PL__PRODUCTS_BASIS";
    }

    public static String ProductsLiabilityLimitofIndemnityTextboxId() {
        return "ctl00_cntMainBody_PL__PRODUCTS_LOI";
    }

    public static String ProductsLiabilityQuestionnairesReceivedCheckboxId() {
        return "ctl00_cntMainBody_PL__PRODUCTS_QUESTION";
    }

    public static String ProductsLiabilityFoodPoisoningOnlyCheckboxId() {
        return "ctl00_cntMainBody_PL__PRODUCTS_FOOD";
    }

    public static String ExtensionsLegalDefenceCheckboxId() {
        return "ctl00_cntMainBody_CHECK_46";
    }

    public static String ExtensionsLegalDefenceRateTextboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_46";
    }

    public static String ExtensionsLegalDefencePremiumTextboxId() {
        return "ctl00_cntMainBody_PREMIUM_46";
    }

    public static String ExtensionsSpreadofFireCheckboxId() {
        return "ctl00_cntMainBody_CHECK_48";
    }

    public static String ExtensionsSpreadofFireLimitofIndemnityTextboxId() {
        return "ctl00_cntMainBody_LIMIT_OF_INDEMNITY_48";
    }

    public static String ExtensionsSpreadofFireRateTextboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_48";
    }

    public static String ExtensionsSpreadofFAPPercentageTextboxId() {
        return "ctl00_cntMainBody_FAP_PERC_48";
    }

    public static String ExtensionsSpreadofFAPAmountTextboxId() {
        return "ctl00_cntMainBody_FAP_AMOUNT_48";
    }

    public static String ExtensionsWrongfulArrestCheckboxId() {
        return "ctl00_cntMainBody_CHECK_47";
    }

    public static String ExtensionsWrongfulArrestRateTextboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_47";
    }

    public static String ExtensionsWrongfulArrestPremiumTextboxId() {
        return "ctl00_cntMainBody_PREMIUM_47";
    }

    public static String EndorsementsSelectButtonId() {
        return "_btnShowSelect";
    }

    public static String EndorsementsAddAllButtonId() {
        return "ctl00_cntMainBody_PL__ENDORSEMENTS_PckTemplates_RemoveAllCmd";
    }

    public static String EndorsementsApplyButtonId() {
        return "ctl00_cntMainBody_PL__ENDORSEMENTS_btnApplySelection";
    }

    public static String ExtensionsWrongfulArrestRateTextboxboxId() {
        return "ctl00_cntMainBody_AGREED_RATE_47";
    }

    public static String RiskCommentsTextboxId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__COMMENTS";
    }

    public static String RiskNotesTextboxId() {
        return "ctl00_cntMainBody_cntrlNotes_CONTROL__NOTES";
    }

    public static String AddPolicyNotesLabelId() {
        return "ctl00_cntMainBody_cntrlNotes_lbl_CONTROL__ADD_NOTES";
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
