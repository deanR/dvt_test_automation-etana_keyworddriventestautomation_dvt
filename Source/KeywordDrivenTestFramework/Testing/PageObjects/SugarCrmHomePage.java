
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Utilities.ApplicationConfig;

/**
 *
 * @author fnell
 */
public class SugarCrmHomePage
  {
    public static String PageUrl()
      {
        return "https://41.222.48.156/index.php?module=Home&action=index";
      }

    // <editor-fold defaultstate="collapsed" desc="Labels">
    public static String WelcomeMessageDiv()
      {
        return "welcome";
      }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="TextBoxes">
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Buttons">
    public static String AddDashletsButtonId()
      {
        return "add_dashlets";
      }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Tabs">
    public static String SalesTabId()
      {
        return "grouptab_0";
      }

    public static String AccountsTabId()
      {
        return "moduleTab_0_Accounts";
      }

    // </editor-fold>
  }


//~ Formatted by Jindent --- http://www.jindent.com
