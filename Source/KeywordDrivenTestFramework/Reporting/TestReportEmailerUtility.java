
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Reporting;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author fnell
 */
public class TestReportEmailerUtility extends BaseClass
  {
    ApplicationConfig appConfig = new ApplicationConfig();
    int testCount = 0;
    int failCount = 0;
    int passCount = 0;
    long totalSeconds = 0;
    long totalMinutes = 0;
    long totalHours = 0;
    List<TestResult> testResults;
    String[] emailRecipients;
    String emailSender;
    String emailSubject;
    String emailHost;
    long testDuration;
    StringBuilder HtmlEmailBody;
    TestResult testResult;

    public TestReportEmailerUtility(List<TestResult> _testResults)
      {
        this.testResults = _testResults;
        this.emailRecipients = ApplicationConfig.MailingList();
        this.emailSender = ApplicationConfig.EmailSender();
        this.emailHost = ApplicationConfig.EmailHost();
        HtmlEmailBody = new StringBuilder();

        this.CalculateTestStatistics();
      }

    public String GenerateTestReport()
      {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String dateTime = dateFormatter.format(calendar.getTime());
        ResolveEnvironmentName enviro = new ResolveEnvironmentName();

        HtmlEmailBody.append("<!doctype html>\n");
        HtmlEmailBody.append("<html lang='en'>\n");

        HtmlEmailBody.append("<head>\n");
        HtmlEmailBody.append("<meta charset='utf-8'>\n");
        HtmlEmailBody.append("<title style=\"font-family:verdana;\">Automation Test Report - " + scenarioName
                             + "</title>\n");
        HtmlEmailBody.append("</head>\n\n");

        HtmlEmailBody.append("<body>\n");

        HtmlEmailBody.append("<h1 style=\"font-family:verdana;\">Automation Test Report - " + scenarioName + " On - "
                             + enviro.EnvironmentName + " Environment" + "</h1>\n");
        HtmlEmailBody.append("<h3 style=\"font-family:verdana;\">Report date - " + dateTime + "</h2>\n");

        // HtmlEmailBody.append( "<h3>Test statistics</h3>\n" );

        HtmlEmailBody.append("<table cellpadding=\"2\" cellspacing=\"0\" width=\"100%\" style=\" border-collapse:collapse; font-family:verdana; border:1px solid black\">\n");

        HtmlEmailBody.append("<tr>\n");
        HtmlEmailBody.append("<td colspan=\"4\" style=\"background-color:#FFA500; font-size: 15pt\">Test Statistics</td>\n");
        HtmlEmailBody.append("</tr>\n");

        HtmlEmailBody.append("<tr style=\"outline: thin solid black;\">\n");
        HtmlEmailBody.append("<th style=\"border-left:1px solid black;background-color:#B2B2B2;color:#ffffff;\">Total Tests</th>\n");
        HtmlEmailBody.append("<th style=\"border-left:1px solid black;background-color:#B2B2B2;color:#ffffff;\">Total Passed</th>\n");
        HtmlEmailBody.append("<th style=\"border-left:1px solid black;background-color:#B2B2B2;color:#ffffff;\">Total Failed</th>\n");
        HtmlEmailBody.append("<th style=\"border-left:1px solid black;background-color:#B2B2B2;color:#ffffff;\">Total Runtime</th>\n");
        HtmlEmailBody.append("</tr>\n");

        HtmlEmailBody.append("<tr style=\"outline: thin solid black;\">\n");
        HtmlEmailBody.append("<td style=\"border-left:1px solid black;text-align:center;\">" + this.testCount
                             + "</td>\n");
        HtmlEmailBody.append("<td style=\"border-left:1px solid black;text-align:center;\">" + this.passCount
                             + "</td>\n");
        HtmlEmailBody.append("<td style=\"border-left:1px solid black;text-align:center;\">" + this.failCount
                             + "</td>\n");
        HtmlEmailBody.append("<td style=\"border-left:1px solid black;text-align:center;\">" + this.totalHours
                             + " hour(s), " + this.totalMinutes + " minute(s), " + this.totalSeconds
                             + " second(s)</td>\n");
        HtmlEmailBody.append("</tr>\n");

        HtmlEmailBody.append("</table>\n");

        HtmlEmailBody.append("<table cellpadding=\"2\" cellspacing=\"0\" width=\"100%\" style=\" border-collapse:collapse;font-family:verdana; border:1px solid black;\">\n");

        HtmlEmailBody.append("<tr>\n");
        HtmlEmailBody.append("<td colspan=\"5\" style=\"background-color:#FFA500; font-size: 15pt\">Results Summary</td>\n");
        HtmlEmailBody.append("</tr>\n");

        HtmlEmailBody.append("<tr style=\"outline: thin solid black;\">\n");
        HtmlEmailBody.append("<th style=\"border-left:1px solid black;background-color:#B2B2B2;color:#ffffff;\">Test Case ID</th>\n");
        HtmlEmailBody.append("<th style=\"border-left:1px solid black;background-color:#B2B2B2;color:#ffffff;\">Keyword</th>\n");
        HtmlEmailBody.append( "<th style=\"border-left:1px solid black;background-color:#B2B2B2;color:#ffffff;\">Test Description</th>\n");
        HtmlEmailBody.append("<th style=\"border-left:1px solid black;background-color:#B2B2B2;color:#ffffff;\">Pass \\ Fail</th>\n");
        HtmlEmailBody.append("<th style=\"border-left:1px solid black;background-color:#B2B2B2;color:#ffffff;\">Test Message</th>\n");
        HtmlEmailBody.append("<th style=\"border-left:1px solid black;background-color:#B2B2B2;color:#ffffff;\">Test Duration</th>\n");
        HtmlEmailBody.append("</tr>\n");

        for (TestResult testResult : testResults)
          {
            HtmlEmailBody.append("<tr style=\"outline: thin solid black;\">\n");

            if(testResult.testStatus == Enums.ResultStatus.PASS)
            {
                HtmlEmailBody.append( "<td  bgcolor=\"#B2FCA3\" style=\"border-left:1px solid black;\">" + testResult.testData.TestCaseId + "</td>\n");
                HtmlEmailBody.append( "<td style=\"border-left:1px solid black;\">" + testResult.testData.TestMethod + "</td>\n");
                HtmlEmailBody.append( "<td style=\"border-left:1px solid black;\">" + testResult.testData.TestDescription + "</td>\n");
                HtmlEmailBody.append( "<td style=\"border-left:1px solid black;\">Pass</td>\n");
            }
            else if(testResult.testStatus == Enums.ResultStatus.FAIL)
            {
                HtmlEmailBody.append( "<td  bgcolor=\"#FF9494\" style=\"border-left:1px solid black;\">" + testResult.testData.TestCaseId + "</td>\n");
                HtmlEmailBody.append( "<td style=\"border-left:1px solid black;\">" + testResult.testData.TestMethod + "</td>\n");
                HtmlEmailBody.append( "<td style=\"border-left:1px solid black;\">" + testResult.testData.TestDescription + "</td>\n");
                HtmlEmailBody.append( "<td style=\"border-left:1px solid black;\">Fail</td>\n");
            }
            
            else if(testResult.testStatus == Enums.ResultStatus.WARNING)
            {
                HtmlEmailBody.append( "<td  bgcolor=\"#FF9900\" style=\"border-left:1px solid black;\">" + testResult.testData.TestCaseId + "</td>\n");
                HtmlEmailBody.append( "<td style=\"border-left:1px solid black;\">" + testResult.testData.TestMethod + "</td>\n");
                HtmlEmailBody.append( "<td style=\"border-left:1px solid black;\">" + testResult.testData.TestDescription + "</td>\n");
                HtmlEmailBody.append( "<td style=\"border-left:1px solid black;\">Fail</td>\n");
            }

            HtmlEmailBody.append("<td style=\"border-left:1px solid black;\">" + testResult.errorMessage + "</td>\n");

            HtmlEmailBody.append("<td style=\"border-left:1px solid black;\">"
                                 + testResult.calculateFormattedTestTime() + "</td>\n");
            HtmlEmailBody.append("</tr>\n");
          }

        HtmlEmailBody.append("</table>\n");

        HtmlEmailBody.append("</body>\n\n");

        HtmlEmailBody.append("</html>\n");

        this.saveAsHTMLTestReport(scenarioName + ".html");

        return HtmlEmailBody.toString();

      }

    private void saveAsHTMLTestReport(String htmlReportFileName)
      {
        try
          {
            File reportFile = new File("HTMLTestReport\\" + htmlReportFileName);
            File evidenceReportFile = new File(this.reportDirectory + "\\" + htmlReportFileName);

            if (!reportFile.exists())
              {
                reportFile.createNewFile();
              }

            if (!evidenceReportFile.exists())
              {
                evidenceReportFile.createNewFile();
              }

            BufferedWriter writer = new BufferedWriter(new FileWriter(reportFile));

            writer.write(HtmlEmailBody.toString());

            reportFile = null;
            writer.close();

            BufferedWriter writerEvidence = new BufferedWriter(new FileWriter(evidenceReportFile));

            writerEvidence.write(HtmlEmailBody.toString());

            evidenceReportFile = null;
            writerEvidence.close();
          }
        catch (Exception e)
          {
            System.err.println("[Error] Failed to save HTML report to - " + htmlReportFileName + " - Error - "
                               + e.getMessage());
          }

      }

    public void SendResultsEmail()
      {
        try
          {
              
            MimeBodyPart messageBody = new MimeBodyPart();
            
            messageBody.setContent(this.GenerateTestReport(), "text/html");
              
              
            Properties properties = System.getProperties();

            properties.setProperty("mail.smtp.host", this.emailHost);
            properties.setProperty("mail.user", "ferdinandN");
            properties.setProperty("mail.password", "password");

            Session session = Session.getDefaultInstance(properties);

            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress(this.emailSender));

            for (String recipient : this.emailRecipients)
              {
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
              }

            message.setSubject("Etana Automation Test Report - " + scenarioName);

            MimeMultipart multipartMessage = new MimeMultipart();

            
            MimeBodyPart attachment = new MimeBodyPart();
            DataSource inputFile = new FileDataSource(inputFilePath);

            

            attachment.setDataHandler(new DataHandler(inputFile));
            attachment.setHeader("Content-ID", "<" + UUID.randomUUID().toString() + ">");
            attachment.setFileName(inputFile.getName());

            multipartMessage.addBodyPart(messageBody);
            multipartMessage.addBodyPart(attachment);

            message.setContent(multipartMessage);

            Transport.send(message);

            System.out.println("Report Email sent...");

          }
        catch (Exception e)
          {
            System.err.println("[Error] could not send results email - " + e.getMessage());
          }

      }

    private void CalculateTestStatistics()
      {
        for (TestResult result : testResults)
          {
            this.totalSeconds += result.testDuration;
            this.testCount++;

            if (result.testStatus == Enums.ResultStatus.PASS || result.testStatus == Enums.ResultStatus.WARNING)
              {
                this.passCount++;
              }
            else if (result.testStatus == Enums.ResultStatus.FAIL)
              {
                this.failCount++;
              }
          }

        if (totalSeconds > 60)
          {
            while (totalSeconds > 60)
              {
                totalMinutes += 1;
                totalSeconds -= 60;

              }
          }

        if (totalMinutes > 60)
          {
            while (totalMinutes > 60)
              {
                totalHours += 1;
                totalMinutes -= 60;
              }
          }
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
