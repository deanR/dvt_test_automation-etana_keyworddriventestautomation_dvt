
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */

package KeywordDrivenTestFramework.Reporting;

//~--- non-JDK imports --------------------------------------------------------


import KeywordDrivenTestFramework.Entities.Enums.TestEnvironments;
import KeywordDrivenTestFramework.Testing.PageObjects.EKunduHomePage;

/**
 *
 * @author deanR
 */
public class ResolveEnvironmentName
  {
    public TestEnvironments EnvironmentName = ResolveEnvironmentName();

    private TestEnvironments ResolveEnvironmentName()
      {
        String pageurl = EKunduHomePage.PageUrl();

        switch(pageurl)
        {
            case "http://etasupwb08/ekundu" :
                return TestEnvironments.Support;
                
            case "http://etaqawb04/ekundu":
                return TestEnvironments.QA;
                
            case "http://etauatwb02/ekundu":
                    return TestEnvironments.UAT;
                
            case "http://ETATSTWB06/eKundu":
                    return TestEnvironments.Combined;
                
            case "http://hsss4iqawb01/Centauri/(S(awqpru55uplrycfvtabepu45))/secure/workmanager.aspx":
                    return TestEnvironments.NamQA;
                    
            case "http://etafinwb01/ekundu":
                    return TestEnvironments.Finance;
              
            default :
                System.err.println("[ERROR] Failed to resolve testing Environment name for the following url - " + pageurl);
                    return null;
                
        }
        
       
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
