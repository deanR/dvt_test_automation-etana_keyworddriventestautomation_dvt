
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Reporting;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Utilities.ExcelReaderUtility;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import java.sql.Date;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.naming.spi.DirStateFactory.Result;

/**
 *
 * @author fnell
 */
public class ReportGenerator
  {
    long TotalSeconds = 0;
    long TotalMinutes = 0;
    long TotalHours = 0;
    boolean wereRowsShifted = false;
    public List<TestResult> testResults;
    Map<String, Integer> KeywordMapping;
    FileOutputStream outputStream;
    Workbook workbook;
    Sheet InputSheet, OutputSheet;
    Row reportRow;
    String reportDirectory;
    String dateTimeFolder;
    String _inputFilePath;
    ExcelReaderUtility excelReader;
    int currentRow, startColumn, totalTests, totalPasses, totalFails, shiftAmount;

    public ReportGenerator(String inputFilePath, String reportDirectory)
      {
        excelReader = new ExcelReaderUtility();
        _inputFilePath = inputFilePath;

        // Retrieve inputFile Workbook
        workbook = excelReader.getExcelWorkbook(_inputFilePath);

        this.reportDirectory = reportDirectory;
        this.testResults = new ArrayList<TestResult>();

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("(yyyy-MM-dd)(HH-mm-ss)");

        dateTimeFolder = dateFormatter.format(calendar.getTime());
      }

    public void addResult(TestResult testResult)
      {
        totalTests += 1;
        
        if (testResult.testStatus == Enums.ResultStatus.PASS)
          {
            totalPasses += 1;
          }
        else if (testResult.testStatus == Enums.ResultStatus.FAIL)
          {
            totalFails += 1;
          }
          this.testResults.add(testResult);
      }

    public boolean generateTestReport()
      {
        try
          {
            // Create a new sheet within the existing workbook
            this.CreateSheet();

            // Calculate total test time for the summary section
            this.CalculateTotalTestTime();

            // Write test summary data to report
            this.WriteSummarySection();

            // Define report structure; title, columns, etc...
            this.AddReportColumnHeadings();

            // Write results to the report sheet
            this.PrintResults();

            if (this.WriteTestReport())
              {
                return true;
              }
            else
              {
                return false;
              }
          }
        catch (Exception e)
          {
            System.out.println("Error generating report...see message and  stack trace below: " + e.getMessage());

            return false;
          }
      }

    private void CreateSheet()
      {
        try
        {
            String SheetName = "Results " + this.dateTimeFolder;
            this.OutputSheet = this.workbook.createSheet(SheetName);
            this.workbook.setSheetOrder(SheetName, 1);
        }
        catch(Exception ex)
        {
             System.out.println("Error creating new sheet...see message and  stack trace below: " + ex.getMessage());
        }
      }

    private void PrintResults()
      {
        currentRow = 7;

        for (TestResult result : testResults)
          {
            try
              {
                addReportRow(result, currentRow);
                currentRow++;
              }
            catch (Exception e)
              {
                System.err.println("Error printing result - " + e.getMessage());
              }
          }
      }

    private void addReportRow(TestResult result, int rowNumber)
      {
            reportRow = OutputSheet.getRow(rowNumber);
      
        if (reportRow == null)
          {
            reportRow = OutputSheet.createRow(rowNumber);
          }

        if (result.testStatus == Enums.ResultStatus.PASS)
          {
            AddCell(0, "Pass", false, false);
            AddCell(1, result.errorMessage, false, false);
            AddCell(2, result.calculateFormattedTestTime(), false, false);
            AddTestData(result.testData, rowNumber);
            SetResultAsPassed(rowNumber);
          }

        else if(result.testStatus == Enums.ResultStatus.FAIL)
          {
            AddCell(0, "Fail", false, false);
            AddCell(1, result.errorMessage, false, false);
            AddCell(2, result.calculateFormattedTestTime(), false, false);
            AddTestData(result.testData, rowNumber);
            SetResultAsFailed(rowNumber);
          }
        
        else if(result.testStatus == Enums.ResultStatus.WARNING)
          {
            AddCell(0, "Warning", false, false);
            AddCell(1, result.errorMessage, false, false);
            AddCell(2, result.calculateFormattedTestTime(), false, false);
            AddTestData(result.testData, rowNumber);
            SetResultAsWarning(rowNumber);
          }
      }

    private void AddReportColumnHeadings()
      {
        int startRow = 6;

        this.reportRow = OutputSheet.getRow(startRow);

        if (reportRow == null)
          {
            reportRow = OutputSheet.createRow(startRow);
          }

        AddCell(0, "Result", true, false);
        AddCell(1, "Message", true, false);
        AddCell(2, "Time Taken", true, false);
        AddCell(3, "Test Case Id", true, false);
        AddCell(4, "Keyword", true, false);
        AddCell(5, "Parameters", true, false);
      }

    private void AddSummarySectionColumnHeadings()
      {
        this.reportRow = OutputSheet.getRow(0);

        if (reportRow == null)
          {
            reportRow = OutputSheet.createRow(0);
          }

        AddCell(0, "Summary", true, true);
        AddCell(1, "Count", true, true);
        AddCell(2, "Percentage", true, true);

        this.reportRow = OutputSheet.getRow(1);

        if (reportRow == null)
          {
            reportRow = OutputSheet.createRow(1);
          }

        AddCell(0, "Total", true, true);

        this.reportRow = OutputSheet.getRow(2);

        if (reportRow == null)
          {
            reportRow = OutputSheet.createRow(2);
          }

        AddCell(0, "Pass", true, true);

        this.reportRow = OutputSheet.getRow(3);

        if (reportRow == null)
          {
            reportRow = OutputSheet.createRow(3);
          }

        AddCell(0, "Fail", true, true);

        this.reportRow = OutputSheet.getRow(4);

        if (reportRow == null)
          {
            reportRow = OutputSheet.createRow(4);
          }

        AddCell(0, "Run time (hh:mm:ss)", true, true);
      }

    private void AddSummaryData()
      {
        this.reportRow = OutputSheet.getRow(1);
        AddCell(1, String.valueOf(totalTests), false, true);
        AddCell(2, "", false, true);

        this.reportRow = OutputSheet.getRow(2);
        AddCell(1, String.valueOf(totalPasses), false, true);
        AddCell(2, String.valueOf(Math.round(totalPasses * 100.0 / totalTests) + "%"), false, true);

        this.reportRow = OutputSheet.getRow(3);
        AddCell(1, String.valueOf(totalFails), false, true);
        AddCell(2, String.valueOf(Math.round(totalFails * 100.0 / totalTests) + "%"), false, true);

        this.reportRow = OutputSheet.getRow(4);
        AddCell(1, TotalHours + ":" + TotalMinutes + ":" + TotalSeconds, false, true);
        AddCell(2, "", false, true);

      }

    private void AutoSizeColumnsBeforeSave()
      {
        for (int i = 2; i < 50; i++)
          {
            this.OutputSheet.autoSizeColumn(i);
          }
      }

    private void AddCell(int columnIndex, String cellText, boolean isColumnHeader, boolean isSummaryValue)
      {
        Cell cell = reportRow.getCell(columnIndex);

        if (cell == null)
          {
            cell = reportRow.createCell(columnIndex);
          }

        cell.setCellValue(cellText);

        CellStyle cellStyle = workbook.createCellStyle();

        if (isColumnHeader &&!isSummaryValue)
          {
            Font headerFont = workbook.createFont();

            headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
            cellStyle.setFont(headerFont);

            cellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

            cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
            cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
            cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            cellStyle.setBorderRight(CellStyle.BORDER_THIN);
            cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            cellStyle.setBorderTop(CellStyle.BORDER_THIN);
            cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
          }

        else if (isSummaryValue && isColumnHeader)
          {
            Font headerFont = workbook.createFont();

            headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
            cellStyle.setFont(headerFont);

            cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
            cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
            cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            cellStyle.setBorderRight(CellStyle.BORDER_THIN);
            cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            cellStyle.setBorderTop(CellStyle.BORDER_THIN);
            cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
          }

        else if (isSummaryValue &&!isColumnHeader)
          {
            cellStyle.setAlignment(CellStyle.ALIGN_RIGHT);
            cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
            cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
            cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            cellStyle.setBorderRight(CellStyle.BORDER_THIN);
            cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            cellStyle.setBorderTop(CellStyle.BORDER_THIN);
            cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
          }

        cell.setCellStyle(cellStyle);
      }

    private void AddTestData(TestEntity testData, int rowNumber)
      {
        Cell cell;
        CellStyle cellStyle;
        Font headerFont;
        
        cellStyle = workbook.createCellStyle();
        headerFont = workbook.createFont();
        headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        
        // Start column for test data parameters
        int ColumnIndex = 6;
        
        // Add Test Case Id
        AddCell(3, testData.TestCaseId, false, false);
        
        // Make Test Case Id bold
        cell = reportRow.getCell(3);

        cellStyle.setFont(headerFont);
        cell.setCellStyle(cellStyle);
        
        // Add Test Method (Keyword)
        AddCell(4, testData.TestMethod, false, false);
        
        // Make Test Method bold
        cell = reportRow.getCell(4);

        cellStyle.setFont(headerFont);
        cell.setCellStyle(cellStyle);
        
        // Add Test Description 
        AddCell(5, testData.TestDescription, false, false);
        
        // Make Test Description  bold
        cell = reportRow.getCell(5);
        
        cellStyle.setFont(headerFont);
        cell.setCellStyle(cellStyle);
        
        if(testData.TestParameters != null)
        {    
            Iterator it = testData.TestParameters.entrySet().iterator();

            while (it.hasNext()) 
            {
                reportRow = OutputSheet.getRow(rowNumber);
                
                Map.Entry pairs = (Map.Entry)it.next();
                
                AddCell(ColumnIndex, pairs.getKey().toString(), false, false);

                // Make Parameters bold
                cell = reportRow.getCell(ColumnIndex);
                cellStyle = workbook.createCellStyle();
                headerFont = workbook.createFont();
                headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
                cellStyle.setFont(headerFont);
                cell.setCellStyle(cellStyle);


                reportRow = OutputSheet.getRow(rowNumber + 1);
                if(reportRow == null)
                {
                    reportRow = OutputSheet.createRow(rowNumber + 1);
                }

                AddCell(ColumnIndex, pairs.getValue().toString(), false, false);
                it.remove(); 

                ColumnIndex ++;
            }
        }
       currentRow ++;
    }

    private void WriteSummarySection()
      {
        this.AddSummarySectionColumnHeadings();
        this.AddSummaryData();
      }

    private boolean WriteTestReport()
      {
        try
          {
            FileOutputStream outputStream = new FileOutputStream(_inputFilePath);

            AutoSizeColumnsBeforeSave();
            workbook.write(outputStream);

            outputStream.close();

            return true;
          }
        catch (Exception e)
          {
            return false;
          }
      }

    private void SetResultAsPassed(int rowIndex)
      {
        CellStyle cellStyle = workbook.createCellStyle();
        Cell cell = OutputSheet.getRow(rowIndex).getCell(3);

        cellStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
        cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        cell.setCellStyle(cellStyle);
      }

    private void SetResultAsFailed(int rowIndex)
      {
        CellStyle cellStyle = workbook.createCellStyle();
        Cell cell = OutputSheet.getRow(rowIndex).getCell(3);

        cellStyle.setFillForegroundColor(IndexedColors.ROSE.getIndex());
        cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        cell.setCellStyle(cellStyle);
      }
    
    public void SetResultAsWarning(int rowIndex)
    {
       CellStyle cellStyle = workbook.createCellStyle();
        Cell cell = OutputSheet.getRow(rowIndex).getCell(3);

        cellStyle.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
        cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        cell.setCellStyle(cellStyle);
    }
    
    
    
    

    private void CalculateTotalTestTime()
      {
        for (TestResult result : testResults)
          {
            TotalSeconds += result.testDuration;
          }

        if (TotalSeconds > 60)
          {
            while (TotalSeconds > 60)
              {
                TotalMinutes += 1;
                TotalSeconds -= 60;

              }
          }

        if (TotalMinutes > 60)
          {
            while (TotalMinutes > 60)
              {
                TotalHours += 1;
                TotalMinutes -= 60;
              }
          }
        
        
        
        
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
