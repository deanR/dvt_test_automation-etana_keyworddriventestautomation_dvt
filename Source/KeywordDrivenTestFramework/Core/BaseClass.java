
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Core;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Reporting.ReportGenerator;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import KeywordDrivenTestFramework.Utilities.SeleniumDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.Duration;

/**
 *
 * @author fnell
 */

//All tests inherit from base class, 
//Base class contains initialisations of all neccessary utilities and 
//entities
public class BaseClass
  {
    public static ApplicationConfig appConfig = new ApplicationConfig();
    public static List<TestEntity> testDataList;
    public static Enums.BrowserType browserType;
    public static ReportGenerator reportGenerator;
    public static SeleniumDriverUtility SeleniumDriverInstance;
    public static String testCaseId;
    public static String reportDirectory;
    public static String scenarioName;
    public static String inputFilePath;
    public static Enums.TestEnvironments CurrentEnvironment;
    //public static ResolveEnvironmentName enviroName = new ResolveEnvironmentName();
    private DateTime startTime, endTime;
    private Duration testDuration;
    TestEntity testData;

    public BaseClass()
      {
        System.setProperty("java.awt.headless", "true");
        
      }

    public void setStartTime()
      {
        this.startTime = new DateTime();
      }

    public long getTotalExecutionTime()
      {
        this.endTime = new DateTime();
        testDuration = new Duration(this.startTime, this.endTime);

        return testDuration.getStandardSeconds();
      }
    
   

    public String retrieveTestParameterUsingTestCaseId(String testCaseId, String parameterName)
      {
        String defaultReturn = "parameter not found";

        for (TestEntity testEntity : this.testDataList)
          {
            if (testEntity.TestCaseId.toUpperCase().equals(testCaseId.toUpperCase()))
              {
                if (testEntity.TestParameters.containsKey(parameterName))
                  {
                    return testEntity.TestParameters.get(parameterName);
                  }
                else
                  {
                    return defaultReturn;
                  }
              }
          }

        return defaultReturn;
      }
    
    public String retrieveTestParameter(String parameterName)
      {
        String defaultReturn = "parameter not found";

        for (TestEntity testEntity : this.testDataList)
          {
            
                if (testEntity.TestParameters.containsKey(parameterName))
                  {
                    return testEntity.TestParameters.get(parameterName);
                  }
                else
                  {
                    return defaultReturn;
                  }
              
          }

        return defaultReturn;
      }
    
    public String generateDateTimeString()
    {
      Date dateNow = new Date( );
      SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy-MM-dd_hh-mm-ss");
      
      return dateFormat.format(dateNow).toString();
    }

    public void resolveScenarioName()
      {
        String isolatedFileNameString;
        String[] splitFileName;
        String[] inputFileNameArray;
        String resolvedScenarioName = "";

        // Get file name from inputFilePath (remove file extension)
        inputFileNameArray = inputFilePath.split("\\.");
        isolatedFileNameString = inputFileNameArray[0];
        inputFileNameArray = isolatedFileNameString.split("/");
        isolatedFileNameString = inputFileNameArray[inputFileNameArray.length - 1];

        splitFileName = isolatedFileNameString.split("(?=\\p{Upper})");

        for (String word : splitFileName)
          {
            resolvedScenarioName += word + " ";
          }

        scenarioName = resolvedScenarioName.trim();
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
