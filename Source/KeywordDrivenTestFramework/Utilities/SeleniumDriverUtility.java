/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.RetrievedTestValues;
import KeywordDrivenTestFramework.Entities.TestEntity;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author fnell public String DriverExceptionDetail = "";
 *
 */
// Contains logic for handling accessor methods and driver calls.
public class SeleniumDriverUtility extends BaseClass {

    public WebDriver Driver;
    private Enums.BrowserType browserType;
    File fileIEDriver;
    File fileChromeDriver;
    private Boolean _isDriverRunning;
    public RetrievedTestValues retrievedTestValues;
    public String DriverExceptionDetail = "";
    TestEntity testData;

    public SeleniumDriverUtility(Enums.BrowserType selectedBrowser) {
        retrievedTestValues = new RetrievedTestValues();

        _isDriverRunning = false;
        browserType = selectedBrowser;

        fileIEDriver = new File("IEDriverServer.exe");
        System.setProperty("webdriver.ie.driver", fileIEDriver.getAbsolutePath());

        fileChromeDriver = new File("chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", fileChromeDriver.getAbsolutePath());
    }

    public boolean isDriverRunning() {
        return _isDriverRunning;
    }

    public void startDriver() {

        switch (browserType) {
            case IE:
                Driver = new InternetExplorerDriver();
                _isDriverRunning = true;
                break;
            case FireFox:
                Driver = new FirefoxDriver();
                _isDriverRunning = true;
                break;
            case Chrome:
                Driver = new ChromeDriver();
                _isDriverRunning = true;
                break;
            case Safari: ;
                break;
        }
        retrievedTestValues = new RetrievedTestValues();
        //Driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        //Driver.manage().timeouts().pageLoadTimeout(ApplicationConfig.WaitTimeout(), TimeUnit.SECONDS);
        //Driver.manage().timeouts().setScriptTimeout(ApplicationConfig.WaitTimeout(), TimeUnit.SECONDS);
        Driver.manage().window().maximize();
    }

    public boolean enableDisabledElementById(String elementId) {
        try {
            System.out.println("Attempting to enable element - " + elementId);
            JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;
            js.executeScript("document.getElementById('" + elementId + "').removeAttribute('disabled')");
            System.out.println("Element enabled successfully - " + elementId);
            return true;
        } catch (Exception e) {
            System.err.println("Failed to enable specifed element - " + e.getMessage());
            return false;
        }

    }

    public boolean verifyElementDisabledElementById(String elementId) {
        try {
            System.out.println("Attempting to check if  element is disabled - " + elementId);
            WebElement element = Driver.findElement(By.id(elementId));
            //element.click();
            if (element.isEnabled()) {
                return false;
            }
            System.out.println("element is disabled - " + elementId);

        } catch (Exception e) {
            System.err.println("Failed to Verify specifed element - " + e.getMessage());
            return false;
        }
        return true;
    }

    public boolean navigateTo(String hoverMenuID, String pageName) {
        try {
            this.waitForElementById("ctl00_menuDiv");
            WebElement menuDiv = Driver.findElement(By.id("ctl00_menuDiv"));
            WebElement hoverMenu = menuDiv.findElement(By.id(hoverMenuID));
            Actions act = new Actions(Driver);
            act.moveToElement(hoverMenu);
            act.perform();

            this.waitForElementByLinkText(pageName);
            WebElement page = Driver.findElement(By.linkText(pageName));
            String pageurl = page.getAttribute("href");
            Driver.navigate().to(pageurl);
            System.out.println("Navigating to the " + pageName + " page.");
            return true;

        } catch (Exception e) {
            System.err.println("Failed to navigate to the " + pageName + " page." + e.getMessage());
            return false;
        }

    }

    public boolean ArrowdownToElementById(String elementId) {
        try {
            System.out.println("[Info]Attempting to click Arrow down key - " + elementId);
            Thread.sleep(1000);
            waitForElementById(elementId);
            WebElement Execute = Driver.findElement(By.id(elementId));
            Execute.sendKeys(Keys.ARROW_DOWN);
            System.out.println("[Info]Arrow Down Executed");
            Execute.sendKeys(Keys.RETURN);
            System.out.println("[Info]Enter button Executed");

            return true;
        } catch (Exception e) {
            System.err.println("Error sending arrow down key to element by Id - " + elementId + " , error - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementById(String elementId) {
        try {

            System.out.println("[Info]Attempting to click element by ID - " + elementId);
            Thread.sleep(1000);
            waitForElementById(elementId);
            Driver.findElement(By.id(elementId)).click();
            System.out.println("[Info]Element clicked successfully...proceeding");
            return true;
        } catch (Exception e) {
            System.err.println("Error clicking element by Id - " + elementId + " , error - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean doubleClickElementById(String elementId) {
        try {
            System.out.println("[Info]Attempting to click element by ID - " + elementId);

            Thread.sleep(1000);

            waitForElementById(elementId);

            WebElement clickElement = Driver.findElement(By.id(elementId));

            Actions actions = new Actions(Driver);

            actions.moveToElement(clickElement);
            actions.doubleClick();
            actions.perform();

            Driver.findElement(By.id(elementId)).click();
            System.out.println("[Info]Element clicked successfully...proceeding");
            return true;

//            System.out.println("[Info]Attempting to click element by ID - " + elementId);
//            Thread.sleep(1000);
//            waitForElementById(elementId);
//            Driver.findElement(By.id(elementId)).click();
//            System.out.println("[Info]Element clicked successfully...proceeding");
//            return true;
        } catch (Exception e) {
            System.err.println("Error clicking element by Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public void checkPresenceofElementById(String elementToLookFor, String elementToClick) {
        try {

            WebDriverWait wait = new WebDriverWait(Driver, 10);
            List<WebElement> WEelementToLookFor = Driver.findElements(By.id(elementToLookFor));
            if (WEelementToLookFor.isEmpty()) {
                if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementToClick))) != null) {
                    WebElement WEelementToClick = Driver.findElement(By.id(elementToClick));
                    System.out.println("Element to click found");
                    WEelementToClick.click();
                }

            }
            while (WEelementToLookFor.isEmpty()) {
                break;
            }

            System.out.println("Element to look for was found, moving to the next action method...");

        } catch (Exception e) {
            System.err.println("Failed to find specified elements" + e.getMessage());
        }

    }

    public boolean checkRiskActionElement() {
        // WebElement links = Driver.findElement(By.id("ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnEdit"));
        boolean isLink = false;

        try {
            WebDriverWait wait = new WebDriverWait(Driver, 10);
            if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl02_lnkbtnEdit"))) != null) {
                System.out.println("[Info] Element is a link");
                isLink = true;

            } else {
                isLink = false;
                System.out.println("[Info] No link was found");

            }

        } catch (Exception e) {
            System.out.println("[Info] No link was found " + e.getMessage());
            isLink = false;
        }

        return isLink;

    }

    public boolean selectRiskType(String riskname) {
        String RiskName = riskname.trim();
        try {

            int pageNumber = 1;
            boolean hasLinks = false;
            try {
                WebDriverWait wait = new WebDriverWait(Driver, 15);
                if (wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("2"))) != null) {
                    hasLinks = true;
                }

            } catch (Exception e) {
                System.out.println("[info] Could not detect multiple risk pages");
            }

            while (true) {
                System.out.println("[info] Attempting to select risk " + RiskName + " from page " + String.valueOf(pageNumber));

                pause(5000);

                WebElement div = Driver.findElement(By.id("ctl00_cntMainBody_ucSelectRiskType_grdvSelectRiskType"));
                System.out.println("Found div");
                WebElement table = div.findElement(By.tagName("table"));
                System.out.println("Found table");
                List<WebElement> rows = table.findElements(By.tagName("tr"));
                System.out.println("Found rows");
                int rowCount = 1;
                for (WebElement row : rows) {
                    System.out.println("Row -" + rowCount);
                    List<WebElement> cells = row.findElements(By.tagName("td"));

                    if (cells == null || cells.size() < 1) {
                        continue;
                    }
                    String firstcell = cells.get(0).getText();

                    if (firstcell.toUpperCase().trim().equals(RiskName.toUpperCase())) {
                        WebElement selectLink = row.findElement(By.linkText("Select"));
                        selectLink.click();
                        System.out.println("Risk selected successfully - " + RiskName);
                        return true;
                    }

                    rowCount++;
                }

                if (!hasLinks) {
                    break;
                } else {
                    pageNumber++;

                    if (Driver.findElement(By.linkText(String.valueOf(pageNumber))) != null) {
                        System.out.println("[info] Attempting to navigate to page " + String.valueOf(pageNumber));
                        JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;
                        //  javascript:__doPostBack('ctl00$cntMainBody$ucSelectRiskType$grdvSelectRiskType','Page$3')
                        js.executeScript("javascript:__doPostBack('ctl00$cntMainBody$ucSelectRiskType$grdvSelectRiskType','Page$" + String.valueOf(pageNumber) + "')");

                    } else {
                        System.err.println("Could not find specified risk name - " + RiskName);
                        return false;
                    }
                }
            }

            System.err.println("Could not find specified risk name - " + RiskName);
            return false;

        } catch (Exception e) {
            System.err.println("Error selecting risk type - " + RiskName + " - Error - " + e.getMessage());
            return false;
        }

    }

    public boolean changePolicy(String policyNumber, String linkToClick) {
        String policyNum = policyNumber.trim();
        try {

            int pageNumber = 1;
            boolean hasLinks = false;
            try {
                WebDriverWait wait = new WebDriverWait(Driver, 10);
                if (wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("2"))) != null) {
                    hasLinks = true;
                }

            } catch (Exception e) {
                System.out.println("[info] Could not detect multiple policy pages");
            }

            while (true) {
                System.out.println("[info] Attempting to perform policy MTA for policy number - " + policyNum + " from page " + String.valueOf(pageNumber));

                pause(5000);

                WebElement tableContainerDiv = Driver.findElement(By.id("ctl00_cntMainBody_ClientPolicies_grdvQuotes"));
                System.out.println("Table Container Div Found");
                WebElement table = tableContainerDiv.findElement(By.tagName("table"));
                System.out.println("Found table");
                List<WebElement> rows = table.findElements(By.tagName("tr"));
                System.out.println("Found rows");
                int rowCount = 1;
                for (WebElement row : rows) {
                    System.out.println("Row -" + rowCount);
                    List<WebElement> cells = row.findElements(By.tagName("td"));

                    if (cells == null || cells.size() < 1) {
                        continue;
                    }
                    String firstcell = cells.get(0).getText();

                    if (firstcell.toUpperCase().trim().equals(policyNumber.toUpperCase())) {
                        List<WebElement> actionLink = row.findElements(By.linkText(linkToClick));
                        if (!actionLink.isEmpty()) {
                            actionLink.get(0).click();
                            System.out.println("Link clicked successfully by link text - " + linkToClick);
                            return true;
                        }
                    }

                    rowCount++;
                }

                if (!hasLinks) {
                    break;
                } else {
                    pageNumber++;

                    if (Driver.findElement(By.linkText(String.valueOf(pageNumber))) != null) {
                        System.out.println("[info] Attempting to navigate to page " + String.valueOf(pageNumber));
                        JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;
                        //  javascript:__doPostBack('ctl00$cntMainBody$ucSelectRiskType$grdvSelectRiskType','Page$3')
                        js.executeScript("javascript:__doPostBack('ctl00$cntMainBody$ClientPolicies$grdvQuotes','Page$" + String.valueOf(pageNumber) + "')");

                    } else {
                        System.err.println("Could not find specified policy - " + policyNum);
                        return false;
                    }
                }

            }

            System.err.println("Could not find specified policy - " + policyNum);
            return false;

        } catch (Exception e) {
            System.err.println("Error selecting policy - " + policyNum + " - Error - " + e.getMessage());
            return false;
        }

    }

    public boolean Payclaim(String claimref, String linkToClick) {
        String claimNum = claimref.trim();
        try {

            int pageNumber = 1;
            boolean hasLinks = false;
            try {
                WebDriverWait wait = new WebDriverWait(Driver, 10);
                if (wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("2"))) != null) {
                    hasLinks = true;
                }

            } catch (Exception e) {
                System.out.println("[info] Could not detect multiple policy pages");
            }

            while (true) {
                System.out.println("[info] Attempting to perform policy MTA for policy number - " + claimNum + " from page " + String.valueOf(pageNumber));

                pause(5000);

                WebElement tableContainerDiv = Driver.findElement(By.id("ctl00_cntMainBody_grdvLinkedClaims"));
                System.out.println("Table Container Div Found");
                WebElement table = tableContainerDiv.findElement(By.tagName("table"));
                System.out.println("Found table");
                List<WebElement> rows = table.findElements(By.tagName("tr"));
                System.out.println("Found rows");
                int rowCount = 1;
                for (WebElement row : rows) {
                    System.out.println("Row -" + rowCount);
                    List<WebElement> cells = row.findElements(By.tagName("td"));

                    if (cells == null || cells.size() < 1) {
                        continue;
                    }
                    String firstcell = cells.get(0).getText();

                    if (firstcell.toUpperCase().trim().equals(claimref.toUpperCase())) {
                        List<WebElement> actionLink = row.findElements(By.linkText(linkToClick));
                        if (!actionLink.isEmpty()) {
                            actionLink.get(0).click();
                            System.out.println("Link clicked successfully by link text - " + linkToClick);
                            return true;
                        }
                    }

                    rowCount++;
                }

                if (!hasLinks) {
                    break;
                } else {
                    pageNumber++;

                    if (Driver.findElement(By.linkText(String.valueOf(pageNumber))) != null) {
                        System.out.println("[info] Attempting to navigate to page " + String.valueOf(pageNumber));
                        JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;
                        //  javascript:__doPostBack('ctl00$cntMainBody$ucSelectRiskType$grdvSelectRiskType','Page$3')
                        js.executeScript("javascript:__doPostBack('ctl00$cntMainBody$ClientPolicies$grdvQuotes','Page$" + String.valueOf(pageNumber) + "')");

                    } else {
                        System.err.println("Could not find specified Claim - " + claimNum);
                        return false;
                    }
                }

            }

            System.err.println("Could not find specified Claim - " + claimNum);
            return false;

        } catch (Exception e) {
            System.err.println("Error selecting claim - " + claimNum + " - Error - " + e.getMessage());
            return false;
        }

    }

    public boolean checkRenewalManagerPolicy(String policyRef) {
        String policyNumber = policyRef.trim();
        try {

            int pageNumber = 1;
            boolean hasLinks = false;
            try {
                WebDriverWait wait = new WebDriverWait(Driver, 15);
                if (wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("2"))) != null) {
                    hasLinks = true;
                }

            } catch (Exception e) {
                System.out.println("[info] Could not detect multiple policy pages");
            }

            while (true) {
                System.out.println("[info] Attempting to check policy " + policyNumber + " from page " + String.valueOf(pageNumber));

                pause(5000);

                WebElement div = Driver.findElement(By.id("ctl00_cntMainBody_grdvRenQuotes"));
                System.out.println("Found div");
                WebElement table = div.findElement(By.tagName("table"));
                System.out.println("Found table");
                List<WebElement> rows = table.findElements(By.tagName("tr"));
                System.out.println("Found rows");
                int rowCount = 1;
                for (WebElement row : rows) {
                    System.out.println("Row -" + rowCount);
                    List<WebElement> cells = row.findElements(By.tagName("td"));

                    if (cells == null || cells.size() < 1) {
                        continue;
                    }
                    String thirdcell = cells.get(2).getText();

                    if (thirdcell.toUpperCase().trim().equals(policyNumber.toUpperCase())) {
                        WebElement checkbox = row.findElement(By.tagName("input"));
                        checkbox.click();
                        System.out.println("Policy checked successfully - " + policyNumber);
                        return true;
                    }

                    rowCount++;
                }

                if (!hasLinks) {
                    break;
                } else {
                    pageNumber++;

                    if (Driver.findElement(By.linkText(String.valueOf(pageNumber))) != null) {
                        System.out.println("[info] Attempting to navigate to page " + String.valueOf(pageNumber));
                        JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;
                        js.executeScript("javascript:__doPostBack('ctl00$cntMainBody$grdvRenQuotes','Page$" + String.valueOf(pageNumber) + "')");

                    } else {
                        System.err.println("Could not find specified policy number - " + policyNumber);
                        return false;
                    }
                }

            }

            System.err.println("Could not find specified policy number - " + policyNumber);
            return false;

        } catch (Exception e) {
            System.err.println("Error checking policy - " + policyNumber + " - Error - " + e.getMessage());
            return false;
        }

    }

    public boolean clientAddressActionLinks(String riskType, String riskAction) {
        try {
            WebElement addressesDiv = Driver.findElement(By.id("ctl00_cntMainBody_Addresses_drgAddresses"));

            WebElement addressesTable = addressesDiv.findElement(By.tagName("table"));

            List<WebElement> rows = addressesTable.findElements(By.tagName("tr"));

            for (WebElement row : rows) {
                List<WebElement> cells = row.findElements(By.tagName("td"));
                if (cells == null || cells.size() < 1) {
                    continue;
                }
                String firstCell = cells.get(0).getText();

                if (firstCell.toUpperCase().trim().equals(riskType.toUpperCase().trim())) {
                    WebElement actionLink = row.findElement(By.linkText(riskAction));
                    actionLink.click();
                    return true;
                }
//                else
//                {
//                    System.out.println("FAiled to find specified address type");
//                    return false; 
//                }

            }

            System.out.println("FAiled to find specified address type");
            return false;

        } catch (Exception e) {
            System.out.println("FAiled to find specified address type" + e.getMessage());
            return false;
        }
    }

    public boolean selectPolicyAgentCode(String agentCode) {
        try {
            System.out.println("[Info]Attempting to Select Agent code - " + agentCode);
            Thread.sleep(1000);
            waitForElementById(agentCode);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.id(agentCode)));
            WebElement agentCodeRow = Driver.findElement(By.id(agentCode));

            agentCodeRow.findElement(By.tagName("a")).click();

            System.out.println("[Info]Agent code selected successfully...proceeding");
            return true;
        } catch (Exception e) {
            System.err.println("Error selecting Agent code - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean verifyPaymentDetails(String text) {
        try {
            System.out.println("[Info]Attempting to get text - " + text);
            Thread.sleep(1000);

           //List<WebElement> tableList = Driver.findElements(By.tagName("table"));
            //WebElement table = tableList.get(1);
            WebElement element = Driver.findElement(By.id("ctl00_cntMainBody_PayClaim_ctrl_updPayment"));

            WebElement table = element.findElement(By.tagName("table"));
            WebElement tbody = table.findElement(By.tagName("tbody"));

            List<WebElement> tableRows = tbody.findElements(By.tagName("tr"));

            WebElement row = tableRows.get(0);

            List<WebElement> tableCells = row.findElements(By.tagName("td"));

            for (int a = 0; a < tableCells.size(); a++) {
                if (tableCells.get(a).getText().equals(text)) {
                    System.out.println("Text Found " + text);

                    return true;
                }
            }

            System.out.println("[Info]Text found on page ...proceeding");

            return true;
        } catch (Exception e) {
            System.err.println("Error Text Not found - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }
    
    public boolean verifyPremiumDetails() {
        try {
            System.out.println("[Info]Attempting to get text - ");
            Thread.sleep(1000);

           //List<WebElement> tableList = Driver.findElements(By.tagName("table"));
            //WebElement table = tableList.get(1);
            WebElement element = Driver.findElement(By.id("ctl00_cntMainBody_ReInsurance2007Cntrl_gvReinsurance"));

            WebElement table = element.findElement(By.tagName("table"));
            WebElement tbody = table.findElement(By.tagName("tbody"));

            List<WebElement> tableRows = tbody.findElements(By.tagName("tr"));

            WebElement row = tableRows.get(0);

            List<WebElement> tableCells = row.findElements(By.tagName("td"));
            WebElement col = tableCells.get(8);
//            double premium = Double.(col.getText());
            String premium = (col.getText());
            if (premium.equals("0.00") ) {
                System.err.println("Error - premium is 0.00");
                System.out.println("Error - premium is 0.00");

                    return false;
                }
            else{
                System.out.println("Info - premium is  " + col.getText());

                    return true;
                
            }

        } catch (Exception e) {
            System.err.println("Error Text Not found - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectByTextFromDropDownListUsingXpath(String elementXpath, String valueToSelect) {
        try {
            waitForElementByXpath(elementXpath);
            Select dropDownList = new Select(Driver.findElement(By.xpath(elementXpath)));
            dropDownList.selectByVisibleText(valueToSelect);
            return true;
        } catch (Exception e) {
            System.err.println("Error selecting from dropdownlist by text using Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToFrameByClassName(String className) {
        int waitCount = 0;
        try {
            while (waitCount < ApplicationConfig.WaitTimeout()) {
                try {
                    WebElement element = Driver.findElement(By.className(className));
                    Driver.switchTo().frame(element);
                    return true;
                } catch (Exception e) {
                    Thread.sleep(500);
                    waitCount++;
                }
            }
            return false;
        } catch (Exception e) {
            System.err.println("Error switching to frame by ClassName - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToFrameById(String elementId) {
        int waitCount = 0;
        try {
            while (waitCount < ApplicationConfig.WaitTimeout()) {
                try {
                    Driver.switchTo().frame(elementId);
                    return true;
                } catch (Exception e) {
                    Thread.sleep(500);
                    waitCount++;
                }
            }
            return false;
        } catch (Exception e) {
            System.err.println("Error switching to frame by Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToLastDuplicateFrameById(String elementId) {
        int waitCount = 0;
        try {
            this.switchToDefaultContent();
            while (waitCount < ApplicationConfig.WaitTimeout()) {
                try {
                    List<WebElement> iframes = Driver.findElements(By.id(elementId));

                    Driver.switchTo().frame((WebElement) iframes.toArray()[iframes.size() - 1]);
                    return true;
                } catch (Exception e) {
                    Thread.sleep(500);
                    waitCount++;
                }
            }
            return false;
        } catch (Exception e) {
            System.err.println("Error switching to last duplicate frame by Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToDefaultContent() {
        try {
            Driver.switchTo().defaultContent();
            return true;
        } catch (Exception e) {
            System.err.println("Error switching to default content  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean moveToElementById(String elementId) {
        try {
            Actions moveTo = new Actions(Driver);
            moveTo.moveToElement(Driver.findElement(By.id(elementId)));
            moveTo.perform();
            return true;
        } catch (Exception e) {
            System.err.println("Error moving to element - " + elementId + " - " + e.getStackTrace());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean switchToDefaultContentWhenElementNoLongerVisible(String previousFrameId) {
        try {
            waitForElementNoLongerPresentById(previousFrameId);
            Driver.switchTo().defaultContent();
            System.out.println("Successfully switched to default content, current frame handle = " + Driver.getWindowHandle() + ", previous frameId - " + previousFrameId);
            return true;
        } catch (Exception e) {
            System.err.println("Error switching to default content when element is no longer visible - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectByValueFromDropDownListUsingId(String elementId, String valueToSelect) {
        try {
            waitForElementById(elementId);
            Select dropDownList = new Select(Driver.findElement(By.id(elementId)));
            dropDownList.selectByValue(valueToSelect);
            return true;
        } catch (Exception e) {
            System.err.println("Error selecting from dropdownlist by value using Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean acceptAlertDialog() {
        int waitCount = 0;
        try {
            while (waitCount < ApplicationConfig.WaitTimeout()) {
                try {
                    Driver.switchTo().alert().accept();
                    return true;
                } catch (Exception e) {
                    Thread.sleep(500);
                    waitCount++;
                }
            }
            return false;
        } catch (Exception e) {
            System.err.println("Error accepting alert dialog - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public String retrieveTextById(String elementId) {
        String retrievedText = "";
        try {
            waitForElementById(elementId);
            WebElement elementToRead = Driver.findElement(By.id(elementId));
            retrievedText = elementToRead.getText();
            return retrievedText;
        } catch (Exception e) {
            System.err.println("Error reading text from element - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedText;
        }
    }

    public String retrieveTextByXpath(String elementXpath) {
        String retrievedText = "";
        try {
            waitForElementById(elementXpath);
            WebElement elementToRead = Driver.findElement(By.xpath(elementXpath));
            retrievedText = elementToRead.getText();
            System.out.println("Text retrieved successfully from element - " + elementXpath);
            return retrievedText;

        } catch (Exception e) {
            System.err.println("Error reading text from element - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return retrievedText;
        }
    }

    public boolean selectVehicleFromList(String elementId) {
        try {
            waitForElementById(elementId);
            WebElement popupGrid = Driver.findElement(By.id(elementId));
            WebElement tableRow = popupGrid.findElement(By.id("2"));
            WebElement tableCell = tableRow.findElement(By.className("pointer"));

            tableCell.click();

            return true;
        } catch (Exception e) {
            System.err.println("Error selecting from dropdownlist by text using Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectByTextFromDropDownListUsingId(String elementId, String valueToSelect) {
        try {
            waitForElementById(elementId);
            Select dropDownList = new Select(Driver.findElement(By.id(elementId)));
            dropDownList.selectByVisibleText(valueToSelect);
            return true;
        } catch (Exception e) {
            System.err.println("Error selecting from dropdownlist by text using Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean selectByValueFromDropDownListUsingName(String elementName, String valueToSelect) {
        try {
            this.waitForElementByName(elementName);
            Select dropDownList = new Select(Driver.findElement(By.name(elementName)));
            dropDownList.selectByValue(valueToSelect);
            return true;
        } catch (Exception e) {
            System.err.println("Error selecting from dropdownlist by value using Name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public void pressKeyOnElementById(String elementId, Keys keyToPress) {
        try {
            this.waitForElementByName(elementId);
            WebElement elementToAccess = Driver.findElement(By.id(elementId));
            elementToAccess.sendKeys(keyToPress);

        } catch (Exception e) {
            this.DriverExceptionDetail = e.getMessage();
            System.err.println("[Error] Failed to send keypress to element - " + elementId);
        }
    }

    public boolean selectByTextFromDropDownListUsingName(String elementName, String valueToSelect) {
        try {
            this.waitForElementById(elementName);
            Select dropDownList = new Select(Driver.findElement(By.name(elementName)));
            dropDownList.selectByVisibleText(valueToSelect);
            return true;
        } catch (Exception e) {
            System.err.println("Error selecting from dropdownlist by text using name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public void waitUntilElementEnabledByID(String elementID) {
        try {
            int counter = 0;
            boolean isEnabled = false;
            WebDriverWait wait = new WebDriverWait(Driver, 1);

            while (!isEnabled && counter < ApplicationConfig.WaitTimeout()) {
                if (wait.until(ExpectedConditions.elementToBeClickable(By.id(elementID))) != null) {
                    isEnabled = true;
                    break;
                } else {
                    counter++;
                    Thread.sleep(500);
                }

            }
        } catch (Exception e) {
            System.err.println("Error waiting for element to be enabled - " + e.getMessage());
        }

    }

    public boolean checkBoxSelectionById(String elementId, boolean mustBeSelected) {
        try {
            Thread.sleep(2000);
            this.waitForElementById(elementId);
            this.waitUntilElementEnabledByID(elementId);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.id(elementId)));
            WebElement checkBox = Driver.findElement(By.id(elementId));
            if (checkBox.isSelected() != mustBeSelected) {
                checkBox.click();
                return true;
            } else {
                System.err.println("WARNING selecting checkbox byId - " + elementId + " , was not set to selected");
                return true;
            }

        } catch (Exception e) {
            System.err.println("Error selecting checkbox byId - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }

    }

    public boolean uncheckCheckBoxSelectionById(String elementId, boolean mustBeSelected) {
        try {
            Thread.sleep(2000);
            this.waitForElementById(elementId);
            this.waitUntilElementEnabledByID(elementId);
            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.id(elementId)));
            WebElement checkBox = Driver.findElement(By.id(elementId));
            if (checkBox.isSelected() == mustBeSelected) {
                checkBox.click();
            }
            return true;
        } catch (Exception e) {
            System.err.println("Error selecting checkbox byId - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean validateElementTextValueByClassName(String elementClassName, String elementText) {
        try {
            if (waitForElementByClassName(elementClassName)) {
                WebElement elementToValidate = Driver.findElement(By.className(elementClassName));
                String textDetected = elementToValidate.getText();
                if (textDetected.contains(elementText)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            System.err.println("Error validating element text value by class name - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean validateElementTextValueById(String elementId, String elementText) {
        try {
            if (waitForElementById(elementId)) {
                WebElement elementToValidate = Driver.findElement(By.id(elementId));
                String textDetected = elementToValidate.getText();
                if (textDetected.contains(elementText)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            System.err.println("Error validating element text value by ID - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public void WaitUntilDropDownListPopulatedById(String elementId) {

        try {
            this.waitForElementById(elementId);
            int waitCount = 0;
            List<WebElement> optionsList;
            while (waitCount < ApplicationConfig.WaitTimeout()) {
                try {
                    Select dropDownList = new Select(Driver.findElement(By.id(elementId)));
                    optionsList = dropDownList.getOptions();
                    if (optionsList.size() > 0) {
                        break;
                    }
                } catch (Exception e) {

                }
                Thread.sleep(500);
                waitCount++;
            }
        } catch (Exception e) {
            System.err.println("Error waiting for dropdownlist to be populated by ID - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
    }

    public boolean hoverOverElementAndClickSubElementbyIdAndLinkText(String elementId, String LinkText) {
        try {
            this.waitForElementById(elementId);

            Actions actions = new Actions(Driver);
            WebElement menuHoverLink = Driver.findElement(By.id(elementId));
            actions.moveToElement(menuHoverLink);

            WebElement subLink = menuHoverLink.findElement(By.linkText(LinkText));
            actions.moveToElement(subLink);
            actions.doubleClick();
            actions.perform();
            return true;
        } catch (Exception e) {
            System.err.println("Failed to hover over element and click sub element by ID and link text  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean navigateToFindClientPage(String hoverMenuID, String pageName) {
        try {
            this.waitForElementById("ctl00_menuDiv");
            WebElement menuDiv = Driver.findElement(By.id("ctl00_menuDiv"));
            WebElement hoverMenu = menuDiv.findElement(By.id(hoverMenuID));
            Actions act = new Actions(Driver);
            act.moveToElement(hoverMenu);
            act.perform();

            this.waitForElementByLinkText(pageName);
            WebElement page = Driver.findElement(By.linkText(pageName));
            String pageurl = page.getAttribute("href");
            Driver.navigate().to(pageurl);
            System.out.println("Navigating to the " + pageName + " page.");
            return true;

        } catch (Exception e) {
            System.err.println("Failed to navigate to the " + pageName + " page." + e.getMessage());
            return false;
        }

    }

    public boolean hoverOverElementAndClickSubElementbyIdAndXpath(String elementId, String XPath) {
        try {
            this.waitForElementById(elementId);

            Actions actions = new Actions(Driver);
            WebElement menuHoverLink = Driver.findElement(By.id(elementId));
            actions.moveToElement(menuHoverLink);

            WebElement subLink = menuHoverLink.findElement(By.xpath(XPath));
            actions.moveToElement(subLink);
            actions.click();
            actions.perform();
            return true;
        } catch (Exception e) {
            System.err.println("Failed to hover over element and click sub element by ID and Xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickNestedElementUsingIds(String parentElementId, String childElementId) {
        try {
            this.waitForElementById(parentElementId);
            WebElement parentElement = Driver.findElement(By.id(parentElementId));
            WebElement childElement = parentElement.findElement(By.id(childElementId));
            childElement.click();
            return true;
        } catch (Exception e) {
            System.err.println("Error clicking nested elements usind IDs  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementbyXpath(String elementXpath) {
        try {

            WebDriverWait wait = new WebDriverWait(Driver, ApplicationConfig.WaitTimeout());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            elementToClick.click();

            return true;
        } catch (Exception e) {
            System.err.println("Error clicking element by Xpath - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean addRiskToPolicy() {
        try {
            Driver.navigate().refresh();
            WebElement row = Driver.findElement(By.id("11"));
            WebElement checkbox = row.findElement(By.id("ctl00_cntMainBody_MultiRisk1_grdvRisk_ctl12_chkRiskSelect"));
            checkbox.click();
            return true;
        } catch (Exception e) {
            System.err.println("Error adding risk to policy - " + e.getMessage());
            return false;
        }
    }

    public boolean moveToAndClickElementById(String elementId) {
        try {
            //this.waitForElementById(elementId);
            WebElement elementToClick = Driver.findElement(By.id(elementId));

            Actions builder = new Actions(Driver);
            builder.moveToElement(elementToClick).click(elementToClick).perform();

            return true;
        } catch (Exception e) {
            System.err.println("Failed to move to element and click by ID  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean moveToAndClickElementByLinkText(String elementLinkText) {
        try {
            //this.waitForElementById(elementId);
            WebElement elementToClick = Driver.findElement(By.linkText(elementLinkText));

            Actions builder = new Actions(Driver);
            builder.moveToElement(elementToClick);
            builder.click(elementToClick);
            builder.perform();

            return true;
        } catch (Exception e) {
            System.err.println("Failed to move to element and click by link text  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementbyLinkText(String elementLinkText) {
        try {
            waitForElementByLinkText(elementLinkText);
            WebElement elementToClick = Driver.findElement(By.linkText(elementLinkText));
            elementToClick.click();

            System.out.println("[Info]Element successfully clicked by Link Text...proceeding");
            return true;
        } catch (Exception e) {
            System.err.println("Error clicking element by link text  - " + elementLinkText + ", error - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean doubleClickElementbyLinkText(String elementLinkText) {
        try {
            waitForElementByLinkText(elementLinkText);
            WebElement elementToClick = Driver.findElement(By.linkText(elementLinkText));
            elementToClick.click();

            System.out.println("[Info]Element successfully double-clicked by Link Text...proceeding");
            return true;
        } catch (Exception e) {
            System.err.println("Error double-clicking element by link text  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean doubleClickElementbyPartialLinkText(String elementLinkText) {
        try {
            //waitForElementByLinkText(elementLinkText);
            Actions act = new Actions(Driver);
            WebElement elementToClick = Driver.findElement(By.partialLinkText(elementLinkText));

            act.doubleClick(elementToClick).perform();

            return true;
        } catch (Exception e) {
            System.err.println("Error double clicking element by link text  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementbyPartialLinkText(String elementPartialLinkText) {
        try {
            waitForElementByPartialLinkText(elementPartialLinkText);
            WebElement elementToClick = Driver.findElement(By.partialLinkText(elementPartialLinkText));
            elementToClick.click();

            return true;
        } catch (Exception e) {
            System.err.println("Error clicking element by partial link text  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementbyName(String elementName) {
        try {
            this.waitForElementByName(elementName);
            WebElement elementToClick = Driver.findElement(By.name(elementName));
            elementToClick.click();

            return true;
        } catch (Exception e) {
            System.err.println("Error clicking element by name  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clickElementbyClassName(String elementClassName) {
        try {
            this.waitForElementByClassName(elementClassName);
            WebElement elementToClick = Driver.findElement(By.className(elementClassName));
            elementToClick.click();

            return true;
        } catch (Exception e) {
            System.err.println("Error clicking element by class name  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean elementContainsTextById(String elementId) {
        try {
            String retrievedText = "";
            this.waitForElementById(elementId);
            WebElement elementToEvaluate = Driver.findElement(By.id(elementId));
            retrievedText = elementToEvaluate.getText();
            return !retrievedText.equals("");
        } catch (Exception e) {
            System.err.println("Error checking if element contains text - " + elementId + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clearTextAndEnterValueById(String elementId, String textToEnter) {
        try {
            this.waitForElementById(elementId);
            WebElement elementToTypeIn = Driver.findElement(By.id(elementId));
            Actions typeText = new Actions(Driver);
            Actions clearText = new Actions(Driver);
            this.pause(3000);
            elementToTypeIn.click();
            elementToTypeIn.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), Keys.DELETE);
            clearText.perform();

            this.pause(3000);
            //typeText.moveToElement(elementToTypeIn);
            typeText.sendKeys(elementToTypeIn, textToEnter);
            //this.pause(3000);
            typeText.click(elementToTypeIn);
            typeText.perform();

            return true;
        } catch (Exception e) {
            System.err.println("Error entering text - " + elementId + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public void clearTextById(String elementId) {
        try {
            this.waitForElementById(elementId);
            WebElement elementToTypeIn = Driver.findElement(By.id(elementId));
            elementToTypeIn.clear();
            
            
        } catch (Exception e) {
            System.err.println("Error clearing text - " + elementId + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            
        }
    }
    
    public boolean clearTextById2(String elementId) {
        try {
            this.waitForElementById(elementId);
            WebElement elementToTypeIn = Driver.findElement(By.id(elementId));
            elementToTypeIn.clear();
            
          return true;  
        } catch (Exception e) {
            System.err.println("Error clearing text - " + elementId + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean enterTextById(String elementId, String textToEnter) {
        try {
            this.waitForElementById(elementId);
            WebElement elementToTypeIn = Driver.findElement(By.id(elementId));
            //elementToTypeIn.clear();
            Actions typeText = new Actions(Driver);
            typeText.moveToElement(elementToTypeIn);
            typeText.click(elementToTypeIn);
            typeText.sendKeys(elementToTypeIn, textToEnter);
            typeText.click(elementToTypeIn);
            typeText.perform();

            return true;
        } catch (Exception e) {
            System.err.println("Error entering text - " + elementId + " - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean clearTextAndEnterTextById(String elementId, String textToEnter) {
        try {
            this.waitForElementById(elementId);
            WebElement elementToTypeIn = Driver.findElement(By.id(elementId));
            this.pause(3000);
            elementToTypeIn.click();
            elementToTypeIn.click();
            Actions typeText = new Actions(Driver);
           // elementToTypeIn.sendKeys(Keys.HOME,Keys.chord(Keys.SHIFT, Keys.END),Keys.DELETE);
//            //clearText.perform();

            //this.pause(10000);
//            typeText.moveToElement(elementToTypeIn);
//            typeText.click(elementToTypeIn);
            typeText.sendKeys(elementToTypeIn, textToEnter).perform();
            //typeText.click(elementToTypeIn);
            //typeText;
            //this.pause(3000);

            return true;
        } catch (Exception e) {
            System.err.println("Error clearing Text And Entering value By Id - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public void scrollDownByOnePage(String elementId) {
        try {
            this.waitForElementById(elementId);
            WebElement scrollElement = Driver.findElement(By.id(elementId));
            scrollElement.sendKeys(Keys.PAGE_DOWN);

            System.out.println("Sucessfully scrolled down...");
        } catch (Exception e) {
            System.err.println("Error scrolling page - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
    }

    public void fullScrolltoBottomOfPage() {
        try {

            Actions scroll = new Actions(Driver);
            scroll.keyDown(Keys.CONTROL).sendKeys(Keys.END);
            System.out.println("Successfully scrolled to specified element");
        } catch (Exception e) {
            System.err.println("Failed to scroll" + e.getMessage());
        }

    }

    public boolean scrollToElement(String element_ToLookFor) {
        WebDriverWait wait = new WebDriverWait(Driver, 30);
        try {
            for (int i = 0;; i++) {
                if (i >= 30) {
                    Actions scroll = new Actions(Driver);
                    scroll.keyDown(Keys.CONTROL).sendKeys(Keys.END);
                    wait.until(ExpectedConditions.elementToBeClickable(By.id(element_ToLookFor)));
                    System.out.println("Successfully scrolled to specified element");
                    return true;
                }
            }

        } catch (Exception e) {
            System.err.println("Failed to scroll to specified element" + e.getMessage());
            return false;
        }

    }

    public boolean enterTextByName(String elementName, String textToEnter) {
        try {
            Thread.sleep(500);
            this.waitForElementByName(elementName);
            WebElement elementToTypeIn = Driver.findElement(By.name(elementName));
            elementToTypeIn.clear();
            elementToTypeIn.sendKeys(textToEnter);
            elementToTypeIn.click();
            return true;
        } catch (Exception e) {
            System.err.println("Error entering text by name  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean enterTextByXpath(String elementXpath, String textToEnter) {
        try {
            Thread.sleep(500);
            this.waitForElementByXpath(elementXpath);
            List<WebElement> elementToTypeIn = Driver.findElements(By.xpath(elementXpath));
            // elementToTypeIn.clear();
            if (!elementToTypeIn.isEmpty()) {
                elementToTypeIn.get(0).sendKeys(textToEnter);
                // elementToTypeIn.get(0).click();
                return true;
            } else {
                System.err.println("Error entering text by Xpath  - ");
                return false;
            }
        } catch (Exception e) {
            System.err.println("Error entering text by Xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean enterTextByClassName(String elementClassName, String textToEnter) {
        try {
            Thread.sleep(500);
            this.waitForElementByClassName(elementClassName);
            WebElement elementToTypeIn = Driver.findElement(By.className(elementClassName));
            elementToTypeIn.clear();
            elementToTypeIn.sendKeys(textToEnter);

            return true;
        } catch (Exception e) {
            System.err.println("Error entering text by class name  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    // Requires a fully qualified URL - eg: "http://www.myURL.com"
    public boolean navigateTo(String pageUrl) {
        try {
            Driver.navigate().to(pageUrl);
            return true;
        } catch (Exception e) {
            System.err.println("Error navigating to URL - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
            return false;
        }
    }

    public boolean waitForElementNoLongerPresentById(String elementId) {
        boolean elementFound = true;
        try {
            int waitCount = 0;
            while (elementFound && waitCount < ApplicationConfig.WaitTimeout()) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementId))) == null) {
                        elementFound = false;
                        break;
                    }
                } catch (Exception e) {
                    this.DriverExceptionDetail = e.getMessage();
                    elementFound = false;
                    break;
                }

                waitCount++;
            }
        } catch (Exception e) {
            System.err.println("Error waiting for element to be no longer present by ID  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementById(String elementId) {
        boolean elementFound = false;
        try {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout()) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementId))) != null) {

                        elementFound = true;
                        break;
                    }
                } catch (Exception e) {
                    elementFound = false;
                }
                waitCount++;
            }
        } catch (Exception e) {
            System.err.println("Error waiting for element by ID  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByLinkText(String elementLinkText) {
        boolean elementFound = false;
        try {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout()) {
                try {

                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(elementLinkText))) != null) {

                        elementFound = true;
                        break;
                    }
                } catch (Exception e) {
                    elementFound = false;
                }
                waitCount++;
            }
        } catch (Exception e) {
            System.err.println("Error waiting for element by link text  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByPartialLinkText(String elementPartialLinkText) {
        boolean elementFound = false;
        try {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout()) {
                try {
                    Driver.findElement(By.partialLinkText(elementPartialLinkText));
                    elementFound = true;
                    break;
                } catch (Exception e) {
                    elementFound = false;
                }
                Thread.sleep(500);
                waitCount++;
            }
        } catch (Exception e) {
            System.err.println("Error waiting for element by partial link text  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByClassName(String elementClassName) {
        boolean elementFound = false;
        try {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout()) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.className(elementClassName))) != null) {
                        elementFound = true;
                        break;
                    }
                } catch (Exception e) {
                    elementFound = false;
                }
                waitCount++;
            }
        } catch (Exception e) {
            System.err.println("Error waiting for element by class name  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByName(String elementName) {
        boolean elementFound = false;
        try {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout()) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.name(elementName))) != null) {
                        elementFound = true;
                        break;
                    }
                } catch (Exception e) {
                    elementFound = false;
                }
                waitCount++;
            }
        } catch (Exception e) {
            System.err.println("Error waiting for element by name  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public boolean waitForElementByXpath(String elementXpath) {
        boolean elementFound = false;
        try {
            int waitCount = 0;
            while (!elementFound && waitCount < ApplicationConfig.WaitTimeout()) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null) {
                        elementFound = true;
                        break;
                    }
                } catch (Exception e) {
                    elementFound = false;
                }
                Thread.sleep(500);
                waitCount++;
            }

        } catch (Exception e) {
            System.err.println("Error waiting for element by Xpath  - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        return elementFound;
    }

    public void scrollElementIntoViewById(String elementId) {
        try {
            WebElement elementToScrollIntoView = Driver.findElement(By.id(elementId));

            JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;
            js.executeScript("arguments[0].scrollIntoView(true);", elementToScrollIntoView);
            System.out.println("Successfully scrolled element into view - " + elementToScrollIntoView);

        } catch (Exception e) {
            System.err.println("Failed to scroll element into view");
        }

    }

    public void pause(int milisecondsToWait) {
        try {
            Thread.sleep(milisecondsToWait);
        } catch (Exception e) {

        }
    }

    public void shutDown() {
        retrievedTestValues = null;
        try {
            Driver.close();
            Driver.quit();
            CloseChromeInstances();
        } catch (Exception e) {
            System.err.println("Error shutting down driver - " + e.getMessage());
            this.DriverExceptionDetail = e.getMessage();
        }
        _isDriverRunning = false;
    }

    public void takeScreenShot(String screenShotDescription, boolean isError) {
        String imageFilePathString = "";

        try {
            StringBuilder imageFilePathBuilder = new StringBuilder();
            // add date time folder and test case id folder
            imageFilePathBuilder.append(this.reportDirectory + "\\" + testCaseId + "\\");

            if (isError) {
                imageFilePathBuilder.append("FAILED_");
            } else {
                imageFilePathBuilder.append("TEST_STATE_");
            }

            imageFilePathBuilder.append(testCaseId + "_");

            imageFilePathBuilder.append(screenShotDescription + "_");

            imageFilePathBuilder.append(this.generateDateTimeString() + ".png");

            imageFilePathString = imageFilePathBuilder.toString();

            File screenShot = ((TakesScreenshot) Driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenShot, new File(imageFilePathString.toString()));
        } catch (Exception e) {
            System.err.println("[Error] could not take screenshot - " + imageFilePathString.toString() + " - " + e.getMessage());
        }
    }

    public String generateDateTimeString() {
        Date dateNow = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss");

        return dateFormat.format(dateNow).toString();
    }

    public boolean FACCompanySelect(String claimref, String linkToClick) {
        String claimNum = claimref.trim();
        try {

            int pageNumber = 1;
            boolean hasLinks = false;
            try {
                WebDriverWait wait = new WebDriverWait(Driver, 10);
                if (wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("2"))) != null) {
                    hasLinks = true;
                }

            } catch (Exception e) {
                System.out.println("[info] Could not detect multiple policy pages");
            }

            while (true) {
                System.out.println("[info] Attempting to perform policy MTA for policy number - " + claimNum + " from page " + String.valueOf(pageNumber));

                pause(5000);

                WebElement tableContainerDiv = Driver.findElement(By.id("ctl00_cntMainBody_grdvLinkedClaims"));
                System.out.println("Table Container Div Found");
                WebElement table = tableContainerDiv.findElement(By.tagName("table"));
                System.out.println("Found table");
                List<WebElement> rows = table.findElements(By.tagName("tr"));
                System.out.println("Found rows");
                int rowCount = 1;
                for (WebElement row : rows) {
                    System.out.println("Row -" + rowCount);
                    List<WebElement> cells = row.findElements(By.tagName("td"));

                    if (cells == null || cells.size() < 1) {
                        continue;
                    }
                    String firstcell = cells.get(0).getText();

                    if (firstcell.toUpperCase().trim().equals(claimref.toUpperCase())) {
                        List<WebElement> actionLink = row.findElements(By.linkText(linkToClick));
                        if (!actionLink.isEmpty()) {
                            actionLink.get(0).click();
                            System.out.println("Link clicked successfully by link text - " + linkToClick);
                            return true;
                        }
                    }

                    rowCount++;
                }

                if (!hasLinks) {
                    break;
                } else {
                    pageNumber++;

                    if (Driver.findElement(By.linkText(String.valueOf(pageNumber))) != null) {
                        System.out.println("[info] Attempting to navigate to page " + String.valueOf(pageNumber));
                        JavascriptExecutor js = (JavascriptExecutor) SeleniumDriverInstance.Driver;
                        //  javascript:__doPostBack('ctl00$cntMainBody$ucSelectRiskType$grdvSelectRiskType','Page$3')
                        js.executeScript("javascript:__doPostBack('ctl00$cntMainBody$ClientPolicies$grdvQuotes','Page$" + String.valueOf(pageNumber) + "')");

                    } else {
                        System.err.println("Could not find specified Claim - " + claimNum);
                        return false;
                    }
                }

            }

            System.err.println("Could not find specified Claim - " + claimNum);
            return false;

        } catch (Exception e) {
            System.err.println("Error selecting claim - " + claimNum + " - Error - " + e.getMessage());
            return false;
        }

    }

    public void CloseChromeInstances() throws IOException {
        if (browserType.equals(browserType.Chrome)) {
            String TASKLIST = "tasklist";
            String KILL = "taskkill /IM ";
            String line;
            String serviceName = "chrome.exe";
            Process p = Runtime.getRuntime().exec(TASKLIST);
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            System.out.println("Closing Chrome tasks...");
            while ((line = reader.readLine()) != null) {

                if (line.contains(serviceName)) {
                    Runtime.getRuntime().exec(KILL + serviceName);
                }
            }
        }
    }
}
