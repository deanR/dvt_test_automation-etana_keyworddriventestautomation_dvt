
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

//~--- non-JDK imports --------------------------------------------------------

import KeywordDrivenTestFramework.Entities.Enums.BrowserType;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

/**
 *
 * @author fnell
 */
public final class ApplicationConfig
  {
    private static String ExcelInputFile, ReportFileDirectory, browserTypeConfig, VPNPageUrl, mailingList;
    private static String emailSender, emailHost;
    private static int WaitTimeout;
    private static BrowserType browserType;
    public static String EkunduPageUrl;
    private String appConfigFilePath = "config.properties";
    public Properties appConfig;

    public ApplicationConfig()
      {
        try
          {
            loadConfigurationSettings();
          }
        catch (Exception e)
          {
            // One or more of the appConfig values could not be found in the config file -
            // Reload default values and read from file.
            generateDefaultConfigurationFile();
            loadExistingConfigurationFile();
            loadConfigurationSettings();
          }

      }

    public static int WaitTimeout()
      {
        return WaitTimeout;
      }

    public static String InputFileName()
      {
        return ExcelInputFile;
      }

    public static String VPNPageUrl()
      {
        return VPNPageUrl;
      }

    public static String EkunduPageUrl()
      {
        return EkunduPageUrl;
      }

    public static String ReportFileDirectory()
      {
        return ReportFileDirectory;
      }

    public static BrowserType SelectedBrowser()
      {
        return browserType;
      }

    public static String EmailHost()
      {
        return emailHost;
      }

    public static String[] MailingList()
      {
        return mailingList.split(";");
      }

    public static String EmailSender()
      {
        return emailSender;
      }

    private void loadConfigurationSettings()
      {
        if (!loadExistingConfigurationFile())
          {
            generateDefaultConfigurationFile();
          }

        try
          {
            ExcelInputFile = appConfig.getProperty("ExcelInputFile");
            ReportFileDirectory = appConfig.getProperty("ReportFileDirectory");
            VPNPageUrl = appConfig.getProperty("VPNPageUrl");
            EkunduPageUrl = appConfig.getProperty("EkunduPageUrl");

            mailingList = appConfig.getProperty("MailingList");
            emailSender = appConfig.getProperty("EmailSender");
            emailHost = appConfig.getProperty("EmailHost");

            WaitTimeout = Integer.parseInt(appConfig.getProperty("WaitTimeout"));
            browserType = resolveBrowserType();
          }
        catch (Exception e)
          {
            System.out.println("Error Loading application configuration...see stack trace:");
            e.printStackTrace();
          }

      }

    private BrowserType resolveBrowserType()
      {
        browserTypeConfig = appConfig.getProperty("BrowserType");

        switch (browserTypeConfig)
          {
        case "IE" :
            return BrowserType.IE;

        case "FireFox" :
            return BrowserType.FireFox;

        case "Chrome" :
            return BrowserType.Chrome;

        case "Safari" :
            return BrowserType.Safari;

        default :
            return BrowserType.IE;
          }
      }

    private void generateDefaultConfigurationFile()
      {
        try
          {
            appConfig = new Properties();
            appConfig.setProperty("ExcelInputFile", "Keyword Input.xlsx");
            appConfig.setProperty("ReportFileDirectory", "KeywordDrivenTestReports\\");
            appConfig.setProperty("BrowserType", "IE");
            appConfig.setProperty("WaitTimeout", "15");
            appConfig.setProperty("VPNPageUrl", "https://juniper.hollard.co.za/dana-na/auth/url_default/welcome.cgi");
            appConfig.setProperty("EkunduPageUrl", "http://etaqawb04/ekundu");

            appConfig.setProperty("MailingList", "fnell@dvt.co.za");
            appConfig.setProperty("EmailSender", "Automation@etana.co.za");
            appConfig.setProperty("EmailHost", "localhost");

            appConfig.store(new FileOutputStream(appConfigFilePath), null);

          }
        catch (Exception e)
          {
            System.out.println("Error Loading default configuration...see stack trace:");
            e.printStackTrace();
          }
      }

    private boolean loadExistingConfigurationFile()
      {
        try
          {
            if (appConfig == null)
              {
                appConfig = new Properties();
              }

            appConfig.load(new FileInputStream(appConfigFilePath));

            return true;

          }
        catch (Exception e)
          {
            System.out.println("Configuration file not found, reverting to default configuration...see stack trace:");
            e.printStackTrace();
            System.out.println("Loading default configuration");

            return false;
          }
      }
  }


//~ Formatted by Jindent --- http://www.jindent.com
